<link href="estilos/general.css" rel="stylesheet" type="text/css" />
<table width="780" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="30" class="encabezado-titulo-bg"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="20"><img src="images/espacio.gif" width="1" height="1" /></td>
        <td valign="top" class="encabezado-titulo-texto" style="padding-top:5px;">Postulantes por Usuaria </td>
        </tr>
    </table></td>
  </tr>
  <tr>
    <td><img src="images/espacio.gif" width="1" height="10"></td>
  </tr>
</table>

<table border="0" align="center" cellpadding="0" cellspacing="0" style="margin-bottom:10px;" class="buscar">
<tr><td>
<form action="listado-trayectoria.php" method="get" name="frmFiltro" id="frmFiltro">
		<input name="filtrar" type="hidden" value="1" />
          <?php
				$bDocNro = (is_numeric($_GET["documento_nro"]) ? $_GET["documento_nro"] : "");
				$bCliente = ($_GET["cliente"] ? $_GET["cliente"] : "");
				$bClientePart = ($_GET["clientepart"] ? $_GET["clientepart"] : "");
				$bEstadoPost = (is_numeric($_GET["estadopost"]) ? $_GET["estadopost"] : 0);

			?>
          <table border="0" align="center" cellpadding="0" cellspacing="0"style="margin-top:5px;">
            <tr>
              <td class="encabezado-formulario"> N&ordm; documento:</td>
              <td class="encabezado-formulario"><input name="documento_nro" type="text" id="documento_nro" style="width: 140px;" value="<?php print $bDocNro; ?>" maxlength="8" /></td>
              <td class="encabezado-formulario">Situaci&oacute;n:</td>
              <td class="encabezado-formulario"><select name="estadopost" id="estadopost" style="width: 140px;">
                <?php
					$sSQL = "SELECT PEsNro, PEsDescrip FROM postulacionestado  ";
					print(GenerarOptions($sSQL, $bEstadoPost, true, DEFSELECT));
			  ?>
              </select></td>
              <td rowspan="3" align="right" valign="bottom"><input name="btnFiltrar" type="image" id="btnFiltrar" src="images/btn-buscar.jpg" value="1" alt="Filtrar" /></td>
            </tr>
            
<tr class="encabezado-formulario">
              <td width="80">Usuaria:</td>
              <td width="150"><input name="clientepart" type="text" id="clientepart" style="width: 140px;" value="<?php print $bClientePart; ?>" /></td>
              <td colspan="2"><select name="cliente" id="cliente" style="width: 252px;">
                <?php
					$sSQL = "SELECT CliNro, CliRSocial FROM cliente  ";
					$sSQL .= "ORDER BY CliRSocial ASC ";
					print(GenerarOptions($sSQL, $bCliente, true, DEFSELECT));
			  ?>
              </select></td>
            </tr>  </table>
</form>
</td></tr>
</table>

<?php 
  if ($_GET["filtrar"])
  {
?>
<table width="950" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="20"><img src="images/listado-encabezado-inicio.jpg" width="20" height="37"></td>
        <td class="listado-encabezado-bg"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr class="listado-encabezado-texto">
            <td width="130">Usuaria</td>
            <td width="160">Pedido</td>
            <td width="100">ID</td>
            <td width="190">Apellido y Nombres </td>
            <td width="70">Documento</td>
            <td width="100">Situaci&oacute;n</td>
            <td width="100">Historico</td>
            <td>Fecha</td>
          </tr>
        </table></td>
        <td width="20"><img src="images/listado-encabezado-final.jpg" width="20" height="37"></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="5" class="listado-contenido-inicio"><img src="images/espacio.gif" width="1" height="1"></td>
        <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
		  <?php
		  		$lRegistros = 0;

			 	$sSQL = "SELECT c.CliRSocial, o.OfeTitulo, o.OfeReferencia, ";
			 	$sSQL .= "p.PerApellido, p.PerNombres, p.PerDocumento, ";
			 	$sSQL .= "ep.PEsDescrip AS EstadoUltimo, ep2.PEsDescrip AS EstadoHist, ";
			 	$sSQL .= "ph.phistFecha, phistId ";
				
				$sSQL .= "FROM postulacion po ";
				$sSQL .= "INNER JOIN persona p ON p.PerNro = po.PerNro ";
				$sSQL .= "INNER JOIN oferta o ON o.OfeNro = po.OfeNro ";
				$sSQL .= "INNER JOIN ofertascliente oc ON o.OfeNro = oc.OfeNro ";
				$sSQL .= "INNER JOIN cliente c ON c.CliNro = oc.CliNro ";

				$sSQL .= "INNER JOIN postulacionestado ep ON ep.PEsNro = po.PEsNro ";
				
				$sSQL .= "INNER JOIN postulacionhistorico ph ";
				$sSQL .= "ON (ph.OfeNro = po.OfeNro AND ph.PerNro = po.PerNro) ";
				
				$sSQL .= "INNER JOIN postulacionestado ep2 ON ep2.PEsNro = ph.PEsNro ";
				
				$sSQL .= "WHERE 1 ";

				if ($bDocNro > 0)
					$sSQL .= "AND p.PerDocumento = ". $bDocNro ." ";
				if ($bEstadoPost > 0)
					$sSQL .= "AND ph.PEsNro = ". $bEstadoPost ." ";
				if ($bClientePart <> "")
					$sSQL .= "AND c.CliRSocial LIKE '%". $bClientePart ."%' ";
				if ($bCliente > 0)
					$sSQL .= "AND c.CliNro = ". $bCliente ." ";
				
				$sSQL .= "ORDER BY po.PosFecha, ph.phistFecha DESC ";

				//echo $sSQL;
				
				$cBD = new BD();
				$oResultado = $cBD->Seleccionar($sSQL);
				while($aRegistro = $cBD->RetornarFila($oResultado))
				{
					$sPosicion = (($sPosicion == "1") ? "2" : "1");
		  ?>
          <tr>
            <td class="listado-fila-bg-<?php print($sPosicion); ?>"  style="padding: 3px 0;">
			
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr valign="top" class="listado-texto">
                <td width="15"><img src="images/espacio.gif" width="1" height="1"></td>
                <td width="130"><?php print ReemplazarCaracteres($aRegistro["CliRSocial"]); ?></td>
                <td width="160"><?php print ReemplazarCaracteres($aRegistro["OfeTitulo"]); ?></td>
                <td width="100"><?php print ReemplazarCaracteres($aRegistro["OfeReferencia"]); ?></td>
                <td width="190"><?php print ReemplazarCaracteres($aRegistro["PerApellido"].", ".$aRegistro["PerNombres"]); ?></td>
                <td width="70"><?php print($aRegistro["PerDocumento"]); ?></td>
                <td width="100"><?php print($aRegistro["EstadoUltimo"]); ?></td>
                <td width="100"><?php print ($aRegistro["EstadoHist"]); ?></td>
                <td><?php print date("d/m/y", strtotime($aRegistro["phistFecha"])); ?></td>
              </tr>
			  
            </table></td>
          </tr>
          <?php
			 		$lRegistros++;
				}
				if($lRegistros == 0)
				{
			 ?>
          <tr>
            <td><img src="images/espacio.gif" width="1" height="20"></td>
          </tr>
			 <?php } ?>
        </table></td>
        <td width="6" class="listado-contenido-final"><img src="images/espacio.gif" width="1" height="1"></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="100"><img src="images/listado-pie-inicio-deshabilitado.jpg" alt="Agregar" width="100" height="40" border="0"></td>
        <td class="listado-pie-bg">&nbsp;</td>
        <td width="20"><img src="images/listado-pie-final.jpg" width="20" height="40"></td>
      </tr>
    </table></td>
  </tr>
</table>
<?php 			}
?>
