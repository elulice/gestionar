<?php
//ini_set('display_errors', 1);

require_once("clases/framework-1.0/class.bd.php");
require_once("clases/phppaging/PHPPaging.lib.php");
require_once("includes/funciones.php");

// Iniciar variables
$fields = array();
$filters = array();
$arraySql = array();
$arraySqlCount = array();
$arraySqlChart = array();
$arrayFilters = array();

// Verificar que se hayan seleccionado campos
if (isset($_GET['fields'])) {

    // Si hay campos de la tabla all_tickets...
    if (isset($_GET['fields']['all_tickets'])) {
        foreach ($_GET['fields']['all_tickets'] as $k => $v) {
            switch ($k) {
                case 'id': $value = 'Nro. Ticket';
                    break;
                case 'email_cliente': $value = 'Email';
                    break;
                case 'fecha': $value = 'Fecha Alta';
                    break;
                case 'email_send': $value = 'Copiado a';
                    break;
                case 'email_cc': $value = 'Copiado a 2';
                    break;
                case 'tickets_nombre': $value = 'Nombre Solic.';
                    break;
                case 'ticket_contenido': $value = 'Reclamo';
                    break;
                case 'creacion_respuesta': $value = 'Creación/Respuesta';
                    break;
                case 'creacion_cierre': $value = 'Creación/Cierre';
                    break;
                case 'respuesta_cierre': $value = 'Respuesta/Cierre';
                    break;
            }
            $fields['all_tickets.' . $k] = $value;
        }
    }
    if (isset($_GET['fields']['estado_tickets'])) {
        foreach ($_GET['fields']['estado_tickets'] as $k => $v) {
            switch ($k) {
                case 'nombre': $value = 'Estado';
                    break;
            }
            $fields['estado_tickets.' . $k] = $value;
        }
    }
    if (isset($_GET['fields']['respuesta_tickets'])) {
        foreach ($_GET['fields']['respuesta_tickets'] as $k => $v) {
            switch ($k) {
                case 'respuesta_fecha': $value = 'Fecha Envio';
                    break;
                case 'responsed_by': $value = 'Respondido por';
                    break;
            }
            $fields['respuesta_tickets.' . $k] = $value;
        }
    }
    if (isset($_GET['fields']['cliente'])) {
        foreach ($_GET['fields']['cliente'] as $k => $v) {
            switch ($k) {
                case 'CliRsocial': $value = 'Cliente';
                    break;
            }
            $fields['cliente.' . $k] = $value;
        }
    }
    if (isset($_GET['fields']['unidadorg'])) {
        foreach ($_GET['fields']['unidadorg'] as $k => $v) {
            $value = 'Sucursal';
            $fields['unidadorg.UniNombre'] = $value;
        }
    }


    if (isset($_GET['fields']['respuesta_tickets'])) {
        foreach ($_GET['fields']['respuesta_tickets'] as $k => $v) {
            switch ($k) {
                case 'content_response': $value = 'Respuesta Enviada';
                    break;
            }
            $fields['respuesta_tickets.content_response'] = $value;
        }
    }


    if (isset($_GET['fields']['origen_tickets'])) {
        foreach ($_GET['fields']['origen_tickets'] as $k => $v) {
            $value = 'Origen';
            $fields['origen_tickets.nombre_origen'] = $value;
        }
    }
    if (isset($_GET['fields']['tipo_solicitante'])) {
        foreach ($_GET['fields']['tipo_solicitante'] as $k => $v) {
            $value = 'Tipo Solicitante';
            $fields['tipo_solicitante.nombre'] = $value;
        }
    }
    if (isset($_GET['fields']['miembroempresa'])) {
        foreach ($_GET['fields']['miembroempresa'] as $k => $v) {
            $value = 'Responsable';
            $fields['miembroempresa.MEmpNombres'] = $value;
        }
    }
    if (isset($_GET['fields']['responded_to_client'])) {
        foreach ($_GET['fields']['responded_to_client'] as $k => $v) {
            $value = 'Fecha Respuesta';
            $fields['responded_to_client.rtc_fecha'] = $value;
        }
    }
    if (isset($_GET['fields']['tickets_edit'])) {
        foreach ($_GET['fields']['tickets_edit'] as $k => $v) {
            $value = 'Editado Por';
            $fields['tickets_edit.edited_by'] = $value;
        }
    }
    if (isset($_GET['fields']['motivos'])) {
        foreach ($_GET['fields']['motivos'] as $k => $v) {
            $value = 'Motivo';
            $fields['motivos.nombre_motivo'] = $value;
        }
    }
    if (isset($_GET['fields']['tipo_error'])) {
        foreach ($_GET['fields']['tipo_error'] as $k => $v) {
            $value = 'Tipo de Error';
            $fields['tipo_error.nombre_error'] = $value;
        }
    }
    if (isset($_GET['fields']['area'])) {
        foreach ($_GET['fields']['area'] as $k => $v) {
            $value = 'Area';
            $fields['area.AreNom'] = $value;
        }
    }
}

// Configurar los filtros
if (isset($_GET['filters'])) {
    foreach ($_GET['filters'] as $k => $v) {
        switch ($k) {
            case 'fecha':
                if ($_GET['filters']['fecha']['desde']) {
//                    $filters[] = 'fecha >= \'' . date('Y-m-d', strtotime($_GET['filters']['fecha']['desde'])) . '\'';
                    $filters[] = 'fecha >= \'' . get_fecha($_GET['filters']['fecha']['desde']) . '\'';
                    $arrayFilters[] = 'Fecha desde: ' . date('d/m/Y', strtotime($_GET['filters']['fecha']['desde']));
                }
                if ($_GET['filters']['fecha']['hasta']) {
                    $filters[] = 'fecha <= \'' . get_fecha($_GET['filters']['fecha']['hasta']) . '\'';
                    $arrayFilters[] = 'Fecha hasta: ' . date('d/m/Y', strtotime($_GET['filters']['fecha']['hasta']));
                }
                break;

            case 'fecha_respuesta':
                if ($_GET['filters']['fecha_respuesta']['desde']) {
                    $filters[] = 'respuesta_tickets.respuesta_fecha >= \'' . get_fecha($_GET['filters']['fecha_respuesta']['desde']) . '\'';
                    $arrayFilters[] = 'Fecha desde: ' . get_fecha($_GET['filters']['fecha_respuesta']['desde']);
                }
                if ($_GET['filters']['fecha_respuesta']['hasta']) {
                    $filters[] = 'respuesta_tickets.respuesta_fecha <= \'' . get_fecha($_GET['filters']['fecha_respuesta']['hasta']) . '\'';
                    $arrayFilters[] = 'Fecha hasta: ' . date('d/m/Y', strtotime($_GET['filters']['fecha_respuesta']['hasta']));
                }
                break;

            case 'fecha_envio':
                if ($_GET['filters']['fecha_envio']['desde']) {
                    $filters[] = 'responded_to_client.rtc_fecha >= \'' . get_fecha($_GET['filters']['fecha_envio']['desde']) . '\'';
                    $arrayFilters[] = 'Fecha desde: ' . date('d/m/Y', strtotime($_GET['filters']['fecha_envio']['desde']));
                }
                if ($_GET['filters']['fecha_envio']['hasta']) {
                    $filters[] = 'responded_to_client.rtc_fecha <= \'' . get_fecha($_GET['filters']['fecha_envio']['hasta']) . '\'';
                    $arrayFilters[] = 'Fecha hasta: ' . date('d/m/Y', strtotime($_GET['filters']['fecha_envio']['hasta']));
                }
                break;

            case 'estado':
                if ($_GET['filters']['estado']['id']) {
                    if ($_GET['filters']['estado']['id'] == 4) {
                        $filters[] = 'responsed = "S" ';
                        $arrayFilters[] = 'Estado: Respondido';
                    } else if ($_GET['filters']['estado']['id'] == 5) {
                        $filters[] = 'responsed = "N" ';
                        $arrayFilters[] = 'Estado: No Respondido';
                    } else if ($_GET['filters']['estado']['id'] == 6) {
                        $filters[] = 'estado_id != 3 ';
                        $arrayFilters[] = 'Estado: Abierto';
                    } else {
                        $filters[] = 'estado_id = ' . $_GET['filters']['estado']['id'];
                        $arrayFilters[] = 'Estado: ' . $_GET['filters']['estado']['name'];
                    }
                }
                break;

            case 'sucursal':
                if ($_GET['filters']['sucursal']['id']) {
                    $filters[] = 'sucursal_id = ' . $_GET['filters']['sucursal']['id'];
                    $arrayFilters[] = 'Sucursal: ' . $_GET['filters']['sucursal']['name'];
                }
                break;

            case 'responsed_by':
                if ($_GET['filters']['responsed_by']['id']) {
                    $filters[] = "respuesta_tickets.responsed_by LIKE '%" . $_GET['filters']['responsed_by']['name'] . "%'";
                    $arrayFilters[] = 'Respondido Por: ' . $_GET['filters']['responsed_by']['name'];
                }
                break;

            case 'edited_by':
                if ($_GET['filters']['edited_by']['id']) {
                    $filters[] = "tickets_edit.edited_by LIKE '%" . $_GET['filters']['edited_by']['name'] . "%'";
                    $arrayFilters[] = 'Editado Por: ' . $_GET['filters']['edited_by']['name'];
                }
                break;

            case 'usuaria':
                if ($_GET['filters']['usuaria']['id']) {
                    $filters[] = "cliente_id = " . $_GET['filters']['usuaria']['id'];
                    $arrayFilters[] = 'Editado Por: ' . $_GET['filters']['usuaria']['name'];
                }
                break;

            case 'solicitante':
                if ($_GET['filters']['solicitante']['id']) {
                    $filters[] = "origen_id = " . $_GET['filters']['solicitante']['id'];
                    $arrayFilters[] = 'Tipo Solicitante: ' . $_GET['filters']['solicitante']['name'];
                }
                break;

            case 'responsed':
                if ($_GET['filters']['responsed']['id']) {
                    $filters[] = "responsed = '" . $_GET['filters']['responsed']['id'] . "'";
                    $arrayFilters[] = 'Respondido: ' . $_GET['filters']['responsed']['name'];
                }
                break;

            case 'enviado':
                if ($_GET['filters']['enviado']['id']) {
                    $filters[] = "response_sended_to_client = '" . $_GET['filters']['enviado']['id'] . "'";
                    $arrayFilters[] = 'Resp. Enviada: ' . $_GET['filters']['enviado']['name'];
                }
                break;
        }
    }
}

// Configurar los filtros
$groupby = '';
$stringGroupBy = '';
if (isset($_GET['groupby']) && $_GET['groupby']) {
    $groupby = str_replace('-', '.', $_GET['groupby']);
    $stringGroupBy = '';
}

// Configurar los filtros
$orderby = '';
if (isset($_GET['orderby']) && $_GET['orderby']) {
    $orderby = str_replace('-', '.', $_GET['orderby']) . ' ' . $_GET['orderdir'];
}

if ($groupby) {
    $fields['COUNT(all_tickets.id) AS count'] = 'Cantidad de tickets';
}

// Si no tiene seleccionado campos busca el primero
if (isset($_GET['c']['x']) && $_GET['c']['x']) {
    $x = $_GET['c']['x'];
    $xName = $fields[$x];
} else {
    $xName = reset($fields);
    $x = key($fields);
}

// Armar array del query
$arraySqlChart[] = 'SELECT ' . $x . ' AS name, COUNT(all_tickets.id) AS count FROM all_tickets';
$arraySqlCount[] = 'SELECT COUNT(all_tickets.id) AS count FROM all_tickets';
$arraySql[] = 'SELECT ' . implode(', ', array_keys($fields)) . ' FROM all_tickets';

if (isset($_GET['fields']['estado_tickets'])) {
    $sqlPart = 'LEFT JOIN estado_tickets ON estado_tickets.id = all_tickets.estado_id';
    $arraySql[] = $sqlPart;
    $arraySqlCount[] = $sqlPart;
    $arraySqlChart[] = $sqlPart;
}

if (isset($_GET['fields']['respuesta_tickets'])) {
    $sqlPart = 'LEFT JOIN respuesta_tickets ON respuesta_tickets.ticket_id = all_tickets.id';
    $arraySql[] = $sqlPart;
    $arraySqlCount[] = $sqlPart;
    $arraySqlChart[] = $sqlPart;
//} else if (isset($_GET['filters']['fecha_respuesta']["desde"]) || isset($_GET['filters']['responsed_by'])) {
} else if (!empty($_GET['filters']['fecha_respuesta']["desde"]) || !empty($_GET['filters']['fecha_respuesta']["hasta"]) || !empty($_GET['filters']['responsed_by']["id"])) {
    $sqlPart = 'LEFT JOIN respuesta_tickets ON respuesta_tickets.ticket_id = all_tickets.id';
    $arraySql[] = $sqlPart;
    $arraySqlCount[] = $sqlPart;
    $arraySqlChart[] = $sqlPart;
}

if (isset($_GET['fields']['cliente'])) {
    $sqlPart = 'LEFT JOIN cliente ON cliente.CliNro = all_tickets.cliente_id';
    $arraySql[] = $sqlPart;
    $arraySqlCount[] = $sqlPart;
    $arraySqlChart[] = $sqlPart;
}
if (isset($_GET['fields']['unidadorg'])) {
    $sqlPart = 'LEFT JOIN unidadorg ON unidadorg.UniNro = all_tickets.sucursal_id';
    $arraySql[] = $sqlPart;
    $arraySqlCount[] = $sqlPart;
    $arraySqlChart[] = $sqlPart;
}
if (isset($_GET['fields']['origen_tickets'])) {
    $sqlPart = 'LEFT JOIN origen_tickets ON origen_tickets.id = all_tickets.origen_id';
    $arraySql[] = $sqlPart;
    $arraySqlCount[] = $sqlPart;
    $arraySqlChart[] = $sqlPart;
}
if (isset($_GET['fields']['tipo_solicitante'])) {
    $sqlPart = 'LEFT JOIN tipo_solicitante ON all_tickets.origen_id = tipo_solicitante.idsolicitante';
    $arraySql[] = $sqlPart;
    $arraySqlCount[] = $sqlPart;
    $arraySqlChart[] = $sqlPart;
}
if (isset($_GET['fields']['miembroempresa'])) {
    $sqlPart = 'LEFT JOIN miembroempresa ON all_tickets.responsable_id = miembroempresa.PerNro';
    $arraySql[] = $sqlPart;
    $arraySqlCount[] = $sqlPart;
    $arraySqlChart[] = $sqlPart;
}
if (isset($_GET['fields']['area'])) {
    $sqlPart = 'LEFT JOIN area ON all_tickets.sector_id = area.AreNro';
    $arraySql[] = $sqlPart;
    $arraySqlCount[] = $sqlPart;
    $arraySqlChart[] = $sqlPart;
}

if (isset($_GET['fields']['responded_to_client'])) {
    $sqlPart = 'LEFT JOIN responded_to_client ON all_tickets.id = responded_to_client.rtc_id_ticket';
    $arraySql[] = $sqlPart;
    $arraySqlCount[] = $sqlPart;
    $arraySqlChart[] = $sqlPart;
} else if (!empty($_GET['filters']['fecha_envio']["desde"]) || !empty($_GET['filters']['fecha_envio']["hasta"])) {
    $sqlPart = 'LEFT JOIN responded_to_client ON all_tickets.id = responded_to_client.rtc_id_ticket';
    $arraySql[] = $sqlPart;
    $arraySqlCount[] = $sqlPart;
    $arraySqlChart[] = $sqlPart;
}

if (isset($_GET['fields']['tickets_edit'])) {
    $sqlPart = 'LEFT JOIN tickets_edit ON all_tickets.id = tickets_edit.ticket_id';
    $arraySql[] = $sqlPart;
    $arraySqlCount[] = $sqlPart;
    $arraySqlChart[] = $sqlPart;
} else if (!empty($_GET['filters']['edited_by']["id"])) {
    $sqlPart = 'LEFT JOIN tickets_edit ON all_tickets.id = tickets_edit.ticket_id';
    $arraySql[] = $sqlPart;
    $arraySqlCount[] = $sqlPart;
    $arraySqlChart[] = $sqlPart;
}

if (isset($_GET['fields']['motivos'])) {
    $sqlPart = 'LEFT JOIN motivos ON all_tickets.motivo_id = motivos.id';
    $arraySql[] = $sqlPart;
    $arraySqlCount[] = $sqlPart;
    $arraySqlChart[] = $sqlPart;
}
if (isset($_GET['fields']['tipo_error'])) {
    $sqlPart = 'LEFT JOIN tipo_error ON all_tickets.tipo_error_id = tipo_error.iderror';
    $arraySql[] = $sqlPart;
    $arraySqlCount[] = $sqlPart;
    $arraySqlChart[] = $sqlPart;
}

if ($filters) {
    $sqlPart = 'WHERE ' . implode(' AND ', $filters);
    $arraySql[] = $sqlPart;
    $arraySqlCount[] = $sqlPart;
    $arraySqlChart[] = $sqlPart;
}

// Y estan visibles
if ($filters) {
    $arraySql[] = ' AND all_tickets.visible = "S" ';
} else {
    $arraySql[] = ' WHERE all_tickets.visible = "S" ';
}

if ($groupby) {
    $arraySql[] = 'GROUP BY ' . $groupby;
}
if ($orderby) {
    $arraySql[] = 'ORDER BY ' . $orderby;
}

// Iniciar BD
$dbN = new BD();
$dbN->Conectar();
$sql2 = implode(' ', $arraySql);
$sqlSecondary = $dbN->Seleccionar($sql2);

// Cerrar BD 
// Iniciar BD
$db = new BD();
$db->Conectar();

// Generar el SQL definitivo
$sql = implode(' ', $arraySql);
$sqlCount = implode(' ', $arraySqlCount);

//die($sql);

// Ejecutar la consulta con paginacińo
$paging = new PHPPaging($db->RetornarConexion());
$paging->agregarConsulta($sql);
$paging->linkClase("navPage");
$paging->porPagina(10);
$paging->ejecutar();
//$count = $paging->numTotalRegistros();

$resultCount = $db->Seleccionar($sqlCount, true);
$count = $resultCount['count'];

if (isset($_GET['c']) && $_GET['c']) {
    $chartValues = $_GET['c'];
}
?>

<?php if (isset($_GET['chart']) && $_GET['chart'] == 1): ?>

    <?php
    if (!$chartValues['type']) {
        $chartValues['type'] = 'bar';
    }

    $arraySqlChart[] = 'GROUP BY ' . $x;
    $sqlChart = implode(' ', $arraySqlChart);


    $resultChart = $db->Seleccionar($sqlChart);

    $data = array();
    $dataPie = array();
    $dataPie['type'] = 'pie';
    $dataPie['name'] = 'Cantidad de tickets';

    while ($row = $db->RetornarFila($resultChart)) {
        $data[] = array('name' => $row['name'], 'data' => array((float) $row['count']));
        $dataPie['data'][] = array($row['name'], (float) $row['count']);
    }
    ?>

    <script src="scripts/highchart/highcharts.js" type="text/javascript"></script>
    <script>
        var chart; // globally available
        $(document).ready(function() {
    <?php if ($chartValues['type'] == 'bar' || $chartValues['type'] == 'column'): ?>
                chart = new Highcharts.Chart({
                    chart: {
                        renderTo: 'chart-container',
                        type: '<?php echo $chartValues['type'] ?>'
                    },
                    title: {
                        text: '<?php echo $chartValues['title'] ?>'
                    },
                    xAxis: {
                        categories: ['<?php echo $xName ?>']
                    },
                    yAxis: {
                        title: {
                            text: 'Cantidad Ticket'
                        }
                    },
                    series: <?php echo json_encode($data) ?>,
                    credits:{enabled:false}
                });
    <?php elseif ($chartValues['type'] == 'pie'): ?>
                chart = new Highcharts.Chart({
                    chart: {
                        renderTo: 'chart-container',
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false
                    },
                    title: {
                        text: '<?php echo $chartValues['title'] ?>'
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage}%</b>',
                        percentageDecimals: 1,
                        formatter: function() {
                            return '<b>'+ this.point.name +'</b>: '+ Math.round(this.percentage) +' %';
                        }
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                color: '#000000',
                                connectorColor: '#000000',
                                formatter: function() {
                                    return '<b>'+ this.point.name +'</b>: '+ Math.round(this.percentage) +' %';
                                }
                            }
                        }
                    },
                    series: [<?php echo json_encode($dataPie) ?>],
                    credits:{enabled:false}
                });
    <?php endif; ?>
        });

    </script>

    <div id="menu-chart-div" style="width: 900px; float:left; height: 35px;">
        <form method="POST" action="#" onsubmit="return false;" name="form-grafico" id="form_chart">
            <label style="float: left; display: block; margin-left: 5px;">Tipo de Grafico:</label>
            <select name="c[type]" style="float: left; display: block; margin-left: 5px;">
                <option value="bar" <?php echo ($chartValues['type'] == 'bar') ? 'selected="selected"' : '' ?>>Barras</option>
                <option value="column" <?php echo ($chartValues['type'] == 'column') ? 'selected="selected"' : '' ?>>Columnas</option>
                <option value="pie" <?php echo ($chartValues['type'] == 'pie') ? 'selected="selected"' : '' ?>>Torta</option>
                <option style="display:none;" disabled="disabled" value="line" <?php echo ($chartValues['type'] == 'line') ? 'selected="selected"' : '' ?>>Lineas</option>
            </select>
            <label style="float: left; display: block; margin-left: 5px;">X:</label>
            <select name="c[x]" style="float: left; display: block; margin-left: 5px;" id="x-axis">
                <?php foreach ($fields as $k => $v): ?>
                    <option value="<?php echo $k ?>" <?php echo ($k == $x) ? 'selected="selected"' : '' ?>><?php echo $v ?></option>
                <?php endforeach; ?>
            </select>
            <label style="float: left; display: block; margin-left: 5px;">Y:</label>
            <select name="c[function]" style="float: left; display: block; margin-left: 5px;">
                <option value="contar">Contar</option>
            </select>
            <select name="c[y]" style="float: left; display: block; margin-left: 5px;" id="y-axis">
                <option value="tickets_count">Cantidad Tickets</option>
            </select>
            <label style="float: left; display: block; margin-left: 5px;">Titulo:</label>
            <input style="float: left; display: block; margin-left: 5px; width: 120px;" type="text" name="c[title]" value="<?php echo $chartValues['title'] ?>">
        </form>
        <a style="float: left; display: block; margin-left: 5px; font-weight: normal;" href="javascript:void(0);" onclick="loadChart();" class="buttons buttons-graficar ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary" title="Crear Grafico" role="button"><span class="ui-button-icon-primary ui-icon ui-icon-image"></span><span class="ui-button-text">Graficar</span></a>
        <a style="float: left; display: block; margin-left: 5px; font-weight: normal;" href="javascript:void(0);" onclick="loadList();" class="buttons buttons-graficar ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary" title="Crear Grafico" role="button"><span class="ui-button-icon-primary ui-icon ui-icon-image"></span><span class="ui-button-text">Volver</span></a>
    </div>
    <div id="chart-container" style="float: left; display: block;"></div>
<?php else: ?>
    <table width="830" cellpadding="0" cellspacing="0" style="margin:12px 0px 0px 12px;" id="box-table-a" summary="Employee Pay Sheet">
        <thead>
            <tr>
                <?php foreach ($fields as $field): ?>
                    <th scope="col"><span style="color:#C60; font-weight:bold;"><?php echo ($field != 'Fecha Envio') ? $field : 'Fecha Envio/Cierre'; ?></span></th>
                <?php endforeach; ?>
            </tr>
        </thead>
        <tbody>
            <?php $e = 0; ?>
            <?php while ($row = $paging->fetchResultado()): ?>
                <tr>
                    <?php $i = 0; ?>
                    <?php foreach (array_keys($fields) as $field): ?>
                        <?
                        if ($field == "all_tickets.ticket_contenido"):
//                            echo "CONTENIDO";
                            $reclamo = myTruncate($row[$i], 200, ' ', '...');
                            $class = "class='tooltip'";
                        else:
                            $reclamo = $row[$i];
                            $class = "";
                        endif;
                        if ($field == "all_tickets.creacion_respuesta" || $field == "all_tickets.creacion_cierre" || $field == "all_tickets.respuesta_cierre"):
                            $reclamo = time_elapsed_string($row[$i]);
                        endif;
                        ?>
                        <td style="padding-left:8px;" <?php echo $class; ?> title="<?php echo $row[$i]; ?>"><?php echo $reclamo ?></td>
                        <?php $i++; ?>
                    <?php endforeach; ?>
                </tr>
            <?php endwhile; ?>
        </tbody>
    </table>
    <table width="830" cellpadding="0" cellspacing="0" style="margin:12px 0px 0px 12px;display: none" id="box-table-a-hidden" summary="Employee Pay Sheet">
        <thead>
            <tr>
                <?php foreach ($fields as $field2): ?>
                    <th scope="col"><span style="color:#C60; font-weight:bold;"><?php echo ($field2 != 'Fecha Envio') ? $field2 : 'Fecha Envio/Cierre'; ?></span></th>
                <?php endforeach; ?>
            </tr>
        </thead>
        <tbody>
            <?php while ($row2 = $dbN->RetornarFila($sqlSecondary)): ?>
                <tr>
                    <?php $z = 0; ?>
                    <?php foreach (array_keys($fields) as $field2): ?>
                        <td style="padding-left:8px;"><?php echo $row2[$z] ?></td>
                        <?php $z++; ?>
                    <?php endforeach; ?>
                </tr>
            <?php endwhile; ?>
        </tbody>
    </table>
    <script type="text/javascript" src="scripts/jquery-1.9.1.js"></script>
    <script type="text/javascript" src="scripts/jquery-ui-1.10.2.custom.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            j191 = jQuery.noConflict();
            var table = $("#box-table-a-hidden").html()
            $("#hidden_export_excel").val("");
            $("#hidden_export_excel").val("<table>" + table + "</table>");
            $("#hidden_export_pdf").val("");
            $("#hidden_export_pdf").val("<table>" + table + "</table>");
                                                                                       
            j191(".tooltip").tooltip({
                tooltipClass: "z-index",
                track: true
            });
                                                                        
        })
    </script>
    <form id="excel" method="post" action="exportar_reporte.php?objeto=excel">
        <input type="hidden" name="export_excel" id="hidden_export_excel"/>
    </form>
    <form id="pdf" method="post" action="exportar_reporte.php?objeto=pdf">
        <input type="hidden" name="export_pdf" id="hidden_export_pdf"/>
    </form>
    <div class="pagination" style="margin-right: 20px; margin-top: 5px; margin-bottom: 10px;"><?php echo $paging->fetchNavegacion(); ?></div>
<?php endif; ?>

<div id="tot-filtros" style="clear:both; float:left; background:#efefef; width:860px; margin-left:10px; padding:5px;">
    <strong>Cantidad tickets: </strong><?php echo $count ?>
    <?php if ($filters): ?>
        <br />
        <strong>Filtros: </strong><?php echo implode(' | ', $arrayFilters) ?>
    <?php endif; ?>
    <?php if ($stringGroupBy): ?>
        <br />
        <strong>Agrupado por: </strong><?php echo $stringGroupBy ?>
    <?php endif; ?>
</div>

<style>
    form label { width: auto; }
    #chart-container { width:890px; }
</style>