<?php
	if($m_lIDRegistro > 0)
	{
		$sSQL = "SELECT p.PerApellido, p.PerNombres, pn.*  ";
		$sSQL .= "FROM persona p ";
		$sSQL .= "LEFT JOIN postulantenotas pn ON p.PerNro = pn.PerNro ";
		$sSQL .= "WHERE p.PerNro = " . $m_lIDRegistro;
		$cBD = new BD();
		$aRegistro = $cBD->Seleccionar($sSQL, true);
	}
		$aRegistro["NotNro"] = (is_numeric($aRegistro["NotNro"]) ? $aRegistro["NotNro"]: 0);
?>
<link href="estilos/general.css" rel="stylesheet" type="text/css" />


<table width="780" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="30" class="encabezado-titulo-bg"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="20"><img src="images/espacio.gif" width="1" height="1"></td>
        <td class="encabezado-titulo-texto">Perfil del Postulante </td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><img src="images/espacio.gif" width="1" height="20"></td>
  </tr>
</table>
<table width="500" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="20"><img src="images/formulario-encabezado-inicio.jpg" width="20" height="37" /></td>
        <td class="formulario-encabezado-bg">Modificaci&oacute;n del Postulante - <?php print($aRegistro["PerApellido"]); ?>, <?php print($aRegistro["PerNombres"]); ?></td>
        <td width="20"><img src="images/formulario-encabezado-final.jpg" width="20" height="37" /></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="4" class="formulario-contenido-inicio"><img src="images/espacio.gif" width="1" height="1" /></td>
        <td>
		
		
		<!-- <form>-->
          <table width="100%" border="0" cellpadding="4" cellspacing="0">
		  
		  <input name="PerNro" id="PerNro" type="hidden" value="<?php print $m_lIDRegistro; ?>" />
		  <input name="EmpNro" id="EmpNro" type="hidden" value="<?php print RetornarIdEmpresa(); ?>" />
<?php
	$sSQL = "SELECT g.GruNro, g.GruNombre FROM grupo g ";
	$sSQL .= "INNER JOIN grupoempresa ge ON ge.GruNro = g.GruNro ";
	$sSQL .= "WHERE ge.EmpNro = ". RetornarIdEmpresa();
	
	$cBD = new BD();
	$oResultado = $cBD->Seleccionar($sSQL);
	while($aRegistro = $cBD->RetornarFila($oResultado))
	{
?>	
		  
         <tr valign="bottom">
              <td width="10" height="20" class="detalle-seccion">&nbsp;</td>
              <td colspan="2" class="detalle-seccion"><?php print $aRegistro["GruNombre"]; ?></td>
              </tr>
<?php 			  
		$sSQL = "SELECT dp.DPerNro, dp.PerNro, AtrS.EmpNro, AtrS.GruNro, AtrS.AtrNro, ";
		$sSQL .= "dp.DPerValor, AtrS.AtrTipoDato, AtrS.AtrNombre ";
		$sSQL .= "FROM descrippersona dp ";
		$sSQL .= "RIGHT OUTER JOIN ( ";
		
		$sSQL .= "SELECT ge.EmpNro, g.GruNro, a.AtrNro, a.AtrNombre, a.AtrTipoDato ";
		$sSQL .= "FROM grupoempresa ge ";
		$sSQL .= "INNER JOIN grupo g ON ge.GruNro = g.GruNro ";
		$sSQL .= "INNER JOIN grupoatributo ga ON ga.GruNro = g.GruNro ";
		$sSQL .= "INNER JOIN atributo a ON a.AtrNro = ga.AtrNro ";
		$sSQL .= "WHERE ge.EmpNro = '". RetornarIdEmpresa()."' AND g.GruNro = ". $aRegistro["GruNro"] ." ";
		$sSQL .= ") AS AtrS ON AtrS.AtrNro = DP.AtrNro ";
		$sSQL .= "WHERE ISNULL( `PerNro` ) ";
		$sSQL .= "OR PerNro = ". $m_lIDRegistro ; 		

		$cBD = new BD();
		$oResultado2 = $cBD->Seleccionar($sSQL);

		while($bRegistro = $cBD->RetornarFila($oResultado2))
		{
           
		   	$bRegistro["DPerNro"] = is_numeric($bRegistro["DPerNro"]) ? $bRegistro["DPerNro"] : 0;
			
				if ($bRegistro["AtrTipoDato"] == "B")
				{
					$valores = array(array("0", ""), array("1", "Si"), array("2", "No")) ;
					
					switch ($bRegistro["DPerValor"])
					{
						case "Si" : $s = 1; break;
						case "No" : $s = 2; break;
						default: $s = 0; break;
					}			
?>            <tr>
              <td width="10" valign="top" class="formulario-etiquetas">&nbsp;</td>
              <td width="100" valign="top" class="formulario-etiquetas"><?php print $bRegistro["AtrNombre"]; ?></td>
              <td style="padding-left:10px;">
			  <select name='atr' class="formulario-textbox" onchange="cambiaAtributoValor(<?php print $bRegistro["DPerNro"]; ?>,<?php print $m_lIDRegistro; ?>,<?php print $bRegistro["GruNro"]; ?>,<?php print $bRegistro["AtrNro"]; ?>, this)" style="width:50px;">
			  <?php 
			   	print GenerarOptionsArray($valores, $s);
			  ?>
			  </select>
			  </td>
              </tr>
			  
		<?php } ?>
	<?php } ?>
<?php } ?>
			  
          </table>
        <!--</form>-->
		
		</td>
        <td width="6" class="formulario-contenido-final"><img src="images/espacio.gif" width="1" height="1" /></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="20"><img src="images/formulario-pie-inicio.jpg" width="20" height="40" /></td>
        <td class="formulario-pie-bg"><img src="images/espacio.gif" width="1" height="1" /></td>
        <td width="20"><img src="images/formulario-pie-final.jpg" width="20" height="40" /></td>
      </tr>
    </table></td>
  </tr>
</table>
<script>
	function cambiaAtributoValor(IDRegistro,IDPersona,IDGrupo,IDAtributo, pSelect)
	{
		var sValor = pSelect.options[pSelect.selectedIndex].text;
		
		if(confirm("¿Seguro que desea modificar este valor?"))
		{
			x_CambiaAtributoValor(IDRegistro,IDPersona,IDGrupo,IDAtributo, sValor, function(pCadena)
		{
			alert(sValor);
		});

		}
		return true;
	}
</script>
