<?php
	if($m_lIDRegistro > 0)
	{
		$sSQL = "SELECT CliRsocial, CliContacto, ";
		$sSQL .= "CliTelefono, CliEMail, CliDomicilio, ";
		$sSQL .= "CliCUIT, CliRubro, CliEjCta, CliEMailEjCta, ";
		$sSQL .= "CliFechaInicio ";
		$sSQL .= "FROM cliente WHERE CliNro = " . $m_lIDRegistro;
		$cBD = new BD();
		$aRegistro = $cBD->Seleccionar($sSQL, true);
	} else {
		$aRegistro["CliFechaInicio"] = date("Y-m-d");
	}

?>
<link href="estilos/general.css" rel="stylesheet" type="text/css" />

<table width="780" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="30" class="encabezado-titulo-bg"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="20"><img src="images/espacio.gif" width="1" height="1"></td>
        <td class="encabezado-titulo-texto">Alta y modificaci&oacute;n de Usuarias </td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><img src="images/espacio.gif" width="1" height="20"></td>
  </tr>
</table>
<table width="500" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="20"><img src="images/formulario-encabezado-inicio.jpg" width="20" height="37"></td>
        <td class="formulario-encabezado-bg">Usuaria</td>
        <td width="20"><img src="images/formulario-encabezado-final.jpg" width="20" height="37"></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="4" class="formulario-contenido-inicio"><img src="images/espacio.gif" width="1" height="1"></td>
        <td><form action="abm.php?tabla=cliente&columna=CliNro&idregistro=<?php print($m_lIDRegistro); ?>&url=<?php print($m_sURL); ?>&amp;archivo=1&amp;contador=0&amp;thumbs=1,2" method="post" name="frmRegistro" id="frmRegistro" onsubmit="corrigeFecha('fecha_alta');" enctype="multipart/form-data">
<input name="EmpNro" type="hidden" value="<?php print RetornarIdEmpresa(); ?>" />		
		
          <table width="100%" border="0" cellspacing="0" cellpadding="4">
            <tr>
              <td colspan="2" class="detalle-seccion">Generales</td>
            </tr>
            <tr>
              <td class="formulario-etiquetas">Fecha de alta:</td>
              <td class="formulario-textbox"><input name="CliFechaInicio" type="text" class="formulario-textbox" id="CliFechaInicio" style="width: 120px;" maxlenght="64" value="<?php print(date("d-m-Y", strtotime($aRegistro["CliFechaInicio"]))); ?>" />
                dd-mm-aaaa </td>
            </tr>
          <tr>
            <td colspan="2" class="detalle-seccion">cliente</td>
            </tr>
          <tr>
            <td class="formulario-etiquetas">Raz&oacute;n Social:</td>
            <td width="340"><input name="CliRsocial" type="text" class="formulario-textbox" id="CliRsocial" style="width: 300px;" maxlenght="64" value="<?php print($aRegistro["CliRsocial"]); ?>" /></td>
          </tr>
          <tr>
            <td class="formulario-etiquetas">Contacto:</td>
            <td width="340"><input name="CliContacto" type="text" class="formulario-textbox" id="CliContacto" style="width: 300px;" maxlenght="64" value="<?php print($aRegistro["CliContacto"]); ?>" /></td>
          </tr>
          <tr>
            <td class="formulario-etiquetas">Tel&eacute;fono:</td>
            <td><input name="CliTelefono" type="text" class="formulario-textbox" id="CliTelefono" style="width: 300px;" maxlenght="64" value="<?php print($aRegistro["CliTelefono"]); ?>" /></td>
          </tr>
          <tr>
            <td class="formulario-etiquetas">Email:</td>
            <td><input name="CliEMail" type="text" class="formulario-textbox" id="CliEMail" style="width: 300px;" maxlenght="64" value="<?php print($aRegistro["CliEMail"]); ?>" /></td>
          </tr>
          <tr>
            <td class="formulario-etiquetas">Domicilio:</td>
            <td><input name="CliDomicilio" type="text" class="formulario-textbox" id="CliDomicilio" style="width: 300px;" maxlenght="64" value="<?php print($aRegistro["CliDomicilio"]); ?>" /></td>
          </tr>
          <tr>
            <td class="formulario-etiquetas">CUIT:</td>
            <td><input name="CliCUIT" type="text" class="formulario-textbox" id="CliCUIT" style="width: 300px;" maxlenght="64" value="<?php print($aRegistro["CliCUIT"]); ?>" /></td>
          </tr>
          <tr>
            <td class="formulario-etiquetas">Rubro:</td>
            <td width="340"><input name="CliRubro" type="text" class="formulario-textbox" id="CliRubro" style="width: 300px;" maxlenght="64" value="<?php print($aRegistro["CliRubro"]); ?>" /></td>
          </tr>
		  
		            <tr>
            <td class="formulario-etiquetas">Imagen/Logo:</td>
            <td><input name="FILE_Imagen_1" type="file" class="formulario-textbox" id="FILE_Imagen_1" style="width: 300px;" /></td>
          </tr>
          <tr>
            <td class="formulario-etiquetas">Archivo de cliente (.xls):</td>
            <td><input name="FILE_Archivo_1;../clientes/" type="file" class="formulario-textbox" id="FILE_Archivo_1;../clientes/" style="width: 300px;" /></td>
          </tr>
          <tr>
            <td class="detalle-seccion">ejecutivo de cuenta </td>
            <td class="detalle-seccion"><select name="HIDDEN_CliEjCta" id="HIDDEN_CliEjCta" style="width: 300px;" onchange="cargaDatosEj(this);">
              <?php
					$sSQL = "SELECT MEmpNro, CONCAT(MEmpNombres, \" \", MEmpApellido) ";
					$sSQL .= "FROM miembroempresa WHERE MEmpAdmin = 2 ";
					print(GenerarOptions($sSQL, $aRegistro["CliEjCta"], true, "-Seleccione para cambiar-"));
			  ?>
            </select></td>
          </tr>
          <tr>
            <td class="formulario-etiquetas">Nombre:</td>
            <td><input name="CliEjCta" type="text" value="<?php print($aRegistro["CliEjCta"]); ?>" style="width: 300px;" /></td>
          </tr>
          <tr>
            <td class="formulario-etiquetas">Email:</td>
            <td>
			    
			<input name="CliEMailEjCta" type="text" class="formulario-textbox" id="CliEMailEjCta" style="width: 300px;" maxlenght="64" value="<?php print($aRegistro["CliEMailEjCta"]); ?>" readonly="true" /></td>
          </tr>
          <tr>
            <td height="30" class="formulario-etiquetas"><img src="images/espacio.gif" width="1" height="1"></td>
            <td align="center"><input name="BTN_Guardar" type="submit" id="BTN_Guardar" value="Guardar">
              <input name="BTN_Cancelar" type="reset" id="BTN_Cancelar" value="Cancelar" onclick="history.back();"></td>
          </tr>
        </table>
        </form></td>
        <td width="6" class="formulario-contenido-final"><img src="images/espacio.gif" width="1" height="1"></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="20"><img src="images/formulario-pie-inicio.jpg" width="20" height="40"></td>
        <td class="formulario-pie-bg"><img src="images/espacio.gif" width="1" height="1"></td>
        <td width="20"><img src="images/formulario-pie-final.jpg" width="20" height="40"></td>
      </tr>
    </table></td>
  </tr>
</table>
