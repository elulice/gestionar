<?php
if (!$_SESSION["usuario"]) {
    header("Location: index.php");
}
ini_set("display_errors", FALSE);
//ini_set("default_charset", "latin1");
ini_set("default_charset", "utf-8");
$getID = $_GET['id'];
require_once("clases/framework-1.0/class.bd.php");
require_once("clases/phppaging/PHPPaging.lib.php");

/**
 * zerofill()
 *
 * Returns a zerofilled integer up to a given character length
 *
 * @param   int $number
 * @param   int $length
 * @return  string zerofilled_number
 */
function zerofill($number, $length) {
    // Let's clean up to avoid any datatype issues
    $number = (int) $number;
    $length = (int) $length;

    $fill = '';
    /**
     * If the length of $number is greater or equal than $length
     * we return $number
     * else, we fill up with zeros
     * */
    if (strlen($number) < $length) {
        $fill = str_repeat('0', $length - strlen($number));
    }
    return $fill . $number;
}

//header("Content-Type: text/html; charset=UTF-8");

/*
  error_reporting(E_ALL);
  ini_set("display_errors", FALSE);
 */
//echo $_SESSION["strict_menu"] ;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
        <title>Gestion.Ar v1.0</title>
        <link rel="stylesheet" type="text/css" href="css/960.css" />
        <link rel="stylesheet" type="text/css" href="css/reset.css" />
        <link rel="stylesheet" type="text/css" href="css/text.css" />
        <link rel="stylesheet" type="text/css" href="css/blue.css" />
        <link rel="stylesheet" type="text/css" href="css/shadowbox.css" />
        <link rel="stylesheet" type="text/css" href="css/jquery-ui-1.8.10.custom.css" />
        <link rel='stylesheet' type='text/css' href='css/fullcalendar.css' />
        <link rel='stylesheet' type='text/css' href='css/redmond/ui.css' />

        <?php /*
          <script language="JavaScript" type="text/javascript" src="scripts/correctPNG.js"></script>
          <!--[if lt IE 7]>
          <script language="JavaScript">
          window.attachEvent("onload", correctPNG);
          </script>
          <![endif]-->
         */ ?>

        <script type="text/javascript" src="js/jquery-1.4.4.min.js"></script>
<!--        <script type="text/javascript" src="scripts/adminseg.js"></script>-->
        <script type="text/javascript" src="js/blend/jquery.blend.js"></script>
        <script type="text/javascript" src="scripts/jquery-ui-1.8.10.custom.js"></script>
        <script type="text/javascript" src="scripts/jquery.form.js"></script>
        <script type="text/javascript" src="scripts/sistema.js"></script>
        <script type="text/javascript" src="scripts/general.js"></script>
        <script type="text/javascript" src="scripts/instant.js"></script>
        <script type="text/javascript" src="scripts/ajax.php"></script>
        <script type='text/javascript' src='scripts/fullcalendar.js'></script>
        <script src="js/jquery-ui-timepicker-addon.js" type="text/javascript"></script>
        <script src="scripts/highchart/highcharts.js" type="text/javascript"></script>
        <script src="scripts/highchart/modules/exporting.js" type="text/javascript"></script>

        <script src="scripts/TicketsP.js" type="text/javascript"></script>
        <script src="scripts/links.desktop.js" type="text/javascript"></script>
        <script src="scripts/agenda.desktop.js" type="text/javascript"></script>

        <script type="text/javascript" src="js/effects.js"></script>
        <script language="JavaScript" type="text/javascript" src="scripts/swfobject.js"></script>
        <script language="JavaScript" type="text/javascript" src="scripts/correctPNG.js"></script>
        <link rel="stylesheet" type="text/css" media="all" href="css/jScrollPane.css" />


        <?php
        $include_section_script = "";
        if (isset($seccion)) {
            $include_section_script = "<script type=\"text/javascript\" src=\"scripts/$seccion.js\"></script>";
        }
        ?>

        <style type="text/css">
            /* css for timepicker */
            .ui-timepicker-div .ui-widget-header{ margin-bottom: 8px; }
            .ui-timepicker-div dl{ text-align: left; }
            .ui-timepicker-div dl dt{ height: 25px; }
            .ui-timepicker-div dl dd{ margin: -25px 0 10px 65px; }
            .ui-timepicker-div td { font-size: 90%; }
            .ui-dialog .ui-dialog-title .ui-icon {
                float: left;
                margin-right: 4px;
                margin-top:1px;}
            .ui-button-text-only .ui-button-text { padding: .2em 1em; font-size:11px; font-weight:normal;}
        </style>

        <script type="text/javascript" src="scripts/<?php echo $seccion; ?>.js"></script>

        <script type="text/javascript">
            $(document).ready(function () {
                iniciar();
                checkIfTicketIsClosed('<?php echo $getID; ?>');
                
            });

        </script>	

        <style>
            .ui-autocomplete {
                max-height: 150px;
                overflow-y: auto;
                /* prevent horizontal scrollbar */
                overflow-x: hidden;
                /* add padding to account for vertical scrollbar */
                padding-right: 20px;
            }
            /* IE 6 doesn't support max-height
             * we use height instead, but this forces the menu to always be this tall
            */
            * html .ui-autocomplete {
                height: 150px;
            }
        </style>
        <?php
        if ($seccion == "escritorio") {
            ?>

            <script type="text/javascript" src="scripts/pedidos.js"></script>
            <script type="text/javascript" src="scripts/curriculums.js"></script>

            <?php
        } elseif ($seccion == "agenda") {
            ?>
            <script type="text/javascript" src="scripts/curriculums.js"></script>
            <?php
        }
        ?>

        <style type="text/css">

            .portlet-header span img {
                margin:0 10px 0 5px;
            }

            .portlet-header span {
                cursor:default;
            }

            div.menu_escritorio {
                width:90px;
                height:23px;
                float:right;
                margin-right:20px;
                margin-top:-3px;
            }

            div.menu_escritorio a {
                background-image:url("images/menu/desktop_small.png");
                background-repeat:no-repeat;
                padding-left:30px;
                font-weight:bold;
                font-style:italic;
                line-height:23px;
                text-decoration:none;
                color:white;
                display:block;
            }

            div.menu_escritorio a:hover {
                text-decoration:underline;
            }

            a.custom_menu {
                background-repeat:no-repeat;
                padding-left:21px;
                cursor:pointer;
                float:right;
                margin-right:20px;
                font-size:10px;
                padding-top:2px;
            }



            div.dlg_column_left { float:left;width:360px; }
            div.dlg_column_right { float:left;margin-left:20px; }
            div.ddu_title { border-style:solid; border-width:0 0 1px 0; text-indent:5px; margin-bottom:10px;text-align:left; }
            div.ddu_title span { font-weight:bold;text-align:left; }
            div.ddu_campo { height:20px; margin:5px 0 0 5px; }
            div.ddu_campo_select { height:23px; margin:5px 0 0 5px;text-align:left; }
            div.ddu_campo_select select { width:253px;position:absolute; }
            div.ddu_campo_select span { width:100px; float:left;text-align:left; }
            div.ddu_campo_radio { height:22px; margin:10px 0 4px 5px; }
            div.ddu_campo_radio span { width:100px; float:left;margin-left:5px; }
            div.ddu_campo_radio input { width:10px; float:left; }
            div.ddu_campo_textarea { margin:5px 0 0 5px; }
            div.ddu_campo_textarea textarea { width:245px; }
            div.ddu_campo_textarea span { width:100px; float:left;text-align:left; }
            div.ddu_campo_file { height:25px; margin:10px 0 4px 5px; }
            div.ddu_campo_file span { width:100px; float:left;text-align:left; }
            div.ddu_campo_col_izq { float:left;width:190px; }
            div.ddu_campo_col_der { float:left;width:170px; }
            div.ddu_campo_col_der div.ddu_campo span { width:70px;margin-left:10px; }
            div.ddu_campo_col_der div.ddu_campo input { width:75px; }
            div.ddu_campo_col_izq div.ddu_campo input { width:75px; }
            div.ddu_campo_doble { width:360px; height:28px; }
            div.ddu_campo_comandos { width:360px;height:48px;margin-top:10px;padding-top:10px;text-align:right; }
            div.ddu_campo input { width:245px; }
            div.ddu_campo span { width:100px; float:left;text-align:left; }
            div.ddu_campo_g{ width:596px; height:69px; margin:0px 0 0 5px; }
            .ddu_campo_g1{ width:589px; height:68px; overflow:none; }
            .ddu_campo-grilla { height:367px; margin:5px 0 0 0px; }
            .texto-grillas{ height:16px; background-color:#efefef; color:#cc6600; font-weight:bold; float:left; padding:2px 0px 2px 2px; width:145px; border-right:1px solid #CCC; font-family:Arial, Helvetica, sans-serif; font-size:11px;}

            .texto-grilla-gris{ height:16px;  color:#555;  float:left; padding:2px 2px 2px 5px;width:90px; border-right:1px solid #CCC; border-left:1px solid #CCC; font-family:Arial, Helvetica, sans-serif; font-size:11px;}
            .fila{ float:left;  height:20px; border:1px solid #CCC; width:755px;}

            .columna-izq{
                float:left;
                width:374px;
                margin-top:10px;
            }



            /* seccion curriculums dialog asignacioes */
            fieldset { border:#eeeeee 1px groove;padding:10px 0 10px 0; }
            fieldset legend { padding:0 5px 0 5px; }
            form label { float:left;width:80px;margin-left:30px;height:40px; }
            form fieldset div.fila { width:100%;margin-bottom:5px;border-width:0!important; }
            form fieldset div.fila_comandos { text-align:right;margin:5px 22px 0 0; }
            form fieldset input { width:120px;float:left; }
            form fieldset select { width:128px;float:left; }

            /* seccion opciones */
            #accordion_opciones td.label{ width:130px;text-align:center; }
            #accordion_opciones td button{ width:130px; }

            .icons_new_tickets{position: absolute;margin-left: -18px; border: none; outline: none;}
            .int_fecha{background: url("images/icons/calendar.gif"); width: 16px; height: 16px;}
            .int_nombres{background: url("images/icons/user.gif"); width: 16px; height: 16px;}
            .int_clientes{background: url("images/icons/building.png"); width: 16px; height: 16px;}
            .int_origen{background: url("images/icons/origen.png"); width: 16px; height: 16px;}
            .int_sucursal{background: url("images/icons/sucursal.png"); width: 16px; height: 16px;}
            .int_responsable{background: url("images/icons/usermini.png"); width: 16px; height: 16px;}
            .int_estado{background: url("images/icons/status.png"); width: 16px; height: 16px;}
            .int_error{background: url("images/icons/error.png"); width: 16px; height: 16px;}
            .int_reason{background: url("images/icons/exclamation.gif"); width: 16px; height: 16px;}
            .int_mail{background: url("images/icons/mail.png"); width: 16px; height: 16px;}
            .int_mail_cliente{background: url("images/icons/mail_cliente.png"); width: 16px; height: 16px;}
            .int_ticket_nro{background: url("images/icons/tickets.png"); width: 16px; height: 16px;}

            .new_class_add_links{margin-left: 0px;margin-bottom: -25px;margin-top: 5px;}
        </style>

    </head>
    <body>
        <div id="wrapper" class="container_16">
            <div id="logo" class="grid_8">Gestion.Ar v1.0</div>
            <div class="grid_8" style="width: 490px;margin-left: -50px;">
                <div id="user_tools">
                    <span>Fecha: <?php echo date('d-m-Y'); ?> | Hora ingreso: <?php echo date('H:i:s'); ?>
                        <a><?php echo $_SESSION['persona']['apellido'] . ", " . $_SESSION['persona']['nombres']; ?></a>
                        <a href="logout.php" onclick="return confirm('&iquest;Desea salir del Sistema?');">Salir</a>
                    </span>
                </div>
            </div>
            <div id="header" class="grid_16">
                <?php require_once("inc.menu.php"); ?>
            </div>
            <?php require_once("inc.tabs.php"); ?>
            <div id="content" class="grid_16">
                <?php
                if (isset($titulo)) {
                    ?>
                    <div class="clear"></div>

                    <?php if ($titulo != "Reportes") { ?>
                        <div id="portlets">
                            <div class="grid_9">
                                <h1 class="<?php echo "{$seccion}_edit"; ?>">
                                    <? if ($tab == "edit"): ?>
                                        <?php echo $titulo . " " . zerofill($getID, 5) ?> 
                                    <? else: ?>
                                        <?php echo $titulo ?> 
                                    <?php endif; ?>
                                </h1>
                            </div>
                            <div class="clear"></div>


                            <div class="portlet2-header">
                                <span style="float:left;margin-left:10px;">
                                    <img width="16" height="16" alt="Titulo" src="images/glass.png" />
                                    <? if ($tab == "edit"): ?>
                                        <?php echo "Editar Ticket"; ?> 
                                    <? else: ?>
                                        <?php echo $titulo ?> 
                                    <?php endif; ?>
                                    <span class="respond_sended" id="response_sended">
                                        <script type="text/javascript">
                                            checkIfAnswerWasSentToClient('<?php echo $getID; ?>','check','response_sended');
                                        </script>
                                    </span>
                                    <?php if ($tab == "edit"):; ?>
                                        <span class="solicitante_header" id="solicitante_header">
                                            <script type="text/javascript">
                                                obtenerSolicitante('<?php echo $getID; ?>');
                                            </script>
                                        </span>
                                    <?php endif; ?>
                                </span>

                                <?php if ($seccion != "TicketsP" && $seccion != 'noticias' && $seccion != 'usuarios' && $seccion != 'opciones' && $seccion != 'usuarias') { ?>
                                    <span style="float:left;margin-left:10px;">
                                        <img width="16" height="16" alt="Titulo" src="images/glass.png" />
                                        Tickets
                                    </span>
                                <?php } ?>


                                <span style=" float:right; margin-right:15px;display:block; width:420px; margin-top:0px;">
                                    <div style="width:200px; float:left;">
                                    </div>
                                    <div style="margin-left: 200px">
                                        <?php if (!($_SESSION["is_agente"] || $_SESSION["is_productor"])): ?>

                                            <?php if (!empty($getID) && $seccion != 'TicketsP') { ?>
                                                <div style="float:right;margin-right: 40px; ">
                                                    <!--- Si es OPERADOR O ADMINISTRADOR --->
                                                    <?php if ($_SESSION["tipo_usuario"] == 6 || $_SESSION["tipo_usuario"] == 1): ?>
                                                        <a class="edit_inline"  href="javascript:void(0);" onclick="generateNewTicket();">   Agregar Nuevo</a>
                                                    <?php endif; ?>
                                                    <a id="a_polizas_delete" href="javascript:void(0);" onclick="deleteTicket('<? echo $getID ?>')" class="delete_inline delete" style="margin-left:10px;">  Eliminar</a>
                                                </div>
                                                <?
                                            }
                                            if ($seccion == 'TicketsP') {
                                                ?>

                                                <div id="showOnlyIfAnswerIsSentToClient" style=" float:right;margin-right:165px;width:400px;font-weight: bold;margin-bottom:-20px;"></div>
                                                <div class="boton-nuevo">
                                                    <?php if ($_SESSION["tipo_usuario"] == 6 || $_SESSION["tipo_usuario"] == 1): ?>
                                                        <a class="edit_inline"  href="javascript:void(0);" onclick="generateNewTicket();">   Agregar Nuevo</a>
                                                    <?php endif; ?>
                                                    <?php if ($tab == "edit"): ?>
                                                        <?php if ($_SESSION["tipo_usuario"] == 1): ?>
                                                            <a class="close_inline" id="cerrar_ticket" href="javascript:void(0);" onclick="closeTicket('<?php echo $getID ?>')" >   Cerrar Ticket</a>
                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                                </div>
                                                <?
                                            }
                                            ?>
                                        <?php endif; ?>
                                    </div>
                                </span>

                                <span style="float:right;margin-right:30px;">
                                    <?php
                                    $evento = "";
                                    $nombre_comando = "";

                                    if (isset($comando)) {

                                        $evento = " onclick='{$comando["onclick"]}' ";
                                        $nombre_comando = $comando["titulo"];
                                    }

                                    if (!isset($nombre_comando))
                                        $nombre_comando = "Comando de Sección";
                                    $evento_comando = "";
                                    if (isset($evento_comando))
                                        $evento_comando = " onclick='$evento_comando' ";
                                    ?>
                                    <a class="edit_inline" style="margin-left:10px;" href="#" <?php echo $evento; ?>><?php echo $nombre_comando; ?></a>
                                </span>
                                <div style="clear:both;"></div>
                            </div>
                        <?php } ?>

                        <div class="grid_15">
                            <div class="portlet-content" <?php if ($titulo == "Reportes") { ?> style="border-top: none;" <?php } ?>>
                                <?php
                            }

                            $m_lIDRegistro = is_numeric($_REQUEST["idregistro"]) ? $_REQUEST["idregistro"] : 0;
                            $m_lIDSeccion = is_numeric($_REQUEST["idseccion"]) ? $_REQUEST["idseccion"] : 1;
                            $m_lIDPostulante = is_numeric($_REQUEST["idpostulante"]) ? $_REQUEST["idpostulante"] : 0;

                            $m_sURL = $_SERVER["REQUEST_URI"];

                            $aMenues[0][0] = "usuarios";
                            $aMenues[1][0] = "clientes";
                            $aMenues[2][0] = "postulantes";
                            $aMenues[3][0] = "noticias";
                            $aMenues[4][0] = "agenda";
                            $aMenues[5][0] = "trayectoria";
                            $aMenues[6][0] = "ofertas";
                            $aMenues[7][0] = "curriculums";
                            $aMenues[8][0] = "*";

                            for ($lJ = 0; $lJ < count($aMenues); $lJ++) {
                                if ($aMenues[$lJ][0] == "*") {
                                    $aMenues[$lJ][1] = true;
                                    break;
                                } else {
                                    if (strpos(basename($m_sURL), $aMenues[$lJ][0]) > 0) {
                                        $aMenues[$lJ][1] = true;
                                        break;
                                    }
                                }
                            }
                            ?>

                            <script type="text/javascript">

 

                                var _gaq = _gaq || [];

                                _gaq.push(['_setAccount', 'UA-27205757-8']);

                                _gaq.push(['_trackPageview']);

 

                                (function() {

                                    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;

                                    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';

                                    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);

                                })();

 

                            </script>