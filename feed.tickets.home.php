<?php
require_once("clases/framework-1.0/class.bd.php");
require_once("clases/phppaging/PHPPaging.lib.php");
require_once("includes/funciones.php");

$queryZz = "SELECT all_tickets.*, estado_tickets.nombre as eNom, miembroempresa.MEmpNro as rID, miembroempresa.MEmpNombres as rNombre, miembroempresa.MEmpApellido as rApellido, cliente.CliRsocial as razon_social FROM all_tickets
                                LEFT JOIN miembroempresa ON all_tickets.responsable_id = miembroempresa.MEmpNro
                                LEFT JOIN estado_tickets ON all_tickets.estado_id = estado_tickets.id 
                                LEFT JOIN cliente ON all_tickets.cliente_id = cliente.CliNro WHERE all_tickets.visible = 'S' ORDER BY fecha DESC ";
$cBD = new BD();
$cBD->Conectar();

$paging = new PHPPaging($cBD->RetornarConexion());
$paging->porPagina(4);
$paging->linkClase("navPage");
$paging->linkSeparador(" - ");
$paging->agregarConsulta($queryZz);
$paging->ejecutar();

while ($ZzRegistro = $paging->fetchResultado()) {
    $id = $ZzRegistro['id'];
    $date = $ZzRegistro['fecha'];
    $date = explode('-', $date);
    $date = $date[2] . "/" . $date[1] . "/" . $date[0];
    $rNombre = htmlentities($ZzRegistro['rNombre'] . $ZzRegistro['rApellido']);
    $rNombre = myTruncate($rNombre, 15, ' ', ' ...');
    $rSocial = htmlentities($ZzRegistro['razon_social']);
    $rSocial = myTruncate($rSocial, 20, ' ', ' ...');
    $estado = $ZzRegistro["eNom"];
    ?>

    <li style="border-bottom: 1px solid #ccc;margin: 3px 0 3px 0">

        <h5><?php echo $date; ?></h5>
        <h6 style="width: 450px ">
            <a href="javascript:void(0)" class="textohome" onclick="editar_ticket(<? echo $id ?>);" >
                <?php // echo $rNombre . " / " . $rSocial;  ?>
                <?php echo $rSocial . " / " . $estado; ?>
            </a>
            <span style="width: 60px; float: right;margin-top: -5px;">
                <a title="Ver" class="verEvento" href="javascript:void(0)" onclick="editar_ticket(<? echo $id ?>);">
                    <img src="images/icons/zoom_in.png" width="16" height="16">
                </a>
                <?php if ($_SESSION["tipo_usuario"] == 1): ?>
                    <a id="a_lala" href="javascript:void(0)" class="delete" onclick="deleteTicket(<? echo $id ?>,'desktop')">
                        <img src="images/icons/page_delete.png">
                    </a>
                <?php endif; ?>
            </span>
        </h6>
    </li>

    <?php
}
?>
<div class="pagination">
    <?php echo $paging->fetchNavegacion(); ?>
</div>
