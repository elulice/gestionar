<?php

/*
  $n = 12345.67891011;
  setlocale(LC_MONETARY, "en_US");
  echo money_format("%i", $n);
  exit();
 */

error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', TRUE);

/*
  error_reporting(E_ALL);
  ini_set('display_errors', TRUE);
 */

ini_set("expect.timeout", 1000);
ini_set("memory_limit", "70M");
ini_set('upload_max_filesize', '150M');
ini_set('max_execution_time', 3600);
ini_set('post_max_size', '150M');
ini_set('include_path', 'clases/');

ob_start();

//require_once("pdf/dompdf_config.inc.php");
include("includes/funciones.php");

include("clases/framework-1.0/class.bd.php");
require_once ("clases/phppaging/PHPPaging.lib.php");
require_once ("clases/pdf/html2pdf.class.php");


if (isset($_GET['objeto'])) {

    switch ($_GET['objeto']) {

        case "excel":

            header("Content-type: application/vnd.ms-excel; name='excel'");
            header("Content-Disposition: filename=reporte_temporal.xls");
            header("Pragma: no-cache");
            header("Expires: 0");

            $vocales = array("á", "é", "í", "ó", "ú", "ñ", "Á", "É", "Í", "Ó", "Ú", "Ñ");
            $reemplazar = array("&aacute;", "&eacute;", "&iacute;", "&oacute;", "&uacute;", "&ntilde;", "&Aacute;", "&Eacute;", "&Iacute;", "&Oacute;", "&Uacute;", "&Ntilde;");

            $post = str_replace($vocales, $reemplazar, $_POST['export_excel']);
            echo $post;

            break;

        case "pdf":

            $vocales = array("á", "é", "í", "ó", "ú", "ñ", "Á", "É", "Í", "Ó", "Ú", "Ñ");
            $reemplazar = array("&aacute;", "&eacute;", "&iacute;", "&oacute;", "&uacute;", "&ntilde;", "&Aacute;", "&Eacute;", "&Iacute;", "&Oacute;", "&Uacute;", "&Ntilde;");

            $post = str_replace($vocales, $reemplazar, $_POST['export_pdf']);

            $html2pdf = new HTML2PDF('P', 'A4', 'es');
            $html2pdf->WriteHTML($post);
            $html2pdf->Output('reporte_temporal.pdf');

            break;
    }
}
?>