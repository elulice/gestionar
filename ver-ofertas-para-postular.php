<?php
	session_start();
	include("clases/framework-1.0/class.bd.php");
	include("includes/funciones.php");
?>

<?php

	$m_lIDRegistro = is_numeric($_REQUEST["idregistro"]) ? $_REQUEST["idregistro"] : 0;

	if($m_lIDRegistro > 0)
	{
		$sSQL = "SELECT p.apellido, p.nombres ";
		$sSQL .= "FROM postulantes p ";
		$sSQL .= "WHERE p.idpostulante = " . $m_lIDRegistro;
		$cBD = new BD();
		$aRegistro = $cBD->Seleccionar($sSQL, true);
	}
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?php print SITIO; ?></title>
<script language="JavaScript" type="text/javascript" src="scripts/ajax.php"></script>
<script language="JavaScript" type="text/javascript" src="scripts/general.js"></script>

<link href="estilos/general.css" rel="stylesheet" type="text/css">
</head>

<body>

<table width="760" border="0" cellspacing="0" cellpadding="0" align="center" class="panel">
            <tr>
              <td width="415%" class="listado-texto"><h3><?php print ReemplazarCaracteres($aRegistro["apellido"].", ".$aRegistro["nombres"]); ?></h3></td>
  </tr>
            <tr>
              <td class="formulario-textbox">Seleccione mediante los filtros la oferta laboral de su inter&eacute;s</td>
  </tr>
        </table>

<table border="0" align="center" cellpadding="0" cellspacing="0" style="margin-bottom:10px;" class="buscar">
<tr><td>
			<form action="?" method="POST" name="frmFiltro" id="frmFiltro">
		<input name="filtrar" type="hidden" value="1" />
			<?php
				$bCliente = ($_POST["cliente"] ? $_POST["cliente"] : "");
				$bPuesto = ($_POST["puesto"] ? $_POST["puesto"] : "");
				$bFechaDesde = ($_POST["fecha_desde"] ? $_POST["fecha_desde"] : "");
				$bFechaHasta = ($_POST["fecha_hasta"] ? $_POST["fecha_hasta"] : date("d-m-Y"));
				$bArea = (is_numeric($_POST["idarea"]) ? $_POST["idarea"] : 0);
				$bPais = (is_numeric($_POST["idpais"]) ? $_POST["idpais"] : 0);
				$bProvincia = (is_numeric($_POST["idprovincia"]) ? $_POST["idprovincia"] : 0);
				$bLocalidad = (is_numeric($_POST["idlocalidad"]) ? $_POST["idlocalidad"] : 0);
			?>
			
			
<table border="0" cellspacing="0" cellpadding="0" style="margin:5px auto;">
  <tr>
    <td width="100" class="encabezado-formulario">Usuaria:</td>
    <td width="150"><input name="cliente" type="text" id="cliente" style="width: 140px;" value="<?php print $bCliente; ?>" /></td>
    <td width="100" class="encabezado-formulario">&Aacute;rea:</td>
    <td width="210" ><select name="idarea" id="idarea" style="width: 200px;">
      <?php
					$sSQL = "SELECT idarea, nombre FROM areas  ";
					$sSQL .= "ORDER BY nombre ASC ";
					print(GenerarOptions($sSQL, $bArea, true, DEFSELECT));
			  ?>
    </select></td>
    <td width="80" rowspan="4" align="right" valign="bottom"><input name="btnFiltrar" type="image" id="btnFiltrar" src="images/btn-buscar.jpg" alt="Filtrar" /></td>
  </tr>
  <tr>
    <td width="100" class="encabezado-formulario">Puesto:</td>
    <td width="140" ><input name="puesto" type="text" id="puesto" style="width: 140px;" value="<?php print $bPuesto; ?>" /></td>
    <td width="100" class="encabezado-formulario">Pa&iacute;s: </td>
    <td width="200" ><select name="idpais" id="idpais" style="width: 200px;" onChange="cargarListas(this, 'idprovincia')">
      <?php
					$sSQL = "SELECT idpais, nombre FROM paises  ";
					$sSQL .= "ORDER BY nombre ASC ";
					print(GenerarOptions($sSQL, $bPais, true, DEFSELECT));
			  ?>
    </select></td>
    </tr>
  <tr>
    <td class="encabezado-formulario">Fecha desde: </td>
    <td><input name="fecha_desde" type="text" id="fecha_desde" style="width: 140px;" value="<?php print $bFechaDesde; ?>" /></td>
    <td class="encabezado-formulario">Provincia:</td>
    <td><select name="idprovincia" id="idprovincia" style="width: 200px;" onChange="cargarListas(this, 'idlocalidad')">
      <?php
					$sSQL = "SELECT idprovincia, nombre FROM provincias  ";
					$sSQL .= "ORDER BY nombre ASC ";
					print(GenerarOptions($sSQL, $bProvincia, true, DEFSELECT));
			  ?>
    </select></td>
    </tr>
  <tr>
    <td class="encabezado-formulario">Hasta:</td>
    <td><input name="fecha_hasta" type="text" id="fecha_hasta" style="width: 140px;" value="<?php print $bFechaHasta; ?>" /></td>
    <td class="encabezado-formulario">Localidad:</td>
    <td><select name="idlocalidad" id="idlocalidad" style="width: 200px;">
      <?php
					$sSQL = "SELECT idlocalidad, nombre FROM localidades  ";
					$sSQL .= "ORDER BY nombre ASC ";
					print(GenerarOptions($sSQL, $bLocalidad, true, DEFSELECT));
			  ?>
    </select></td>
    </tr>
</table>
	        </form>
</td></tr>
</table>





<?php 
  if ($_GET["filtrar"])
  {
?>


<table width="780" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="20"><img src="images/listado-encabezado-inicio.jpg" width="20" height="37"></td>
        <td class="listado-encabezado-bg"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="70"><img src="images/espacio.gif" width="1" height="1"></td>
            <td width="75" class="listado-encabezado-texto">Fecha</td>
            <td width="150" class="listado-encabezado-texto">Cliente</td>
            <td width="220" class="listado-encabezado-texto">Puesto</td>
            <td width="180" class="listado-encabezado-texto">&Aacute;rea</td>
            <td class="listado-encabezado-texto">Estado</td>
          </tr>
        </table></td>
        <td width="20"><img src="images/listado-encabezado-final.jpg" width="20" height="37"></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="5" class="listado-contenido-inicio"><img src="images/espacio.gif" width="1" height="1"></td>
        <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
		  <?php
		  		$lRegistros = 0;
			 	$sSQL = "SELECT p.idpropuesta, p.puesto, p.fecha_alta, oe.nombre AS estado, ";
				$sSQL .= "a.nombre AS area, e.razon_social AS empresa  ";
				$sSQL .= "FROM propuestas p ";
				$sSQL .= "INNER JOIN areas a ON p.idarea = a.idarea ";
				$sSQL .= "INNER JOIN empresas e ON p.idempresa = e. idempresa ";
				$sSQL .= "INNER JOIN ofertas_estados oe ON p.estado = oe.idestado ";
				$sSQL .= "WHERE 1 ";

				if ($bCliente <> "")
					$sSQL .= "AND e.razon_social LIKE '%". $bCliente ."%' ";
				if ($bPuesto <> "")
					$sSQL .= "AND p.puesto LIKE '%". $bPuesto ."%' ";
				if ($bFechaDesde <> "")
				{				
					$bFechaDesde = explode("-", $bFechaDesde);
					$sFecha = $bFechaDesde[2]."-".$bFechaDesde[1]."-".$bFechaDesde[0];
					$sSQL .= "AND p.fecha_alta >= '". date("Y-m-d", strtotime($sFecha)) ."' ";
				}
				if ($bFechaHasta <> "")
				{
					$bFechaHasta = explode("-", $bFechaHasta);
					$sFecha = $bFechaHasta[2]."-".$bFechaHasta[1]."-".$bFechaHasta[0];
					$sSQL .= "AND p.fecha_alta <= '". date("Y-m-d", strtotime($sFecha)) ."' ";
				}
				if ($bArea > 0)
					$sSQL .= "AND p.idarea = ". $bArea ." ";
				if ($bPais > 0)
					$sSQL .= "AND p.idpais = ". $bPais ." ";
				if ($bProvincia > 0)
					$sSQL .= "AND p.idprovincia = ". $bProvincia ." ";
				if ($bLocalidad > 0)
					$sSQL .= "AND p.idlocalidad = ". $bLocalidad ." ";

				
				$sSQL .= "ORDER BY fecha_alta DESC, idpropuesta DESC ";
							
				//print $sSQL;
					
				$cBD = new BD();
				$oResultado = $cBD->Seleccionar($sSQL);
				while($aRegistro = $cBD->RetornarFila($oResultado))
				{
					$sPosicion = (($sPosicion == "1") ? "2" : "1");
		  ?>


          <tr>
            <td class="listado-fila-bg-<?php print($sPosicion); ?>"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="15"><img src="images/espacio.gif" width="1" height="1"></td>
                <td width="70"><table width="70" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="30"><a href="javascript:verPostulados(<?php print($aRegistro["idpropuesta"]); ?>, <?php print($sPosicion); ?>)"><img src="images/btn-ver-mas-<?php print($sPosicion); ?>.jpg" alt="Ver Postulados" width="24" height="23" border="0"></a></td>

                    <td width="30"><a href="am-ofertas.php?idregistro=<?php print($aRegistro["idpropuesta"]); ?>&url=<?php print($m_sURL); ?>"><img src="images/btn-modificar-<?php print($sPosicion); ?>.jpg" alt="Modificar" width="24" height="23" border="0"></a></td>
                    <td><a href="abm.php?tabla=propuestas&columna=idpropuesta&idregistro=<?php print($aRegistro["idpropuesta"]); ?>&url=<?php print($m_sURL); ?>" onClick="return confirm('&iquest;Desea eliminar esta Ofertas?')"><img src="images/btn-eliminar-<?php print($sPosicion); ?>.jpg" alt="Eliminar" width="24" height="23" border="0"></a></td>
                  </tr>
                </table></td>
                <td width="75" class="listado-texto"><?php print(date("d/m/Y", strtotime($aRegistro["fecha_alta"]))); ?></td>
                <td width="150" class="listado-texto"><?php print($aRegistro["empresa"]); ?></td>
                <td width="220" class="listado-texto"><?php print($aRegistro["puesto"]); ?></td>
                <td width="180" class="listado-texto"><?php print($aRegistro["area"]); ?></td>
                <td width="55" class="listado-texto"><?php print($aRegistro["estado"]); ?></td>
              </tr>

              <tr id="trOferta-<?php print($aRegistro["idpropuesta"]); ?>" class="listado-fila-oculta">
                <td colspan="7" id="tdOferta-<?php print($aRegistro["idpropuesta"]); ?>" class="informe-separador" style="padding: 5px 0;">&nbsp;</td>
              </tr>
		  
            </table></td>
          </tr>


          <?php
			 		$lRegistros++;
				}
				if($lRegistros == 0)
				{
			 ?>
          <tr>
            <td><img src="images/espacio.gif" width="1" height="20"></td>
          </tr>
			 <?php } ?>
        </table></td>
        <td width="6" class="listado-contenido-final"><img src="images/espacio.gif" width="1" height="1"></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="100"><a href="am-ofertas.php?idregistro=0&url=<?php print($m_sURL); ?>"><img src="images/listado-pie-inicio.jpg" alt="Agregar" width="100" height="40" border="0"></a></td>
        <td class="listado-pie-bg">&nbsp;</td>
        <td width="20"><img src="images/listado-pie-final.jpg" width="20" height="40"></td>
      </tr>
    </table></td>
  </tr>
</table>
<?php 			}
?>





</body>
</html>
