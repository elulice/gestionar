<div id="menu">
    <ul class="group" id="menu_group_main">
        <li class="item first" id="three">
            <a href="escritorio.php" class="main <?php echo ($seccion == "escritorio") ? "current" : ""; ?>"><span class="outer"><span class="inner dashboard png">Escritorio</span></span></a></li>
        <li class="item middle" id="one">
            <a href="usuarias.php" class="main <?php echo ($seccion == "usuarias" ) ? "current" : ""; ?>"><span class="outer"><span class="inner usuarias">Usuarias</span></span></a></li>
        <li class="item middle" id="four">
            <a href="pedidos.php" class="main <?php echo ($seccion == "pedidos") ? "current" : ""; ?>"><span class="outer"><span class="inner pedidos">Pedidos</span></span></a></li>
        <li class="item middle" id="two">
            <a href="agenda.php" class="main <?php echo ($seccion == "agenda") ? "current" : ""; ?>"><span class="outer"><span class="inner agenda">Agenda</span></span></a></li>
        <li class="item middle" id="five">
            <a href="curriculums.php" class="main <?php echo ($seccion == "curriculums") ? "current" : ""; ?>"><span class="outer"><span class="inner curriculums">Administrar CV</span></span></a></li>
        <!--li class="item middle" id="three">
            <a href="trayectoria.php" class="main <?php echo ($seccion == "trayectoria") ? "current" : ""; ?>"><span class="outer"><span class="inner trayectoria png">Trayectoria</span></span></a></li-->

        <li class="item middle" id="seven">
            <a href="<?php echo ($_SESSION["strict_menu"]) ? "reclamos.php" : "#"; ?>" class="main <?php echo ($seccion == "reclamos") ? "current" : ""; ?>"><span class="outer"><span class="inner reclamos">Reclamos</span></span></a></li>
        <li class="item middle" id="six">
            <a href="noticias.php" class="main <?php echo ($seccion == "noticias") ? "current" : ""; ?>"><span class="outer"><span class="inner noticias">Noticias</span></span></a></li>
        <li class="item last" id="eight">
            <a href="<?php echo ($_SESSION["strict_menu"]) ? "usuarios.php" : "#"; ?>" class="main <?php echo ($seccion == "usuarios") ? "current" : ""; ?>"><span class="outer"><span class="inner usuarios">Usuarios</span></span></a></li>
        <!--<li class="item middle" id="seven"><a><span class="outer"><span class="inner newsletter">Comisiones</span></span></a></li>        -->
    </ul>
</div>
