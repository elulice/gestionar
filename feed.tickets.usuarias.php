<?php
/*
  error_reporting(E_ALL);
  ini_set("display_errors", TRUE);
 */

require_once("clases/framework-1.0/class.bd.php");
require_once("clases/phppaging/PHPPaging.lib.php");
require_once("includes/funciones.php");

function compararFechas($primera, $segunda)
{
    $valoresPrimera = explode("/", $primera);
    $valoresSegunda = explode("/", $segunda);

    $diaPrimera = $valoresPrimera[0];
    $mesPrimera = $valoresPrimera[1];
    $anyoPrimera = $valoresPrimera[2];

    $diaSegunda = $valoresSegunda[0];
    $mesSegunda = $valoresSegunda[1];
    $anyoSegunda = $valoresSegunda[2];

    $diasPrimeraJuliano = gregoriantojd($mesPrimera, $diaPrimera, $anyoPrimera);
    $diasSegundaJuliano = gregoriantojd($mesSegunda, $diaSegunda, $anyoSegunda);

    if (!checkdate($mesPrimera, $diaPrimera, $anyoPrimera)) {
        // "La fecha ".$primera." no es v&aacute;lida";
        return 0;
    } elseif (!checkdate($mesSegunda, $diaSegunda, $anyoSegunda)) {
        // "La fecha ".$segunda." no es v&aacute;lida";
        return 0;
    } else {
        return $diasPrimeraJuliano - $diasSegundaJuliano;
    }
}
?>
<table cellspacing="0" cellpadding="0" width="840" id="box-table-a" style="margin:12px 0 0 12px;">
    <thead>
        <tr>
            <th width="80" scope="col"><span style="color:#c60;font-weight:bold;">Fecha</span></th>
            <th width="60" scope="col"><span style="color:#c60;font-weight:bold;">Nro</span></th>
            <th width="100" scope="col"><span style="color:#c60;font-weight:bold;">Nombre</span></th>
            <th width="100" scope="col"><span style="color:#c60;font-weight:bold;">Usuarias</span></th>
            <th width="100" scope="col"><span style="color:#c60;font-weight:bold;">Origen</span></th>
            <th width="100" scope="col"><span style="color:#c60;font-weight:bold;">Sucursal</span></th>
            <th width="100" scope="col"><span style="color:#c60;font-weight:bold;">Responsable</span></th>
            <th width="100" scope="col"><span style="color:#c60;font-weight:bold;">Estado</span></th>
            <th width="100" scope="col"><span style="color:#c60;font-weight:bold;">Creado Por</span></th>
<!--            <th width="15" scope="col"><span style="color:#c60;font-weight:bold;">Editado</span></th>
            <th width="15" scope="col"><span style="color:#c60;font-weight:bold;">Resp.</span></th>-->
            <th width="30" scope="col"><span style="color:#c60;font-weight:bold;">Opci&oacute;n</span></th>
        </tr>
    </thead>
    <tbody>
        <?
        $bnombre = $_GET["tnombre"];
        $bsucursal = $_GET["tsucursal"];
        $bestado = $_GET["testado"];
        $bresponsable = $_GET["tresponsable"];

        $bfecha = $_GET["tfecha"];
        if ($bfecha != "") {
            $bfecha = explode('/', $bfecha);
            $bfecha = $bfecha[2] . "-" . $bfecha[1] . "-" . $bfecha[0];
        }

        $busuarias = $_GET["tusuarias"];
        $bNroReclamo = (int) $_GET["tnroreclamo"];



        $queryZz = "SELECT all_tickets.*, all_tickets.response_sended_to_client as envio_respuesta, cliente.CliRsocial as razon_social,
            origen_tickets.nombre_origen as donde_origen, unidadorg.UniNombre as donde_sucursal, miembroempresa.* ,
            estado_tickets.nombre as state_tickets, respuesta_tickets.respuesta_fecha as resp_fecha, respuesta_tickets.respuesta_hora as resp_hora,
            responded_to_client.rtc_fecha as fecha_cierre, responded_to_client.rtc_hora as hora_cierre
                                FROM all_tickets
                                LEFT JOIN cliente ON all_tickets.cliente_id = cliente.CliNro
                                LEFT JOIN origen_tickets ON all_tickets.origen_id = origen_tickets.id
                                LEFT JOIN unidadorg ON all_tickets.sucursal_id = unidadorg.UniNro
                                LEFT JOIN miembroempresa ON all_tickets.creado_por_id = miembroempresa.MEmpNro
                                LEFT JOIN estado_tickets ON all_tickets.estado_id = estado_tickets.id 
                                LEFT JOIN respuesta_tickets ON all_tickets.id = respuesta_tickets.ticket_id 
                                LEFT JOIN responded_to_client ON all_tickets.id = responded_to_client.rtc_id_ticket 
                                WHERE all_tickets.visible = 'S' ";
        if ($_SESSION["tipo_usuario"] == 0) {
            if (isset($_SESSION["empresa_id"]) && !empty($_SESSION["empresa_id"])) {
                $queryZz .= " AND sucursal_id = " . $_SESSION["empresa_id"];
            }
            if (isset($_SESSION["PerNro"]) && !empty($_SESSION["PerNro"])) {
                $queryZz .= " OR responsable_id = " . $_SESSION["PerNro"];
            }
        }

        if ($bnombre != "") {
            $queryZz .= " AND tickets_nombre LIKE '%" . $bnombre . "%' ";
        }

        if ($bsucursal != "0" && $bsucursal != "") {
            $queryZz .= " AND sucursal_id = $bsucursal ";
        }

        if ($bestado != "0" && $bestado != "4" && $bestado != "5" && $bestado != "6" && $bestado != "") {
            $queryZz .= " AND estado_id = $bestado ";
        }

        if ($bestado != "0" && $bestado == "4") {
            $queryZz .= " AND responsed = 'S' ";
        }

        if ($bestado != "0" && $bestado == "5") {
            $queryZz .= " AND responsed = 'N' ";
        }

        if ($bestado != "0" && $bestado == "6") {
            $queryZz .= " AND estado_id != 3 ";
        }

        if ($bresponsable != "0" && $bresponsable != "") {
            $queryZz .= " AND responsable_id = $bresponsable ";
        }

        if ($bfecha != "") {
            $queryZz .= " AND fecha = '$bfecha' ";
        }

        if ($busuarias != "") {
            $queryZz .= " AND CliRsocial LIKE '%" . $busuarias . "%' ";
        }

        if ($bNroReclamo != "") {
            $queryZz .= " AND all_tickets.id = '$bNroReclamo' ";
        }

        $queryZz .= " ORDER BY id DESC ";

        $cBD = new BD();
        $cBD->Conectar();

        $paging = new PHPPaging($cBD->RetornarConexion());
        $paging->agregarConsulta($queryZz);
        $paging->linkClase("navPage");
        $paging->porPagina(4);
//        $paging->linkSeparador(" - ");
        $paging->ejecutar();
        while ($ZzRegistro = $paging->fetchResultado()) {
            $resp_id = $ZzRegistro['responsable_id'];

            $newQuery = "SELECT MEmpNombres, MEmpApellido FROM miembroempresa WHERE PerNro = $resp_id ORDER BY MEmpApellido";
            $cBD = new BD();
            $oRegistro = $cBD->Seleccionar($newQuery);
            while ($xRegistro = $cBD->RetornarFila($oRegistro)) {
                $responsable = $xRegistro['MEmpApellido'] . " " . $xRegistro['MEmpNombres'];
                $responsable = myTruncate($responsable, 8, ' ', ' ...');
            }

            $fechaAllTk = $ZzRegistro['fecha'];
            $fechaAllTk = str_replace('-', '/', $fechaAllTk);
            $fechaAllTk = explode('/', $fechaAllTk);
            $fechaAllTk = $fechaAllTk[2] . '/' . $fechaAllTk[1] . '/' . $fechaAllTk[0];
            $idDel = $ZzRegistro['id'];
            $rSocial = $ZzRegistro['razon_social'];

            $rSocialTrunkate = myTruncate($rSocial, 8, ' ', ' ...');
            $replaceVocals = array('á', 'é', 'í', 'ó', 'ú');
            $replaceVocalsFor = array('&aacute;', '&eacute;', '&iacute;', '&oacute;', '&uacute;');

            $rEmpName = str_replace($replaceVocals, $replaceVocalsFor, $rEmpName);
            $ZzRegistro['donde_sucursal'] = str_replace($replaceVocals, $replaceVocalsFor, utf8_encode($ZzRegistro['donde_sucursal']));



            $cNombre = $ZzRegistro['MEmpApellido'] . " " . $ZzRegistro['MEmpNombres'];
            $cNombre = myTruncate($cNombre, 12, ' ', ' ...');

            $ZzRegistro['tickets_nombre'] = myTruncate($ZzRegistro['tickets_nombre'], 12, ' ', '...');
            $comparar = compararFechas($fechaAllTk, date('d/m/Y'));
            //var_dump($ZzRegistro); die;
            // Si la respuesta se envio al solicitante
            if ($ZzRegistro["envio_respuesta"] == "S") {
                $styleTickets = "class='blue_ticket'";

                // Si el ticket esta respondido
            } else if ($ZzRegistro["responsed"] == "S") {
                $styleTickets = "class='bold_ticket'";

                // Si pasaron 24hs sin respuesta
            } else if ($comparar == -1) {
                $styleTickets = "class='orange_ticket'";

                // Si pasaron 48hs sin respuesta
            } else if ($comparar <= -1) {
                $styleTickets = "class='red_ticket'";
            }
            ?>
            <tr <?php echo $styleTickets; ?>>
                <td <?php echo $styleTickets; ?> style = "padding:8px;"><?php echo $fechaAllTk; ?></td>
                <td <?php echo $styleTickets; ?> style = "padding:8px;"><?php echo "#" . $ZzRegistro["id"]; ?></td>
                <td <?php echo $styleTickets; ?> style = "padding:8px;"><?php echo html_entity_decode(utf8_decode($ZzRegistro['tickets_nombre'])); ?></td>
                <td <?php echo $styleTickets; ?> style = "padding:8px;"><?php echo $rSocialTrunkate; ?></td>
                <td <?php echo $styleTickets; ?> style = "padding:8px;"><?php echo $ZzRegistro['donde_origen']; ?></td>
                <td <?php echo $styleTickets; ?> style = "padding:8px;"><?php echo $ZzRegistro['donde_sucursal']; ?></td>
                <td <?php echo $styleTickets; ?> style = "padding:8px; display: none;"><?php //echo ucwords(strtolower($ZzRegistro['responsable']))                                                               ?></td>
                <td <?php echo $styleTickets; ?> style = "padding:8px;"><? echo $responsable; ?></td>
                <td <?php echo $styleTickets; ?> style = "padding:8px;"><?php echo $ZzRegistro['state_tickets']; ?></td>
                <td <?php echo $styleTickets; ?> style = "padding:8px;"><?php echo $cNombre; ?></td>
                <td style = "padding:8px;">
                    <img style = "cursor:pointer;position: absolute;" onclick = "editar_ticket(<? echo $ZzRegistro['id'] ?>);" title = "Ver/Editar" src = "images/icons/zoom_in.png" />
                    <?php if ($_SESSION["tipo_usuario"] == 1): ?>
                        <a id="a_polizas_delete" class="delete_inline delete" href="javascript:void(0)" onclick="deleteTicket(<? echo $idDel ?>)" style="width: 16px;height: 16px;margin-left: 20px;cursor: pointer" title="Eliminar"></a>
                    <?php endif; ?>
                </td>
            </tr>
            <?
        }
        ?>
    </tbody>

</table>

<div class="pagination">
    <?php echo $paging->fetchNavegacion(); ?>
</div>


