<?php

session_start();

// Si NO es - Admin | Operador | Supervisor
if ($_SESSION["tipo_usuario"] != 1 && $_SESSION["tipo_usuario"] != 6 && $_SESSION["tipo_usuario"] != 7) {
    header("Location: escritorio.php");
}

//// Solo pueden ingresar a esta sección administradores
//if ($_SESSION["tipo_usuario"] != '1') {
//    
// Y los usuarios habilitados
if ($_SESSION["reportes"] != '1') {
    header("Location: escritorio.php");
}
//}

$seccion = "TicketsPReportes";
$titulo = "Reportes";

include("clases/framework-1.0/class.bd.php");
include("includes/funciones.php");
require_once ('clases/phppaging/PHPPaging.lib.php');

include("inc.encabezado.php");
include("inc.report.php");
include("inc.pie.php");
?>
