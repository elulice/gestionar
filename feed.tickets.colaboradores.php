<?php

function formatDate($date) {
    $date = trim($date);
    if (!empty($date)) {
        $y = substr($date, 0, 4);
        $m = substr($date, 4, 2);
        $d = substr($date, 6, 2);
        return $y . '-' . $m . '-' . $d;
    } else {
        return NULL;
    }
}

if (!empty($_GET['term']) && strlen($_GET['term']) > 3) {
    $connect = odbc_connect("SQLGestion", "gestiondbconsultas", "gconsultas");


    $query = "
        select TOP 10 
            persona.PerDocumento as 'Documento',
            persona.PerApellido as 'Apellido', 
            persona.PerNombres as 'Nombres',
            e1.EmpRSocial as 'Usuaria',
            uo1.UniNombre as 'UOC',
            Legajonodo.LNodFecIng as 'F.Ingreso Asig.', 
            legajonodo.LNodFecEgr as 'F.Egreso Asig.',
            com1.ComValor as 'Email',
            e1.EmpNro as 'Cod.Cliente', 
            persona.PerNro as 'Cod.persona'
          from LegajoNodo
            join legajo on legajonodo.LegNro=legajo.LegNro
            join Persona on legajo.PerNro=persona.PerNro
            join Nodo as n1 on legajonodo.NodNro=n1.NodNro
            join Empresa as e1 on n1.EmpNro=e1.EmpNro
            left join Comunicacion as com1 on com1.PerNro=persona.PerNro and com1.MComNro=2  -- email
            join UnidadOrg as uo1 on uo1.UniNro=n1.UniNro --UOC
            WHERE
  persona.PerDocumento LIKE '" . $_GET['term'] . "%' ORDER BY Legajonodo.LNodFecIng DESC";

    $result = odbc_exec($connect, $query);

    $users = array();

    $return = array(array('id' => 0, 'value' => $_GET['term'], 'label' => 'Sin resultados'));
    if ($result !== FALSE && odbc_num_rows($result) > 0) {
        $return = array();
        $i = 0;
        while (odbc_fetch_row($result)) {
            $data = array();
            $data['cliente'] = trim(odbc_result($result, 4));
            $data['UOC'] = trim(odbc_result($result, 5));
            $nombre = htmlentities(odbc_result($result, 2));
            $nombres = str_replace('&Ntilde;', 'Ñ', $nombre);
            $nombres = str_replace('&Ccedil;', 'Ç', $nombre);
            $data['nombre'] = (trim($nombres)) . ', ' . trim(odbc_result($result, 3));
            $data['entrada'] = formatDate(odbc_result($result, 6));
            $data['salida'] = formatDate(odbc_result($result, 7));
            $data['clienteid'] = trim(odbc_result($result, 9));
            $data['label'] = trim(odbc_result($result, 1)) . ' - ' . $data['nombre'] . ' (' . $data['cliente'] . ') ' . date('d/m/Y', strtotime($data['entrada'])) .
                    ' - ' . $data['UOC'];
//            $data['label'] = trim(odbc_result($result, 1));
            $data['id'] = trim(odbc_result($result, 10));
            $data['email'] = trim(odbc_result($result, 8));

            if (!array_key_exists($data['id'], $users)) {
                $users[$data['id']] = true;
                $data['ultima_entrada'] = true;
            } else {
                $data['ultima_entrada'] = false;
            }

            $resultEmails = odbc_exec($connect, "select * from comunicacion where EmpNro = '" . $data['clienteid'] . "' and McomNro = '2'");
            while ($row = odbc_fetch_array($resultEmails)) {
                $data['usuariaemail'] = $row['ComValor'];
            }

            $return[] = $data;
            $i++;
        }
    }
# 6 - destruyo la conexion
    odbc_close($connect);
    die(json_encode($return));




//    include('clases/framework-1.0/class.bd.php');
//    $sql = 'SELECT email,
//        CONCAT(apellido, ", ", nombre) as nombre,
//        CONCAT(dni, " - ", apellido, ", ", nombre) as label,
//        id,
//        fecha_hasta as salida,
//        CliRsocial as cliente,
//        CliNro as clienteid,
//        dni as value
//        FROM colaboradores
//        INNER JOIN cliente ON(usuaria_id = CliNro)
//        WHERE dni LIKE "%' . $_GET['term'] . '%" ORDER BY LENGTH(dni), nombre LIMIT 15';
//    $bd = new BD();
//    $result = $bd->Seleccionar($sql);
//    if (mysql_num_rows($result) !== FALSE && mysql_num_rows($result) !== 0) {
//        $json = array();
//        while ($row = mysql_fetch_assoc($result)) {
//            $json[] = $row;
//        }
//        die(json_encode($json));
//    }
//    die(json_encode(array(array('id' => 0, 'value' => $_GET['term'], 'label' => 'Sin resultados'))));
}
die('No results');
?>
