<?php

# te paso los pasos para conectarte por PHP via ODBC a nuestro SQL.
# 1 - conecto con el DSN "SQLGestion" con nuevo usuario y password. 
$connect = odbc_connect("SQLGestion", "consulta", "gestionconsulta");

# 2 - armo la tabla empresa para los campos numero y razonsocial
$query = "SELECT Distinct MiEmpresa.EmpNro AS MiEmpNro, MiEmpresa.EmpRSocial AS [Empresa Administrada], MiUnidadOrg.UniNro AS MiUniNro, 

MiUnidadOrg.UniNombre AS [U.O. Propia], 

UnidadOrg.UniNro, 

Empresa.EmpNro, Empresa.EmpRSocial AS [Empresa], UnidadOrg.UniNombre AS [U.O. Empresa], 

MiNodo.NodNro MiNodNro, Nodo.NodNro 

FROM RelEntreNodos INNER JOIN 

UnidadOrg INNER JOIN 

Nodo ON UnidadOrg.UniNro = Nodo.UniNro ON 

RelEntreNodos.NodNro = Nodo.NodNro INNER JOIN 

UnidadOrg MiUnidadOrg INNER JOIN 

Nodo MiNodo ON 

MiUnidadOrg.UniNro = MiNodo.UniNro ON 

RelEntreNodos.MiNodNro = MiNodo.NodNro 

INNER JOIN Empresa MiEmpresa ON MiNodo.EmpNro = MiEmpresa.EmpNro 

INNER JOIN Empresa ON Nodo.EmpNro = Empresa.EmpNro 

INNER JOIN RelEntreEmpr ON Empresa.EmpNro = RelEntreEmpr.TieneEmprNro and RelEntreEmpr.RelTipo=2

WHERE MiNodo.EmpNro IN (17, 271, 6247, 6906, 10215, 8148) AND 

Nodo.NodNro IN (7562, 11501, 11500, 11870, 11499, 12162) AND 

MiNodo.NodNro IN (8125, 2735) ORDER BY MiEmpresa.EmpRSocial, MiEmpresa.EmpNro, Empresa.EmpRSocial, Empresa.EmpNro, MiUnidadOrg.UniNombre, MiUnidadOrg.UniNro, UnidadOrg.UniNombre, UnidadOrg.UniNro
";

# 3 - ejecuto la consulta
$result = odbc_exec($connect, $query);

#4 - Limpio la tabla de Clientes

$conectID2 = mysql_connect('localhost', 'root', '');
mysql_select_db('gestion_ar');

$drop = mysql_query('TRUNCATE TABLE cliente');

# 5 - guardo la data en una estructura repetitiva y actualiza la base de datos.
while (odbc_fetch_row($result)) {
    // NO SE USA -> $razonSocial = odbc_result($result, 1);
    $empNombre = odbc_result($result, 2);
    echo $empNombre . " - ";
    $empNro = odbc_result($result, 3);
    echo $empNro . " - ";
    $empRSocial = odbc_result($result, 4);
    echo $empRSocial . " - ";
    $empCuit = odbc_result($result, 5);
    echo $empCuit . " - ";
    $empEmail = odbc_result($result, 6);
    echo $empEmail . "<br/>";
}
# 6 - destruyo la conexion
odbc_close($connect);
?>
