<?php

require_once("clases/framework-1.0/class.bd.php");
require_once("clases/phppaging/PHPPaging.lib.php");
require_once("includes/funciones.php");

switch ($_GET["objeto"]) {

   case "principal"  : curriculums_principal(); break;
   case "detalle"    : curriculums_detalle(); break;
   case "documentos" : curriculums_documentos(); break;
   case "clientes"   : curriculums_clientes(); break;
   case "explaboral"   : cargar_explaboral(); break;
   case "estudios"   : cargar_estudios(); break;
   case "asignacion" : curriculums_asignacion(); break;
   case "postulaciones"   : curriculums_postulaciones(); break;

}

function curriculums_principal() {

	$desde_dialog = FALSE;
   if (isset($_GET["dialog"])) $desde_dialog = TRUE;

   $dni = (int) $_GET["txt_dni"];
   $apellido = $_GET["txt_apellido"];
   $nombre = $_GET["txt_nombre"];

   $where = "";
   if ($dni > 0) $where .= " AND PerDocumento LIKE '". $dni ."%' ";
   if (!empty($apellido)) $where .= " AND PerApellido LIKE '%$apellido%' ";
   if (!empty($nombre)) $where .= " AND PerNombres LIKE '%$nombre%' ";

   $query = "SELECT p.PerNro, p.PerApellido, p.PerNombres, p.PerTelefono, p.PerLocalidad, p.PerFechModific, p.PerEstado, e.PEsNro, e.PEsDescrip
      FROM persona p LEFT JOIN postulacionestado AS e ON p.PerEstado = e.PEsNro WHERE 1 $where  ";
	  
	 if ($desde_dialog == TRUE) {
      $idoferta = (int) $_GET["idoferta"];
      if ($idoferta > 0)
	 $query .= " AND p.PerNro NOT IN (SELECT PerNro FROM postulacion WHERE OfeNro=$idoferta) ";
   }

   $query .= " ORDER BY PerApellido ASC, PerNombres ASC";

   $db = new BD();
   $db->Conectar();

   $paging = new PHPPaging($db->RetornarConexion());
   $paging->agregarConsulta($query);
   $paging->linkClase("navPage");
   $paging->porPagina(5);
   $paging->ejecutar();

   ?>
   <div style="height:240px">
   <table width="833" cellpadding="0" cellspacing="0" style="margin:12px 0 0 12px;" id="box-table-a">
      <thead>
	 <tr>
	 <?php if ($desde_dialog == FALSE) { ?>
	    <th width="120" scope="col"><span style="color:#c60;font-weight:bold;">Apellido</span></th>
	    <th width="120" scope="col"><span style="color:#c60;font-weight:bold;">Nombres</span></th>
	    <th width="60" scope="col"><span style="color:#c60;font-weight:bold;">Teléfono</span></th>
	    <th width="60" scope="col"><span style="color:#c60;font-weight:bold;">Localidad</span></th>
	    <th width="60" scope="col"><span style="color:#c60;font-weight:bold;">Actualización</span></th>
	    <th width="30" scope="col"><span style="color:#c60;font-weight:bold;">Opciones</span></th>
		<?php } else { ?>
		<th width="120" scope="col"><span style="color:#c60;font-weight:bold;">Apellido</span></th>
	    <th width="120" scope="col"><span style="color:#c60;font-weight:bold;">Nombres</span></th>
	    <th width="60" scope="col"><span style="color:#c60;font-weight:bold;">Teléfono</span></th>
	    <th width="60" scope="col"><span style="color:#c60;font-weight:bold;">Localidad</span></th>
	    <th width="60" scope="col"><span style="color:#c60;font-weight:bold;">Actualización</span></th>
	    <th width="30" scope="col"><span style="color:#c60;font-weight:bold;">Opciones</span></th>
		<?php } ?>
	 </tr>
      </thead>
      <tbody>
   <?php

   while ($row = $paging->fetchResultado()) {
   ?>
   <tr>
    <?php if ($desde_dialog == FALSE) { ?>
      <td style="padding:8px;"><?php echo $row["PerApellido"]; ?>&nbsp;</td>
      <td style="padding:8px;"><?php echo $row["PerNombres"]; ?>&nbsp;</td>
      <td style="padding:8px;"><?php echo $row["PerTelefono"]; ?>&nbsp;</td>
      <td style="padding:8px;"><?php echo $row["PerLocalidad"]; ?>&nbsp;</td>
      <td style="padding:8px;"><?php print(date("d/m/Y", strtotime($row["PerFechModific"]))); ?>&nbsp;</td>
      <td style="padding:8px;">
      <img src="images/icons/zoom_in.png" alt="Ver Detalles" title="Ver Detalles" onclick="editar_curriculum(<?php echo $row["PerNro"]; ?>);" style="cursor:pointer;" />
      <img src="images/icons/link.png" alt="Asignar a Oferta" title="Asignar a Oferta" onclick="mostrar_asignaciones(<?php echo $row["PerNro"]; ?>);" style="cursor:pointer;" />
      <?php if($_SESSION["strict_menu"]): ?>
	  <img src="images/icons/page_delete.png" alt="Eliminar" title="Eliminar" style="cursor:pointer" onclick="eliminar_cv(<?php echo $row["PerNro"]; ?>);" />
      <?php endif; ?>
      </td>
	   <?php } else { ?>
	   <td style="padding:8px;"><?php echo $row["PerApellido"]; ?>&nbsp;</td>
      <td style="padding:8px;"><?php echo $row["PerNombres"]; ?>&nbsp;</td>
      <td style="padding:8px;"><?php echo $row["PerTelefono"]; ?>&nbsp;</td>
      <td style="padding:8px;"><?php echo $row["PerLocalidad"]; ?>&nbsp;</td>
      <td style="padding:8px;"><?php print(date("d/m/Y", strtotime($row["PerFechModific"]))); ?>&nbsp;</td>
      <td style="padding:8px;">
      <img src="images/icons/add_small.png" alt="Asignar a Oferta" title="Asignar a Oferta" style="cursor:pointer;" onclick="asignar_postulante_oferta2(<?php echo $row["PerNro"]; ?>);" />
      </td>
	  <?php }  ?>
   </tr>
   <?php
   }
   ?>
   </tbody>
   </table>
   </div>
   <div class="pagination"><?php echo $paging->fetchNavegacion(); ?></div>
   <?php
}

function curriculums_detalle() {

   $id_persona = (int) $_GET["id_postulante"];
   if ($id_persona == 0) exit("");

   $query = "SELECT p.*, est.*, pn.* FROM persona AS p 
      LEFT JOIN estudio AS est USING(PerNro) LEFT JOIN postulantenotas AS pn USING(PerNro)
      WHERE p.PerNro = $id_persona";

   $db = new BD();
   $db->Conectar();
   $row = $db->Seleccionar($query, TRUE);

   echo json_encode($row);
}

function curriculums_documentos() {

   $id_postulante = (int) $_GET["id_postulante"];
   if ($id_postulante < 1) exit("");

   $query = "SELECT d.DNro, d.DFecha, d.PerNro, td.TDNombre, d.DNombre FROM documentacion AS d
      INNER JOIN tipodocumentacion AS td USING(TDNro) WHERE d.PerNro=$id_postulante
      ORDER BY DFecha DESC, TDNombre";

   $db = new BD();
   $db->Conectar();

   $paging = new PHPPaging($db->RetornarConexion());
   $paging->agregarConsulta($query);
   $paging->linkClase("navPage");
   $paging->porPagina(10);
   $paging->ejecutar();

   ?>
   <div style="height:465px">
   <table width="727" cellpadding="0" cellspacing="0" style="margin:12px 0 0 12px;" id="box-table-a">
      <thead>
	 <tr>
	    <th width="720" scope="col"><span style="color:#c60;font-weight:bold;">Documento</span></th>
	    <th width="80" scope="col"><span style="color:#c60;font-weight:bold;">Fecha</span></th>
	    <th width="80" scope="col"><span style="color:#c60;font-weight:bold;">Opciones</span></th>
	 </tr>
      </thead>
      <tbody>
   <?php

   while ($row = $paging->fetchResultado()) {

      $js_parm = $id_postulante . ", " . $row["DNro"];

      $nombre_archivo = "upload/documentos/" . $row["PerNro"] . "_" . $row["DNro"] . "_" . str_replace(" ", "_", $row["DNombre"]);
      $link_descarga = FALSE; 
      if (file_exists($nombre_archivo)) $link_descarga = TRUE;
   ?>
      <tr>
	 <td style="padding:8px;"><?php echo $row["TDNombre"] . " - " . $row["DNombre"]; ?></td>
	 <td style="padding:8px;"><?php echo $row["DFecha"]; ?></td>
	 <td style="padding:8px;">
	 <?php
	    
	    if ($link_descarga == TRUE) {
	    ?>
	       <img src="images/icons/bullet_disk.png" width="20" height="20" alt="Descargar" title="Descargar" style="cursor:pointer;" onclick="window.open('<?php echo $nombre_archivo; ?>');" />
	    <?php
	    }

	 ?>
	    <img src="images/icons/page_delete.png" alt="Eliminar" title="Eliminar" onclick="eliminar_documento(<?php echo $js_parm; ?>);" style="cursor:pointer;" />
	 </td>
      </tr>
   <?php
   }

   ?>
      </tbody>
   </table>
   </div>
   <div class="pagination"><?php echo $paging->fetchNavegacion(); ?></div>
   <?php
}

function curriculums_clientes() {

   $id_postulante = (int) $_GET["id_postulante"];
   if ($id_postulante == 0) exit("");

   $query = "SELECT pc.PerCli, pc.CliNro, c.CliRSocial FROM personacliente pc
      INNER JOIN cliente c USING(CliNro) WHERE pc.PerNro=$id_postulante
      ORDER BY CliRSocial";

   $db = new BD();
   $db->Conectar();

   $paging = new PHPPaging($db->RetornarConexion());
   $paging->agregarConsulta($query);
   $paging->linkClase("navPage");
   $paging->porPagina(10);
   $paging->ejecutar();

   ?>
   <div style="height:465px;">
   <table width="727" cellpadding="0" cellspacing="0" style="margin:12px 0 0 12px;" id="box-table-a">
      <thead>
	 <tr>
	    <th width="720" scope="col"><span style="color:#c60;font-weight:bold;">Cliente</span></th>
	    <th width="80" scope="col"><span style="color:#c60;font-weight:bold;">Opciones</span></th>
	 </tr>
      </thead>
      <tbody>
   <?php

   while ($row = $paging->fetchResultado()) {
   ?>
      <tr>
	 <td style="padding:8px;"><?php echo htmlentities($row["CliRSocial"]); ?></td>
	 <td style="padding:8px;"><img src="images/icons/page_delete.png" alt="Eliminar" title="Eliminar" onclick="eliminar_cliente(<?php echo $row["PerCli"]; ?>);" style="cursor:pointer;" /></td>
      </tr>
   <?php
   }
   ?>
      </tbody>
   </table>
   </div>
   <div class="pagination"><?php echo $paging->fetchNavegacion(); ?></div>
      <?php
}

function curriculums_postulaciones() {

   $id_postulante = (int) $_GET["id_postulante"];
   if ($id_postulante == 0) exit("");

   $query = "SELECT f.OfeTitulo, e.PEsDescrip,o.PosEstado,o.id_postulacion, CliRsocial  FROM persona p 
left join postulacion as o on p.PerNro = o.PerNro 
left join oferta as f on o.OfeNro = f.OfeNro
left join postulacionestado as e on o.PosEstado = e.PEsNro
LEFT JOIN ofertascliente oc ON oc.OfeNro = f.OfeNro
LEFT JOIN cliente ON oc.CliNro = cliente.CliNro
where p.PerNro=$id_postulante 
and p.PerNro IS NOT NULL and o.OfeNro IS NOT NULL and o.PosEstado IS NOT NULL and oc.OfeNro IS NOT NULL and oc.CliNro IS NOT NULL";

   $db = new BD();
   $db->Conectar();

   $paging = new PHPPaging($db->RetornarConexion());
   $paging->agregarConsulta($query);
   $paging->linkClase("navPage");
   $paging->porPagina(10);
   $paging->ejecutar();

   ?>
   <div style="height:465px;">
   <table width="727" cellpadding="0" cellspacing="0" style="margin:12px 0 0 12px;" id="box-table-a">
      <thead>
	 <tr>
	    <th width="300" scope="col"><span style="color:#c60;font-weight:bold;">Cliente</span></th>
		<th width="300" scope="col"><span style="color:#c60;font-weight:bold;">Puesto</span></th>
		<th width="120" scope="col"><span style="color:#c60;font-weight:bold;">Estado</span></th>
	    <th width="80" scope="col"><span style="color:#c60;font-weight:bold;">Opciones</span></th>
	 </tr>
      </thead>
      <tbody>
   <?php

   while ($row = $paging->fetchResultado()) {
   ?>
      <tr>
	   <td style="padding:8px;"><?php echo $row["CliRsocial"]; ?></td>
	 <td style="padding:8px;"><?php echo $row["OfeTitulo"]; ?></td>
	 <td style="padding:8px;">
	 
	 <script>
   $('#PosEstado<?php echo $row["id_postulacion"]; ?>').change(function(){
   $valor=$(this).attr('value');
   $idpostulacion = <?php echo $row["id_postulacion"]; ?>;
   actualizar_estado($valor,$idpostulacion);
});
   
</script>

	 
	 <select class="smallInput" id="PosEstado<?php echo $row["id_postulacion"]; ?>" >
	 <option value="0">--</option>
       <?php
					$sSQL = "SELECT PEsNro, PEsDescrip FROM postulacionestado  ";
					print(GenerarOptions($sSQL, $row["PosEstado"]));
			  ?>
      </select></td>
	 <td style="padding:8px;"><img src="images/icons/page_delete.png" alt="Eliminar" title="Eliminar" onclick="eliminar_postulaciones(<?php echo $row["id_postulacion"]; ?>);" style="cursor:pointer;" /></td>
      </tr>
   <?php
   }
   ?>
      </tbody>
   </table>
   </div>
   <div class="pagination"><?php echo $paging->fetchNavegacion(); ?></div>
   <?php

}

function curriculums_asignacion() {

   $id_postulante = (int) $_GET["id_postulante"];
   if ($id_postulante == 0) exit("");

   $query = "SELECT * FROM postulacion WHERE PerNro=$id_postulante";

   $db = new BD();
   $db->Conectar();

   $paging = new PHPPaging($db->RetornarConexion());
   $paging->agregarConsulta($query);
   $paging->linkClase("navPage");
   $paging->porPagina(5);
   $paging->ejecutar();

   ?>
   <div style="height:230px;">
   <table width="450" cellpadding="0" cellspacing="0" style="margin:12px 0 0 12px;" id="box-table-a">
      <thead>
	 <tr>
	    <th width="80" scope="col"><span style="color:#c60;font-weight:bold;">Fecha</span></th>
	    <th width="300" scope="col"><span style="color:#c60;font-weight:bold;">OfeNro</span></th>
	    <th width="60" scope="col"><span style="color:#c60;font-weight:bold;">Opciones</span></th>
	 </tr>
      </thead>
      <tbody>
   <?php

   while ($row = $paging->fetchResultado()) {
   ?>
      <tr>
	 <td style="padding:8px;"><?php echo date("d/m/Y", strtotime($row["PosFecha"])); ?></td>
	 <td style="padding:8px;"><?php echo $row["OfeNro"]; ?></td>
	 <td style="padding:8px;"><img src="images/icons/page_delete.png" alt="Eliminar" title="Eliminar" onclick="eliminar_postulacion(<?php echo $row["PerCli"]; ?>);" style="cursor:pointer;" /></td>
      </tr>
   <?php
   }

   ?>
      </tbody>
   </table>
   </div>
   <div class="pagination"><?php echo $paging->fetchNavegacion(); ?></div>
   <?php


}

function cargar_explaboral() {

   $id_postulante = (int) $_GET["id_postulante"];
   if ($id_postulante == 0) exit("");

   $query = "SELECT * FROM `datospuesto` WHERE PerNro=$id_postulante";

   $db = new BD();
   $db->Conectar();

   $paging = new PHPPaging($db->RetornarConexion());
   $paging->agregarConsulta($query);
   $paging->linkClase("navPage");
   $paging->porPagina(10);
   $paging->ejecutar();

   ?>
   <div style="height:465px;">
   <table width="727" cellpadding="0" cellspacing="0" style="margin:12px 0 0 12px;" id="box-table-a">
      <thead>
	 <tr>
	    <th width="720" scope="col"><span style="color:#c60;font-weight:bold;">Empresa</span></th>
	    <th width="80" scope="col"><span style="color:#c60;font-weight:bold;">Opciones</span></th>
	 </tr>
      </thead>
      <tbody>
   <?php

   while ($row = $paging->fetchResultado()) {
   ?>
      <tr>
	 <td style="padding:8px;"><? echo $row[PueUltimaEmpresa] ?></td>
	 <td style="padding:8px;"> <img src="images/icons/zoom_in.png" alt="Ver Detalles" title="Ver Detalles" onclick="cargar_expl(<? echo $row[DPueNro] ?>)" style="cursor:pointer; margin:0 0 0 17px" /></td>
      </tr>
   <?php
   }
   ?>
      </tbody>
   </table>
   </div>
   <div class="pagination"><?php echo $paging->fetchNavegacion(); ?></div>
   <?php

}

function cargar_estudios() {

   $id_postulante = (int) $_GET["id_postulante"];
   if ($id_postulante == 0) exit("");

   $query = "SELECT * FROM `estudio` WHERE PerNro=$id_postulante";

   $db = new BD();
   $db->Conectar();

   $paging = new PHPPaging($db->RetornarConexion());
   $paging->agregarConsulta($query);
   $paging->linkClase("navPage");
   $paging->porPagina(10);
   $paging->ejecutar();

   ?>
   <div style="height:465px;">
   <table width="727" cellpadding="0" cellspacing="0" style="margin:12px 0 0 12px;" id="box-table-a">
      <thead>
	 <tr>
	    <th width="720" scope="col"><span style="color:#c60;font-weight:bold;">Titulo</span></th>
	    <th width="80" scope="col"><span style="color:#c60;font-weight:bold;">Opciones</span></th>
	 </tr>
      </thead>
      <tbody>
   <?php

   while ($row = $paging->fetchResultado()) {
   ?>
      <tr>
	 <td style="padding:8px;"><? echo $row[EtuInstitucion] ?></td>
	 <td style="padding:8px;"> <img src="images/icons/zoom_in.png" alt="Ver Detalles" title="Ver Detalles" onclick="cargar_estudio(<? echo $row[EtuNro] ?>)" style="cursor:pointer; margin:0 0 0 17px" /></td>
      </tr>
   <?php
   }
   ?>
      </tbody>
   </table>
   </div>
   <div class="pagination"><?php echo $paging->fetchNavegacion(); ?></div>
   <?php

}


?>



