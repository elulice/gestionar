<?php
	include("includes/excelwriter.inc.php");
	include("clases/framework-1.0/class.bd.php");

	header("Content-Type: application/vnd.ms-excel");
	header("Expires: 0");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("content-disposition: attachment;filename=".basename($xlsFile));
	
	$xlsFile = "temporal/reporte.xls";
	
	
  if ($_GET["tiporeporte"] == 2)
	{
		$excel=new ExcelWriter($xlsFile);
		
		if($excel==false)	
			echo $excel->error;
		
		$myArr=array("Alta","Validez", "Ref", "Titulo", "Clasificacion", "Area", "Puesto", "Localidad", "Selector", "Usuaria", "Ejec. Cuenta", "Unidad Organizativa", "Vacantes", "Situacion", "Forma Cont", "Postulantes", "Tipo y nro. Documento", "Condicion", "Fecha");
		$excel->writeLineHead($myArr);
		
		$sSQL = "SELECT o.OfeNro, o.OfeFechAlta, o.OfeValidez, o.OfeReferencia, ";
		$sSQL .= "o.OfeTitulo, o.OfeClasificacion, a.AreNom , pue.PueNom, ";
		$sSQL .= "o.OfeLocalidad, CONCAT(me.MEmpApellido, \" \", me.MEmpNombres) AS Selector, ";
		$sSQL .= "c.CliRSocial AS Cliente,  c.CliEjCta AS Ejec_cuenta, ";
		$sSQL .= "uo.UniNombre, o.OfeCantPost AS Vacantes, EOfeValor, tp.nombre ";

		$sSQL .= "FROM oferta o ";
		$sSQL .= "INNER JOIN estadooferta ON estadooferta.OfeNro = o.OfeNro ";
		$sSQL .= "INNER JOIN puesto pue ON o.PueNro = pue.PueNro ";
		$sSQL .= "INNER JOIN area a ON a.AreNro = o.AreNro ";
		$sSQL .= "INNER JOIN ofertascliente oc ON oc.OfeNro = o.OfeNro  ";
		$sSQL .= "INNER JOIN cliente c ON oc.CliNro = c.CliNro ";
		$sSQL .= "LEFT JOIN miembroempresa me ON me.MEmpNro = o.MEmpNro ";
		$sSQL .= "LEFT JOIN unidadorg uo ON uo.UniNro = o.NodNro ";
		$sSQL .= "INNER JOIN _tipo_pedido tp ON tp.idtipo = o.TPeNro ";
		
		
		$sSQL .= "WHERE EOfeValor = (SELECT EOfeValor FROM estadooferta eo WHERE eo.OfeNro = o.OfeNro ORDER BY EOfeFecha DESC LIMIT 1) ";
		
		$sSQL .= GenerarFiltros();
		$sSQL .= " ORDER BY OfeFechAlta DESC, OfeNro DESC LIMIT 50 ";
		
		
		$cBD = new BD();
		$oResultado = $cBD->Seleccionar($sSQL);
		
		while ($aRegistro = $cBD->RetornarFila($oResultado))
		{
			$IDPropuesta = $aRegistro["OfeNro"];
			$excel->writeRow();
			$excel->writeCol(date("d/m/Y", strtotime($aRegistro["OfeFechAlta"])), false, true);
			$excel->writeCol(date("d/m/Y", strtotime($aRegistro["OfeValidez"])), false, true);
			//$excel->writeCol(date("d/m/Y", strtotime($aRegistro["fecha_modificacion"])), false, true);
			$excel->writeCol($aRegistro["OfeReferencia"]);
			$excel->writeCol($aRegistro["OfeTitulo"]);
			$excel->writeCol($aRegistro["OfeClasificacion"]);
			$excel->writeCol($aRegistro["AreNom"]);
			$excel->writeCol($aRegistro["PueNom"]);
			$excel->writeCol($aRegistro["OfeLocalidad"]);
			$excel->writeCol($aRegistro["Selector"]);
			$excel->writeCol($aRegistro["Cliente"]);
			$excel->writeCol($aRegistro["Ejec_cuenta"]);
			$excel->writeCol($aRegistro["UniNombre"]);
			$excel->writeCol($aRegistro["Vacantes"], true);
			$excel->writeCol($aRegistro["EOfeValor"]);
			$excel->writeCol($aRegistro["nombre"]);
			
			//loop interno
					$sSQL = "SELECT CONCAT(PerApellido, \" \", PerNombres) AS Postulante, ";
					$sSQL .= "CONCAT(PerTipoDoc, \" \", PerDocumento) AS Documento, ";
					$sSQL .= "po.PosEstFecha, pe.PEsDescrip ";

					$sSQL .= "FROM postulacion po ";
					$sSQL .= "INNER JOIN persona p ON p.PerNro = po.PerNro ";
					$sSQL .= "LEFT JOIN postulacionestado pe ON pe.PEsNro = po.PEsNro ";

					$sSQL .= "WHERE po.OfeNro = ". $IDPropuesta ." ";
					$sSQL .= "ORDER BY po.PosFecha DESC, po.PosHora DESC ";
					$cBD = new BD();
					$oResultados2 = $cBD->Seleccionar($sSQL);
					
						//$excel->writeRow();
						//$excel->writeCol($sSQL);

					while ($bRegistro = $cBD->RetornarFila($oResultados2))
					{
						$IDPropuesta = $aRegistro["OfeNro"];
						$excel->writeRow();
						for ($i = 1; $i <= 15; $i++)
							$excel->writeCol("");
						
						$excel->writeCol($bRegistro["Postulante"]);
						$excel->writeCol($bRegistro["Documento"]);
						$excel->writeCol($bRegistro["PEsDescrip"]);
						//$excel->writeCol($bRegistro["forma_cont"]);
						$excel->writeCol(date("d/m/Y", strtotime($bRegistro["PosEstFecha"])), false, true);
					}  
			//fin loop interno
			$excel->writeRow();
		}  
	
			
	
		
		
			$excel->close();
			readfile($xlsFile);

		//echo "data is write into reporteDetallado.xls Successfully.";
		//echo $sSQL;
	}



//***************************************************************************	
	function GenerarFiltros()
	{
		$bFechaDesde = ($_GET["fecha_desde"] ? $_GET["fecha_desde"] : "");
		$bFechaHasta = ($_GET["fecha_hasta"] ? $_GET["fecha_hasta"] : date("d-m-Y"));
//		$bFechaModifDesde = ($_GET["fecha_modif_desde"] ? $_GET["fecha_modif_desde"] : "");
//		$bFechaModifHasta = ($_GET["fecha_modif_hasta"] ? $_GET["fecha_modif_hasta"] : date("d-m-Y"));
		
		$bUnidad = (is_numeric($_GET["idunidad"]) ? $_GET["idunidad"] : 0);
		$bArea = (is_numeric($_GET["idarea"]) ? $_GET["idarea"] : 0);
	
		$bCliente = ($_GET["cliente"] ? $_GET["cliente"] : "");
		$bIdCliente = ($_GET["idcliente"] ? $_GET["idcliente"] : "");
	
		$bReferencia = ($_GET["referencia"] ? $_GET["referencia"] : "");
		$bSelector = (is_numeric($_GET["idselector"]) ? $_GET["idselector"] : 0);
		$bEstado = ($_GET["estado"] ? $_GET["estado"] : "");
		
	
	if ($bCliente <> "")
		$sFiltro .= "AND c.CliRsocial LIKE '%". $bCliente ."%' ";
	if ($bIdCliente <> "")
		$sFiltro .= "AND c.CliNro =". $bIdCliente ." ";
		
	if ($bReferencia <> "")
		$sFiltro .= "AND o.OfeReferencia LIKE '%". $bReferencia ."%' ";
	if ($bFechaDesde <> "")
	{				
		$bFechaDesde = explode("-", $bFechaDesde);
		$sFecha = $bFechaDesde[2]."-".$bFechaDesde[1]."-".$bFechaDesde[0];
		$sFiltro .= "AND o.OfeFechAlta >= '". date("Y-m-d", strtotime($sFecha)) ."' ";
	}
	if ($bFechaHasta <> "")
	{
		$bFechaHasta = explode("-", $bFechaHasta);
		$sFecha = $bFechaHasta[2]."-".$bFechaHasta[1]."-".$bFechaHasta[0];
		$sFiltro .= "AND o.OfeFechAlta <= '". date("Y-m-d", strtotime($sFecha)) ."' ";
	}
//		if ($bFechaModifDesde <> "")
//		{				
//			$bFechaModifDesde = explode("-", $bFechaModifDesde);
//			$sFecha = $bFechaModifDesde[2]."-".$bFechaModifDesde[1]."-".$bFechaModifDesde[0];
//			$sFiltro .= "AND p.fecha_modificacion >= '". date("Y-m-d", strtotime($sFecha)) ."' ";
//		}
//		if ($bFechaModifHasta <> "")
//		{
//			$bFechaModifHasta = explode("-", $bFechaModifHasta);
//			$sFecha = $bFechaModifHasta[2]."-".$bFechaModifHasta[1]."-".$bFechaModifHasta[0];
//			$sFiltro .= "AND p.fecha_modificacion <= '". date("Y-m-d", strtotime($sFecha)) ."' ";
//		}
	if ($bArea > 0)
		$sFiltro .= "AND o.AreNro = ". $bArea ." ";
	if (strlen($bEstado) > 0)
		$sFiltro .= "AND EOfeValor = '". $bEstado ."' ";
	if ($bUnidad > 0)
		$sFiltro .= "AND o.NodNro = ". $bUnidad ." ";
	if ($bSelector > 0)
		$sFiltro .= "AND o.MEmpNro = ". $bSelector ." ";
	
		return $sFiltro;
	}
?>
