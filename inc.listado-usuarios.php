<table width="780" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="30" class="encabezado-titulo-bg"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="20"><img src="images/espacio.gif" width="1" height="1" /></td>
        <td class="encabezado-titulo-texto">Listado de Usuarios </td>
        <td align="right" class="encabezado-titulo-texto"><a href="am-usuarios.php?idregistro=0&amp;url=<?php print($m_sURL); ?>"><img src="images/btn-agregar.jpg" width="80" height="20" border="0" /></a></td>
        <td width="20"><img src="images/espacio.gif" width="1" height="1" /></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><img src="images/espacio.gif" width="1" height="20"></td>
  </tr>
</table>
<table width="780" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="20"><img src="images/listado-encabezado-inicio.jpg" width="20" height="37"></td>
        <td class="listado-encabezado-bg"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="70"><img src="images/espacio.gif" width="1" height="1"></td>
            <td width="120" class="listado-encabezado-texto">Apellido</td>
            <td width="120" class="listado-encabezado-texto">Nombres</td>
            <td width="120" class="listado-encabezado-texto">Nombre de Usuario</td>
            <td width="150" class="listado-encabezado-texto">Tipo</td>
            <td class="listado-encabezado-texto">Sucursales</td>
            </tr>
        </table></td>
        <td width="20"><img src="images/listado-encabezado-final.jpg" width="20" height="37"></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="5" class="listado-contenido-inicio"><img src="images/espacio.gif" width="1" height="1"></td>
        <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
		  <?php
		  		$lRegistros = 0;
			 	$sSQL = "SELECT MEmpNro AS idmiembro,MEmpApellido AS apellido, MEmpNombres AS nombres, TMiembDescrip AS tipo, MEmpUsuario AS usuario, UniNombre AS unidad ";
				
				$sSQL .= "FROM miembroempresa me INNER JOIN tipomiembro ON MEmpAdmin = TMiembNro ";
				$sSQL .= "LEFT JOIN unidadorg uo ON me.UniNro = uo.UniNro ";
				
				if (RetornarTipoUsuario() == 1)
					$sSQL .= "WHERE me.EmpNro = ".$_SESSION["idempresa"]." ";
				else
					$sSQL .= "WHERE MEmpNro = ".$_SESSION["idmiembro"]." ";
				
				$sSQL .= "ORDER BY apellido ASC, nombres ASC ";
//				echo $sSQL;
				
				$cBD = new BD();
				$oResultado = $cBD->Seleccionar($sSQL);
				while($aRegistro = $cBD->RetornarFila($oResultado))
				{
					$sPosicion = (($sPosicion == "1") ? "2" : "1");
		  ?>
          <tr>
            <td class="listado-fila-bg-<?php print($sPosicion); ?>"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="15"><img src="images/espacio.gif" width="1" height="1"></td>
                <td width="70"><table width="70" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="30"><a href="am-usuarios.php?idregistro=<?php print($aRegistro["idmiembro"]); ?>&url=<?php print($m_sURL); ?>"><img src="images/btn-modificar-<?php print($sPosicion); ?>.jpg" alt="Modificar" width="24" height="23" border="0"></a></td>
					
<?php 	if (RetornarTipoUsuario() == 1) {
?>                    <td><a href="abm.php?tabla=usuarios&columna=idusuario&idregistro=<?php print($aRegistro["idmiembro"]); ?>&url=<?php print($m_sURL); ?>" onclick="return confirm('&iquest;Desea eliminar este Usuario?')"><img src="images/btn-eliminar-<?php print($sPosicion); ?>.jpg" alt="Eliminar" width="24" height="23" border="0"></a></td>
<?php 	} ?>
                  </tr>
                </table></td>
                <td width="120" class="listado-texto"><?php print($aRegistro["apellido"]); ?></td>
                <td width="120" class="listado-texto"><?php print($aRegistro["nombres"]); ?></td>
                <td width="120" class="listado-texto"><?php print($aRegistro["usuario"]); ?></td>
                <td width="150" class="listado-texto"><?php print($aRegistro["tipo"]); ?></td>
                <td width="150" class="listado-texto"><?php print($aRegistro["unidad"]); ?></td>
                <td>&nbsp;</td>
              </tr>
            </table></td>
          </tr>
          <?php
			 		$lRegistros++;
				}
				if($lRegistros == 0)
				{
			 ?>
          <tr>
            <td><img src="images/espacio.gif" width="1" height="20"></td>
          </tr>
			 <?php } ?>
        </table></td>
        <td width="6" class="listado-contenido-final"><img src="images/espacio.gif" width="1" height="1"></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="100"><img src="images/listado-pie-inicio-deshabilitado.jpg" alt="Agregar" width="100" height="40" border="0"></td>
        <td class="listado-pie-bg">&nbsp;</td>
        <td width="20"><img src="images/listado-pie-final.jpg" width="20" height="40"></td>
      </tr>
    </table></td>
  </tr>
</table>
