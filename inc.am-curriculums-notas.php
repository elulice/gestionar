<?php
	if($m_lIDRegistro > 0)
	{
		$sSQL = "SELECT p.PerApellido, p.PerNombres, pn.*  ";
		$sSQL .= "FROM persona p ";
		$sSQL .= "LEFT JOIN postulantenotas pn ON p.PerNro = pn.PerNro ";
		$sSQL .= "WHERE p.PerNro = " . $m_lIDRegistro;
		$cBD = new BD();
		$aRegistro = $cBD->Seleccionar($sSQL, true);
	}
		$aRegistro["NotNro"] = (is_numeric($aRegistro["NotNro"]) ? $aRegistro["NotNro"]: 0);
	
?>
<link href="estilos/general.css" rel="stylesheet" type="text/css" />


<table width="780" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="30" class="encabezado-titulo-bg"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="20"><img src="images/espacio.gif" width="1" height="1"></td>
        <td class="encabezado-titulo-texto">Alta y modificaci&oacute;n de Notas</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><img src="images/espacio.gif" width="1" height="20"></td>
  </tr>
</table>
<table width="780" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="20"><img src="images/formulario-encabezado-inicio.jpg" width="20" height="37" /></td>
        <td class="formulario-encabezado-bg">Notas sobre el Postulante - <?php print($aRegistro["PerApellido"]); ?>, <?php print($aRegistro["PerNombres"]); ?></td>
        <td width="20"><img src="images/formulario-encabezado-final.jpg" width="20" height="37" /></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="4" class="formulario-contenido-inicio"><img src="images/espacio.gif" width="1" height="1" /></td>
        <td><form action="abm.php?tabla=postulantenotas&amp;columna=NotNro&amp;idregistro=<?php print($aRegistro["NotNro"]); ?>&amp;url=<?php print($m_sURL); ?>" method="post" name="frmRegistro" id="frmRegistro">
          <table width="100%" border="0" cellpadding="0" cellspacing="0">
		  
		  <input name="PerNro" id="PerNro" type="hidden" value="<?php print $m_lIDRegistro; ?>" />
		  <input name="EmpNro" id="EmpNro" type="hidden" value="<?php print RetornarIdEmpresa(); ?>" />
		  
         <tr>
              <td width="10" rowspan="4" valign="top" class="formulario-etiquetas">&nbsp;</td>
              <td width="100" valign="top" class="formulario-etiquetas">Notas:</td>
              <td><textarea name="NotaDescrip" class="formulario-textbox" id="NotaDescrip" style="width: 600px; height:70px;"><?php print($aRegistro[NotaDescrip]); ?></textarea></td>
            </tr>
            <tr>
              <td height="20" colspan="2" valign="bottom" class="detalle-seccion">Grilla:</td>
            </tr>
            <tr>
              <td colspan="2" valign="top" class="formulario-textbox"><table border="1" align="center" cellpadding="0" cellspacing="1" style="border-collapse:collapse;">
                <tr class="formulario-textbox">
                  <td width="120" height="18" class="formulario-etiquetas">Presencia:</td>
                  <?php
					print(EnumRadio("postulantenotas", "presencia", $aRegistro["presencia"]));
			  		?>
                </tr>
                <tr class="formulario-textbox">
                  <td height="18" class="formulario-etiquetas">Imagen:</td>
                  <?php
					print(EnumRadio("postulantenotas", "imagen", $aRegistro["imagen"]));
			  		?>
                </tr>
                <tr class="formulario-textbox">
                  <td height="18" class="formulario-etiquetas">Contextura:</td>
                  <?php
					print(EnumRadio("postulantenotas", "contextura", $aRegistro["contextura"]));
			  		?>
<td colspan="2">&nbsp;</td>
                </tr>
                <tr class="formulario-textbox">
                  <td height="18" class="formulario-etiquetas">Trato:</td>
                  <?php
					print(EnumRadio("postulantenotas", "trato", $aRegistro["trato"]));
			  		?>
					<td colspan="4">&nbsp;</td>
                </tr>
                <tr class="formulario-textbox">
                  <td height="18" class="formulario-etiquetas">Comunicaci&oacute;n:</td>
                  <?php
					print(EnumRadio("postulantenotas", "comunicacion", $aRegistro["comunicacion"]));
			  		?>
					<td colspan="2">&nbsp;</td>
                </tr>
                <tr class="formulario-textbox">
                  <td height="18" class="formulario-etiquetas">Nivel Socio Cultural:</td>
                  <?php
					print(EnumRadio("postulantenotas", "nivel_sociocultural", $aRegistro["nivel_sociocultural"]));
			  		?><td colspan="2">&nbsp;</td>
                </tr>
                <tr class="formulario-textbox">
                  <td height="18" class="formulario-etiquetas">Flexibilidad/Adaptaci&oacute;n:</td>
                  <?php
					print(EnumRadio("postulantenotas", "flexibilidad", $aRegistro["flexibilidad"]));
			  		?>
                </tr>
                <tr class="formulario-textbox">
                  <td height="18" class="formulario-etiquetas">Dinamismo/Vitalidad:</td>
                  <?php
					print(EnumRadio("postulantenotas", "dinamismo", $aRegistro["dinamismo"]));
			  		?>
                </tr>
                <tr class="formulario-textbox">
                  <td height="18" class="formulario-etiquetas">Actitud  hacia el trabajo/oferta:</td>
                  <?php
					print(EnumRadio("postulantenotas", "actitud", $aRegistro["actitud"]));
			  		?>
                </tr>
                <tr class="formulario-textbox">
                  <td height="18" class="formulario-etiquetas">Perfil Comercial:</td>
                  <?php
					print(EnumRadio("postulantenotas", "perfil_comercial", $aRegistro["perfil_comercial"]));
			  		?>
                </tr>
                <tr class="formulario-textbox">
                  <td height="18" class="formulario-etiquetas">Experiencia:</td>
                  <?php
					print(EnumRadio("postulantenotas", "experiencia", $aRegistro["experiencia"]));
			  		?>
                </tr>
                <tr class="formulario-textbox">
                  <td height="18" class="formulario-etiquetas">Concepto General:</td>
                  <?php
					print(EnumRadio("postulantenotas", "concepto", $aRegistro["concepto"]));
			  		?>
                </tr>
                <tr class="formulario-textbox">
                  <td height="18" class="formulario-etiquetas">Disposicion trabajos Temporales:</td>
                  <?php
					print(EnumRadio("postulantenotas", "disp_temporal", $aRegistro["disp_temporal"]));
			  		?>
                </tr>
                <tr class="formulario-textbox">
                  <td height="18" class="formulario-etiquetas">Disposicion Turnos :</td>
                  <?php
					print(EnumRadio("postulantenotas", "disp_turnos", $aRegistro["disp_turnos"]));
			  		?><td colspan="2">&nbsp;</td>
                </tr>
                <tr class="formulario-textbox">
                  <td height="18" class="formulario-etiquetas">Disposicion fin de semana:</td>
                  <?php
					print(EnumRadio("postulantenotas", "disp_finde", $aRegistro["disp_finde"]));
			  		?><td colspan="4">&nbsp;</td>
                </tr>
                <tr class="formulario-textbox">
                  <td height="18" class="formulario-etiquetas">Libreta sanitaria: </td>
                  <?php
					print(EnumRadio("postulantenotas", "libreta_sanitaria", $aRegistro["libreta_sanitaria"]));
			  		?><td colspan="4">&nbsp;</td>
                </tr>
                <tr class="formulario-textbox">
                  <td height="18" class="formulario-etiquetas">Registro:</td>
                  <?php
					print(EnumRadio("postulantenotas", "registro", $aRegistro["registro"]));
			  		?><td colspan="2">&nbsp;</td>
                </tr>
              </table></td>
            </tr>
            <tr>
              <td height="30" colspan="2" align="center"><img src="images/espacio.gif" width="1" height="1" />
                      <input name="BTN_Guardar" type="submit" id="BTN_Guardar" value="Guardar" />
                      <input name="BTN_Cancelar" type="reset" id="BTN_Cancelar" value="Cancelar" onclick="history.back();" /></td>
            </tr>
          </table>
        </form></td>
        <td width="6" class="formulario-contenido-final"><img src="images/espacio.gif" width="1" height="1" /></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="20"><img src="images/formulario-pie-inicio.jpg" width="20" height="40" /></td>
        <td class="formulario-pie-bg"><img src="images/espacio.gif" width="1" height="1" /></td>
        <td width="20"><img src="images/formulario-pie-final.jpg" width="20" height="40" /></td>
      </tr>
    </table></td>
  </tr>
</table>
