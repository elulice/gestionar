<?php
$min_id = 'tickets.php';
$cDB = new BD();
$query = "SELECT MIN(id) as min_id FROM all_tickets WHERE visible = 'S'";
$oResultado = $cDB->Seleccionar($query);
while ($aRegistro = $cDB->RetornarFila($oResultado)) {

    $min_id_href = "tickets.edit.php?id=" . $aRegistro['min_id'];
}
?>
<div class="grid_16">
    <div id="tabs">
        <div class="container">
            <ul>
                <?php
                if (!isset($seccion))
                    $seccion = "usuarias";

                switch ($seccion) {

                    case "usuarias" :
                        ?>
                        <li><a href="usuarias.php" class="current"><span>Listado</span></a></li>
                        <?php
                        break;

                    case "pedidos" :
                        ?>
                        <li><a href="pedidos.php" class="current"><span>Listado</span></a></li>
                        <?php
                        break;

                    case "agenda" :
                        ?>
                        <li><a href="agenda.php" class="current"><span>Principal</span></a></li>
                        <?php
                        break;

                    case "curriculums" :
                        ?>
                        <li><a href="curriculums.php" class="current"><span>Principal</span></a></li>
                        <?php
                        break;

                    case "TicketsP" :
                        ?>
                        <li><a href="tickets.php" <?php if ($titulo == "Tickets") { ?>class="current" <?php } ?>><span>Principal</span></a></li>
                        <li><a href="" <?php if ($titulo == "Ticket Nro. ") { ?>class="current" <?php } ?>><span>Ticket</span></a></li>
                        <!--<li><a href="showReportes.php" <?php // if ($titulo == "Reportes") {  ?>class="current" <?php // }  ?>><span>Reportes</span></a></li>-->
                        <?php
                        break;

                    case "trayectoria" :
                        ?>
                        <li><a href="trayectoria.php" class="current"><span>Listado</span></a></li>
                        <?php
                        break;

                    case "noticias" :
                        ?>
                        <li><a href="noticias.php" class="current"><span>Listado</span></a></li>
                        <?php
                        break;

                    case "opciones" :
                        ?>
                        <li><a href="opciones.php" class="current"><span>Principal</span></a></li>
                        <?php /*
                          <li><a href="generales.php?sub=paises" <?php if ($sub == "paises") echo "class='current'"; ?>><span>PaÃ­ses</span></a></li>
                          <li><a href="generales.php?sub=provincias" <?php if ($sub == "provincias") echo "class='current'"; ?>><span>Provincias</span></a></li>
                          <li><a href="generales.php?sub=areas" <?php if ($sub == "areas") echo "class='current'"; ?>><span>Ã?reas</span></a></li>
                          <li><a href="generales.php?sub=contratacion" <?php if ($sub == "contratacion") echo "class='current'"; ?>><span>ContrataciÃ³n</span></a></li>
                          <li><a href="generales.php?sub=estudios" <?php if ($sub == "estudios") echo "class='current'"; ?>><span>Estudios</span></a></li>
                          <li><a href="generales.php?sub=sucursales" <?php if ($sub == "sucursales") echo "class='current'"; ?>><span>Sucursales</span></a></li>
                         */ ?>
                        <?php
                        break;

                    case "usuarios" :
                        ?>
                        <li><a href="usuarios.php" class="current"><span>Listado</span></a></li>
                        <?php
                        break;
                }
                ?>
            </ul>
            <div class="menu_escritorio">
                <a href="escritorio.php" class="link_escritorio">Escritorio</a>
            </div>
        </div>
    </div>
</div>

<?php
