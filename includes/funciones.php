<?php

session_start();
define("SITIO", "Gestion.Ar");
define("NODATA", "Sin especificar");
define("DEFSELECT", "-");

if (!function_exists("htmlspecialchars_decode")) {

    function htmlspecialchars_decode($pCadena) {
        return strtr($pCadena, array_flip(get_html_translation_table(HTML_SPECIALCHARS)));
    }

}

function get_fecha($value) {
    $fecha = FALSE;

    if (!empty($value)) {
        if (($fecha = strtotime(str_replace("/", "-", $value))) !== FALSE)
            $fecha = date("Y-m-d", $fecha);
    }
    return $fecha;
}

//	function arrSep($v1,$v2)
//	{
//		return $v1 . "," . $v2;
//	}
//	
//	function arrToString($pArreglo)
//	{
//		return array_reduce($pArreglo, "arrSep");
//	}

function edad($fecha_nac) {
    list($anio, $mes, $dia) = explode("-", $fecha_nac);
    $anio_dif = date("Y") - $anio;
    $mes_dif = date("m") - $mes;
    $dia_dif = date("d") - $dia;
    if ($dia_dif < 0 || $mes_dif < 0)
        $anio_dif--;
    return $anio_dif;
}

function ReemplazarCaracteres($pCadena, $pXML = false, $pBR = false) {
    if ($pXML) {
        $pCadena = preg_split("//", $pCadena, -1, PREG_SPLIT_NO_EMPTY);
        for ($lJ = 0; $lJ < count($pCadena); $lJ++) {
            $lDecimal = ord($pCadena[$lJ]);
            if ($lDecimal > 127)
                $pCadena[$lJ] = "&#" . $lDecimal . ";";
        }
        $pCadena = implode("", $pCadena);
    }
    else {
        $pCadena = htmlentities($pCadena, ENT_NOQUOTES, "ISO-8859-1");
        $pCadena = htmlspecialchars_decode($pCadena, ENT_NOQUOTES);
        if ($pBR)
            $pCadena = str_replace("\n", "<br />", $pCadena);
    }
    return $pCadena;
}

function GenerarOptions($pSQL, $pValor = 0, $pInicializada = false, $pLeyenda = "", $pHTML = true, $pWhere = "") {
    if ($pInicializada) {
        if ($pHTML)
            if ($pWhere == "usuarios") {
                $sOpciones = "<option value=\"-1\">" . $pLeyenda . "</option>";
            } else {
                $sOpciones = "<option value=\"0\">" . $pLeyenda . "</option>";
            } else
        if ($pWhere == "usuarios") {
            $sOpciones = "-1:|:" . $pLeyenda;
        } else {
            $sOpciones = "0:|:" . $pLeyenda;
        }
    }
    $cBD = new BD();
    $oResultado = $cBD->Seleccionar($pSQL);
    while ($aRegistro = $cBD->RetornarFila($oResultado)) {
        if ($pHTML) {
            $sSeleccionado = (($aRegistro[0] == $pValor) ? " selected=\"selected\"" : "");
            $sOpciones .= "<option value=\"" . $aRegistro[0] . "\"" . $sSeleccionado . ">";

            $aRegistro[1] = str_replace('&ntilde;', 'ñ', $aRegistro[1]);
            $aRegistro[1] = str_replace('&Ntilde;', 'Ñ', $aRegistro[1]);
            $sOpciones .= htmlentities(utf8_decode($aRegistro[1])) /* ReemplazarCaracteres($aRegistro[1]) */ . "</option>";
        } else {
            if ($sOpciones <> "")
                $sOpciones .= ":|:";
            $sOpciones .= $aRegistro[0] . ":|:" . $aRegistro[1];
        }
    }
    return $sOpciones;
}

function EnumOptions($pTabla, $pColumna, $pValor = 0) {
    $pSQL = "SHOW COLUMNS FROM " . $pTabla . " LIKE '" . $pColumna . "' ";
    $cBD = new BD();
    $oResultado = $cBD->Seleccionar($pSQL, true);

    $oResultado = explode("','", preg_replace("/(enum|set)\('(.+?)'\)/", "\\2", $oResultado[1]));

    foreach ($oResultado AS $key) {
        $sSeleccionado = (($key == $pValor) ? " selected=\"selected\"" : "");
        $sOpciones .= "<option value=\"" . $key . "\"" . $sSeleccionado . ">";
        $sOpciones .= $key . "</option>";
    }
    return $sOpciones;
}

function EnumRadio($pTabla, $pColumna, $pValor = 0) {
    $pSQL = "SHOW COLUMNS FROM " . $pTabla . " LIKE '" . $pColumna . "' ";
    $cBD = new BD();
    $oResultado = $cBD->Seleccionar($pSQL, true);

    $oResultado = explode("','", preg_replace("/(enum|set)\('(.+?)'\)/", "\\2", $oResultado[1]));


//<input name="portada" type="radio" value="S" <?php if($aRegistro["portada"] == "S") { 

    foreach ($oResultado AS $key) {
        $sSeleccionado = (($key == $pValor) ? " checked=\"checked\"" : "");
        $sOpciones .= "<td><input name='" . $pColumna . "' type='radio' value='" . $key . "' " . $sSeleccionado . "></td>";
        $sOpciones .= "<td width='130'>" . ReemplazarCaracteres($key) . " </td>";
    }
    return $sOpciones;
}

function GenerarOptionsArray($pArreglo, $pValor = 0, $pInicializada = false, $pLeyenda = "", $pHTML = true) {
    if ($pInicializada) {
        if ($pHTML)
            $sOpciones = "<option value=\"0\">" . $pLeyenda . "</option>";
        else
            $sOpciones = "0:|:" . $pLeyenda;
    }

    foreach ($pArreglo as $aRegistro) {
        if ($pHTML) {
            $sSeleccionado = (($aRegistro[0] == $pValor) ? " selected=\"selected\"" : "");
            $sOpciones .= "<option value=\"" . $aRegistro[0] . "\"" . $sSeleccionado . ">";
            $sOpciones .= ReemplazarCaracteres($aRegistro[1]) . "</option>";
        } else {
            if ($sOpciones <> "")
                $sOpciones .= ":|:";
            $sOpciones .= $aRegistro[0] . ":|:" . $aRegistro[1];
        }
    }
    return $sOpciones;
}

function RetornarTipoUsuario() {
    return $_SESSION["tipo_usuario"];
}

function RetornarIdEmpresa() {
    return $_SESSION["idempresa"];
}

function EvitarCache() {
    header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
    header("Cache-Control: no-store, no-cache, must-revalidate");
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");
}

function fechaHoy() {
    /* OBTENGO LA CADENA QUE REPRESENTA EL DIA PERO EN INGLES */
    $dia = date("l");
    if ($dia == "Monday")
        $dia = "Lunes";
    if ($dia == "Tuesday")
        $dia = "Martes";
    if ($dia == "Wednesday")
        $dia = "Miércoles";
    if ($dia == "Thursday")
        $dia = "Jueves";
    if ($dia == "Friday")
        $dia = "Viernes";
    if ($dia == "Saturday")
        $dia = "Sabado";
    if ($dia == "Sunday")
        $dia = "Domingo";

    /* EL NUMERO DEL DIA */
    $dia2 = date("d");

    /* OBTENGO EL STRING DEL MES */
    $mes = date("F");
    if ($mes == "January")
        $mes = "Enero";
    if ($mes == "February")
        $mes = "Febrero";
    if ($mes == "March")
        $mes = "Marzo";
    if ($mes == "April")
        $mes = "Abril";
    if ($mes == "May")
        $mes = "Mayo";
    if ($mes == "June")
        $mes = "Junio";
    if ($mes == "July")
        $mes = "Julio";
    if ($mes == "August")
        $mes = "Agosto";
    if ($mes == "September")
        $mes = "Setiembre";
    if ($mes == "October")
        $mes = "Octubre";
    if ($mes == "November")
        $mes = "Noviembre";
    if ($mes == "December")
        $mes = "Diciembre";

    /* FINALMENTE EL AÑO */
    $ano = date("Y");

    /* RETORNAMOS LA FECHA ENTERA */
    return "$dia, $dia2 de $mes del $ano";
}

function myTruncate($string, $limit, $break = ".", $pad = "�") {
// return with no change if string is shorter than $limit 
    if (strlen($string) <= $limit)
        return $string;
// is $break present between $limit and the end of the string? 
    if (false !== ($breakpoint = strpos($string, $break, $limit))) {
        if ($breakpoint < strlen($string) - 1) {
            $string = substr($string, 0, $breakpoint) . $pad;
        }
    }
    return $string;
}

function replaceSpecialChar($text) {
    $filter = trim($text); //aqui asiganmos el valor que obtenemos del formulario a esta variable
    $filter = ereg_replace('\"+', '', $filter); //de esta manera eliminamos inmediatamente los espacios en blanco que pudieran estar al principio y al final de la variable
    $filter = str_replace("\'", "", $filter); //aca eliminamos las comillas simples (')
    $filter = str_replace('\"', "", $filter); //eliminamos las comillas dobles (")
    $filter = stripslashes($filter); //eliminamos las diagonales invertidas que quedaron al eliminar las comiilas (\)
    $filter = explode("\\", $filter);
    $filter = implode("", $filter); //con esto por fin eliminamos las diagonales invertidas que quedaban al principio y al final cuando el usuario introducia comillas en estas posiciones, de esta manera ya tenemos el titulo sin comillas
    $filter = strtolower($filter); //Simplemente convierte a minisculas, esto ya es para la url
    $filter = trim($filter);
    $filter = ereg_replace(' +', '-', $filter); //reemplazamos los espacios en blanco por guiones medio (-)
    $b = array("á", "é", "í", "ó", "u", "ä", "ë", "ï", "ö", "ü", "à", "è", "ì", "ò", "ù", "ñ", " ", ",", ".", ";", ":", "¡", "!", "¿", "?", "/", "*", "+", "", "{", "}", "�", "�", "�", "�", "�", "�", "^", "#", "|", "�", "=", "[", "]", "<", ">", "`", "(", ")", "&", "%", "$", "�", "@", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "~", "�", "�", "�", "�", "�", "", "\\"); //Estos arreglos nos ayudaran a reemplazar todos estos simbolos por letras que no causen problemas
    $c = array("a", "e", "i", "o", "u", "a", "e", "i", "o", "u", "a", "e", "i", "o", "u", "n", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "a", "e", "i", "o", "u", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "A", "E", "I", "O", "U", "A", "E", "I", "O", "U", "A", "E", "I", "O", "U", "", "A", "E", "I", "O", "U", " ", "");
    $filter = str_replace($b, $c, $filter); //simplemente hacemos el reemplazo
    return $text;
}

//function time_elapsed_string($origintime, $responsedtime)
//{
//    if (trim($responsedtime) != "") {
//        $otime = strtotime($origintime);
//        $rtime = strtotime($responsedtime);
//
//        $etime = $rtime - $otime;
//
//        if ($etime < 1) {
////            return '0 segundos';
//            return '0';
//        }
//
//        $a = array(12 * 30 * 24 * 60 * 60 => 'año',
//            30 * 24 * 60 * 60 => 'mes',
//            24 * 60 * 60 => 'día',
//            60 * 60 => 'hora',
//            60 => 'minuto',
//            1 => 'segundo'
//        );
//
//        foreach ($a as $secs => $str) {
//            $d = $etime / $secs;
//            if ($d >= 1) {
//                $r = round($d);
//                return $r . ' ' . $str . ($r > 1 ? 's' : '');
//            }
//        }
//    }
//}

function time_elapsed_string($etime) {

    if ($etime < 1) {
//            return '0 segundos';
        return '0';
    }

    $a = array(12 * 30 * 24 * 60 * 60 => 'año',
        30 * 24 * 60 * 60 => 'mes',
        24 * 60 * 60 => 'día',
        60 * 60 => 'hora',
        60 => 'minuto',
        1 => 'segundo'
    );

    foreach ($a as $secs => $str) {
        $d = $etime / $secs;
        if ($d >= 1) {
            $r = round($d);
            return $r . ' ' . $str . ($r > 1 ? 's' : '');
        }
    }
}

?>