<div id="portlets">
    <div id="reclamos_admin_contenido" style="width: inherit; height: inherit;">
        <img src="images/loading.gif" style="float: left; margin-left: 198px; margin-top: 61px;" />
        <script type="text/javascript">
            getFormReclamos(<?php echo ((!empty($_GET['idregistro'])) ? $_GET['idregistro'] : 'null'); ?>, <?php echo ((!empty($_GET['polizaid'])) ? $_GET['polizaid'] : 'null'); ?>);
        </script>
    </div>
    <div id="enviarPorMail" style="display:none">
        <form action="enviar_email.php" method="post" enctype="multipart/form-data" onSubmit="return false;" name="formEnviarMail" id="formEnviarMail" >
            <div class="form-cuadros" style="clear:both; width:340px;">
                <label for="invite" class="form-label">Asunto:</label>
                <input type="text" name="asunto" id="emailAsunto" class="form-contacto-text smallInput" style="width: 223px; margin-bottom:10px;" />
                <div id="usuarios_mails"></div>
                <label for="invite" class="form-label" style="margin-right: 18px;">Para:</label>
                <input type="text" name="emails" id="autocompletarEmails" class="form-contacto-text smallInput" style="width: 223px; margin-bottom:10px;" />
                <div class="form-cuadros"><label for="fullbody" class="form-label">Mensaje:</label>
                    <textarea name="mensaje" id="fullbody" class="form-contacto-text smallInput" style="width: 330px; height: 110px;" ></textarea>
                </div>
                <label for="invite" class="form-label">Adjuntar:</label>
                <input type="radio" name="adjunto" value="xls" /><img src="images/excel.png" />
                <input type="radio" name="adjunto" value="pdf" /><img src="images/pdf.png" />
                <input type="hidden" id="email_tipo" name="tipo" value="poliza" />
                <input type="hidden" id="email_id" name="id" value="" />
            </div>
        </form>
    </div>
</div>
