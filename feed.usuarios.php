<?php
require_once("clases/framework-1.0/class.bd.php");
require_once("clases/phppaging/PHPPaging.lib.php");
require_once("includes/funciones.php");

switch ($_GET["objeto"]) {

    case "principal" : usuarios_principal();
        break;
    case "detalle" : usuarios_detalle();
        break;
    case "usuario" : usuario_existe();
        break;
}

function usuario_existe() {

    $user = $_GET['username'];
    $query2 = "SELECT * FROM `miembroempresa` WHERE `MEmpUsuario` = '$user'";
    $db2 = new BD();
    $row2 = $db2->Seleccionar($query2, true);
    if (!$row2['MEmpNro']) {
        echo 0;
    } else {
        echo 1;
    }
}

function usuarios_principal() {

    $query = "SELECT MEmpNro AS idmiembro,MEmpApellido AS apellido, MEmpNombres AS nombres, ";
    $query.= "TMiembDescrip AS tipo, MEmpUsuario AS usuario, UniNombre AS unidad ";
    $query.= "FROM miembroempresa me ";
    $query.= "LEFT JOIN user_type ON me.PerNro = user_type.idusuario ";
    $query.= "LEFT JOIN tipomiembro ON user_type.tipo_id = TMiembNro  ";
    $query.= "LEFT JOIN unidadorg uo ON me.UniNro = uo.UniNro  ";
    $query.= "WHERE 1=1 ";
//    $query.= "WHERE 1=1 GROUP BY me.PerNro ";

    if ($_GET["nombres"] != "") {
        $query .= "AND MEmpNombres LIKE '%" . $_GET["nombres"] . "%' ";
    }

    if ($_GET["apellido"] != "") {
        $query .= "AND MEmpApellido LIKE '%" . $_GET["apellido"] . "%' ";
    }

    if ((int) $_GET["sucursal"] != "" && (int) $_GET["sucursal"] != 0) {
        $query .= "AND me.UniNro = " . (int) $_GET["sucursal"] . " ";
    }

    if (RetornarTipoUsuario() == 1)
        $query.= "AND me.EmpNro = " . $_SESSION["idempresa"] . " ";
    else
        $query.= "AND MEmpNro = " . $_SESSION["idmiembro"] . " ";
    $query.= "ORDER BY apellido ASC, nombres ASC ";
    ?>
    <div style="height:285px;">
        <table width="833" cellpadding="0" cellspacing="0" style="margin:12px 0 0 12px;" id="box-table-a">
            <thead>
                <tr>
                    <th width="100" scope="col"><span style="color:#c60; font-weight:bold;">Apellido</span></th>
                    <th width="100" scope="col"><span style="color:#c60; font-weight:bold;">Nombres</span></th>
                    <th width="100" scope="col"><span style="color:#c60; font-weight:bold;">Nombre Usuario</span></th>
                    <th width="100" scope="col"><span style="color:#c60; font-weight:bold;">Tipo</span></th>
                    <th width="100" scope="col"><span style="color:#c60; font-weight:bold;">Sucursales</span></th>
                    <th width="100" scope="col"><span style="color:#c60; font-weight:bold;">Opciones</span></th>
                </tr>
            </thead>
            <tbody>
                <?php
                $cBD = new BD();
                $cBD->Conectar();
                $paging = new PHPPaging($cBD->RetornarConexion());
                $paging->agregarConsulta($query);
                $paging->linkClase("navPage");
                $paging->porPagina(5);
                $paging->ejecutar();

                while ($aRegistro = $paging->fetchResultado()) {
                    if ($aRegistro["tipo"] == "") {
                        $aRegistro["tipo"] = "Asignar";
                    }
                    ?>
                    <tr height="20">
                        <td width="150" style="padding:8px;"><?php print($aRegistro["apellido"]); ?></td>
                        <td width="170" style="padding:8px;"><?php print($aRegistro["nombres"]); ?></td>
                        <td width="230" style="padding:8px;"><?php print($aRegistro["usuario"]); ?></td>
                        <td width="120" style="padding:8px;"><?php print($aRegistro["tipo"]); ?></td>
                        <td width="100" style="padding:8px;"><?php print($aRegistro["unidad"]); ?></td>
                        <td height="30" style="padding:8px;">
                            <img src="images/icons/page_edit.png" alt="Editar" title="Editar" style="cursor:pointer;" onclick="editar_usuario(<?php echo $aRegistro["idmiembro"]; ?>);" />
                            <img src="images/icons/page_delete.png" alt="Eliminar" title="Eliminar" style="cursor:pointer;" onclick="eliminar_usuario(<?php echo $aRegistro["idmiembro"]; ?>);" />
                        </td>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
    </div>
    <div class="pagination"><?php echo $paging->fetchNavegacion(); ?></div>
    <?php
}

function usuarios_detalle() {

    $id_usuario = (int) $_GET["id_usuario"];
    if ($id_usuario < 1)
        exit("");

    $query = "SELECT me.MEmpNro, me.PerNro, me.MEmpApellido, me.MEmpNombres, me.MEmpEmail, 
       me.MEmpUsuario, p.password AS MEmpClave, p.password AS MEmpClaveRepeat, me.MEmpDetalle, 
       me.UniNro, user_type.tipo_id as MEmpAdmin, user_reports.reportes AS reportes
       FROM miembroempresa me
       LEFT JOIN user_type ON me.PerNro = user_type.idusuario
       LEFT JOIN user_reports ON me.PerNro = user_reports.idusuario
       INNER JOIN passwords p ON(me.PerNro = p.user_id)
       WHERE MEmpNro=$id_usuario";

    $db = new BD();
    $db->Conectar();

    $row = $db->Seleccionar($query, TRUE);
    echo json_encode($row);
}
?>
