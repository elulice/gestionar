<?php

require_once("clases/framework-1.0/class.bd.php");
require_once("clases/phppaging/PHPPaging.lib.php");
require_once("includes/funciones.php");

switch ($_GET["objeto"]) {

   case "principal"	: pedidos_principal(); break;
   case "detalle"	: pedidos_detalle(); break;
   case "postulantes"	: pedidos_postulantes(); break;

}

function pedidos_principal() {

   $desde_dialog = FALSE;
   if (isset($_GET["dialog"])) $desde_dialog = TRUE;

   $where = "";

   if (($area = (int) $_GET["cbo_area"]) != 0) $where.= " AND oferta.AreNro = $area ";
   if (($cliente = $_GET["txt_usuaria"]) != "") $where.= " AND CliRsocial LIKE '%$cliente%' ";
   if (($ejecutivo = (int) $_GET["cbo_ejecutivo"]) != 0) $where .= " AND cliente.CliNro = $ejecutivo ";

   $situacion = $_GET["cbo_situacion"];
   if (!empty($situacion)) $where.= " AND EOfeValor = '$situacion' ";

   if (($sucursal = (int) $_GET["cbo_sucursales"]) != 0) $where.= " AND NodNro = $sucursal ";
   if (($selector = (int) $_GET["cbo_selector"]) != 0) $where.= " AND oferta.MEmpNro = $selector ";

   $fecha_desde = get_fecha($_GET["dp_fecha_desde"]);
   $fecha_hasta = get_fecha($_GET["dp_fecha_hasta"]);
   if ($fecha_desde != FALSE AND $fecha_hasta != FALSE)
      $where.= " AND OfeFechAlta BETWEEN '$fecha_desde' AND '$fecha_hasta' ";
   else if ($fecha_desde != FALSE)
      $where.= " AND OfeFechAlta > '$fecha_desde' ";
   else if ($fecha_hasta != FALSE)
      $where.= " AND OfeFechAlta < '$fecha_hasta' ";

   $query = "SELECT oferta.OfeNro AS idpropuesta, PueNom, OfeFechAlta, eo.EOfeValor, OfeReferencia, OfeCantPost, ";
   $query .= "OfeLocalidad, AreNom, CliRsocial FROM oferta
      LEFT JOIN estadooferta ON estadooferta.OfeNro = oferta.OfeNro
      LEFT JOIN puesto ON oferta.PueNro = puesto.PueNro
      LEFT JOIN area ON area.AreNro = oferta.AreNro
      LEFT JOIN ofertascliente oc ON oc.OfeNro = oferta.OfeNro
      LEFT JOIN cliente ON oc.CliNro = cliente.CliNro
      LEFT JOIN estadooferta AS eo ON oferta.OfeNro = eo.OfeNro
      WHERE 1 $where";

   if ($desde_dialog == TRUE) {
      $id_postulante = (int) $_GET["id_postulante"];
      if ($id_postulante > 0)
	 $query .= " AND oferta.OfeNro NOT IN (SELECT OfeNro FROM postulacion WHERE PerNro=$id_postulante) ";
   }

   $query .= " ORDER BY OfeFechAlta DESC, idpropuesta DESC";

   ?>
   <?php if ($desde_dialog == FALSE) { ?>
   <div style="height:210px;">
   <table width="853" cellpadding="0" cellspacing="0" style="margin:12px 0 0 12px;" id="box-table-a">
   <?php } else { ?>
   <div style="height:310px;overflow-y:auto;overflow-x:hidden;border:1px solid #dddddd;">
   <table width="710" cellpadding="0" cellspacing="0" style="margin:12px 0 0 12px;" id="box-table-a">
   <?php } ?>
      <thead>
	 <tr>
	    <?php if ($desde_dialog == FALSE) { ?>
	    <th width="50" scope="col"><span style="color:#c60; font-weight:bold;">Fecha</span></td>
	    <th width="70" scope="col"><span style="color:#c60; font-weight:bold;">Ref</span></td>
	    <th width="150" scope="col"><span style="color:#c60; font-weight:bold;">Usuaria</span></td>
	    <th width="150" scope="col"><span style="color:#c60; font-weight:bold;">Área</span></td>
	    <th width="150" scope="col"><span style="color:#c60; font-weight:bold;">Puesto</span></td>
	    <th width="20" scope="col"><span style="color:#c60; font-weight:bold;">Vac</span></td>
	    <th width="20" scope="col"><span style="color:#c60; font-weight:bold;">Post</span></td>
	    <th width="50" scope="col"><span style="color:#c60; font-weight:bold;">Situación</span></td>
	    <th width="90" scope="col"><span style="color:#c60; font-weight:bold;">Opciones</span></td>
	    <?php } else { ?>
	    <th width="50" scope="col"><span style="color:#c60; font-weight:bold;">Fecha</span></td>
	    <th width="200" scope="col"><span style="color:#c60; font-weight:bold;">Usuaria</span></td>
	    <th width="200" scope="col"><span style="color:#c60; font-weight:bold;">Área</span></td>
	    <th width="80" scope="col"><span style="color:#c60; font-weight:bold;">Puesto</span></td>
	    <th width="50" scope="col"><span style="color:#c60; font-weight:bold;">Vac</span></td>
	    <th width="40" scope="col"><span style="color:#c60; font-weight:bold;"></span></td>
	    <?php } ?>
	 </tr>
      </thead>
      <tbody>
      <?php

      $cBD = new BD();
      $cBD->Conectar();
      $paging = new PHPPaging($cBD->RetornarConexion());
      $paging->agregarConsulta($query);
      $paging->linkClase("navPage");

      if ($desde_dialog == FALSE) $paging->porPagina(3);
      else $paging->porPagina(10);

      $paging->ejecutar();

      while($aRegistro = $paging->fetchResultado()) {
	     $sPosicion = (($sPosicion == "1") ? "2" : "1");
   ?>
      <tr>
	 <?php if ($desde_dialog == FALSE) { ?>
	 <td style="padding:8px;text-align:center;"><?php print(date("d/m/Y", strtotime($aRegistro["OfeFechAlta"]))); ?>&nbsp;</td>
	 <td style="padding:8px;"><?php print($aRegistro["OfeReferencia"]); ?>&nbsp;</td>
	 <td style="padding:8px;"><?php print($aRegistro["CliRsocial"]); ?>&nbsp;</td>
	 <td style="padding:8px;"><?php print($aRegistro["AreNom"]); ?>&nbsp;</td>
	 <td style="padding:8px;"><?php print($aRegistro["PueNom"]); ?>&nbsp;</td>
	 <td style="padding:8px;"><?php print($aRegistro["OfeCantPost"]); ?>&nbsp;</td>
	 <td style="padding:8px;"><?php print($aRegistro["postulados"]); ?>&nbsp;</td>
	 <td style="padding:8px;"><?php print($aRegistro["EOfeValor"]); ?>&nbsp;</td>
	 <td style="padding:8px;">
	 <img src="images/icons/page_edit.png" alt="Editar" title="Editar" style="cursor:pointer;" onclick="editar_oferta(<?php echo $aRegistro["idpropuesta"]; ?>);" />
	 <img src="images/icons/user-16.png" alt="Ver Postulantes" title="Ver Postulantes" style="cursor:pointer" onclick="mostrar_postulantes(<?php echo $aRegistro["idpropuesta"]; ?>);" />
	 <img src="images/icons/link.png" alt="Asignar a Oferta" title="Asignar a Oferta" onclick="mostrar_postulantes2(<?php echo $aRegistro["idpropuesta"]; ?>);" style="cursor:pointer;" />
	 <img src="images/icons/page_delete.png" alt="Eliminar" title="Eliminar" style="cursor:pointer" onclick="eliminar_oferta(<?php echo $aRegistro["idpropuesta"]; ?>);" />
	 </td>
	 <?php } else { ?>
	 <td style="padding:8px;text-align:center;"><?php print(date("d/m/Y", strtotime($aRegistro["OfeFechAlta"]))); ?>&nbsp;</td>
	 <td style="padding:8px;"><?php print($aRegistro["CliRsocial"]); ?>&nbsp;</td>
	 <td style="padding:8px;"><?php print($aRegistro["AreNom"]); ?>&nbsp;</td>
	 <td style="padding:8px;"><?php print($aRegistro["PueNom"]); ?>&nbsp;</td>
	 <td style="padding:8px;"><?php print($aRegistro["OfeCantPost"]); ?>&nbsp;</td>
	 <td style="padding:8px;">
	 <img src="images/icons/add_small.png" alt="Asignar a Oferta" title="Asignar a Oferta" style="cursor:pointer;" onclick="asignar_postulante_oferta(<?php echo $aRegistro["idpropuesta"]; ?>);" />
	 </td>
	 <?php } ?>
      </tr>
   <?php
      }
   ?>
      </tbody>
   </table>
   </div>
   <div class="pagination"><?php echo $paging->fetchNavegacion(); ?></div>
   <?php

}

function pedidos_detalle() {

   $id_oferta = (int) $_GET["id_oferta"];

   if ($id_oferta > 0) {

      $sSQL = "SELECT o.OfeTitulo, DATE_FORMAT(o.OfeFechAlta, '%d/%m/%Y') AS OfeFechAlta, ";
      $sSQL .= "DATE_FORMAT(o.OfeValidez, '%d/%m/%Y') AS OfeValidez, o.OfeVisible, ";
      $sSQL .= "oc.CliNro, o.OFePedidoPor, o.OfeDireccion, o.OfeEMail, o.OfeURL, ";
      $sSQL .= "o.OfeReferencia, o.AreNro, o.PueNro, o.ZonNro, ";
      $sSQL .= "o.OfeOferta, o.OfeCantPost, o.TPeNro, ";
      $sSQL .= "o.OfeDeEdad, o.OfeHtaEdad, o.OfeSexo, ";
      $sSQL .= "o.OfeEducacion, o.OfeProvincia, o.OfeLocalidad, ";
      $sSQL .= "o.OfeIngles, o.OfeExpAnios, o.OfeRemunerac, ";
      $sSQL .= "o.OfeDescTar, o.OfeClasificacion, ";
      $sSQL .= "o.OfeVisible, o.OfeDestacada, o.OfeEnviaMail, ";
      $sSQL .= "eo.EOfeValor, o.OfeDescSel, o.NodNro, ";
      $sSQL .= "me.MEmpNro, me.MEmpApellido, me.MEmpNombres, me.MEmpEmail, c.CliRsocial as nombrecliente ";

      $sSQL .= "FROM oferta o ";
      $sSQL .= "LEFT JOIN ofertascliente oc ON o.OfeNro = oc.OfeNro ";
      $sSQL .= "LEFT JOIN cliente c ON oc.CliNro = c.CliNro ";
      $sSQL .= "LEFT JOIN area a ON o.AreNro = a.AreNro ";
      $sSQL .= "LEFT JOIN puesto p ON p.PueNro = o.PueNro ";
      $sSQL .= "LEFT JOIN estadooferta eo ON eo.OfeNro = o.OfeNro ";
      $sSQL .= "LEFT JOIN miembroempresa me ON me.MEmpNro = o.MEmpNro ";
      $sSQL .= "WHERE o.OfeNro = " . $id_oferta;

      $db = new BD();
      $db->Conectar();
      $row = $db->Seleccionar($sSQL, TRUE);

   } else $row = FALSE;

   require_once("inc.pedidos.detalle.php");
}

function pedidos_postulantes() {

   $id_oferta = (int) $_GET["id_oferta"];
   if ($id_oferta == 0) exit("");

   $query = "SELECT id_postulacion, CONCAT(PerApellido, ', ', PerNombres) AS postulante, PerTelefono, PosFecha, PEsDescrip
      FROM postulacion LEFT JOIN persona USING(PerNro) LEFT JOIN postulacionestado USING(PEsNro)
      WHERE OfeNro=$id_oferta ORDER BY postulante";

   $db = new BD();
   $db->Conectar();

   $paging = new PHPPaging($db->RetornarConexion());
   $paging->agregarConsulta($query);
   $paging->linkClase("navPage");
   $paging->porPagina(5);
   $paging->ejecutar();

   ?>
   <div style="height:230px;">
      <table width="600" cellpadding="0" cellspacing="0" style="margin:12px 0 0 12px;" id="box-table-a">
	 <thead>
	    <tr>
	       <th width="280" scope="col"><span style="color:#c60; font-weight:bold;">Nombre</span></td>
	       <th width="140" scope="col"><span style="color:#c60; font-weight:bold;">Teléfono</span></td>
	       <th width="100" scope="col"><span style="color:#c60; font-weight:bold;">Situación</span></td>
	       <th width="80" scope="col"><span style="color:#c60; font-weight:bold;">Opciones</span></td>
	    </tr>
	 </thead>
	 <tbody>
   <?php

      while($aRegistro = $paging->fetchResultado())
      {
      ?>
	    <tr>
	       <td style="padding:8px;"><?php echo mb_convert_case($aRegistro["postulante"], MB_CASE_TITLE); ?>&nbsp;</td>
	       <td style="padding:8px;"><?php echo $aRegistro["PerTelefono"]; ?>&nbsp;</td>
	       <td style="padding:8px;"><?php echo $aRegistro["PEsDescrip"]; ?>&nbsp;</td>
	       <td style="padding:8px;">
	       <img src="images/icons/page_delete.png" alt="Eliminar Postulante" title="Eliminar Postulante" style="cursor:pointer;" onclick="eliminar_postulante(<?php echo $aRegistro["id_postulacion"]; ?>);" />
	       </td>
	    </tr>
      <?php
      }

   ?>
	 </tbody>
      </table>
   </div>
   <div class="pagination"><?php echo $paging->fetchNavegacion(); ?></div> 
   <?php

}

?>
