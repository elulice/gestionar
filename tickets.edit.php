<?php

session_start();
$seccion = "TicketsP";
$titulo = "Ticket Nro. ";

$tab = "edit";

include("clases/framework-1.0/class.bd.php");

$BD = new BD();
$querySession = "SELECT * FROM all_tickets WHERE id = " . $_GET["id"];
$oRegis = $BD->Seleccionar($querySession);
$aRegis = $BD->RetornarFila($oRegis);

if ($_SESSION["tipo_usuario"] == 0) {
    if ($aRegis["responsable_id"] == $_SESSION["PerNro"]) {
        
    } else {
        $motivo = $_GET["id"] . "_Motivos: \n<br>";
        $motivo .= "- No eres el Responsable del ticket.\n<br>";
        if ($aRegis["sucursal_id"] == $_SESSION["empresa_id"]) {
            
        } else {
            $motivo .= "- La Sucursal del Ticket es distinta.";
            $_SESSION["error_auth"] = TRUE;
            $_SESSION["motivo"] = $motivo;
            header('Location: tickets.php');
        }
    }
}

include("includes/funciones.php");
require_once ('clases/phppaging/PHPPaging.lib.php');

include("inc.encabezado.php");
include("inc.tickets-nuevo.php");
include("inc.pie.php");
?>
