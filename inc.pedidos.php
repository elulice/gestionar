<div id="div_dialog_pedidos" style="display:none;"></div>
<?php require_once("inc.pedidos.postulantes.php"); ?>
<?php require_once("inc.pedidos.asignaciones.php"); ?>
		     <!-- < /Código de formulario > -->
		     <div style="margin:8px 0 0 18px;">
			<form id="frm_pedidos_listado_filtro" target="hidden_iframe" onsubmit="actualizar_pedidos_listado();">
			   <input type="submit" style="display:none;" />
			   <table width="796" cellspacing="3" style="margin-left:40px;">
			      <tbody>
				 <tr height="30">
				    <td width="230">
				       <div style="float:left;width:220px;">
					  <div class="form-label" style="width:80px;text-align:left;">Fecha Desde:</div>
					  <input type="text" class="smallInput" name="dp_fecha_desde" style="width:120px;text-align:center;" />
				       </div>
				    </td>
				    <td width="230">
				       <div style="float:left;width:220px;">
					  <div class="form-label" style="width:80px;text-align:left;">Fecha Hasta:</div>
					  <input type="text" class="smallInput" name="dp_fecha_hasta" style="width:120px;text-align:center;" />
				       </div>
				    </td>
				    <td width="230">
				       <div style="float:left;width:220px;">
					  <div class="form-label" style="width:80px;text-align:left;">Selector:</div>
					  <select class="smallInput" onchange="actualizar_pedidos_listado();" name="cbo_selector" style="width:130px;">
					  <?php
   $query = "SELECT MEmpNro, CONCAT(MEmpApellido, ' ', MEmpNombres) FROM miembroempresa WHERE MEmpAdmin = 0
      ORDER BY MEmpApellido ASC, MEmpNombres ASC";
   echo GenerarOptions($query, NULL, TRUE, DEFSELECT);
					  ?>
					  </select>
				       </div>
				    </td>
				 </tr>
				 <tr>
				    <td width="230">
				       <div style="float:left;width:220px;">
					  <div class="form-label" style="width:80px;text-align:left;">Sucursales:</div>
					  <select class="smallInput" onchange="actualizar_pedidos_listado();" name="cbo_sucursales" style="width:130px;">
					  <?php
   $query = "SELECT UniNro, UniNombre FROM unidadorg";
   echo GenerarOptions($query, NULL, TRUE, DEFSELECT);
					  ?>
					  </select>
				       </div>
				    </td>
				    <td width="230">
				       <div style="float:left;width:220px;">
					  <div class="form-label" style="width:80px;text-align:left;">Area:</div>
					  <select class="smallInput" onchange="actualizar_pedidos_listado();" name="cbo_area" style="width:130px;">
					  <?php
   $query = "SELECT AreNro, AreNom FROM area ORDER BY AreNom";
   echo GenerarOptions($query, NULL, TRUE, DEFSELECT);
					  ?>
					  </select>
				       </div>
				    </td>
				    <td width="230">
				       <div style="float:left;width:220px;">
					  <div class="form-label" style="width:80px;text-align:left;">Situación:</div>
					  <select class="smallInput" onchange="actualizar_pedidos_listado();" name="cbo_situacion" style="width:130px;">
					  <?php
   $query = "SELECT nombre, nombre FROM _ofertas_estados";
   echo GenerarOptions($query, NULL, TRUE, DEFSELECT);
					  ?>
					  </select>
				       </div>
				    </td>
				 </tr>
				 <tr>
				    <td width="230">
				       <div style="float:left;width:220px;">
					  <div class="form-label" style="width:80px;text-align:left;">Ejecutivo:</div>
					  <select class="smallInput" onchange="actualizar_pedidos_listado();" name="cbo_ejecutivo" style="width:130px;">
					  <?php
   $query = "SELECT MEmpNro, CONCAT(MEmpNombres, ' ', MEmpApellido) AS nombre
      FROM miembroempresa WHERE MEmpAdmin=2 ORDER BY nombre";
   echo GenerarOptions($query, NULL, TRUE, DEFSELECT);
					  ?>
					  </select>
				       </div>
				    </td>
				    <td width="230">
				       <div style="float:left;width:220px;">
					  <div class="form-label" style="width:80px;text-align:left;">Usuaria:</div>
					  <input type="text" class="smallInput" name="txt_usuaria" style="width:120px;" />
				       </div>
				    </td>
				    <td width="290">
				       <a class="button_notok" onclick="$('#frm_pedidos_listado_filtro').clearForm(); actualizar_pedidos_listado();" style="margin-top:3px;"><span>Limpiar Búsqueda</span></a>
				       <a class="button_ok" onclick="actualizar_pedidos_listado();" style="margin-top:3px;"><span>Buscar</span></a>
				    </td>
				 </tr>
			      </tbody>
			   </table>
			</form>
		     </div>
		     <div id="div_pedidos_listado" style="height:220px;">
			<img src="images/loading.gif" class="loading" />
		     </div>
