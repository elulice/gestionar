<?php
/*
  error_reporting(E_ALL);
  ini_set("display_errors", TRUE);
 */

require_once("clases/framework-1.0/class.bd.php");
require_once("clases/phppaging/PHPPaging.lib.php");
require_once("includes/funciones.php");
?>

<?php
switch ($_GET["objeto"]) {

    case "principal" : usuarias_principal();
        break;
    case "detalle" : usuarias_detalle();
        break;
    case "sucursales" : usuarias_sucursales();
        break;
    case "responsables" : usuarias_responsables();
        break;
    case "responsables_por_sector" : usuarias_responsables_por_sector();
        break;
    case "sector" : usuarias_sector();
        break;
    case "sector_por_sucursal" : usuarias_sector_por_sucursal();
        break;
    case "email" : usuarias_email();
        break;
}

/* inicio de funciones */

function usuarias_detalle()
{

    $id_cliente = (int) $_GET["id_cliente"];
    if ($id_cliente == 0)
        exit("");

    $query = "SELECT CliRsocial, CliContacto, CliTelefono, CliEmail, CliDomicilio,
	 CliCUIT, CliRubro, CliEjCta, CliEMailEjCta, DATE_FORMAT(CliFechaInicio, '%d/%m/%Y') AS CliFechaInicio
	 FROM cliente WHERE CliNro = $id_cliente";

    $db = new BD(TRUE);
    $db->Conectar();

    $res = $db->Seleccionar($query, TRUE);

    foreach ($res as $key => $value)
        if (is_numeric($key))
            unset($res[$key]);

    $json = json_encode($res);
    echo $json;
    ?>
    <?php
}

function usuarias_sucursales()
{
    $id_cliente = (int) $_GET["id_cliente"];
    if ($id_cliente == 0)
        exit("");

    $dbN = new BD(TRUE);
    $dbN->Conectar();

    $queryKnow = "SELECT cs.SucNro, uo.UniNombre FROM cliente_sucursales cs
        INNER JOIN unidadorg uo ON(cs.SucNro = uo.UniNro) ";
    $queryKnow .= " WHERE cs.SucNro = 391 ";
    $queryKnow .= "AND cs.EmpNro = $id_cliente ";

    $resKnow = $dbN->Seleccionar($queryKnow);

    $query = "SELECT cs.SucNro, uo.UniNombre FROM cliente_sucursales cs
        INNER JOIN unidadorg uo ON(cs.SucNro = uo.UniNro) ";

    if ($dbN->ContarFilas($resKnow) > 0) {
        $query .= " GROUP BY UniNombre ";
    } else {
        $query .= " WHERE cs.EmpNro = $id_cliente";
        echo $query;
    }

    $db = new BD(TRUE);
    $db->Conectar();

    $res = $db->Seleccionar($query);
    ?>
    <option value="0">-</option>
    <?php
    while ($row = mysql_fetch_assoc($res)) {
        $selected = '';
        if (mysql_num_rows($res) == 1) {
            $selected = 'selected="selected"';
        }
        ?>
        <option <?php echo $selected ?> value="<?php echo $row['SucNro'] ?>"><?php echo $row['UniNombre'] ?></option>
        <?php
    }
}

function usuarias_responsables()
{

    $id_cliente = (int) $_GET["id_sucursal"];
    $query = "SELECT me.PerNro, CONCAT(me.MEmpNombres,' ',me.MEmpApellido) FROM unidadorg uo
        INNER JOIN miembroempresa me ON(uo.UniResponsable = me.PerNro)
	 WHERE uo.UniNro = $id_cliente AND me.active=1";

//    echo $query;
    $db = new BD(TRUE);
    $db->Conectar();
    $res = $db->Seleccionar($query);
    if (mysql_num_rows($res) > 0) {
        ?>
        <option value="0">-</option>
        <?php
        while ($row = mysql_fetch_array($res)) {
            ?>
            <option value="<?php echo $row[0] ?>"><?php echo $row[1] ?></option>
            <?php
        }
    } elseif ($id_cliente != 0) {
//        echo 'aoe';
        $query = "SELECT PerNro, CONCAT(MEmpNombres,' ',MEmpApellido) FROM miembroempresa
            WHERE MEmpAdmin = '0' AND UniNro = '" . $id_cliente . "' AND active=1 ORDER BY MEmpNombres";
        echo GenerarOptions(htmlentities($query), NULL, TRUE, DEFSELECT);
    } else {
        $query = "SELECT PerNro, CONCAT(MEmpNombres,' ',MEmpApellido) FROM miembroempresa
            WHERE MEmpAdmin = '0'  AND active=1 ORDER BY MEmpNombres";
        echo GenerarOptions(htmlentities($query), NULL, TRUE, DEFSELECT);
    }
}

function usuarias_email()
{
    $id_cliente = (int) $_GET["id_cliente"];
    if ($id_cliente == 0)
        exit("");

    $query = "SELECT CliEMailEjCta AS email FROM cliente WHERE CliNro = $id_cliente LIMIT 1";
//    echo $query;
    $db = new BD(true);
    $db->Conectar();

    $result = $db->Seleccionar($query, true);
    die(trim($result['email']));
}

function usuarias_sector()
{

    $id_cliente = (int) $_GET["res_id"];
    $query = "SELECT a.AreNom FROM miembroempresa mi INNER JOIN area a ON(mi.AreNro = a.AreNro)
	 WHERE mi.PerNro = $id_cliente LIMIT 1";
//    echo $query;
    $db = new BD(TRUE);
    $db->Conectar();
    $res = $db->Seleccionar($query, TRUE);
    if (!empty($res)) {
        echo $res[0];
    } else {
        echo 'No encontrado';
    }
}

function usuarias_sector_por_sucursal()
{
    $id_sucursal = (int) $_GET["id_sucursal"];
    $query = "SELECT a.AreNro, a.AreNom FROM miembroempresa mi INNER JOIN area a ON mi.AreNro = a.AreNro WHERE UniNro = $id_sucursal GROUP BY a.AreNro ORDER BY a.AreNom ASC";
    echo GenerarOptions(htmlentities($query), null, true, DEFSELECT);
}

function usuarias_responsables_por_sector()
{
    $id_sector = (int) $_GET["id_sector"];
    $id_sucursal = (int) $_GET["id_sucursal"];
    $query = "SELECT me.PerNro, CONCAT(me.MEmpNombres,' ',me.MEmpApellido,' (',pu.PueNom,')') as fullname FROM miembroempresa me LEFT JOIN puesto pu ON me.PueNro = pu.PueNro WHERE me.AreNro = $id_sector AND UniNro = $id_sucursal ORDER BY fullname ASC";
    echo GenerarOptions(htmlentities($query), null, false, DEFSELECT);
}

function usuarias_principal()
{
    ?>
    <div style="height:260px;">
        <table width="833" cellpadding="0" cellspacing="0" style="margin:12px 0 0 12px;" id="box-table-a">
            <thead>
                <tr>
                    <th width="100" scope="col"><span style="color:#c60; font-weight:bold;">Razón Social</span></th>
                    <th width="100" scope="col"><span style="color:#c60; font-weight:bold;">Contacto</span></th>
                    <th width="100" scope="col"><span style="color:#c60; font-weight:bold;">Teléfono</span></th>
                    <th width="100" scope="col"><span style="color:#c60; font-weight:bold;">Ejecutivo de Cuenta</span></th>
                    <th width="100" scope="col"><span style="color:#c60; font-weight:bold;">Opciones</span></th>
                </tr>
            </thead>
            <tbody>
                <?php
                $bCliente = (isset($_GET["cliente"]) ? $_GET["cliente"] : NULL);
                $bEjecutivo = (isset($_GET["ejecutivo"]) ? (int) $_GET["ejecutivo"] : NULL);

                $sSQL = "SELECT cliente.CliNro AS idcliente, CliRsocial, CliContacto, CliEjCta, CliTelefono ";
                $sSQL .= "FROM cliente LEFT JOIN clienteejcta cej ON cliente.CliNro = cej.CliNro WHERE 1 ";
                /* $sSQL .= "WHERE EmpNro = ". RetornarIdEmpresa() . " "; */

                if (strlen($bCliente) > 0)
                    $sSQL .= " AND CliRsocial LIKE '%" . $bCliente . "%' ";
                if (isset($bEjecutivo) AND $bEjecutivo != 0)
                    $sSQL .= " AND cej.MEmpNro = " . $bEjecutivo . " ";

                $sSQL .= " ORDER BY CliRsocial ";

                $cBD = new BD();
                $cBD->Conectar();
                $paging = new PHPPaging($cBD->RetornarConexion());
                $paging->agregarConsulta($sSQL);
                $paging->linkClase("navPage");
                $paging->porPagina(5);
                $paging->ejecutar();

                while ($aRegistro = $paging->fetchResultado()) {
                    $sPosicion = (($sPosicion == "1") ? "2" : "1");
                    ?>
                    <tr height="20">
                        <td width="220" style="padding:8px;"><?php print($aRegistro["CliRsocial"]); ?></td>
                        <td width="150" style="padding:8px;"><?php print($aRegistro["CliContacto"]); ?></td>
                        <td width="100" style="padding:8px;"><?php print($aRegistro["CliTelefono"]); ?></td>
                        <td width="150" style="padding:8px;"><?php print($aRegistro["CliEjCta"]); ?></td>
                        <td height="30" style="padding:8px;">
                            <img src="images/icons/page_edit.png" alt="Editar" title="Editar" style="cursor:pointer;" onclick="editar_cliente(<?php echo $aRegistro["idcliente"]; ?>);" />
                            <img src="images/icons/page_delete.png" alt="Eliminar" title="Eliminar" style="cursor:pointer;" onclick="eliminar_cliente(<?php echo $aRegistro["idcliente"]; ?>);" />
                        </td>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
    </div>
    <div class="pagination"><?php echo $paging->fetchNavegacion(); ?></div>
    <?php
}

/*
  $sArchivo = "../clientes/".$aRegistro["idcliente"].".xls";
  if (file_exists($sArchivo)) {
 */
?>
