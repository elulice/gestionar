<?php
include("includes/funciones.php");
include("clases/framework-1.0/class.bd.php");
require_once ("clases/phppaging/PHPPaging.lib.php");
$bd = new BD();
$sql = "SELECT idevento, heading, date, dateout FROM events";
$resultado = $bd->Seleccionar($sql);
$json = array();
while($feed = $bd->RetornarFila($resultado)){
    $json[] = (object)array('id' => $feed['idevento'], 'title' => $feed['heading'], 'start' => $feed['date'], 'end' => $feed['dateout'], 'allDay' => false);
}
die(json_encode($json));
