<script>
    $(document).ready(function () {
        var availableTags = <?php include("feed.usuarias.autocomplete.php"); ?>; 
        $(".labelsUsuaria").autocomplete({
            source: availableTags
        });
    });
</script>

<div id="div_dialog_usuarias" style="display:none;">
    <input type="hidden" name="hdn_id_cliente" />
    <iframe style="display:none;" name="hidden_iframe"></iframe>
    <div style="width:360px;margin-top:4px;">
        <form id="frm_clientes_detalle" method="post" action="abm.php" enctype="multipart/form-data" target="hidden_iframe"> 
            <div class="ddu_title">
                <span>Datos de Cliente</span>
            </div>
            <div class="ddu_campo">
                <span>Razón Social:</span>
                <input type="text" class="smallInput" name="CliRsocial" />
            </div>
            <div class="ddu_campo">
                <span>Contacto:</span>
                <input type="text" class="smallInput" name="CliContacto" />
            </div>
            <div class="ddu_campo">
                <span>Teléfono:</span>
                <input type="text" class="smallInput" name="CliTelefono" />
            </div>
            <div class="ddu_campo">
                <span>Email:</span>
                <input type="text" class="smallInput" name="CliEmail" />
            </div>
            <div class="ddu_campo">
                <span>Domicilio:</span>
                <input type="text" class="smallInput" name="CliDomicilio" />
            </div>
            <div class="ddu_campo">
                <span>C.U.I.T:</span>
                <input type="text" class="smallInput" name="CliCUIT" />
            </div>
            <div class="ddu_campo">
                <span>Rubro:</span>
                <input type="text" class="smallInput" name="CliRubro" />
            </div>
            <div class="ddu_campo">
                <span>Imágen/Logo:</span>
                <input type="file" class="smallInput" name="FILE_Imagen_1" style="width:80px;" />
            </div>
            <div class="ddu_campo">
                <span>Archivo:</span>
                <input type="file" class="smallInput" name="FILE_Archivo_1" />
            </div>
            <div class="ddu_title" style="margin-top:15px;">
                <span>Generales</span>
            </div>
            <div class="ddu_campo_select">
                <span>Ejecutivo Cta:</span>
                <select class="smallInput" name="CliEjCta">
                    <?php
                    $query = "SELECT MEmpNro, CONCAT(MEmpNombres, ' ', MEmpApellido) FROM miembroempresa WHERE MEmpAdmin=2";
                    echo GenerarOptions($query, NULL);
                    ?>
                </select>
            </div>
            <div class="ddu_campo">
                <span>Fecha:</span>
                <input type="text" class="smallInput" name="CliFechaInicio" />
            </div>
            <div class="ddu_campo">
                <span>Email:</span>
                <input type="text" class="smallInput" name="CliEMailEjCta" />
            </div>
        </form>
    </div>
</div>
<div style="margin:8px 0 0 18px;">
    <form id="frm_usuarias_listado_filtro" target="hidden_iframe" onsubmit="actualizar_usuarias_listado();">
        <input type="submit" style="display:none;" />
        <table width="796" cellspacing="3" style="margin-left:40px;">
            <tbody>
                <tr>
                    <td width="230">
                        <div style="float:left;width:220px;">
                            <div class="form-label">Usuaria:</div>
                            <input type="text" class="smallInput labelsUsuaria" id="searchBox" name="cliente" style="width:130px;float:right;" />
                        </div>
                    </td>
                    <td width="230">
                        <div style="float:left;width:220px;">
                            <div class="form-label">Ejecutivo:</div>
                            <select class="smallInput" onchange="actualizar_usuarias_listado();" id="inputEjecutivo" name="ejecutivo" style="margin-left:20px;">
                                <?php
                                $query = "SELECT MEmpNro, CONCAT(MEmpNombres, ' ', MEmpApellido) AS nombre
FROM miembroempresa WHERE MEmpAdmin=2 ORDER BY nombre";

                                echo GenerarOptions($query, NULL, TRUE, DEFSELECT);
                                ?>
                            </select>
                        </div>
                    </td>
                    <td width="290">
                        <a class="button_notok" onclick="$('#frm_usuarias_listado_filtro').clearForm();actualizar_usuarias_listado();" style="margin-top:3px;"><span>Limpiar Búsqueda</span></a>
                        <a class="button_ok" onclick="actualizar_usuarias_listado();" style="margin-top:3px;"><span>Buscar</span></a>
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
</div>
<div id="div_usuarias_listado" class="navPage">
    <img src="images/loading.gif" class="loading" />
</div>
<?php
$m_lIDRegistro = is_numeric($_REQUEST["idregistro"]) ? $_REQUEST["idregistro"] : 0;
$m_lIDSeccion = is_numeric($_REQUEST["idseccion"]) ? $_REQUEST["idseccion"] : 1;
$m_lIDPostulante = is_numeric($_REQUEST["idpostulante"]) ? $_REQUEST["idpostulante"] : 0;

$m_sURL = $_SERVER["REQUEST_URI"];

$aMenues[0][0] = "usuarios";
$aMenues[1][0] = "clientes";
$aMenues[2][0] = "postulantes";
$aMenues[3][0] = "noticias";
$aMenues[4][0] = "agenda";
$aMenues[5][0] = "trayectoria";
$aMenues[6][0] = "ofertas";
$aMenues[7][0] = "curriculums";
$aMenues[8][0] = "*";

for ($lJ = 0; $lJ < count($aMenues); $lJ++) {
    if ($aMenues[$lJ][0] == "*") {
        $aMenues[$lJ][1] = true;
        break;
    } else {
        if (strpos(basename($m_sURL), $aMenues[$lJ][0]) > 0) {
            $aMenues[$lJ][1] = true;
            break;
        }
    }
}
?>
