<?php
// require_once("inc.postulante.detalle.php");
//
?>
<script type="text/javascript">

    $(document).ready(function () {
        var availableTags = <?php include("feed.tickets.usuarias.autocomplete.php"); ?>;
        $(".labelsUsuaria").autocomplete({
            source: availableTags
        });

        var availableTagsNew = <?php include("feed.new.ticket.usuarias.php"); ?>;
        $("#p_cliente_new").autocomplete({
            source: availableTagsNew,
            select: function(event, ui) {
                updateSucursales(ui.item.id);
                updateEmail(ui.item.id);
            }
        });
        $('#p_sucursal_new').change(function(){
            var select = $(this);
            
            $.ajax({
                type: 'get',
                url: 'feed.usuarias.php?objeto=sector_por_sucursal&id_sucursal='+select.val(),
                beforeSend: function(){
                    $('#load-sector').css('display', 'block');
                },
                success: function(data){
                    $('#load-sector').css('display', 'none');
                    $('#sector_select').html(data);
                    $('#p_responsable_new').html('');
                }
            });
            
            /*$.ajax({
                type: 'get',
                url: 'feed.usuarias.php?objeto=responsables&id_sucursal='+select.val(),
                beforeSend: function(){
                    $('#select_planes_polizas').find('img').css('display', 'block');
                    $('#select_planes_polizas select').attr('disabled', 'disabled');
                    $('#select_planes_polizas select').attr('readonly', 'readonly');
                    $('#select_planes_polizas select').css('background-color', '#DDD');
                },
                success: function(data){
                    $('#select_planes_polizas').find('img').css('display', 'none');
                    $('#select_planes_polizas select').removeAttr('disabled');
                    $('#select_planes_polizas select').removeAttr('readonly');
                    $('#select_planes_polizas select').css('background-color', '#FFF');
                    $('#p_responsable_new').html(data);
                }
            });*/
        });
        
        $('#sector_select').change(function(){
            var select = $(this);
            
            $.ajax({
                type: 'get',
                url: 'feed.usuarias.php?objeto=responsables_por_sector&id_sector=' + select.val() + '&id_sucursal=' + $('#p_sucursal_new').val(),
                beforeSend: function(){
                    $('#load-responsable').css('display', 'block');
                },
                success: function(data){
                    $('#load-responsable').css('display', 'none');
                    $('#p_responsable_new').html(data);
                    
                    var responsable = '';
                    $('#p_responsable_new option').each(function(){
                        $(this).removeAttr("selected");
                        if ($(this).html().indexOf('Responsable') != -1) {
                            $(this).css('font-weight', 'bold');
                            $(this).attr('selected', 'selected');
                            responsable = $(this);
                            $(this).remove();
                        }
                    });
                    
                    if (responsable) {
                        $('#p_responsable_new').prepend(responsable);
                    }
                    fill_mail_responsable($('#p_responsable_new').val())
                }
            });
        });


    });
    function updateSucursales(empresaId){
        if(empresaId != 0){
            $.ajax({
                type: 'get',
                url: 'feed.usuarias.php?objeto=sucursales&id_cliente='+empresaId,
                beforeSend: function(){
                    $('#select_planes_polizas, #select_planes_polizas_new').find('img').css('display', 'block');
                    $('#select_planes_polizas select, #select_planes_polizas_new select').attr('disabled', 'disabled');
                    $('#select_planes_polizas select, #select_planes_polizas_new select').attr('readonly', 'readonly');
                    $('#select_planes_polizas select, #select_planes_polizas_new select').css('background-color', '#DDD');
                },
                success: function(data){
                    $('#select_planes_polizas, #select_planes_polizas_new').find('img').css('display', 'none');
                    $('#select_planes_polizas select, #select_planes_polizas_new select').removeAttr('disabled');
                    $('#select_planes_polizas select, #select_planes_polizas_new select').removeAttr('readonly');
                    $('#select_planes_polizas select, #select_planes_polizas_new select').css('background-color', '#FFF');
                    $('#p_sucursal_new').html(data);
                    $('#p_sucursal_new').change();
                }
            });
        }
    }
    function updateEmail(empresaId){
        solicitante = $("#p_origen_new").val();
        if(empresaId != 0 && solicitante == 2){
            $.ajax({
                type: 'get',
                url: 'feed.usuarias.php?objeto=email&id_cliente='+empresaId,
                success: function(data){
                    $('#p_email_cliente_new').val(data);
                }
            });
        }
    }
</script>
<div id="div_dialog_agenda" style="display:none;"><?php // require_once("inc.curriculums.notas.php");                                                                                                                                                                                                                                                 ?></div>
<div style="margin:8px 0 0 18px;">
    <form id="frm_tickets_usuarias_listado_filtro" target="hidden_iframe" onsubmit="actualizar_tickets_usuarias_listado();">
        <input type="submit" style="display:none;" />
        <table width="796" cellspacing="5" style="margin-left:40px;">
            <tbody>
                <tr height="30">
                    <td width="235">
                        <div style="float:left;width:220px;">
                            <div class="form-label" style="width:80px;text-align:left;"><img src="images/icons/pixel.png" class="icons_new_tickets int_nombres"/>Nombre:</div>
                            <input type="text" class="smallInput" name="tnombre" onkeyup="actualizar_tickets_usuarias_listado();" style="width:120px;" />
                        </div>
                    </td>
                    <td width="235">
                        <div style="float:left;width:220px;">
                            <div class="form-label" style="width:80px;text-align:left;"><img src="images/icons/pixel.png" class="icons_new_tickets int_nombres"/>Responsable:</div>
                            <select class="smallInput" onchange="actualizar_tickets_usuarias_listado();" name="tresponsable" style="width:130px;"/>
                            <?php
                            $query = "SELECT m.PerNro, CONCAT(m.MEmpApellido, ', ', m.MEmpNombres) FROM miembroempresa m WHERE m.PerNro IN (SELECT DISTINCT(t.responsable_id) FROM all_tickets t) ORDER BY m.MEmpApellido ASC, m.MEmpNombres ASC";
                            echo GenerarOptions($query, NULL, TRUE, DEFSELECT);
                            ?>
                            </select>
                        </div>
                    </td>
                    <td width="235">
                        <div style="float:left;width:220px;">
                            <div class="form-label" style="width:80px;text-align:left;"><img src="images/icons/pixel.png" class="icons_new_tickets int_estado"/>Estado:</div>
                            <select class="smallInput" onchange="actualizar_tickets_usuarias_listado();" name="testado" style="width:130px;"/>
                            <?php
                            $query = "SELECT id, nombre FROM estado_tickets";
                            echo GenerarOptions(html_entity_decode($query), NULL, TRUE, DEFSELECT);
                            ?>
                            </select>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td width="235">
                        <div style="float:left;width:220px;">
                            <div class="form-label" style="width:80px;text-align:left;"><img src="images/icons/pixel.png" class="icons_new_tickets int_fecha"/>Fecha:</div>
                            <input class="smallInput" name="tfecha" onchange="actualizar_tickets_usuarias_listado();" id="p_fecha_buscar" style="width:120px;text-align:center;" />
                        </div>
                    </td>
                    <td width="235">
                        <div style="float:left;width:220px;">
                            <div class="form-label" style="width:80px;text-align:left;"><img src="images/icons/pixel.png" class="icons_new_tickets int_clientes"/>Usuarias:</div>
                            <input type="text" class="smallInput labelsUsuaria" onchange="actualizar_tickets_usuarias_listado();" name="tusuarias" style="width:120px;" />
                        </div>
                    </td>
                    <td width="235">
                        <div style="float:left;width:220px;">
                            <div class="form-label" style="width:80px;text-align:left;"><img src="images/icons/pixel.png" class="icons_new_tickets int_sucursal"/>Sucursal:</div>
                            <select class="smallInput" onchange="actualizar_tickets_usuarias_listado();" name="tsucursal" style="width:130px;"/>
                            <?php
                            $query = "SELECT UniNro, UniNombre FROM unidadorg";
                            echo GenerarOptions($query, NULL, TRUE, DEFSELECT);
                            ?>
                            </select>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td width="235">
                        <div style="float:left;width:220px;">
                            <div class="form-label" style="width:80px;text-align:left;"><img src="images/icons/pixel.png" class="icons_new_tickets int_ticket_nro"/>Nro. Reclamo:</div>
                            <input type="text" class="smallInput" onchange="actualizar_tickets_usuarias_listado();" name="tnroreclamo" style="width:120px;" />
                        </div>
                    </td>
                    <td width="235"></td>
                    <td width="235">
                        <a class="button_notok" onclick="$('#frm_tickets_usuarias_listado_filtro').clearForm(); actualizar_tickets_usuarias_listado();" style="margin-top:3px;"><span>Limpiar B&uacute;squeda</span></a>
                        <a class="button_ok" onclick="actualizar_tickets_usuarias_listado();" style="margin-top:3px;"><span>Buscar</span></a>
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
</div>
<div id="div_tickets_usuarias_listado" class="navPage">
    <img src="images/loading.gif" class="loading" />
</div>

<?php include 'form_new_ticket.php' ?>

<div id="edit_ticket_dialog"><div id="textToEdit"></div></div>
<div id="form-ticket-adjuntos"></div>

<div id="response_for_dialogs" style="top: 0;left: 0;margin: 0 0 0 0;background:#fefefe;width: 100%;height: 100%;">
    <div id="content_information"></div>
    <div style="right: 15px;position: absolute;bottom: 10px;"><a href="javascript:void(0)" onclick="$( '#response_for_dialogs:ui-dialog' ).dialog( 'close' ); "class="buttons ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" ><span class="ui-button-text" style="font-size: 11px;">Cerrar</span></a></div>
</div>
<div id="form-ticket-adjuntos" style="display:none;"></div>

<script type="text/javascript" >
    $(document).ready(function(){
        $('#p_fecha_buscar').datepicker();
        $.datepicker.setDefaults({ dateFormat: 'dd/mm/yy' });
        $( "#generateNewTicket:ui-dialog" ).dialog( "destroy" );
        $( "#generateNewTicket" ).dialog({
            width: 855,
            height: 531,
            closeOnEscape: true,
            autoOpen: false,
            modal: true,
            resizable: false,
            title: 'Nuevo Ticket'
        });
        $( "#response_for_dialogs" ).dialog({
            width: 250,
            height: 115,
            closeOnEscape: true,
            autoOpen: false,
            modal: true,
            resizable: false,
            title: 'Informaci&oacute;n'
        });
        $( "#dialog-alert" ).dialog({
            width: 360,
            height: 162,
            closeOnEscape: true,
            autoOpen: false,
            modal: true,
            resizable: false,
            title: 'ATENCIÓN',
            buttons: {
                "Cerrar": function() {
                    $(this).dialog("close");
                }
            }
        });
    });

    jQuery(function($){
        $.datepicker.regional['es'] = {
            clearText: 'Borra',
            clearStatus: 'Borra fecha actual',
            closeText: 'Cerrar',
            closeStatus: 'Cerrar sin guardar',
            prevText: '<Ant',
            prevBigText: '<<',
            prevStatus: 'Mostrar mes anterior',
            prevBigStatus: 'Mostrar año anterior',
            nextText: 'Sig>',
            nextBigText: '>>',
            nextStatus: 'Mostrar mes siguiente',
            nextBigStatus: 'Mostrar año siguiente',
            currentText: 'Hoy',
            currentStatus: 'Mostrar mes actual',
            monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio', 'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
            monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
            monthStatus: 'Seleccionar otro mes',
            yearStatus: 'Seleccionar otro año',
            weekHeader: 'Sm',
            weekStatus: 'Semana del año',
            dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
            dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Jue', 'Vie', 'Sáb'],
            dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
            dayStatus: 'Set DD as first week day',
            dateStatus: 'Select D, M d',
            dateFormat: 'dd/mm/yy',
            firstDay: 1,
            initStatus: 'Seleccionar fecha',
            isRTL: false
        };
        $.datepicker.setDefaults($.datepicker.regional['es']);
    });
</script>
<?php
if ($_SESSION["error_auth"] == TRUE) {
    $motivo = explode('_', $_SESSION["motivo"]);
    $idmotivo = $motivo[0];
    $motivos = $motivo[1];
    ?>
    <script type="text/javascript">
        $(document).ready(function(){
            $("#dialog-alert").dialog('open');
        })
    </script>
    <?
    unset($_SESSION["error_auth"]);
    unset($_SESSION["motivo"]);
}
?>
<div id="dialog-alert">No está autorizado para visualizar éste Ticket (Nro. #<?php echo $idmotivo; ?>).<br/><?php echo $motivos; ?></div>