<?php

	$sSQL = "SELECT PerApellido, PerNombres ";
	$sSQL .= "FROM persona ";
	$sSQL .= "WHERE PerNro = " . $m_lIDPostulante;
	$cBD = new BD();
	$aPostulante = $cBD->Seleccionar($sSQL, true);

	if($m_lIDRegistro > 0)
	{
		$sSQL = "SELECT d.* ";
		$sSQL .= "FROM documentacion d ";
		$sSQL .= "WHERE d.DNro = " . $m_lIDRegistro;
		$cBD = new BD();
		$aRegistro = $cBD->Seleccionar($sSQL, true);
	}
	
?>
<link href="estilos/general.css" rel="stylesheet" type="text/css" />


<table width="780" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="30" class="encabezado-titulo-bg"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="20"><img src="images/espacio.gif" width="1" height="1"></td>
        <td class="encabezado-titulo-texto">Alta y modificaci&oacute;n de Documentaci&oacute;n </td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><img src="images/espacio.gif" width="1" height="20"></td>
  </tr>
</table>
<table width="580" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="20"><img src="images/formulario-encabezado-inicio.jpg" width="20" height="37"></td>
        <td class="formulario-encabezado-bg">Documentacion - <?php print $aPostulante["PerApellido"]." ".$aPostulante["PerNombres"]; ?> </td>
        <td width="20"><img src="images/formulario-encabezado-final.jpg" width="20" height="37"></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="4" class="formulario-contenido-inicio"><img src="images/espacio.gif" width="1" height="1"></td>
        <td><form action="abm.php?tabla=documentacion&columna=DNro&idregistro=<?php print($m_lIDRegistro); ?>&url=<?php print($m_sURL); ?>" method="post" name="frmRegistro" id="frmRegistro">
 <input name="PerNro" type="hidden" value="<?php print $m_lIDPostulante; ?>" />
<input name="DFecha" type="hidden" value="<?php print(date("Ymd")); ?>" />
<input name="EmpNro" type="hidden" value="<?php print(RetornarIdEmpresa()); ?>" />
          <table width="100%" border="0" cellspacing="0" cellpadding="4">
          <tr>
            <td class="formulario-etiquetas">Tipo de documento :</td>
            <td width="340"><select name="TDNro" id="TDNro" style="width: 300px;">
              <?php
					$sSQL = "SELECT TDNro, TDNombre FROM tipodocumentacion  ";
					print(GenerarOptions($sSQL, $aRegistro["TDNro"]));
			  ?>
            </select></td>
          </tr>
          <tr>
            <td class="formulario-etiquetas">Nombre Documento:</td>
            <td><input name="DNombre" type="text" class="formulario-textbox" id="DNombre" style="width: 300px;" value="<?php print($aRegistro["DNombre"]); ?>" readonly="readonly" /></td>
          </tr>
		  
          <tr>
            <td class="formulario-etiquetas">Archivo:</td>
            <td><input type="file" name="FILE_Archivo;../Documentacion/" id="FILE_Archivo;../Documentacion/" class="formulario-textbox" style="width: 300px;" onchange="actualizarNombre()"  /></td>
          </tr>
          <tr>
            <td height="30" class="formulario-etiquetas"><img src="images/espacio.gif" width="1" height="1"></td>
            <td align="center"><input name="BTN_Guardar" type="submit" id="BTN_Guardar" value="Guardar">
              <input name="BTN_Cancelar" type="reset" id="BTN_Cancelar" value="Cancelar" onclick="history.back();"></td>
          </tr>
        </table>
        </form></td>
        <td width="6" class="formulario-contenido-final"><img src="images/espacio.gif" width="1" height="1"></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="20"><img src="images/formulario-pie-inicio.jpg" width="20" height="40"></td>
        <td class="formulario-pie-bg"><img src="images/espacio.gif" width="1" height="1"></td>
        <td width="20"><img src="images/formulario-pie-final.jpg" width="20" height="40"></td>
      </tr>
    </table></td>
  </tr>
</table>
