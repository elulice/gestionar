<?php
session_start();
include("clases/framework-1.0/class.bd.php");
include("includes/funciones.php");
ValidarSesion();
$cBD = new BD();
// Chack if is comes from a 'poliza' if this is null or empty must chack if it's 
// editing, in case that is not any of those must let the user select a 'poliza'
$polizaId = $_GET['polizaid'];
$m_lIDRegistro = $_GET['idregistro'];
// Initializate 'reclamo' 
$aRegistro = array();
$aRegistro['fecha_envio_scan'] = '0000-00-00';
$aRegistro['fecha_envio_original'] = '0000-00-00';
$aRegistro['fecha_recepcion_scan'] = '0000-00-00';
$aRegistro["fecha_recepcion_original"] = '0000-00-00';
$usuario = "Nuevo Reclamo";
// Chck if it's editing
if ($m_lIDRegistro > 0) {
    // If it's editing must load the 'reclamo'
    $sSQL = "SELECT * FROM reclamos INNER JOIN estado_reclamos 
        ON(reclamos.estado_reclamo_id = estado_reclamos.id) 
        INNER JOIN tipo_reclamos ON(reclamos.tipo_reclamo_id = tipo_reclamos.id) 
        WHERE reclamos.id = '$m_lIDRegistro' LIMIT 1";
    // Run select query
    $resultado = $cBD->Seleccionar($sSQL);
    // Check result
    if (mysql_num_rows($resultado) != 1) {
        $m_lIDRegistro = 0;
    } else {
        // Put the result on $aRegistro var
        $aRegistro = $cBD->RetornarFila($resultado);
        // And finally put the 'poliza_id' on the var so the 'poliza' can 
        // be loaded on the next if
        $polizaId = $aRegistro['poliza_id'];
    }
}
?>
<!-- CHROME HACK -->
<style type="text/css">
    body:nth-of-type(1) .buttons {
        margin-bottom:5px;
    }
</style>
<div class="grid_8" style="overflow:hidden; height:55px; margin-bottom:5px;">
    <h1 class="new_edit" id="p_nombre_completo"> <?php echo $usuario; ?>  </h1>
</div>
<span style=" float:right;display:block; width:420px; margin-top:15px;">
    <div style="width:200px; float:left;">
        <form action="" id="formBuscarContacto" name="formBuscarContacto" onsubmit="buscarPolizaAdmin(); return false;" method="GET">
            <input type="text" name="txtFormBuscar" id="txtFormBuscar" class="smallInput" style="width:120px; float:left; margin-top:-3px;" value=""/>
            <a class="search_inline" href="javascript: void(0);" onclick="buscarPolizaAdmin()" style="margin-left:10px; float:left;">     Buscar</a>
        </form>
    </div>
    <?php if (!($_SESSION["is_agente"] || $_SESSION["is_productor"])): ?>
        <a class="edit_inline"  href="javascript:void(0);" onclick="guardarReclamo(false);">   Agregar</a>
        <a class="save_inline" href="javascript:void(0);" onclick="guardarReclamo(true);" style="margin-left:10px;">     Guardar</a>
        <a id="a_polizas_delete" href="abm.php?tabla=reclamos&columna=id&idregistro=<?php echo $aRegistro["id"]; ?>&ajax=true&function=getFormReclamos(0)" class="reject_inline delete" style="margin-left:10px;">  Eliminar</a>
    <?php endif; ?>
</span>
<div class="clear" style="margin-bottom:15px;"></div>
<!--THIS IS A WIDE PORTLET-->
<form action="abm.php?tabla=reclamos&amp;columna=id&amp;idregistro=<?php echo ($m_lIDRegistro); ?>&amp;archivo=0&amp;contador=1&amp;ajax=true" method="post" enctype="multipart/form-data" name="formReclamo" target="hidden_form" id="formReclamos">
    <input type="hidden" id="id_reclamo" value="<?php echo ($m_lIDRegistro); ?>" />
    <div style="width:910px; display:inline; float:left; height:385px; margin-left:5px; overflow:hidden;">
        <div style="width:210px; margin-right:20px; display:block; float:left;">
            <input type="submit" style="display:none" value="Submit" />
            <input type="hidden" name="formChanged" value="false" id="formChanged" />
            <!-- POLIZA ID -->
            <input type="hidden" name="poliza_id" id="p_id_solicitud" value="0" />
            <!-- DATOS DE POLIZA TOOLTIP -->
            <span>Datos de la p&oacute;liza</span>
            <!-- NUMERO DE POLIZA -->
            <div class="form-administrar" style="position: relative;">
                <span class="form-label">NÂ° de p&oacute;liza:</span>
                <input type="text" name="nro_poliza" id="p_nro_poliza" disabled="disabled" class="form-contacto-text smallInput disabled" style="width:130px; float:right;"/>
                <a href="javascript: void(0);" id="btn_select_poliza" onclick="$('#dialog-reclamos-buscar-poliza').dialog('open');" style="display: <?php echo ((empty($polizaId)) ? 'block' : 'none') ?>; position: absolute; right: 2px; top: 5px;" ><img src="images/icons/zoom.png" /></a>
            </div>
            <!-- APELLIDO -->
            <div class="form-administrar"><span class="form-label">Apellido:</span>
                <input type="text" name="apellido" id="p_apellido" disabled="disabled" class="form-contacto-text smallInput disabled" style="width:130px; float:right;"/>
            </div>
            <!-- NOMBRE -->
            <div class="form-administrar"><span class="form-label">Nombres:</span>
                <input type="text" name="nombres" id="p_nombres" disabled="disabled" class="form-contacto-text smallInput disabled" style="width:130px; float:right;"/>
            </div>
            <!-- COMPAÃIA -->
            <div class="form-administrar"><span class="form-label">CompaÃ±Ã­a:</span>
                <select name="id_compania" disabled="disabled" id="p_id_compania" size="1" class="form-contacto-text smallInput disabled" style="width:138px; float:right;">
                    <?php
                    $sSQL = "SELECT idcompania, com_nombre FROM companias ORDER BY com_nombre ";
                    echo GenerarOptions($sSQL, 0, true, "Seleccione");
                    ?>
                </select>
            </div>
            <!-- PRODUCTO -->
            <div class="form-administrar"><span class="form-label">Producto:</span>
                <div id="select_productos_polizas">
                    <select name="id_producto" disabled="disabled" id="p_id_producto" size="1" class="disabled form-contacto-text smallInput" style="width:138px; float:right;">
                        <?php
                        $sSQL = "SELECT id_producto, nombre_producto FROM productos ORDER BY nombre_producto ";
                        echo GenerarOptions($sSQL, 0, true, "Seleccione");
                        ?>
                    </select>
                </div>
            </div>
            <!-- PLAN -->
            <div class="form-administrar"><span class="form-label">Plan:</span>
                <div id="select_planes_polizas">
                    <select name="id_plan" disabled="disabled" id="p_id_plan" size="1" class="form-contacto-text smallInput disabled" style="width:138px; float:right;">
                        <?php
                        $sSQL = "SELECT id_plan, codigo_plan FROM planes ORDER BY nombre_plan ";
                        echo GenerarOptions($sSQL, 0, true, "Seleccione");
                        ?>
                    </select>
                </div>
            </div>   
            <!-- AGENTE -->
            <div class="form-administrar">
                <span class="form-label">Agente:</span>
                <select name="id_agente" disabled="disabled" id="p_id_agente" size="1" class="form-contacto-text smallInput disabled" style="width:138px; float:right;">
                    <?php
                    $sSQL = "SELECT id_agente, agente FROM estructura_comercial where agente != '' ORDER BY agente ";
                    echo GenerarOptions($sSQL, 0, true, "Seleccione");
                    ?>
                </select>
            </div>
            <!-- GERENTE/DIRECTOR/DIR. GENERAL  -->
            <div id="agente-datos">
                <div class="form-administrar">
                    <span class="form-label">Gerente:</span>
                    <input type="text" disabled="disabled" id="p_gerente" class="form-contacto-text smallInput disabled" style="width:40px; float:right; margin-right:90px; background-color: #DEDEDE;" />
                </div>

                <div class="form-administrar">
                    <span class="form-label">Director:</span>
                    <input type="text" disabled="disabled" id="p_director" class="form-contacto-text smallInput disabled" style="width:40px; float:right; margin-right:90px; background-color: #DEDEDE;" />
                </div>
                <div class="form-administrar">
                    <span class="form-label">Dir. General:</span>
                    <input type="text" disabled="disabled" id="p_director_general" class="form-contacto-text smallInput disabled" style="width:40px; float:right; margin-right:90px; background-color: #DEDEDE;" />
                </div>
            </div>
        </div>
        <div style="width:210px; margin-right:20px; display:block; float:left;">
            <!-- DATOS DEL RECLAMO TOOLTIP -->
            <span>Datos del reclamo</span>
            <!-- RECLAMANTE -->
            <div class="form-administrar">
                <span class="form-label">Reclamante:</span>
                <input type="text" name="reclamante" title="Nombre del Reclamante" class="form-contacto-text smallInput" style="width:130px; float:right;" value="<?php echo $aRegistro["reclamante"]; ?>"/>
            </div>
            <!-- FECHA DE ENVIO DEL SCAN -->
            <div class="form-administrar">
                <span class="form-label">F. envio scan:</span>
                <input type="text" name="fecha_envio_scan" title="Fecha de envio del scan" class="datepicker form-contacto-text smallInput" style="width:130px; float:right;" value="<?php echo (($aRegistro["fecha_envio_scan"] != '0000-00-00') ? date('Y-m-d', strtotime($aRegistro["fecha_envio_scan"])) : ''); ?>"/>
            </div>
            <!-- FECHA DE ENVIO DEL ORIGINAL -->
            <div class="form-administrar">
                <span class="form-label">F. envio orig.:</span>
                <input type="text" id="feo_input" onchange="makeTimeStamp()" name="fecha_envio_original" title="Fecha de envio del original" class="datepicker form-contacto-text smallInput" style="width:130px; float:right;" value="<?php echo (($aRegistro["fecha_envio_original"] != '0000-00-00') ? date('Y-m-d', strtotime($aRegistro["fecha_envio_original"])) : ''); ?>"/>
            </div>
            <!-- SOLO PARA OBTENER EL DÍA STRING TIME() -->
            <div class="form-administrar">
                <input type="hidden" id="string_day" name="string_day" class="form-contacto-text smallInput" style="width:130px; float:right;" value="<?php echo $aRegistro["string_day"]; ?>" />
            </div>
            <!-- FECHA DE RECEPCION DEL SCAN -->
            <div class="form-administrar">
                <span class="form-label">F. rec. scan:</span>
                <input type="text" name="fecha_recepcion_scan" title="Fecha de recepcion del scan" class="datepicker form-contacto-text smallInput" style="width:130px; float:right;" value="<?php echo (($aRegistro["fecha_recepcion_scan"] != '0000-00-00') ? date('Y-m-d', strtotime($aRegistro["fecha_recepcion_scan"])) : ''); ?>"/>
            </div>
            <!-- FECHA DE RECEPCION DEL ORIGINAL -->
            <div class="form-administrar">
                <span class="form-label">F. rec. orig.:</span>
                <input type="text" name="fecha_recepcion_original" title="Fecha de recepcion del original" class="datepicker form-contacto-text smallInput" style="width:130px; float:right;" value="<?php echo (($aRegistro["fecha_recepcion_original"] != '0000-00-00') ? date('Y-m-d', strtotime($aRegistro["fecha_recepcion_original"])) : ''); ?>"/>
            </div>
            <!-- NUMERO DE RECLAMO -->
            <div class="form-administrar">
                <span class="form-label">Nro. reclamo:</span>
                <input type="text" name="numero_reclamo" title="Numero de reclamo" class="form-contacto-text smallInput" style="width:130px; float:right;" value="<?php echo $aRegistro["numero_reclamo"]; ?>"/>
            </div>
            <!-- IMPORTE  -->
            <div class="form-administrar">
                <span class="form-label">Importe:</span>
                <input type="text" name="importe" class="form-contacto-text smallInput" style="width:95px; float:right;" value="<?php echo $aRegistro['importe']; ?>"/>
                <div class="moneda form-label" style="width: 10px; margin: 4px 15px 0px 0px; float: right; display: block; text-align: left;">$</div>
            </div>
            <!-- MONEDA -->
            <div class="form-administrar">
                <span class="form-label">Moneda:</span>
                <div id="select-polizas-moneda">
                    <select name="moneda_id" size="1" id="p_id_moneda" class="form-contacto-text smallInput" style="width:138px; float:right;" onchange="$('.moneda').html($('#p_id_moneda option:selected').attr('title'))" >
                        <?php
                        $sSQL = "SELECT idmoneda, moneda,  simbolo as title FROM monedas ORDER BY moneda ";
                        echo GenerarOptions($sSQL, $aRegistro['moneda_id'], true, "Seleccione");
                        ?>
                    </select>
                </div>
            </div>
            <!-- TIPO  -->
            <div class="form-administrar">
                <span class="form-label">Tipo:</span>
                <div id="select-polizas-frecuencia">
                    <select name="tipo_reclamo_id" size="1" class="form-contacto-text smallInput" style="width:138px; float:right;">
                        <?php
                        $sSQL = "SELECT id, nombre FROM tipo_reclamos ORDER BY nombre ";
                        echo GenerarOptions($sSQL, $aRegistro['tipo_reclamo_id'], true, "Seleccione");
                        ?>
                    </select>
                </div>
            </div>
            <!-- ESTADO  -->
            <div class="form-administrar">
                <span class="form-label">Estado:</span>
                <div id="select-polizas-frecuencia">
                    <select name="estado_reclamo_id" size="1" class="form-contacto-text smallInput" style="width:138px; float:right;">
                        <?php
                        $sSQL = "SELECT id, nombre FROM estado_reclamos ORDER BY nombre ";
                        echo GenerarOptions($sSQL, $aRegistro['estado_reclamo_id'], true, "Seleccione");
                        ?>
                    </select>
                </div>
            </div>
            <!-- FACTURAS -->
            <div class="form-administrar">
                <span class="form-label" style="margin-right:10px;">Facturas:</span>
                <a class="buttons" style="font-size: 10px; width: 110px; float: right;" id="btn-facturas" onclick="getListadoFacturas();" href="javascript: void(0);">Abrir formulario</a>
            </div>
        </div>
        <!-- DETALLES Y ADJUNTOS -->
        <div style="width:420px; margin-right:20px; display:block; float:left;">
            <!-- EMPTY TOOLTIP(Used to align form) -->
            <span>&nbsp;</span>
            <!-- DETALLES  -->
            <div class="form-administrar"  style="width:440px;">
                <span class="form-label">Detalles:</span>
                <textarea name="detalles" class="form-contacto-text smallInput" style="width:430px; height: 120px; margin-top: 5px; float:right;"><?php echo $aRegistro['detalles']; ?></textarea>
            </div>
            <!-- ARCHIVOS ADJUNTOS -->
            <div id="form-reclamos-adjuntos" style="display:none;">
                <div class="form-administrar">
                    <span class="form-label">Archivo:</span>
                    <input type="file" id="file_actual" name="1" width="250"/>
                </div>
            </div>
            <div id="inc-files-reclamos" class="largeInput" style="clear: left; float: left; height:120px; margin-top: 10px; width: 430px;">
                <div id="inc-files-reclamos-container" style="float: left; height:120px; width: 430px;" >
                </div>
            </div>
        </div>
    </div>
    <div id="reclamos-facturas" style="display:none;">
        <input type="hidden" class="tmp_factura" id="factura_actual" name="Factura[1]" value=""/>
    </div>
    <div style="float:right; margin-top: 0px;">
        <a style="float:left;" href="javascript: void(0);" onclick="history.back();" >
            <span class="ui-icon ui-icon-arrowreturnthick-1-w" style="float:left;"></span>
            Volver a <?php echo $_SESSION['from']; ?>
        </a>
    </div>
</form>
<div id="dialog-reclamos-facturas" style="display:none;"></div>
<div id="dialog-reclamos-buscar-poliza" style="display:none;"></div>
<div id="form-reclamos-adjuntos-list" style="display:none;"></div>
<div id="dialog-reclamos-agregar-facturas" style="display:none;"></div>
<div id="dialog-reclamos-alarm" style="display:none;"></div>
<div id="guardar-reclamos-form" style="display:none;" title="Guardar">El formulario fue modificado. Â¿Desea guardar los datos cambios antes de continuar?</div>
<div onclick="getAlarms();">getAlarms</div>
<iframe name="hidden_form" id="hidden_form" height="0" width="0" style="display: none;"></iframe>
<script type="text/javascript">
    $(document).ready(function(){
        Date.prototype.toString = function () {return isNaN (this) ? 'NaN' : [this.getDate() > 9 ? this.getDate() : '0' + this.getDate(),  this.getMonth() > 8 ? this.getMonth() + 1 : '0' + (this.getMonth() + 1), this.getFullYear()].join('/')}
        var d = new Date().toString();
        $(".datepicker").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd/mm/yy",
            defaultDate: d,
            regional: 'es',
            minDate: new Date(1920, 0, 1),
            yearRange:'1920:c',
            dayNames: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
            dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
            dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
            monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
            monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
        });

        $(".buttons").button();
        var dlg = $( "#form-reclamos-adjuntos" ).dialog({
            autoOpen: false,
            height: 'auto',
            width: 370,
            modal: true,
            resizable: false,
            title: '<span class="ui-icon ui-icon-link"></span> Adjuntar Archivo',
            buttons: {
                "Adjuntar": function() {
                    // First create another input file.
                    $( this ).dialog( "close" );
                    var count = parseInt($('#file_actual').attr('name'));
                    var fileName = basename($('#file_actual').val());
                    insertNewFileToTheList(fileName);
                    
                    $('#file_actual').after('<input type="file" id="new_file_actual" name="'+(count+1)+'" width="250"/>');
                    $('#file_actual').addClass("temp_attached_file");
                    $('#file_actual').css('display', 'none');
                    $('#file_actual').removeAttr("id");
                    $('#new_file_actual').attr("id", 'file_actual');
                },
                "Cancelar": function() {
                    $( this ).dialog( "close" );
                }
            }
        });
        dlg.parent().appendTo($("#formReclamos"));
        $( "#form-reclamos-adjuntos-list" ).dialog({
            autoOpen: false,
            width: 'auto',
            maxWidth: 580,
            height: 'auto',
            modal: true,
            resizable: false,
            title: '<span class="ui-icon ui-icon-link"></span> Archivos adjuntos',
            resizeStop: function(event, ui) {
                $('#form-polizas-adjuntos-list').dialog('option', 'position', 'center');
            },
            buttons: {
                "Cerrar": function() {
                    $( this ).dialog( "close" );
                }
            },
            open: function(){
                actualizarListadoAdjuntos(<?php echo ($m_lIDRegistro); ?>);
            },
            close: function(){
                actualizarArchivosReclamo(<?php echo ($m_lIDRegistro); ?>);
            }
        });
        // DIALOGO BUSCAR POLIZA
        $( "#dialog-reclamos-buscar-poliza" ).dialog({
            autoOpen: false,
            width: 'auto',
            maxWidth: 580,
            height: 'auto',
            modal: true,
            resizable: false,
            title: '<span class="ui-icon ui-icon-document"></span> Buscar poliza',
            resizeStop: function(event, ui) {
                $('#dialog-reclamos-buscar-poliza').dialog('option', 'position', 'center');
            },
            buttons: {
                "Cerrar": function() {
                    $( this ).dialog( "close" );
                }
            },
            open: function(){
                // Get the polizas list
                actualizarSeleccionarPoliza();
            }
        });
        // DIALOGO ALARMA / ALERTAS
        $( "#dialog-reclamos-alarm" ).dialog({
            autoOpen: false,
            width: 'auto',
            maxWidth: 580,
            height: 'auto',
            modal: true,
            resizable: false,
            title: '<span class="ui-icon ui-icon-document"></span> Alarma',
            resizeStop: function(event, ui) {
                $('#dialog-reclamos-buscar-poliza').dialog('option', 'position', 'center');
            },
            buttons: {
                "Cerrar": function() {
                    $( this ).dialog( "close" );
                }
            },
            open: function(){
                // Get the polizas list
                //                actualizarSeleccionarPoliza();
            }
        });
        // DIALOGO FACTURAS
        $( "#dialog-reclamos-facturas" ).dialog({
            autoOpen: false,
            width: 'auto',
            maxWidth: 580,
            height: 'auto',
            modal: true,
            resizable: false,
            title: '<span class="ui-icon ui-icon-document"></span> Administrar facturas',
            resizeStop: function(event, ui) {
                $('#dialog-reclamos-facturas').dialog('option', 'position', 'center');
            },
            buttons: {
                "Cerrar": function() {
                    $( this ).dialog( "close" );
                }
            },
            open: function(){
                updateFacturas();
            }
        });
        // DIALOGO AGREGAR FACTURAS
        $( "#dialog-reclamos-agregar-facturas" ).dialog({
            autoOpen: false,
            width: 'auto',
            maxWidth: 580,
            height: 'auto',
            modal: true,
            resizable: false,
            title: '<span class="ui-icon ui-icon-plusthick"></span> Agregar factura',
            resizeStop: function(event, ui) {
                $('#dialog-reclamos-agregar-facturas').dialog('option', 'position', 'center');
            },
            buttons: {
                "Guardar": function() {
                    // Parse the count of 'facturas' created
                    var count = parseInt($('#factura_actual').attr('name').match(/.[^\[]+\[(\d+)\]/).pop());
                    // Create a object 'factura'
                    var factura = {
                        'id': $('#formNuevaFactura input[name=id]').val(),
                        'reclamo_id': $('#formNuevaFactura input[name=reclamo_id]').val(),
                        'moneda_id': $('#formNuevaFactura select[name=moneda_id]').val(),
                        'numero': $('#formNuevaFactura input[name=numero]').val(),
                        'concepto': $('#formNuevaFactura textarea[name=concepto]').val(),
                        'fecha': parseDate($('#formNuevaFactura input[name=fecha]').val(), 'dd/mm/yyyy').format('yyyy-mm-dd'),
                        'importe': $('#formNuevaFactura input[name=importe]').val(),
                        'pagada': $('#formNuevaFactura select[name=pagada]').val()
                    }
                    // Stringify it to store it on a hidden field
                    var text = JSON.stringify(factura);
                    if($('#formNuevaFactura input[name="editing"]').val() != ''){
                        $('.tmp_factura[name=\''+$('#formNuevaFactura input[name="editing"]').val()+'\']').val(text);
                        updateFacturas();
                        $( this ).dialog( "close" );
                        return false;
                    }
                    // Set the stringified 'factura' to the current hidden field
                    $('#factura_actual').val(text);
                    // Create another field for the next item
                    $('#factura_actual').after('<input class="tmp_factura" type="hidden" id="new_factura_actual" name="Factura['+(count+1)+']" />');
                    // And chnage ids
                    $('#factura_actual').addClass("temp_attached_file");
                    $('#factura_actual').removeAttr("id");
                    $('#new_factura_actual').attr("id", 'factura_actual');
                    updateFacturas();
                    // Close the dialog
                    $( this ).dialog( "close" );
                },
                "Cancelar": function() {
                    $( this ).dialog( "close" );
                }
            },
            open: function(){
                // Get the polizas list
                //                actualizarSeleccionarPoliza();
            }
        });
        $( "#guardar-reclamos-form" ).dialog({
            autoOpen: false,
            height: 'auto',
            width: 370,
            modal: true,
            resizable: false,
            title: '<span class="ui-icon ui-icon-disk"></span> Guardar',
            buttons: {
                "Si": function() {
                    desformatearFechas();
                    $("#formReclamos").attr('action', $("#formReclamos").attr('action') + "&clear=true"),
                    $("#formReclamos").submit();
                    $( this ).dialog( "close" );
                },
                "No": function() {
                    getFormReclamos(0);
                    $( this ).dialog( "close" );
                },
                "Cancelar": function() {
                    $( this ).dialog( "close" );
                }
            }
        });
        $("#dialog-polizas-requisitos").dialog({
            modal: true,
            autoOpen: false,
            width: 370,
            height: 'auto',
            title: '<span class="ui-icon ui-icon-document"></span> Requisitos',
            resizable: false,
            resizeStop: function(event, ui) {
                $('#dialog-polizas-requisitos').dialog('option', 'position', 'center');
            },
            open: function(){
                $('textarea').blur();
            },
            buttons: {
                "Guardar": function() {
                    //alert($("*:focus").html());
                }
            }
        });
        $("#dialog-polizas-adicionales").dialog({
            modal: true,
            autoOpen: false,
            width: 370,
            height: 'auto',
            title: '<span class="ui-icon ui-icon-circle-plus"></span> Asegurados Adicionales',
            resizable: false,
            resizeStop: function(event, ui) {
                $('#dialog-polizas-requisitos').dialog('option', 'position', 'center');
            },
            open: function(){
                $('textarea').blur();
            }
        });
        $("#dialog-polizas-coberturas").dialog({
            modal: true,
            autoOpen: false,
            width: 370,
            height: 'auto',
            title: '<span class="ui-icon ui-icon-circle-plus"></span> Coberturas Adicionales',
            resizable: false,
            resizeStop: function(event, ui) {
                $('#dialog-polizas-coberturas').dialog('option', 'position', 'center');
            },
            buttons: {
                "Guardar": function() {
                    $("#formCoberturas").submit();
                    $( this ).dialog( "close" );
                },
                "Cancelar": function() {
                    $( this ).dialog( "close" );
                }
            }
        });
        function fechaEmision(form){
            var fechaNacimiento = form.fecha_nacimiento.value.split("-");
            fechaNacimiento = new Date(fechaNacimiento[0], fechaNacimiento[1], fechaNacimiento[2]);
            var fechaAprobacion = form.fecha_aprobacion.value.split("-")
            fechaAprobacion = new Date(fechaAprobacion[0], fechaAprobacion[1], fechaAprobacion[2]);
            var diferencia = fechaAprobacion.getFullYear() - fechaNacimiento.getFullYear();
            if((fechaAprobacion.getMonth() - fechaNacimiento.getMonth()) <= 0 ){
                if((fechaAprobacion.getDate() - fechaNacimiento.getDate()) < 0){
                    diferencia--;
                }
            }
            form.edad_emision.value = diferencia;
        }
        fetchAndLoadPoliza('<?php echo $polizaId ?>');
        actualizarArchivosReclamo(<?php echo ($m_lIDRegistro); ?>);
<?php if ($m_lIDRegistro > 0) { ?>
            getFormAdjunto(<?php echo ($m_lIDRegistro); ?>);
<?php } ?>
        $(".enviarPoliza").click(function(){
            $('#email_id').val(<?php echo ($m_lIDRegistro); ?>);
            $('#email_tipo').val('poliza');
            $("#enviarPorMail").dialog('open');
        });
        formatearFechas();
        var cache = {};
        var cacheSize = 0;
                                                
        $(".removeEmail", document.getElementById("usuarios_mails")).live("click", function(){
            var rel = $(this).attr('rel');
            $('[id*="formEnviarMail_"]').each(function(index, element){
                if($(element).val() == rel)
                    $(element).remove();
            });
            //remove current friend
            $(this).parent().remove();
                                                
            //correct 'to' field position
            if($("#usuarios_mails span").length === 0) {
                $("#autocompletarUsuario").css("top", 0);
            }
        });
        $("#autocompletarEmails").autocomplete({
            minLength: 3,
            source: function(request, response) {
                if (cache[request.term]) {
                    response(cache[request.term]);
                    return;
                }
                $.ajax({
                    url: "feed.contactos.autocomplete.php",
                    type: "GET",
                    data: {
                        q: request.term,
                        tipo: 'emails',
                        noempty: 1
                    },
                    dataType: "json",
                    success: function(data, textStatus) {
                        if (cacheSize >= 16) {
                            cache = {};
                            cacheSize = 0;
                        }
                        cache[request.term] = data;
                        ++cacheSize;
                        response(data);
                    },
                    error: function(xhr, textStatus, ex) {
                        alert('Oops, an error occurred. ' + xhr.statusText + ' - ' +
                            xhr.responseText);
                    }
                });
            },
            select: function(e, ui) {
                var ban = true;
                $('[id*="formEnviarMail_"]').each(function(index, element){
                    if(element.value == ui.item.id)
                        ban = false;
                });
                                
                if (ban) {
                    //create formatted friend
                    var friend = ui.item.value + "<" + ui.item.id + ">",
                    span = $("<span>").text(friend),
                    a = $("<a>").addClass("removeEmail").attr({
                        href: "javascript:",
                        title: "Remove " + friend,
                        rel:    ui.item.id
                    }).text("x").appendTo(span);
                    span.addClass("ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only");
                    //alert(span);
                    //add friend to friend div
                    //span.insertBefore("#invite");
                    span.appendTo("#usuarios_mails");
                    $("#formEnviarMail").append('<input type="hidden" name="mails[]" id="formEnviarMail_'+ui.item.id+'" value="'+ui.item.id+'" />');
                    //$("#autocompletarEmails").val("");
                    this.value = '';
                    return false;
                }
            },
            //define select handler
            change: function(event, ui) {
                //prevent 'to' field being updated and correct position
                $("#autocompletarEmails").val("").css("top", 2);
            }
        });
    });
    function updateFacturas(){
        getListadoFacturas(<?php echo ($m_lIDRegistro); ?>);
    }
    checkAlarms();
</script>
