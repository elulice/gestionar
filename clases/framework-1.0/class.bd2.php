<?php

ini_set("display_errors", FALSE);

class BD2 {

    var $sServidor = "localhost";
    var $sBaseDeDatos = "gestion_ar";
    var $sUsuario = "root";
    var $sClave = "GestionSite00";

    /*
      var $sBaseDeDatos = "consultores";
      var $sUsuario = "root";
      var $sClave = "";
     */
    var $oConexion;

    public function __construct($utf = TRUE) {
        $this->utf = $utf;
    }

    function Conectar() {
        if (($this->sServidor != "") && ($this->sUsuario != "")) {
            $this->oConexion = @mysql_connect($this->sServidor, $this->sUsuario, $this->sClave);
            @mysql_select_db($this->sBaseDeDatos, $this->oConexion);
            mysql_set_charset("latin2", $this->oConexion);
        }
    }

    function RetornarConexion() {
        return $this->oConexion;
    }

    function Seleccionar($pSQL, $pRetornarFila = false) {
        $oResultado = $this->Ejecutar($pSQL);
        return (($pRetornarFila) ? $this->RetornarFila($oResultado) : $oResultado);
    }

    function RetornarFila($pResultado) {
        return @mysql_fetch_array($pResultado);
    }

    function ContarFilas($pResultado) {
        $lFilas = 0;
        if ($pResultado)
            $lFilas = mysql_num_rows($pResultado);
        return $lFilas;
    }

    function Ejecutar($pSQL) {
        //echo $pSQL;
        $this->Conectar();
        $oResultado = @mysql_query($pSQL, $this->oConexion);
        if ($oResultado) {
            if (strpos(strtoupper($pSQL), "INSERT INTO") !== false)
                $oResultado = mysql_insert_id();
            elseif (strpos(strtoupper($pSQL), "UPDATE") !== false)
                $oResultado = mysql_affected_rows();
        }
        $this->Desconectar();
        return $oResultado;
    }

    function RetornarTipo($pResultado, $pCampo) {
        $sTipo = "";
        if ($pResultado)
            $sTipo = mysql_field_type($pResultado, $pCampo);
        return $sTipo;
    }

    function RetornarLongitud($pResultado, $pCampo) {
        $lLongitud = 0;
        if ($pResultado)
            $lLongitud = mysql_field_len($pResultado, $pCampo);
        return $lLongitud;
    }

    function Desconectar() {
        @mysql_close($this->oConexion);
    }

}

?>
