<?php

class ABM {

    var $m_sCarpeta = "";
    var $m_sColumnas = "";
    var $m_sValores = "";
    var $m_lContador = "";
    var $m_sURL = "";

    function URL() {
        return $this->m_sURL;
    }

    function Simple() {

        /* actualizar la clave para guardar en sha1 */
        if (isset($_POST["PerClave"])) {
            $tmp = sha1($_POST["PerClave"]);
            $_POST["PerClave"] = $tmp;
        }

        /* formatear fechas a iso  */
        $campos_fecha = array("CliFechaInicio", "OfeFechAlta", "OfeValidez", "PerFechaNac", "EtuIngreso", "EtuEgreso");

        foreach ($_POST as $key => $value) {
            if (in_array($key, $campos_fecha)) {
                $temp = date("Y-m-d", strtotime(str_replace("/", "-", $value)));
                $_POST[$key] = $temp;
            }
        }

        $sTabla = $_GET["tabla"];
        $sFiltro = $_GET["columna"];
        $lIDRegistro = $_GET["idregistro"];
        $bArchivo = $_GET["archivo"];
        $bContador = $_GET["contador"];
        if ($_GET["thumbs"] <> "")
            $aThumbs = explode(",", $_GET["thumbs"]);
        if ($_SERVER["REQUEST_METHOD"] == "GET") {

            $sSQL = "DELETE FROM " . $sTabla . " WHERE (" . $sFiltro . "=" . $lIDRegistro . ")";

            /* borrar el archivo si existe */
            if ($sTabla == "documentacion") {

                $id_postulante = (int) $_GET["id_postulante"];

                $archivo = $id_postulante . "_" . $lIDRegistro . "_*";
                $archivos = glob("upload/documentos/$archivo");

                if (count($archivos) == 1) {
                    unlink($archivos[0]);
                }
            }
        } else {
            if ($sTabla == "origen_tickets" || $sTabla == "responsable" || $sTabla == "estado_tickets") {
                $lTipo = (($lIDRegistro == 0) ? 1 : 2);
                foreach ($_POST as $sColumna => $sValor)
                    $this->Concatenar($lTipo, $sColumna, $sValor);
                if ($lTipo == 1)
                    $sSQL = "INSERT INTO " . $sTabla . " (" . $this->m_sColumnas . ") VALUES (" . utf8_decode($this->m_sValores) . ")";
                else
                    $sSQL = "UPDATE " . $sTabla . " SET " . htmlentities($this->m_sValores) . " WHERE (" . $sFiltro . "=" . $lIDRegistro . ")";
            }else {
                $lTipo = (($lIDRegistro == 0) ? 1 : 2);
                foreach ($_POST as $sColumna => $sValor)
                    $this->Concatenar($lTipo, $sColumna, $sValor);
                if ($lTipo == 1)
                    $sSQL = "INSERT INTO " . $sTabla . " (" . $this->m_sColumnas . ") VALUES (" . $this->m_sValores . ")";
                else
                    $sSQL = "UPDATE " . $sTabla . " SET " . $this->m_sValores . " WHERE (" . $sFiltro . "=" . $lIDRegistro . ")";
            }
        }


        //exit($sSQL);

        $cBD = new BD();
        $lIDGenerado = $cBD->Ejecutar($sSQL);
        $lIDRegistro = (($lTipo == 1) ? $lIDGenerado : $lIDRegistro);
        if ($bArchivo) {
            foreach ($_FILES as $sColumna => $aArchivo) {
                $lIDThumb = str_replace("FILE_", "", $sColumna);
                if ($this->EsImagen($aArchivo["type"]) and (is_array($aThumbs) or is_numeric($lIDThumb))) {
                    $this->SubirImagenes($aArchivo, $lIDRegistro, (is_numeric($lIDThumb) ? false : $bContador), (is_numeric($lIDThumb) ? array($lIDThumb) : $aThumbs));
                }
                else
                    $this->SubirArchivo($sColumna, $aArchivo, $lIDRegistro);
            }
        }
        $this->GenerarURL($lIDRegistro);
        return $lIDRegistro;
    }

    function Encuesta() {
        $sPTabla = $_GET["ptabla"];
        $sPFiltro = $_GET["pcolumna"];
        $lPIDRegistro = $_GET["pidregistro"];
        $sSTabla = $_GET["stabla"];
        $sSFiltro = $_GET["scolumna"];
        $aCampos = explode(",", $_GET["scampos"]);
        $lRegistros = $_GET["registros"];
        if ($_SERVER["REQUEST_METHOD"] == "GET") {
            $lTipo = 0;
            $sSQL = "DELETE FROM " . $sPTabla . " WHERE (" . $sPFiltro . "=" . $lPIDRegistro . ")";
        } else {
            $lTipo = (($lPIDRegistro == 0) ? 1 : 2);
            foreach ($_POST as $sColumna => $sValor) {
                if (strpos($sColumna, "REL_") === false)
                    $this->Concatenar($lTipo, $sColumna, $sValor);
            }
            if ($lTipo == 1)
                $sSQL = "INSERT INTO " . $sPTabla . " (" . $this->m_sColumnas . ") VALUES (" . $this->m_sValores . ")";
            else
                $sSQL = "UPDATE " . $sPTabla . " SET " . $this->m_sValores . " WHERE (" . $sPFiltro . "=" . $lPIDRegistro . ")";
        }
        $cBD = new BD();
        $lIDGenerado = $cBD->Ejecutar($sSQL);
        $cBD->Desconectar();
        if ($lTipo > 0) {
            $lPIDRegistro = (($lTipo == 1) ? $lIDGenerado : $lPIDRegistro);
            for ($lJ = 1; $lJ <= $lRegistros; $lJ++) {
                $this->m_sColumnas = "";
                $this->m_sValores = "";
                $lSIDRegistro = $_POST["REL_" . $sSFiltro . "_" . $lJ];
                $lTipo = (($lSIDRegistro == 0) ? 1 : 2);
                for ($lK = 0; $lK < count($aCampos); $lK++) {
                    $sValor = $_POST["REL_" . $aCampos[$lK] . "_" . $lJ];
                    if ($lK == 0) {
                        if ($sValor == "") {
                            $bRequerido = false;
                            break;
                        }
                        else
                            $bRequerido = true;
                    }
                    $this->Concatenar($lTipo, $aCampos[$lK], $sValor);
                }
                if (!$bRequerido) {
                    if ($lSIDRegistro > 0)
                        $sSQL = "DELETE FROM " . $sSTabla . " WHERE (" . $sSFiltro . "=" . $lSIDRegistro . ")";
                    else
                        $sSQL = "";
                }
                else {
                    if ($lTipo == 1) {
                        $this->m_sColumnas = $sPFiltro . ", " . $this->m_sColumnas;
                        $this->m_sValores = $lPIDRegistro . ", " . $this->m_sValores;
                        $sSQL = "INSERT INTO " . $sSTabla . " (" . $this->m_sColumnas . ") VALUES (" . $this->m_sValores . ")";
                    } else {
                        $this->m_sValores = $sPFiltro . "='" . $lPIDRegistro . "', " . $this->m_sValores;
                        $sSQL = "UPDATE " . $sSTabla . " SET " . $this->m_sValores;
                        $sSQL .= " WHERE (" . $sSFiltro . "=" . $lSIDRegistro . ")";
                    }
                }
                if ($bRequerido or (!$bRequerido and $lSIDRegistro > 0)) {
                    if ($sSQL <> "") {
                        $cBD = new BD();
                        $cBD->Ejecutar($sSQL);
                        $cBD->Desconectar();
                    }
                }
            }
        }
        $this->GenerarURL($lPIDRegistro);
    }

    function GenerarURL($pIDRegistro) {
        $this->m_sURL = $_GET["url"];
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            if ($_GET["parametros"] <> "") {
                $aParametros = explode(",", $_GET["parametros"]);
                for ($lJ = 0; $lJ < count($aParametros); $lJ++)
                    $this->GenerarParametros($lJ, $aParametros[$lJ], (($aParametros[$lJ] == "idregistro") ? $pIDRegistro : $_POST[$aParametros[$lJ]]));
            }
        }
    }

    function GenerarParametros($pPosicion, $pClave, $pValor) {
        $this->m_sURL .= (($pPosicion == 0) ? "?" : "&");
        $this->m_sURL .= $pClave . "=" . $pValor;
    }

    function Concatenar($pTipo, $pColumna, $pValor) {
        if (strpos($pColumna, "FILE_") === false and strpos($pColumna, "BTN_") === false and strpos($pColumna, "HIDDEN_") === false) {
            if ($pTipo == 1) {
                if ($this->m_sValores <> "") {
                    $this->m_sColumnas .= ", ";
                    $this->m_sValores .= ", ";
                }
                $this->m_sColumnas .= $pColumna;
                $this->m_sValores .= "'" . $pValor . "'";
            } else {
                if ($this->m_sValores <> "")
                    $this->m_sValores .= ", ";
                $this->m_sValores .= $pColumna . "='" . $pValor . "'";
            }
        }
    }

    function RetornarExtension($pArchivo) {
        $aExtension = explode(".", $pArchivo);
        return strtolower($aExtension[count($aExtension) - 1]);
    }

    function EsImagen($pTipo) {
        if ($pTipo == "image/gif" or $pTipo == "image/jpeg"
                or $pTipo == "image/pjpeg" or $pTipo == "image/png")
            $bImagen = true;
        else
            $bImagen = false;
        return $bImagen;
    }

    function SubirArchivo($pColumna, $pArchivo, $pNombre) {
        $pColumna = str_replace("__", "..", $pColumna);
        $aCarpeta = explode(";", $pColumna);
        $sArchivo = $aCarpeta[count($aCarpeta) - 1];
        $sArchivo .= $pNombre . "." . $this->RetornarExtension($pArchivo["name"]);
        @copy($pArchivo["tmp_name"], $sArchivo);
    }

    function SubirImagenes($pArchivo, $pNombre, $pContador, $pThumbs) {
        $cBD = new BD();
        $sSQL = "SELECT prefijo, carpeta, ancho, alto, proporcionalidad ";
        $sSQL .= "FROM config_thumbs WHERE ";
        for ($lJ = 0; $lJ < count($pThumbs); $lJ++)
            $sSQL .= (($lJ > 0) ? " OR " : "") . "idthumb=" . $pThumbs[$lJ];
        $cBD = new BD();
        $oResultado = $cBD->Seleccionar($sSQL);
        if ($pContador) {
            $this->m_lContador += 1;
            $pNombre .= "-" . $this->m_lContador;
        }
        $sExtension = $this->RetornarExtension($pArchivo["name"]);
        $pNombre .= ".jpg";
        $sArchivo = $this->m_sCarpeta . "temporal/" . $pNombre;
        @copy($pArchivo["tmp_name"], $sArchivo);
        while ($aRegistro = $cBD->RetornarFila($oResultado)) {
            switch ($sExtension) {
                case "gif":
                    $oOrigen = imagecreatefromgif($sArchivo);
                    break;
                case "png":
                    $oOrigen = imagecreatefrompng($sArchivo);
                    break;
                default:
                    $oOrigen = imagecreatefromjpeg($sArchivo);
            }
            $lAnchoOrigen = imagesx($oOrigen);
            $lAltoOrigen = imagesy($oOrigen);
            $lAlto = $lAltoOrigen;
            $lAncho = $lAnchoOrigen;
            switch ($aRegistro["proporcionalidad"]) {
                case "WH":
                    if ($lAnchoOrigen > $aRegistro["ancho"]) {
                        $lAlto = $lAltoOrigen * $aRegistro["ancho"] / $lAnchoOrigen;
                        $lAncho = $aRegistro["ancho"];
                    }
                    if ($lAlto > $aRegistro["alto"]) {
                        $lAlto = $aRegistro["alto"];
                        $lAncho = $lAnchoOrigen * $aRegistro["alto"] / $lAltoOrigen;
                    }
                    break;
                default:
                    if ($lAnchoOrigen > $aRegistro["ancho"]) {
                        $lAlto = $lAltoOrigen * $aRegistro["ancho"] / $lAnchoOrigen;
                        $lAncho = $aRegistro["ancho"];
                    }
            }
            $oDestino = imagecreatetruecolor($lAncho, $lAlto);
            imagecopyresized($oDestino, $oOrigen, 0, 0, 0, 0, $lAncho, $lAlto, $lAnchoOrigen, $lAltoOrigen);
            $sDestino = $this->m_sCarpeta . $aRegistro["carpeta"] . $aRegistro["prefijo"] . $pNombre;
            imagejpeg($oDestino, $sDestino);
            imagedestroy($oOrigen);
            imagedestroy($oDestino);
        }
        @unlink($sArchivo);
    }

}

?>
