<?php
	if($m_lIDRegistro > 0)
	{

		$sSQL = "SELECT MEmpNro, MEmpApellido, MEmpNombres, MEmpEmail, ";
		$sSQL .= "MEmpAdmin, MEmpUsuario, MEmpClave, MEmpDetalle, ";
		$sSQL .= "UniNro ";
		$sSQL .= "FROM miembroempresa WHERE MEmpNro = " . $m_lIDRegistro;

		$cBD = new BD();
		$aRegistro = $cBD->Seleccionar($sSQL, true);
	}
?>
<table width="780" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="30" class="encabezado-titulo-bg"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="20"><img src="images/espacio.gif" width="1" height="1"></td>
        <td class="encabezado-titulo-texto">Alta y modificaci&oacute;n de Usuarios </td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><img src="images/espacio.gif" width="1" height="20"></td>
  </tr>
</table>
<table width="500" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="20"><img src="images/formulario-encabezado-inicio.jpg" width="20" height="37"></td>
        <td class="formulario-encabezado-bg">Usuarios </td>
        <td width="20"><img src="images/formulario-encabezado-final.jpg" width="20" height="37"></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="4" class="formulario-contenido-inicio"><img src="images/espacio.gif" width="1" height="1"></td>
        <td><form action="abm.php?tabla=miembroempresa&columna=MEmpNro&idregistro=<?php print($m_lIDRegistro); ?>&archivo=0&url=<?php print($m_sURL); ?>" method="post" name="frmRegistro" id="frmRegistro">
          <table width="100%" border="0" cellspacing="0" cellpadding="4">
            <tr>
              <td class="formulario-etiquetas">Apellido:</td>
              <td><input name="MEmpApellido" type="text" class="formulario-textbox" id="MEmpApellido" style="width: 300px;" value="<?php print($aRegistro["MEmpApellido"]); ?>" /></td>
            </tr>
            <tr>
              <td class="formulario-etiquetas">Nombres:</td>
              <td><input name="MEmpNombres" type="text" class="formulario-textbox" id="MEmpNombres" style="width: 300px;" value="<?php print($aRegistro["MEmpNombres"]); ?>" /></td>
            </tr>
            <tr>
              <td class="formulario-etiquetas">Email:</td>
              <td><input name="MEmpEmail" type="text" class="formulario-textbox" id="MEmpEmail" style="width: 300px;" value="<?php print($aRegistro["MEmpEmail"]); ?>" /></td>
            </tr>
          <tr>
            <td class="formulario-etiquetas">Usuario:</td>
            <td width="340"><input name="MEmpUsuario" type="text" class="formulario-textbox" id="MEmpUsuario" style="width: 300px;" value="<?php print($aRegistro["MEmpUsuario"]); ?>" /></td>
          </tr>
          <tr>
            <td class="formulario-etiquetas">Clave:</td>
            <td><input name="MEmpClave" type="password" class="formulario-textbox" id="MEmpClave" style="width: 300px;" value="<?php print($aRegistro["MEmpClave"]); ?>" /></td>
          </tr>
          <tr>
            <td class="formulario-etiquetas">Detalle:</td>
            <td><input name="MEmpDetalle" type="text" class="formulario-textbox" id="MEmpDetalle" style="width: 300px;" value="<?php print($aRegistro["MEmpDetalle"]); ?>" /></td>
          </tr>
          <tr>
            <td class="formulario-etiquetas">Tipo de Usuario:</td>
            <td><select name="MEmpAdmin" id="MEmpAdmin" style="width: 300px;" <?php if (RetornarTipoUsuario() <> 1) print "disabled" ?>>
              <?php
					$sSQL = "SELECT TMiembNro, TMiembDescrip FROM tipomiembro  ";
					print(GenerarOptions($sSQL, $aRegistro["MEmpAdmin"]));
			  ?>
            </select></td>
          </tr>
          <tr>
            <td class="formulario-etiquetas">Sucursal:</td>
            <td><select name="UniNro" id="UniNro" style="width: 300px;" <?php if (RetornarTipoUsuario() <> 1) print "disabled" ?>>
                <?php
					$sSQL = "SELECT UniNro, UniNombre FROM unidadorg WHERE EmpNro = ".RetornarIdEmpresa();
					print(GenerarOptions($sSQL, $aRegistro["UniNro"], true, ""));
			  ?>
                        </select>
			</td>
          </tr>

          <tr>
            <td height="30" class="formulario-etiquetas"><img src="images/espacio.gif" width="1" height="1"></td>
            <td align="center"><input name="BTN_Guardar" type="submit" id="BTN_Guardar" value="Guardar">
              <input name="BTN_Cancelar" type="reset" id="BTN_Cancelar" value="Cancelar" onclick="history.back();"></td>
          </tr>
        </table>
        </form></td>
        <td width="6" class="formulario-contenido-final"><img src="images/espacio.gif" width="1" height="1"></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="20"><img src="images/formulario-pie-inicio.jpg" width="20" height="40"></td>
        <td class="formulario-pie-bg"><img src="images/espacio.gif" width="1" height="1"></td>
        <td width="20"><img src="images/formulario-pie-final.jpg" width="20" height="40"></td>
      </tr>
    </table></td>
  </tr>
</table>
