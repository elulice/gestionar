<form action="abm.php" method="post" id="frm_cv_datos_laborales" target="hidden_iframe">
   <div class="dlg_column_left"> 

	


      <div class="ddu_title">Objetivos Laborales</div>

	 <div class="ddu_campo_select">
	 <span>Disponibilidad:</span>
	 <select class="smallInput" name="PerDisponible">
	 <?php
   $query = "SELECT TDiNro, TDiDescrip FROM tipodisponibilidad";
   echo GenerarOptions($query, NULL);
	 ?>
	 </select>
      </div>
      <div class="ddu_campo" style="margin-top:10px;">
	 <span>Disp. Viajes:</span>
	 <input type="radio" style="width:10px;float:left;" value="1" name="PerDispViaje"><span style="float: left; width: 10px;">Si</span>
	 <input type="radio" style="float: left; width: 13px; margin-left: 10px;" checked="checked" value="0" name="PerDispViaje">
	 <span style="width: 15px; margin-left: 5px;">No</span>
      </div>
      <div class="ddu_campo">
	 <span>Puestos Event.</span>
	 <input type="radio" style="width: 10px; float: left;" value="1" name="PerAceptaEve"><span style="float: left; width: 10px;">Si</span>
	 <input type="radio" style="float: left; width: 13px; margin-left: 10px;" checked="checked" value="0" name="PerAceptaEve">
	 <span style="width: 15px; margin-left: 5px;">No</span>
      </div>
      <div class="ddu_campo" style="margin-bottom:10px;">
	 <span>Relocación:</span>
	 <input type="radio" style="width: 10px; float: left;" value="1" name="PerDispCiudad"><span style="float: left; width: 10px;">Si</span>
	 <input type="radio" style="float: left; width: 13px; margin-left: 10px;" checked="checked" value="0" name="PerDispCiudad">
	 <span style="width: 15px; margin-left: 5px;">No</span>
      </div>
      <div class="ddu_campo_textarea">
	 <span>Preferencias Laborales:</span>
         <textarea style="height: 39px;" name="PerPrefLaboral" class="smallInput"></textarea>
      </div>
      <div class="ddu_campo_textarea">
	 <span>Aspiraciones:</span>
         <textarea style="height:59px;" name="PerAspPuesto" class="smallInput"></textarea>
      </div>
      <div class="ddu_campo_select">
	 <span>Remuneración</span>
	 
	  <select class="smallInput" name="PerRemuneracion">
	 <?php
   $query = "SELECT idremuneracion, descripcion FROM remuneracion ORDER BY idremuneracion";
   echo GenerarOptions($query, NULL);
	 ?>
	 </select>
	 
      </div>
      <?php /*
      <div class="ddu_campo">
	 <span>Teléfono:</span>
	 <input type="text" class="smallInput" name="CliRsocial" />
      </div>
      <div class="ddu_campo">
	 <span>Teléfono Msjs:</span>
	 <input type="text" class="smallInput" name="CliRsocial" />
      </div>
      */ ?>
   </div>
   <div class="dlg_column_right">
      <div class="ddu_title">Áreas de Interés</div>
      <div class="ddu_campo_select">
	 <span>Área 1:</span>
	 <select class="smallInput" name="AreNro1">
	 <?php
   $query = "SELECT AreNro, AreNom FROM area ORDER BY AreNom";
   echo GenerarOptions($query, NULL);
	 ?>
	 </select>
      </div>
      <div class="ddu_campo_select">
	 <span>Área 2:</span>
	 <select class="smallInput" name="AreNro2">
	 <?php
   $query = "SELECT AreNro, AreNom FROM area ORDER BY AreNom";
   echo GenerarOptions($query, NULL);
	 ?>
	 </select>
      </div>
      <div class="ddu_campo_select">
	 <span>Área 3:</span>
	 <select class="smallInput" name="AreNro3">
	 <?php
   $query = "SELECT AreNro, AreNom FROM area ORDER BY AreNom";
   echo GenerarOptions($query, NULL);
	 ?>
	 </select>
      </div>
      <div class="ddu_title">Conocimientos</div>    
      <div class="ddu_campo">
	 <span>Inglés</span>
	 <input type="radio" style="width: 10px; float: left;" value="1" name="PerIngles">
	 <span style="float: left; width: 10px;">Si</span>
	 <input type="radio" style="float: left; width: 13px; margin-left: 10px;" checked="checked" value="0" name="PerIngles">
	 <span style="width: 15px; margin-left: 5px;">No</span>
      </div>
      <div class="ddu_campo_doble">
	 <div class="ddu_campo_col_izq">
	    <div class="ddu_campo">
	       <span>Habla:</span>
	       <select class="smallInput" name="PerInglesHabla" style="width:85px;float:right;">
	       <?php
   $query = "SELECT idnivel, nombre FROM _nivel_idioma WHERE idnivel > 1 ORDER BY idnivel";
   echo GenerarOptions($query, NULL);
	       ?>
	       </select>
	    </div>
	 </div>
	 <div class="ddu_campo_col_der">
	    <div class="ddu_campo">
	       <span>Escribe:</span>
	       <select class="smallInput" name="PerInglesEscribe" style="float:right;width:85px;">
	       <?php
   $query = "SELECT idnivel, nombre FROM _nivel_idioma WHERE idnivel > 1 ORDER BY idnivel";
   echo GenerarOptions($query, NULL);
	       ?>
	       </select>
	    </div>
	 </div>
      </div>
      <div class="ddu_campo_textarea" style="margin-top:10px;">
	 <span>Otros Idiomas:</span>
         <textarea style="height:39px;" name="PerIdiomas" class="smallInput"></textarea>
      </div>
      <div class="ddu_campo_textarea">
	 <span>Otros Conocimientos:</span>
         <textarea style="height:39px;" name="PerOtrosConoc" class="smallInput"></textarea>
      </div>
   </div>
</form>
