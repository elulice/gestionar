<?php 
	$sSQL = "SELECT PerApellido, PerMombres ";
	$sSQL .= "FROM persona ";
	$sSQL .= "WHERE PerNro = " . $m_lIDPostulante;
	$cBD = new BD();
	$aPostulante = $cBD->Seleccionar($sSQL, true);

?>
<table width="780" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="30" class="encabezado-titulo-bg"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="20"><img src="images/espacio.gif" width="1" height="1" /></td>

        <td valign="top" class="encabezado-titulo-texto" style="padding-top:5px;">Listado de Documentaci&oacute;n - 
		<?php print $aPostulante["PerApellido"]." ".$aPostulante["PerMombres"]; ?></td>
        </tr>
    </table></td>
  </tr>
  <tr>
    <td><img src="images/espacio.gif" width="1" height="20"></td>
  </tr>
</table>
<table width="780" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="20"><img src="images/listado-encabezado-inicio.jpg" width="20" height="37"></td>
        <td class="listado-encabezado-bg"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="100"><img src="images/espacio.gif" width="1" height="1"></td>
            <td width="450" class="listado-encabezado-texto">Documento</td>
            <td width="50" align="left" class="listado-encabezado-texto">Fecha</td>
            <td align="center" class="listado-encabezado-texto">Ver</td>
          </tr>
        </table></td>
        <td width="20"><img src="images/listado-encabezado-final.jpg" width="20" height="37"></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="5" class="listado-contenido-inicio"><img src="images/espacio.gif" width="1" height="1"></td>
        <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
		  <?php
		  		$lRegistros = 0;
			 	$sSQL = "SELECT d.DNro, d.DFecha, td.TDNombre, d.DNombre ";
				$sSQL .= "FROM documentacion d ";
				$sSQL .= "INNER JOIN tipodocumentacion td ON td.TDNro = d.TDNro ";
				$sSQL .= "WHERE d.PerNro = ".$m_lIDPostulante." ";
				$sSQL .= "ORDER BY DFecha DESC ";
				//		echo $sSQL;
				$cBD = new BD();
				$oResultado = $cBD->Seleccionar($sSQL);
				while($aRegistro = $cBD->RetornarFila($oResultado))
				{
					$sPosicion = (($sPosicion == "1") ? "2" : "1");
					
		  ?>
          <tr>
            <td class="listado-fila-bg-<?php print($sPosicion); ?>">
			
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="15"><img src="images/espacio.gif" width="1" height="1"></td>
                <td width="100">
				
				<table width="90" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="30"><a href="am-curriculums-documentos.php?idregistro=<?php print($aRegistro["DNro"]); ?>&idpostulante=<?php print $m_lIDPostulante; ?>&url=<?php print($m_sURL); ?>"><img src="images/btn-modificar-<?php print($sPosicion); ?>.jpg" alt="Editar" width="24" height="23" border="0"></a></td>

                    <td><a href="abm.php?tabla=documentacion&columna=DNro&idregistro=<?php print($aRegistro["DNro"]); ?>&url=<?php print($m_sURL); ?>" onclick="return confirm('&iquest;Desea eliminar este Documento?')"><img src="images/btn-eliminar-<?php print($sPosicion); ?>.jpg" alt="Eliminar" width="24" height="23" border="0"></a></td>
                  </tr>
                </table>				</td>
                <td width="450" class="listado-texto"><?php print($aRegistro["TDNombre"]); ?></td>
                <td width="50" align="left" class="listado-texto"><?php print(date("d/m/Y", $aRegistro["DFecha"])); ?></td>
                <td align="center" class="listado-texto">
				<?php 
					$sExtension = explode(".", $aRegistro["DNombre"]);
					$sArchivo = "../Documentacion/".$aRegistro["DNro"].".". $sExtension[count($sExtension)-1];
					if (file_exists($sArchivo))
					{
				?>
				<a href="<?php print $sArchivo; ?>" target="_blank"><img src="images/btn-doc.png" border="0" /></a>
				<?php } else { ?>
				<img src="images/btn-nolink.png" />
				<?php }
				?>				</td>
              </tr>
              
            </table></td>
          </tr>
          <?php
			 		$lRegistros++;
				}
				if($lRegistros == 0)
				{
			 ?>
          <tr>
            <td><img src="images/espacio.gif" width="1" height="20"></td>
          </tr>
			 <?php } ?>
        </table></td>
        <td width="6" class="listado-contenido-final"><img src="images/espacio.gif" width="1" height="1"></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="100"><a href="am-curriculums-documentos.php?idregistro=0&idpostulante=<?php print $m_lIDPostulante; ?>&url=<?php print($m_sURL); ?>"><img src="images/listado-pie-inicio.jpg" alt="Agregar" width="100" height="40" border="0"></a></td>
        <td class="listado-pie-bg">&nbsp;</td>
        <td width="20"><img src="images/listado-pie-final.jpg" width="20" height="40"></td>
      </tr>
    </table></td>
  </tr>
</table>
