  <link href="estilos/general.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" type="text/css" href="css/jquery-ui-1.8.10.custom.css" />
  <link rel='stylesheet' type='text/css' href='css/redmond/ui.css' />
  
   <style type="text/css">

   .portlet-header span img {
      margin:0 10px 0 5px;
   }

   .portlet-header span {
      cursor:default;
   }

   div.menu_escritorio {
      width:90px;
      height:23px;
      float:right;
      margin-right:20px;
      margin-top:-3px;
   }

   div.menu_escritorio a {
      background-image:url("images/menu/desktop_small.png");
      background-repeat:no-repeat;
      padding-left:30px;
      font-weight:bold;
      font-style:italic;
      line-height:23px;
      text-decoration:none;
      color:white;
      display:block;
   }

   div.menu_escritorio a:hover {
      text-decoration:underline;
   }

   a.custom_menu {
      background-repeat:no-repeat;
      padding-left:21px;
      cursor:pointer;
      float:right;
      margin-right:20px;
      font-size:10px;
      padding-top:2px;
   }



	 div.dlg_column_left { float:left;width:360px; }
	 div.dlg_column_right { float:left;margin-left:20px; }
	 div.ddu_title { border-style:solid; border-width:0 0 1px 0; text-indent:5px; margin-bottom:10px;text-align:left; }
	 div.ddu_title span { font-weight:bold;text-align:left; }
	 div.ddu_campo { height:20px; margin:5px 0 0 5px; }
	 div.ddu_campo_select { height:23px; margin:5px 0 0 5px;text-align:left; }
	 div.ddu_campo_select select { width:253px;position:absolute; }
	 div.ddu_campo_select span { width:100px; float:left;text-align:left; }
	 div.ddu_campo_radio { height:22px; margin:10px 0 4px 5px; }
	 div.ddu_campo_radio span { width:100px; float:left;margin-left:5px; }
	 div.ddu_campo_radio input { width:10px; float:left; }
	 div.ddu_campo_textarea { margin:5px 0 0 5px; }
	 div.ddu_campo_textarea textarea { width:245px; }
	 div.ddu_campo_textarea span { width:100px; float:left;text-align:left; }
	 div.ddu_campo_file { height:25px; margin:10px 0 4px 5px; }
	 div.ddu_campo_file span { width:100px; float:left;text-align:left; }
	 div.ddu_campo_col_izq { float:left;width:190px; }
	 div.ddu_campo_col_der { float:left;width:170px; }
	 div.ddu_campo_col_der div.ddu_campo span { width:70px;margin-left:10px; }
	 div.ddu_campo_col_der div.ddu_campo input { width:75px; }
	 div.ddu_campo_col_izq div.ddu_campo input { width:75px; }
	 div.ddu_campo_doble { width:360px; height:28px; }
	 div.ddu_campo_comandos { width:360px;height:48px;margin-top:10px;padding-top:10px;text-align:right; }
	 div.ddu_campo input { width:245px; }
	 div.ddu_campo span { width:100px; float:left;text-align:left; }
	  div.ddu_campo_g{ width:596px; height:69px; margin:0px 0 0 5px; }
	 .ddu_campo_g1{ width:589px; height:68px; overflow:none; }
	 .ddu_campo-grilla { height:367px; margin:5px 0 0 0px; }
	 .texto-grillas{ height:16px; background-color:#efefef; color:#cc6600; font-weight:bold; float:left; padding:2px 0px 2px 2px; width:145px; border-right:1px solid #CCC; font-family:Arial, Helvetica, sans-serif; font-size:11px;}
	 
	 .texto-grilla-gris{ height:16px;  color:#555;  float:left; padding:2px 2px 2px 5px;width:90px; border-right:1px solid #CCC; border-left:1px solid #CCC; font-family:Arial, Helvetica, sans-serif; font-size:11px;}
	 .fila{ float:left;  height:20px; border:1px solid #CCC; width:755px;}

	 .columna-izq{
		 float:left;
		 width:374px;
		 margin-top:10px;
		 }



   /* seccion curriculums dialog asignacioes */
   fieldset { border:#eeeeee 1px groove;padding:10px 0 10px 0; }
   fieldset legend { padding:0 5px 0 5px; }
   form label { float:left;width:80px;margin-left:30px;height:40px; }
   form fieldset div.fila { width:100%;margin-bottom:5px;border-width:0!important; }
   form fieldset div.fila_comandos { text-align:right;margin:5px 22px 0 0; }
   form fieldset input { width:120px;float:left; }
   form fieldset select { width:128px;float:left; }

   /* seccion opciones */
   #accordion_opciones td.label{ width:130px;text-align:center; }
   #accordion_opciones td button{ width:130px; }


      </style>
  <? 
  include("clases/framework-1.0/class.bd.php");
	include("includes/funciones.php");
  $ids=$_GET['id'];
  $sSQL = "SELECT * FROM `datospuesto` WHERE DPueNro=$ids";
	$cBD = new BD();
	$riw = $cBD->Seleccionar($sSQL,true); ?>
   <div style="width:361px;margin:auto;">
   
   <form id="frm_cv_xp_laboral" method="post" >
   <input type="hidden" name="DPueNro"  value="<? echo $riw['DPueNro'] ?>"/>
   <div class="ddu_title">Experiencia Laboral</div>
   <div class="ddu_campo_select">
      <span>Area:</span>
      <select class="smallInput" name="AreNro">
      <?php
   $query = "SELECT AreNro, AreNom FROM area";
   echo GenerarOptions($query, $riw['AreNro']);
      ?>
      </select>
   </div>
   <div class="ddu_campo_select">
      <span>Puesto:</span>
      <select class="smallInput" name="PueNro">
      <?php
   $query = "SELECT PueNro, PueNom FROM puesto";
   echo GenerarOptions($query, $riw['PueNro']);
      ?>
      </select>
   </div>
   <div class="ddu_campo">
      <span>Cargo:</span>
      <input type="text" class="smallInput" name="PueCargos" value="<? echo $riw['PueCargos'] ?> "  />
   </div>
   <div class="ddu_campo">
      <span>Empresa:</span>
      <input type="text" class="smallInput" name="PueUltimaEmpresa" value="<? echo $riw['PueUltimaEmpresa'] ?> "  />
   </div>
   <div class="ddu_campo">
      <span>Rubro:</span>
      <input type="text" class="smallInput" name="PueEmpRubro" value="<? echo $riw['PueEmpRubro'] ?> "  />
   </div>
   <div class="ddu_campo">
      <span>Remuneraci&oacute;n:</span>
	   <select class="smallInput" name="PueRemuneracion">
      <?php
   $query = "SELECT  idremuneracion, descripcion FROM remuneracion";
   echo GenerarOptions($query, $riw['PueRemuneracion']);
      ?>
      </select>
   </div>
   <div class="ddu_campo_textarea">
      <span>Tareas Realizadas:</span>
      <textarea type="text" class="smallInput" name="PueDescPuesto"><? echo $riw['PueDescPuesto'] ?> </textarea>
   </div>
   <div class="ddu_campo_doble">
      <div class="ddu_campo_col_izq">
	 <div class="ddu_campo">
	    <span>Fecha Alta:</span>
	    <input type="text" name="PueFecha"  class="smallInput" style="width:75px;"  value="<? echo date("d-m-Y", strtotime($riw["PueFecha"])) ?> "  />
	 </div>
      </div>
      <div class="ddu_campo_col_der">
	 <div class="ddu_campo">
	    <span>Fecha Baja:</span>
	    <input type="text" name="PueFechaBaja" class="smallInput" style="width:75px;" value="<? echo date("d-m-Y", strtotime($riw["PueBaja"]))?> "  />
	 </div>
      </div>
   </div>
   <div class="ddu_campo_doble">
      <div class="ddu_campo_col_izq">
	 <div class="ddu_campo">
	    <span>Antig&uuml;edad A&ntilde;os:</span>
	    <input type="text" name="PueExpAnos" class="smallInput" value="<? echo $riw['PueExpAnos'] ?> " />
	 </div>
      </div>
      <div class="ddu_campo_col_der">
	 <div class="ddu_campo">
	    <span>Meses:</span>
	    <input type="text" name="PueExpMeses" class="smallInput" value="<? echo $riw['PueExpMeses'] ?> "  />
	 </div>
      </div>
   </div>
   <div class="ddu_campo">
      <span>Causa Baja:</span>
      <input type="text" name="PueCausaBaja" class="smallInput" value="<? echo $riw['PueCausaBaja'] ?>"  />
   </div>
   </form>
</div>
