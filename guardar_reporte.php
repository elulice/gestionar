<?php
session_start();
include("includes/funciones.php");
include("clases/framework-1.0/class.bd.php");

if ($_GET['q']) {
    if ($_GET['q'] == 'template') {
        foreach ($_POST as $key => $pos) {
            if ($pos == '' || ($pos == '0' && strpos($pos, 'f_') == 0))
                unset($_POST[$key]);
            if ($pos == 'on')
                $_POST[$key] = 1;
        }
        $nombre = $_POST['nombre_reporte'];
        unset($_POST['nombre_reporte']);
        $id = intval($_POST['id_reporte']);
        unset($_POST['id_reporte']);
//$nombre = 'Prueba';
        $bd = new BD();
        $reporte = serialize($_POST);
//        die(var_dump($reporte));
        //die("UPDATE reportes SET reporte='$reporte' WHERE id = '{$id}' ");
        if ($id > 0)
            $bd->Ejecutar("UPDATE reportes SET reporte='$reporte' WHERE id = '{$id}' ");
        else
            $id = $bd->Ejecutar("INSERT INTO reportes (reporte, id_usuario, nombre) VALUES ('$reporte', '{$_SESSION["PerNro"]}', '$nombre')");
        die('Se ha guardado con exito! <script type="text/javascript"> $("[name=id_reporte]").val(' . $id . ')');
    }
    if ($_GET['q'] == 'reporte') {
        ob_start();
        //header("content-type: text/xml");
        $nombre = $_POST['nombre_reporte'];
        unset($_POST['nombre_reporte']);
        $id = intval($_POST['id_reporte']);
        unset($_POST['id_reporte']);

        if (!file_exists('reportes')) {
            mkdir('reportes');
        }
        if (!file_exists('reportes/' . $_SESSION['IDUsuario'])) {
            mkdir('reportes/' . $_SESSION['IDUsuario']);
        }

        $campos = array();
        $filtros = array();
        $totales = array();
        $order = array();

        foreach ($_POST as $key => $pos) {
            if ($pos == '0' || trim($pos) == '')
                continue;
            if (strpos($key, 'f_') === 0) {
                $filtros[str_replace('f_', '', $key)] = $pos;
                continue;
            }
            if (strpos($key, 'totales_') === 0) {
                $totales[$key] = $pos;
                continue;
            }
            if (strpos($key, 'o_') === 0) {
                $order[str_replace('o_', '', $key)] = $pos;
                continue;
            }
            $campos[] = $key;
        }
        foreach ($filtros as $key => $fil) {
            if (strpos($key, 'fecha') === 0) {
                if (strpos($key, '_d', strlen($key) - 3) !== FALSE) {
                    $newkey = str_replace('_d', '', $key, intval(strlen($key) - 3));
                    $tmp = $newkey . '_h';
                    if (isset($filtros[$tmp]))
                        $hasta = $filtros[$tmp];
                    else
                        $hasta = date('Y-m-d');
                    if (strpos($fil, '/') !== FALSE) {
                        $new = explode('/', $fil);
                        $fil = $new[2] . '-' . $new[1] . '-' . $new[0];
                    }
                    if (strpos($hasta, '/') !== FALSE) {
                        $new = explode('/', $hasta);
                        $hasta = $new[2] . '-' . $new[1] . '-' . $new[0];
                    }
                    $filtros[$newkey] = array('desde' => $fil, 'hasta' => $hasta);
                    unset($filtros[$key], $filtros[$tmp]);
                    continue;
                }
                if (strpos($key, '_h', strlen($key) - 3) !== FALSE) {
                    $newkey = str_replace('_h', '', $key, intval(strlen($key) - 3));
                    $tmp = $newkey . '_d';
                    if (!isset($filtros[$tmp]))
                        unset($filtros[$key]);
                    continue;
                }
            }
            if (strpos($key, '_v', strlen($key) - 3) !== FALSE) {
                $newkey = str_replace('_v', '', $key, intval(strlen($key) - 3));
                $tmp = $newkey . '_c';
                if (isset($filtros[$tmp]))
                    $comp = $filtros[$tmp];
                else
                    $comp = 0;
                $filtros[$newkey] = array('valor' => $fil, 'comp' => $comp);
                unset($filtros[$key], $filtros[$tmp]);
                //die($key);
            }
            if (strpos($key, '_c', strlen($key) - 3) !== FALSE) {
                $newkey = str_replace('_c', '', $key, intval(strlen($key) - 3));
                $tmp = $newkey . '_v';
                if (!isset($filtros[$tmp]))
                    unset($filtros[$key]);
                continue;
            }
        }
	
	$campos_externos = array(
	   'estado'	=> 'esta.estado',
	   'id_agente'	=> 'est.agente',
	   'gerente' => 'est.gerente',
	   'director' => 'est.director',
	   'frecuencia_pago' => 'fre.frecuencia',
	   'id_plan' => 'CONCAT(plan.codigo_plan, ", ", prod.codigo_producto) AS plan',
	   'nombres' => 'CONCAT(pol.apellido, ", ", pol.nombres) AS nombres',
	   'pais' => 'pai.pais',
	   'condicion' => 'cond.condicion',
	   'forma_pago' => 'form.forma',
	   'id_moneda' => 'mon.moneda',
	   'id_compania' => 'comp.com_codigo'
	);

        $where = '';
        $comparadores = array('>', '<', '=');
        foreach ($filtros as $key => &$filtro) {
            if (is_array($filtro)) {
                if (array_key_exists('desde', $filtro)) {
                    if (strpos($filtro['desde'], '/') !== FALSE) {
                        $new = explode('/', $filtro['desde']);
                        //die(var_dump($new));
                        $filtro['desde'] = $new[2] . '-' . $new[1] . '-' . $new[0];
                    }
                    if (strpos($filtro['hasta'], '/') !== FALSE) {
                        $new = explode('/', $filtro['hasta']);
                        //die(var_dump($new));
                        $filtro['hasta'] = $new[2] . '-' . $new[1] . '-' . $new[0];
                    }
                    $where .= ' AND DATE(pol.' . $key . ') BETWEEN DATE(\'' . $filtro['desde'] . '\') AND DATE(\'' . $filtro['hasta'] . '\')';
                    continue;
                }
                if (array_key_exists('valor', $filtro)) {
                    $where .= ' AND pol.' . $key . ' ' . $comparadores[$filtro['comp']] . ' \'' . $filtro['valor'] . '\'';
                    continue;
                }
            }
            if ($key == 'gerente' || $key == 'director' || $key == 'director_general') {
                $where .= ' AND est.' . $key . ' = \'' . $filtro . '\'';
                continue;
            }
            if ($key == 'fumador')
                $filtro = $filtro - 1;
            $where .= ' AND pol.' . $key . ' = \'' . $filtro . '\'';
        }

        $totales_temp = array();
        foreach ($totales as $key => &$total) {
            if (strpos($key, '_func', strlen($key) - 6) !== FALSE) {
                $newkey = str_replace('_func', '', $key, intval(strlen($key) - 6));
                $tmp = $newkey . '_cam';
                if (isset($totales[$tmp]))
                    $comp = $totales[$tmp];
                else
                    continue;
                $totales_temp[] = array('funcion' => $total, 'campo' => $comp);
                unset($totales[$key], $totales[$tmp]);
                //die($key);
            }
        }

//                    $campos_select = array('estado' => 'esta.estado', 'id_agente' => 'est.agente', 'gerente' => 'est.gerente', 'director' => 'est.director',
//                        'frecuencia_pago' => 'fre.frecuencia', 'id_plan' => 'CONCAT(prod.codigo_producto, ", ", plan.codigo_plan) AS plan',
//                        'nombres' => 'CONCAT(pol.nombres, ", ", pol.apellido) AS nombres', 'pais' => 'pai.pais', 'condicion' => 'cond.condicion',
//                        'forma_pago' => 'form.forma', 'id_moneda' => 'mon.moneda', 'id_compania' => 'comp.com_nombre');

        $fields = array('estado' => array('label' => 'Estado', 'campo' => 'estado'), 'id_agente' => array('label' => 'Agente', 'campo' => 'agente'),
            'gerente' => array('label' => 'Gerente', 'campo' => 'gerente'), 'director' => array('label' => 'Director', 'campo' => 'director'),
            'frecuencia_pago' => array('label' => 'Frecuencia', 'campo' => 'frecuencia'), 'id_plan' => array('label' => 'Producto y Plan', 'campo' => 'plan'),
            'forma_pago' => array('label' => 'Forma de Pago', 'campo' => 'forma'), 'id_moneda' => array('label' => 'Moneda', 'campo' => 'moneda'),
            'id_compania' => array('label' => 'Compañía', 'campo' => 'com_codigo'), 'fumador' => array('label' => 'Fumador', 'campo' => 'fumador'),
            'nombres' => array('label' => 'Apellido y Nombre', 'campo' => 'nombres'), 'fecha_nacimiento' => array('label' => 'Fecha de Nacimiento', 'campo' => 'fecha_nacimiento'),
            'fecha_envio' => array('label' => 'Fecha de Envio', 'campo' => 'fecha_envio'), 'suma_asegurada' => array('label' => 'Suma Asegurada', 'campo' => 'suma_asegurada'),
            'prima_inicial' => array('label' => 'Prima Inicial', 'campo' => 'prima_inicial'), 'prima_planeada' => array('label' => 'Planeada Anual', 'campo' => 'prima_planeada'),
            'prima_adicional' => array('label' => 'Prima Adicional', 'campo' => 'prima_adicional'), 'prima_unica' => array('label' => 'Prima Única', 'campo' => 'prima_unica'),
            'prima_target' => array('label' => 'Prima Target', 'campo' => 'prima_target'), 'condicion' => array('label' => 'Condicion', 'campo' => 'condicion'),
            'pais' => array('label' => 'País', 'campo' => 'pais'), 'fecha_aprobacion' => array('label' => 'Fecha de Aprobacion', 'campo' => 'fecha_aprobacion'),
            'fecha_recibido' => array('label' => 'Fecha de Recibido', 'campo' => 'fecha_recibido'),
            'fecha_vigencia' => array('label' => 'Fecha de Vigencia', 'campo' => 'fecha_vigencia'));

        $clean_select = "";
        $having = "";
        foreach ($campos as $campo) {
            if (array_key_exists($campo, $campos_externos)) {
                $campos_select[] = $campos_externos[$campo];
                if (strlen($having) == 0) {
                    $having .= " HAVING " . $fields[$campo]['campo'] . " != '0' AND " . $fields[$campo]['campo'] . " != '' AND " . $fields[$campo]['campo'] . " IS NOT NULL";
                }else
                    $having .= " AND " . $fields[$campo]['campo'] . " != '0' AND " . $fields[$campo]['campo'] . " != '' AND " . $fields[$campo]['campo'] . " IS NOT NULL";
            } else {
                $campos_select[] = 'pol.' . $campo;
                if ($campo != 'fumador')
                    $clean_select .= " AND (pol." . $campo . " != '0' AND pol." . $campo . " != '' AND pol." . $campo . " != '0000-00-00' AND pol." . $campo . " IS NOT NULL)";
            }
        }
        $campos_select = implode(', ', $campos_select);
//die("Filtros: " . var_export($filtros, true) . "\n Totales: " . var_export($totales, true) . "\n Campos: " . $campos_select);

        $return_filters = array();
        $compa = array('Mayor que', 'Menor que', 'Igual a');
        foreach ($filtros as $key => &$filtro) {
            if (is_array($filtro)) {
                if (array_key_exists('desde', $filtro)) {
                    $return_filters[] = $fields[$key]['label'] . ' Desde: ' . date('d/m/Y', strtotime($filtro['desde'])) . ' Hasta: ' . date('d/m/Y', strtotime($filtro['hasta']));
                    continue;
                }
                if (array_key_exists('valor', $filtro)) {

                    $return_filters[] = $fields[$key]['label'] . ' ' . $compa[$filtro['comp']] . ' $' . $filtro['valor'];
                    continue;
                }
            }
            if ($key == 'fumador') {
                $return_filters[] = $fields[$key]['label'] . ': ' . (($filtro) ? "Si" : "No");
                continue;
            }
            $return_filters[] = $fields[$key]['label'];
        }
        $return_filters = implode(', ', $return_filters);

        $funciones = array(2 => 'SUM(', 3 => 'AVG(', 4 => 'MAX(', 5 => 'MIN(');
        $f2 = array(2 => 'Suma ', 3 => 'Promedio ', 4 => 'Maximo ', 5 => 'Minimo ');
        $computables = array(0 => 'Campo', 1 => 'suma_asegurada', 2 => 'prima_inicial',
            3 => 'prima_planeada', 4 => 'prima_target', 5 => 'prima_adicional', 6 => 'prima_unica');
        //$sSQL = array();
        $resultados = array();
        foreach ($totales_temp as $total) {
            $sSQL = 'SELECT ' . $funciones[$total['funcion']] . $computables[$total['campo']] . ') as resultado
                FROM Solicitudes_Detalle pol
                LEFT JOIN solicitud_estado esta ON esta.id = pol.estado
                LEFT JOIN estructura_comercial est ON est.id_agente = pol.id_agente
                LEFT JOIN companias comp ON comp.idcompania = pol.id_compania
                LEFT JOIN productos prod ON prod.id_producto = pol.id_producto 
                LEFT JOIN planes plan ON plan.id_plan = pol.id_plan
                LEFT JOIN paises pai ON pai.id = pol.pais
                LEFT JOIN monedas mon ON mon.idmoneda = pol.id_moneda
                LEFT JOIN frecuencia_pago fre ON fre.id_frecuencia = pol.frecuencia_pago
                LEFT JOIN forma_pago form ON form.id = pol.forma_pago
                LEFT JOIN solicitud_condicion cond ON cond.id = pol.condicion 
                WHERE id_solicitud !=  0 ' . $where;
            $cBD = new BD();
            $tmp = $cBD->Seleccionar($sSQL, true);
            $resultados[] = array('label' => $f2[$total['funcion']] . $fields[$computables[$total['campo']]]['label'], 'resultado' => $tmp['resultado']);
        }

        $sSQL = 'SELECT ' . $campos_select . '
                FROM Solicitudes_Detalle pol
                LEFT JOIN solicitud_estado esta ON esta.id = pol.estado
                LEFT JOIN estructura_comercial est ON est.id_agente = pol.id_agente
                LEFT JOIN companias comp ON comp.idcompania = pol.id_compania
                LEFT JOIN productos prod ON prod.id_producto = pol.id_producto 
                LEFT JOIN planes plan ON plan.id_plan = pol.id_plan
                LEFT JOIN paises pai ON pai.id = pol.pais
                LEFT JOIN monedas mon ON mon.idmoneda = pol.id_moneda
                LEFT JOIN frecuencia_pago fre ON fre.id_frecuencia = pol.frecuencia_pago
                LEFT JOIN forma_pago form ON form.id = pol.forma_pago
                LEFT JOIN solicitud_condicion cond ON cond.id = pol.condicion 
                WHERE id_solicitud !=  0 ' . $where . ' 
                ORDER BY ' . ((array_key_exists('order', $order)) ? ((isset($campos_externos[$order['order']]) && ($order['order'] != 'nombres' && $order['order'] != 'id_plan')) ? $campos_externos[$order['order']] : $fields[$order['order']]['campo']) : 'fecha_envio') . ' 
                ' . ((array_key_exists('progresion', $order)) ? $order['progresion'] : 'DESC');

        echo '<?xml version="1.0" encoding="UTF-8"?>';
        ?>
        <reporte>
            <tabla>
                <cabecera>
                    <?php foreach ($campos as $campo): ?>
                        <campo><?php echo $fields[$campo]['label']; ?></campo>
                    <?php endforeach; ?>
                </cabecera>
                <cuerpo>
                    <?php
                    //die($sSQL);
                    $cBD = new BD();
                    $result = $cBD->Seleccionar($sSQL);

                    while ($aRegistro = $cBD->RetornarFila($result)) {
                        //die(var_dump($aRegistro))
                        ?>
                        <fila>
                            <?php foreach ($campos as $campo): ?>
                                <campo><?php
                if ($campo == 'fumador')
                    echo (($aRegistro[$fields[$campo]['campo']]) ? "Si" : "No");
                elseif (strpos($campo, 'fecha') === 0 && $campo != '0000-00-00')
                    echo date('d/m/Y', strtotime($aRegistro[$fields[$campo]['campo']]));
                else
                    echo $aRegistro[$fields[$campo]['campo']];
                                ?></campo>
                            <?php endforeach; ?>
                        </fila>
                    <?php } ?>
                </cuerpo>
            </tabla>
            <total><?php echo mysql_num_rows($result); ?></total>
            <resultados>
                <?php foreach ($resultados as $resu) { ?>
                    <resultado>
                        <nombre><?php echo $resu['label'] ?></nombre>
                        <valor><?php echo money_format('%.2n', $resu['resultado']); ?></valor>
                    </resultado>
                    <?php
                }
                ?>
            </resultados>
            <filtros><?php echo $return_filters; ?></filtros>
        </reporte>
        <?php
        $xml = ob_get_clean();
        ob_end_clean();
        //die($nombre);
        file_put_contents('reportes/' . $_SESSION['IDUsuario'] . '/' . $nombre . '.xml', $xml);
        die("Guardado con exito!");
    }
}
?>
