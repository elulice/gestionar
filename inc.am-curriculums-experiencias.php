<?php

	$sSQL = "SELECT PerApellido, PerNombres ";
	$sSQL .= "FROM persona ";
	$sSQL .= "WHERE PerNro = " . $m_lIDPostulante;
	$cBD = new BD();
	$aPostulante = $cBD->Seleccionar($sSQL, true);

	if($m_lIDRegistro > 0)
	{
		$sSQL = "SELECT e.* ";
		$sSQL .= "FROM datospuesto e ";
		$sSQL .= "WHERE e.DPueNro = " . $m_lIDRegistro;
		$cBD = new BD();
		$aRegistro = $cBD->Seleccionar($sSQL, true);
	}
	$aRegistro["PueFecha"] = explode("-", $aRegistro["PueFecha"]);
	$aRegistro["PueFechaBaja"] = explode("-", $aRegistro["PueFechaBaja"]);
?>

<script>
	function corrige(pForm)
	{
		with(pForm)
		{
			PueFecha.value = HIDDEN_alta_a.value + "-" + HIDDEN_alta_m.value + "-00";
			PueFechaBaja.value = HIDDEN_baja_a.value + "-" + HIDDEN_baja_m.value + "-00";
		}
		return true;
	}
</script>
<link href="estilos/general.css" rel="stylesheet" type="text/css" />


<table width="780" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="30" class="encabezado-titulo-bg"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="20"><img src="images/espacio.gif" width="1" height="1"></td>
        <td class="encabezado-titulo-texto">Alta y modificaci&oacute;n de Experiencias Laborales </td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><img src="images/espacio.gif" width="1" height="20"></td>
  </tr>
</table>
<table width="580" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="20"><img src="images/formulario-encabezado-inicio.jpg" width="20" height="37"></td>
        <td class="formulario-encabezado-bg">Experiencia Laboral - <?php print $aPostulante["PerApellido"]." ".$aPostulante["PerNombres"]; ?> </td>
        <td width="20"><img src="images/formulario-encabezado-final.jpg" width="20" height="37"></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="4" class="formulario-contenido-inicio"><img src="images/espacio.gif" width="1" height="1"></td>
        <td><form action="abm.php?tabla=datospuesto&columna=DPueNro&idregistro=<?php print($m_lIDRegistro); ?>&url=<?php print($m_sURL); ?>" method="post" name="frmRegistro" id="frmRegistro" onsubmit="corrige(this);">
		
		
<input name="PerNro" type="hidden" value="<?php print $m_lIDPostulante; ?>" />
<input name="EmpNro" type="hidden" value="<?php print RetornarIdEmpresa(); ?>" />

          <table width="100%" border="0" cellspacing="0" cellpadding="4">
          <tr>
            <td class="formulario-etiquetas">&Aacute;rea:</td>
            <td width="340"><select name="AreNro" id="AreNro" style="width: 300px;" onchange="cargarListas(this, 'PueNro')">
              <?php
					$sSQL = "SELECT AreNro, AreNom FROM area  ";
					print(GenerarOptions($sSQL, $aRegistro["AreNro"]));
			  ?>
            </select></td>
          </tr>
          <tr>
            <td class="formulario-etiquetas">Puesto:</td>
            <td width="340">
            <select name="PueNro" id="PueNro" style="width: 300px;">
              <?php
					$sSQL = "SELECT PueNro, PueNom FROM puesto  ";
					print(GenerarOptions($sSQL, $aRegistro["PueNro"]));
			  ?>
            </select>
            </td>
          </tr>
          <tr>
            <td class="formulario-etiquetas">Cargo:</td>
            <td class="formulario-textbox"><input name="PueCargos" type="text" class="formulario-textbox" id="PueCargos" style="width: 300px;" value="<?php print($aRegistro["PueCargos"]); ?>" maxlength="64" maxlenght="64" /></td>
          </tr>
          <tr>
            <td class="formulario-etiquetas">Empresa donde trabaj&oacute;:</td>
            <td><input name="PueUltimaEmpresa" type="text" class="formulario-textbox" id="PueUltimaEmpresa" style="width: 300px;" value="<?php print($aRegistro["PueUltimaEmpresa"]); ?>" maxlength="64" maxlenght="64" /></td>
          </tr>
          <tr valign="top">
            <td class="formulario-etiquetas">Rubro de la empresa: </td>
            <td width="340"><input name="PueEmpRubro" type="text" class="formulario-textbox" id="PueEmpRubro" style="width: 300px;" value="<?php print($aRegistro["PueEmpRubro"]); ?>" maxlength="64" maxlenght="64" /></td>
          </tr>
          <tr valign="top">
            <td class="formulario-etiquetas">Remuneraci&oacute;n mensual :</td>
            <td width="340">
            
            <input name="PueRemuneracion" type="text" class="formulario-textbox" id="PueRemuneracion" style="width: 120px;" value="<?php print($aRegistro["PueRemuneracion"]); ?>" maxlenght="512" />
            
            </td>
          </tr>
          <tr valign="top">
            <td class="formulario-etiquetas">Tareas realizadas :</td>
            <td><textarea name="PueDescPuesto" class="formulario-textbox" id="PueDescPuesto" style="width: 300px;" maxlenght="512"><?php print($aRegistro["PueDescPuesto"]); ?></textarea></td>
          </tr>
		  
          <tr>
            <td class="formulario-etiquetas">Fecha Ingreso:</td>
            <td class="formulario-textbox">
			<input name="HIDDEN_alta_m" type="text" class="formulario-textbox" id="HIDDEN_alta_m" style="width: 40px;" value="<?php print($aRegistro["PueFecha"][1]); ?>" />&nbsp;mes &nbsp;&nbsp;&nbsp;&nbsp;
			<input name="HIDDEN_alta_a" type="text" class="formulario-textbox" id="HIDDEN_alta_a" style="width: 60px;" value="<?php print($aRegistro["PueFecha"][0]); ?>" /> a&ntilde;o 
			
			<input name="PueFecha" type="hidden" value="" />
			</td>
          </tr>
		  
          <tr>
            <td class="formulario-etiquetas">Fecha Baja:</td>
            <td class="formulario-textbox">
			<input name="HIDDEN_baja_m" type="text" class="formulario-textbox" id="HIDDEN_baja_m" style="width: 40px;" value="<?php print($aRegistro["PueFechaBaja"][1]); ?>" />&nbsp;mes &nbsp;&nbsp;&nbsp;&nbsp;
			<input name="HIDDEN_baja_a" type="text" class="formulario-textbox" id="HIDDEN_baja_a" style="width: 60px;" value="<?php print($aRegistro["PueFechaBaja"][0]); ?>" /> a&ntilde;o 

			<input name="PueFechaBaja" type="hidden" value="" />
			</td>
          </tr>
		  
          <tr>
            <td class="formulario-etiquetas">Antig&uuml;edad:</td>
            <td class="formulario-textbox">
			<input name="PueExpAnos" type="text" class="formulario-textbox" id="PueExpAnos" style="width: 40px;" value="<?php print ($aRegistro["PueExpAnos"]) ; ?>" />
			&nbsp;a&ntilde;o/s &nbsp;
			<input name="PueExpMeses" type="text" class="formulario-textbox" id="PueExpMeses" style="width: 60px;" value="<?php print ($aRegistro["PueExpMeses"]); ?>" /> 
			mes/es 

			</td>
          </tr>
		  
		  
          <tr valign="top">
            <td class="formulario-etiquetas">Causa de baja:</td>
            <td><input name="PueCausaBaja" type="text" class="formulario-textbox" id="PueCausaBaja" style="width: 300px;" value="<?php print($aRegistro["PueCausaBaja"]); ?>" maxlenght="512" /></td>
          </tr>
		  
          <tr>
            <td height="30" class="formulario-etiquetas"><img src="images/espacio.gif" width="1" height="1"></td>
            <td align="center"><input name="BTN_Guardar" type="submit" id="BTN_Guardar" value="Guardar">
              <input name="BTN_Cancelar" type="reset" id="BTN_Cancelar" value="Cancelar" onclick="history.back();"></td>
          </tr>
        </table>
        </form></td>
        <td width="6" class="formulario-contenido-final"><img src="images/espacio.gif" width="1" height="1"></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="20"><img src="images/formulario-pie-inicio.jpg" width="20" height="40"></td>
        <td class="formulario-pie-bg"><img src="images/espacio.gif" width="1" height="1"></td>
        <td width="20"><img src="images/formulario-pie-final.jpg" width="20" height="40"></td>
      </tr>
    </table></td>
  </tr>
</table>
