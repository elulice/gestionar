<?php
session_start();

// Si NO es - Admin | Operador | Supervisor
if ($_SESSION["tipo_usuario"] != 1 && $_SESSION["tipo_usuario"] != 6 && $_SESSION["tipo_usuario"] != 7) {
    header("Location: escritorio.php");
}

$seccion = "noticias";
$titulo = "Noticias";
if ($_SESSION["tipo_usuario"] == 6 || $_SESSION["tipo_usuario"] == 1) {
    $comando = array(
        "titulo" => "Nueva Noticia",
        "onclick" => "nueva_noticia()"
    );
}
include("clases/framework-1.0/class.bd.php");
include("includes/funciones.php");
require_once ('clases/phppaging/PHPPaging.lib.php');

include("inc.encabezado.php");
?>
<div id="dialog-detalle-noticia" style="display:none;"></div>
<div id="div_noticias_listado" style="height:350px;">
    <div id="div_noticias_listado" style="height:350px;">
        <img src="images/loaging.gif" class="loading">
    </div>
</div>
<?php
include("inc.pie.php");
?>
<?php /*
  <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
  <html>
  <head>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
  <title><?php print SITIO; ?></title>
  <script language="JavaScript" type="text/javascript" src="scripts/ajax.php"></script>
  <script language="JavaScript" type="text/javascript" src="scripts/general.js"></script>

  <link href="estilos/general.css" rel="stylesheet" type="text/css">
  <link href="estilos/vistas.css" rel="stylesheet" type="text/css">
  </head>

  <body>
  <?php
  include("inc.encabezado.php");
  include("inc.listado-clientes.php");
  include("inc.pie.php");
  /*
  ?>
  </body>
  </html>
 */ ?>
