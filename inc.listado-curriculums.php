<link href="estilos/general.css" rel="stylesheet" type="text/css" />
<table width="780" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="30" class="encabezado-titulo-bg"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="20"><img src="images/espacio.gif" width="1" height="1" /></td>
        <td valign="top" class="encabezado-titulo-texto" style="padding-top:5px;">Listado de Postulantes</td>
        <td align="right" valign="top" class="encabezado-titulo-texto" style="padding-top:5px; padding-right:10px;"><a href="am-curriculums.php?idregistro=0&amp;url=<?php print($m_sURL); ?>"><img src="images/btn-agregar.jpg" width="80" height="20" border="0" /></a></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><img src="images/espacio.gif" width="1" height="10"></td>
  </tr>
</table>


<?php 
	if ($_GET["avanzado"] == 1)
		include ("inc.buscador-cv-avanzado.php");
	else
		include ("inc.buscador-cv-basico.php");
?>







<?php 
  if ($_GET["filtrar"])
  {
?>

<table width="880" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="20"><img src="images/listado-encabezado-inicio.jpg" width="20" height="37"></td>
        <td class="listado-encabezado-bg"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="100"><img src="images/espacio.gif" width="1" height="1"></td>
            <td width="120" class="listado-encabezado-texto">Apellido</td>
            <td width="160" class="listado-encabezado-texto">Nombres</td>
            <td width="120" class="listado-encabezado-texto">Tel&eacute;fono</td>
            <td width="120" class="listado-encabezado-texto">Localidad</td>
            <td width="90" class="listado-encabezado-texto">Actualizaci&oacute;n</td>
            <td width="70"><img src="images/espacio.gif" width="1" height="1"></td>
          </tr>
        </table></td>
        <td width="20"><img src="images/listado-encabezado-final.jpg" width="20" height="37"></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="5" class="listado-contenido-inicio"><img src="images/espacio.gif" width="1" height="1"></td>
        <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
		  <?php
		  		$lRegistros = 0;

				$cBD = new BD();
				$cBD->Conectar();
				$paging = new PHPPaging($cBD->RetornarConexion());
				$paging->agregarConsulta($sSQL);
				$paging->ejecutar();

				while($aRegistro = $paging->fetchResultado())
				{
					$sPosicion = (($sPosicion == "1") ? "2" : "1");
		  ?>
          <tr>
            <td class="listado-fila-bg-<?php print($sPosicion); ?>">
			
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="15"><img src="images/espacio.gif" width="1" height="1"></td>
                <td width="100">
				
				<table width="90" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="30"><a href="javascript:verPostulante(<?php print($aRegistro["PerNro"]); ?>)"><img src="images/btn-ver-mas-<?php print($sPosicion); ?>.jpg" alt="Vista Rapida" width="24" height="23" border="0"></a></td>
                    <td width="30"><a href="ver-postulante.php?idregistro=<?php print($aRegistro["PerNro"]); ?>" target="_blank"><img src="images/btn-informe-<?php print($sPosicion); ?>.jpg" alt="Vista Completa" width="24" height="23" border="0"></a></td>
                    <td width="30"><a href="am-curriculums-notas.php?idregistro=<?php print($aRegistro["PerNro"]); ?>&idpostulante=<?php print($aRegistro["PerNro"]); ?>&url=<?php print($m_sURL); ?>"><img src="images/btn-ver-mas.png" alt="Ver Notas sobre el Postulante" width="23" height="23" border="0"></a></td>
                    <td><a href="listado-curriculums-asignar.php?idpostulante=<?php print ($aRegistro["PerNro"]); ?>"><img src="images/btn-adjuntar.png" alt="Asignar a una Oferta" width="23" height="23" border="0"></a></td>
                  </tr>
                </table>
				
				</td>
                <td width="120" class="listado-texto"><?php print($aRegistro["PerApellido"]); ?></td>
                <td width="160" class="listado-texto"><?php print($aRegistro["PerNombres"]); ?></td>
                <td width="120" class="listado-texto"><?php print($aRegistro["PerTelefono"]); ?></td>
                <td width="140" class="listado-texto"><?php print($aRegistro["PerLocalidad"]); ?></td>
                <td width="90" class="listado-texto"><?php print date("d/m/y", strtotime($aRegistro["PerFechModific"])); ?></td>

                <td width="70">
				<table width="70" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="30"><a href="am-curriculums.php?idregistro=<?php print($aRegistro["PerNro"]); ?>&idpostulante=<?php print($aRegistro["PerNro"]); ?>&url=<?php print($m_sURL); ?>"><img src="images/btn-modificar-<?php print($sPosicion); ?>.jpg" alt="Editar" width="24" height="23" border="0"></a></td>
                    <td><a href="abm.php?tabla=persona&columna=PerNro&idregistro=<?php print($aRegistro["idpostulante"]); ?>&url=<?php print($m_sURL); ?>" onclick="return confirm('&iquest;Desea eliminar este Postulante?')"><img src="images/btn-eliminar-<?php print($sPosicion); ?>.jpg" alt="Eliminar" width="24" height="23" border="0"></a></td>
                  </tr>
                </table>
				
				</td>
              </tr>
			  
              <tr id="trPostulante-<?php print($aRegistro["PerNro"]); ?>" class="listado-fila-oculta">
                <td colspan="8" id="tdPostulante-<?php print($aRegistro["PerNro"]); ?>" class="informe-separador">&nbsp;</td>
              </tr>
            </table></td>
          </tr>
          <?php
			 		$lRegistros++;
				}
				if($lRegistros == 0)
				{
			 ?>
          <tr>
            <td><img src="images/espacio.gif" width="1" height="20"></td>
          </tr>
			 <?php } ?>
        </table></td>
        <td width="6" class="listado-contenido-final"><img src="images/espacio.gif" width="1" height="1"></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="100"><img src="images/listado-pie-inicio-deshabilitado.jpg" alt="Agregar" width="100" height="40" border="0"></td>
        <td class="listado-pie-bg link" align="center"><?php echo $paging->fetchNavegacion();?>&nbsp;</td>
        <td width="20"><img src="images/listado-pie-final.jpg" width="20" height="40"></td>
      </tr>
    </table></td>
  </tr>
</table>
<?php 			}
?>
