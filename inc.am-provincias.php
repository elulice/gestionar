<?php
	if($m_lIDRegistro > 0)
	{
		$sSQL = "SELECT idprovincia, idpais, nombre ";
		$sSQL .= "FROM provincia WHERE idprovincia = " . $m_lIDRegistro;
		$cBD = new BD();
		$aRegistro = $cBD->Seleccionar($sSQL, true);
	}

?>
<table width="780" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="30" class="encabezado-titulo-bg"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="20"><img src="images/espacio.gif" width="1" height="1"></td>
        <td class="encabezado-titulo-texto">Alta y modificaci&oacute;n de Provincias</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><img src="images/espacio.gif" width="1" height="20"></td>
  </tr>
</table>
<table width="500" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="20"><img src="images/formulario-encabezado-inicio.jpg" width="20" height="37"></td>
        <td class="formulario-encabezado-bg">Provincia</td>
        <td width="20"><img src="images/formulario-encabezado-final.jpg" width="20" height="37"></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="4" class="formulario-contenido-inicio"><img src="images/espacio.gif" width="1" height="1"></td>
        <td><form action="abm.php?tabla=provincia&columna=idprovincia&idregistro=<?php print($m_lIDRegistro); ?>&archivo=0&url=<?php print($m_sURL); ?>" method="post" name="frmRegistro" id="frmRegistro">
          <table width="100%" border="0" cellspacing="0" cellpadding="4">
          <tr>
            <td class="formulario-etiquetas">Pais:</td>
            <td><select name="idpais" id="idpais" style="width: 300px;">
              <?php
					$sSQL = "SELECT idpais, nombre FROM paises  ";
					$sSQL .= "ORDER BY nombre ASC ";
					print(GenerarOptions($sSQL, $aRegistro["idpais"]));
			  ?>
            </select></td>
          </tr>          
	<tr>
            <td class="formulario-etiquetas">Nombre:</td>
            <td width="340"><input name="nombre" type="text" class="formulario-textbox" id="nombre" style="width: 300px;" maxlenght="64" value="<?php print($aRegistro["nombre"]); ?>" /></td>
          </tr>
          <tr>
            <td height="30" class="formulario-etiquetas"><img src="images/espacio.gif" width="1" height="1"></td>
            <td align="center"><input name="BTN_Guardar" type="submit" id="BTN_Guardar" value="Guardar">
              <input name="BTN_Cancelar" type="reset" id="BTN_Cancelar" value="Cancelar" onclick="history.back();"></td>
          </tr>
        </table>
        </form></td>
        <td width="6" class="formulario-contenido-final"><img src="images/espacio.gif" width="1" height="1"></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="20"><img src="images/formulario-pie-inicio.jpg" width="20" height="40"></td>
        <td class="formulario-pie-bg"><img src="images/espacio.gif" width="1" height="1"></td>
        <td width="20"><img src="images/formulario-pie-final.jpg" width="20" height="40"></td>
      </tr>
    </table></td>
  </tr>
</table>
