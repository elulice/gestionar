<?php 
	$sSQL = "SELECT PerApellido, PerNombres ";
	$sSQL .= "FROM persona ";
	$sSQL .= "WHERE PerNro = " . $m_lIDPostulante;
	$cBD = new BD();
	$aPostulante = $cBD->Seleccionar($sSQL, true);

?>
<table width="780" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="30" class="encabezado-titulo-bg"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="20"><img src="images/espacio.gif" width="1" height="1" /></td>

        <td valign="top" class="encabezado-titulo-texto" style="padding-top:5px;">Trayectoria del Postulante  - 
		<?php print $aPostulante["PerApellido"]." ".$aPostulante["PerNombres"]; ?></td>
        </tr>
    </table></td>
  </tr>
  <tr>
    <td><img src="images/espacio.gif" width="1" height="20"></td>
  </tr>
</table>
<table width="870" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="20"><img src="images/listado-encabezado-inicio.jpg" width="20" height="37"></td>
        <td class="listado-encabezado-bg"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr class="listado-encabezado-texto">
            <td colspan="4" align="center">OFPEDIDO</td>
            <td colspan="4" align="center">POSTULANTE</td>
            </tr>
          <tr class="listado-encabezado-texto">
            <td width="60">Ref</td>
            <td width="150">Cliente</td>
            <td width="120">T&iacute;tulo</td>
            <td width="100">Situaci&oacute;n</td>
            <td width="60">F. post.</td>
            <td width="70">Situaci&oacute;n</td>
            <td width="100">Cambio situaci&oacute;n </td>
            <td>Detalle</td>
          </tr>
        </table></td>
        <td width="20"><img src="images/listado-encabezado-final.jpg" width="20" height="37"></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="5" class="listado-contenido-inicio"><img src="images/espacio.gif" width="1" height="1"></td>
        <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
		  <?php
		  		$lRegistros = 0;

				$sSQL = "SELECT o.OfeNro, o.OfeReferencia, o.OfeTitulo, eo.EOfeValor, ";
				$sSQL .="post.PosAsociado, post.PosFecha, PostulacionEstado.PEsDescrip, ";
				$sSQL .="post.PosEstFecha, post.PosDetalle, ";
				//$sSQL .="MiembroEmpresa.EmpNro, ";
				$sSQL .="c.CliRsocial  ";
				$sSQL .="FROM postulacion post ";
				$sSQL .="LEFT OUTER JOIN PostulacionEstado ON post.PEsNro = PostulacionEstado.PEsNro ";
				$sSQL .="INNER JOIN oferta  o ON post.OfeNro = o.OfeNro ";
				$sSQL .="INNER JOIN estadooferta eo ON o.OfeNro = eo.OfeNro ";
				//$sSQL .="INNER JOIN MiembroEmpresa ON Oferta.MEmpNro = MiembroEmpresa.MEmpNro ";
				$sSQL .="LEFT JOIN ofertascliente oc ON oc.OfeNro = o.OfeNro ";
				$sSQL .="LEFT JOIN cliente c ON c.CliNro = oc.CliNro ";
				$sSQL .="WHERE post.PerNro = " . $m_lIDPostulante. " ";
				//$sSQL .="And MiembroEmpresa.EmpNro = " & Session("EmpNro") & " ";
				$sSQL .="ORDER BY post.PosEstFecha DESC ";
									
				$cBD = new BD();
				$oResultado = $cBD->Seleccionar($sSQL);
				while($aRegistro = $cBD->RetornarFila($oResultado))
				{
					$sPosicion = (($sPosicion == "1") ? "2" : "1");
					
		  ?>
          <tr>
            <td class="listado-fila-bg-<?php print($sPosicion); ?>">
			
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr class="listado-texto-chico">
                <td width="15"><img src="images/espacio.gif" width="1" height="1"></td>
             				
				<td width="60"><?php print($aRegistro["OfeReferencia"]); ?></td>
				<td width="150"><?php print($aRegistro["CliRsocial"]); ?></td>
				<td width="120"><?php print($aRegistro["OfeTitulo"]); ?></td>
				<td width="100"><?php print($aRegistro["EOfeValor"]); ?>&nbsp;</td>
				<td width="60"><?php print(date("d/m/Y", strtotime($aRegistro["PosFecha"]))); ?></td>
				<td width="70"><a href="am-curriculums-estado.php?idoferta=<?php print $aRegistro["OfeNro"]; ?>&idpostulante=<?php print $m_lIDPostulante; ?>"><?php print($aRegistro["PEsDescrip"]); ?></a>&nbsp;</td>
				<td width="100"><?php print(date("d/m/Y", strtotime($aRegistro["PosEstFecha"]))); ?></td>
				<td><?php print($aRegistro["PosDetalle"]); ?>&nbsp;</td>
              </tr>

            </table></td>
          </tr>
          <?php
			 		$lRegistros++;
				}
				if($lRegistros == 0)
				{
			 ?>
          <tr>
            <td><img src="images/espacio.gif" width="1" height="20"></td>
          </tr>
			 <?php } ?>
        </table></td>
        <td width="6" class="listado-contenido-final"><img src="images/espacio.gif" width="1" height="1"></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="100"><img src="images/listado-pie-inicio-deshabilitado.jpg" alt="Agregar" width="100" height="40" border="0"></td>
        <td class="listado-pie-bg">&nbsp;</td>
        <td width="20"><img src="images/listado-pie-final.jpg" width="20" height="40"></td>
      </tr>
    </table></td>
  </tr>
</table>
