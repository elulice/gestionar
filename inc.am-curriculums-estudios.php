<?php

	$sSQL = "SELECT PerApellido, PerNombres ";
	$sSQL .= "FROM persona ";
	$sSQL .= "WHERE PerNro = " . $m_lIDPostulante;
	$cBD = new BD();
	$aPostulante = $cBD->Seleccionar($sSQL, true);

	if($m_lIDRegistro > 0)
	{
		$sSQL = "SELECT * ";
		$sSQL .= "FROM estudio e ";
		$sSQL .= "WHERE e.EtuNro = " . $m_lIDRegistro;
		$cBD = new BD();
		$aRegistro = $cBD->Seleccionar($sSQL, true);
	}
	
?>
<link href="estilos/general.css" rel="stylesheet" type="text/css" />


<table width="780" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="30" class="encabezado-titulo-bg"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="20"><img src="images/espacio.gif" width="1" height="1"></td>
        <td class="encabezado-titulo-texto">Alta y modificaci&oacute;n de Estudios</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><img src="images/espacio.gif" width="1" height="20"></td>
  </tr>
</table>
<table width="580" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="20"><img src="images/formulario-encabezado-inicio.jpg" width="20" height="37"></td>
        <td class="formulario-encabezado-bg">Estudios - <?php print $aPostulante["PerApellido"]." ".$aPostulante["PerNombres"]; ?> </td>
        <td width="20"><img src="images/formulario-encabezado-final.jpg" width="20" height="37"></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="4" class="formulario-contenido-inicio"><img src="images/espacio.gif" width="1" height="1"></td>
        <td><form action="abm.php?tabla=estudio&columna=EtuNro&idregistro=<?php print($m_lIDRegistro); ?>&url=<?php print($m_sURL); ?>" method="post" name="frmRegistro" id="frmRegistro" onsubmit="corrigeFechas(['EtuIngreso', 'EtuEgreso'])">
 <input name="PerNro" type="hidden" value="<?php print $m_lIDPostulante; ?>" />

          <table width="100%" border="0" cellspacing="0" cellpadding="4">
            <tr>
              <td class="formulario-etiquetas">Instituci&oacute;n:</td>
              <td><input name="EtuInstitucion" type="text" class="formulario-textbox" id="EtuInstitucion" style="width: 300px;" value="<?php print($aRegistro["EtuInstitucion"]); ?>" maxlength="128" maxlenght="64" /></td>
            </tr>
          <tr>
            <td class="formulario-etiquetas">Fecha de ingreso:</td>
            <td width="340" class="formulario-textbox"><input name="EtuIngreso" type="text" class="formulario-textbox" id="EtuIngreso" style="width: 120px;" value="<?php print(date("d-m-Y", strtotime($aRegistro["EtuIngreso"]))); ?>" maxlenght="10" /> 
              dd-mm-aaaa </td>
          </tr>
          <tr>
            <td class="formulario-etiquetas">Fecha de egreso:</td>
            <td class="formulario-textbox"><input name="EtuEgreso" type="text" class="formulario-textbox" id="EtuEgreso" style="width: 120px;" value="<?php print(date("d-m-Y", strtotime($aRegistro["EtuEgreso"]))); ?>" maxlenght="10" /> 
            dd-mm-aaaa </td>
          </tr>
          <tr>
            <td class="formulario-etiquetas">Nivel Estudio:</td>
            <td width="340"><select name="NEsNro" id="NEsNro" style="width: 300px;" onchange="cargarListas(this, 'EtuAreaEstudio')">
              <?php
					$sSQL = "SELECT NEsNro, NEsDescrip FROM nivelestudio  ";
					print(GenerarOptions($sSQL, $aRegistro["NEsNro"]));
			  ?>
                        </select></td>
          </tr>
          <tr>
            <td class="formulario-etiquetas">Finalizado:</td>
            <td class="formulario-textbox"><select name="EtuFinalizado" id="EtuFinalizado" style="width: 300px;"  >
              <?php
					$sSQL = "SELECT idestado, nombre FROM _estado_estudio ";
					print(GenerarOptions($sSQL, $aRegistro["EtuFinalizado"]));
			  ?>
            </select></td>
          </tr>
          <tr>
            <td class="formulario-etiquetas">&Aacute;rea de Estudio: </td>
            <td><span class="formulario-textbox">
              <select name="EtuAreaEstudio" id="EtuAreaEstudio" style="width: 300px;">
              </select>
            </span></td>
          </tr>
          <tr valign="top">
            <td class="formulario-etiquetas">T&iacute;tulo: </td>
            <td width="340"><input name="EtuTitulo" type="text" class="formulario-textbox" id="EtuTitulo" style="width: 300px;" value="<?php print($aRegistro["EtuTitulo"]); ?>" maxlenght="512" /></td>
          </tr>
          <tr valign="top">
            <td class="formulario-etiquetas">Materias: </td>
            <td width="340"><table width="300" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="100" class="formulario-textbox">Cursadas: <span class="formulario-textbox">
                  <input name="EtuCursadas" type="text" class="formulario-textbox" id="EtuCursadas" style="width: 30px;" value="<?php print($aRegistro["EtuCursadas"]); ?>" maxlength="2" />
                </span></td>
                <td width="100" align="center" class="formulario-textbox">Aprobadas: <span class="formulario-textbox">
                <input name="EtuAprobadas" type="text" class="formulario-textbox" id="EtuAprobadas" style="width: 30px;" value="<?php print($aRegistro["EtuAprobadas"]); ?>" maxlength="2" />
                </span></td>
                <td width="100" align="right"><span class="formulario-textbox">Promedio:
                    <input name="EtuPromedio" type="text" class="formulario-textbox" id="EtuPromedio" style="width: 30px;" value="<?php print($aRegistro["EtuPromedio"]); ?>" maxlength="2" />
                </span></td>
              </tr>
            </table></td>
          </tr>
          <tr valign="top">
            <td class="formulario-etiquetas">A&ntilde;o que cursa:</td>
            <td><span class="formulario-textbox">
              <input name="EtuAnoCursado" type="text" class="formulario-textbox" id="EtuAnoCursado" style="width: 60px;" value="<?php print($aRegistro["EtuAnoCursado"]); ?>" maxlength="2" />
            </span></td>
          </tr>
          <tr valign="top">
            <td class="formulario-etiquetas">Observaciones:</td>
            <td><textarea name="EtuObservaciones" class="formulario-textbox" id="EtuObservaciones" style="width: 300px;" maxlenght="512"><?php print($aRegistro["EtuObservaciones"]); ?></textarea></td>
          </tr>
		  
          <tr>
            <td height="30" class="formulario-etiquetas"><img src="images/espacio.gif" width="1" height="1"></td>
            <td align="center"><input name="BTN_Guardar" type="submit" id="BTN_Guardar" value="Guardar">
              <input name="BTN_Cancelar" type="reset" id="BTN_Cancelar" value="Cancelar" onclick="history.back();"></td>
          </tr>
        </table>
        </form></td>
        <td width="6" class="formulario-contenido-final"><img src="images/espacio.gif" width="1" height="1"></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="20"><img src="images/formulario-pie-inicio.jpg" width="20" height="40"></td>
        <td class="formulario-pie-bg"><img src="images/espacio.gif" width="1" height="1"></td>
        <td width="20"><img src="images/formulario-pie-final.jpg" width="20" height="40"></td>
      </tr>
    </table></td>
  </tr>
</table>
<script language="JavaScript" type="text/javascript">
	cargarListas(document.getElementById('NEsNro'), 'EtuAreaEstudio')
</script>
