<div id="dialog_opciones_sucursales">
    <div style="width:500px;height:330px;">
        <div style="margin:10px;">
            <form action="abm.php?tabla=unidadorg" method="post" id="form_opciones_nueva_sucursal">
                <p>
                    <span style="float:left;margin-right:10px;">Nombre:</span>
                    <input type="text" name="UniNombre" class="smallInput" style="width: 150px;float:left;margin-right:10px;" />
                    <span style="float:left;margin-right:10px;">Email:</span>
                    <input type="text" name="UniMail" class="smallInput" style="width: 150px;float:left;margin-right:10px;" />
                    <span style="float:left;margin-right:10px;">Reponsable:</span>
                    <select class="smallInput" name="UniResponsable" style="float: left;width:138px;margin-right:10px;">
                        <?php
                        $query = "SELECT MEmpNro, CONCAT(MEmpApellido, ' ' ,MEmpNombres) FROM miembroempresa WHERE active=1 ORDER BY MEmpApellido ";
                        echo GenerarOptions($query, NULL, TRUE, DEFSELECT);
                        ?>
                    </select>
                    <span style="float:left;margin-right:10px;">Prefijo:</span>
                    <input type="text" name="UniReducido1" class="smallInput" style="float:left;width:140px;margin-right:10px;" />
                    <input type="submit" value="Guardar" style="float: right;margin-right: 55px;"/>
                </p>
            </form>
        </div>
        <div id="dialog_opciones_sucursales_lista" class="navPage"></div>
    </div>
</div>

<div id="dialog_opciones_sucursales_edit">
    <div style="width:400px;height:60px;">
        <div style="margin:5px;">
            <p>
                <input type="hidden" id="sucHelper"/>
                <span style="float:left;margin-right:10px;">Email:</span>
                <input type="text" id="UniMail" class="smallInput" style="width: 120px;float:left;margin-right:10px;" value=""/>
                <span style="float:left;margin-right:10px;">Reponsable:</span>
                <select class="smallInput" id="UniResponsable" style="float: left;width:108px;margin-right:10px;">
                    <?php
                    $query = "SELECT PerNro, CONCAT(MEmpApellido, ' ' ,MEmpNombres) FROM miembroempresa ORDER BY MEmpApellido ";
                    echo GenerarOptions($query, NULL, TRUE, DEFSELECT);
                    ?>
                </select>
                <input type="button" value="Guardar" onclick="editar_elemento('sucursales_save_edit',$('#sucHelper').val(),$('#UniMail').val(),$('#UniResponsable').val())" style="float: right;margin-right: 52px;"/>
            </p>
        </div>
    </div>
</div>
