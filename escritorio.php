<?php
$seccion = "escritorio";

$comando = array(
    "titulo" => "Nuevo Cliente",
    "onclick" => "nuevo_cliente();"
);

session_start();
include("clases/framework-1.0/class.bd.php");
include("includes/funciones.php");
require_once ('clases/phppaging/PHPPaging.lib.php');

include("inc.encabezado.php");
include("inc.home.principal.php");
include("inc.pie.php");
?>

<div id="dialog-form-novedades" title="Nueva Novedad" style="display:none;">
    <div style="width:340px">
        <form action="abm.php?tabla=novedades&amp;columna=idnovedad&amp;idregistro=0&amp;archivo=0&amp;contador=1&amp;thumbs=10&amp;url=home.php&amp;ajax=true" method="post" onsubmit="return false;" enctype="multipart/form-data" name="formNuevaNovedad" id="formNuevaNovedad">
            <input type="hidden" name="idusuario" value="<?php echo $_SESSION["IDUsuario"]; ?>">
            <div class="form-cuadros"><label for="titulo" class="form-label">T&iacute;tulo:</label>
                <input type="text" name="titulo" id="titulo" class="form-contacto-text smallInput" style="width: 330px; float: left;" /></div>
            <div class="form-cuadros"><div style="float:left; width:150px;"><label for="fecha" class="form-label">Fecha:</label>
                    <input type="text" name="fecha" id="fecha" value="" class="form-contacto-text smallInput datepicker" style="width: 100px;" /></div>
                <div style="float:left; width:160px; margin-left:30px"><label for="volanta" class="form-label">Volanta:</label>
                    <input type="text" name="volanta" id="volanta" value="" class="form-contacto-text smallInput" style="width: 100px;" /></div></div>
            <div class="form-cuadros"><label for="copete" class="form-label">Copete:</label>
                <textarea name="copete" id="copete" class="form-contacto-text smallInput" style="width: 330px; height: 110px;" ></textarea></div>
            <div class="form-cuadros" style="clear:both;"><label for="copete" class="form-label">Contenido:</label>
                <textarea name="contenido" id="contenido" class="form-contacto-text smallInput" style="width: 330px; height: 110px;" ></textarea></div>
        </form>
    </div>
</div>





<div id="notify" style="display: none;">
    <form action="enviar_notificacion.php" method="post" enctype="multipart/form-data" onSubmit="return false;" name="formNotificar" id="formNotificar" >
        <div class="form-cuadros" style="clear:both; width:340px;"><label for="invite" class="form-label">Notificar Usuarios:</label>
            <input type="text" name="invite" id="autocompletarUsuario" class="form-contacto-text smallInput" style="width: 223px; margin-bottom:10px;" />
            <div id="invites_spans"></div>
            <div class="form-cuadros"><label for="fullbody" class="form-label">Mensaje:</label>
                <textarea name="mensaje" id="fullbody" class="form-contacto-text smallInput" style="width: 330px; height: 110px;" ></textarea>
            </div>
            <input type="hidden" id="notify_tipo" name="tipo" value="" />
            <input type="hidden" id="notify_id" name="id" value="" />
        </div>
    </form>
    <script type="text/javascript">
        $(document).ready(function(){
            var cache = {};
            var cacheSize = 0;


            funciones_actualizar = "home";



            $(".removeNotify", document.getElementById("invites_spans")).live("click", function(){
                $('#formNotificar_'+$(this).attr('rel')).remove();
                //remove current friend
                $(this).parent().remove();

                //correct 'to' field position
                if($("#invites_spans span").length === 0) {
                    $("#autocompletarUsuario").css("top", 0);
                }
            });
            $("#autocompletarUsuario").autocomplete({
                minLength: 2,
                source: function(request, response) {
                    if (cache[request.term]) {
                        response(cache[request.term]);
                        return;
                    }
                    $.ajax({
                        url: "feed.contactos.autocomplete.php",
                        type: "GET",
                        data: {
                            q: request.term,
                            tipo: 'usuarios',
                            noempty: 1
                        },
                        dataType: "json",
                        success: function(data, textStatus) {
                            if (cacheSize >= 16) {
                                cache = {};
                                cacheSize = 0;
                            }
                            cache[request.term] = data;
                            ++cacheSize;
                            response(data);
                        },
                        error: function(xhr, textStatus, ex) {
                            alert('Oops, an error occurred. ' + xhr.statusText + ' - ' +
                                xhr.responseText);
                        }
                    });
                },
                select: function(e, ui) {
                    if (!($('#formNotificar_'+ui.item.id).length > 0)) {
                        //create formatted friend
                        var friend = ui.item.value,
                        span = $("<span>").text(friend),
                        a = $("<a>").addClass("removeNotify").attr({
                            href: "javascript:",
                            title: "Remove " + friend,
                            rel:    ui.item.id
                        }).text("x").appendTo(span);
                        //alert(span);
                        //add friend to friend div
                        //span.insertBefore("#invite");
                        span.appendTo("#invites_spans");
                        $("#formNotificar").append('<input type="hidden" name="invitados[]" id="formNotificar_'+ui.item.id+'" value="'+ui.item.id+'" />');
                        //$("#autocompletarUsuario").val("");
                        this.value = '';
                        return false;
                    }
                },
                //define select handler
                change: function(event, ui) {
                    //prevent 'to' field being updated and correct position
                    $("#autocompletarUsuario").val("").css("top", 2);
                }
            });
        });
    </script>
</div>

<div id="enviarPorMail" style="display:none">
    <form action="enviar_email.php" method="post" enctype="multipart/form-data" onSubmit="return false;" name="formEnviarMail" id="formEnviarMail" >
        <div class="form-cuadros" style="clear:both; width:340px;">
            <label for="invite" class="form-label">Asunto:</label>
            <input type="text" name="asunto" id="emailAsunto" class="form-contacto-text smallInput" style="width: 223px; margin-bottom:10px;" />
            <div id="usuarios_mails"></div>
            <label for="invite" class="form-label" style="margin-right: 18px;">Para:</label>
            <input type="text" name="emails" id="autocompletarEmails" class="form-contacto-text smallInput" style="width: 223px; margin-bottom:10px;" />
            <div class="form-cuadros"><label for="fullbody" class="form-label">Mensaje:</label>
                <textarea name="mensaje" id="fullbody" class="form-contacto-text smallInput" style="width: 330px; height: 110px;" ></textarea>
            </div>
            <input type="hidden" id="email_tipo" name="tipo" value="" />
            <input type="hidden" id="email_id" name="id" value="" />
        </div>
    </form>
    <script type="text/javascript">
        $(document).ready(function(){
            var cache = {};
            var cacheSize = 0;

            $(".removeEmail", document.getElementById("usuarios_mails")).live("click", function(){
                $('#formEnviarMail_'+$(this).attr('rel')).remove();
                //remove current friend
                $(this).parent().remove();

                //correct 'to' field position
                if($("#usuarios_mails span").length === 0) {
                    $("#autocompletarUsuario").css("top", 0);
                }
            });
            $("#autocompletarEmails").autocomplete({
                minLength: 3,
                source: function(request, response) {
                    if (cache[request.term]) {
                        response(cache[request.term]);
                        return;
                    }
                    $.ajax({
                        url: "feed.contactos.autocomplete.php",
                        type: "GET",
                        data: {
                            q: request.term,
                            tipo: 'emails',
                            noempty: 1
                        },
                        dataType: "json",
                        success: function(data, textStatus) {
                            if (cacheSize >= 16) {
                                cache = {};
                                cacheSize = 0;
                            }
                            cache[request.term] = data;
                            ++cacheSize;
                            response(data);
                        },
                        error: function(xhr, textStatus, ex) {
                            alert('Oops, an error occurred. ' + xhr.statusText + ' - ' +
                                xhr.responseText);
                        }
                    });
                },
                select: function(e, ui) {
                    if (!($('#formEnviarMail'+ui.item.id).length > 0)) {
                        //create formatted friend
                        var friend = ui.item.value + "<" + ui.item.id + ">",
                        span = $("<span>").text(friend),
                        a = $("<a>").addClass("removeEmail").attr({
                            href: "javascript:",
                            title: "Remove " + friend,
                            rel:    ui.item.id
                        }).text("x").appendTo(span);
                        span.addClass("ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only");
                        //alert(span);
                        //add friend to friend div
                        //span.insertBefore("#invite");
                        span.appendTo("#usuarios_mails");
                        $("#formEnviarMail").append('<input type="hidden" name="mails[]" id="formEnviarMail_'+ui.item.id+'" value="'+ui.item.id+'" />');
                        //$("#autocompletarEmails").val("");
                        this.value = '';
                        return false;
                    }
                },
                //define select handler
                change: function(event, ui) {
                    //prevent 'to' field being updated and correct position
                    $("#autocompletarEmails").val("").css("top", 2);
                }
            });
        });
    </script>
</div>




