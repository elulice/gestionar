<?php
require_once("clases/framework-1.0/class.bd.php");
require_once("clases/phppaging/PHPPaging.lib.php");
require_once("includes/funciones.php");

switch ($_GET["objeto"]) {

    case "paises" : opciones_paises();
        break;
    case "areas" : opciones_areas();
        break;
    case "estudios" : opciones_estudios();
        break;
    case "contrataciones" : opciones_contrataciones();
        break;
    case "provincias" : opciones_provincias();
        break;
    case "sucursales" : opciones_sucursales();
        break;
    case "responsable" : opciones_responsable();
        break;
    case "estado" : opciones_estado();
        break;
    case "origen" : opciones_origen();
        break;
    case "motivos" : opciones_motivo();
        break;
    default : break;
}

function opciones_paises() {

    $query = "SELECT * FROM pais ORDER BY PaiNom";

    $db = new BD();
    $db->Conectar();

    $paging = new PHPPaging($db->RetornarConexion());
    $paging->agregarConsulta($query);
    $paging->linkClase("navPage");
    $paging->porPagina(5);
    $paging->ejecutar();
    ?>
    <div style="height:230px;">
        <table width="480" cellpadding="0" cellspacing="0" style="margin:12px 0 0 12px" id="box-table-a">
            <thead>
                <tr>
                    <th width="340" scope="col"><span style="color:#c60;font-weight:bold;">País</span></th>
                    <th width="60" scope="col"><span style="color:#c60;font-weight:bold;">Opciones</span></th>
                <tr>
            </thead>
            <tbody>
                <?php
                while ($row = $paging->fetchResultado()) {
                    ?>
                    <tr>
                        <td style="padding:8px;"><?php echo ucwords(strtolower($row["PaiNom"])); ?></td>
                        <td style="padding:8px;">
                            <img src="images/icons/page_delete.png" title="Eliminar" alt="[x]" style="cursor:pointer;" onclick="borrar_elemento('paises', <?php echo $row["PaiNro"]; ?>);" />
                        </td>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
    </div>
    <div class="pagination"><?php echo $paging->fetchNavegacion(); ?></div>
    <?php
}

function opciones_areas() {

    $query = "SELECT * FROM areasuniversitarias ORDER BY AreuNombre";

    $db = new BD();
    $db->Conectar();

    $paging = new PHPPaging($db->RetornarConexion());
    $paging->agregarConsulta($query);
    $paging->linkClase("navPage");
    $paging->porPagina(5);
    $paging->paginasAntes(2);
    $paging->paginasDespues(2);
    $paging->ejecutar();
    ?>
    <div style="height:230px;">
        <table width="480" cellpadding="0" cellspacing="0" style="margin:12px 0 0 12px" id="box-table-a">
            <thead>
                <tr>
                    <th width="340" scope="col"><span style="color:#c60;font-weight:bold;">Area</span></th>
                    <th width="60" scope="col"><span style="color:#c60;font-weight:bold;">Opciones</span></th>
                <tr>
            </thead>
            <tbody>
                <?php
                while ($row = $paging->fetchResultado()) {
                    ?>
                    <tr>
                        <td style="padding:8px;"><?php echo ucwords(strtolower($row["AreuNombre"])); ?></td>
                        <td style="padding:8px;">
                            <img src="images/icons/page_delete.png" title="Eliminar" alt="[x]" style="cursor:pointer;" onclick="borrar_elemento('areas', <?php echo $row["AreuNro"]; ?>);" />
                        </td>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
    </div>
    <div class="pagination"><?php echo $paging->fetchNavegacion(); ?></div>
    <?php
}

function opciones_estudios() {

    $query = "SELECT * FROM area_estudio ORDER BY nombre";

    $db = new BD();
    $db->Conectar();

    $paging = new PHPPaging($db->RetornarConexion());
    $paging->agregarConsulta($query);
    $paging->linkClase("navPage");
    $paging->porPagina(5);
    $paging->ejecutar();
    ?>
    <div style="height:230px;">
        <table width="480" cellpadding="0" cellspacing="0" style="margin:12px 0 0 12px" id="box-table-a">
            <thead>
                <tr>
                    <th width="340" scope="col"><span style="color:#c60;font-weight:bold;">Area</span></th>
                    <th width="60" scope="col"><span style="color:#c60;font-weight:bold;">Opciones</span></th>
                <tr>
            </thead>
            <tbody>
                <?php
                while ($row = $paging->fetchResultado()) {
                    ?>
                    <tr>
                        <td style="padding:8px;"><?php echo ucwords(strtolower($row["nombre"])); ?></td>
                        <td style="padding:8px;">
                            <img src="images/icons/page_delete.png" title="Eliminar" alt="[x]" style="cursor:pointer;" onclick="borrar_elemento('estudios', <?php echo $row["id_area_estudio"]; ?>);" />
                        </td>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
    </div>
    <div class="pagination"><?php echo $paging->fetchNavegacion(); ?></div>
    <?php
}

function opciones_contrataciones() {

    $query = "SELECT * FROM tipo_contratacion ORDER BY nombre";

    $db = new BD();
    $db->Conectar();

    $paging = new PHPPaging($db->RetornarConexion());
    $paging->agregarConsulta($query);
    $paging->linkClase("navPage");
    $paging->porPagina(5);
    $paging->paginasAntes(2);
    $paging->paginasDespues(2);
    $paging->ejecutar();
    ?>
    <div style="height:230px;">
        <table width="480" cellpadding="0" cellspacing="0" style="margin:12px 0 0 12px" id="box-table-a">
            <thead>
                <tr>
                    <th width="340" scope="col"><span style="color:#c60;font-weight:bold;">Tipo Contratación</span></th>
                    <th width="60" scope="col"><span style="color:#c60;font-weight:bold;">Opciones</span></th>
                <tr>
            </thead>
            <tbody>
                <?php
                while ($row = $paging->fetchResultado()) {
                    ?>
                    <tr>
                        <td style="padding:8px;"><?php echo ucwords(strtolower($row["nombre"])); ?></td>
                        <td style="padding:8px;">
                            <img src="images/icons/page_delete.png" title="Eliminar" alt="[x]" style="cursor:pointer;" onclick="borrar_elemento('contrataciones', <?php echo $row["id_tipo_contratacion"]; ?>);" />
                        </td>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
    </div>
    <div class="pagination"><?php echo $paging->fetchNavegacion(); ?></div>
    <?php
}

function opciones_provincias() {

    $query = "SELECT PrvNro, PrvNom, PaiNom FROM provincia LEFT JOIN pais USING(PaiNro) ORDER BY PrvNom";

    $db = new BD();
    $db->Conectar();

    $paging = new PHPPaging($db->RetornarConexion());
    $paging->agregarConsulta($query);
    $paging->linkClase("navPage");
    $paging->porPagina(5);
    $paging->paginasAntes(2);
    $paging->paginasDespues(2);
    $paging->ejecutar();
    ?>
    <div style="height:230px;">
        <table width="480" cellpadding="0" cellspacing="0" style="margin:12px 0 0 12px" id="box-table-a">
            <thead>
                <tr>
                    <th width="340" scope="col"><span style="color:#c60;font-weight:bold;">Provincia</span></th>
                    <th width="340" scope="col"><span style="color:#c60;font-weight:bold;">País</span></th>
                    <th width="60" scope="col"><span style="color:#c60;font-weight:bold;">Opciones</span></th>
                <tr>
            </thead>
            <tbody>
                <?php
                while ($row = $paging->fetchResultado()) {
                    ?>
                    <tr>
                        <td style="padding:8px;"><?php echo ucwords(strtolower($row["PrvNom"])); ?></td>
                        <td style="padding:8px;"><?php echo ucwords(strtolower($row["PaiNom"])); ?></td>
                        <td style="padding:8px;">
                            <img src="images/icons/page_delete.png" title="Eliminar" alt="[x]" style="cursor:pointer;" onclick="borrar_elemento('provincias', <?php echo $row["PrvNro"]; ?>);" />
                        </td>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
    </div>
    <div class="pagination"><?php echo $paging->fetchNavegacion(); ?></div>
    <?php
}

function opciones_sucursales() {
    header('Content-Type: text/html; charset=utf-8');
    $query = "SELECT unidadorg.*, miembroempresa.MEmpNombres as meNombre, miembroempresa.MEmpApellido as meApellido, miembroempresa.MEmpEmail FROM unidadorg
        LEFT JOIN miembroempresa ON unidadorg.UniResponsable = miembroempresa.MEmpNro
        ORDER BY UniNombre";

    $db = new BD(TRUE);
    $db->Conectar();

    $paging = new PHPPaging($db->RetornarConexion());
    $paging->agregarConsulta($query);
    $paging->linkClase("navPage");
    $paging->porPagina(5);
    $paging->paginasAntes(2);
    $paging->paginasDespues(2);
    $paging->ejecutar();
    ?>
    <div style="height:230px;">
        <table width="480" cellpadding="0" cellspacing="0" style="margin:12px 0 0 12px" id="box-table-a">
            <thead>
                <tr>

                    <th width="180" scope="col"><span style="color:#c60;font-weight:bold;">Tipo Contratación</span></th>
                    <th width="100" scope="col"><span style="color:#c60;font-weight:bold;">Email</span></th>
                    <th width="160" scope="col" style="width: 160px;max-width: 160px;overflow: hidden"><span style="color:#c60;font-weight:bold;">Responsable</span></th>
                    <th width="60" scope="col"><span style="color:#c60;font-weight:bold;">Prefijo</span></th>
                    <th width="60" scope="col"><span style="color:#c60;font-weight:bold;">Opciones</span></th>
                <tr>
            </thead>
            <tbody>
                <?php
                while ($row = $paging->fetchResultado()) {
                    $na = $row["meNombre"] . " " . $row['meApellido'];
                    ?>
                    <tr>
                        <td style="padding:8px;"><?php echo $row["UniNombre"]; ?></td>
                        <td style="padding:8px;"><?php echo $row["UniMail"]; ?></td>
                        <td style="padding:8px;"><?php echo myTruncate($na, 8, ' ', ' ...'); ?></td>
                        <td style="padding:8px;"><?php echo $row["UniReducido1"]; ?></td>
                        <td style="padding:8px;">
                            <img src="images/icons/page_edit.png" title="Reasignar" alt="[o]" style="cursor:pointer;" onclick="editar_elemento('sucursales', <?php echo $row["UniNro"]; ?>);" />
                            <img src="images/icons/page_delete.png" title="Eliminar" alt="[x]" style="cursor:pointer;" onclick="borrar_elemento('sucursales', <?php echo $row["UniNro"]; ?>);" />
                        </td>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
    </div>
    <div class="pagination" style="margin-top: 120px;"><?php echo $paging->fetchNavegacion(); ?></div>
    <?php
}

function opciones_responsable() {

    $query = "SELECT * FROM responsable ORDER BY responsable_nombre";

    $db = new BD();
    $db->Conectar();

    $paging = new PHPPaging($db->RetornarConexion());
    $paging->agregarConsulta($query);
    $paging->linkClase("navPage");
    $paging->porPagina(5);
    $paging->paginasAntes(2);
    $paging->paginasDespues(2);
    $paging->ejecutar();
    ?>
    <div style="height:230px;">
        <table width="480" cellpadding="0" cellspacing="0" style="margin:12px 0 0 12px" id="box-table-a">
            <thead>
                <tr>
                    <th width="180" scope="col"><span style="color:#c60;font-weight:bold;">Responsable</span></th>
                    <th width="100" scope="col"><span style="color:#c60;font-weight:bold;">Email</span></th>
                    <th width="60" scope="col"><span style="color:#c60;font-weight:bold;">Opciones</span></th>
                <tr>
            </thead>
            <tbody>
                <?php
                while ($row = $paging->fetchResultado()) {
                    ?>
                    <tr>
                        <td style="padding:8px;"><?php echo htmlentities(ucwords(strtolower($row["responsable_nombre"]))); ?></td>
                        <td style="padding:8px;"><?php echo html_entity_decode($row["responsable_email"]); ?></td>
                        <td style="padding:8px;">
                            <img src="images/icons/page_delete.png" title="Eliminar" alt="[x]" style="cursor:pointer;" onclick="borrar_elemento('responsable', <?php echo $row["id"]; ?>);" />
                        </td>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
    </div>
    <div class="pagination"><?php echo $paging->fetchNavegacion(); ?></div>
    <?php
}

function opciones_estado() {

    $query = "SELECT * FROM estado_tickets ORDER BY nombre";

    $db = new BD();
    $db->Conectar();

    $paging = new PHPPaging($db->RetornarConexion());
    $paging->agregarConsulta($query);
    $paging->linkClase("navPage");
    $paging->porPagina(5);
    $paging->paginasAntes(2);
    $paging->paginasDespues(2);
    $paging->ejecutar();
    ?>
    <div style="height:230px;">
        <table width="480" cellpadding="0" cellspacing="0" style="margin:12px 0 0 12px" id="box-table-a">
            <thead>
                <tr>
                    <th width="180" scope="col"><span style="color:#c60;font-weight:bold;">Estado</span></th>
                    <th width="60" scope="col"><span style="color:#c60;font-weight:bold;">Opciones</span></th>
                <tr>
            </thead>
            <tbody>
                <?php
                while ($row = $paging->fetchResultado()) {
                    ?>
                    <tr>
                        <td style="padding:8px;"><?php echo htmlentities(ucwords(strtolower($row["nombre"]))); ?></td>
                        <td style="padding:8px;">
                            <img src="images/icons/page_delete.png" title="Eliminar" alt="[x]" style="cursor:pointer;" onclick="borrar_elemento('estado', <?php echo $row["id"]; ?>);" />
                        </td>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
    </div>
    <div class="pagination"><?php echo $paging->fetchNavegacion(); ?></div>
    <?php
}

function opciones_motivo() {

    $query = "SELECT motivos.nombre_motivo, tipo_error.nombre_error AS tipo_error FROM motivos ";
    $query .= " LEFT JOIN tipo_error ON motivos.tipo_error_id = tipo_error.iderror ";
    $query .= " ORDER BY nombre_motivo ";

    $db = new BD();
    $db->Conectar();

    $paging = new PHPPaging($db->RetornarConexion());
    $paging->agregarConsulta($query);
    $paging->linkClase("navPage");
    $paging->porPagina(5);
    $paging->paginasAntes(2);
    $paging->paginasDespues(2);
    $paging->ejecutar();
    ?>
    <div style="height:230px;">
        <table width="480" cellpadding="0" cellspacing="0" style="margin:12px 0 0 12px" id="box-table-a">
            <thead>
                <tr>
                    <th width="180" scope="col"><span style="color:#c60;font-weight:bold;">Motivo</span></th>
                    <th width="180" scope="col"><span style="color:#c60;font-weight:bold;">Tipo de Error</span></th>
                    <th width="60" scope="col"><span style="color:#c60;font-weight:bold;">Opciones</span></th>
                <tr>
            </thead>
            <tbody>
                <?php
                while ($row = $paging->fetchResultado()) {
                    ?>
                    <tr>
                        <td style="padding:8px;"><?php echo htmlentities(ucwords(strtolower($row["nombre_motivo"]))); ?></td>
                        <td style="padding:8px;"><?php echo htmlentities(ucwords(strtolower($row["tipo_error"]))); ?></td>
                        <td style="padding:8px;">
                            <img src="images/icons/page_delete.png" title="Eliminar" alt="[x]" style="cursor:pointer;" onclick="borrar_elemento('motivos', <?php echo $row["id"]; ?>);" />
                        </td>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
    </div>
    <div class="pagination"><?php echo $paging->fetchNavegacion(); ?></div>
    <?php
}

function opciones_origen() {

    $query = "SELECT * FROM origen_tickets ORDER BY nombre_origen";

    $db = new BD();
    $db->Conectar();

    $paging = new PHPPaging($db->RetornarConexion());
    $paging->agregarConsulta($query);
    $paging->linkClase("navPage");
    $paging->porPagina(5);
    $paging->paginasAntes(2);
    $paging->paginasDespues(2);
    $paging->ejecutar();
    ?>
    <div style="height:230px;">
        <table width="480" cellpadding="0" cellspacing="0" style="margin:12px 0 0 12px" id="box-table-a">
            <thead>
                <tr>
                    <th width="180" scope="col"><span style="color:#c60;font-weight:bold;">Origen</span></th>
                    <th width="60" scope="col"><span style="color:#c60;font-weight:bold;">Opciones</span></th>
                <tr>
            </thead>
            <tbody>
                <?php
                while ($row = $paging->fetchResultado()) {
                    ?>
                    <tr>
                        <td style="padding:8px;"><?php echo (ucwords(strtolower($row["nombre_origen"]))); ?></td>
                        <td style="padding:8px;">
                            <img src="images/icons/page_delete.png" title="Eliminar" alt="[x]" style="cursor:pointer;" onclick="borrar_elemento('origen', <?php echo $row["id"]; ?>);" />
                        </td>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
    </div>
    <div class="pagination"><?php echo $paging->fetchNavegacion(); ?></div>
    <?php
}
?>
