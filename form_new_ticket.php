<div id="generateNewTicket" style="top: 0;left: 0;margin: 0 0 0 0;background:#fefefe;width: 100%;height: 100%;">
    <?
    $queryB = "SELECT MAX(id) as maxID FROM all_tickets";
    $cBD = new BD();
    $oResultado = $cBD->Seleccionar($queryB);
    while ($aRegistro = $cBD->RetornarFila($oResultado)) {
        $newID = $aRegistro['maxID'];
        if ($newID < 00001) {
            $newID = '00001';
        } else {
            $newID = $newID + 1;
        }
    }
    ?>
    <div style = "display:inline; float:left; margin-left:20px;margin-bottom: 10px">
        <div style = "width:255px;display:block; float:left;border-right: 1px solid #bbb;">
            <div class = "form-administrar" style = "margin-top:9px;"><span class = "form-label"><img src="images/icons/pixel.png" class="icons_new_tickets int_origen"/>Nro Ticket:</span>
                <input type = "text" name = "nroticket" id = "nro_ticket_new" class = "form-contacto-text smallInput disabledNew"  disabled="disabled" style = "width:150px; float:right;" value="<? echo zerofill($newID, 5) ?>"/>
            </div>
            <!--fecha-->
            <div class = "form-administrar" style = "margin-top:9px;"><span class = "form-label"><img src="images/icons/pixel.png" class="icons_new_tickets int_fecha"/>Fecha:</span>
                <input type = "text" name = "apellido" id = "p_fecha_new" class = "form-contacto-text smallInput disabledNew" disabled="disabled" value="<?php echo date('d/m/Y'); ?>" style = "width:150px; float:right;"/>
            </div>
            <!-- Solicitante -->
            <div class="form-administrar" style="margin-top:9px;">
                <span class="form-label">
                    <img src="images/icons/services.png" class="icons_new_tickets"/>
                    Solicitante:
                </span>
                <select name="id_compania" id="p_origen_new" size="1" onchange="updateTicketsForm(this.value)" class="form-contacto-text smallInput " style="width:157px; float:right;">
                    <?php
                    $query = "SELECT idsolicitante, nombre FROM tipo_solicitante ORDER BY idsolicitante";
                    echo html_entity_decode(GenerarOptions($query));
                    ?>
                </select>
            </div>
            <div id="dni" class="hidden">
                <!--DNI-->
                <div class = "form-administrar" style = "margin-top:9px;">
                    <span class = "form-label">
                        <img src="images/icons/pixel.png" class="icons_new_tickets int_nombres"/>DNI:
                    </span>
                    <span onclick="showColaboradorInformation();" style="display: block; float: right; margin: 4px 2px; cursor: pointer;"><img src="images/add.png" /></span>
                    <!--<a href="javascript: showColaboradorInformation();" title="Ver informacion del colaborador" style="display: block; float: right; margin: 4px 2px;">
                        
                    </a>-->
                    <input type="text" id="p_dni" class = "form-contacto-text smallInput " style = "width:130px; float:right;"/>
                    <input type="hidden" name = "colaborador_id" id="p_dni_id"/>

                </div>
            </div>
            <div id="move_usuaria_here"></div>
            <div id="name" class="form-administrar" style="margin-top:9px;"><span class="form-label"><img src="images/icons/pixel.png" class="icons_new_tickets int_nombres"/>Nombres:</span>
                <input type="text" name="nombres" id="p_nombres_new" class = "form-contacto-text smallInput " style="width:150px; float:right;"/>
            </div>
            <div id="email" class="form-administrar" style="margin-top:9px;"><span class="form-label"><img src="images/icons/pixel.png" class="icons_new_tickets int_mail_cliente"/>Email Solicit:</span>
                <input type="text" name="email_cliente" id="p_email_cliente_new" class="form-contacto-text smallInput " style="width:150px; float:right;"/>
            </div>
        </div>
        <div style = "width:255px;display:block; float:left;margin-left: 30px;border-right: 1px solid #bbb;">
            <div id="original_place_usuaria">
                <div class = "form-administrar" id="cliente_new" style = "margin-top:9px"><span class = "form-label"><img src="images/icons/pixel.png" class="icons_new_tickets int_clientes"/>Usuaria:</span>
                    <input type = "text" name = "p_cliente_new" id = "p_cliente_new" class = "form-contacto-text smallInput labelsUsuariaNew" style = "width:150px; float:right;"/>
                </div>
            </div>
            <!--cliente-->
            <div class="form-administrar" style="margin-top:7px;">
                <span class="form-label"><img src="images/icons/pixel.png" class="icons_new_tickets int_origen"/>Origen:</span>

                <select name="id_compania" id="p_origen_new" size="1" class="form-contacto-text smallInput " style="width:157px; float:right;">
                    <?php
                    $query = "SELECT id, nombre_origen FROM origen_tickets
                            ORDER BY id ASC";
                    echo html_entity_decode(GenerarOptions($query, NULL, TRUE, DEFSELECT));
                    ?>
                </select>
            </div>
            <!-- sucursal -->
            <div class="form-administrar" style="margin-top:9px; width: 264px;"><span class="form-label"><img src="images/icons/pixel.png" class="icons_new_tickets int_sucursal"/>Sucursal:</span>
                <div id="select_planes_polizas_new">
                    <div style="float: right; width: 24px; height: 24px;">
                        <img src="images/ajax-loader.gif" style="float: right; margin: 4px; display: none;" />
                    </div>
                    <select name="id_plan" id="p_sucursal_new" size="1" onchange="fill_mail_sucursal($(this).val(), 'p_email_sucursal')" class="form-contacto-text smallInput " style="width:157px; float:right;">
                        <?php
                        $query = "SELECT UniNro, UniNombre FROM unidadorg";
                        echo GenerarOptions($query, NULL, TRUE, DEFSELECT);
                        ?>
                    </select>
                </div>
            </div>
            <!-- SECTOR -->
            <div class="form-administrar" style="margin-top:7px;  width: 264px;">
                <span class="form-label">
                    <img src="images/icons/branch.png" class="icons_new_tickets int_responsable"/>Sector:
                </span>
                <div id="select_sector_div">
                    <div style="float: right; width: 24px; height: 24px;">
                        <img src="images/ajax-loader.gif" style="float: right; margin: 4px;display: none;" id="load-sector" />
                    </div>
                    <select id="sector_select" size="1" class="form-contacto-text smallInput " style="width:157px; float:right;">
                        <?php /* $sql = 'SELECT a.AreNro, a.AreNom FROM miembroempresa mi INNER JOIN area a ON(mi.AreNro = a.AreNro)' ?>
                          <?php echo GenerarOptions(htmlentities($sql), null, true, DEFSELECT) */ ?>
                    </select>
                    <!--<input type="text" id="sector_select" readonly="readonly" class="form-contacto-text smallInput disabledNew" style="width:150px; float:right;"/>-->
                </div>
            </div>
            <!-- RESPONSABLE -->
            <div class="form-administrar" style="margin-top:7px;  width: 264px;">
                <span class="form-label"><img src="images/icons/pixel.png" class="icons_new_tickets int_responsable"/>Responsable:</span>
                <div id="select_planes_polizas">
                    <div style="float: right; width: 24px; height: 24px;">
                        <img src="images/ajax-loader.gif" style="float: right; margin: 4px;display: none;" id="load-responsable" />
                    </div>
                    <select name="id_plan" id="p_responsable_new" <?php /* onchange="fill_mail_responsable($(this).val(),'p_email_responsable')" */ ?> size="1" class="form-contacto-text smallInput " style="width:157px; float:right;">
                        <?php
                        /* $query = "SELECT MEmpNro, CONCAT(MEmpNombres,' ',MEmpApellido) FROM miembroempresa WHERE MEmpAdmin = '0' ORDER BY MEmpNombres";
                          echo GenerarOptions(htmlentities($query), NULL, TRUE, DEFSELECT); */
                        ?>
                    </select>
                </div>
            </div>
        </div>
        <div style = "width:220px;display:block; float:left;margin-left: 30px;margin-top: -2px;">
            <!-- ESTADO  -->
            <div id="agente-datos">
                <div class="form-administrar" style="margin-top:9px;">
                    <span class="form-label"><img src="images/icons/pixel.png" class="icons_new_tickets int_estado"/>Estado:</span>
                    <select name="id_plan" id="p_estado_new" size="1" class="form-contacto-text smallInput " style="width:157px; float:right;">
                        <?php
                        $query = "SELECT id, nombre FROM estado_tickets";
                        echo GenerarOptions(html_entity_decode($query), '1', TRUE, DEFSELECT);
                        ?>
                    </select>
                </div>
            </div>
            <!-- MOTIVO -->
            <div id="agente-datos">
                <div class="form-administrar" style="margin-top:9px;">
                    <span class="form-label"><img src="images/icons/pixel.png" class="icons_new_tickets int_reason" />Motivo:</span>
                    <select id="p_motivo" size="1" class="form-contacto-text smallInput " style="width:157px; float:right;">
                        <?php
                        $query = "SELECT id, nombre_motivo FROM motivos";
                        echo GenerarOptions($query, NULL, TRUE, DEFSELECT);
                        ?>
                    </select>
                </div>
            </div>
            <!-- TIPO DE ERROR -->
            <div id="agente-datos">
                <div class="form-administrar" style="margin-top:9px;">
                    <span class="form-label"><img src="images/icons/pixel.png" class="icons_new_tickets int_error" />Tipo Error:</span>
                    <select id="p_tipo_error_org" size="1" class="form-contacto-text smallInput " style="width:157px; float:right;">
                    </select>
                </div>
            </div>
            <!-- EMAIL -->
            <div id="insert_email"></div>
            <div class = "form-administrar" style = "margin-top:9px;" id="rem_col"><span class = "form-label" ><img src="images/icons/pixel.png" class="icons_new_tickets int_mail"/>CC:</span>
                <input type = "text" name = "email" id = "email_new" class = "form-contacto-text smallInput " style = "width:150px; float:right;">
            </div>
            <!-- EMAIL CC -->
            <div class = "form-administrar" style = "margin-top:9px;"><span class = "form-label"><img src="images/icons/pixel.png" class="icons_new_tickets int_mail"/>CC2:</span>
                <input type = "text" name = "email_cc" id = "cc_email_new" class = "form-contacto-text smallInput " style = "width:150px; float:right;">
            </div>
            <input type = "hidden" name = "nombres" id = "p_email_sucursal" class = "form-contacto-text smallInput disabledNew" style = "width:150px; float:right;" />
            <input type = "hidden" name = "nombres" id = "p_email_responsable" class = "form-contacto-text smallInput disabledNew" style = "width:150px; float:right;" />
        </div>
    </div>
    <div style="margin-top: 5px; width:505px;">
        <div class="texto-cuadro-reclamo" >
            <textarea name="detalles" placeholder="Escribir la consulta o reclamo..." class="form-contacto-text smallInput textarea_no_resize" id="p_content_new_ticket" style="width: 820px;height: 150px;margin-top: 6px;"></textarea>
        </div>
        <div class="form-administrar" style="margin-top:5px;width: 815px;">
            <div style="background: #EEEEE0 !important;border: 1px solid #999 !important;height: 50px;padding: 5px;width: 815px;">
                <span>Para adjuntar archivos primero debes guardar el ticket.</span>
                <span style="display: none;">
                    <a href="javascript: void(0);" onclick="$('#form-tickets-adjuntos-list' ).dialog('open');" style="margin-left: 3px;">Archivos adjuntos:</a>
                </span>
                <span style="float:right; display: none;">
                    <a href="javascript:void(0);" onclick="$('#archivo').trigger('click');" class="edit_inline" style="margin-left:10px;">Agregar </a>
                </span>
                <input id="archivo" name="archivo" type="file" style="width:0; height: 0;" />
            </div>
        </div>
    </div>
    <div style="float: right;margin: 5px 3px 0px 0px;">
        <a href="javascript:void(0)" onclick="saveNewTicket('<?php echo zerofill($newID, 5); ?>',$('textarea#p_content_new_ticket').val(),0)" class="buttons ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only">
            <span class="ui-button-text" style="font-size: 11px;">Guardar</span>
        </a>
        <a href="javascript:void(0)" onclick="saveNewTicket('<?php echo zerofill($newID, 5); ?>',$('textarea#p_content_new_ticket').val(),1)" class="buttons ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only">
            <span class="ui-button-text" style="font-size: 11px;">Guardar y cerrar</span>
        </a>
        <a href="javascript:void(0)" onclick="$('body').css('cursor', 'wait');window.location.reload();return;$('#generateNewTicket:ui-dialog').dialog('close');" class="buttons ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only">
            <span class="ui-button-text" style="font-size: 11px;">Cancelar</span>
        </a>
    </div>
</div>