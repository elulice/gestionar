<?php

include("clases/framework-1.0/class.bd.php");
include("includes/funciones.php");

$cBD = new BD(TRUE);
$sql = "SELECT all_tickets.id, all_tickets.fecha, all_tickets.hora, respuesta_tickets.respuesta_fecha, respuesta_tickets.respuesta_hora, 
    responded_to_client.rtc_fecha, responded_to_client.rtc_hora FROM all_tickets 
    LEFT JOIN respuesta_tickets ON all_tickets.id = respuesta_tickets.ticket_id 
    LEFT JOIN responded_to_client ON all_tickets.id = responded_to_client.rtc_id_ticket ORDER BY fecha DESC";

$oReg = $cBD->Seleccionar($sql);
while ($aReg = $cBD->RetornarFila($oReg)) {

    $idTicket = $aReg["id"];
    $creacion = $aReg["fecha"] . " " . $aReg["hora"];
    $respuesta = $aReg["respuesta_fecha"] . " " . $aReg["respuesta_hora"];
    $cierre = $aReg["rtc_fecha"] . " " . $aReg["rtc_hora"];

    if (str_replace(" ", "", $creacion) != "" && str_replace(" ", "", $respuesta) != "") {
        $creacion_respuesta = strtotime($respuesta) - strtotime($creacion);
    }
    if (trim(str_replace(" ", "", $cierre)) != "" && trim(str_replace(" ", "", $creacion)) != "") {
        $creacion_cierre = strtotime($cierre) - strtotime($creacion);
    }
    if (str_replace(" ", "", $cierre) != "" && str_replace(" ", "", $respuesta) != "") {
        $respuesta_cierre = strtotime($cierre) - strtotime($respuesta);
    }

    $sqlInsert = "UPDATE all_tickets SET creacion_respuesta = '$creacion_respuesta', creacion_cierre = '$creacion_cierre', respuesta_cierre = '$respuesta_cierre' WHERE all_tickets.id = $idTicket";
//    echo $sqlInsert . ";<br/>";
    if ($cBD->Ejecutar($sqlInsert)) {
        echo "INSERT Correcto. Ticket ID: " . $idTicket . "<br/>";
    } else {
        echo "ERROR. Ticket ID: " . $idTicket . "<br/>";
    }
}
?>
