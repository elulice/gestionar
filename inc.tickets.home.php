﻿<div class="portlet">
    <div class="portlet-header"><span style="display:block; width:90px; float:left "><img src="images/menu/reclamos.png" width="16" height="16" alt="Comments" />Tickets</span>
<!--        <span style=" float:right;display:block; width:170px; ">
            <a class="edit_inline" href="javascript:void(0);" onclick="generateNewTicket()" style="margin-left:10px;display: block;float: right">    Tickets </a>
        </span>-->
        <div style="clear:both"></div>
    </div>
    <div class="portlet-content">
        <div style="height: inherit;">
            <ul  style="">
                <div class="navPage" id="fill_tickets_home">
                    <img src="images/loading.gif" class="loading" />
                </div>

            </ul>
        </div>
    </div>
</div>
<div id="generateNewTicket" style="top: 0;left: 0;margin: 0 0 0 0;background:#fefefe;width: 100%;height: 100%;">
    <?
    $queryB = "SELECT MAX(id) as maxID FROM all_tickets";
    $cBD = new BD();
    $oResultado = $cBD->Seleccionar($queryB);
    while ($aRegistro = $cBD->RetornarFila($oResultado)) {
        $newID = $aRegistro['maxID'];
        $newID = $newID + 1;
    }
    ?>
    <div style = "display:inline; float:left; margin-left:20px;">
        <div style = "width:240px;display:block; float:left;">
            <div class = "form-administrar" style = "margin-top:10px;"><span class = "form-label"><img src="images/icons/pixel.png" class="icons_new_tickets int_origen"/>Nro Ticket:</span>
                <input type = "text" name = "nombres" id = "nro_ticket_new" class = "form-contacto-text smallInput disabledNew" disabled="disabled" value="<?php echo zerofill($newID, 5) ?>" style = "width:150px; float:right;"/>
            </div>
            <!--fecha-->
            <div class = "form-administrar" style = "margin-top:10px;"><span class = "form-label"><img src="images/icons/pixel.png" class="icons_new_tickets int_fecha"/>Fecha:</span>
                <input type = "text" name = "apellido" id = "p_fecha_new" class = "form-contacto-text smallInput disabledNew"  disabled="disabled" value="<?php echo date('d/m/Y'); ?>" style = "width:150px; float:right;"/>
            </div>
            <!--NOMBRE-->
            <div class = "form-administrar" style = "margin-top:10px;"><span class = "form-label"><img src="images/icons/pixel.png" class="icons_new_tickets int_nombres"/>Nombres:</span>
                <input type = "text" name = "nombres" id = "p_nombres_new" class = "form-contacto-text smallInput " style = "width:150px; float:right;"/>
            </div>
            <div class = "form-administrar" style = "margin-top:10px;"><span class = "form-label"><img src="images/icons/pixel.png" class="icons_new_tickets int_mail_cliente"/>Email Cliente:</span>
                <input type = "text" name = "email_cliente" id = "p_email_cliente_new" class = "form-contacto-text smallInput " style = "width:150px; float:right;"/>
            </div>
            <!--cliente-->
            <div class = "form-administrar" style = "margin-top:10px;"><span class = "form-label"><img src="images/icons/pixel.png" class="icons_new_tickets int_clientes"/>Usuarias:</span>
                <input type = "text" name = "id_cliente" id = "p_cliente_new" class = "form-contacto-text smallInput " style = "width:150px; float:right;"/>
            </div>
<!--            <div class = "form-administrar" style = "margin-top:10px;"><span class = "form-label"><img src="images/icons/pixel.png" class="icons_new_tickets int_clientes"/>Usuarias:</span>
                <select name = "id_compania" id = "p_cliente_new" size = "1" class = "form-contacto-text smallInput" style = "width:157px; float:right;">
            <?php
//                    $query = "SELECT CliNro, CliRsocial FROM cliente ORDER BY CliRsocial";
//                    echo GenerarOptions($query, NULL, TRUE, DEFSELECT);
            ?>
                </select>
            </div>-->
            <!-- origen -->
            <div class="form-administrar" style="margin-top:10px;"><span class="form-label"><img src="images/icons/pixel.png" class="icons_new_tickets int_origen"/>Origen:</span>
                <select name="id_compania" id="p_origen_new" size="1" class="form-contacto-text smallInput " style="width:157px; float:right;">
                    <?php
                    $query = "SELECT id, nombre_origen FROM origen_tickets 
                            ORDER BY id ASC";
                    echo html_entity_decode(GenerarOptions($query, NULL, TRUE, DEFSELECT));
                    ?>
                </select>
            </div>
            <!-- sucursal -->
            <div class="form-administrar" style="margin-top:10px;"><span class="form-label"><img src="images/icons/pixel.png" class="icons_new_tickets int_sucursal"/>Sucursal:</span>
                <div id="select_planes_polizas">
                    <select name="id_plan" id="p_sucursal_new" size="1" onchange="fill_mail_sucursal($(this).val(), 'p_email_sucursal')" class="form-contacto-text smallInput " style="width:157px; float:right;">
<!--                        <select name="id_plan" id="p_sucursal_new" size="1" onchange="fill_responsable_select($(this).val(), 'p_responsable_new'), fill_mail_sucursal($(this).val(), 'p_email_sucursal')" class="form-contacto-text smallInput " style="width:157px; float:right;">-->
                        <?php
                        $query = "SELECT UniNro, UniNombre FROM unidadorg";
                        echo GenerarOptions($query, NULL, TRUE, DEFSELECT);
                        ?>    
                    </select>
                </div>
            </div>   
            <!-- RESPONSABLE -->
            <div class="form-administrar" style="margin-top:10px;">
                <span class="form-label"><img src="images/icons/pixel.png" class="icons_new_tickets int_responsable"/>Responsable:</span>
                <div id="select_planes_polizas">
                    <select name="id_plan" id="p_responsable_new" size="1" onchange="fill_mail_responsable($(this).val(),'p_email_responsable')" class="form-contacto-text smallInput " style="width:157px; float:right;">
                        <?php
                        $query = "SELECT MEmpNro, CONCAT(MEmpNombres,' ',MEmpApellido) FROM miembroempresa WHERE MEmpAdmin = '0' ORDER BY MEmpNombres";
                        echo GenerarOptions(htmlentities($query), NULL, TRUE, DEFSELECT);
                        ?>
                    </select>
                </div>
            </div>
            <!-- ESTADO  -->
            <div id="agente-datos">
                <div class="form-administrar" style="margin-top:10px;">
                    <span class="form-label"><img src="images/icons/pixel.png" class="icons_new_tickets int_estado"/>Estado:</span>
                    <select name="id_plan" id="p_estado_new" size="1" class="form-contacto-text smallInput " style="width:157px; float:right;">
                        <?php
                        $query = "SELECT id, nombre FROM estado_tickets";
                        echo GenerarOptions(html_entity_decode($query), '1', TRUE, DEFSELECT);
                        ?>    
                    </select>
                </div>
            </div>
            <div class = "form-administrar" style = "margin-top:10px;"><span class = "form-label"><img src="images/icons/pixel.png" class="icons_new_tickets int_mail"/>Email:</span>
                <input type = "text" name = "nombres" id = "email_new" class = "form-contacto-text smallInput " style = "width:150px; float:right;">
            </div>
            <div class = "form-administrar" style = "margin-top:10px;"><span class = "form-label"><img src="images/icons/pixel.png" class="icons_new_tickets int_mail"/>CC:</span>
                <input type = "text" name = "nombres" id = "cc_email_new" class = "form-contacto-text smallInput " style = "width:150px; float:right;">
            </div>
            <input type = "hidden" name = "nombres" id = "p_email_sucursal" class = "form-contacto-text smallInput disabledNew" style = "width:150px; float:right;" />          
            <input type = "hidden" name = "nombres" id = "p_email_responsable" class = "form-contacto-text smallInput disabledNew" style = "width:150px; float:right;" />
        </div>
    </div>
    <div style="float: right;margin-right: 40px;margin-top: 5px;width:505px;">
        <div class="texto-cuadro-reclamo"> 
            <textarea name="detalles" placeholder="Escribir la consulta o reclamo..." class="form-contacto-text smallInput textarea_no_resize" id="p_content_new_ticket" style="width:425px; height: 350px; margin-top: 6px; float:right; margin-right:6px;"></textarea>
        </div>
        <div class="form-administrar" style="margin-top:5px;width: 421px;margin-left: 66px;">
            <div style="background: #EEEEE0 !important;border: 1px solid #999 !important;height: 50px;padding: 5px;width: 421px;">
                <span>
                    <a href="javascript: void(0);" onclick="$('#form-reclamos-adjuntos-list' ).dialog('open');" style="margin-left: 3px;">Archivos adjuntos:</a>
                </span>
                <span style="float:right;">
                    <a href="javascript:void(0);" onclick="$('#form-reclamos-adjuntos').dialog('open');" class="edit_inline" style="margin-left:10px;">Agregar </a>
                </span>
            </div>
        </div>
    </div>
    <div style="top: 450px;left: 628px;position: absolute">
        <a href="javascript:void(0)" onclick="saveNewTicket('<?php echo $newID; ?>',$('textarea#p_content_new_ticket').val())" class="buttons ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only">
            <span class="ui-button-text" style="font-size: 11px;">Guardar</span>
        </a> 
        <a href="javascript:void(0)" onclick="$( '#generateNewTicket:ui-dialog' ).dialog( 'close' );"class="buttons ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only">
            <span class="ui-button-text" style="font-size: 11px;">Cancelar</span>
        </a>
    </div>
</div>
<div id="response_for_dialogs" style="top: 0;left: 0;margin: 0 0 0 0;background:#fefefe;width: 100%;height: 100%;">
    <div id="content_information"></div>
    <div style="right: 15px;position: absolute;bottom: 10px;"><a href="javascript:void(0)" onclick="$( '#response_for_dialogs:ui-dialog' ).dialog( 'close' ); "class="buttons ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" ><span class="ui-button-text" style="font-size: 11px;">Cerrar</span></a></div>
</div>
<script type="text/javascript" >
    $(document).ready(function(){
        $.datepicker.setDefaults({ dateFormat: 'dd/mm/yy' });
        $( "#generateNewTicket:ui-dialog" ).dialog( "destroy" );
        $( "#generateNewTicket" ).dialog({
            width: 830,
            height: 530,
            closeOnEscape: true,
            autoOpen: false,
            modal: true,
            resizable: false,
            close: function(event, ui) {
                $('body').css('cursor', 'wait');
                window.location.reload();
            },
            title: 'Nuevo Ticket'
        });
        $( "#response_for_dialogs" ).dialog({
            width: 250,
            height: 115,
            closeOnEscape: true,
            autoOpen: false,
            modal: true,
            resizable: false,
            title: 'Informaci&oacute;n'
        });
    });
    
    jQuery(function($){
        $.datepicker.regional['es'] = {
            clearText: 'Borra',
            clearStatus: 'Borra fecha actual',
            closeText: 'Cerrar',
            closeStatus: 'Cerrar sin guardar',
            prevText: '<Ant',
            prevBigText: '<<',
            prevStatus: 'Mostrar mes anterior',
            prevBigStatus: 'Mostrar año anterior',
            nextText: 'Sig>',
            nextBigText: '>>',
            nextStatus: 'Mostrar mes siguiente',
            nextBigStatus: 'Mostrar año siguiente',
            currentText: 'Hoy',
            currentStatus: 'Mostrar mes actual',
            monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio', 'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
            monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
            monthStatus: 'Seleccionar otro mes',
            yearStatus: 'Seleccionar otro año',
            weekHeader: 'Sm',
            weekStatus: 'Semana del año',
            dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
            dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Jue', 'Vie', 'Sáb'],
            dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
            dayStatus: 'Set DD as first week day',
            dateStatus: 'Select D, M d',
            dateFormat: 'dd/mm/yy',
            firstDay: 1,
            initStatus: 'Seleccionar fecha',
            isRTL: false
        };
        $.datepicker.setDefaults($.datepicker.regional['es']);
    });
</script>
