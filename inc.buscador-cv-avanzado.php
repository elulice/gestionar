<?php
	
	$bArea = (is_numeric($_GET["AreNro"]) ? $_GET["AreNro"] : 0);
	$bPuesto = $_GET["PueNro"];
	$bClientes = $_GET["Clientes"];
	$bEdadDesde =  (is_numeric($_GET["EdadDesde"]) ? $_GET["EdadDesde"] : 0);
	$bEdadHasta =  (is_numeric($_GET["EdadHasta"]) ? $_GET["EdadHasta"] : 100);
	$bSexo =  (is_numeric($_GET["PerSexo"]) ? $_GET["PerSexo"] : 0);
	$bProvincia = ($_GET["PerProvincia"] ? $_GET["PerProvincia"] : "");
	$bZona =  (is_numeric($_GET["PerZona"]) ? $_GET["PerZona"] : 0);
	$bIngles =  (is_numeric($_GET["PerIngles"]) ? $_GET["PerIngles"] : 0);
	$bFecha = (is_numeric($_GET["FechaIngreso"]) ? $_GET["FechaIngreso"] : 0);

	$bPalabras = explode(" ", $_GET["Palabras"]);
	$bNotas =  (is_numeric($_GET["NotasAsoc"]) ? $_GET["NotasAsoc"] : -1);
	$bAtributos = $_GET["Atributos"];
	
	
/*print	count($bAtributos);
	$bExperiencia =  (is_numeric($_GET["PerExperiencia"]) ? $_GET["PerExperiencia"] : 0);
	$bNivelEst =  (is_numeric($_GET["PerNivelE"]) ? $_GET["PerNivelE"] : 0);
	$bEstCompl =  (is_numeric($_GET["EstCompl"]) ? $_GET["EstCompl"] : -1);
	$bUniv =  (is_numeric($_GET["EstUniv"]) ? $_GET["EstUniv"] : 0);
	$bSecund =  (is_numeric($_GET["EstSec"]) ? $_GET["EstSec"] : 0);
*/

?>
<link href="estilos/general.css" rel="stylesheet" type="text/css" />

<form action="listado-curriculums.php" method="get" name="frmFiltro" id="frmFiltro">
<input name="filtrar" type="hidden" value="1" />
<input name="avanzado" type="hidden" value="1" />

<table border="0" align="center" cellpadding="0" cellspacing="0" style="margin-bottom:10px;" class="buscar">
<tr align="left">
	<td valign="top" class="encabezado-formulario">&Aacute;rea:</td>
	<td valign="top"><select name="AreNro" class="formulario-textbox" id="AreNro" style="width: 180px;" onchange="cargarListas(this, 'PueNro[]')">
      <?php
					$sSQL = "SELECT AreNro, AreNom FROM area  ";
					$sSQL .= "ORDER BY AreNom ASC ";
					print(GenerarOptions($sSQL, $bArea, true, DEFSELECT));
			  ?>
    </select></td>
	<td valign="top" class="encabezado-formulario">Puesto:</td>
	<td colspan="2" valign="top"><select name="PueNro[]" class="formulario-textbox" id="PueNro[]" style="width: 180px;" multiple="multiple">
      <?php
					$sSQL = "";
					print(GenerarOptions($sSQL, $bPuesto, true, "[Indistinto]"));
			  ?>
    </select></td>
    </tr>
<tr align="left">
  <td class="encabezado-formulario">Edad (a&ntilde;os) :</td>
  <td class="formulario-textbox">
    <input name="EdadDesde" type="text" class="formulario-textbox" id="EdadDesde" style="width: 80px;" value="<?php print $bEdadDesde; ?>" />
   a    
   <input name="EdadHasta" type="text" class="formulario-textbox" id="EdadHasta" style="width: 80px;" value="<?php print $bEdadHasta; ?>" /></td>
  <td class="encabezado-formulario">Sexo:</td>
  <td colspan="2">
    <select name="PerSexo" class="formulario-textbox" id="PerSexo" style="width: 180px;">
      <?php
					$sSQL = "SELECT idsexo, nombre FROM _sexos  ";
					$sSQL .= "ORDER BY idsexo ASC ";
					print(GenerarOptions($sSQL, $bSexo));
			  ?>
        </select>  </td>
  </tr>
<tr align="left">
  <td class="encabezado-formulario">Provincia:</td>
  <td><select name="PerProvincia" class="formulario-textbox" id="PerProvincia" style="width: 180px;">
    <?php
					$sSQL = "SELECT PrvNom, PrvNom FROM provincia  ";
					$sSQL .= "ORDER BY PrvNom ASC ";
					print(GenerarOptions($sSQL, $bProvincia, true, DEFSELECT));
			  ?>
    </select></td>
  <td class="encabezado-formulario">Zona:</td>
  <td colspan="2">
    <select name="PerZona" class="formulario-textbox" id="PerZona" style="width: 180px;">
      <?php
					$sSQL = "SELECT ZonNro, ZonDescrip FROM zona  ";
					$sSQL .= "ORDER BY ZonDescrip ASC ";
					print(GenerarOptions($sSQL, $bZona, true, DEFSELECT));
			  ?>
        </select>  	</td>
  </tr>
<tr align="left">
  <td class="encabezado-formulario">Inlg&eacute;s:</td>
  <td><select name="PerIngles" class="formulario-textbox" id="PerIngles" style="width: 180px;">
    <?php
					$sSQL = "SELECT idnivel, nombre FROM _nivel_idioma  ";
					print(GenerarOptions($sSQL, $bIngles, true, DEFSELECT));
			  ?>
  </select></td>
  <td class="encabezado-formulario">Experiencia m&iacute;nima (a&ntilde;os): * </td>
  <td colspan="2">
    <select name="PerExperiencia" class="formulario-textbox" id="PerExperiencia" style="width:100px;">
						<option value="0" selected>DEFSELECT</option>
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3">3</option>
						<option value="4">4</option>
						<option value="5">5</option>
						<option value="6">6</option>
						<option value="7">7</option>
						<option value="8">8</option>
						<option value="9">9</option>
    </select>  	</td>
  </tr>
<tr align="left">
  <td class="encabezado-formulario">Nivel de educaci&oacute;n:</td>
  <td><select name="PerNivelE" class="formulario-textbox" id="PerNivelE" style="width: 180px;">
    <?php
					$sSQL = "SELECT NEsNro, NEsDescrip FROM nivelestudio  ";
					print(GenerarOptions($sSQL, $bNivelEst, true, DEFSELECT));
			  ?>
  </select></td>
  <td class="encabezado-formulario">Estudios completos: * </td>
  <td colspan="2">
    <select name="EstCompl" class="formulario-textbox" id="EstCompl" style="width: 80px;">
				    <option value="-1" selected>DEFSELECT</option>
					<option value="1">SI</option>
					<option value="0">NO</option>
					<option value="2">En curso</option>
    </select>  	</td>
  </tr>
<tr align="left">
  <td class="encabezado-formulario">Estudios universitarios:*</td>
  <td><select name="EstUniv" class="formulario-textbox" id="EstUniv" style="width: 180px;">
    <?php
					$sSQL = "SELECT AreuNro, AreNom FROM areasuniversitarias  ";
					$sSQL .= "ORDER BY AreuNombre ASC ";
					print(GenerarOptions($sSQL, $bUniv, true, DEFSELECT));
			  ?>
    </select></td>
  <td class="encabezado-formulario">Estudios secundarios: * </td>
  <td colspan="2">
    <select name="EstSec" class="formulario-textbox" id="EstSec" style="width: 180px;">
      <?php
					$sSQL = "SELECT AresNro, AresNombre FROM areassecundarias  ";
					$sSQL .= "ORDER BY AresNombre ASC ";
					print(GenerarOptions($sSQL, $bArea, true, DEFSELECT));
			  ?>
    </select>  </td>
  </tr>
<tr align="left">
  <td class="encabezado-formulario">Otros estudios: * </td>
  <td><select name="EstUniv" class="formulario-textbox" id="EstUniv" style="width: 180px;">
    <?php
					$sSQL = "SELECT AreuNro, AreNom FROM areasuniversitarias  ";
					$sSQL .= "ORDER BY AreuNombre ASC ";
					print(GenerarOptions($sSQL, $bUniv, true, DEFSELECT));
			  ?>
    </select></td>
  <td class="encabezado-formulario">Postgrado: * </td>
  <td colspan="2">
    <select name="EstSec" class="formulario-textbox" id="EstSec" style="width: 180px;">
      <?php
					$sSQL = "SELECT AresNro, AresNombre FROM areassecundarias  ";
					$sSQL .= "ORDER BY AresNombre ASC ";
					print(GenerarOptions($sSQL, $bArea, true, DEFSELECT));
			  ?>
    </select>  </td>
  </tr>
  
<tr align="left">
  <td class="encabezado-formulario">Notas asociadas:</td>
  <td><select name="NotasAsoc" class="formulario-textbox" id="NotasAsoc" style="width: 80px;">
		<option value="-1" selected>DEFSELECT</option>
		<option value="1">SI</option>
		<option value="0">NO</option>
  </select></td>
  <td class="encabezado-formulario">Fecha de ingreso:</td>
  <td colspan="2">
    <select name="FechaIngreso" class="formulario-textbox" id="FechaIngreso" style="width: 180px;">
						<option value=0>DEFSELECT</option>
						<option value=10>7 dias</option>
						<option value=11>14 dias</option>
						<option value=1>1 Mes</option>
						<option value=2>2 Meses</option>
						<option value=3>3 Meses</option>
						<option value=4>4 Meses</option>								
						<option value=5>5 Meses</option>
						<option value=6>6 Meses</option>									
    </select>  </td>
</tr>
<tr align="left" valign="top">
  <td class="encabezado-formulario">Que puedan postularse a: </td>
  <td><select name="Clientes[]" class="formulario-textbox" id="Clientes[]" style="width: 180px; height:100px;" multiple="multiple">
    <?php
					$sSQL = "SELECT CliNro, CliRSocial FROM cliente ORDER BY CliRSocial ASC ";
					print(GenerarOptions($sSQL, $bClientes, true, "[Indistinto]"));
			  ?>
  </select></td>
  <td><span class="encabezado-formulario">Con las palabras:</span></td>
  <td colspan="2"><input name="Palabras" type="text" class="formulario-textbox" id="Palabras" style="width: 180px;" /></td>
</tr>

<tr align="left" valign="top">
  <td class="encabezado-formulario">B&uacute;squeda por perfil: </td>
  <td colspan="2"><table width="180" border="0" cellpadding="0" cellspacing="4" style="border: 1px solid #FF8737;">
    
<!--    <tr>
      <td class="encabezado-formulario" colspan="2">Grupo</td>
        <td class="encabezado-formulario">Atributo</td>
      </tr>
-->    <?php
	$sSQL = "SELECT g.GruNro, g.GruNombre FROM grupo g ";
	$sSQL .= "INNER JOIN grupoempresa ge ON ge.GruNro = g.GruNro ";
	$sSQL .= "WHERE ge.EmpNro = ". RetornarIdEmpresa();
	
	$cBD = new BD();
	$oResultado = $cBD->Seleccionar($sSQL);
	while($aRegistro = $cBD->RetornarFila($oResultado))
	{
?>
    <tr valign="top">
      <td width="10"><input name="GruNro" type="checkbox" value="<?php print $aRegistro["GruNro"]; ?>" onclick="habilitarAtributos(this)" /></td>
        <td><?php print $aRegistro["GruNombre"]; ?></td>
        <td id="atributos-<?php print $aRegistro["GruNro"]; ?>">&nbsp;</td>
      </tr>
    <?php } ?>
  </table></td>
  <td colspan="2">&nbsp;</td>
</tr>


<tr align="left">
	<td colspan="3" class="encabezado-formulario">&nbsp;</td>
	<td><input name="btnFiltrar" type="image" id="btnFiltrar" src="images/btn-buscar.jpg" alt="Filtrar" value="1"/>    </td>
    <td valign="top"><a href="listado-curriculums.php?avanzado=0"><img src="images/btn-basico.jpg" width="70" height="20" border="0" /></a></td>
</tr>
</table>
</form>

<?php 
// Consulta
	$sSQL = "SELECT p.PerNro, p.PerApellido, p.PerNombres, p.PerTelefono, p.PerLocalidad, p.PerFechModific ";
	$sSQL .= "FROM persona p ";
	$sSQL .= "WHERE 1 ";


//Armado del filtro segun los parametros seleccionados
	if ($bEdadDesde > 0)
		$sFiltro .= "AND p.PerFechaNac  <= '". date("Y-m-d", strtotime("-".$bEdadDesde." year")) ."' ";

	if ($bEdadHasta > 0)
		$sFiltro .= "AND p.PerFechaNac  > '". date("Y-m-d", strtotime("-".++$bEdadHasta." year")) ."' ";

	if ($bSexo > 0)
		$sFiltro .= "AND p.PerSexo  = ". $bSexo ." ";
					
	if (strlen($bProvincia) > 0)
		$sFiltro .= "AND p.PerProvincia LIKE '%". $bProvincia ."%' ";
	if ($bZona > 0)
		$sFiltro .= "AND p.ZonNro  = ". $bZona ." ";
	if ($bIngles > 0)
		$sFiltro .= "AND p.PerIngles  = ". $bIngles ." ";
	
	if ($bFecha)
	{
		switch($bFecha)
		{
			case 10: $dias = "7 days"; break;
			case 11: $dias = " 14 days"; break;
			case 1: $dias = " 1 months"; break;
			case 2: $dias = " 2 months"; break;
			case 3: $dias = " 3 months"; break;
			case 4: $dias = " 4 months"; break;
			case 5: $dias = " 5 months"; break;
			case 6: $dias = " 6 months"; break;
		}
		if($dias)
			$sFiltro .= "AND p.TS >= '".date("Y-m-d", strtotime("- ".$dias))."' ";
	}

	if ($bArea > 0)
	{
		$sFiltro .= "AND EXISTS (SELECT * FROM datospuesto t1 WHERE t1.AreNro = ".$bArea ."  ";
		$sFiltro .= "AND t1.PerNro = p.PerNro ) ";
	}	
	if (count($bPuesto) > 0)
	{
		$sFiltro .= "AND (";
		for ($i = 0; $i < count($bPuesto); $i++)
		{
			$sFiltro .= "EXISTS (SELECT * FROM datospuesto t1 WHERE t1.PueNro = ".$bPuesto[$i]."  ";
			$sFiltro .= "AND t1.PerNro = p.PerNro ) ";
		}
		$sFiltro .= ") ";
	}

	if (count($bClientes) > 0)
	{
		$sFiltro .= "AND (";
		for ($i = 0; $i < count($bClientes); $i++)
		{
			$sFiltro .= "EXISTS(SELECT * FROM personacliente pc WHERE p.PerNro = pc.PerNro AND pc.CliNro = ".$bClientes[$i]."  ) ";
		}
		$sFiltro .= ") ";
	}
		
	switch($bNotas)
	{
		case 1: $sFiltro .= "AND EXISTS(SELECT * FROM postulantenotas pn WHERE pn.PerNro = p.PerNro AND pn.NotaDescrip <> \"\" ) "; break;
		case 0: $sFiltro .= "AND NOT EXISTS(SELECT * FROM postulantenotas pn WHERE pn.PerNro = p.PerNro AND pn.NotaDescrip <> \"\" ) "; break;
	}

	if (count($bPalabras) > 0)
	{
		$sFiltro .= "AND ( ";
		for ($i = 0; $i < ($i < 3 && count($bPalabras)); $i++)
		{
			//if (strlen($bPalabras[$i]) > 2)
			{
				$sFiltro .= "EXISTS (SELECT * FROM datospuesto t1 WHERE t1.PueDescPuesto LIKE '%".$bPalabras[$i]."%' ";
				$sFiltro .= "AND t1.PerNro = p.PerNro ) ";
				$sFiltro .= "OR EXISTS (SELECT * FROM estudio t1 WHERE t1.EtuObservaciones LIKE '%".$bPalabras[$i]."%' ";
				$sFiltro .= "AND t1.PerNro = p.PerNro ) ";
				$sFiltro .= "OR (p.PerPrefLaboral LIKE '%".$bPalabras[$i]."%') ";
				$sFiltro .= "OR (p.PerAspPuesto LIKE '%".$bPalabras[$i]."%') ";
				$sFiltro .= "OR (p.PerIdiomas LIKE '%".$bPalabras[$i]."%') ";
				$sFiltro .= "OR (p.PerOtrosConoc LIKE '%".$bPalabras[$i]."%') ";
			}
		}
		$sFiltro .= ") ";
	}


	if (count($bAtributos) > 0)
	{
		$sFiltro .= "AND (";
		$sValor = $_GET["valor-".$bAtributos[$i]];
		for ($i = 0; $i < (count($bAtributos)); $i++)
		{
			$sValor = $sValor <> "-" ? $sValor : "";
			
			$sFiltro .= "EXISTS (SELECT * FROM descrippersona t1 WHERE t1.DPerValor LIKE '%".$sValor."%' ";
			$sFiltro .= " AND t1.AtrNro = ". $bAtributos[$i] ." AND t1.PerNro = p.PerNro ) ";
		}
		$sFiltro .= ") ";
	}
		
//Orden consulta
				$sSQL .= $sFiltro;
				$sSQL .= "ORDER BY PerApellido ASC, PerNombres ASC ";
?>

<script>
	function habilitarAtributos(pCheckbox)
	{
		var tContenedor = document.getElementById("atributos-" + pCheckbox.value);
		
		if(pCheckbox.checked)
		{
			x_GenerarAtributos(pCheckbox.value, function(pCadena)
			{
				tContenedor.innerHTML = pCadena;
			});
		} else {
				tContenedor.innerHTML = "";
		}
	}
</script>
