<?php
session_start();
include("includes/funciones.php");
include("clases/framework-1.0/class.bd.php");
require_once ("clases/phppaging/PHPPaging.lib.php");
header('content-type: text/html; charset=UTF-8');

if ($_GET['objeto']) {
    switch ($_GET['objeto']) {
        case "reportes":
            ?>
            <table width="450" cellpadding="0" cellspacing="0" style="margin:12px 0px 0px 12px;" id="box-table-a" summary="Employee Pay Sheet">
                <thead>
                    <tr>
                        <th width="220" scope="col"><span style="color:#C60; font-weight:bold;">Nombre</span></th>
                        <th width="100" scope="col"><span style="color:#C60; font-weight:bold;">Fecha de Creaci&oacute;n</span></th>
                        <th width="30" scope="col"><span style="color:#C60; font-weight:bold;">Opciones</span></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $sSQL = "SELECT nombre, fecha, id FROM reportes WHERE id_usuario = '{$_SESSION['PerNro']}' ORDER BY fecha DESC";

                    $cBD = new BD();
                    $cBD->Conectar();

                    //echo $sSQL;

                    $paging = new PHPPaging($cBD->RetornarConexion());
                    $paging->porPagina(6);
                    $paging->linkClase("navPage");
                    $paging->agregarConsulta($sSQL);
                    $paging->ejecutar();

                    while ($aRegistro = $paging->fetchResultado()) {
                        ?>             <tr>
                            <td height="30" style="padding-left:8px;"><?php echo $aRegistro["nombre"]; ?></td>
                            <td height="30" style="padding-left:8px;"><?php echo date("d/m/Y", strtotime($aRegistro["fecha"])); ?></td>
                            <td height="30" style="padding-left:8px;"><a href="javascript: void(0)" onclick="cargarReporte(<?php echo $aRegistro["id"]; ?>)" title="Editar / Ver"><img src="images/icons/page_edit.png" /></a>
                                <a id="a_produccion_reportes" href="abm.php?tabla=reportes&columna=id&idregistro=<?php echo $aRegistro["id"]; ?>&ajax=true&function=actualizarListadoReportes()" class="delete" title="Eliminar"><img src="images/icons/page_delete.png" /></a></td>
                        </tr>


                        <?php
                    }
                    ?>
                </tbody>
            </table>
            <div class="pagination">
                <?php echo $paging->fetchNavegacion(); ?>
            </div>
            <?php
            break;







        case "historiales":
            ?>
            <table width="450" cellpadding="0" cellspacing="0" style="margin:12px 0px 0px 12px;" id="box-table-a" summary="Employee Pay Sheet">
                <thead>
                    <tr>
                        <th width="220" scope="col"><span style="color:#C60; font-weight:bold;">Nombre</span></th>
                        <th width="100" scope="col"><span style="color:#C60; font-weight:bold;">Fecha de Creaci&oacute;n</span></th>
                        <th width="30" scope="col"><span style="color:#C60; font-weight:bold;">Opciones</span></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $archivos = glob("reportes/{$_SESSION['IDUsuario']}/*.xml");
                    //echo "polizas/P_$id-*";
                    $paging = new PHPPaging();
                    $paging->porPagina(6);
                    $paging->linkClase("navPage");
                    $paging->agregarArray($archivos);
                    $paging->ejecutar();

                    while ($aRegistro = $paging->fetchResultado()) {
                        $nombre = substr(strrchr($aRegistro, '/'), 1);
                        $extension = pathinfo($aRegistro);
                        ?><tr>
                            <td height="30" style="padding-left:8px;"><?php echo htmlentities(substr(basename(utf8_decode($nombre), '.' . $extension['extension']), 0, 25)); ?></td>
                            <td height="30" style="padding-left:8px;"><?php echo date("d/m/Y", filemtime($aRegistro)); ?></td>
                            <td height="30" style="padding-left:8px;"><a href="javascript: void(0)" onclick="generarReporteHistorial('<?php echo htmlentities(substr(basename(utf8_decode($nombre), '.' . $extension['extension']), 0, 25)) ?>')" title="Editar / Ver"><img src="images/icons/page_edit.png" /></a>
                                <a id="a_produccion_historiales" href="abm.php?tabla=archivos&columna=id&idregistro=1&ajax=true&function=actualizarListadoHistoriales()&archivo=<?php echo str_replace('.' . $extension['extension'], '', $aRegistro); ?>" class="delete" title="Eliminar"><img src="images/icons/page_delete.png" /></a></td>
                        </tr><?php
            }
                    ?>
                </tbody>
            </table>
            <div class="pagination">
                <?php echo $paging->fetchNavegacion(); ?>
            </div>
            <?php
            break;
        case "reporte":
            $bd = new BD();
            $id = $_GET['id'];
            $reporte = $bd->Seleccionar("SELECT reporte, id, nombre FROM reportes WHERE id = '$id'", true);
            $json = array();
            $json[] = array("name" => 'id_reporte', "value" => $reporte['id']);
            $json[] = array("name" => 'nombre_reporte', "value" => $reporte['nombre']);
            $reporte = unserialize($reporte['reporte']);
            foreach ($reporte as $key => $objeto) {
                $json[] = array("name" => $key, "value" => $objeto);
            }
            die(json_encode($json));
            break;




        case "repo":

            /*
              echo "<pre>";
              print_r($_POST);
              echo "</pre>";
              exit();
             */

            require_once("clases/framework-1.0/data.produccion.php");

            $nombre = $_POST['nombre_reporte'];
            unset($_POST['nombre_reporte']);
            $id = intval($_POST['id_reporte']);
            unset($_POST['id_reporte']);

            $campos = array();
            $filtros = array();
            $totales = array();
            $order = array();
            $cotizar = 1;

            foreach ($_POST as $key => $pos) {
                if ($pos == '0' || trim($pos) == '')
                    continue;
                if (strpos($key, 'f_') === 0) {
                    $filtros[str_replace('f_', '', $key)] = $pos;
                    continue;
                }
                if (strpos($key, 'totales_') === 0) {
                    $totales[$key] = $pos;
                    continue;
                }
                if (strpos($key, 'o_') === 0) {
                    $order[str_replace('o_', '', $key)] = $pos;
                    continue;
                }
                if (strpos($key, 'cot_') === 0) {
                    $cotizar = $pos;
                    continue;
                }
                $campos[] = $key;
            }


            foreach ($filtros as $key => $fil) {

                if (strpos($key, 'fecha') === 0) {

                    if (strpos($key, '_d', strlen($key) - 3) !== FALSE) {

                        $newkey = str_replace('_d', '', $key, intval(strlen($key) - 3));
                        $tmp = $newkey . '_h';

                        if (isset($filtros[$tmp]))
                            $hasta = $filtros[$tmp];
                        else
                            $hasta = date('Y-m-d');
                        if (strpos($fil, '/') !== FALSE) {
                            $new = explode('/', $fil);
                            $fil = $new[2] . '-' . $new[1] . '-' . $new[0];
                        }
                        if (strpos($hasta, '/') !== FALSE) {
                            $new = explode('/', $hasta);
                            $hasta = $new[2] . '-' . $new[1] . '-' . $new[0];
                        }
                        $filtros[$newkey] = array('desde' => $fil, 'hasta' => $hasta);
                        unset($filtros[$key], $filtros[$tmp]);
                        continue;
                    }
                    if (strpos($key, '_h', strlen($key) - 3) !== FALSE) {
                        $newkey = str_replace('_h', '', $key, intval(strlen($key) - 3));
                        $tmp = $newkey . '_d';
                        if (!isset($filtros[$tmp]))
                            unset($filtros[$key]);
                        continue;
                    }
                }
                if (strpos($key, '_v', strlen($key) - 3) !== FALSE) {
                    $newkey = str_replace('_v', '', $key, intval(strlen($key) - 3));
                    $tmp = $newkey . '_c';
                    if (isset($filtros[$tmp]))
                        $comp = $filtros[$tmp];
                    else
                        $comp = 0;
                    $filtros[$newkey] = array('valor' => $fil, 'comp' => $comp);
                    unset($filtros[$key], $filtros[$tmp]);
                    //die($key);
                }
                if (strpos($key, '_c', strlen($key) - 3) !== FALSE) {
                    $newkey = str_replace('_c', '', $key, intval(strlen($key) - 3));
                    $tmp = $newkey . '_v';
                    if (!isset($filtros[$tmp]))
                        unset($filtros[$key]);
                    continue;
                }
            }

            /*
              $campos_externos = array(
              'estado'		 => 'esta.estado',
              'id_agente'	 => 'CONCAT(cont.apellido, ", ", cont.nombre) as agente',
              'gerente'	 => 'est.gerente',
              'director'	 => 'est.director',
              'frecuencia_pago' => 'fre.frecuencia',
              'id_plan'	 => 'plan.codigo_plan',
              'id_producto'	 => 'prod.codigo_producto',
              'nombres'	 => 'CONCAT(pol.apellido, ", ", pol.nombres) AS nombres',
              'pais'		 => 'pai.pais',
              'condicion'	 => 'cond.condicion',
              'forma_pago'	 => 'form.forma',
              'id_moneda'	 => 'mon.moneda',
              'id_compania'	 => 'comp.com_codigo'
              );
             */

            $where = '';
            $comparadores = array('>', '<', '=');

            foreach ($filtros as $key => &$filtro) {
                if (is_array($filtro)) {
                    if (array_key_exists('desde', $filtro)) {
                        $where .= ' AND DATE(pol.' . $key . ') BETWEEN DATE(\'' . $filtro['desde'] . '\') AND DATE(\'' . $filtro['hasta'] . '\')';
                        continue;
                    }
                    if (array_key_exists('valor', $filtro)) {
                        $where .= ' AND pol.' . $key . ' ' . $comparadores[$filtro['comp']] . ' \'' . $filtro['valor'] . '\'';
                        continue;
                    }
                }
                if ($key == 'gerente' || $key == 'director' || $key == 'director_general') {
                    $where .= ' AND est.' . $key . ' = \'' . $filtro . '\'';
                    continue;
                }
                if ($key == 'fumador')
                    $filtro = $filtro - 1;
                $where .= ' AND pol.' . $key . ' = \'' . $filtro . '\'';
            }

//            $campos_select = array('estado' => 'esta.estado', 'id_agente' => 'est.agente', 'gerente' => 'est.gerente', 'director' => 'est.director',
//                'frecuencia_pago' => 'fre.frecuencia', 'id_plan' => 'CONCAT(prod.codigo_producto, ", ", plan.codigo_plan) AS plan',
//                'nombres' => 'CONCAT(pol.nombres, ", ", pol.apellido) AS nombres', 'pais' => 'pai.pais', 'condicion' => 'cond.condicion',
//                'forma_pago' => 'form.forma', 'id_moneda' => 'mon.moneda', 'id_compania' => 'comp.com_nombre');


            $clean_select = "";
            $having = "";
            $campos_select = array();
            $contables = array('suma_asegurada', 'prima_inicial', 'prima_planeada', 'prima_adicional', 'prima_unica', 'prima_target');

            //die(var_dump($campos));
            foreach ($campos as $campo) {

                /* activar los joins correspondientes */
                if (isset($fields[$campo]["join"]))
                    $fields[$campo]["join"]["enabled"] = TRUE;


                //echo $campo . '; ' . $campos_externos[$campo] . "\n";
                if (isset($campos_externos[$campo])) {
                    $campos_select[] = $campos_externos[$campo];
                    if (strlen($having) == 0) {
                        $having .= " HAVING " . $fields[$campo]['campo'] . " != '0' AND " . $fields[$campo]['campo'] . " != '' AND " . $fields[$campo]['campo'] . " IS NOT NULL";
                    }else
                        $having .= " AND " . $fields[$campo]['campo'] . " != '0' AND " . $fields[$campo]['campo'] . " != '' AND " . $fields[$campo]['campo'] . " IS NOT NULL";
                    continue;
                } else {
                    if (array_key_exists('group', $order)) {
                        if (array_search($campo, $contables) !== FALSE) {
                            $campos_select[] = 'SUM(cotizar(pol.' . $campo . ', pol.id_moneda, ' . $cotizar . ')) AS ' . $campo;
                            continue;
                        }
                    }
                    if (array_search($campo, $contables) !== FALSE) {
                        $campos_select[] = 'cotizar(pol.' . $campo . ', pol.id_moneda, ' . $cotizar . ') AS ' . $campo;
                        continue;
                    }
                    $campos_select[] = 'pol.' . $campo;
                    if ($campo != 'fumador')
                        $clean_select .= " AND (pol." . $campo . " != '0' AND pol." . $campo . " != '' AND pol." . $campo . " != '0000-00-00' AND pol." . $campo . " IS NOT NULL)";
                }
            }
            //die(var_dump($campos_select));
            $campos_select = implode(', ', $campos_select);
//die("Filtros: " . var_export($filtros, true) . "\n Totales: " . var_export($totales, true) . "\n Campos: " . $campos_select);
            if ($_SESSION["is_agente"]) {
                $where .= ' AND pol.id_agente = ' . $_SESSION["id_agente"];
            }
            if ($_SESSION["is_gerente"]) {
                $where .= ' AND est.gerente = "' . $_SESSION["gerente"] . '"';
            }
            if ($_SESSION["is_director"]) {
                $joins["estructura_comercial"]["enabled"] = TRUE;
                $where .= ' AND';
                if (isset($_SESSION["is_gag"]) && $_SESSION["is_gag"] == TRUE) {
                    $where.= ' ( ';
                }

                $where .= ' est.director = "' . $_SESSION["director"] . '"';

                if (isset($_SESSION["is_gag"]) && $_SESSION["is_gag"] == TRUE) {
                    $where .= " OR est.gerente='LRK' OR est.director='MEG')";
                }
            }
            if ($_SESSION["is_productor"] && $_SESSION["id_compania"] != 0) {
                $where .= ' AND pol.id_compania = ' . $_SESSION["id_compania"];
            }
            if ($_SESSION["is_productor"] && $_SESSION["id_producto"] != 0) {
                $where .= ' AND pol.id_producto = ' . $_SESSION["id_producto"];
            }
            /* if (isset($_SESSION["is_gag"]) AND $_SESSION["is_gag"] == TRUE) {
              $joins["estructura_comercial"]["enabled"] = TRUE;
              $where.= " OR ( (est.gerente='LRK' AND pol.id_producto NOT IN(25,33) ) OR ( est.gerente='MEG' AND pol.id_producto NOT IN(25,33) ) ) ";
              } */






            $sSQL = 'SELECT pol.id_moneda AS cotizar, ' . $campos_select . '
                FROM Solicitudes_Detalle pol';

            foreach ($joins as &$join) {

                if ($join["enabled"] === TRUE)
                    $sSQL.= " {$join["join"]} ";
            }

            /*
              LEFT JOIN solicitud_estado esta ON esta.id = pol.estado
              LEFT JOIN estructura_comercial est ON est.id_agente = pol.id_agente
              LEFT JOIN contactos cont ON (cont.id_agente = pol.id_agente)
              LEFT JOIN companias comp ON comp.idcompania = pol.id_compania
              LEFT JOIN productos prod ON prod.id_producto = pol.id_producto
              LEFT JOIN planes plan ON plan.id_plan = pol.id_plan
              LEFT JOIN paises pai ON pai.id = pol.pais
              LEFT JOIN monedas mon ON mon.idmoneda = pol.id_moneda
              LEFT JOIN frecuencia_pago fre ON fre.id_frecuencia = pol.frecuencia_pago
              LEFT JOIN forma_pago form ON form.id = pol.forma_pago
              LEFT JOIN solicitud_condicion cond ON cond.id = pol.condicion
             */



            $sSQL.= '
                WHERE id_solicitud !=  0 ' . $where . ((array_key_exists('group', $order)) ? ' GROUP BY ' . ((isset($campos_externos[$order['group']]) && $order['group'] != 'nombres' && $order["group"] != "mes_envio" && $order["group"] != "anio_envio" && $order['group'] != 'id_agente') ? $campos_externos[$order['group']] : $fields[$order['group']]['campo']) : '' ) . ' 
                ORDER BY ' . ((array_key_exists('order', $order)) ? ((isset($campos_externos[$order['order']]) && $order["order"] != "mes_envio" && $order["order"] != "anio_envio" && $order['order'] != 'nombres' && $order['order'] != 'id_agente') ? $campos_externos[$order['order']] : $fields[$order['order']]['campo']) : 'fecha_envio') . ' 
                ' . ((array_key_exists('progresion', $order)) ? $order['progresion'] : 'DESC');

            /* saque  AND cont.id_agente != 0 */
            ?>
            <table width="830" cellpadding="0" cellspacing="0" style="margin:12px 0px 0px 12px;" id="box-table-a" summary="Employee Pay Sheet">
                <thead>
                    <tr>
                        <?php foreach ($campos as $campo): ?>
                            <th scope="col"><span style="color:#C60; font-weight:bold;"><?php echo $fields[$campo]['label']; ?></span></th>
                        <?php endforeach; ?>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    /*
                      echo $sSQL;
                      exit();
                     */

                    setlocale(LC_MONETARY, $locales[$cotizar]);

                    /*
                      echo $sSQL;
                      exit();
                     */


                    $cBD = new BD();
                    $cBD->Conectar();
                    $paging = new PHPPaging($cBD->RetornarConexion());
                    $paging->porPagina(20);
                    $paging->linkClase("navRepo");
                    $paging->agregarConsulta($sSQL);
                    $paging->ejecutar();
                    while ($aRegistro = $paging->fetchResultado()) {
                        //die(var_dump($aRegistro))
                        ?>
                        <tr>
                        <?php foreach ($campos as $campo): ?>
                                <td style="padding-left:8px;"><?php
                    if ($campo == 'fumador')
                        echo (($aRegistro[$fields[$campo]['campo']]) ? "Si" : "No");
                    elseif (strpos($campo, 'fecha') === 0)
                        echo date('d/m/Y', strtotime($aRegistro[$fields[$campo]['campo']]));
                    elseif (array_search($campo, $contables) !== FALSE) {
                        echo money_format('%.2n', $aRegistro[$fields[$campo]['campo']]);
                    } else
                        echo $aRegistro[$fields[$campo]['campo']];
                            ?></td>
                                <?php endforeach; ?>
                        </tr>
                        <?php } ?>
                </tbody>
            </table>
            <div class="pagination" style="margin-right: 20px; margin-top: 5px; margin-bottom: 10px;">
            <?php echo $paging->fetchNavegacion(); ?>
            </div>
                <?php
                break;






            case "repo-tot":

                require_once("clases/framework-1.0/data.produccion.php");

                $nombre = $_POST['nombre_reporte'];
                unset($_POST['nombre_reporte']);
                $id = intval($_POST['id_reporte']);
                unset($_POST['id_reporte']);

                $campos = array();
                $filtros = array();
                $totales = array();
                $order = array();
                $cotizar = 1;

                foreach ($_POST as $key => $pos) {
                    if ($pos == '0' || trim($pos) == '')
                        continue;
                    if (strpos($key, 'f_') === 0) {
                        $filtros[str_replace('f_', '', $key)] = $pos;
                        continue;
                    }
                    if (strpos($key, 'totales_') === 0) {
                        $totales[$key] = $pos;
                        continue;
                    }
                    if (strpos($key, 'o_') === 0) {
                        $order[str_replace('o_', '', $key)] = $pos;
                        continue;
                    }
                    if (strpos($key, 'cot_') === 0) {
                        $cotizar = $pos;
                        continue;
                    }
                    $campos[] = $key;
                }

                foreach ($filtros as $key => $fil) {
                    if (strpos($key, 'fecha') === 0) {
                        if (strpos($key, '_d', strlen($key) - 3) !== FALSE) {
                            $newkey = str_replace('_d', '', $key, intval(strlen($key) - 3));
                            $tmp = $newkey . '_h';
                            if (isset($filtros[$tmp]))
                                $hasta = $filtros[$tmp];
                            else
                                $hasta = date('Y-m-d');
                            if (strpos($fil, '/') !== FALSE) {
                                $new = explode('/', $fil);
                                $fil = $new[2] . '-' . $new[1] . '-' . $new[0];
                            }
                            if (strpos($hasta, '/') !== FALSE) {
                                $new = explode('/', $hasta);
                                $hasta = $new[2] . '-' . $new[1] . '-' . $new[0];
                            }
                            $filtros[$newkey] = array('desde' => $fil, 'hasta' => $hasta);
                            unset($filtros[$key], $filtros[$tmp]);
                            continue;
                        }
                        if (strpos($key, '_h', strlen($key) - 3) !== FALSE) {
                            $newkey = str_replace('_h', '', $key, intval(strlen($key) - 3));
                            $tmp = $newkey . '_d';
                            if (!isset($filtros[$tmp]))
                                unset($filtros[$key]);
                            continue;
                        }
                    }
                    if (strpos($key, '_v', strlen($key) - 3) !== FALSE) {
                        $newkey = str_replace('_v', '', $key, intval(strlen($key) - 3));
                        $tmp = $newkey . '_c';
                        if (isset($filtros[$tmp]))
                            $comp = $filtros[$tmp];
                        else
                            $comp = 0;
                        $filtros[$newkey] = array('valor' => $fil, 'comp' => $comp);
                        unset($filtros[$key], $filtros[$tmp]);
                        //die($key);
                    }
                    if (strpos($key, '_c', strlen($key) - 3) !== FALSE) {
                        $newkey = str_replace('_c', '', $key, intval(strlen($key) - 3));
                        $tmp = $newkey . '_v';
                        if (!isset($filtros[$tmp]))
                            unset($filtros[$key]);
                        continue;
                    }
                }

//            $campos_externos = array('estado' => 'esta.estado', 'agente' => 'est.agente', 'gerente' => 'est.gerente', 'director' => 'est.director',
//                'frecuencia_pago' => 'fre.frecuencia', 'id_plan' => 'CONCAT(plan.codigo_plan, ", ", prod.codigo_producto) AS plan',
//                'nombres' => 'CONCAT(pol.nombres, ", ", pol.apellido) AS nombres', 'pais' => 'pai.pais', 'condicion' => 'cond.condicion',
//                'forma_pago' => 'form.forma', 'id_moneda' => 'mon.moneda', 'id_compania' => 'comp.com_nombre');

                $where = '';
                $comparadores = array('>', '<', '=');
                foreach ($filtros as $key => &$filtro) {
                    if (is_array($filtro)) {
                        if (array_key_exists('desde', $filtro)) {
                            if (strpos($filtro['desde'], '/') !== FALSE) {
                                $new = explode('/', $filtro['desde']);
                                //die(var_dump($new));
                                $filtro['desde'] = $new[2] . '-' . $new[1] . '-' . $new[0];
                            }
                            if (strpos($filtro['hasta'], '/') !== FALSE) {
                                $new = explode('/', $filtro['hasta']);
                                //die(var_dump($new));
                                $filtro['hasta'] = $new[2] . '-' . $new[1] . '-' . $new[0];
                            }
                            $where .= ' AND DATE(pol.' . $key . ') BETWEEN DATE(\'' . $filtro['desde'] . '\') AND DATE(\'' . $filtro['hasta'] . '\')';
                            continue;
                        }
                        if (array_key_exists('valor', $filtro)) {
                            $where .= ' AND pol.' . $key . ' ' . $comparadores[$filtro['comp']] . ' \'' . $filtro['valor'] . '\'';
                            continue;
                        }
                    }
                    if ($key == 'gerente' || $key == 'director' || $key == 'director_general') {
                        $where .= ' AND est.' . $key . ' = \'' . $filtro . '\'';
                        continue;
                    }
                    if ($key == 'fumador')
                        $filtro = $filtro - 1;
                    $where .= ' AND pol.' . $key . ' = \'' . $filtro . '\'';
                }
                $totales_temp = array();
                foreach ($totales as $key => &$total) {
                    if (strpos($key, '_func', strlen($key) - 6) !== FALSE) {
                        $newkey = str_replace('_func', '', $key, intval(strlen($key) - 6));
                        $tmp = $newkey . '_cam';
                        if (isset($totales[$tmp]))
                            $comp = $totales[$tmp];
                        else
                            continue;
                        $totales_temp[] = array('funcion' => $total, 'campo' => $comp);
                        unset($totales[$key], $totales[$tmp]);
                        //die($key);
                    }
                }
                $consulta = array();
                $consulta['campos'] = $campos;
                $consulta['filtros'] = $filtros;
                $consulta['totales'] = $totales_temp;
                $consulta['cotizar'] = $cotizar;

//            $campos_select = array('estado' => 'esta.estado', 'id_agente' => 'est.agente', 'gerente' => 'est.gerente', 'director' => 'est.director',
//                'frecuencia_pago' => 'fre.frecuencia', 'id_plan' => 'CONCAT(prod.codigo_producto, ", ", plan.codigo_plan) AS plan',
//                'nombres' => 'CONCAT(pol.nombres, ", ", pol.apellido) AS nombres', 'pais' => 'pai.pais', 'condicion' => 'cond.condicion',
//                'forma_pago' => 'form.forma', 'id_moneda' => 'mon.moneda', 'id_compania' => 'comp.com_nombre');

                /*
                  $fields = array('estado' => array('label' => 'Estado', 'campo' => 'estado'), 'id_agente' => array('label' => 'Agente', 'campo' => 'agente'),
                  'gerente' => array('label' => 'Gerente', 'campo' => 'gerente'), 'director' => array('label' => 'Director', 'campo' => 'director'),
                  'frecuencia_pago' => array('label' => 'Frecuencia', 'campo' => 'frecuencia'), 'id_plan' => array('label' => 'Plan', 'campo' => 'codigo_plan'),
                  'forma_pago' => array('label' => 'Forma de Pago', 'campo' => 'forma'), 'id_moneda' => array('label' => 'Moneda', 'campo' => 'moneda'),
                  'id_compania' => array('label' => 'Compañía', 'campo' => 'com_codigo'), 'fumador' => array('label' => 'Fumador', 'campo' => 'fumador'),
                  'nombres' => array('label' => 'Apellido y Nombre', 'campo' => 'nombres'), 'fecha_nacimiento' => array('label' => 'Fecha de Nacimiento', 'campo' => 'fecha_nacimiento'),
                  'fecha_envio' => array('label' => 'Fecha de Envio', 'campo' => 'fecha_envio'), 'suma_asegurada' => array('label' => 'Suma Asegurada', 'campo' => 'suma_asegurada'),
                  'prima_inicial' => array('label' => 'Prima Inicial', 'campo' => 'prima_inicial'), 'prima_planeada' => array('label' => 'Planeada Anual', 'campo' => 'prima_planeada'),
                  'prima_adicional' => array('label' => 'Prima Adicional', 'campo' => 'prima_adicional'), 'prima_unica' => array('label' => 'Prima Única', 'campo' => 'prima_unica'),
                  'prima_target' => array('label' => 'Prima Target', 'campo' => 'prima_target'), 'condicion' => array('label' => 'Condicion', 'campo' => 'condicion'),
                  'pais' => array('label' => 'País', 'campo' => 'pais'), 'fecha_aprobacion' => array('label' => 'Fecha de Aprobacion', 'campo' => 'fecha_aprobacion'),
                  'fecha_recibido' => array('label' => 'Fecha de Recibido', 'campo' => 'fecha_recibido'), 'id_producto' => array('label' => 'Producto', 'campo' => 'codigo_producto'),
                  'fecha_vigencia' => array('label' => 'Fecha de Vigencia', 'campo' => 'fecha_vigencia'), 'nro_poliza' => array('label' => 'Número de Póliza', 'campo' => 'nro_poliza'));
                 */

                foreach (array_keys($filtros) as $campo) {

                    if (isset($fields[$campo]["join"]))
                        $fields[$campo]["join"]["enabled"] = TRUE;
                }

                /*
                  foreach ($campos as $campo) {

                  if (isset($fields[$campo]["join"]))
                  $fields[$campo]["join"]["enabled"] = TRUE;
                  }
                 */


//            $clean_select = "";
//            $having = "";
//            foreach ($campos as $campo) {
//                if (array_key_exists($campo, $campos_externos)) {
//                    $campos_select[] = $campos_externos[$campo];
//                    if (strlen($having) == 0) {
//                        $having .= " HAVING " . $fields[$campo]['campo'] . " != '0' AND " . $fields[$campo]['campo'] . " != '' AND " . $fields[$campo]['campo'] . " IS NOT NULL";
//                    }else
//                        $having .= " AND " . $fields[$campo]['campo'] . " != '0' AND " . $fields[$campo]['campo'] . " != '' AND " . $fields[$campo]['campo'] . " IS NOT NULL";
//                } else {
//                    $campos_select[] = 'pol.' . $campo;
//                    if ($campo != 'fumador')
//                        $clean_select .= " AND (pol." . $campo . " != '0' AND pol." . $campo . " != '' AND pol." . $campo . " != '0000-00-00' AND pol." . $campo . " IS NOT NULL)";
//                }
//            }
                //$campos_select = implode(', ', $campos_select);
//die("Filtros: " . var_export($filtros, true) . "\n Totales: " . var_export($totales, true) . "\n Campos: " . $campos_select);

                $funciones = array(2 => 'SUM(', 3 => 'AVG(', 4 => 'MAX(', 5 => 'MIN(');
                $f2 = array(2 => 'Suma ', 3 => 'Promedio ', 4 => 'Maximo ', 5 => 'Minimo ');
                $computables = array(0 => 'Campo', 1 => 'suma_asegurada', 2 => 'prima_inicial',
                    3 => 'prima_planeada', 4 => 'prima_target', 5 => 'prima_adicional', 6 => 'prima_unica');
                //$sSQL = array();
                $resultados = array();
                if ($_SESSION["is_agente"]) {
                    $where .= ' AND pol.id_agente = ' . $_SESSION["id_agente"];
                }
                if ($_SESSION["is_gerente"]) {
                    $where .= ' AND est.gerente = "' . $_SESSION["gerente"] . '"';
                }
                if ($_SESSION["is_director"]) {
                    $where .= ' AND est.director = "' . $_SESSION["director"] . '"';
                }
                if ($_SESSION["is_productor"] && $_SESSION["id_compania"] != 0) {
                    $where .= ' AND pol.id_compania = ' . $_SESSION["id_compania"];
                }
                if ($_SESSION["is_productor"] && $_SESSION["id_producto"] != 0) {
                    $where .= ' AND pol.id_producto = ' . $_SESSION["id_producto"];
                }
                if (isset($_SESSION["is_gag"]) AND $_SESSION["is_gag"] == TRUE) {
                    $joins["estructura_comercial"]["enabled"] = TRUE;
                    $where.= " OR ((est.gerente='LRK' AND pol.id_producto NOT IN(25,33)) AND (est.gerente='MEG' AND pol.id_producto NOT IN(25,33))) ";
                }



                /* xstat */
                /*
                  echo "<pre>";
                  print_r($joins);
                  echo "</pre>";
                  exit();
                 */


                foreach ($totales_temp as $total) {
                    $sSQL = 'SELECT ' . $funciones[$total['funcion']] . 'cotizar(' . $computables[$total['campo']] . ', pol.id_moneda, ' . $cotizar . ')) as resultado
		   FROM Solicitudes_Detalle pol';

                    foreach ($joins as &$join) {

                        if ($join["enabled"] === TRUE)
                            $sSQL.= " {$join["join"]} ";
                    }
                    /*
                      LEFT JOIN solicitud_estado esta ON esta.id = pol.estado
                      LEFT JOIN estructura_comercial est ON est.id_agente = pol.id_agente
                      LEFT JOIN contactos cont ON (cont.id_agente = pol.id_agente)
                      LEFT JOIN companias comp ON comp.idcompania = pol.id_compania
                      LEFT JOIN productos prod ON prod.id_producto = pol.id_producto
                      LEFT JOIN planes plan ON plan.id_plan = pol.id_plan
                      LEFT JOIN paises pai ON pai.id = pol.pais
                      LEFT JOIN monedas mon ON mon.idmoneda = pol.id_moneda
                      LEFT JOIN frecuencia_pago fre ON fre.id_frecuencia = pol.frecuencia_pago
                      LEFT JOIN forma_pago form ON form.id = pol.forma_pago
                      LEFT JOIN solicitud_condicion cond ON cond.id = pol.condicion
                     */


                    $sSQL .= ' WHERE id_solicitud <> 0 ' . $where;



                    /*
                      echo $sSQL;
                      exit();
                     */

                    $cBD = new BD();
                    /*

                      #Abrimos el fichero en modo de escritura
                      $DescriptorFichero = fopen("temporal/sql_log1.txt","w");

                      #Escribimos la segunda línea de texto
                      $string2 = "\n -------------------------------------- \n";

                      fputs($DescriptorFichero,$string2);

                      fputs($DescriptorFichero,$sSQL);

                      #Cerramos el fichero
                      fclose($DescriptorFichero);
                     */

                    /*
                      $cBD->Ejecutar('
                      //DELIMITER //;
                      //DROP FUNCTION IF EXISTS cotizar;
                      ////
                      //CREATE FUNCTION cotizar(valor float,desde int, hacia int) returns float
                      //begin
                      //    declare cotizacionDesde float;
                      //    declare cotizacionHacia float;
                      //    declare relacion float;
                      //    declare aux float;
                      //    IF desde = 0 THEN
                      //        SET desde = 2;
                      //    END IF;
                      //    SET cotizacionDesde = (SELECT cotizacion FROM monedas WHERE idmoneda = desde);
                      //    SET cotizacionHacia = (SELECT cotizacion FROM monedas WHERE idmoneda = hacia);
                      //    IF cotizacionHacia = 0 THEN
                      //        SET cotizacionHacia = 1;
                      //    END IF;
                      //    SET relacion = cotizacionDesde / cotizacionHacia;
                      //    SET aux = relacion * valor;
                      //    RETURN aux;
                      //end;
                      ////
                      //');
                     */

                    /*
                      error_log("L670", 0);
                      error_log($sSQL, 0);
                      exit();
                     */






                    $tmp = $cBD->Seleccionar($sSQL, true);
                    //$totalTmp = 0;
//                while ($moneda = $cBD->RetornarFila($tmp)) {
//                    if ($moneda['id_moneda'] != 0 && $moneda['id_moneda'] != $cotizar) {
//                        $totalTmp += cotizarMoneda($moneda['resultado'], $moneda['id_moneda'], $cotizar);
//                    }
//                    else
//                        $totalTmp += $moneda['resultado'];
//                }
                    $resultados[] = array('label' => $f2[$total['funcion']] . $fields[$computables[$total['campo']]]['label'], 'resultado' => $tmp['resultado']);
                }



                if ($_SESSION["is_agente"]) {
                    $where .= ' AND pol.id_agente = ' . $_SESSION["id_agente"];
                }
                if ($_SESSION["is_gerente"]) {
                    $where .= ' AND est.gerente = "' . $_SESSION["gerente"] . '"';
                }
                if ($_SESSION["is_director"]) {
                    $where .= ' AND est.director = "' . $_SESSION["director"] . '"';
                }
                if ($_SESSION["is_productor"] && $_SESSION["id_compania"] != 0) {
                    $where .= ' AND pol.id_compania = ' . $_SESSION["id_compania"];
                }
                if ($_SESSION["is_productor"] && $_SESSION["id_producto"] != 0) {
                    $where .= ' AND pol.id_producto = ' . $_SESSION["id_producto"];
                }



                //$sSQL = 'SELECT id_solicitud FROM Solicitudes_Detalle pol ';
                $sSQL = 'SELECT COUNT(*) AS total FROM Solicitudes_Detalle pol ';

                foreach ($joins as &$join) {
                    if ($join["enabled"] === TRUE)
                        $sSQL.= " {$join["join"]} ";
                }

                /*
                  LEFT JOIN solicitud_estado esta ON esta.id = pol.estado
                  LEFT JOIN estructura_comercial est ON est.id_agente = pol.id_agente
                  LEFT JOIN contactos cont ON (cont.id_agente = pol.id_agente)
                  LEFT JOIN companias comp ON comp.idcompania = pol.id_compania
                  LEFT JOIN productos prod ON prod.id_producto = pol.id_producto
                  LEFT JOIN planes plan ON plan.id_plan = pol.id_plan
                  LEFT JOIN paises pai ON pai.id = pol.pais
                  LEFT JOIN monedas mon ON mon.idmoneda = pol.id_moneda
                  LEFT JOIN frecuencia_pago fre ON fre.id_frecuencia = pol.frecuencia_pago
                  LEFT JOIN forma_pago form ON form.id = pol.forma_pago
                  LEFT JOIN solicitud_condicion cond ON cond.id = pol.condicion
                 */
                $sSQL.= 'WHERE id_solicitud !=  0 ' . $where;


                #Abrimos el fichero en modo de escritura 
                $DescriptorFichero = fopen("temporal/sql_log2.txt", "w");

                #Escribimos la segunda línea de texto 
                $string2 = "\n -------------------------------------- \n";

                fputs($DescriptorFichero, $string2);

                fputs($DescriptorFichero, $sSQL);

                #Cerramos el fichero 
                fclose($DescriptorFichero);


                $return_filters = array();
                $compa = array('Mayor que', 'Menor que', 'Igual a');
                foreach ($filtros as $key => &$filtro) {
                    if (is_array($filtro)) {
                        if (array_key_exists('desde', $filtro)) {
                            $return_filters[] = $fields[$key]['label'] . ' Desde: ' . date('d/m/Y', strtotime($filtro['desde'])) . ' Hasta: ' . date('d/m/Y', strtotime($filtro['hasta']));
                            continue;
                        }
                        if (array_key_exists('valor', $filtro)) {

                            $return_filters[] = $fields[$key]['label'] . ' ' . $compa[$filtro['comp']] . ' $' . $filtro['valor'];
                            continue;
                        }
                    }
                    if ($key == 'fumador') {
                        $return_filters[] = $fields[$key]['label'] . ': ' . (($filtro) ? "Si" : "No");
                        continue;
                    }
                    $return_filters[] = $fields[$key]['label'];
                }
                $return_filters = implode(', ', $return_filters);
                //die(var_dump($return_filters));
                $cBD = new BD();


                /*
                  echo $sSQL;
                  exit();
                  error_log("L770", 0);
                  error_log($sSQL, 0);
                  exit();
                 */


                $tmp = $cBD->Seleccionar($sSQL, TRUE);

                setlocale(LC_MONETARY, $locales[$cotizar]);

                $totalRes = 0;
                if (!empty($tmp))
                    $totalRes = $tmp[0];



                $out = '<script type="text/javascript">';
                if (count($resultados) > 0)
                    $out .= '$(\'#tot-funciones\').html(\'<span><strong>Totales:</strong></span>\');';
                else
                    $out .= '$(\'#tot-funciones\').remove();';
                $out .= '$(\'#tot-funciones\').append(\'| <strong>Total de registros: </strong><span style="color:#C60; font-weight:bold;"> ' . $totalRes . '</span>\');';
                foreach ($resultados as $resu) {
                    $out .= '$(\'#tot-funciones\').append(\'| <strong>' . $resu['label'] . ':</strong><span style="color:#C60; font-weight:bold;"> ' . money_format('%.2n', $resu['resultado']) . '</span>\');';
                }
                if (strlen($return_filters) > 2) {
                    $out .= '$(\'#tot-filtros\').html(\'<strong>Filtros: </strong>' . $return_filters . '\');';
                }
                else
                    $out .= '$(\'#tot-filtros\').remove();';
                $out .= '$(\'#serialized-report\').html(\'' . serialize($consulta) . '\');';
                $out .= '</script>';
                die($out);
                break;
            case "histo":
                $nombre = $_GET['nombre'];

                if (file_exists('reportes/' . $_SESSION['IDUsuario'] . '/' . $nombre . '.xml')) {
                    $xml = simplexml_load_file('reportes/' . $_SESSION['IDUsuario'] . '/' . $nombre . '.xml');
                } else {
                    die('No se ha encontrado el archivo');
                }
                //die(print_r($xml));
                ?>

            <table width="830" cellpadding="0" cellspacing="0" style="margin:12px 0px 0px 12px;" id="box-table-a" summary="Employee Pay Sheet">
                <thead>
                    <tr>
                        <?php foreach ($xml->tabla->cabecera->campo as $campo): ?>
                            <th scope="col"><span style="color:#C60; font-weight:bold;"><?php echo $campo; ?></span></th>
                        <?php endforeach; ?>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    //die($sSQL);
                    //echo($sSQL);
                    $asd = (array) $xml->tabla->cuerpo;
                    //die(var_dump($asd['fila']));
                    $paging = new PHPPaging();
                    $paging->porPagina(20);
                    $paging->linkClase("navRepo");
                    $paging->agregarArray($asd['fila']);
                    $paging->ejecutar();
                    while ((array) $aRegistro = $paging->fetchResultado()) {
                        //die(var_dump($aRegistro));
                        ?>
                        <tr>
                            <?php foreach ($aRegistro as $campo): ?>
                                <td style="padding-left:8px;"><?php
                                echo $campo;
                                ?></td>
                                <?php endforeach; ?>
                        </tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
            <div class="pagination" style="margin-right: 20px; margin-top: 5px; margin-bottom: 10px;">
                <?php echo $paging->fetchNavegacion(); ?>
            </div>
            <?php
            break;
        case "histo-tot":
            $nombre = $_GET['nombre'];

            if (file_exists('reportes/' . $_SESSION['IDUsuario'] . '/' . $nombre . '.xml')) {
                $xml = simplexml_load_file('reportes/' . $_SESSION['IDUsuario'] . '/' . $nombre . '.xml');
            } else {
                die('No se ha encontrado el archivo');
            }
            $asd = (array) $xml->resultados;
            //die(var_dump($asd['resultado']));

            $out = '<script type="text/javascript">';
            if (count($asd['resultado']) > 0)
                $out .= '$(\'#tot-funciones\').html(\'<span><strong>Totales:</strong></span>\');';
            else
                $out .= '$(\'#tot-funciones\').remove();';
            foreach ($asd['resultado'] as $resu) {
                $out .= '$(\'#tot-funciones\').append(\'| <strong>' . $resu->nombre . ':</strong><span style="color:#C60; font-weight:bold;"> ' . $resu->valor . '</span>\');';
            }
            if (strlen($xml->filtros) > 2) {
                $out .= '$(\'#tot-filtros\').html(\'<strong>Filtros: </strong>' . $xml->filtros . '\');';
            }
            else
                $out .= '$(\'#tot-filtros\').remove();';
            $out .= '$(\'#tot-registros\').html(\'Total de registros: ' . $xml->total . '\');';
            $out .= '$(\'#tot-registros\').next().css("width", "455px");';
            //$out .= '$(\'.excel_inline\').attr("onclick", "exportarReporte(\'' . $nombre . '\')");';
            $out .= '</script>';
            die($out);
            break;
    }
}
?>
