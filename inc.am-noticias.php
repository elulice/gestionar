<?php
	if($m_lIDRegistro > 0)
	{
		$sSQL = "SELECT fecha, volanta, copete, titulo, ";
		$sSQL .= "contenido, epigrafe, fuente, portada, destacada ";
		$sSQL .= "FROM noticias WHERE idnoticia = " . $m_lIDRegistro;
		$cBD = new BD();
		$aRegistro = $cBD->Seleccionar($sSQL, true);
	}
	else
	{
		$aRegistro["fecha"] = date("Y-m-d");
		$aRegistro["idseccion"] = is_numeric($_GET["idseccion"]) ? $_GET["idseccion"] : 0;
		$aRegistro["portada"] = "S";
		$aRegistro["destacada"] = "S";
		$aRegistro["visible"] = "S";
	}
?>
<table width="780" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="30" class="encabezado-titulo-bg"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="20"><img src="images/espacio.gif" width="1" height="1"></td>
        <td class="encabezado-titulo-texto">Alta y modificaci&oacute;n de Noticias </td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><img src="images/espacio.gif" width="1" height="20"></td>
  </tr>
</table>
<table width="500" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="20"><img src="images/formulario-encabezado-inicio.jpg" width="20" height="37"></td>
        <td class="formulario-encabezado-bg">Noticias</td>
        <td width="20"><img src="images/formulario-encabezado-final.jpg" width="20" height="37"></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="4" class="formulario-contenido-inicio"><img src="images/espacio.gif" width="1" height="1"></td>
        <td><form action="abm.php?tabla=noticias&amp;columna=idnoticia&amp;idregistro=<?php print($m_lIDRegistro); ?>&amp;archivo=1&amp;contador=0&amp;thumbs=3,4&amp;url=<?php print($m_sURL); ?>" method="post" enctype="multipart/form-data" name="frmRegistro" id="frmRegistro">
          <table width="100%" border="0" cellspacing="0" cellpadding="4">
          <tr>
            <td width="150" class="formulario-etiquetas">Fecha: </td>
            <td width="340"><input name="fecha" type="text" class="formulario-textbox" id="fecha" style="width: 120px;" value="<?php print($aRegistro["fecha"]); ?>" maxlength="10"></td>
          </tr>
<!--          <tr>
            <td class="formulario-etiquetas">Secci&oacute;n:</td>
            <td><select name="idseccion" id="idseccion" style="width: 300px;">
              <?php
					$sSQL = "SELECT idseccion, descripcion FROM secciones  ";
					$sSQL .= "ORDER BY descripcion ASC";
					print(GenerarOptions($sSQL, $aRegistro["idseccion"]));
			  ?>
            </select></td>
          </tr>
-->          <tr>
            <td class="formulario-etiquetas">Volanta:</td>
            <td><input name="volanta" type="text" class="formulario-textbox" id="volanta" style="width: 300px;" value="<?php print($aRegistro["volanta"]); ?>" /></td>
          </tr>
          <tr>
            <td class="formulario-etiquetas">T&iacute;tulo:</td>
            <td><input name="titulo" type="text" class="formulario-textbox" id="titulo" style="width: 300px;" value="<?php print($aRegistro["titulo"]); ?>" maxlength="255" /></td>
          </tr>
          <tr>
            <td class="formulario-etiquetas">Copete:</td>
            <td><input name="copete" type="text" class="formulario-textbox" id="copete" style="width: 300px;" value="<?php print($aRegistro["copete"]); ?>" /></td>
          </tr>
          
          <tr>
            <td colspan="2" class="formulario-etiquetas">Contenido:</td>
            </tr>
          <tr>
            <td colspan="2" align="center"><textarea name="contenido" id="contenido" style="height: 400px; width: 470px;"><?php print($aRegistro["contenido"]); ?></textarea></td>
          </tr>
          <tr>
            <td class="formulario-etiquetas">Imagen :</td>
            <td><input name="FILE_Imagen_1" type="file" class="formulario-textbox" id="FILE_Imagen_1" style="width: 300px;" /></td>
          </tr>
          
          <tr>
            <td class="formulario-etiquetas">Ep&iacute;grafe:</td>
            <td><input name="epigrafe" type="text" class="formulario-textbox" id="epigrafe" style="width: 300px;" value="<?php print($aRegistro["epigrafe"]); ?>" maxlength="255" /></td>
          </tr>
          <tr>
            <td class="formulario-etiquetas">Fuente:</td>
            <td><input name="fuente" type="text" class="formulario-textbox" id="fuente" style="width: 300px;" value="<?php print($aRegistro["fuente"]); ?>" maxlength="255" /></td>
          </tr>
          <tr>
            <td class="formulario-etiquetas">Visualizar en portada: </td>
            <td><table width="100" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="25"><input name="portada" type="radio" value="S" <?php if($aRegistro["portada"] == "S") { ?>checked="checked" <?php } ?>/></td>
                <td width="25" class="formulario-textbox">Si</td>
                <td width="25"><input name="portada" type="radio" value="N" <?php if($aRegistro["portada"] == "N") { ?>checked="checked" <?php } ?>/></td>
                <td class="formulario-textbox">No</td>
              </tr>
            </table></td>
          </tr>
		   <tr>
            <td class="formulario-etiquetas">Noticia Destacada: </td>
            <td><table width="100" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="25"><input name="destacada" type="radio" value="S" <?php if($aRegistro["destacada"] == "S") { ?>checked="checked" <?php } ?>/></td>
                <td width="25" class="formulario-textbox">Si</td>
                <td width="25"><input name="destacada" type="radio" value="N" <?php if($aRegistro["destacada"] == "N") { ?>checked="checked" <?php } ?>/></td>
                <td class="formulario-textbox">No</td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td height="30" class="formulario-etiquetas"><img src="images/espacio.gif" width="1" height="1"></td>
            <td align="center"><input name="BTN_Guardar" type="submit" id="BTN_Guardar" value="Guardar">
              <input name="BTN_Cancelar" type="reset" id="BTN_Cancelar" value="Cancelar" onclick="history.back()"></td>
          </tr>
        </table>
        </form></td>
        <td width="6" class="formulario-contenido-final"><img src="images/espacio.gif" width="1" height="1"></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="20"><img src="images/formulario-pie-inicio.jpg" width="20" height="40"></td>
        <td class="formulario-pie-bg"><img src="images/espacio.gif" width="1" height="1"></td>
        <td width="20"><img src="images/formulario-pie-final.jpg" width="20" height="40"></td>
      </tr>
    </table></td>
  </tr>
</table>
<script language="JavaScript" type="text/javascript">
window.onload = function()
	{
		oEditor = new Bs_Editor("oEditor");
		with(oEditor)
		{
			hrefSelector = new Array("blueshoes-4.5/javascript/components/editor/windowHref.html", 490, 350);
			specialCharSelector = new Array("blueshoes-4.5/javascript/components/editor/windowSpecialChar.html", 450, 300);
			editareaOnPaste = 4;
			mayResize = false;
			editorCssString = "body { font-family: Arial, Helvetica, sans-serif; font-size: 11px; }";
			buttonsDefaultPath = "blueshoes-4.5/images/buttons/";
			bsImgPath = "blueshoes-4.5/images/";
			loadButtonsWysiwyg();
			buttonsWysiwyg["font"] = null;
			buttonsWysiwyg["forecolor"] = null;
			buttonsWysiwyg["outdent"] = null;
			buttonsWysiwyg["indent"] = null;
			buttonsWysiwyg["insertimage"] = null;
			buttonsWysiwyg["justifyleft"] = null;
			buttonsWysiwyg["justifycenter"] = null;
			buttonsWysiwyg["justifyright"] = null;
			loadButtonsHtml();
			buttonsHtml = true;
			convertField("contenido");
		}
	}
</script>
