<?php
require_once("clases/framework-1.0/class.bd.php");
require_once("clases/phppaging/PHPPaging.lib.php");
require_once("includes/funciones.php");

if ($_GET["id_oferta"] != null && $_GET['id_oferta'] > 0) {

    $sSQL = "SELECT
                DATE_FORMAT(o.OfeFechAlta,'%d/%m/%Y') AS OfeFechaAltaF
                , DATE_FORMAT(o.OfeValidez,'%d/%m/%Y') AS OfeValidezF
                , IF(o.OfeVisible,'Si','No') as OfeVisibleF
                , IF(o.OfeDestacada,'Si','No') as OfeDestacada
                , eo.EOfeValor
                , o.OfeFactura
                , c.CliRsocial
                , o.OfePedidoPor
                , o.OfeDireccion
                , o.OfeEMail
                , o.OfeURL
                , o.OfeReferencia
                , o.OfeCantPost
                , a.AreNom
                , p.PueNom
                , o.OfeTitulo
                , o.OfeOferta
                , z.ZonDescrip
                , tp.nombre AS TipoPedido
                , o.OfeDeEdad
                , o.OfeHtaEdad
                , s.nombre AS Sexo
                , o.OfeEducacion
                , o.OfeProvincia
                , o.OfeLocalidad
                , IF(o.OfeIngles,'Si','No') as OfeInglesF
                , o.OfeExpAnios
                , o.OfeRemunerac
                , o.OfeDescTar
                , o.OfeClasificacion
                , o.OfeDescSel
                , u.UniNombre
            FROM
                oferta o
                LEFT JOIN ofertascliente oc ON o.OfeNro = oc.OfeNro
                LEFT JOIN cliente c ON oc.CliNro = c.CliNro
                LEFT JOIN estadooferta eo ON eo.OfeNro = o.OfeNro
                LEFT JOIN area a ON o.AreNro = a.AreNro
                LEFT JOIN puesto p ON p.PueNro = o.PueNro
                LEFT JOIN zona z ON z.ZonNro = o.ZonNro
                LEFT JOIN _tipo_pedido tp ON tp.idtipo = o.TPeNro
                LEFT JOIN _sexos s ON s.idsexo = o.OfeSexo
                LEFT JOIN unidadorg u ON u.UniNro = o.NodNro
            WHERE
                o.OfeNro = " . $_GET["id_oferta"];

    //secho $sSQL;

    $db = new BD();
    $db->Conectar();
    $row = $db->Seleccionar($sSQL, TRUE);
} else
    $row = FALSE;

//print_r($row);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Gesti&oacute;n Consultores</title>
        <link rel="stylesheet" type="text/css" href="css/print.css" />
    </head>
    <body onload="window.print(); window.close();">
        <div id="main-container">
            <h1><?php echo $row['OfeReferencia']; ?>: <?php echo $row['OfeTitulo']; ?> en <?php echo $row['CliRsocial']; ?>.</h1>
            <div class="column">
                <h2>Generales.</h2>
                <dl>
                    <dt>Fecha Alta:</dt>
                    <dd><?php echo $row['OfeFechaAltaF']; ?></dd>
                    <dt>Vigencia:</dt>
                    <dd><?php echo $row['OfeValidezF']; ?></dd>
                    <dt>Visible:</dt>
                    <dd><?php echo $row['OfeVisibleF']; ?></dd>
                    <dt>Destacada:</dt>
                    <dd><?php echo $row['OfeDestacada']; ?></dd>
                    <dt>Situaci&oacute;n:</dt>
                    <dd><?php echo ($row['EOfeValor'] != '' ? $row['EOfeValor'] : 'Vigente'); ?></dd>
                    <dt>Factura:</dt>
                    <dd><?php echo $row['OfeFactura']; ?></dd>
                </dl>
                <h2>Datos de la Usuaria.</h2>
                <dl>
                    <dt>Empresa:</dt>
                    <dd><?php echo $row['CliRsocial']; ?></dd>
                    <dt>Solicitante:</dt>
                    <dd><?php echo $row['OfePedidoPor']; ?></dd>
                    <dt>Direcci&oacute;n:</dt>
                    <dd><?php echo $row['OfeDireccion']; ?></dd>
                    <dt>Email:</dt>
                    <dd><?php echo $row['OfeEMail']; ?></dd>
                    <dt>Url:</dt>
                    <dd><?php echo $row['OfeURL']; ?></dd>
                </dl>
                <h2>Datos de la publicaci&oacute;n.</h2>
                <dl>
                    <dt>N&ordm; Referencia:</dt>
                    <dd><?php echo $row['OfeReferencia']; ?></dd>
                    <dt>Vacantes:</dt>
                    <dd><?php echo $row['OfeCantPost']; ?></dd>
                    <dt>&Aacute;rea de inter&eacute;s:</dt>
                    <dd><?php echo $row['AreNom']; ?></dd>
                    <dt>Puesto:</dt>
                    <dd><?php echo $row['PueNom']; ?></dd>
                    <dt>T&iacute;tulo:</dt>
                    <dd><?php echo $row['OfeTitulo']; ?></dd>
                    <dt>Descripci&oacute;n:</dt>
                    <dd><?php echo $row['OfeOferta']; ?></dd>
                    <dt>Zona:</dt>
                    <dd><?php echo $row['ZonDescrip']; ?></dd>
                </dl>
            </div>
            <div class="column">
                <h2>Datos Generales.</h2>
                <dl>
                    <dt>Tipo B&uacute;squeda:</dt>
                    <dd><?php echo $row['TipoPedido']; ?></dd>
                    <dt>Edad:</dt>
                    <dd>Desde <?php echo $row['OfeDeEdad']; ?>. Hasta <?php echo $row['OfeHaEdad']; ?></dd>
                    <dt>Sexo:</dt>
                    <dd><?php echo $row['Sexo']; ?></dd>
                    <dt>Nivel Educaci&oacute;n:</dt>
                    <dd><?php echo $row['OfeEducacion']; ?></dd>
                    <dt>Provincia:</dt>
                    <dd><?php echo $row['OfeProvincia']; ?></dd>
                    <dt>Localidad:</dt>
                    <dd><?php echo $row['OfeLocalidad']; ?></dd>
                    <dt>Habla Ingl&eacute;s:</dt>
                    <dd><?php echo $row['OfeInglesF']; ?></dd>
                    <dt>Exp. Previa:</dt>
                    <dd><?php echo $row['OfeExpAnios']; ?></dd>
                    <dt>Remuneraci&oacute;n:</dt>
                    <dd><?php echo $row['OfeRemunerac']; ?></dd>
                    <dt>Tareas:</dt>
                    <dd><?php echo $row['OfeDescTar']; ?></dd>
                    <dt>Clasificaci&oacute;n:</dt>
                    <dd><?php echo $row['OfeClasificacion']; ?></dd>
                    <dt>Observaciones:</dt>
                    <dd><?php echo $row['OfeDescSel']; ?></dd>
                    <dt>Sucursal:</dt>
                    <dd><?php echo $row['UniNro']; ?></dd>
                </dl>
            </div>
        </div>
    </body>
</html>
