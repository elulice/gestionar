<div id="dlg_detalle_usuario" style="display:none;">
    <form id="frm_detalle_usuario" method="post" action="abm.php?tabla=miembroempresa&amp;columna=MEmpNro&amp;idregistro=" >
        <input type="hidden" name="MEmpNro" />
        <input type="hidden" name="PerNro" />
        <input type="hidden" name="EmpNro" value="313" />
        <div class="ddu_campo">
            <span>Apellido:</span>
            <input type="text" name="MEmpApellido" class="smallInput" />
        </div>
        <div class="ddu_campo">
            <span>Nombres:</span>
            <input type="text" name="MEmpNombres" class="smallInput" />
        </div>
        <div class="ddu_campo">
            <span>Email:</span>
            <input type="text" name="MEmpEmail" class="smallInput" />
        </div>
        <div class="ddu_campo">
            <span>Usuario:</span>
            <input type="text" name="MEmpUsuario" class="smallInput" />
        </div>
        <div class="ddu_campo">
            <span>Clave:</span>
            <input type="password" name="MEmpClave" class="smallInput" />
        </div>
        <div class="ddu_campo">
            <span>Confirma Clave:</span>
            <input type="password" id="MEmpClave" name="MEmpClaveRepeat" class="smallInput" />
        </div>
        <div class="ddu_campo">
            <span>Detalle:</span>
            <input type="text" name="MEmpDetalle" class="smallInput" />
        </div>
        <div class="ddu_campo_select">
            <span>Reportes:</span>
            <select name="reportes" class="smallInput">
                <option value="0">No</option>
                <option value="1">Si</option>
            </select>
        </div>
        <div class="ddu_campo_select">
            <span>Tipo Usuario:</span>
            <select name="MEmpAdmin" class="smallInput">
                <?php
                $query = "SELECT TMiembNro, TMiembDescrip FROM tipomiembro ORDER BY TMiembDescrip";
                echo GenerarOptions($query, '-1', TRUE, "-", TRUE, "usuarios");
                ?>
            </select>
        </div>
        <div class="ddu_campo_select">
            <span>Sucursal:</span>
            <select name="UniNro" class="smallInput">
                <?php
                $query = "SELECT UniNro, UniNombre FROM unidadorg WHERE EmpNro=" . RetornarIdEmpresa();
                echo GenerarOptions($query, NULL);
                ?>
            </select>
        </div>
    </form>
</div>
<div style="margin:8px 0 0 18px;">
    <form id="frm_usuarios_listado_filtro" target="hidden_iframe" onsubmit="cargar_lista_usuarios();">
        <input type="submit" style="display:none;" />
        <table width="833" cellspacing="3" style="margin-left:40px;">
            <tbody>
                <tr>
                    <td width="230">
                        <div style="float:left;width:220px;">
                            <div class="form-label">Nombres:</div>
                            <input type="text" class="smallInput" onchange="cargar_lista_usuarios();" name="nombres" style="width:130px;float:right;" />
                        </div>
                    </td>
                    <td width="230">
                        <div style="float:left;width:220px;">
                            <div class="form-label">Apellido:</div>
                            <input type="text" class="smallInput" onchange="cargar_lista_usuarios();" name="apellido" style="width:130px;float:right;" />
                        </div>
                    </td>
                    <td width="230">
                        <div style="float:left;">
                            <div class="form-label">Sucursal:</div>
                            <select class="smallInput" onchange="cargar_lista_usuarios();" name="sucursal" style="margin-left:20px;">
                                <?php
                                $query = "SELECT UniNro, UniNombre FROM unidadorg ORDER BY UniNombre";
                                echo GenerarOptions($query, NULL, TRUE, DEFSELECT);
                                ?>
                            </select>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td width="230"></td>
                    <td width="230"></td>
                    <td width="290">
                        <a class="button_notok" onclick="$('#frm_usuarios_listado_filtro').clearForm();cargar_lista_usuarios();" style="margin-top:3px;"><span>Limpiar Búsqueda</span></a>
                        <a class="button_ok" onclick="cargar_lista_usuarios();" style="margin-top:3px;"><span>Buscar</span></a>
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
</div>
<div id="div_usuarios_lista" class="navPage" style="height:350px;">
    <img src="images/loading.gif" class="loading" />
</div>
