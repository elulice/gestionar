<?php
	session_start();
	include("clases/framework-1.0/class.bd.php");
	include("includes/funciones.php");
	require_once ('clases/phppaging/PHPPaging.lib.php');

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?php print SITIO; ?></title>
<script language="JavaScript" type="text/javascript" src="scripts/ajax.php"></script>
<script language="JavaScript" type="text/javascript" src="scripts/general.js"></script>

<link href="estilos/general.css" rel="stylesheet" type="text/css">
<link href="estilos/vistas.css" rel="stylesheet" type="text/css">
</head>

<body>
<?php
	include("inc.encabezado.php");
	include("inc.listado-clientes.php");
	include("inc.pie.php");
?>
</body>
</html>
