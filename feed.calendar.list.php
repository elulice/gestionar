<?php
include("includes/funciones.php");
include("clases/framework-1.0/class.bd.php");
require_once ("clases/phppaging/PHPPaging.lib.php");
?>
<ul class = "news_items">
    <?php
//    $sSQL = "SELECT * FROM events WHERE MONTH(date) = " . date("m") . " AND DAY(date) >= " . date("d");
    $sSQL = "SELECT * FROM events WHERE MONTH(date) = " . date("m");
    $sSQL .= " ORDER BY date ASC";
    $cBD = new BD();
    $cBD->Conectar();

    $paging = new PHPPaging($cBD->RetornarConexion());
    $paging->porPagina(3);
    $paging->linkClase("navPage");
    $paging->agregarConsulta($sSQL);
    $paging->ejecutar();

    while ($evento = $paging->fetchResultado()) {
        ?>
        <li>
            <h5><?php echo date("d-m-Y", strtotime($evento['date'])); ?></h5>
            <h6 style="width: 450px;">
                <a style=" color: #4985B2;text-decoration: none;" href="ver.php?elemento=evento&amp;id=<?php echo $evento['idevento']; ?>" class="verEvento">
                    <?php echo $evento['heading']; ?></a>
                <span style="width: 80px; float: right; *margin-top:-20px;">
                <!--        <a title="Notificar" href="javascript: void(0);" onclick="$('#notify_id').val(<?php // echo $evento['idevento'];       ?>);$('#notify_tipo').val('evento');$('#notify').dialog('open');"><img src="images/icons/edit.gif" width="16" height="16" /></a>&nbsp;<a title="Enviar Mail" href="javascript: void(0);" onclick="$('#email_id').val(<?php // echo $evento['idevento'];       ?>);$('#email_tipo').val('evento');$('#emailAsunto').val('<?php // echo $evento['heading'];       ?>');$('#enviarPorMail').dialog('open');"><img src="images/icons/Forward.png" width="16" height="16" /></a>&nbsp;<a title="Imprimir" href="ver.php?elemento=evento&amp;id=<?php // echo $evento['idevento'];       ?>&amp;print=1" target="_blank"><img src="images/icons/printer.png" width="16" height="16" /></a> ---> 
                    <a title="Ver" class="verEvento" href="ver.php?elemento=evento&amp;id=<?php echo $evento['idevento']; ?>"><img src="images/icons/zoom_in.png" width="16" height="16" />
                    </a>
                    <a id="a_lala" href="abm.php?tabla=events&columna=idevento&idregistro=<?php echo $evento["idevento"]; ?>&ajax=true&urlback=escritorio.php" class="delete">
                        <img src="images/icons/page_delete.png" />
                    </a>
                </span>
            </h6>
        </li>
    <?php } ?>
</ul>
<div class="pagination" style="margin-top: -5px; *margin-top:-10px;">
    <?php echo $paging->fetchNavegacion(); ?>
</div>
