<?php
$link = array();
$java = "javascript:void(0)";
$_SESSION["autorizar_respuesta"] = "";
if ($_SESSION["reportes"] == '1') {
    $reportes = "showReportes.php";
} else {
    $reportes = $java;
}

// Si NO es Administrador
if ($_SESSION["tipo_usuario"] != 1) {

    // Si es Responsable
    if ($_SESSION["tipo_usuario"] == 0) {
        $link[0] = "escritorio.php";
        $link[1] = "$java";
        $link[2] = "tickets.php";
        $link[3] = "$reportes";
        $link[4] = "$java";
        $link[5] = "$java";
        $_SESSION["autorizar_respuesta"] = false;
    }

    // Si es Operador
    if ($_SESSION["tipo_usuario"] == 6) {
        $link[0] = "escritorio.php";
        $link[1] = "usuarias.php";
        $link[2] = "tickets.php";
        $link[3] = "$reportes";
        $link[4] = "noticias.php";
        $link[5] = "$java";
        $_SESSION["autorizar_respuesta"] = false;
    }

    // Si es Supervisor
    if ($_SESSION["tipo_usuario"] == 7) {
        $link[0] = "escritorio.php";
        $link[1] = "usuarias.php";
        $link[2] = "tickets.php";
        $link[3] = "$reportes";
        $link[4] = "noticias.php";
        $link[5] = "$java";
        $_SESSION["autorizar_respuesta"] = true;
    }
} else {

    // Si es administrador
    $link[0] = "escritorio.php";
    $link[1] = "usuarias.php";
    $link[2] = "tickets.php";
    $link[3] = "$reportes";
    $link[4] = "noticias.php";
    $link[5] = "$java";
    $_SESSION["autorizar_respuesta"] = true;
}

if ($_SESSION["strict_menu"] == true) {
    $link[6] = "usuarios.php";
    $link[7] = "opciones.php";
} else {
    $link[6] = "$java";
    $link[7] = "$java";
}
?>
<div id="menu" style="text-align: center">
    <ul class="group" id="menu_group_main">
        <li class="item first" id="one">
            <a href="<?php echo $link[0]; ?>" class="main <?php echo ($seccion == "escritorio") ? "current" : ""; ?>"><span class="outer"><span class="inner dashboard png">Escritorio</span></span></a></li>
        <li class="item middle" id="two">
            <a href="<?php echo $link[1]; ?>" class="main <?php echo ($seccion == "usuarias" ) ? "current" : ""; ?>"><span class="outer"><span class="inner usuarias">Usuarias</span></span></a></li>
        <!--        <li class="item middle" id="four">
                    <a href="pedidos.php" class="main <?php echo ($seccion == "pedidos") ? "current" : ""; ?>"><span class="outer"><span class="inner pedidos">Pedidos</span></span></a></li>-->
        <!--        <li class="item middle" id="two">
                    <a href="agenda.php" class="main <?php echo ($seccion == "agenda") ? "current" : ""; ?>"><span class="outer"><span class="inner agenda">Agenda</span></span></a></li>-->
        <!--        <li class="item middle" id="five">
                    <a href="curriculums.php" class="main <?php echo ($seccion == "curriculums") ? "current" : ""; ?>"><span class="outer"><span class="inner curriculums">Administrar CV</span></span></a></li>-->
        <!--li class="item middle" id="three">
            <a href="trayectoria.php" class="main <?php echo ($seccion == "trayectoria") ? "current" : ""; ?>"><span class="outer"><span class="inner trayectoria png">Trayectoria</span></span></a></li-->
        <li class="item middle" id="three">
            <a href="<?php echo $link[2]; ?>" class="main <?php echo ($seccion == "TicketsP") ? "current" : ""; ?>"><span class="outer"><span class="inner reclamos">Tickets</span></span></a>
        </li>
        <li class="item middle" id="four">
            <a href="<?php echo $link[3]; ?>" class="main <?php echo ($seccion == "TicketsPReportes") ? "current" : ""; ?>"><span class="outer"><span class="inner reportes">Reportes</span></span></a>
        </li>
        <li class="item middle" id="five">
            <a href="<?php echo $link[4]; ?>" class="main <?php echo ($seccion == "noticias") ? "current" : ""; ?>"><span class="outer"><span class="inner noticias">Noticias</span></span></a>
        </li>
        <li class="item middle" id="six">
            <a href="<?php echo $link[5]; ?>" class="main <?php echo ($seccion == "descargas") ? "current" : ""; ?>"><span class="outer"><span class="inner descargas">Descargas</span></span></a>
        </li>
        <li class="item middle" id="seven">
            <a href="<?php echo $link[6]; ?>" class="main <?php echo ($seccion == "usuarios") ? "current" : ""; ?>"><span class="outer"><span class="inner usuarios">Usuarios</span></span></a>
        </li>
        <li class="item last" id="eight">
            <a href="<?php echo $link[7]; ?>" class="main <?php echo ($seccion == "opciones") ? "current" : ""; ?>"><span class="outer"><span class="inner opciones">Opciones</span></span></a>
        </li>
        <!--<li class="item middle" id="seven"><a><span class="outer"><span class="inner newsletter">Comisiones</span></span></a></li>        -->
    </ul>
</div>
