<div id="portlets">
                    <div class="grid_8" style="width: 165px;">
                        <h1 class="prod_edit">Producci&oacute;n</h1></div>
                    <div style=" float:right;display:block; width:580px; margin-top:18px;">
                        <a href="javascript: void(0);" onClick="$('#historiales-dialog').dialog('open')" class="buttons buttons-history" style="width: 100px; font-size:11px; font-weight: normal; float: left; margin-left: 5px;">Historial</a>
                        <a href="javascript: void(0);" onClick="$('#reportes-dialog').dialog('open')" class="buttons buttons-cargar" style="width: 100px; font-size:11px; font-weight: normal; float: left; margin-left: 5px;">Cargar</a>
                        <a href="javascript: void(0);" onClick="if($('[name=id_reporte]').val() == 0){$('#nombre-dialog').dialog('open');} else{guardarReporte();}" class="buttons buttons-guardar" style="width: 100px; font-size:11px; font-weight: normal; float: left; margin-left: 5px;">Guardar</a>
                        <a href="javascript: void(0);" onClick="$('#nombre-dialog').dialog('open');" class="buttons buttons-guardar-como" style="width: 120px; font-size:11px; font-weight: normal; float: left; margin-left: 5px;">Guardar como</a>
                        <a href="javascript: void(0);" onClick="generarReporteProduccion();" class="buttons buttons-consultar" style="width: 100px; font-size:11px; font-weight: bold; float: left; margin-left: 5px;">Consultar</a>
                    </div>
                    <div class="clear"></div>
                    <form action="#" method="post" enctype="multipart/form-data" name="formProduccion" id="formProduccion">
                        <div class="portlet">
                            <span class="portlet-header" style="color: #333333 !important;">Datos a Consultar</span>
                            <div style="width:880px; border-top: 1px solid #CCCCCC; display:inline; float:left; height:120px; overflow:hidden; padding: 10px 0px 0px 25px;">

                                <input type="submit" style="display:none" value="Submit" />

                                <div style="width:125px; margin-right:15px; display:block; float:left;">
                                    <div class="form-administrar2"><input name="nombres" id="nombres" type="checkbox" class="form-contacto-text selectCheckbox"/> <label for="nombres">Apellido y Nombre</label></div>
                                    <div class="form-administrar2"><input name="fecha_nacimiento" id="fecha_nacimiento" type="checkbox" class="form-contacto-text selectCheckbox"/> <label for="fecha_nacimiento">Fecha de Nacim.</label></div>
                                    <div class="form-administrar2"><input name="pais" id="pais" type="checkbox" class="form-contacto-text selectCheckbox"/> <label for="pais">Pa&iacute;s</label></div>
                                    <div class="form-administrar2"><input name="fumador" id="fumador" type="checkbox" class="form-contacto-text selectCheckbox"/> <label for="fumador">Fumador</label></div>

                                </div>

                                <div style="width:100px; margin-right:15px; display:block; float:left;">
                                    <div class="form-administrar2"><input name="id_compania" id="id_compania" type="checkbox" class="form-contacto-text selectCheckbox"/> <label for="id_compania">Compa&ntilde;&iacute;a</label></div>
                                    <div class="form-administrar2"><input name="id_producto" id="id_producto" type="checkbox" class="form-contacto-text selectCheckbox"/> <label for="id_producto">Producto</label></div>
                                    <div class="form-administrar2"><input name="id_plan" id="id_plan" type="checkbox" class="form-contacto-text selectCheckbox"/> <label for="id_plan">Plan</label></div>
                                    <div class="form-administrar2"><input name="fecha_envio" id="fecha_envio" type="checkbox" class="form-contacto-text selectCheckbox"/> <label for="fecha_envio">Fecha de Env&iacute;o</label></div>

                                </div>
                                <div style="width:125px; margin-right:15px; display:block; float:left;">
                                    <div class="form-administrar2"><input name="forma_pago" id="forma_pago" type="checkbox" class="form-contacto-text selectCheckbox"/> <label for="forma_pago">Tipo de Pago</label></div>
                                    <div class="form-administrar2"><input name="id_moneda" id="id_moneda" type="checkbox" class="form-contacto-text selectCheckbox"/> <label for="id_moneda">Moneda</label></div>
                                    <div class="form-administrar2"><input name="suma_asegurada" id="suma_asegurada" type="checkbox" class="form-contacto-text selectCheckbox"/> <label for="suma_asegurada">Suma Asegurada</label></div>
                                    <div class="form-administrar2"><input name="prima_inicial" id="prima_inicial" type="checkbox" class="form-contacto-text selectCheckbox" /> <label for="prima_inicial">Prima Inicial</label></div>

                                </div>
                                <div style="width:100px; margin-right:15px; display:block; float:left;">
                                    <div class="form-administrar2"><input name="prima_planeada" id="prima_planeada" type="checkbox" class="form-contacto-text selectCheckbox" /> <label for="prima_planeada">Prima Anual</label></div>
                                    <div class="form-administrar2"><input name="prima_target" id="prima_target" type="checkbox" class="form-contacto-text selectCheckbox" /> <label for="prima_target">Prima Target</label></div>
                                    <div class="form-administrar2"><input name="prima_adicional" id="prima_adicional" type="checkbox" class="form-contacto-text selectCheckbox" /> <label for="prima_adicional">Prima Adicional</label></div>
                                    <div class="form-administrar2"><input name="prima_unica" id="prima_unica" type="checkbox" class="form-contacto-text selectCheckbox" /> <label for="prima_unica">Prima &Uacute;nica</label></div>

                                </div>
                                <div style="width:125px; margin-right:15px; display:block; float:left;">
                                    <div class="form-administrar2"><input name="estado" id="estado" type="checkbox" class="form-contacto-text selectCheckbox" /> <label for="estado">Estado</label></div>
                                    <div class="form-administrar2"><input name="condicion" id="condicion" type="checkbox" class="form-contacto-text selectCheckbox" /> <label for="condicion">Condicion</label></div>
                                    <div class="form-administrar2"><input name="fecha_aprobacion" id="fecha_aprobacion" type="checkbox" class="form-contacto-text selectCheckbox" /> <label for="fecha_aprobacion">Fecha de Aprobacion</label></div>
                                    <div class="form-administrar2"><input name="fecha_recibido" id="fecha_recibido" type="checkbox" class="form-contacto-text selectCheckbox" /> <label for="fecha_recibido">Fecha de Recepcion</label></div>
                                </div>

                                <div style="width:100px; margin-right:15px; display:block; float:left;">
                                    <div class="form-administrar2"><input name="fecha_vigencia" id="fecha_vigencia" type="checkbox" class="form-contacto-text selectCheckbox"> <label for="fecha_vigencia">Fecha de Vig.</label></div>
                                    <div class="form-administrar2"><input name="id_agente" id="agente" type="checkbox" class="form-contacto-text selectCheckbox" /> <label for="agente">Agente</label></div>
                                    <div class="form-administrar2"><input name="gerente" id="gerente" type="checkbox" class="form-contacto-text selectCheckbox" /> <label for="gerente">Gerente</label></div>
                                    <div class="form-administrar2"><input name="director" id="director" type="checkbox" class="form-contacto-text selectCheckbox" /> <label for="director">Director</label></div>
                                </div>
                                <div style="width:100px; margin-right:15px; display:block; float:left;">
                                    <div class="form-administrar2"><input name="frecuencia_pago" id="frecuencia_pago" type="checkbox" class="form-contacto-text selectCheckbox"/> <label for="frecuencia_pago">Frecuencia</label></div>
                                    <div class="form-administrar2"><input name="nro_poliza" id="nro_poliza" type="checkbox" class="form-contacto-text selectCheckbox"/> <label for="nro_poliza">Nro. Póliza</label></div>
                                    <div class="form-administrar2"><input name="mes_envio" id="mes_envio" type="checkbox" class="form-contacto-text selectCheckbox"/> <label for="mes_envio">Mes de Envío</label></div>
                                    <div class="form-administrar2"><input name="anio_envio" id="anio_envio" type="checkbox" class="form-contacto-text selectCheckbox"/> <label for="anio_envio">Año de Envío</label></div>
                                </div>
                            </div>
                        </div>
                        <div class="portlet">
                            <span class="portlet-header" style="color: #333333 !important;">Filtros</span>
                            <div style="width:880px; display:inline; border-top: 1px solid #CCCCCC; float:left; height:180px; overflow:hidden; padding: 10px 0px 0px 25px; font-weight: normal;" >
                                <div style="width:190px; margin-right:15px; display:block; float:left;">
                                    <div class="form-administrar2" style="width:190px;"><label for="f_fecha_envio_d" style="width:120px;">Fecha de Envio desde:</label>
                                        <input type="text" name="f_fecha_envio_d" id="f_fecha_envio_d" style="width:60px; float:right;" value="" class="datepicker form-contacto-text smallInput"/>
                                    </div>
                                    <div class="form-administrar2" style="width:190px;"><label for="f_fecha_envio_h" style="width:120px;">Fecha de Envio hasta:</label>
                                        <input type="text" name="f_fecha_envio_h" id="f_fecha_envio_h"  style="width:60px; float:right;" value="<?php echo date('Y-m-d'); ?>" class="datepicker form-contacto-text smallInput"/>
                                    </div>
                                    <div class="form-administrar2" style="width:190px;"><label for="f_fecha_nacimiento_d" style="width:120px;">Fecha de Nac. desde:</label>
                                        <input type="text" name="f_fecha_nacimiento_d" id="f_fecha_nacimiento_d" style="width:60px; float:right;" value="" class="datepicker form-contacto-text smallInput"/>
                                    </div>
                                    <div class="form-administrar2" style="width:190px;"><label for="f_fecha_nacimiento_h" style="width:120px;">Fecha de Nac. hasta:</label>
                                        <input type="text" name="f_fecha_nacimiento_h" id="f_fecha_nacimiento_h"  style="width:60px; float:right;" value="" class="datepicker form-contacto-text smallInput"/>
                                    </div>
                                    <div class="form-administrar2" style="width:190px;"><label for="f_fecha_vigencia_d" style="width:120px;">Fecha de Vig. desde:</label>
                                        <input type="text" name="f_fecha_vigencia_d" id="f_fecha_vigencia_d" style="width:60px; float:right;" value="" class="datepicker form-contacto-text smallInput"/>
                                    </div>
                                    <div class="form-administrar2" style="width:190px;"><label for="f_fecha_vigencia_h" style="width:120px;">Fecha de Vig. hasta:</label>
                                        <input type="text" name="f_fecha_vigencia_h" id="f_fecha_vigencia_h"  style="width:60px; float:right;" value="" class="datepicker form-contacto-text smallInput"/>
                                    </div>

                                </div>

                                <div style="width:190px; margin-right:15px; display:block; float:left;">
                                    <div class="form-administrar2" style="width:190px;"><label for="f_fecha_aprobacion_d" style="width:120px;">Fecha de Aprob. desde:</label>
                                        <input type="text" name="f_fecha_aprobacion_d" id="f_fecha_aprobacion_d" style="width:60px; float:right;" value="" class="datepicker form-contacto-text smallInput"/>
                                    </div>
                                    <div class="form-administrar2" style="width:190px;"><label for="f_fecha_aprobacion_h" style="width:120px;">Fecha de Aprob. hasta:</label>
                                        <input type="text" name="f_fecha_aprobacion_h" id="f_fecha_aprobacion_h"  style="width:60px; float:right;" value="" class="datepicker form-contacto-text smallInput"/>
                                    </div>
                                    <div class="form-administrar2" style="width:190px;"><label for="f_fecha_recibido_d" style="width:120px;">Fecha de Recep. desde:</label>
                                        <input type="text" name="f_fecha_recibido_d" id="f_fecha_recibido_d" style="width:60px; float:right;" value="" class="datepicker form-contacto-text smallInput"/>
                                    </div>
                                    <div class="form-administrar2" style="width:190px;"><label for="f_fecha_recibido_h" style="width:120px;">Fecha de Recep. hasta:</label>
                                        <input type="text" name="f_fecha_recibido_h" id="f_fecha_recibido_h"  style="width:60px; float:right;" value="" class="datepicker form-contacto-text smallInput"/>
                                    </div>
                                    <div class="form-administrar2" style="width:190px;"><label for="o_order" style="width:100px;">Ordenar por:</label>
                                        <select name="o_order" id="o_order"  class="smallInput" title="Ordenar por" style="width:90px; float:left; margin-bottom: 5px ;" >
                                            <option value="0">Campo</option>
                                        </select>
                                    </div>
                                    <div class="form-administrar2" style="width:190px;"><label for="o_progresion" style="width:100px;">Orden:</label>
                                        <select name="o_progresion" id="o_progresion"  class="smallInput" title="Progresion" style="width:90px; float:left; margin-bottom: 5px ;" >
                                            <option value="0">Descendente</option>
                                            <option value="ASC">Ascendente</option>
                                        </select>
                                    </div>
                                </div>
                                <div style="width:190px; margin-right:15px; display:block; float:left;">
                                    <div class="form-administrar2" style="width:190px;"><label for="f_suma_asegurada_v" style="width:70px;">Suma Aseg:</label>
                                        <div style="width:120px; display:block; float:left;">
                                            <input type="text" name="f_suma_asegurada_v" id="f_suma_asegurada_v" style="width:60px; float:right;" value="" class="form-contacto-text smallInput"/>
                                            <input type="hidden" class="comparador" name="f_suma_asegurada_c" value="0" />
                                            <a href="javascript: void(0);" onClick="cambiarComparacion('f_suma_asegurada_c', this);" class="buttons" style="margin-right: 10px; font-size: 16px; float:right; text-decoration: none; width: 38px; height: 20px;" title="Mayor que"><span class="ui-icon ui-icon-plusthick"></span></a>
                                        </div>
                                    </div>
                                    <div class="form-administrar2" style="width:190px;"><label for="f_prima_inicial_v" style="width:70px;">Prima Inicial:</label>
                                        <div style="width:120px; display:block; float:left;">
                                            <input type="text" name="f_prima_inicial_v" id="f_prima_inicial_v" style="width:60px; float:right;" value="" class="form-contacto-text smallInput"/>
                                            <input type="hidden" class="comparador" name="f_prima_inicial_c" value="0" />
                                            <a href="javascript: void(0);" onClick="cambiarComparacion('f_prima_inicial_c', this);" class="buttons" style="margin-right: 10px; font-size: 16px; float:right; text-decoration: none; width: 38px; height: 20px;" title="Mayor que"><span class="ui-icon ui-icon-plusthick"></span></a>
                                        </div>
                                    </div>
                                    <div class="form-administrar2" style="width:190px;"><label for="f_prima_planeada_v" style="width:70px;">Prima Anual:</label>
                                        <div style="width:120px; display:block; float:left;">
                                            <input type="text" name="f_prima_planeada_v" id="f_prima_planeada_v" style="width:60px; float:right;" value="" class="form-contacto-text smallInput"/>
                                            <input type="hidden" class="comparador" name="f_prima_planeada_c" value="0" />
                                            <a href="javascript: void(0);" onClick="cambiarComparacion('f_prima_planeada_c', this);" class="buttons" style="margin-right: 10px; font-size: 16px; float:right; text-decoration: none; width: 38px; height: 20px;" title="Mayor que"><span class="ui-icon ui-icon-plusthick"></span></a>
                                        </div>
                                    </div>
                                    <div class="form-administrar2" style="width:190px;"><label for="f_prima_target_v" style="width:70px;">Prima Target:</label>
                                        <div style="width:120px; display:block; float:left;">
                                            <input type="text" name="f_prima_target_v" id="f_prima_target_v" style="width:60px; float:right;" value="" class="form-contacto-text smallInput"/>
                                            <input type="hidden" class="comparador" name="f_prima_target_c" value="0" />
                                            <a href="javascript: void(0);" onClick="cambiarComparacion('f_prima_target_c', this);" class="buttons" style="margin-right: 10px; font-size: 16px; float:right; text-decoration: none; width: 38px; height: 20px;" title="Mayor que"><span class="ui-icon ui-icon-plusthick"></span></a>
                                        </div>
                                    </div>
                                    <div class="form-administrar2" style="width:190px;"><label for="f_prima_adicional_v" style="width:70px;">Prima Adic.:</label>
                                        <div style="width:120px; display:block; float:left;">
                                            <input type="text" name="f_prima_adicional_v" id="f_prima_adicional_v" style="width:60px; float:right;" value="" class="form-contacto-text smallInput"/>
                                            <input type="hidden" class="comparador" name="f_prima_adicional_c" value="0" />
                                            <a href="javascript: void(0);" onClick="cambiarComparacion('f_prima_adicional_c', this);" class="buttons" style="margin-right: 10px; font-size: 16px; float:right; text-decoration: none; width: 38px; height: 20px;" title="Mayor que"><span class="ui-icon ui-icon-plusthick"></span></a>
                                        </div>
                                    </div>
                                    <div class="form-administrar2" style="width:190px;"><label for="f_prima_unica_v" style="width:70px;">Prima &Uacute;nica:</label>
                                        <div style="width:120px; display:block; float:left;">
                                            <input type="text" name="f_prima_unica_v" id="f_prima_unica_v" style="width:60px; float:right;" value="" class="form-contacto-text smallInput"/>
                                            <input type="hidden" name="f_prima_unica_c" class="comparador" value="0" />
                                            <a href="javascript: void(0);" onClick="cambiarComparacion('f_prima_unica_c', this);" class="buttons" style="margin-right: 10px; font-size: 16px; float:right; text-decoration: none; width: 38px; height: 20px;" title="Mayor que"><span class="ui-icon ui-icon-plusthick"></span></a>
                                        </div>
                                    </div>
                                </div>
                                <div style="width:90px; margin-right:15px; display:block; float:left;">
                                    <select name="f_pais"  class="smallInput" title="País" style="width:90px; float:left; margin-bottom: 5px ; " >
                                        <?php
                                        $sSQL = "SELECT id,pais FROM paises ORDER BY pais ASC ";
                                        echo GenerarOptions($sSQL, 0, true, "Pais");
                                        ?>
                                    </select>
                                    <select name="f_fumador"  class="smallInput" title="Fumador" style="width:90px; float:left; margin-bottom: 5px ;" >
                                        <option value="0">Fumador</option>
                                        <option value="2">Si</option>
                                        <option value="1">No</option>
                                    </select>
                                    <select name="f_id_compania"  class="smallInput" title="Compañía" style="width:90px; float:left; margin-bottom: 5px ;" >
                                        <?php
                                        $sSQL = "SELECT idcompania, com_nombre FROM companias ORDER BY com_nombre ";
                                        echo GenerarOptions($sSQL, 0, true, "Compa&ntilde;&iacute;a");
                                        ?>
                                    </select>
                                    <select name="f_id_producto"  class="smallInput" title="Producto" style="width:90px; float:left; margin-bottom: 5px ;" >
                                        <?php
                                        $sSQL = "SELECT id_producto, nombre_producto FROM productos ORDER BY nombre_producto ";
                                        echo GenerarOptions($sSQL, 0, true, "Producto");
                                        ?>
                                    </select>
                                    <select name="f_id_agente"  class="smallInput" title="Agente" style="width:90px; float:left; margin-bottom: 5px ; margin-right: 10px ;" >
                                        <?php
                                        $sSQL = "SELECT DISTINCT id_agente, agente FROM estructura_comercial where agente != '' GROUP BY agente ";
                                        echo GenerarOptions($sSQL, 0, true, "Agente");
                                        ?>
                                    </select>
                                    <select name="f_gerente"  class="smallInput" title="Gerente" style="width:90px; float:left; margin-bottom: 5px ;" >
                                        <option value="0">Gerente</option>
                                        <?php
                                        $sSQL = "SELECT DISTINCT gerente, gerente FROM estructura_comercial WHERE gerente != '' ORDER BY gerente";
                                        echo GenerarOptions($sSQL, null);
                                        ?>
                                    </select>
                                </div>
                                <div style="width:160px; display:block; float:left;">
                                    <select name="f_frecuencia_pago"  class="smallInput" title="Frecuencia" style="width:90px; float:left; margin-bottom: 5px;" >
                                        <?php
                                        $sSQL = "SELECT id_frecuencia, frecuencia FROM frecuencia_pago ORDER BY frecuencia ";
                                        echo GenerarOptions($sSQL, 0, true, "Frecuencia");
                                        ?>
                                    </select>
                                    <select name="f_forma_pago"  class="smallInput" title="Forma de Pago" style="width:90px; float:left; margin-bottom: 5px;" >
                                        <?php
                                        $sSQL = "SELECT id, forma FROM forma_pago ORDER BY forma ";
                                        echo GenerarOptions($sSQL, 0, true, "Forma de Pago");
                                        ?>
                                    </select>
                                    <select name="f_estado"  class="smallInput" title="Estado" style="width:90px; float:left; margin-bottom: 5px;" >
                                        <?php
                                        $sSQL = "SELECT id, estado FROM solicitud_estado ORDER BY estado ";
                                        echo GenerarOptions($sSQL, 0, true, "Estado");
                                        ?>
                                    </select>
                                    <select name="f_id_plan"  class="smallInput" title="Plan" style="width:90px; float:left; margin-bottom: 5px ;" >
                                        <?php
                                        $sSQL = "SELECT id_plan, codigo_plan FROM planes ORDER BY nombre_plan ";
                                        echo GenerarOptions($sSQL, 0, true, "Plan");
                                        ?>
                                    </select>
                                    <select name="f_director"  class="smallInput" title="Director" style="width:90px; float:left; margin-bottom: 5px ;margin-right: 10px ;" >
                                        <option value="0">Director</option>
                                        <?php
                                        $sSQL = "SELECT DISTINCT director, director FROM estructura_comercial WHERE director != ''  ORDER BY director";
                                        echo GenerarOptions($sSQL, null);
                                        ?>
                                    </select>
                                    <select name="o_group" id="o_group"  class="smallInput" title="Agrupar Por" style="width:90px; float:left;margin-bottom: 5px ;" >
                                        <option value="0">Agrupar por</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <?php
                        $computables = array(0 => 'Campo', 1 => 'Suma Asegurada', 2 => 'Prima Inicial', 3 => 'Prima Anual', 4 => 'Prima Target', 5 => 'Prima Adic.', 6 => 'Prima &Uacute;nica');
                        $funciones = array(0 => 'Funci&oacute;n', 1 => 'Cuenta', 2 => 'Suma', 3 => 'Promedio', 4 => 'Mayor', 5 => 'Menor');
                        ?>
                        <div style="width:900px; display:block; float:left; clear: both; padding-left: 15px; border-bottom: 1px solid #DDDDDD;border-top: 1px solid #DDDDDD;margin-bottom: 5px;" >
                            <div style="float: left; padding: 0pt 15px 0 0; margin: 5px 0pt; height: 24px;">
                                <span style="font-size: 13px; font-weight: bold;">Totales:</span>
                            </div>
                            <div style="border-right: 2px solid #CCCCCC; float: left; padding: 0pt 15px; margin: 5px 0pt;">
                                <select name="totales_1_func"  class="smallInput" style="width:80px; float:left; margin-right: 5px;" >
                                    <?php
                                    foreach ($funciones as $key => $funcion) {
                                        echo '<option value="' . $key . '" ' . (($_SESSION['c_dir_letra'] == $key) ? 'selected="selected"' : '') . '>' . $funcion . '</option>';
                                    }
                                    ?>
                                </select>
                                <select name="totales_1_cam"  class="smallInput" style="width:80px; float:left;" >
                                    <?php
                                    foreach ($computables as $key => $computable) {
                                        echo '<option value="' . $key . '" ' . (($_SESSION['c_dir_letra'] == $key) ? 'selected="selected"' : '') . '>' . $computable . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div style="border-right: 2px solid #CCCCCC; float: left; padding: 0pt 15px; margin: 5px 0pt;">
                                <select name="totales_2_func"  class="smallInput" style="width:80px; float:left; margin-right: 5px;" >
                                    <?php
                                    foreach ($funciones as $key => $funcion) {
                                        echo '<option value="' . $key . '" ' . (($_SESSION['c_dir_letra'] == $key) ? 'selected="selected"' : '') . '>' . $funcion . '</option>';
                                    }
                                    ?>
                                </select>
                                <select name="totales_2_cam"  class="smallInput" style="width:80px; float:left;" >
                                    <?php
                                    foreach ($computables as $key => $computable) {
                                        echo '<option value="' . $key . '" ' . (($_SESSION['c_dir_letra'] == $key) ? 'selected="selected"' : '') . '>' . $computable . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div style="border-right: 2px solid #CCCCCC; float: left; padding: 0pt 15px; margin: 5px 0pt;">
                                <select name="totales_3_func"  class="smallInput" style="width:80px; float:left; margin-right: 5px;" >
                                    <?php
                                    foreach ($funciones as $key => $funcion) {
                                        echo '<option value="' . $key . '" ' . (($_SESSION['c_dir_letra'] == $key) ? 'selected="selected"' : '') . '>' . $funcion . '</option>';
                                    }
                                    ?>
                                </select>
                                <select name="totales_3_cam"  class="smallInput" style="width:80px; float:left;" >
                                    <?php
                                    foreach ($computables as $key => $computable) {
                                        echo '<option value="' . $key . '" ' . (($_SESSION['c_dir_letra'] == $key) ? 'selected="selected"' : '') . '>' . $computable . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div style="float: left; padding: 0pt 15px; margin: 5px 0pt;">
                                <select name="totales_4_func"  class="smallInput" style="width:80px; float:left; margin-right: 5px;" >
                                    <?php
                                    foreach ($funciones as $key => $funcion) {
                                        echo '<option value="' . $key . '" ' . (($_SESSION['c_dir_letra'] == $key) ? 'selected="selected"' : '') . '>' . $funcion . '</option>';
                                    }
                                    ?>
                                </select>
                                <select name="totales_4_cam"  class="smallInput" style="width:80px; float:left;" >
                                    <?php
                                    foreach ($computables as $key => $computable) {
                                        echo '<option value="' . $key . '" ' . (($_SESSION['c_dir_letra'] == $key) ? 'selected="selected"' : '') . '>' . $computable . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <input type="hidden" name="nombre_reporte" value="" />
                        <input type="hidden" name="id_reporte" value="0" />
                        <input type="hidden" name="cot_cotizar" value="0" />
                        <div id="serialized-report" style="display: none;"></div>
                        <div id="nombre-dialog" style="display: none;">
                            <label for="tmp_reporte" style="width:70px;">Nombre:</label>
                            <input type="text" name="tmp_reporte" id="tmp_reporte" style="width:200px;" value="" class="form-contacto-text smallInput"/>
                        </div>
                        <div id="nombrehist-dialog" style="display: none;">
                            <label for="tmp_hist" style="width:70px;">Nombre:</label>
                            <input type="text" name="tmp_hist" id="tmp_hist" style="width:200px;" value="" class="form-contacto-text smallInput"/>
                        </div>
                        <div id="reportes-dialog" style="display: none;"></div>
                        <div id="historiales-dialog" style="display: none;"></div>
                        <div id="produccion-dialog" style="display: none;"></div>
                    </form>

                    <script type="text/javascript">
                        if ($.browser.webkit) {
                            $('select.smallInput').css('padding', '1px');
                        }

                        Date.prototype.toString = function () {return isNaN (this) ? 'NaN' : [this.getDate() > 9 ? this.getDate() : '0' + this.getDate(),  this.getMonth() > 8 ? this.getMonth() + 1 : '0' + (this.getMonth() + 1), this.getFullYear()].join('/')}
                        var d = new Date().toString();
                        $(".datepicker").datepicker({
                            changeMonth: true,
                            changeYear: true,
                            dateFormat: "dd/mm/yy",
                            defaultDate: d,
                            regional: 'es',
                            minDate: new Date(1920, 0, 1),
                            yearRange:'1920:c',
                            dayNames: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
                            dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
                            dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
                            monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
                            monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
                        });
                        $(document).ready(function(){
                            $( ".buttons-cargar" ).button( "option", "icons", {primary:'ui-icon-folder-open'} );
                            $( ".buttons-guardar" ).button( "option", "icons", {primary:'ui-icon-disk'} );
                            $( ".buttons-guardar-como" ).button( "option", "icons", {primary:'ui-icon-disk'} );
                            $( ".buttons-consultar" ).button( "option", "icons", {primary:'ui-icon-gear'} );
                            $( ".buttons-history" ).button( "option", "icons", {primary:'ui-icon-clock'} );
                            clear_form_elements('#formProduccion');
                            formatearFechas();
                            actualizarCheckeds();
                            $('.selectCheckbox').change(function(){
                                if($(this).attr('checked')){
                                    $('#o_order').append('<option value="'+$(this).attr('name')+'">'+$('label[for="'+$(this).attr('id')+'"]').html()+'</option>');
                                    if($('#o_order').find('option[value="0"]').length > 0)
                                        $('#o_order').find('option[value="0"]').remove();
                                    $('#o_group').append('<option value="'+$(this).attr('name')+'">'+$('label[for="'+$(this).attr('id')+'"]').html()+'</option>');
                                }
                                else{
                                    $('#o_order').find('option[value="'+$(this).attr('name')+'"]').remove();
                                    if($('#o_order option').size() == 0)
                                        $('#o_order').append('<option value="0">Campo</option>');
                                    $('#o_group').find('option[value="'+$(this).attr('name')+'"]').remove();
                                }
                                ordenarSelect('#o_order');
                                ordenarSelect('#o_group');
                            });
                        });
                        function actualizarCheckeds(){
                            $('input.selectCheckbox:checked').each(function(){
                                if(!($('#o_order').find('option[value="'+$(this).attr('name')+'"]').length > 0))
                                    $('#o_order').append('<option value="'+$(this).attr('name')+'">'+$('label[for="'+$(this).attr('id')+'"]').html()+'</option>');
                                if($('#o_order').find('option[value="0"]').length > 0)
                                    $('#o_order').find('option[value="0"]').remove();
                                if(!($('#o_group').find('option[value="'+$(this).attr('name')+'"]').length > 0))
                                    $('#o_group').append('<option value="'+$(this).attr('name')+'">'+$('label[for="'+$(this).attr('id')+'"]').html()+'</option>');
                            });
                            ordenarSelect('#o_order');
                            ordenarSelect('#o_group');
			}



                        function exportarReporte(src){
                            if(src == null){
                                src = 'produccion';
                            }
                            else{
                                src = 'history&src='+src;
                            }
                            $('#formProduccion').attr('action', 'exportar_reporte.php?objeto='+src+'&tipo=xls');
                            $('#formProduccion').attr('target', '_blank');
                            $('#formProduccion').submit();
                            $('#formProduccion').attr('action', '#');
                            $('#formProduccion').removeAttr('target');
                        }

			function exportarReportePDF(src) {

			   if (src == null) src = "produccion";
			   else src = "history&src=" + src;

			   $("#formProduccion").attr("action", "exportar_reporte.php?objeto=" + src + "&tipo=pdf");
			   $("#formProduccion").attr("target", "_blank");
			   $("#formProduccion").submit();
			   $("#formProduccion").attr("action", "#");
			   $("#formProduccion").removeAttr("target");
			}



                        function ordenarSelect(id){
                            var ops = $(id +" option");
                            var slected = $(id+' option:selected').val();
                            ops.sort(function (a,b) {
                                return ( $(a).text().toUpperCase() > $(b).text().toUpperCase())
                            });
                            var html="";
                            for(i=0;i<ops.length;i++)
                            {
                                html += "<option value='" + $(ops[i]).val() + "'>" + $(ops[i]).text() + "</option>";
                            }
                            $(id).html(html);
                            if($(id).find('option[value="'+slected+'"]').length > 0)
                                $(id).find('option[value="'+slected+'"]').attr('selected', 'selected');
                            else
                                $(id).val(0);
                        }
                        $("#nombre-dialog").dialog({
                            modal: true,
                            autoOpen: false,
                            width: 370,
                            height: 'auto',
                            title: '<span class="ui-icon ui-icon-disk"></span> Guardar',
                            resizable: false,
                            resizeStop: function(event, ui) {
                                $('#nombre-dialog').dialog('option', 'position', 'center');
                            },
                            buttons: {
                                "Guardar": function() {
                                    $('[name="nombre_reporte"]').val($('#tmp_reporte').val());
                                    $('[name="id_reporte"]').val(0);
                                    guardarReporte();
                                    $( this ).dialog( "close" );
                                },
                                "Cancelar": function() {
                                    $( this ).dialog( "close" );
                                }
                            }
                        });
                        $("#nombrehist-dialog").dialog({
                            modal: true,
                            autoOpen: false,
                            width: 370,
                            height: 'auto',
                            title: '<span class="ui-icon ui-icon-clock"></span> Guardar',
                            resizable: false,
                            resizeStop: function(event, ui) {
                                $('#nombre-dialog').dialog('option', 'position', 'center');
                            },
                            buttons: {
                                "Guardar": function() {
                                    var nom = $('[name="nombre_reporte"]').val();
                                    $('[name="nombre_reporte"]').val($('#tmp_hist').val());
                                    guardarReporte(1111111);
                                    $('[name="nombre_reporte"]').val(nom);
                                    $( this ).dialog( "close" );
                                },
                                "Cancelar": function() {
                                    $( this ).dialog( "close" );
                                }
                            }
                        });
                        $("#reportes-dialog").dialog({
                            modal: true,
                            autoOpen: false,
                            width: 'auto',
                            maxWidth: 320,
                            height: 'auto',
                            resizable: false,
                            title: '<span class="ui-icon ui-icon-folder-open"></span> Cargar',
                            resizeStop: function(event, ui) {
                                $('#reportes-dialog').dialog('option', 'position', 'center');
                            },
                            open: function(){
                                actualizarListadoReportes();
                            }
                        });
                        $("#historiales-dialog").dialog({
                            modal: true,
                            autoOpen: false,
                            width: 'auto',
                            maxWidth: 320,
                            height: 'auto',
                            resizable: false,
                            title: '<span class="ui-icon ui-icon-clock"></span> Cargar Historial',
                            resizeStop: function(event, ui) {
                                $('#historiales-dialog').dialog('option', 'position', 'center');
                            },
                            open: function(){
                                actualizarListadoHistoriales();
                            }
                        });
                        $("#produccion-dialog").dialog({
                            modal: true,
                            autoOpen: false,
                            width: 980,
                            //maxWidth: 800,
                            height: 600,
                            resizable: false,
                            title: '<span class="ui-icon ui-icon-folder-open"></span> Reporte',
                            resizeStop: function(event, ui) {
                                $('#produccion-dialog').dialog('option', 'position', 'center');
                            }
                            //                            open: function(){
                            //                                generarReporteProduccion();
                            //                            }
                        });
                    </script>
                    <div class="clear"></div>
                </div>
