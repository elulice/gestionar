 
 
  <form action="" method="post" target="hidden_iframe" id="frm_cv_datos_personales">
   <div class="ddu_title">Datos Personales</div>
    <div class="dlg_column_left"> 
      <div class="ddu_campo" >
	 <span>Apellido:</span>
	 <input type="text" class="smallInput" name="PerApellido" />
      </div>
      <div class="ddu_campo" >
	 <span>Nombre:</span>
	 <input type="text" class="smallInput" name="PerNombres" />
      </div>
      <div class="ddu_campo_doble">
	 <div class="ddu_campo_col_izq">
	    <div class="ddu_campo_select">
	       <span>Documento:</span>
	       <select class="smallInput" name="PerTipoDoc" style="width:100px;">
	       <?php
   $query = "SELECT TipoDocNro, TipoDocNombre FROM tipodocumento ORDER BY TipoDocNombre";
   echo GenerarOptions($query, NULL);
	       ?>
	       </select>
	    </div>
	 </div>
	 <div class="ddu_campo_col_der">
	    <div class="ddu_campo">
	       <input type="text" class="smallInput" name="PerDocumento" style="width:130px; float:right;margin-right:2px;" />
	    </div>
	 </div>
      </div> 
      <div class="ddu_campo">
	 <span>Fecha Nac:</span>
	 <input type="text" class="smallInput" name="PerFechaNac" id="PerFechaNac" >
      </div>
      <div class="ddu_campo_select">
	 <span>Nacionalidad:</span>
	 <select class="smallInput" name="PerNacionalidad">
	 <?php
   $query = "SELECT nombre, nombre FROM _nacionalidades ORDER BY orden, idnacionalidad";
   echo GenerarOptions($query, NULL);
	 ?>
	 </select>
      </div>
      <div class="ddu_campo_select">
	 <span>Estado Civil:</span>
         <select class="smallInput" name="PerECivil">
	 <?php
   $query = "SELECT nombre, nombre FROM _estadocivil ORDER BY orden";
   echo GenerarOptions($query, NULL);
	 ?>
	 </select>
      </div>
      <div class="ddu_campo" style="margin:16px 0 16px 0;">
	 <span>Per. a Cargo:</span>
	 <input type="radio" style="width: 10px; float: left;" value="1" name="PerAcargo"><span style="float: left; width: 10px;">Si</span>
	 <input type="radio" style="float: left; width: 13px; margin-left: 10px;" checked="checked" value="0" name="PerAcargo">
	 <span style="width: 15px; margin-left: 5px;">No</span>
      </div>
      <div class="ddu_campo_select">
	 <span>País:</span>
	 <select class="smallInput" name="PerPais">
	 <?php
   $query = "SELECT PaiNom, PaiNom FROM pais";
   echo GenerarOptions($query, NULL);
	 ?>
	 </select>
      </div>
      <div class="ddu_campo">
	 <span>Localidad:</span>
	 <input type="text" name="PerLocalidad" class="smallInput" />
      </div>
      <div class="ddu_campo">
	 <span>Barrio:</span>
	 <input type="text" class="smallInput" name="PerBarrio" />
      </div>
   </div>
   <div class="dlg_column_right"> 
      <div class="ddu_campo_doble">
	 <div class="ddu_campo_col_izq">
	    <div class="ddu_campo_select">
	       <span>C.U.I.T./C.U.I.L:</span>
	       <select class="smallInput" name="PerCuitCuil" style="width:100px;">
		  <option value="CUIT" selected="yes" >C.U.I.T.</option>
		  <option value="CUIT">C.U.I.L.</option>
	       </select>
	    </div>
	 </div>
	 <div class="ddu_campo_col_der">
	    <div class="ddu_campo">
	       <input type="text" class="smallInput" name="PerNumeroCC" style="width:130px; float:right;margin-right:2px;" />
	    </div>
	 </div>
      </div> 
      <div class="ddu_campo_select">
	 <span>Sexo:</span>
	 <select class="smallInput" name="PerSexo">
	 <?php
   $query = "SELECT idsexo, nombre FROM _sexos WHERE idsexo > 0 ORDER BY idsexo";
   echo GenerarOptions($query, NULL);
	 ?>
	 </select>
      </div>
      <div class="ddu_campo">
	 <span>Cant. de hijos:</span>
	  <input type="text" class="smallInput" name="PerHijos">
      </div>
      <div class="ddu_campo">
	 <span>Codigo Postal:</span>
	 <input type="text" class="smallInput" name="PerCodPos">
      </div>
      <div class="ddu_campo_select">
	 <span>Provincia:</span>
	 <select class="smallInput" name="PerProvincia">
	 <?php
   $query = "SELECT PrvNom, PrvNom FROM provincia ORDER BY PrvNom";
   echo GenerarOptions($query, NULL, TRUE, "[Otra]");
	 ?>
	 </select>
      </div>
      <div class="ddu_campo_select">
	 <span>Zona:</span>
	 <select class="smallInput" name="ZonNro">
	 <?php
   $query = "SELECT ZonNro, ZonDescrip FROM zona ORDER BY ZonDescrip";
   echo GenerarOptions($query, NULL);
	 ?>
	 </select>
      </div>
      <div class="ddu_campo">
	 <span>Domicilio:</span>
	 <input type="text" class="smallInput" name="PerDomicilio" />
      </div>
      <div class="ddu_campo">
	 <span>Teléfono:</span>
	 <input type="text" class="smallInput" name="PerTelefono" />
      </div>
      <div class="ddu_campo">
	 <span>Teléfono Msjs:</span>
	 <input type="text" class="smallInput" name="PerTelMensaje" />
      </div>
      <div class="ddu_campo">
	 <span>Teléfono Celular:</span>
	 <input type="text" class="smallInput" name="PerCelular" />
      </div>
      <div class="ddu_campo">
	 <span>E-mail:</span>
	 <input type="text" class="smallInput" name="PerEmail" />
      </div>
   </div>
   </form>
