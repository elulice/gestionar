<?php
require_once("includes/funciones.php");
require_once("clases/framework-1.0/class.bd.php");
require_once("clases/phppaging/PHPPaging.lib.php");

switch ($_GET["objeto"]) {

    case "principal" : agenda_principal();
        break;
    case "detalle" : agenda_detalle();
        break;
}

function agenda_principal() {

    $where = "";

    $fecha_desde = get_fecha($_GET["fecha_desde"]);
    $fecha_hasta = get_fecha($_GET["fecha_hasta"]);

    if ($fecha_desde != FALSE AND $fecha_hasta != FALSE)
        $where = " AND po.PosFecha BETWEEN '$fecha_desde' AND '$fecha_hasta' ";
    else if ($fecha_desde != FALSE)
        $where.= " AND po.PosFecha >= '$fecha_desde' ";
    else if ($fecha_hasta != FALSE)
        $where.= " AND po.PosFecha <= '$fecha_hasta' ";

    $id_cliente = (int) $_GET["CliNro"];
    if ($id_cliente > 0)
        $where .= " AND c.CliNro = $id_cliente ";

    $estado_post = (int) $_GET["estadopost"];
    if ($estado_post > 0)
        $where .= " AND pe.PEsNro = $estado_post ";

    $estado = $_GET["estado"];
    if (!empty($estado))
        $where .= " AND o.EOfeValor = '$estado' ";

    $selector = (int) $_GET["MEmpNro"];
    if ($selector > 0)
        $where .= " AND o.MEmpNro = $selector ";

    $query = "SELECT
                    po.PosFecha
                    , p.PerApellido
                    , p.PerNombres
                    , p.PerNumeroCC
                    , pu.PueNom
                    , c.CliRsocial
                    , pe.PEsDescrip
                    , pn.NotNro
                    , p.PerNro
                FROM
                    postulacion po
                    LEFT JOIN persona p ON po.PerNro = p.PerNro
                    LEFT JOIN oferta o ON po.OfeNro = o.OfeNro
                    LEFT JOIN puesto pu ON o.PueNro = pu.PueNro
                    LEFT JOIN ofertascliente oc ON o.OfeNro = oc.OfeNro
                    LEFT JOIN cliente c ON oc.CliNro = c.CliNro
                    LEFT JOIN postulacionestado pe ON po.PosEstado = pe.PEsNro
                    LEFT JOIN postulantenotas pn ON p.PerNro = pn.PerNro
                WHERE
                    1 $where ORDER BY po.PosFecha DESC";

    //echo $query;
    ?>
    <div>
        <table width="833" cellpadding="0" cellspacing="0" style="margin:12px 0 0 12px;" id="box-table-a">
            <thead>
                <tr>
                    <th width="100" scope="col"><span style="color:#c60;font-weight:bold;">Apellido y Nombre</th>
                    <th width="60" scope="col"><span style="color:#c60;font-weight:bold;">C.U.I.L.</th>
                    <th width="100" scope="col"><span style="color:#c60;font-weight:bold;">Puesto</th>
                    <th width="100" scope="col"><span style="color:#c60;font-weight:bold;">Usuaria</th>
                    <th width="60" scope="col"><span style="color:#c60;font-weight:bold;">Situación</th>
                    <th width="30" scope="col"><span style="color:#c60;font-weight:bold;">Fecha</th>
                    <th width="20" scope="col"><span style="color:#c60;font-weight:bold;">Hora</th>
                    <th width="30" scope="col"><span style="color:#c60;font-weight:bold;">Opciones</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $db = new BD();
                $db->Conectar();
                $paging = new PHPPaging($db->RetornarConexion());
                $paging->agregarConsulta($query);
                $paging->linkClase("navPage");
                $paging->porPagina(5);
                $paging->ejecutar();

                while ($row = $paging->fetchResultado($rset)) {
                    ?>
                    <tr>
                        <td style="padding:8px;"><?php echo "{$row["PerApellido"]}, {$row["PerNombres"]}"; ?></td>
                        <td style="padding:8px;"><?php echo $row["PerNumeroCC"]; ?></td>
                        <td style="padding:8px;"><?php echo $row["PueNom"]; ?></td>
                        <td style="padding:8px;"><?php echo $row["CliRsocial"]; ?></td>
                        <td style="padding:8px;"><?php echo $row["PEsDescrip"]; ?></td>
                        <td style="padding:8px;"><?php echo date("d/m/Y", strtotime($row["PosFecha"])); ?></td>
                        <td style="padding:8px;"><?php echo date("H:i", strtotime($row["PosFecha"])); ?></td>
                        <td style="padding:8px;">
                            <img src="images/icons/zoom_in.png" alt="Ver Detalles" title="Ver Detalles" onclick="editar_curriculum(<?php echo $row["PerNro"]; ?>);" style="cursor:pointer;" />
                        </td>
                    </tr>
        <?php
    }
    ?>
            </tbody>
        </table>
    </div>
    <div class="pagination"><?php echo $paging->fetchNavegacion(); ?></div>
    <?php
}

function agenda_detalle() {

    $id_registro = (int) $_GET["id_postulante"];
    if ($id_registro <= 0)
        exit("");

    $query = "SELECT p.PerApellido, p.PerNombres, pn.* FROM persona p
      LEFT JOIN postulantenotas pn ON p.PerNro = pn.PerNro
      WHERE pn.NotNro = $id_registro";

    $db = new BD();
    $db->Conectar();
    $row = $db->Seleccionar($query, TRUE);

    echo json_encode($row);
}
?>
