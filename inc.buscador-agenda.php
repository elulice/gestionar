<?php
	$bEstadoPost = (($_GET["PEsNro"]) ? $_GET["PEsNro"] : 0);
	$bEstadoOf = (($_GET["estado"]) ? $_GET["estado"] : "");

	$bFechaDesde = ($_GET["fecha_desde"] ? $_GET["fecha_desde"] : "");
	$bFechaHasta = ($_GET["fecha_hasta"] ? $_GET["fecha_hasta"] : date("d-m-Y"));

	$bSelector = (is_numeric($_GET["MEmpNro"]) ? $_GET["MEmpNro"] : 0);
	$bUnidad = (is_numeric($_GET["UniNro"]) ? $_GET["UniNro"] : 0);

	$bCliente = ($_GET["cliente"] ? $_GET["cliente"] : "");
	$bIdCliente = (is_numeric($_GET["CliNro"]) ? $_GET["CliNro"] : 0);

?>


<form action="" method="get" name="frmFiltro" id="frmFiltro">
<table border="0" align="center" cellpadding="0" cellspacing="0" style="margin-bottom:10px;" class="buscar">
  <!--<tr align="left">
    <td class="encabezado-titulo-texto">Empresa:</td>
    <td colspan="3">
<select name="tiporeporte" id="tiporeporte" class="formulario-textbox" onchange="slectAction(this)" style="width: 300px;">
<option value="1">Reporte web</option>
<option value="2">Reporte General Detallado</option>
<!--<option value="3">Reporte Detallado Simple</option>
<option value="4">Reporte General Resumido</option>
<option value="5">Resumen Por Selector</option>
</select>	
	</td>
    </tr>-->
    <tr align="left">
      <td class="encabezado-formulario">Estado del postulado:</td>
      <td><select name="estadopost" class="formulario-textbox" id="estadopost" style="width: 180px;">
          <?php
					$sSQL = "SELECT PEsNro, PEsDescrip FROM postulacionestado  ";
					print(GenerarOptions($sSQL, $bEstadoPost, true, DEFSELECT));
			  ?>
      </select></td>
      <td class="encabezado-formulario">Situaci&oacute;n de la oferta: </td>
      <td><select name="estado" class="formulario-textbox" id="estado" style="width: 180px;">
          <?php
					$sSQL = "SELECT nombre, nombre FROM _ofertas_estados  ";
					print(GenerarOptions($sSQL, $bEstadoPost, true, DEFSELECT));
			  ?>
      </select></td>
    </tr>
<tr align="left">
	<td class="encabezado-formulario">Fecha desde:</td>
	<td><input name="fecha_desde" type="text" class="formulario-textbox" id="fecha_desde" style="width: 140px;" value="<?php print $bFechaDesde; ?>" /></td>
	<td class="encabezado-formulario">Fecha hasta:</td>
	<td><input name="fecha_hasta" type="text" class="formulario-textbox" id="fecha_hasta" style="width: 140px;" value="<?php print $bFechaHasta; ?>" /></td>
  </tr>
<tr align="left">
	<td class="encabezado-formulario">Selector:</td>
	<td><select name="MEmpNro" class="formulario-textbox" id="MEmpNro" style="width: 180px;">
      <?php
					$sSQL = "SELECT MEmpNro, CONCAT(MEmpApellido, \" \", MEmpNombres) FROM miembroempresa  ";
					//$sSQL .= "WHERE idtipo = 2 ";
					$sSQL .= "ORDER BY MEmpApellido ASC, MEmpNombres ASC ";
					print(GenerarOptions($sSQL, $bSelector, true, DEFSELECT));
			  ?>
    </select></td>
	<td class="encabezado-formulario">Sucursal:</td>
	<td><select name="UniNro" class="formulario-textbox" id="UniNro" style="width: 180px;">
      <?php
					$sSQL = "SELECT UniNro, UniNombre FROM unidadorg  ";
					print(GenerarOptions($sSQL, $bUnidad, true, DEFSELECT));
			  ?>
    </select></td>
  </tr>
<tr align="left">
	<td class="encabezado-formulario">Cliente:</td>
	<td><input name="cliente" type="text" class="formulario-textbox" id="cliente" style="width: 180px;" value="<?php print $bCliente; ?>" /></td>
	<td colspan="2" class="encabezado-formulario"><select name="CliNro" class="formulario-textbox" id="CliNro" style="width: 342px;">
	    <?php
					$sSQL = "SELECT CliNro, CliRSocial FROM cliente  ";
					$sSQL .= "ORDER BY CliRSocial ASC ";
					print(GenerarOptions($sSQL, $bIdCliente, true, DEFSELECT));
			  ?>
      </select></td>
  </tr>
<tr align="left">
	<td colspan="3" class="encabezado-formulario">&nbsp;</td>
	<td align="right" class="encabezado-formulario" style="padding-right:50px;">
      <input name="btnFiltrar" type="image" id="btnFiltrar" src="images/btn-buscar.jpg" alt="Filtrar" />    </td>
</tr>
  </table>
</form>
