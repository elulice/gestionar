<div id="dialog_cvs_asignaciones"> 
   <input type="hidden" id="dca_id_postulante" />
   <div style="width:740px;height:480px;">
      <a href="javascript:void();" style="position:fixed;">&nbsp;</a>
      <div>
	 <form method="get" action="feed.pedidos.php?objeto=principal&dialog=1" id="dca_ofertas_filtro">
	    <fieldset>
	       <legend>Filtro de Búsqueda:</legend>
	       <div class="fila">
		  <label>Desde:</label>
		  <input type="text" name="dp_fecha_desde" class="smallInput" style="float:left;" />
		  <label>Hasta:</label>
		  <input type="text" name="dp_fecha_hasta" class="smallInput" style="float:left;" />
		  <label>Selector:</label>
		  <select name="cbo_selector" class="smallInput" onchange="dca_buscar_ofertas();">
		  <?php
   $query = "SELECT MEmpNro, CONCAT(MEmpApellido, ' ', MEmpNombres) FROM miembroempresa WHERE MEmpAdmin=0
      ORDER BY MEmpApellido ASC, MEmpNombres ASC";
   echo GenerarOptions($query, NULL, TRUE, DEFSELECT);
		  ?>
		  </select>
	       </div>
	       <div class="fila">
		  <label>Sucursales:</label>
		  <select name="cbo_sucursales" class="smallInput" onchange="dca_buscar_ofertas();">
		  <?php
   $query = "SELECT UniNro, UniNombre FROM unidadorg";
   echo GenerarOptions($query, NULL, TRUE, DEFSELECT);
		  ?>
		  </select>
		  <label>Área:</label>
		  <select name="cbo_area" class="smallInput" onchange="dca_buscar_ofertas();">
		  <?php
   $query = "SELECT AreNro, AreNom FROM area ORDER BY AreNom";
   echo GenerarOptions($query, NULL, TRUE, DEFSELECT);
		  ?>
		  </select>
		  <label>Situación</label>
		  <select name="cbo_situacion" class="smallInput" onchange="dca_buscar_ofertas();">
		  <?php
   $query = "SELECT nombre, nombre FROM _ofertas_estados";
   echo GenerarOptions($query, NULL, TRUE, DEFSELECT);
		  ?>
		  </select>
	       </div>
	       <div class="fila">
		  <label>Ejecutivo:</label>
		  <select name="cbo_ejecutivo" class="smallInput" onchange="dca_buscar_ofertas();">
		  <?php
   $query = "SELECT MEmpNro, CONCAT(MEmpNombres, ' ', MEmpApellido) AS nombre
      FROM miembroempresa WHERE MEmpAdmin=2 ORDER BY nombre";
   echo GenerarOptions($query, NULL, TRUE, DEFSELECT);
		  ?>
		  </select>
		  <label>Usuaria:</label>
		  <input type="text" name="txt_usuaria" class="smallInput" />
		  <div class="fila_comandos">
		     <span onclick="dca_buscar_ofertas();">Buscar</span>
		     <span onclick="$('#dca_ofertas_filtro').clearForm();dca_buscar_ofertas();">Limpiar Búsqueda</span>
		  </div>
	       </div>
	    </fieldset>
	 </form>
      </div>
      <div id="dca_lista_ofertas" class="navPage"></div>
   </div>
</div>
