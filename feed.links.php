<?php
include("includes/funciones.php");
include("clases/framework-1.0/class.bd.php");
require_once ("clases/phppaging/PHPPaging.lib.php");
if ($_GET['objeto'] == "links") {
    ?>
    <table width="195" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet" style="font-size:9px;" >
        <tr>
            <?php
            $sSQL = "SELECT * FROM links ORDER BY titulo ASC";
            $cBD = new BD();
            $cBD->Conectar();

            $paging = new PHPPaging($cBD->RetornarConexion());
            $paging->porPagina(30);
            $paging->agregarConsulta($sSQL);
            $paging->ejecutar();
            $cont = 0;
            while ($aRegistro = $paging->fetchResultado()) {
                if ($cont == 2) {
                    $cont = 0;
                    echo "</tr><tr>";
                }
                $imagenes = glob("links/L1_" . $aRegistro['idlink'] . "-1.jpg");
                if (count($imagenes) == 1) {
                    $imagen = $imagenes[0];
                } else {
                    $imagen = "links/non_exists";
                }
                ?>
                <td style="padding-left:8px;" width="90"><a href="<?php echo $aRegistro["url"]; ?>" target="_blank" title="<?php echo $aRegistro["titulo"]; ?>" <?php echo (!file_exists($imagen)) ? 'class="buttons" style="height: 35px; vertical-align:bottom; width: 80px;"' : ''; ?>><?php echo (file_exists($imagen)) ? '<img src="' . $imagen . '" />' : substr($aRegistro["titulo"], 0, 20); ?></a></td>
                <?php
                $cont++;
            }
            if ($cont == 1)
                echo "<td>&nbsp;</td>";
            ?>
        </tr>
    </table>
    <script type="text/javascript">
        $(".buttons").button();
    </script>
    <?php
}
if ($_GET['objeto'] == "listar_links") {
    ?>
    <table width="280" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet" style="font-size:9px;" >
        <thead>
            <tr class="portlet-header">
                <th width="70" scope="col">Titulo</th>
                <th width="70" scope="col">URL</th>
                <th width="70" scope="col">Descripci&oacute;n</th>
                <th width="70" scope="col">Opciones</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $sSQL = "SELECT * FROM links ORDER BY titulo ASC";
            //print $sSQL;
            $cBD = new BD();
            $cBD->Conectar();

            $paging = new PHPPaging($cBD->RetornarConexion());
            $paging->porPagina(30);
            $paging->agregarConsulta($sSQL);
            $paging->ejecutar();
            $cont = 0;
            while ($aRegistro = $paging->fetchResultado()) {
                ?>
                <tr>
                    <td style="padding:8px;"><h4><?php echo $aRegistro["titulo"]; ?></h4></td>
                    <td style="padding:8px;"><h4><?php echo $aRegistro["url"] ?></h4></td>
                    <td style="padding:8px;"><h4><?php echo $aRegistro["descripcion"]; ?></h4></td>
                    <td style="padding:8px;"><a class="delete" href="abm.php?tabla=links&columna=idlink&idregistro=<?php echo $aRegistro["idlink"]; ?>&ajax=true&function=actualizarListadoLinks()&urlback=escritorio.php"><img src="images/icons/page_delete.png" /></a></td>
                </tr>
                <?php
            }
            ?>
        </tbody>
    </table>
<?php } ?>
