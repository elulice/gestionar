<?php
	session_start();
	include("clases/flxajax-0.2.2/flxajax.class.php4");
	include("clases/flxajax-0.2.2/flxajax_client.class.php4");
	include("clases/flxajax-0.2.2/flxajax_server.class.php4");
	include("clases/framework-1.0/class.bd.php");
	include("includes/funciones.php");
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?php print SITIO; ?></title>
<script language="JavaScript" type="text/javascript" src="scripts/ajax.php"></script>
<script language="JavaScript" type="text/javascript" src="scripts/general.js"></script>

<link href="estilos/general.css" rel="stylesheet" type="text/css">
</head>

<body>
<?php
	include("inc.encabezado.php");
	if ($m_lIDPostulante > 0) include("inc.submenu-curriculums.php");
	include("inc.listado-curriculums-experiencias.php");
	include("inc.pie.php");
?>
</body>
</html>
