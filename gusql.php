<?php

# te paso los pasos para conectarte por PHP via ODBC a nuestro SQL.
# 1 - conecto con el DSN "SQLGestion" con nuevo usuario y password. 
$connect = odbc_connect("SQLGestion", "gestiondbconsultas", "gconsultas");

# 2 - armo la tabla empresa para los campos numero y razonsocial
$query = "select
                            EA.EmpRSocial
                            , UOP.UniNombre AS UOP
                            , Empresa.EmpNro
                            , Empresa.EmpRSocial
                            , Empresa.EmpCUIT
                            , max(IsNull(Comunicacion.ComValor, '')) as  email
                        from
                            Empresa
                            inner join Organigrama
                            inner join Nodo
                            inner join UnidadOrg
                                on UnidadOrg.UniNro = Nodo.UniNro
                                on Nodo.OrgNro = Organigrama.OrgNro
                                on Organigrama.EmpNro = Empresa.EmpNro
                                    and Organigrama.OrgPrincipal = 1
                            inner join RelEntreEmpr
                            inner join Empresa EA
                                on EA.EmpNro = RelEntreEmpr.EmpNro
                                on RelEntreEmpr.TieneEmprNro = Empresa.EmpNro
                                    and RelEntreEmpr.RelTipo = 2
                            inner join Organigrama OP
                            inner join Nodo NP
                            inner join UnidadOrg UOP
                                on UOP.UniNro=NP.UniNro
                                on NP.OrgNro = OP.OrgNro
                                on OP.EmpNro = EA.EmpNro
                                    and OP.OrgPrincipal = 1
                            inner join RelEntreNodos
                                on RelEntreNodos.NodNro = Nodo.NodNro
                                    and RelEntreNodos.MiNodNro = NP.NodNro
                            left join Comunicacion
                            inner join ParametrosGenerales
                                on ParametrosGenerales.IdEmail = Comunicacion.MComNro
                                on comunicacion.EmpNro = Empresa.EmpNro
                        where
                            EA.EmpNro in (17, 271,398,4311,6247,6906,8148)  --IDs de Empresas Administradas
                            and REEFechFin = ''
                        group by
                            EA.EmpRSocial
                            , UOP.UniNombre
                            , Empresa.EmpNro
                            , Empresa.EmpRSocial
                            , Empresa.EmpCUIT
                            , Comunicacion.ComValor
                        order by
                            EA.EmpRSocial
                            , UOP.UniNombre
                            , Empresa.EmpRSocial";

# 3 - ejecuto la consulta
$result = odbc_exec($connect, $query);

#4 - Limpio la tabla de Clientes

$conectID2 = mysql_connect('localhost', 'root', 'arwebs2013');
mysql_select_db('gestion_ar');

$drop = mysql_query('TRUNCATE TABLE cliente');

# 5 - guardo la data en una estructura repetitiva y actualiza la base de datos.
while (odbc_fetch_row($result)) {
    // NO SE USA -> $razonSocial = odbc_result($result, 1);
    $empNombre = odbc_result($result, 2);
    $empNro = odbc_result($result, 3);
    $empRSocial = odbc_result($result, 4);

    echo $empRSocial . "<br/>";
    $empCuit = odbc_result($result, 5);
    $empEmail = odbc_result($result, 6);

    $aux = mysql_query("SELECT COUNT(*) AS nro FROM cliente WHERE EmpNro = " . $empNro, $conectID2);
    $existe = mysql_fetch_assoc($aux);
    if ($existe['nro'] == 0) {
        $consulta = "INSERT INTO
                        cliente
                            (CliNro
                            , CliRsocial
                            , CliEMailEjCta
                            , EmpNro
                            , CliCUIT
                            , CliUop
                            , CliClave)
                    VALUES
                        ('" . $empNro . "', '" . $empRSocial . "', '" . $empEmail . "', '" . $empNro . "', '" . $empCuit . "', '" . $empNombre . "', '')";
        mysql_query($consulta, $conectID2);
    }
}
# 6 - destruyo la conexion
odbc_close($connect);
?>
