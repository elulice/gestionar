<?php 
	$sSQL = "SELECT PerApellido, PerNombres ";
	$sSQL .= "FROM persona ";
	$sSQL .= "WHERE PerNro = " . $m_lIDPostulante;
	$cBD = new BD();
	$aPostulante = $cBD->Seleccionar($sSQL, true);

?>
<table width="780" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="30" class="encabezado-titulo-bg"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="20"><img src="images/espacio.gif" width="1" height="1" /></td>

        <td valign="top" class="encabezado-titulo-texto" style="padding-top:5px;">Usuarias a las que pueda postularse : 
		<?php print $aPostulante["PerApellido"]." ".$aPostulante["PerNombres"]; ?></td>
        </tr>
    </table></td>
  </tr>
  <tr>
    <td><img src="images/espacio.gif" width="1" height="20"></td>
  </tr>
</table>
<table width="780" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="20"><img src="images/listado-encabezado-inicio.jpg" width="20" height="37"></td>
        <td class="listado-encabezado-bg"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="100"><img src="images/espacio.gif" width="1" height="1"></td>
            <td width="450" class="listado-encabezado-texto">Usuaria</td>
            <td width="50">&nbsp;</td>
            <td align="center" class="listado-encabezado-texto">Quitar</td>
          </tr>
        </table></td>
        <td width="20"><img src="images/listado-encabezado-final.jpg" width="20" height="37"></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="5" class="listado-contenido-inicio"><img src="images/espacio.gif" width="1" height="1"></td>
        <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
		  <?php
		  		$lRegistros = 0;
			 	$sSQL = "SELECT pc.CliNro, c.CliRSocial ";
				$sSQL .= "FROM personacliente pc ";
				$sSQL .= "INNER JOIN cliente c ON c.CliNro = pc.CliNro ";
				$sSQL .= "WHERE pc.PerNro = ".$m_lIDPostulante." ";
				$sSQL .= "ORDER BY CliRSocial ";

				$cBD = new BD();
				$oResultado = $cBD->Seleccionar($sSQL);
				while($aRegistro = $cBD->RetornarFila($oResultado))
				{
					$sPosicion = (($sPosicion == "1") ? "2" : "1");
					
		  ?>
          <tr>
            <td class="listado-fila-bg-<?php print($sPosicion); ?>">
			
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="15"><img src="images/espacio.gif" width="1" height="1"></td>
                <td width="100">&nbsp;</td>
                <td width="450" class="listado-texto"><?php print($aRegistro["CliRSocial"]); ?></td>
                <td width="50" align="left" class="listado-texto">&nbsp;</td>
                <td align="center" class="listado-texto"><img src="images/chk-N-<?php print($sPosicion); ?>.jpg" width="23" height="23" border="0" onclick="eliminarCalificacionCliente(<?php print $m_lIDPostulante; ?>,<?php print $aRegistro["CliNro"]; ?>)" style="cursor:pointer;" /></td>
              </tr>
              
            </table></td>
          </tr>
          <?php
			 		$lRegistros++;
				}
				if($lRegistros == 0)
				{
			 ?>
          <tr>
            <td><img src="images/espacio.gif" width="1" height="20"></td>
          </tr>
			 <?php } ?>
        </table></td>
        <td width="6" class="listado-contenido-final"><img src="images/espacio.gif" width="1" height="1"></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="100"><a href="am-curriculums-clientes.php?idregistro=0&idpostulante=<?php print $m_lIDPostulante; ?>&url=<?php print($m_sURL); ?>"><img src="images/listado-pie-inicio.jpg" alt="Agregar" width="100" height="40" border="0"></a></td>
        <td class="listado-pie-bg">&nbsp;</td>
        <td width="20"><img src="images/listado-pie-final.jpg" width="20" height="40"></td>
      </tr>
    </table></td>
  </tr>
</table>
