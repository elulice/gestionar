<?php
session_start();
include("includes/funciones.php");
include("clases/framework-1.0/class.bd.php");

require_once ("clases/phppaging/PHPPaging.lib.php");
$seccion = preg_replace("/.*\/(.*)\.php/", "$1", $_SERVER['SCRIPT_FILENAME']);
$submenu = (isset($_GET['sub'])) ? $_GET['sub'] : "principal";
include("inc.encabezado.php");
?>
<body>
    <?php
    if ($submenu != "principal")
        include("inc.reclamos.$submenu.php");
    else {
        $_SESSION['from'] = "Reclamos Principal";
        ?>
        <div id="portlets">
            <!-- TITULO -->
            <div class="grid_9">
                <h1 class="content_edit">Reclamos</h1>
            </div>
            <div class="clear"></div>
            <!-- SUB-TITULO Y LINK LATERAL -->
            <div class="portlet2-header"> 
                <span style="display:block; width:100px; float:left;" >
                    <img src="images/glass.png" height="16" width="16" alt="Contactos"  />Buscar    </span>
                <span style=" float:right;display:block; width:120px; margin-right:10px;">     
                    <?php if (!($_SESSION["is_agente"] || $_SESSION["is_productor"])): ?>
                        <a href="reclamos.php?sub=administrar" class="edit_inline" style="margin-left:10px;">     Agregar Reclamo</a>
                    <?php endif; ?>
                </span>
                <div style="clear:both"></div>
            </div>
            <div class="grid_15">
                <div class="portlet-content">
                    <!-- FORM DE FILTRO -->
                    <div style="margin:8px 0 0 18px">
                        <form action="contactos.php" method="GET" name="formReclamosBuscarHome" id="formReclamosBuscarHome" onSubmit="actualizarReclamosHome(); return false;">
                            <input type="submit" style="display: none;"/>
                            <table width="796" cellspacing="3" style="margin-left:40px;">
                                <tr>
                                    <td width="250">
                                        <div style="display:block; float:left; width:220px;">
                                            <div class="form-label">N&uacute;m. Poliza:</div>
                                            <input type="text" name="nro_poliza" class="smallInput" style="width:130px; float:right;" value="<?php echo htmlspecialchars($_SESSION['rec_pr_nro_poliza']); ?>"/>
                                        </div>
                                    </td>
                                    <td width="250">
                                        <div style="display:block; float:left; width:220px;">
                                            <div class="form-label">Apellido:</div>
                                            <input type="text" name="apellido" class="smallInput" style="width:130px; float:right;" value="<?php echo htmlspecialchars($_SESSION['rec_pr_apellido']); ?>"/>
                                        </div>
                                    </td>
                                    <td width="250">
                                        <div style="display:block; float:left; width:220px;">
                                            <div class="form-label">Cod. Agente:</div>
                                            <input type="text" name="agente" class="smallInput" style="width:130px; float:right;" value="<?php echo htmlspecialchars($_SESSION['rec_pr_agente']); ?>"/>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="250">
                                        <div style="display:block; float:left; width:220px; *margin-top:8px;">
                                            <div class="form-label">Plan:</div>
                                            <select name="codigo_plan"  class="smallInput" style="width:138px; float:right;" onChange="this.form.onsubmit()">
                                                <?php
                                                $sql = "SELECT id_plan, nombre_plan FROM planes ORDER BY nombre_plan";
                                                echo GenerarOptions($sql, $_SESSION['rec_pr_codigo_plan'], true, "Seleccione");
                                                ?>
                                            </select>
                                        </div>
                                    </td>
                                    <td width="250">
                                        <div style="display:block; float:left; width:220px; *margin-top:8px;">
                                            <div class="form-label">Compañ&iacute;a:</div>
                                            <select name="codigo_compania"  class="smallInput" style="width:138px; float:right;" onChange="this.form.onsubmit()">
                                                <?php
                                                $sql = "SELECT idcompania,com_nombre FROM companias ORDER BY com_nombre";
                                                echo GenerarOptions($sql, $_SESSION['rec_pr_codigo_compania'], true, "Seleccione");
                                                ?>
                                            </select>
                                        </div>
                                    </td>
                                    <td width="250">
                                        <div style="display:block; float:left; width:220px;">
                                            <div class="form-label">Nro. Gesti&oacute;n:</div>
                                            <input type="text" name="num_gest" class="smallInput" style="width:130px; float:right;" value="<?php echo htmlspecialchars($_SESSION['rec_pr_gest']); ?>"/>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td colspan="2">
                                        <a style="margin-top: 3px;" href="javascript:void(0);" onClick="clear_form_elements('#formReclamosBuscarHome');actualizarReclamosHome();" class="button_notok">
                                            <span>Limpiar busqueda</span>
                                        </a>
                                        <a style="margin-top: 3px;" class="button_ok" href="javascript:void(0);" onClick="actualizarReclamosHome();">
                                            <span>Buscar</span>
                                        </a>
                                    </td>
                                </tr>
                            </table>
                        </form>
                    </div>
                    <!-- LISTADO PRINCIPAL -->
                    <div class="portlet">
                        <div id="reclamos_listado_principal"></div>
                        <a style="margin-top: 3px; margin-left: 30px;" href="javascript:void(0);" onClick="exportarListado('reclamos_principal');">
                            <img src="images/excel.png" />
                        </a>
                        <a style="margin-top: 3px;"  onClick="exportarListado('reclamos_principal', 'pdf');" href="javascript:void(0);" onClick="actualizarPolizasPendientes();">
                            <img src="images/pdf.png" />
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <!-- CARGAR LISTA AJAX -->
        <script type="text/javascript">
            $(function(){
                actualizarReclamosHome();
            });
        </script>
    <?php } ?>
    <div class="clear"></div>
    <?php include("inc.footer.php"); ?>
</body>
</html>
