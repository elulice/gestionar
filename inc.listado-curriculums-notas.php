<?php 
	$sSQL = "SELECT p.PerApellido, p.PerNombres ";
	$sSQL .= "FROM persona p LEFT JOIN postulantenotas np ON p.PerNro = pn.PerNro ";
	$sSQL .= "WHERE pn.PerNro = " . $m_lIDPostulante;
	$cBD = new BD();
	$aPostulante = $cBD->Seleccionar($sSQL, true);
echo $sSQL;
?>
<table width="780" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="30" class="encabezado-titulo-bg"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="20"><img src="images/espacio.gif" width="1" height="1" /></td>

        <td valign="top" class="encabezado-titulo-texto" style="padding-top:5px;">Listado de Notas - 
		<?php print $aPostulante["PerApellido"]." ".$aPostulante["PerNombres"]; ?></td>
        </tr>
    </table></td>
  </tr>
  <tr>
    <td><img src="images/espacio.gif" width="1" height="20"></td>
  </tr>
</table>
<table width="780" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="20"><img src="images/listado-encabezado-inicio.jpg" width="20" height="37"></td>
        <td class="listado-encabezado-bg"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="90"><img src="images/espacio.gif" width="1" height="1"></td>
            <td width="70" class="listado-encabezado-texto">Fecha</td>
            <td width="380" class="listado-encabezado-texto">Nota</td>
            <td width="90" align="center" class="listado-encabezado-texto">Experiencia</td>
            <td align="center" class="listado-encabezado-texto">Concepto General</td>
          </tr>
        </table></td>
        <td width="20"><img src="images/listado-encabezado-final.jpg" width="20" height="37"></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="5" class="listado-contenido-inicio"><img src="images/espacio.gif" width="1" height="1"></td>
        <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
		  <?php
		  		$lRegistros = 0;
			 	$sSQL = "SELECT n.idnota, n.fecha, n.nota, c1.abrev AS experiencia, c2.abrev AS conceptogeneral ";
				$sSQL .= "FROM notas_postulantes n ";
				$sSQL .= "LEFT OUTER JOIN calificaciones c1 ON c1.idcalificacion = n.experiencia ";
				$sSQL .= "LEFT OUTER JOIN calificaciones c2 ON c2.idcalificacion = n.conceptogeneral ";
				$sSQL .= "WHERE n.idpostulante = ".$m_lIDPostulante." ";
				$sSQL .= "ORDER BY n.fecha DESC ";
									
				$cBD = new BD();
				$oResultado = $cBD->Seleccionar($sSQL);
				while($aRegistro = $cBD->RetornarFila($oResultado))
				{
					$sPosicion = (($sPosicion == "1") ? "2" : "1");
					
		  ?>
          <tr>
            <td class="listado-fila-bg-<?php print($sPosicion); ?>">
			
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="15"><img src="images/espacio.gif" width="1" height="1"></td>
                <td width="90">
				
				<table width="90" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="30"><a href="javascript:verNota(<?php print($aRegistro["idnota"]); ?>, <?php print($aRegistro["idnota"]); ?>)"><img src="images/btn-ver-mas-<?php print($sPosicion); ?>.jpg" alt="Vista Rapida" width="24" height="23" border="0"></a></td>
				    <td width="30"><a href="am-curriculums-notas.php?idregistro=<?php print($aRegistro["idnota"]); ?>&idpostulante=<?php print $m_lIDPostulante; ?>&url=<?php print($m_sURL); ?>"><img src="images/btn-modificar-<?php print($sPosicion); ?>.jpg" alt="Editar" width="24" height="23" border="0"></a></td>

                    <td width="70"><a href="abm.php?tabla=notas_postulante&columna=idnota&idregistro=<?php print($aRegistro["idnota"]); ?>&url=<?php print($m_sURL); ?>" onclick="return confirm('&iquest;Desea eliminar este Estudio?')"><img src="images/btn-eliminar-<?php print($sPosicion); ?>.jpg" alt="Eliminar" width="24" height="23" border="0"></a></td>
                  </tr>
                </table>				</td>
               <td width="70" class="listado-texto"><?php print(date("d/m/y", strtotime($aRegistro["fecha"]))); ?></td>
               <td width="380" class="listado-texto"><?php print(substr($aRegistro["nota"], 0, 67)." [...]"); ?></td>
                <td width="90" align="center" class="listado-texto"><?php print($aRegistro["experiencia"]); ?></td>
                <td align="center" class="listado-texto"><?php print($aRegistro["conceptogeneral"]); ?></td>
              </tr>

<tr id="trNota-<?php print($aRegistro["idnota"]); ?>" class="listado-fila-oculta">
                <td width="15"><img src="images/espacio.gif" width="1" height="1"></td>
                <td colspan="6" id="tdNota-<?php print($aRegistro["idnota"]); ?>" class="informe-separador">&nbsp;</td>
               </tr>
            </table></td>
          </tr>
          <?php
			 		$lRegistros++;
				}
				if($lRegistros == 0)
				{
			 ?>
          <tr>
            <td><img src="images/espacio.gif" width="1" height="20"></td>
          </tr>
			 <?php } ?>
        </table></td>
        <td width="6" class="listado-contenido-final"><img src="images/espacio.gif" width="1" height="1"></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="100"><a href="am-curriculums-notas.php?idregistro=0&idpostulante=<?php print $m_lIDPostulante; ?>&url=<?php print($m_sURL); ?>"><img src="images/listado-pie-inicio.jpg" alt="Agregar" width="100" height="40" border="0"></a></td>
        <td class="listado-pie-bg">&nbsp;</td>
        <td width="20"><img src="images/listado-pie-final.jpg" width="20" height="40"></td>
      </tr>
    </table></td>
  </tr>
</table>
