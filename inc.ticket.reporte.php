<div id="reporte" class="grid_15">

    <!-- Cabecera -->
    <div id="menu-repo-div">
        <div style="float:right;display:none; margin:5px 0px; width: 150px;" id="menu-graficos">
            <a href="javascript: void(0);" onclick="mostrarConsulta();" class="buttons buttons-volver ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary" style="width: 130px; font-size:11px; font-weight: normal; margin-left: 20px;" role="button"><span class="ui-button-icon-primary ui-icon ui-icon-arrowreturnthick-1-w"></span><span class="ui-button-text">Volver a Consulta</span></a>
        </div>
        <div style="float:right;display:block; width:640px; margin:5px 0px;">
            <!--            <a class="save_inline" href="javascript:void(0);" onclick="if($('[name=id_reporte]').val() == 0){$('#nombre-dialog').dialog('open');} else{guardarReporte();}">     Guardar</a>
                        <a class="historial_inline" href="javascript: void(0);" onclick="$('#nombrehist-dialog').dialog('open');" style="margin-left:10px;"> Guardar Historial  </a>-->
            <a class="excel_inline" href="javascript: void(0);" onclick="exportarReporteExcel('excel');" style="margin-left:10px; ">  Exportar Excel</a>
            <a class="pdf_inline" href="#" style="margin-left:10px;" onclick="exportarReportePdf();">  Exportar PDF</a>
            <!--<a class="enviar_inline" href="#" style="margin-left:10px; ">  Enviar</a>-->
            <a class="print_inline" href="#" style="margin-left:10px; " onclick="imprimirReporte();">  Imprimir</a>
            <a href="javascript:void(0);" onclick="loadChart();" class="buttons buttons-graficos ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary" style="width: 85px; font-size:11px; font-weight: normal; margin-left: 20px;" role="button"><span class="ui-button-icon-primary ui-icon ui-icon-image"></span><span class="ui-button-text">Gráficos</span></a>
        </div>
    </div>

    <!-- Lista -->
    <div id="repo-div" style="clear:both;float:left;overflow:none;width:870px;height:500px;margin-left:10px;"></div>

    <!-- Gráficos -->
    <div id="graficos-div" style="clear:both; float:left; width:870px; height:460px; margin-left:10px; display: none;"></div>

</div>