<?php
ini_set("display_errors", TRUE);

require_once("clases/framework-1.0/class.bd.php");
require_once("clases/phppaging/PHPPaging.lib.php");

switch ($_GET["objeto"]) {
    case "pedidos" : escritorio_pedidos();
        break;
    case "eventos" : escritorio_eventos();
        break;
    case "postulantes" : escritorio_postulantes();
        break;
}

function escritorio_pedidos() {
    $query = "SELECT OfeNro, OfeFechAlta, CliRSocial, PueNom from oferta
      LEFT JOIN ofertascliente USING(OfeNro) LEFT JOIN puesto USING(PueNro)
      LEFT JOIN cliente USING(CliNro) ORDER BY OfeFechAlta DESC";

    $db = new BD();
    $db->Conectar();

    $paging = new PHPPaging($db->RetornarConexion());
    $paging->agregarConsulta($query);
    $paging->linkClase("navPage");
    $paging->porPagina(2);
    $paging->paginasAntes(1);
    $paging->paginasDespues(1);
    $paging->ejecutar();
    ?>
    <div style="height:140px;">
        <table width="430" cellpadding="0" cellspacing="0" id="box-table-a">
            <thead>
            <th width="60" scope="col"><span style="color:#c60;font-weight:bold;">Fecha</span></th>
            <th width="" scope="col"><span style="color:#c60;font-weight:bold;">Usuaria</span></th>
            <th width="" scope="col"><span style="color:#c60;font-weight:bold;">Puesto</span></th>
            <th width="60" scope="col"><span style="color:#c60;font-weight:bold;">Opciones</span></th>
            </thead>
            <tbody>
                <?php
                while ($row = $paging->fetchResultado()) {
                    ?>
                    <tr>
                        <td style="padding:8px;"><?php echo date("d/m/Y", strtotime($row["OfeFechAlta"])); ?></td>
                        <td style="padding:8px;"><?php echo $row["CliRSocial"]; ?></td>
                        <td style="padding:8px;"><?php echo $row["PueNom"]; ?></td>
                        <td style="padding:8px;">
                            <img src="images/icons/page_edit.png" title="Editar" style="cursor:pointer;" onclick="editar_oferta(<?php echo $row["OfeNro"]; ?>);" />
                            <!--img src="images/icons/page_delete.png" title="Editar" style="cursor:pointer ;"/-->
                        </td>
                    </tr>
        <?php
    }
    ?>
            </tbody>
        </table>
    </div>
    <div class="pagination"><?php echo $paging->fetchNavegacion(); ?></div>
    <?php
}

function escritorio_postulantes() {
    $query = "SELECT PerNro, PerApellido, PerNombres, PerLocalidad FROM persona";

    $db = new BD();
    $db->Conectar();

    $paging = new PHPPaging($db->RetornarConexion());
    $paging->agregarConsulta($query);
    $paging->linkClase("navPage");
    $paging->porPagina(3);
    $paging->paginasAntes(1);
    $paging->paginasDespues(1);
    $paging->ejecutar();
    ?>
    <div style="height:140px;">
        <table width="430" cellpadding="0" cellspacing="0" id="box-table-a">
            <thead>
            <th width="110" scope="col"><span style="color:#c60;font-weight:bold;">Apellido</span></th>
            <th width="" scope="col"><span style="color:#c60;font-weight:bold;">Nombre</span></th>
            <th width="" scope="col"><span style="color:#c60;font-weight:bold;">Localidad</span></th>
            <th width="60" scope="col"><span style="color:#c60;font-weight:bold;">Opciones</span></th>
            </thead>
            <tbody>
    <?php
    while ($row = $paging->fetchResultado()) {
        ?>
                    <tr>
                        <td style="padding:8px;"><?php echo $row["PerApellido"]; ?></td>
                        <td style="padding:8px;"><?php echo $row["PerNombres"]; ?></td>
                        <td style="padding:8px;"><?php echo $row["PerLocalidad"]; ?></td>
                        <td style="padding:8px;">
                            <img src="images/icons/page_edit.png" title="Editar" style="cursor:pointer;" onclick="editar_curriculum(<?php echo $row["PerNro"]; ?>);" />
                            <!--img src="images/icons/page_delete.png" title="Editar" style="cursor:pointer ;"/-->
                        </td>
                    </tr>
        <?php
    }
    ?>
            </tbody>
        </table>
    </div>
    <div class="pagination"><?php echo $paging->fetchNavegacion(); ?></div>
<?php
    }

    function escritorio_eventos() {

        $query = "SELECT
                        p.PerApellido, p.PerNombres, pu.PueNom, c.CliRsocial, p.PerNro
                    FROM
                        postulacion po
                            LEFT JOIN persona p ON po.PerNro = p.PerNro
                            LEFT JOIN oferta o ON po.OfeNro = o.OfeNro
                            LEFT JOIN puesto pu ON o.PueNro = pu.PueNro
                            LEFT JOIN ofertascliente oc ON o.OfeNro = oc.OfeNro
                            LEFT JOIN cliente c ON oc.CliNro = c.CliNro
                    WHERE
                        1 ORDER BY po.PosFecha DESC";

        $db = new BD();
        $db->Conectar();

        $paging = new PHPPaging($db->RetornarConexion());
        $paging->agregarConsulta($query);
        $paging->linkClase("navPage");
        $paging->porPagina(2);
        $paging->paginasAntes(1);
        $paging->paginasDespues(1);
        $paging->ejecutar();
?>
    <div style="height: 140px;">
        <table width="430" cellpadding="0" cellspacing="0" id="box-table-a">
            <thead>
                <tr>
                    <th width="" scope="col"><span style="color:#c60;font-weight:bold;">Nombre</span></th>
                    <th width="" scope="col"><span style="color:#c60;font-weight:bold;">Usuaria</span></th>
                    <th width="" scope="col"><span style="color:#c60;font-weight:bold;">Puesto</span></th>
                    <th width="" scope="col"><span style="color:#c60;font-weight:bold;">Opciones</span></th>
                </tr>
            </thead>
            <tbody>
<?php
    while ($row = $paging->fetchResultado()) {
?>
                <tr>
                    <td style="padding:8px;"><?php echo $row["PerApellido"] . ", " . $row["PerNombres"]; ?></td>
                    <td style="padding:8px;"><?php echo $row["CliRsocial"]; ?></td>
                    <td style="padding:8px;"><?php echo $row["PueNom"]; ?></td>
                    <td style="padding:8px;">
                            <img src="images/icons/page_edit.png" title="Editar" style="cursor:pointer;" onclick="editar_curriculum(<?php echo $row["PerNro"]; ?>);" />
                            <!--img src="images/icons/page_delete.png" title="Editar" style="cursor:pointer ;"/-->
                        </td>
                </tr>
<?php
    }
?>
            </tbody>
        </table>
    </div>
    <div class="pagination"><?php echo $paging->fetchNavegacion(); ?></div>
    <?php
}
?>
