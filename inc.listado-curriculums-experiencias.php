<?php 
	$sSQL = "SELECT PerApellido, PerNombres ";
	$sSQL .= "FROM persona ";
	$sSQL .= "WHERE PerNro = " . $m_lIDPostulante;
	$cBD = new BD();
	$aPostulante = $cBD->Seleccionar($sSQL, true);

?>
<table width="780" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="30" class="encabezado-titulo-bg"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="20"><img src="images/espacio.gif" width="1" height="1" /></td>

        <td valign="top" class="encabezado-titulo-texto" style="padding-top:5px;">Listado de Experiencias Laborales - 
		<?php print $aPostulante["PerApellido"]." ".$aPostulante["PerNombres"]; ?></td>
        </tr>
    </table></td>
  </tr>
  <tr>
    <td><img src="images/espacio.gif" width="1" height="20"></td>
  </tr>
</table>
<table width="780" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="20"><img src="images/listado-encabezado-inicio.jpg" width="20" height="37"></td>
        <td class="listado-encabezado-bg"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="100"><img src="images/espacio.gif" width="1" height="1"></td>
            <td width="280" class="listado-encabezado-texto">Puesto</td>
            <td width="250" class="listado-encabezado-texto">Empresa</td>
            <td width="110" align="center" class="listado-encabezado-texto">Egreso</td>
          </tr>
        </table></td>
        <td width="20"><img src="images/listado-encabezado-final.jpg" width="20" height="37"></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="5" class="listado-contenido-inicio"><img src="images/espacio.gif" width="1" height="1"></td>
        <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
		  <?php
		  		$lRegistros = 0;
			 	$sSQL = "SELECT exp.DPueNro, exp.PueUltimaEmpresa, pue.PueNom, exp.PueFechaBaja ";
				$sSQL .= "FROM datospuesto exp ";
				$sSQL .= "INNER JOIN puesto pue ON pue.PueNro = exp.PueNro ";
				$sSQL .= "WHERE exp.PerNro = ".$m_lIDPostulante." ";
				$sSQL .= "ORDER BY PueFechaBaja DESC ";
						
				$cBD = new BD();
				$oResultado = $cBD->Seleccionar($sSQL);
				while($aRegistro = $cBD->RetornarFila($oResultado))
				{
					$sPosicion = (($sPosicion == "1") ? "2" : "1");
					
		  ?>
          <tr>
            <td class="listado-fila-bg-<?php print($sPosicion); ?>">
			
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="15"><img src="images/espacio.gif" width="1" height="1"></td>
                <td width="100">
				
				<table width="90" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="30"><a href="javascript:verExperiencia(<?php print($aRegistro["DPueNro"]); ?>)"><img src="images/btn-ver-mas-<?php print($sPosicion); ?>.jpg" alt="Vista Rapida" width="24" height="23" border="0"></a></td>
				    <td width="30"><a href="am-curriculums-experiencias.php?idregistro=<?php print($aRegistro["DPueNro"]); ?>&idpostulante=<?php print $m_lIDPostulante; ?>&url=<?php print($m_sURL); ?>"><img src="images/btn-modificar-<?php print($sPosicion); ?>.jpg" alt="Editar" width="24" height="23" border="0"></a></td>

                    <td><a href="abm.php?tabla=experiencia_laboral&columna=DPueNro&idregistro=<?php print($aRegistro["DPueNro"]); ?>&url=<?php print($m_sURL); ?>" onclick="return confirm('&iquest;Desea eliminar esta Experiencia?')"><img src="images/btn-eliminar-<?php print($sPosicion); ?>.jpg" alt="Eliminar" width="24" height="23" border="0"></a></td>
                  </tr>
                </table>				</td>
                <td width="280" class="listado-texto"><?php print($aRegistro["PueNom"]); ?></td>
                <td width="250" class="listado-texto"><?php print($aRegistro["PueUltimaEmpresa"]); ?></td>
                <td width="110" align="center" class="listado-texto"><?php print(date("d/m/Y", strtotime($aRegistro["PueFechaBaja"]))); ?></td>
              </tr>
              <tr>
                
                </tr>
              <tr id="trExperiencia-<?php print($aRegistro["DPueNro"]); ?>" class="listado-fila-oculta">
                <td colspan="5" id="tdExperiencia-<?php print($aRegistro["DPueNro"]); ?>" class="informe-separador">&nbsp;</td>
              </tr>
            </table></td>
          </tr>
          <?php
			 		$lRegistros++;
				}
				if($lRegistros == 0)
				{
			 ?>
          <tr>
            <td><img src="images/espacio.gif" width="1" height="20"></td>
          </tr>
			 <?php } ?>
        </table></td>
        <td width="6" class="listado-contenido-final"><img src="images/espacio.gif" width="1" height="1"></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="100"><a href="am-curriculums-experiencias.php?idregistro=0&idpostulante=<?php print $m_lIDPostulante; ?>&url=<?php print($m_sURL); ?>"><img src="images/listado-pie-inicio.jpg" alt="Agregar" width="100" height="40" border="0"></a></td>
        <td class="listado-pie-bg">&nbsp;</td>
        <td width="20"><img src="images/listado-pie-final.jpg" width="20" height="40"></td>
      </tr>
    </table></td>
  </tr>
</table>
