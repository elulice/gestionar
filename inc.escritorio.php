<?php
require_once("clases/framework-1.0/class.bd.php");
require_once("clases/phppaging/PHPPaging.lib.php");
require_once("includes/funciones.php");
?>
<?php require_once("inc.postulante.detalle.php"); ?>
<div id="div_dialog_pedidos" style="display:none;"></div>

<div id="dialog_widget_eventos_detalle" style="display:none;">
    <div class="ddu_campo">
        <span>Título:</span>
        <input type="text" name="txt1" class="smallInput" />
    </div>
    <div class="ddu_campo_doble">
        <div class="ddu_campo_col_izq">
            <div class="ddu_campo">
                <span>Desde:</span>
                <input type="text" name="fecha_desde" class="smallInput" />
            </div>
        </div>
        <div class="ddu_campo_col_der">
            <div class="ddu_campo">
                <span>Hasta:</span>
                <input type="text" name="fecha_hasta" class="smallInput" />
            </div>
        </div>
    </div>
    <div class="ddu_campo_textarea">
        <span>Detalle:</span>
        <textarea name="detalle" class="smallInput" style="height:50px;"></textarea>
    </div>
</div>
<div id="portlets" style="padding:0;margin-left:15px;">
    <div class="column">
        <div class="portlet">
            <div class="portlet-header">
                <span> 
                    <img src="images/icons/services.png" />Agenda
                </span>
                <!--a class="custom_menu" style="background-image:url(images/icons/add_small.png);" onclick="nuevo_evento();">Evento</a>
                <a class="custom_menu" style="background-image:url(images/icons/calendar.gif);">Ver Calendario</a-->
            </div>
            <div class="portlet-content">
                <div id="widget_eventos" class="navPage"></div>
            </div>
        </div>
        <div class="portlet">
            <div class="portlet-header">
                <span> 
                    <img src="images/icons/user-16.png" />Postulantes
                </span>
            </div>
            <div class="portlet-content">
                <div id="widget_postulantes" class="navPage"></div>
            </div>
        </div>
    </div>
    <div class="column" style="margin-left:10px;">
        <div class="portlet">
            <div class="portlet-header">
                <span> 
                    <img src="images/icons/clock.png" />Pedidos
                </span>
            </div>
            <div class="portlet-content">
                <div id="widget_pedidos" class="navPage"></div>
            </div>
        </div>
        <div class="portlet">
            <div class="portlet-header">
                <span> 
                    <img src="images/icons/chart_bar.gif" />Reportes
                </span>
                <!--a class="custom_menu" style="background-image:url(images/icons/link.png);">Configurar</a-->
            </div>
            <div class="portlet-content">
                <div id="production-chart" style="height: 170px"></div>
            </div>
            <script type="text/javascript">
<?php
//                    $sSQL = 'SELECT
//                                c.CliRsocial
//                                , COUNT(*) AS nro
//                            FROM
//                                postulacion p
//                                LEFT JOIN oferta o ON p.OfeNro = o.OfeNro
//                                LEFT JOIN ofertascliente oc ON o.OfeNro = oc.OfeNro
//                                LEFT JOIN cliente c ON oc.CliNro = c.CliNro
//                            GROUP BY
//                                c.CliNro
//                            ORDER BY
//                                COUNT(*) DESC
//                            LIMIT 10';

$sSQL = 'SELECT
                                c.CliRsocial
                                , COUNT(*) AS nro
                            FROM
                                all_tickets p
                            GROUP BY
                                cliente.CliNro
                            ORDER BY
                                COUNT(*) DESC
                            LIMIT 10';

$db = new BD();
$db->Conectar();
$experiencias = $db->Ejecutar($sSQL);
while ($experiencia = mysql_fetch_assoc($experiencias)) {
    $usuarias[] = $experiencia['CliRsocial'];
    $cantidad[] = (int) $experiencia['nro'];
}
?>
    $(document).ready(function() {
        chart = new Highcharts.Chart({
            chart: {
                renderTo: 'production-chart',
                type: 'column',
                margin: [ 15, 15, 60, 50]
            },
            credits : {
                enabled: false
            },
            title: {
                text: 'Postulantes por Usuaria',
                style: {
                    font: 'normal 13px Verdana, sans-serif'
                }
            },
            xAxis: {
                categories: <?php echo json_encode($usuarias) ?>,
                labels: {
                    rotation: -45,
                    align: 'right',
                    style: {
                        font: 'normal 10px Verdana, sans-serif'
                    }
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Postulantes'
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                formatter: function() {
                    return '<b>'+ this.x +'</b><br/>'+
                        'Postulantes: '+ this.y;
                }
            },
            series: [{
                    name: 'Postulante',
                    data: <?php echo json_encode($cantidad) ?>,
                    dataLabels: {
                        enabled: true,
                        rotation: -90,
                        color: '#FFFFFF',
                        align: 'right',
                        x: -3,
                        y: 10,
                        formatter: function() {
                            return this.y;
                        },
                        style: {
                            font: 'normal 11px Verdana, sans-serif'
                        }
                    }
                }]
        });
    });

            </script>
        </div>
    </div>
</div>
<div>
    <div>
