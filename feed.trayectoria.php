<?php

require_once("clases/framework-1.0/class.bd.php");
require_once("clases/phppaging/PHPPaging.lib.php");

switch ($_GET["objeto"]) {

   case "principal" : curriculumns_principal(); break;

}

function curriculumns_principal() {

   $dni = (int) $_GET["txt_dni"];
   $cliente = (int) $_GET["cbo_cliente"];
   $estado_post = (int) $_GET["cbo_estadopost"];

   $where = "";
   if ($dni > 0) $where .= " AND p.PerDocumento like '%$dni%' ";
   if ($estado_post > 0) $where .= " AND ph.PEsNro = $estado_post ";
   if ($cliente > 0) $where .= " AND c.CliNro = $cliente ";

   $sSQL = "SELECT c.CliRSocial, o.OfeTitulo, o.OfeReferencia, ";
   $sSQL .= "p.PerApellido, p.PerNombres, p.PerDocumento, ";
   $sSQL .= "ep.PEsDescrip AS EstadoUltimo, ep2.PEsDescrip AS EstadoHist, ";
   $sSQL .= "ph.phistFecha, phistId ";
   $sSQL .= "FROM postulacion po ";
   $sSQL .= "INNER JOIN persona p ON p.PerNro = po.PerNro ";
   $sSQL .= "INNER JOIN oferta o ON o.OfeNro = po.OfeNro ";
   $sSQL .= "INNER JOIN ofertascliente oc ON o.OfeNro = oc.OfeNro ";
   $sSQL .= "INNER JOIN cliente c ON c.CliNro = oc.CliNro ";
   $sSQL .= "INNER JOIN postulacionestado ep ON ep.PEsNro = po.PEsNro ";
   $sSQL .= "INNER JOIN postulacionhistorico ph ";
   $sSQL .= "ON (ph.OfeNro = po.OfeNro AND ph.PerNro = po.PerNro) ";
   $sSQL .= "INNER JOIN postulacionestado ep2 ON ep2.PEsNro = ph.PEsNro ";
   $sSQL .= "WHERE 1 $where";

   echo $sSQL;
   
   
   $db = new BD();
   $db->Conectar();

   $paging = new PHPPaging($db->RetornarConexion());
   $paging->agregarConsulta($sSQL);
   $paging->linkClase("navPage");
   $paging->porPagina(4);
   $paging->ejecutar();

   ?>
   <div style="height:240px">
   <table width="830" cellpadding="0" cellspacing="0" style="margin:12px 0 0 12px;" id="box-table-a">
      <thead>
	 <tr>
	    <th width="150" scope="col"><span style="color:#c60;font-weight:bold;">Cliente</span></th>
	    <th width="150" scope="col"><span style="color:#c60;font-weight:bold;">Pedido</span></th>
	    <th width="50" scope="col"><span style="color:#c60;font-weight:bold;">ID</span></th>
	    <th width="150" scope="col"><span style="color:#c60;font-weight:bold;">Apellido y Nombre</span></th>
	    <th width="50" scope="col"><span style="color:#c60;font-weight:bold;">Documento</span></th>
	    <th width="80" scope="col"><span style="color:#c60;font-weight:bold;">Situación</span></th>
	    <th width="80" scope="col"><span style="color:#c60;font-weight:bold;">Histórico</span></th>
	    <th width="50" scope="col"><span style="color:#c60;font-weight:bold;">Fecha</span></th>
	 </tr>
      </thead>
      <tbody>
   <?php

   while ($row = $paging->fetchResultado()) {
   ?>
   <tr>
      <td style="padding:8px;"><?php echo $row["CliRSocial"]; ?></td>
      <td style="padding:8px;"><?php echo $row["OfeTitulo"]; ?></td>
      <td style="padding:8px;"><?php echo $row["OfeReferencia"]; ?></td>
      <td style="padding:8px;"><?php echo $row["PerApellido"]; ?></td>
      <td style="padding:8px;"><?php echo $row["PerDocumento"]; ?></td>
      <td style="padding:8px;"><?php echo $row["EstadoUltimo"]; ?></td>
      <td style="padding:8px;"><?php echo $row["EstadoHist"]; ?></td>
      <td style="padding:8px;"><?php echo date("d/m/Y", strtotime($row["phistFecha"])); ?></td>
   </tr>
   <?php
   }
   ?>
   </tbody>
   </table>
   </div>
   <div class="pagination"><?php echo $paging->fetchNavegacion(); ?></div>
   <?php
}

?>
