<?php
	//if($m_lIDRegistro > 0)
	$sSQL = "SELECT PerApellido, PerNombres ";
	$sSQL .= "FROM persona ";
	$sSQL .= "WHERE PerNro = " . $m_lIDPostulante;
	$cBD = new BD();
	$aPostulante = $cBD->Seleccionar($sSQL, true);
	
	$m_lIDOferta = is_numeric($_REQUEST["idoferta"]) ? $_REQUEST["idoferta"] : 0;
	{
		$sSQL = "SELECT *  ";
		$sSQL .= "FROM postulacionhistorico ph ";

		$sSQL .= "WHERE ph.PerNro = " . $m_lIDPostulante;
		$sSQL .= " AND ph.OfeNro = " . $m_lIDOferta;
		$sSQL .= " ORDER BY ph.phistId DESC LIMIT 1 ";

//echo $sSQL;

		$cBD = new BD();
		$aRegistro = $cBD->Seleccionar($sSQL, true);
	}
	
?>
<link href="estilos/general.css" rel="stylesheet" type="text/css" />


<table width="780" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="30" class="encabezado-titulo-bg"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="20"><img src="images/espacio.gif" width="1" height="1"></td>
        <td class="encabezado-titulo-texto">Situaci&oacute;n del postulante</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><img src="images/espacio.gif" width="1" height="20"></td>
  </tr>
</table>
<table width="550" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="20"><img src="images/formulario-encabezado-inicio.jpg" width="20" height="37" /></td>
        <td class="formulario-encabezado-bg">Situaci&oacute;n del Postulante - <?php print($aPostulante["PerApellido"]); ?>, <?php print($aPostulante["PerNombres"]); ?></td>
        <td width="20"><img src="images/formulario-encabezado-final.jpg" width="20" height="37" /></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="4" class="formulario-contenido-inicio"><img src="images/espacio.gif" width="1" height="1" /></td>
        <td><form action="abm.php?tabla=postulacionhistorico&amp;columna=phistId&amp;idregistro=0&amp;url=listado-curriculums-trayectoria.php?idpostulante=<?php print($m_lIDPostulante); ?>" method="post" name="frmRegistro" id="frmRegistro" onsubmit="adaptaFecha(this)">
          <table width="100%" border="0" cellpadding="4" cellspacing="0">
		  
		  <input name="PerNro" id="PerNro" type="hidden" value="<?php print $m_lIDPostulante; ?>" />
		  <input name="OfeNro" id="OfeNro" type="hidden" value="<?php print $m_lIDOferta; ?>" />
		  <input name="SelectorNro" id="SelectorNro" type="hidden" value="<?php print $_SESSION["idmiembro"]; ?>" />
		  
         <tr>
              <td width="10" valign="top" class="formulario-etiquetas">&nbsp;</td>
              <td width="100" valign="top" class="formulario-etiquetas">Situaci&oacute;n:</td>
              <td>
                <select name="PEsNro" id="PEsNro" style="width: 120px;">
                  <?php
					$sSQL = "SELECT PEsNro, PEsDescrip FROM postulacionestado ";
					$sSQL .= "ORDER BY PEsNro ASC ";
					print(GenerarOptions($sSQL, $aRegistro["PEsNro"]));
			  ?>
                </select>              </td>
            </tr>
            <tr>
              <td width="10" valign="top" class="formulario-etiquetas">&nbsp;</td>
              <td height="10" valign="bottom" class="formulario-etiquetas">Fecha:</td>
              <td height="0" valign="bottom">
                <input name="HIDDEN_phistFecha_F" type="text" class="formulario-textbox" id="HIDDEN_phistFecha_F" style="width: 120px;" maxlenght="64" value="<?php print(date("d-m-Y", strtotime($aRegistro["phistFecha"]))); ?>" />
              </td>
              </tr>
            <tr>
              <td width="10" valign="top" class="formulario-etiquetas">&nbsp;</td>
              <td height="10" valign="bottom" class="formulario-etiquetas">Hora:</td>
              <td height="0" valign="bottom">
                <input name="HIDDEN_phistFecha_H" type="text" class="formulario-textbox" id="HIDDEN_phistFecha_H" style="width: 120px;" maxlenght="64" value="<?php print(date("H:i", strtotime($aRegistro["phistFecha"]))); ?>" />
				
				<input name="phistFecha" id="phistFecha" type="hidden" value="<?php print(date("Y-m-d H:i:s", strtotime($aRegistro["phistFecha"]))); ?>" />
              </td>
            </tr>
            <tr>
              <td width="10" valign="top" class="formulario-etiquetas">&nbsp;</td>
              <td height="20" valign="top" class="formulario-etiquetas">Observaciones:</td>
              <td height="20" valign="bottom"><textarea name="PosDetalle" class="formulario-textbox" id="PosDetalle" style="width: 300px; height: 100px;"><?php print($aRegistro["PosDetalle"]); ?></textarea></td>
            </tr>
            <tr>
              <td width="10" valign="top">&nbsp;</td>
              <td height="30" colspan="2" align="center"><img src="images/espacio.gif" width="1" height="1" />
                      <input name="BTN_Guardar" type="submit" id="BTN_Guardar" value="Guardar" />
                      <input name="BTN_Cancelar" type="reset" id="BTN_Cancelar" value="Cancelar" onclick="history.back();" /></td>
            </tr>
          </table>
        </form></td>
        <td width="6" class="formulario-contenido-final"><img src="images/espacio.gif" width="1" height="1" /></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="20"><img src="images/formulario-pie-inicio.jpg" width="20" height="40" /></td>
        <td class="formulario-pie-bg"><img src="images/espacio.gif" width="1" height="1" /></td>
        <td width="20"><img src="images/formulario-pie-final.jpg" width="20" height="40" /></td>
      </tr>
    </table></td>
  </tr>
</table>
<script>
	function adaptaFecha(pForm)
	{
		var fFecha = pForm.HIDDEN_phistFecha_F.value;
		var fHora = pForm.HIDDEN_phistFecha_H.value;
//		var fFinal = pForm.phistFecha;
		
		pForm.phistFecha.value = formateaFecha(fFecha) + " " + fHora;
	}
</script>
