<div style="width:380px;margin:auto;">
   <input type="hidden" name="EtuNro" />
   <form id="frm_cv_estudios" method="post" target="hidden_iframe">
   <div class="ddu_title">Estudios</div>
   <div class="ddu_campo">
      <span>Institución:</span>
      <input type="text" name="EtuInstitucion" class="smallInput" />
   </div>
   <div class="ddu_campo_doble">
      <div class="ddu_campo_col_izq">
	 <div class="ddu_campo">
	    <span>Ingreso:</span>
	    <input type="text" name="EtuIngreso" id="EtuIngreso" class="smallInput" />
	 </div>
      </div>
      <div class="ddu_campo_col_der">
	 <div class="ddu_campo">
	    <span>Egreso:</span>
	    <input type="text" name="EtuEgreso" class="smallInput" />
	 </div>
      </div>
   </div>
   <div class="ddu_campo_select">
      <span>Nivel Estudios:</span>
      <select name="NEsNro" class="smallInput" onchange="cargarListas(this, 'EtuAreaEstudio');">
      <?php
   $query = "SELECT NEsNro, NEsDescrip FROM nivelestudio";
   echo GenerarOptions($query, NULL);
      ?>
      </select>
   </div>
   <div class="ddu_campo_select">
      <span>Finalizado:</span>
      <select name="EtuFinalizado" class="smallInput">
      <?php
   $query = "SELECT idestado, nombre FROM _estado_estudio";
   echo GenerarOptions($query, NULL);
      ?>
      </select>
   </div>
   <div class="ddu_campo_select">
      <span>Area Estudio:</span>
      <select name="EtuAreaEstudio" id="EtuAreaEstudio" class="smallInput">
      </select>
   </div>
   <div class="ddu_campo">
      <span>Título</span>
      <input type="text" name="EtuTitulo" class="smallInput" />
   </div>
   <div class="ddu_campo_doble">
      <div class="ddu_campo_col_izq">
	 <div class="ddu_campo">
	    <span>M. Cursadas:</span>
	    <input type="text" name="EtuCursadas" class="smallInput" />
	 </div>
      </div>
      <div class="ddu_campo_col_der">
	 <div class="ddu_campo">
	    <span>Aprobadas:</span>
	    <input type="text" name="EtuAprobadas" class="smallInput" />
	 </div>
      </div>
   </div>
   <div class="ddu_campo_doble">
      <div class="ddu_campo_col_izq">
	 <div class="ddu_campo">
	    <span>Promedio:</span>
	    <input type="text" name="EtuPromedio" class="smallInput" />
	 </div>
      </div>
      <div class="ddu_campo_col_der">
	 <div class="ddu_campo">
	    <span>Cursando:</span>
	    <input type="text" name="EtuAnoCursado" class="smallInput" />
	 </div>
      </div>
   </div>
   <div class="ddu_campo_textarea">
      <span>Notas:</span>
      <textarea name="EtuObservaciones" class="smallInput"></textarea>
   </div>
   </form>
</div>

