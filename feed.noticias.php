<?php

require_once("clases/framework-1.0/class.bd.php");
require_once("clases/phppaging/PHPPaging.lib.php");

switch ($_GET["objeto"]) {

   case "principal"  : curriculums_principal(); break;
   case "detalle"    : curriculums_detalle(); break;
   case "detalle-noticia" : noticias_form(); break;

}

function curriculums_principal() {

   $dni = (int) $_GET["txt_dni"];
   $apellido = $_GET["txt_apellido"];
   $nombre = $_GET["txt_nombre"];

   $where = "";
   if ($dni > 0) $where .= " AND PerDocumento = $dni ";
   if (!empty($apellido)) $where .= " AND PerApellido LIKE '%$apellido%' ";
   if (!empty($nombre)) $where .= " AND PerNombres LIKE '%$nombre%' ";

   $query = "SELECT idnoticia, fecha, titulo, portada, destacada FROM noticias
      ORDER BY fecha DESC, idnoticia DESC";

   $db = new BD();
   $db->Conectar();

   $paging = new PHPPaging($db->RetornarConexion());
   $paging->agregarConsulta($query);
   $paging->linkClase("navPage");
   $paging->porPagina(8);
   $paging->ejecutar();

   ?>
   <div style="height:320px">
   <table width="833" cellpadding="0" cellspacing="0" style="margin:12px 0 0 12px;" id="box-table-a">
      <thead>
	 <tr>
	    <th width="40" scope="col"><span style="color:#c60;font-weight:bold;">Fecha</span></th>
	    <th width="550" scope="col"><span style="color:#c60;font-weight:bold;">Título</span></th>
	    <th width="60" scope="col"><span style="color:#c60;font-weight:bold;">Portada</span></th>
	    <th width="90" scope="col"><span style="color:#c60;font-weight:bold;">Visible</span></th>
	    <th width="60" scope="col"><span style="color:#c60;font-weight:bold;">Opciones</span></th>
	 </tr>
      </thead>
      <tbody>
   <?php

   while ($row = $paging->fetchResultado()) {
   ?>
   <tr>
      <td style="padding:8px;"><?php echo $row["fecha"]; ?></td>
      <td style="padding:8px;"><?php echo $row["titulo"]; ?></td>
      <td style="padding:8px;"><?php echo $row["portada"]; ?></td>
      <td style="padding:8px;"><?php echo $row["destacada"]; ?></td>
      <td style="padding:8px;">
	 <img src="images/icons/page_edit.png" alt="Editar Noticia" title="Editar Noticia" style="cursor:pointer;" onclick="editar_noticia(<?php echo $row["idnoticia"]; ?>);" />
	 <img src="images/icons/page_delete.png" alt="Eliminar" title="Eliminar" style="cursor:pointer;" onclick="eliminar_noticia(<?php echo $row["idnoticia"]?>);"/>
      </td>
   </tr>
   <?php
   }
   ?>
   </tbody>
   </table>
   </div>
   <div class="pagination"><?php echo $paging->fetchNavegacion(); ?></div>
   <?php
}

function curriculums_detalle() {

   $id_noticia = (int) $_GET["id_noticia"];
   if ($id_noticia < 1) exit("ERROR");

   $query = "SELECT * FROM noticias WHERE idnoticia=$id_noticia";

   $db = new BD();
   $db->Conectar();

   $row = $db->Seleccionar($query, TRUE);

   $find = array("<", ">");
   $replace = array("&lt;", "&gt;");

   $contenido = str_replace($find, $replace, $row["contenido"]);
   $row["contenido"] = $contenido;
   echo json_encode($row);
}

function noticias_form() {

   $id_noticia = (int) $_GET["id"];
   $form_action = "abm.php?tabla=noticias";

   if ($id_noticia > 0) {
      $db = new BD(); $db->Conectar();
      $query = "SELECT * FROM noticias WHERE idnoticia=$id_noticia";
      $noticia = $db->Seleccionar($query, TRUE);

      if ($noticia !== FALSE) $form_action .= "&columna=idnoticia&idregistro=$id_noticia";
   }

?>
   <a href="javascript:void" style="position:absolute;">&nbsp;</a>
   <form id="form-detalle-noticia" action="<?php echo $form_action; ?>" method="post">
   <div class="ddu_campo">
      <span>Fecha:</span>
      <input type="text" name="fecha" value="<?php echo $noticia["fecha"] ?>" class="smallInput" />
   </div>
   <div class="ddu_campo">
      <span>Volanta:</span>
      <input type="text" name="volanta" value="<?php echo $noticia["volanta"]; ?>" class="smallInput" />
   </div>
   <div class="ddu_campo">
      <span>Título:</span>
      <input type="text" name="titulo" value="<?php echo $noticia["titulo"]; ?>" class="smallInput" />
   </div>
   <div class="ddu_campo_textarea">
      <span>Copete:</span>
      <textarea name="copete" class="smallInput" style="width:500px;"><?php echo $noticia["copete"]; ?></textarea>
   </div>
   <div class="ddu_campo_textarea">
      <span>Contenido:</span>
      <textarea name="contenido" class="smallInput" style="height:250px;width:500px;"><?php echo $noticia["contenido"]; ?></textarea>
   </div>
   <div class="ddu_campo_file">
      <span>Imágen:</span>
      <input type="file" name="FILE_Imagen_1" class="smallInput" />
   </div>
   <div class="ddu_campo">
      <span>Epígrafe:</span>
      <input type="text" name="epigrafe" value="<?php echo $noticia["epigrafe"]; ?>" class="smallInput" />
   </div>
   <div class="ddu_campo">
      <span>Fuente:</span>
      <input type="text" name="fuente" value="<?php echo $noticia["fuente"]; ?>" class="smallInput" />
   </div>
   <div class="ddu_campo_radio">
      <span>En Portada:</span>
      <span style="float:left;width:10px;">Si</span>
      <input type="radio" name="portada" class="smallInput" value="S" <?php echo $noticia["portada"] == "S" ? "checked=\"true\"" : ""; ?> />
      <span style="float:left;width:10px;">No</span>
      <input type="radio" name="portada" class="smallInput" value="N" <?php echo ($noticia["portada"] == "N" OR !is_array($noticia)) ? "checked=\"true\"" : ""; ?> />
   </div>
   <div class="ddu_campo_radio">
      <span>Destacada:</span>
      <span style="float:left;width:10px;">Si</span>
      <input type="radio" name="destacada" class="smallInput" value="S" <?php echo $noticia["destacada"] == "S" ? "checked=\"true\"" : ""; ?> />
      <span style="float:left;width:10px;">No</span>
      <input type="radio" name="destacada" class="smallInput" value="N" <?php echo ($noticia["destacada"] == "N" OR !is_array($noticia)) ? "checked=\"true\"" : ""; ?> />
   </div>
   </form>
   <script type="text/javascript">

      $(document).ready(function() {
          
         dp_options = { dateFormat: "yy-mm-dd" };

	 $("#form-detalle-noticia input[name='fecha']").datepicker(dp_options);

	 $("#form-detalle-noticia").ajaxForm(function(res) {
	    if (res.length > 0) {
	       alert("Noticia actualizada exitosamente");
	       actualizar_noticias_listado();
	    }
	 });

      });

   </script>
<?php
}

?>
