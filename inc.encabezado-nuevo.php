<?php
if (!$_SESSION["usuario"]) {
    header("Location: index.php");
}
ini_set("display_errors", FALSE);
//ini_set("default_charset", "latin1");
ini_set("default_charset", "utf-8");
$getID = $_GET['id'];

//header("Content-Type: text/html; charset=UTF-8");

/*
  error_reporting(E_ALL);
  ini_set("display_errors", FALSE);
 */
//echo $_SESSION["strict_menu"] ;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <?php /* <meta http-equiv="Content-Type" content="text/html;charset=utf-8" /> */ ?>
        <title>Gestión Consultores</title>
        <link rel="stylesheet" type="text/css" href="css/960.css" />
        <link rel="stylesheet" type="text/css" href="css/reset.css" />
        <link rel="stylesheet" type="text/css" href="css/text.css" />
        <link rel="stylesheet" type="text/css" href="css/extra.css" />
        <link rel="stylesheet" type="text/css" href="css/blue.css" />
        <link rel="stylesheet" type="text/css" href="css/shadowbox.css" />
        <link rel="stylesheet" type="text/css" href="css/jquery-ui-1.8.10.custom.css" />
        <link rel='stylesheet' type='text/css' href='css/fullcalendar.css' />
        <link rel='stylesheet' type='text/css' href='css/redmond/ui.css' />

        <?php /*
          <script language="JavaScript" type="text/javascript" src="scripts/correctPNG.js"></script>
          <!--[if lt IE 7]>
          <script language="JavaScript">
          window.attachEvent("onload", correctPNG);
          </script>
          <![endif]-->
         */ ?>

        <script type="text/javascript" src="scripts/jquery.js"></script>
        <!--script type="text/javascript" src="scripts/jquery-ui-1.8.16.custom.min.js"></script-->
        <script type="text/javascript" src="scripts/jquery-ui-1.8.10.custom.js"></script>
        <script type="text/javascript" src="scripts/jquery.form.js"></script>
        <script type="text/javascript" src="scripts/sistema.js"></script>
        <script src="scripts/highchart/highcharts.js" type="text/javascript"></script>
        <script src="scripts/highchart/modules/exporting.js" type="text/javascript"></script>
        <script type="text/javascript" src="scripts/general.js"></script>
        <script type="text/javascript" src="scripts/instant.js"></script>
        <script type="text/javascript" src="scripts/ajax.php"></script>

        <?php if ($seccion != 'reclamos'): ?>
            <script type="text/javascript" src="scripts/<?php echo $seccion; ?>.js"></script>
        <?php endif; ?>
        <script>
            $(document).ready(function () {
                iniciar();	
                /*
                  var availableTags = <?php include("feed.usuarias.autocomplete.php"); ?>; 
                  $(".labelsUsuaria").autocomplete({
                          source: availableTags
                  });
                 */
		
            });
	
        </script>	

        <style>
            .ui-autocomplete {
                max-height: 150px;
                overflow-y: auto;
                /* prevent horizontal scrollbar */
                overflow-x: hidden;
                /* add padding to account for vertical scrollbar */
                padding-right: 20px;
            }
            /* IE 6 doesn't support max-height
             * we use height instead, but this forces the menu to always be this tall
            */
            * html .ui-autocomplete {
                height: 150px;
            }
        </style>
        <?php
        if ($seccion == "escritorio") {
            ?>

            <script type="text/javascript" src="scripts/pedidos.js"></script>
            <script type="text/javascript" src="scripts/curriculums.js"></script>

            <?php
        } elseif ($seccion == "agenda" || $seccion == 'reclamos') {
            ?>
            <script type="text/javascript" src="scripts/agenda.js"></script>
            <script type="text/javascript" src="scripts/curriculums.js"></script>
            <?php
        }
        ?>

        <style type="text/css">

            .portlet-header span img {
                margin:0 10px 0 5px;
            }

            .portlet-header span {
                cursor:default;
            }

            div.menu_escritorio {
                width:90px;
                height:23px;
                float:right;
                margin-right:20px;
                margin-top:-3px;
            }

            div.menu_escritorio a {
                background-image:url("images/menu/opciones_small.png");
                background-repeat:no-repeat;
                padding-left:30px;
                font-weight:bold;
                font-style:italic;
                line-height:23px;
                text-decoration:none;
                color:white;
                display:block;
            }

            div.menu_escritorio a:hover {
                text-decoration:underline;
            }

            a.custom_menu {
                background-repeat:no-repeat;
                padding-left:21px;
                cursor:pointer;
                float:right;
                margin-right:20px;
                font-size:10px;
                padding-top:2px;
            }



            div.dlg_column_left { float:left;width:360px; }
            div.dlg_column_right { float:left;margin-left:20px; }
            div.ddu_title { border-style:solid; border-width:0 0 1px 0; text-indent:5px; margin-bottom:10px;text-align:left; }
            div.ddu_title span { font-weight:bold;text-align:left; }
            div.ddu_campo { height:20px; margin:5px 0 0 5px; }
            div.ddu_campo_select { height:23px; margin:5px 0 0 5px;text-align:left; }
            div.ddu_campo_select select { width:253px;position:absolute; }
            div.ddu_campo_select span { width:100px; float:left;text-align:left; }
            div.ddu_campo_radio { height:22px; margin:10px 0 4px 5px; }
            div.ddu_campo_radio span { width:100px; float:left;margin-left:5px; }
            div.ddu_campo_radio input { width:10px; float:left; }
            div.ddu_campo_textarea { margin:5px 0 0 5px; }
            div.ddu_campo_textarea textarea { width:245px; }
            div.ddu_campo_textarea span { width:100px; float:left;text-align:left; }
            div.ddu_campo_file { height:25px; margin:10px 0 4px 5px; }
            div.ddu_campo_file span { width:100px; float:left;text-align:left; }
            div.ddu_campo_col_izq { float:left;width:190px; }
            div.ddu_campo_col_der { float:left;width:170px; }
            div.ddu_campo_col_der div.ddu_campo span { width:70px;margin-left:10px; }
            div.ddu_campo_col_der div.ddu_campo input { width:75px; }
            div.ddu_campo_col_izq div.ddu_campo input { width:75px; }
            div.ddu_campo_doble { width:360px; height:28px; }
            div.ddu_campo_comandos { width:360px;height:48px;margin-top:10px;padding-top:10px;text-align:right; }
            div.ddu_campo input { width:245px; }
            div.ddu_campo span { width:100px; float:left;text-align:left; }
            div.ddu_campo_g{ width:596px; height:69px; margin:0px 0 0 5px; }
            .ddu_campo_g1{ width:589px; height:68px; overflow:none; }
            .ddu_campo-grilla { height:367px; margin:5px 0 0 0px; }
            .texto-grillas{ height:16px; background-color:#efefef; color:#cc6600; font-weight:bold; float:left; padding:2px 0px 2px 2px; width:145px; border-right:1px solid #CCC; font-family:Arial, Helvetica, sans-serif; font-size:11px;}

            .texto-grilla-gris{ height:16px;  color:#555;  float:left; padding:2px 2px 2px 5px;width:90px; border-right:1px solid #CCC; border-left:1px solid #CCC; font-family:Arial, Helvetica, sans-serif; font-size:11px;}
            .fila{ float:left;  height:20px; border:1px solid #CCC; width:755px;}

            .columna-izq{
                float:left;
                width:374px;
                margin-top:10px;
            }



            /* seccion curriculums dialog asignacioes */
            fieldset { border:#eeeeee 1px groove;padding:10px 0 10px 0; }
            fieldset legend { padding:0 5px 0 5px; }
            form label { float:left;width:80px;margin-left:30px;height:40px; }
            form fieldset div.fila { width:100%;margin-bottom:5px;border-width:0!important; }
            form fieldset div.fila_comandos { text-align:right;margin:5px 22px 0 0; }
            form fieldset input { width:120px;float:left; }
            form fieldset select { width:128px;float:left; }

            /* seccion opciones */
            #accordion_opciones td.label{ width:130px;text-align:center; }
            #accordion_opciones td button{ width:130px; }


        </style>

    </head>
    <body>
        <div id="wrapper" class="container_16">
            <div id="logo" class="grid_8">Gestión Consultores</div>
            <div class="grid_8">
                <div id="user_tools">
                    <span>Fecha: <?php echo date('d-m-Y'); ?> | Hora ingreso: <?php echo $_SESSION['hora']; ?>
                        <a><?php echo $_SESSION['persona']['apellido'] . ", " . $_SESSION['persona']['nombres']; ?></a>
                        <a href="logout.php" onclick="return confirm('&iquest;Desea salir del Sistema?');">Salir</a>
                    </span>
                </div>
            </div>
            <div id="header" class="grid_16">
                <?php require_once("inc.menu.reclamos.php"); ?>
            </div>
            <?php require_once("inc.tabs-nuevo.php"); ?>
            <div id="content" class="grid_16">
                <?php
                if (isset($titulo)) {
                    ?>
                    <div class="clear"></div>


                    <div id="portlets">
                        <div class="grid_9" style="height:60px;"><h1 class="<?php echo "{$seccion}_edit"; ?>"><img src="images/menu/reclamos30.png"/><?php echo $titulo; ?></h1></div>
                        <div class="clear"></div>


                        <div class="portlet2-header">
                            <? if ($seccion != "TicketsP") { ?>
                                <span style="float:left;margin-left:10px;">
                                    <img width="16" height="16" alt="Titulo" src="images/glass.png" />
                                    Tickets
                                </span>
                            <? } ?>


                            <span style=" float:right; margin-right:15px;display:block; width:420px; margin-top:0px;">
                                <div style="width:200px; float:left;">
                                </div>
                                <div style="margin-left: 200px">
                                    <?php if (!($_SESSION["is_agente"] || $_SESSION["is_productor"])): ?>

                                        <? if (!empty($getID) && $seccion != 'TicketsP') { ?>
                                            <div style="float:right;margin-right: 40px; ">
                                                <a class="edit_inline"  href="javascript:void(0);" onclick="generateNewTicket();">   Agregar Nuevo</a>
                                                <a id="a_polizas_delete" href="javascript:void(0);" onclick="deleteTicket('<? echo $getID ?>')" class="delete_inline delete" style="margin-left:10px;">  Eliminar</a>
                                            </div>
                                            <?
                                        }
                                        if ($seccion == 'TicketsP') {
                                            ?>
                                            <div style="float:right;margin-right: 40px; ">
                                                <a class="edit_inline"  href="javascript:void(0);" onclick="generateNewTicket();">   Agregar Nuevo</a>
                                            </div>
                                            <?
                                        }
                                        ?>
                                    <?php endif; ?>
                                </div>
                            </span>






                            <span style="float:right;margin-right:30px;">
                                <?php
                                $evento = "";
                                $nombre_comando = "";

                                if (isset($comando)) {

                                    $evento = " onclick='{$comando["onclick"]}' ";
                                    $nombre_comando = $comando["titulo"];
                                }

                                if (!isset($nombre_comando))
                                    $nombre_comando = "Comando de Sección";
                                $evento_comando = "";
                                if (isset($evento_comando))
                                    $evento_comando = " onclick='$evento_comando' ";
                                ?>
                                <a class="edit_inline" style="margin-left:10px;" href="#" <?php echo $evento; ?>><?php echo $nombre_comando; ?></a>






                            </span>
                            <div style="clear:both;">


                            </div>



                        </div>

                        <div class="grid_15">
                            <div class="portlet-content">
                                <?php
                            }

                            $m_lIDRegistro = is_numeric($_REQUEST["idregistro"]) ? $_REQUEST["idregistro"] : 0;
                            $m_lIDSeccion = is_numeric($_REQUEST["idseccion"]) ? $_REQUEST["idseccion"] : 1;
                            $m_lIDPostulante = is_numeric($_REQUEST["idpostulante"]) ? $_REQUEST["idpostulante"] : 0;

                            $m_sURL = $_SERVER["REQUEST_URI"];

                            $aMenues[0][0] = "usuarios";
                            $aMenues[1][0] = "clientes";
                            $aMenues[2][0] = "postulantes";
                            $aMenues[3][0] = "noticias";
                            $aMenues[4][0] = "agenda";
                            $aMenues[5][0] = "trayectoria";
                            $aMenues[6][0] = "ofertas";
                            $aMenues[7][0] = "curriculums";
                            $aMenues[8][0] = "*";

                            for ($lJ = 0; $lJ < count($aMenues); $lJ++) {
                                if ($aMenues[$lJ][0] == "*") {
                                    $aMenues[$lJ][1] = true;
                                    break;
                                } else {
                                    if (strpos(basename($m_sURL), $aMenues[$lJ][0]) > 0) {
                                        $aMenues[$lJ][1] = true;
                                        break;
                                    }
                                }
                            }
                            ?>
