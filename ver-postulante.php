<?php
	session_start();
	include("clases/framework-1.0/class.bd.php");
	include("includes/funciones.php");

	$m_lIDRegistro = is_numeric($_REQUEST["idregistro"]) ? $_REQUEST["idregistro"] : 0;
	if($m_lIDRegistro > 0)
	{
		$sSQL = "SELECT p.PerApellido, p.PerNombres, p.PerTipoDoc, p.PerDocumento, p.PerFechaNac, ";
		$sSQL .= "p.PerCuitCuil, p.PerNumeroCC, p.PerNacionalidad, ec.nombre AS PerECivil, ";
		$sSQL .= "p.PerTelefono, p.PerEmail, s.nombre AS PerSexo, ";
		$sSQL .= "p.PerHijos, p.PerAcargo, p.PerCodPos, p.PerDomicilio, ";
		$sSQL .= "p.PerBarrio, p.PerCelular, p.PerTelMensaje, ";
		$sSQL .= "p.PerPais, PerProvincia, PerLocalidad, ";
		$sSQL .= "PerAceptaEve, PerDispViaje, PerDispCiudad, ";
		$sSQL .= "PerPrefLaboral, PerAspPuesto, PerOtrosConoc, ";
		$sSQL .= "p.PerIngles, nih.nombre AS InglesHabla, nie.nombre AS InglesEscribe  ";

		$sSQL .= "FROM persona p ";
		$sSQL .= "INNER JOIN _sexos s ON p.PerSexo = s.idsexo ";
		$sSQL .= "LEFT JOIN _estadocivil ec ON p.PerECivil = ec.idestadocivil ";
		$sSQL .= "LEFT JOIN _nivel_idioma nih ON nih.idnivel = PerInglesHabla ";
		$sSQL .= "LEFT JOIN _nivel_idioma nie ON nie.idnivel = PerInglesEscribe ";
		
		$sSQL .= "WHERE PerNro = " . $m_lIDRegistro;
		
		//echo $sSQL;
		
		$cBD = new BD();
		$aRegistro = $cBD->Seleccionar($sSQL, true);
	}
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?php print SITIO; ?></title>
<script language="JavaScript" type="text/javascript" src="scripts/ajax.php"></script>
<script language="JavaScript" type="text/javascript" src="scripts/general.js"></script>
<link href="estilos/general.css" rel="stylesheet" type="text/css">

<script src="scripts/jquery.js"></script>
<script src="scripts/jquery.highlight-3.js"></script>
</head>

<?php if (strlen($_SESSION["Palabras"]) > 3) { ?>
<body onLoad="void($('#highlight-plugin').removeHighlight().highlight(' <?php print($_SESSION["Palabras"]); ?>'));">
<?php } else { ?>
<body>
<?php } ?>
<table width="800" border="0" cellspacing="0" cellpadding="0" align="center" class="panel" id="highlight-plugin">
            <tr>
              <td height="50" class="listado-texto"><span style="text-transform:capitalize;font-size: 18px; font-weight: bold;"><?php print($aRegistro["PerApellido"]); ?>, <?php print($aRegistro["PerNombres"]); ?></span></td>
              <td colspan="2" align="center" class="listado-texto"><a href="imprimir-postulante.php?idpostulante=<?php print $m_lIDRegistro; ?>" target="_blank"><img src="images/btn-print.png" alt="Imprimir" width="24" height="24" border="0"> </a>
			  
<!--			  <a href="javascript:">
			  <img src="images/btn-resaltar.jpg" alt="Resaltar" width="24" height="24" border="0">			  </a>			 
-->			   </td>
  </tr>
            <tr>
              <td colspan="3" class="detalle-seccion">DATOS PERSONALES </td>
  </tr>
          <tr>
            <td width="50%" class="informe-texto"><strong>Documento:</strong> <?php print($aRegistro["PerTipoDoc"]); ?> <?php print($aRegistro["PerDocumento"]); ?></td>
            <td width="340" colspan="2" class="informe-texto"><strong>Pa&iacute;s:</strong> <?php print $aRegistro["pais"]; ?></td>
          </tr>
          <tr>
            <td class="informe-texto"><strong><?php print($aRegistro["PerCuitCuil"]); ?>:</strong> <?php print($aRegistro["PerNumeroCC"]); ?></td>
            <td colspan="2" class="informe-texto"><strong>Provincia:</strong> <?php print $aRegistro["PerProvincia"]; ?></td>
          </tr>
          <tr>
            <td class="informe-texto"><strong>Fecha de Nacimiento: </strong><?php print(date("d-m-Y", strtotime($aRegistro["PerFechaNac"]))); ?></td>
            <td width="340" class="informe-texto"><strong>Localidad:</strong> <?php print $aRegistro["PerLocalidad"]; ?></td>
            <td width="340" class="informe-texto"><strong>C&oacute;digo Postal:</strong> <?php print $aRegistro["PerCodPos"]; ?></td>
          </tr>
          <tr>
            <td class="informe-texto"><strong>Sexo:</strong> <?php print $aRegistro["PerSexo"]; ?></td>
            <td colspan="2" valign="top" class="informe-texto"><strong>Domicilio:</strong> <?php print $aRegistro["PerDomicilio"]; ?></td>
          </tr>
          <tr>
            <td class="informe-texto"><strong>Nacionalidad:</strong> <?php  print $aRegistro["PerNacionalidad"]; ?></td>
            <td class="informe-texto"><strong>Barrio:</strong> <?php ($aRegistro["PerBarrio"]) ? print $aRegistro["barrio"] : print NODATA; ?></td>
            <td class="informe-texto">&nbsp;</td>
          </tr>
          <tr valign="top">
            <td class="informe-texto"><strong>Estado Civil:</strong>  <?php print $aRegistro["PerECivil"]; ?></td>
            <td class="informe-texto"><strong>Telefono:</strong> <?php ($aRegistro["PerTelefono"]) ? print $aRegistro["PerTelefono"] : print NODATA; ?></td>
            <td class="informe-texto"><strong>Celular:</strong> <?php ($aRegistro["PerCelular"]) ? print $aRegistro["PerCelular"] : print NODATA; ?><br>    
            <strong>Mensajes:</strong>              <?php ($aRegistro["PerTelMensaje"]) ? print $aRegistro["PerTelMensaje"] : print NODATA; ?></td>
          </tr>
          <tr>
            <td class="informe-texto"><strong>Cantidad de hijos:</strong> <?php print $aRegistro["PerHijos"]; ?></td>
            <td colspan="2" class="informe-texto">&nbsp;</td>
          </tr>
          <tr>
            <td class="informe-texto"><strong>Personas a cargo: </strong> <?php ($aRegistro["PerAcargo"] == "S") ? print "S&iacute;" : print "No"; ?></td>
            <td colspan="2" valign="top" class="informe-texto"><strong>Email:</strong> <a href="mailto:<?php print $aRegistro["PerEmail"]; ?>"><?php print $aRegistro["PerEmail"]; ?></a></td>
          </tr>
          <tr>
            <td colspan="3" class="informe-texto">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="3" class="detalle-seccion">GENERALES </td>
          </tr>
          <tr>
            <td class="informe-texto"><strong>Disponibilidad:</strong> <?php print($aRegistro["disponibilidad"]); ?></td>
            <td colspan="2" class="informe-texto"><strong>Aceptar&iacute;a puestos eventuales: </strong>
            <?php ($aRegistro["PerAceptaEve"]) ? print "S&iacute;" : print "No"; ?></td>
          </tr>
          <tr>
            <td class="informe-texto"><strong>Disponibilidad para viajar al Interior o Exterior del pa&iacute;s:</strong>
                <?php ($aRegistro["PerDispViaje"]) ? print "S&iacute;" : print "No"; ?>            </td>
            <td colspan="2" class="informe-texto"><strong>Disponibilidad para radicarse en otra ciudad:</strong>
                <?php ($aRegistro["PerDispCiudad"]) ? print "S&iacute;" : print "No"; ?>           </td>
          </tr>
          <tr>
            <td colspan="3" class="informe-texto"><strong>Preferencia laborales: </strong>
              <?php ($aRegistro["PerPrefLaboral"]) ? print $aRegistro["PerPrefLaboral"] : print NODATA; ?>            </td>
          </tr>
          <tr>
            <td colspan="3" class="informe-texto"><strong>Aspiraciones para pr&oacute;ximo puesto: </strong><?php ($aRegistro["PerAspPuesto"]) ? print $aRegistro["PerAspPuesto"] : print NODATA; ?></td>
          </tr>
          <tr>
            <td colspan="3" class="informe-texto">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="3" class="detalle-seccion">IDIOMAS / CONOCIMIENTOS </td>
          </tr>
          <tr>
            <td class="informe-texto"><strong>Ingl&eacute;s:</strong> 
			<?php 	if (!$aRegistro["PerIngles"])
					{
						print "No";
					} else {
						print "Si (Habla: ".$aRegistro["InglesHabla"];
						print " - Escribe: ". $aRegistro["InglesEscribe"].")";	
					}		
			?>
			
			</td>
            <td colspan="2" class="informe-texto"><strong>Otros Conocimientos:</strong>              <?php ($aRegistro["PerOtrosConoc"]) ? print $aRegistro["PerOtrosConoc"] : print NODATA; ?></td>
          </tr>
          <tr>
            <td colspan="3" class="informe-texto">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="3" class="detalle-seccion">ESTUDIOS </td>
          </tr>
          <tr>
            <td colspan="3">
			<table width="98%" border="0" align="left" cellpadding="0" cellspacing="0" class="list">
			<tr>
				<td width="200"><strong>Nivel</strong></td>
				<td width="200"><strong>T&iacute;tulo</strong></td>
				<td width="80"><strong>Ingreso</strong></td>
				<td width="80"><strong>Egreso</strong></td>
				<td width="200"><strong>Instituci&oacute;n</strong></td>
				<td><strong>Observaciones</strong></td>
			</tr>
			<?php 
		  		$lRegistros = 0;

			 	$sSQL = "SELECT e.EtuNro, ee.nombre AS EtuEstado, e.EtuTitulo, e.EtuIngreso, e.EtuEgreso, ";
			 	$sSQL .= "e.EtuInstitucion, ne.NEsDescrip, ne.NEsNro ";
				
				$sSQL .= "FROM estudio e ";
				$sSQL .= "LEFT JOIN nivelestudio ne ON e.NEsNro = ne.NEsNro ";
				$sSQL .= "LEFT JOIN _estado_estudio ee ON ee.idestado = e.EtuFinalizado ";
				$sSQL .= "WHERE PerNro = ".$m_lIDRegistro." ";
				$sSQL .= "ORDER BY EtuIngreso DESC ";
				
				$cBD = new BD();
				$oResultadoEstudio = $cBD->Seleccionar($sSQL);
				while($aEstudio = $cBD->RetornarFila($oResultadoEstudio))
				{
		  
?>

			<tr valign="top">
                <td width="200"><?php print($aEstudio["NEsDescrip"]); ?> (<?php print($aEstudio["EtuEstado"]); ?>) </td>
              <td width="200"><?php print($aEstudio["EtuTitulo"]); ?>&nbsp;</td>
              <td width="100"><?php print(date("d/m/y", strtotime($aEstudio["EtuIngreso"]))); ?></td>
              <td width="100"><?php print(date("d/m/y", strtotime($aEstudio["EtuEgreso"]))); ?></td>
              <td><?php print($aEstudio["EtuInstitucion"]); ?>&nbsp;</td>
              <td><?php print($aEstudio["EtuObservaciones"]); ?>&nbsp;</td>
			  </tr>
		  
          <?php
			 		$lRegistros++;
				}
				if($lRegistros == 0)
				{
			 ?>
          <tr>
            <td colspan="6" class="informe-texto">No hay estudios asociadas a este postulante.</td>
          </tr>
			 <?php } ?>
			</table>			</td>
          </tr>
          <tr>
            <td colspan="3" class="informe-texto">&nbsp;</td>
          </tr>
		  


          <tr>
            <td colspan="3" class="detalle-seccion">EXPERIENCIA LABORAL</td>
          </tr>
          <tr>
            <td colspan="3">
			<table width="98%" border="0" align="left" cellpadding="0" cellspacing="0" class="list">
			<tr>
				<td><strong>Puesto</strong></td>
				<td><strong>Empresa</strong></td>
				<td><strong>Egreso</strong></td>
				<td><strong>&Aacute;rea</strong></td>
				<td><strong>Tareas Realizadas</strong></td>
			</tr>
<?php 
		  		$lRegistros = 0;
			 	$sSQL = "SELECT exp.PueUltimaEmpresa, pue.PueNom, a.AreNom, exp.PueFechaBaja, exp.PueDescPuesto ";
				$sSQL .= "FROM datospuesto exp ";
				$sSQL .= "INNER JOIN puesto pue ON pue.PueNro = exp.PueNro ";
				$sSQL .= "INNER JOIN area a ON a.AreNro = exp.AreNro ";
				$sSQL .= "WHERE exp.PerNro = ".$m_lIDRegistro." ";
				$sSQL .= "ORDER BY PueFechaBaja DESC ";

				$cBD = new BD();
				$oResultadoEstudio = $cBD->Seleccionar($sSQL);
				while($aEstudio = $cBD->RetornarFila($oResultadoEstudio))
				{
?>
			<tr valign="top">
				<td width="250"><?php print($aEstudio["PueNom"]); ?></td>
				<td width="200"><?php print($aEstudio["PueUltimaEmpresa"]); ?>&nbsp;</td>
				<td width="100"><?php print(date("m/Y", strtotime($aEstudio["PueFechaBaja"]))); ?></td>
				<td><?php print($aEstudio["AreNom"]); ?></td>
				<td><?php print($aEstudio["PueDescPuesto"]); ?>&nbsp;</td>
			  </tr>
		  
          <?php
			 		$lRegistros++;
				}
				if($lRegistros == 0)
				{ 
			 ?>
          <tr>
            <td colspan="5" class="informe-texto">No hay experiencias asociadas a este postulante.</td>
          </tr>
			 <?php } ?>
			</table>			</td>
          </tr>
		  
          <tr>
            <td colspan="3" class="informe-texto">&nbsp;</td>
          </tr>
		  


          <tr>
            <td colspan="3" class="detalle-seccion">Trayectoria del postulante </td>
          </tr>

		<tr>
            <td colspan="3">
			<!-- inicio tabla trayectoria-->
			<table width="98%" border="0" cellpadding="0" cellspacing="0" class="list">
			<tr align="center">
				<td colspan="4"><strong>PEDIDO</strong></td>
				<td colspan="4"><strong>POSTULANTE</strong></td>
			</tr>
			<tr>
				<td><strong>Ref</strong></td>
				<td><strong>Usuaria</strong></td>
				<td><strong>Pedido</strong></td>
				<td><strong>Situaci&oacute;n</strong></td>
				<td><strong>Fecha Post.</strong></td>
				<td><strong>Situaci&oacute;n</strong></td>
				<td><strong>Historico</strong></td>
				<td><strong>Detalle</strong></td>
			</tr>
<?php 
		  		$lRegistros = 0;
				$sSQL = "SELECT o.OfeReferencia, o.OfeTitulo, eo.EOfeValor, ";
				$sSQL .="post.PosAsociado, post.PosFecha, pe.PEsDescrip, ";
				$sSQL .="post.PosEstFecha, post.PosDetalle, ";
				//$sSQL .="MiembroEmpresa.EmpNro, ";
				$sSQL .="c.CliRsocial  ";
				$sSQL .="FROM postulacion post ";
				$sSQL .="LEFT OUTER JOIN postulacionestado pe ON post.PEsNro = pe.PEsNro ";
				$sSQL .="INNER JOIN oferta  o ON post.OfeNro = o.OfeNro ";
				$sSQL .="INNER JOIN estadooferta eo ON o.OfeNro = eo.OfeNro ";
				//$sSQL .="INNER JOIN MiembroEmpresa ON Oferta.MEmpNro = MiembroEmpresa.MEmpNro ";
				$sSQL .="LEFT JOIN ofertascliente oc ON oc.OfeNro = o.OfeNro ";
				$sSQL .="LEFT JOIN cliente c ON c.CliNro = oc.CliNro ";
				$sSQL .="WHERE post.PerNro = " . $m_lIDRegistro. " ";
				//$sSQL .="And MiembroEmpresa.EmpNro = " & Session("EmpNro") & " ";
				$sSQL .="ORDER BY post.PosEstFecha DESC ";
				//echo $sSQL;								
				$cBD = new BD();
				$oResultadoEstudio = $cBD->Seleccionar($sSQL);
				while($aRegistro = $cBD->RetornarFila($oResultadoEstudio))
				{
		  
?>          <tr>
				<td><?php print($aRegistro["OfeReferencia"]); ?></td>
				<td><?php print($aRegistro["CliRsocial"]); ?></td>
				<td><?php print($aRegistro["OfeTitulo"]); ?></td>
				<td><?php print($aRegistro["EOfeValor"]); ?>&nbsp;</td>
				<td><?php print(date("d/m/Y", strtotime($aRegistro["PosFecha"]))); ?></td>
				<td><?php print($aRegistro["PEsDescrip"]); ?>&nbsp;</td>
				<td><?php print(date("d/m/Y", strtotime($aRegistro["PosEstFecha"]))); ?></td>
				<td><?php print($aRegistro["PosDetalle"]); ?>&nbsp;</td>
          </tr>
		  
          <?php
			 		$lRegistros++;
				}
				if($lRegistros == 0)
				{ $sSQL;
			 ?>
          <tr>
            <td colspan="8" class="informe-texto">No hay trayectoria asociada a este postulante.</td>
          </tr>
			 <?php } ?>
			</table>		  
			
			<!-- fin tabla trayectoria-->		  </td>
          </tr>		  
        </table>

</body>
</html>
