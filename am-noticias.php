<?php
	session_start();
	include("clases/flxajax-0.2.2/flxajax.class.php4");
	include("clases/flxajax-0.2.2/flxajax_client.class.php4");
	include("clases/flxajax-0.2.2/flxajax_server.class.php4");
	include("clases/framework-1.0/class.bd.php");
	include("includes/funciones.php");
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?php print SITIO; ?></title>
<script type="text/javascript" src="blueshoes-4.5/javascript/core/lang/Bs_Misc.lib.js"></script>
<script type="text/javascript" src="blueshoes-4.5/javascript/core/html/Bs_HtmlUtil.lib.js"></script>
<script type="text/javascript" src="blueshoes-4.5/javascript/core/form/Bs_FormFieldSelect.class.js"></script>
<script type="text/javascript" src="blueshoes-4.5/javascript/components/tabset/Bs_TabSet.class.js"></script>
<script type="text/javascript" src="blueshoes-4.5/javascript/components/editor/Bs_Editor.class.js"></script>
<script type="text/javascript" src="blueshoes-4.5/javascript/components/editor/lang/en.js"></script>
<script type="text/javascript" src="blueshoes-4.5/javascript/components/toolbar/Bs_ButtonBar.class.js"></script>
<script type="text/javascript" src="blueshoes-4.5/javascript/components/toolbar/Bs_Button.class.js"></script>
<script type="text/javascript" src="blueshoes-4.5/javascript/components/resizegrip/Bs_ResizeGrip.class.js"></script>
<script language="JavaScript" type="text/javascript">
	if(moz)
		document.writeln("<link rel=\"stylesheet\" href=\"blueshoes-4.5/javascript/components/toolbar/win2k_mz.css\">");
	else
		document.writeln("<link rel=\"stylesheet\" href=\"blueshoes-4.5/javascript/components/toolbar/win2k_ie.css\">");
</script>
<link href="blueshoes-4.5/javascript/components/tabset/default.css" rel="stylesheet" type="text/css">
<link href="estilos/general.css" rel="stylesheet" type="text/css">
</head>

<body>
<?php
	include("inc.encabezado.php");
	include("inc.am-noticias.php");
	include("inc.pie.php");
?>
</body>
</html>
