<?php
	if($m_lIDRegistro > 0)
	{
		$sSQL = "SELECT p.* ";
		$sSQL .= "FROM persona p ";
		$sSQL .= "WHERE p.PerNro = " . $m_lIDRegistro;
		$cBD = new BD();
		$aRegistro = $cBD->Seleccionar($sSQL, true);
	} else {
		$aRegistro["fecha_alta"] = date("Y-m-d");
		$aRegistro["PerTipoDoc"] = "DNI";
		$aRegistro["PerPais"] = "Argentina";
	}
?>

<table width="780" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="30" class="encabezado-titulo-bg"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="20"><img src="images/espacio.gif" width="1" height="1"></td>
        <td class="encabezado-titulo-texto">Alta y modificaci&oacute;n de Curr&iacute;culums </td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><img src="images/espacio.gif" width="1" height="20"></td>
  </tr>
</table>
<table width="580" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="20"><img src="images/formulario-encabezado-inicio.jpg" width="20" height="37"></td>
        <td class="formulario-encabezado-bg">Datos del Postulante </td>
        <td width="20"><img src="images/formulario-encabezado-final.jpg" width="20" height="37"></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="4" class="formulario-contenido-inicio"><img src="images/espacio.gif" width="1" height="1"></td>
        <td><form action="abm.php?tabla=persona&columna=PerNro&idregistro=<?php print($m_lIDRegistro); ?>&archivo=1&url=<?php print($m_sURL); ?>" method="post" name="frmRegistro" id="frmRegistro" onsubmit="corrigeFecha('PerFechaNac'); return validarExtension(this);"  enctype="multipart/form-data">
 
          <table width="100%" border="0" cellspacing="0" cellpadding="4">
            <tr>
              <td colspan="2" class="listado-encabezado-texto">Datos Personales</td>
              </tr>
          <tr>
            <td class="formulario-etiquetas">Apellido:</td>
            <td width="340"><input name="PerApellido" type="text" class="formulario-textbox" id="PerApellido" style="width: 300px;" value="<?php print($aRegistro["PerApellido"]); ?>" maxlength="64" maxlenght="64" /></td>
          </tr>
          <tr>
            <td class="formulario-etiquetas">Nombres:</td>
            <td width="340"><input name="PerNombres" type="text" class="formulario-textbox" id="PerNombres" style="width: 300px;" value="<?php print($aRegistro["PerNombres"]); ?>" maxlength="64" maxlenght="64" /></td>
          </tr>
          <tr>
            <td class="formulario-etiquetas">Documento:</td>
            <td class="formulario-textbox"><select name="PerTipoDoc" id="PerTipoDoc" style="width: 120px;">
              <?php
					$sSQL = "SELECT TipoDocNombre, TipoDocNombre FROM tipodocumento  ";
					$sSQL .= "ORDER BY TipoDocNombre ASC ";
					print(GenerarOptions($sSQL, $aRegistro["PerTipoDoc"]));
			  ?>
                        </select>
             &nbsp;&nbsp; &nbsp;&nbsp; <input name="PerDocumento" type="text" class="formulario-textbox" id="PerDocumento" style="width: 160px;" value="<?php print($aRegistro["PerDocumento"]); ?>" maxlength="8" maxlenght="5" /></td>
          </tr>
          <tr>
            <td class="formulario-etiquetas">C.U.I.T./C.U.I.L.:</td>
            <td class="formulario-textbox"><select name="PerCuitCuil" id="PerCuitCuil" style="width: 120px;">
			<option value="CUIT" <?php ($aRegistro["PerCuitCuil"] == "CUIT") ? print "selected" : print ""; ?>>C.U.I.T.</option>
			<option value="CUIL" <?php ($aRegistro["PerCuitCuil"] == "CUIL") ? print "selected" : print ""; ?>>C.U.I.L.</option>
              </select>
              &nbsp;&nbsp; &nbsp;&nbsp;
              <input name="PerNumeroCC" type="text" class="formulario-textbox" id="PerNumeroCC" style="width: 160px;" value="<?php print($aRegistro["PerNumeroCC"]); ?>" maxlength="13" maxlenght="5" /></td>
          </tr>
          <tr>
            <td class="formulario-etiquetas">Fecha de Nacimiento:</td>
            <td class="formulario-textbox"><input name="PerFechaNac" type="text" class="formulario-textbox" id="PerFechaNac" style="width: 120px;" maxlenght="64" value="<?php print(date("d-m-Y", strtotime($aRegistro["PerFechaNac"]))); ?>" />
              dd-mm-aaaa </td>
          </tr>
          <tr>
            <td class="formulario-etiquetas">Sexo:</td>
            <td width="340"><select name="PerSexo" id="PerSexo" style="width: 120px;">
              <?php
					$sSQL = "SELECT idsexo, nombre FROM _sexos  ";
					$sSQL .= "WHERE idsexo > 0 ORDER BY idsexo ASC ";
					print(GenerarOptions($sSQL, $aRegistro["PerSexo"]));
			  ?>
            </select></td>
          </tr>
          <tr>
            <td class="formulario-etiquetas">Nacionalidad:</td>
            <td><select name="PerNacionalidad" id="PerNacionalidad" style="width: 300px;">
                <?php
					$sSQL = "SELECT nombre, nombre FROM _nacionalidades  ";
					$sSQL .= "ORDER BY orden ASC, idnacionalidad ASC ";
					print(GenerarOptions($sSQL, $aRegistro["PerNacionalidad"]));
			  ?>
                        </select></td>
          </tr>
          <tr>
            <td class="formulario-etiquetas">Estado Civil:</td>
            <td width="340"><select name="PerECivil" id="PerECivil" style="width: 300px;">
              <?php
					$sSQL = "SELECT nombre, nombre FROM _estadocivil  ";
					$sSQL .= "ORDER BY orden ";
					print(GenerarOptions($sSQL, $aRegistro["PerECivil"]));
			  ?>
            </select></td>
          </tr>
          <tr>
            <td class="formulario-etiquetas">Cantidad de hijos:</td>
            <td class="formulario-textbox"><input name="PerHijos" type="text" class="formulario-textbox" id="PerHijos" style="width: 120px;" maxlenght="64" value="<?php print ($aRegistro["PerHijos"]); ?>" /></td>
          </tr>
          <tr>
            <td class="formulario-etiquetas">Personas a cargo: </td>
            <td><table width="100" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="25"><input name="PerAcargo" type="radio" value="1" <?php if($aRegistro["PerAcargo"] == "1") { ?>checked="checked" <?php } ?>/></td>
                  <td width="25" class="formulario-textbox">Si</td>
                  <td width="25"><input name="PerAcargo" type="radio" value="0" <?php if($aRegistro["PerAcargo"] == "0") { ?>checked="checked" <?php } ?>/></td>
                  <td class="formulario-textbox">No</td>
                </tr>
            </table></td>
          </tr>
          <tr>
            <td class="formulario-etiquetas">C&oacute;digo Postal:</td>
            <td class="formulario-textbox"><input name="PerCodPos" type="text" class="formulario-textbox" id="PerCodPos" style="width: 120px;" maxlenght="64" value="<?php print ($aRegistro["PerCodPos"]); ?>" /></td>
          </tr>
          <tr>
            <td class="formulario-etiquetas">Pa&iacute;s:</td>
            <td width="340"><select name="PerPais" id="PerPais" style="width: 300px;">
              <?php
					$sSQL = "SELECT PaiNom, PaiNom FROM pais  ";
					//$sSQL .= "ORDER BY PaiNom ASC ";
					print(GenerarOptions($sSQL, $aRegistro["PerPais"]));
			  ?>
            </select></td>
          </tr>
          <tr>
            <td class="formulario-etiquetas">Provincia:</td>
            <td width="340"><select name="PerProvincia" id="PerProvincia" style="width: 300px;" >
              <?php
					$sSQL = "SELECT PrvNom, PrvNom FROM provincia  ";
					$sSQL .= "ORDER BY PrvNom ASC ";
					print(GenerarOptions($sSQL, $aRegistro["PerProvincia"], true, "[Otra]"));
			  ?>
            </select></td>
          </tr>
          <tr>
            <td class="formulario-etiquetas">Localidad:</td>
            <td width="340"><input name="PerLocalidad" type="text" class="formulario-textbox" id="PerLocalidad" style="width: 300px;" value="<?php print($aRegistro["PerLocalidad"]); ?>" maxlenght="512" /></td>
          </tr>
          <tr>
            <td class="formulario-etiquetas">Zona:</td>
            <td><select name="ZonNro" id="ZonNro" style="width: 300px;">
                <?php
					$sSQL = "SELECT ZonNro, ZonDescrip FROM zona  ";
					$sSQL .= "ORDER BY ZonDescrip ASC ";
					print(GenerarOptions($sSQL, $aRegistro["ZonNro"]));
			  ?>
            </select></td>
          </tr>
          <tr>
            <td class="formulario-etiquetas">Barrio:</td>
            <td><input name="PerBarrio" type="text" class="formulario-textbox" id="PerBarrio" style="width: 300px;" value="<?php print($aRegistro["PerBarrio"]); ?>" maxlength="64" maxlenght="64" /></td>
          </tr>


          <tr valign="top">
            <td class="formulario-etiquetas">Domicilio:</td>
            <td width="340"><input name="PerDomicilio" type="text" class="formulario-textbox" id="PerDomicilio" style="width: 300px;" value="<?php print($aRegistro["PerDomicilio"]); ?>" maxlenght="512" /></td>
          </tr>
          <tr valign="top">
            <td class="formulario-etiquetas">Telefono:</td>
            <td width="340"><input name="PerTelefono" type="text" class="formulario-textbox" id="PerTelefono" style="width: 300px;" value="<?php print($aRegistro["PerTelefono"]); ?>" maxlenght="512" /></td>
          </tr>
          <tr valign="top">
            <td class="formulario-etiquetas">Tel&eacute;fono celular:</td>
            <td><input name="PerCelular" type="text" class="formulario-textbox" id="PerCelular" style="width: 300px;" value="<?php print($aRegistro["PerCelular"]); ?>" maxlenght="512" /></td>
          </tr>
          <tr valign="top">
            <td class="formulario-etiquetas">Tel&eacute;fono para mensajes:</td>
            <td><input name="PerTelMensaje" type="text" class="formulario-textbox" id="PerTelMensaje" style="width: 300px;" value="<?php print($aRegistro["PerTelMensaje"]); ?>" maxlenght="512" /></td>
          </tr>
          <tr>
            <td class="formulario-etiquetas">Email:</td>
            <td width="340"><input name="PerEmail" type="text" class="formulario-textbox" id="PerEmail" style="width: 300px;" value="<?php print($aRegistro["PerEmail"]); ?>" /></td>
          </tr>
<!--          <tr>
            <td colspan="2" class="listado-encabezado-texto informe-separador">Situaci&oacute;n Laboral </td>
          </tr>
          <tr>
            <td class="formulario-etiquetas">Forma de contacto con la empresa:</td>
            <td><select name="idformacontacto" id="idformacontacto" style="width: 300px;">
              <?php
					$sSQL = "SELECT idforma, nombre FROM forma_contacto  ";
					print(GenerarOptions($sSQL, $aRegistro["idformacontacto"]));
			  ?>
                        </select></td>
          </tr>
          <tr>
            <td class="formulario-etiquetas">Situaci&oacute;n laboral actual:</td>
            <td><select name="idsituacionactual" id="idsituacionactual" style="width: 300px;">
              <?php
					$sSQL = "SELECT idsituacion, nombre FROM situacion_laboral  ";
					print(GenerarOptions($sSQL, $aRegistro["idsituacionactual"]));
			  ?>
                        </select></td>
          </tr>
          <tr>
            <td class="formulario-etiquetas">Forma de contrataci&oacute;n actual:</td>
            <td class="formulario-textbox"><select name="idcontratacionactual" id="idcontratacionactual" style="width: 300px;">
              <?php
					$sSQL = "SELECT idforma, nombre FROM forma_contratacion  ";
					print(GenerarOptions($sSQL, $aRegistro["idcontratacionactual"]));
			  ?>
                        </select>              &nbsp;&nbsp; &nbsp;&nbsp;</td>
          </tr>-->
          <tr>
            <td colspan="2" class="listado-encabezado-texto informe-separador">Objetivos Laborales</td>
          </tr>
          <tr>
            <td class="formulario-etiquetas">Disponibilidad:</td>
            <td><select name="PerDisponible" id="PerDisponible" style="width: 300px;">
                <?php
					$sSQL = "SELECT TDiNro, TDiDescrip FROM tipodisponibilidad  ";
					print(GenerarOptions($sSQL, $aRegistro["PerDisponible"]));
			  ?>
            </select></td>
          </tr>
          <tr>
            <td class="formulario-etiquetas">Aceptar&iacute;a puestos eventuales:</td>
            <td><table width="100" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="25"><input name="PerAceptaEve" type="radio" value="1" <?php if($aRegistro["PerAceptaEve"] == "1") { ?>checked="checked" <?php } ?>/></td>
                <td width="25" class="formulario-textbox">Si</td>
                <td width="25"><input name="PerAceptaEve" type="radio" value="0" <?php if($aRegistro["PerAceptaEve"] == "0") { ?>checked="checked" <?php } ?>/></td>
                <td class="formulario-textbox">No</td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td class="formulario-etiquetas">Disponibilidad para viajar al Interior o Exterior del pa&iacute;s:</td>
            <td class="formulario-textbox"><table width="100" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="25"><input name="PerDispViaje" type="radio" value="1" <?php if($aRegistro["PerDispViaje"] == "1") { ?>checked="checked" <?php } ?>/></td>
                <td width="25" class="formulario-textbox">Si</td>
                <td width="25"><input name="PerDispViaje" type="radio" value="0" <?php if($aRegistro["PerDispViaje"] == "0") { ?>checked="checked" <?php } ?>/></td>
                <td class="formulario-textbox">No</td>
              </tr>
            </table>                   </td>
          </tr>
          <tr>
            <td class="formulario-etiquetas">Disponibilidad para radicarse en otra ciudad:</td>
            <td class="formulario-textbox"><table width="100" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="25"><input name="PerDispCiudad" type="radio" value="1" <?php if($aRegistro["PerDispCiudad"] == "1") { ?>checked="checked" <?php } ?>/></td>
                  <td width="25" class="formulario-textbox">Si</td>
                  <td width="25"><input name="PerDispCiudad" type="radio" value="0" <?php if($aRegistro["PerDispCiudad"] == "0") { ?>checked="checked" <?php } ?>/></td>
                  <td class="formulario-textbox">No</td>
                </tr>
              </table>                   </td>
          </tr>
          <tr valign="top">
            <td class="formulario-etiquetas">Preferencia laborales:</td>
            <td><textarea name="PerPrefLaboral" class="formulario-textbox" id="PerPrefLaboral" style="width: 300px;" maxlenght="512"><?php print($aRegistro["PerPrefLaboral"]); ?></textarea></td>
          </tr>
          <tr valign="top">
            <td class="formulario-etiquetas">Aspiraciones para pr&oacute;ximo puesto: </td>
            <td><textarea name="PerAspPuesto" class="formulario-textbox" id="PerAspPuesto" style="width: 300px;" maxlenght="512"><?php print($aRegistro["PerAspPuesto"]); ?></textarea></td>
          </tr>
          <tr valign="top">
            <td class="formulario-etiquetas">Remuneraci&oacute;n Pretendida: </td>
            <td><input name="PerRemuneracion" type="text" class="formulario-textbox" id="PerRemuneracion" style="width: 300px;" value="<?php print($aRegistro["PerRemuneracion"]); ?>" /></td>
          </tr>
          <tr>
            <td colspan="2" class="listado-encabezado-texto informe-separador">&Aacute;reas de Inter&eacute;s </td>
          </tr>
          <tr valign="top">
            <td class="formulario-etiquetas">&Aacute;rea 1:</td>
            <td class="formulario-textbox"><select name="AreNro1" id="AreNro1" style="width: 300px;" >
              <?php
					$sSQL = "SELECT AreNro, AreNom FROM area  ";
					$sSQL .= "ORDER BY AreNom ASC ";
					print(GenerarOptions($sSQL, $aRegistro["AreNro1"], true, "[Indistinto]"));
			  ?>
            </select></td>
            </tr>
          <tr valign="top">
            <td class="formulario-etiquetas">&Aacute;rea 2:</td>
            <td class="formulario-textbox"><select name="AreNro2" id="AreNro2" style="width: 300px;" >
              <?php
					$sSQL = "SELECT AreNro, AreNom FROM area  ";
					$sSQL .= "ORDER BY AreNom ASC ";
					print(GenerarOptions($sSQL, $aRegistro["AreNro2"], true, "[Indistinto]"));
			  ?>
            </select></td>
            </tr>
          <tr valign="top">
            <td class="formulario-etiquetas">&Aacute;rea 3:</td>
            <td class="formulario-textbox"><select name="AreNro3" id="AreNro3" style="width: 300px;" >
              <?php
					$sSQL = "SELECT AreNro, AreNom FROM area  ";
					$sSQL .= "ORDER BY AreNom ASC ";
					print(GenerarOptions($sSQL, $aRegistro["AreNro3"], true, "[Indistinto]"));
			  ?>
            </select></td>
            </tr>

          <tr>
            <td colspan="2" class="listado-encabezado-texto informe-separador">Conocimientos</td>
          </tr>
          <tr>
            <td class="formulario-etiquetas">Ingl&eacute;s:</td>
            <td class="formulario-textbox">
			
			<table border="0" cellspacing="4" cellpadding="0">
			  <tr>
				<td><input name="PerIngles" type="radio" value="1" <?php ($aRegistro["PerIngles"] == 1) ? print "checked=\"checked\"" : print ""; ?> onclick="habilitaIdioma(this.value)" /></td>
				<td width="25">Si</td>
				<td><input name="PerIngles" type="radio" value="0" <?php ($aRegistro["PerIngles"] == 0) ? print "checked=\"checked\"" : print ""; ?> onclick="habilitaIdioma(this.value)" /></td>
				<td width="50">No</td>
				
				<td>Habla:</td>
				<td><select name="PerInglesHabla" id="PerInglesHabla" style="width: 120px;">
                  <?php
					$sSQL = "SELECT idnivel, nombre FROM _nivel_idioma  ";
					$sSQL .= "WHERE idnivel > 1 ";
					$sSQL .= "ORDER BY idnivel ASC ";
					print(GenerarOptions($sSQL, $aRegistro["PerInglesHabla"]));
			  ?>
                </select></td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>Escribe:</td>
				<td><select name="PerInglesEscribe" id="PerInglesEscribe" style="width: 120px;">
                  <?php
					$sSQL = "SELECT idnivel, nombre FROM _nivel_idioma  ";
					$sSQL .= "WHERE idnivel > 1 ";
					$sSQL .= "ORDER BY idnivel ASC ";
					print(GenerarOptions($sSQL, $aRegistro["PerInglesEscribe"]));
			  ?>
                </select></td>
			  </tr>
			</table>

			
			</td>
          </tr>
          <tr>
            <td valign="top" class="formulario-etiquetas">Otros idiomas :</td>
            <td><textarea name="PerIdiomas" class="formulario-textbox" id="PerIdiomas" style="width: 300px;" maxlenght="512"><?php print($aRegistro["PerIdiomas"]); ?></textarea></td>
          </tr>
          <tr>
            <td valign="top" class="formulario-etiquetas">Otros conocimientos:</td>
            <td><textarea name="PerOtrosConoc" class="formulario-textbox" id="PerOtrosConoc" style="width: 300px;" maxlenght="512"><?php print($aRegistro["PerOtrosConoc"]); ?></textarea></td>
          </tr>
		  
          <tr>
            <td height="30" class="formulario-etiquetas"><img src="images/espacio.gif" width="1" height="1"></td>
            <td align="center"><input name="BTN_Guardar" type="submit" id="BTN_Guardar" value="Guardar">
              <input name="BTN_Cancelar" type="reset" id="BTN_Cancelar" value="Cancelar" onclick="history.back();"></td>
          </tr>
        </table>
        </form></td>
        <td width="6" class="formulario-contenido-final"><img src="images/espacio.gif" width="1" height="1"></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="20"><img src="images/formulario-pie-inicio.jpg" width="20" height="40"></td>
        <td class="formulario-pie-bg"><img src="images/espacio.gif" width="1" height="1"></td>
        <td width="20"><img src="images/formulario-pie-final.jpg" width="20" height="40"></td>
      </tr>
    </table></td>
  </tr>
</table>

<script>
	habilitaIdioma(<?php print $aRegistro["PerIngles"]; ?>)
	
	function habilitaIdioma(pRadio)
	{
		var ingH = document.getElementById("PerInglesHabla");
		var ingE = document.getElementById("PerInglesEscribe")
		
		if(pRadio == 1)
		{
			ingH.disabled = false; ingE.disabled = false;
		}
		else
		{
			ingH.disabled = true; ingE.disabled = true;
		}
		return true;	
	}
</script>
