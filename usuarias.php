<?php
session_start();

// Si NO es - Admin | Operador | Supervisor
if ($_SESSION["tipo_usuario"] != 1 && $_SESSION["tipo_usuario"] != 6 && $_SESSION["tipo_usuario"] != 7) {
    header("Location: escritorio.php");
}

$seccion = "usuarias";
$titulo = "Usuarias";

$comando = array(
    "titulo" => "Nuevo Cliente",
    "onclick" => "nuevo_cliente();"
);

include("clases/framework-1.0/class.bd.php");
include("includes/funciones.php");
require_once ('clases/phppaging/PHPPaging.lib.php');

include("inc.encabezado.php");
include("inc.usuarias.php");
include("inc.pie.php");
?>
