<?php
	$aSubMenues[0][0] = "areas";
	$aSubMenues[1][0] = "rubros";
	$aSubMenues[2][0] = "idiomas";
	$aSubMenues[3][0] = "contrataciones";
	$aSubMenues[4][0] = "estudios";
	$aSubMenues[5][0] = "niveles";
	$aSubMenues[6][0] = "paises";
	$aSubMenues[7][0] = "provincias";
	$aSubMenues[8][0] = "localidades";
	$aSubMenues[9][0] = "unidades";

	for($lJ = 0; $lJ < count($aSubMenues); $lJ++)
	{
		if($aSubMenues[$lJ][0] == "*")
		{
			$aSubMenues[$lJ][1] = true;
			break;
		}
		else
		{
			if(strpos(basename($m_sURL), $aSubMenues[$lJ][0]) > 0)
			{
				$aSubMenues[$lJ][1] = true;
				break;
			}
		}
	}
?>
<table width="780" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="31" valign="top" class="encabezado-submenu-bg"><table width="780" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="1"><img src="images/inicio-encabezado-submenu.jpg" width="1" height="32" /></td>
        <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><img src="images/espacio.gif" width="1" height="4" /></td>
          </tr>
          <tr>
            <td><table border="0" cellspacing="0" cellpadding="0" class="submenu">
                <tr>
					<td width="15"><img src="images/espacio.gif" width="1" height="1" /></td>

					<td><a href="listado-paises.php"><img src="images/encabezado-btn-paises-<?php print(($aSubMenues[6][1] ? "2" : "1")); ?>.jpg" alt="Paises" width="80" height="20" border="0" style="margin-right:1px;" /></a></td>

					<td><a href="listado-provincias.php"><img src="images/encabezado-btn-provincias-<?php print(($aSubMenues[7][1] ? "2" : "1")); ?>.jpg" alt="Provincias" width="80" height="20" border="0" style="margin-right:1px;" /></a></td>

					<td><a href="listado-areas.php"><img src="images/encabezado-btn-areas-<?php print(($aSubMenues[0][1] ? "2" : "1")); ?>.jpg" alt="Areas" width="80" height="20" border="0" style="margin-right:1px;" /></a></td>

					<td><a href="listado-contrataciones.php"><img src="images/encabezado-btn-contrataciones-<?php print(($aSubMenues[3][1] ? "2" : "1")); ?>.jpg" alt="Tipos de Contratacion" width="90" height="20" border="0" style="margin-right:1px;" /></a></td>

					<td><a href="listado-estudios.php"><img src="images/encabezado-btn-estudios-<?php print(($aSubMenues[4][1] ? "2" : "1")); ?>.jpg" alt="Areas de Estudio" width="70" height="20" border="0" style="margin-right:1px;" /></a></td>

					<td><a href="listado-unidades.php"><img src="images/encabezado-btn-sucursales-<?php print(($aSubMenues[9][1] ? "2" : "1")); ?>.jpg" alt="Unidades Organizativas" width="80" height="20" border="0" style="margin-right:1px;" /></a></td>
                </tr>
            </table></td>
          </tr>
        </table></td>
        <td width="1"><img src="images/final-encabezado-submenu.jpg" width="1" height="32" /></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><img src="images/espacio.gif" width="1" height="5"></td>
  </tr>
</table>
