<script type="text/JavaScript">
<!--
function selectAction(selObj){ //v3.0
	var tipoReporte = selObj.options[selObj.selectedIndex].value;
	var actionForm = "listado-ofertas.php";
	
	switch(tipoReporte)
	{
		case "2" : actionForm = "reportes.php"; break;
	}

	//alert (actionForm);
	document.forms["frmFiltro"].action = actionForm;
}
//-->
</script>


<?php
	$bFechaDesde = ($_GET["fecha_desde"] ? $_GET["fecha_desde"] : "");
	$bFechaHasta = ($_GET["fecha_hasta"] ? $_GET["fecha_hasta"] : date("d-m-Y"));
//	$bFechaModifDesde = ($_GET["fecha_modif_desde"] ? $_GET["fecha_modif_desde"] : "");
//	$bFechaModifHasta = ($_GET["fecha_modif_hasta"] ? $_GET["fecha_modif_hasta"] : date("d-m-Y"));
	
	$bUnidad = (is_numeric($_GET["idunidad"]) ? $_GET["idunidad"] : 0);
	$bArea = (is_numeric($_GET["idarea"]) ? $_GET["idarea"] : 0);

	$bCliente = ($_GET["cliente"] ? $_GET["cliente"] : "");
	$bIdCliente = ($_GET["idcliente"] ? $_GET["idcliente"] : "");

	$bReferencia = ($_GET["referencia"] ? $_GET["referencia"] : "");
	$bSelector = (is_numeric($_GET["idselector"]) ? $_GET["idselector"] : 0);
	$bEstado = ($_GET["estado"] ? $_GET["estado"] : "");
?>

<tr align="left">
	<td class="encabezado-formulario">Fecha alta desde:</td>
	<td><input name="fecha_desde" type="text" class="formulario-textbox" id="fecha_desde" style="width: 140px;" value="<?php print $bFechaDesde; ?>" /></td>
	<td class="encabezado-formulario">Fecha alta hasta:</td>
	<td><input name="fecha_hasta" type="text" class="formulario-textbox" id="fecha_hasta" style="width: 140px;" value="<?php print $bFechaHasta; ?>" /></td>
  </tr>
<!--<tr align="left">
  <td class="encabezado-formulario">Fecha modificaci&oacute;n desde:</td>
  <td><input name="fecha_modif_desde" type="text" class="formulario-textbox" id="fecha_modif_desde" style="width: 140px;" value="<?php print $bFechaModifDesde; ?>" /></td>
  <td class="encabezado-formulario">Fecha modificaci&oacute;n hasta:</td>
  <td><input name="fecha_modif_hasta" type="text" class="formulario-textbox" id="fecha_modif_hasta" style="width: 140px;" value="<?php print $bFechaModifHasta; ?>" /></td>
  </tr>
--><tr align="left">
	<td class="encabezado-formulario">Sucursal:</td>
	<td><select name="idunidad" class="formulario-textbox" id="idunidad" style="width: 180px;">
      <?php
					$sSQL = "SELECT UniNro, UniNombre FROM unidadorg  ";
					print(GenerarOptions($sSQL, $bUnidad, true, DEFSELECT));
			  ?>
    </select></td>
	<td class="encabezado-formulario">&Aacute;rea:</td>
	<td><select name="idarea" class="formulario-textbox" id="idarea" style="width: 180px;">
      <?php
					$sSQL = "SELECT AreNro, AreNom FROM area  ";
					$sSQL .= "ORDER BY AreNom ASC ";
					print(GenerarOptions($sSQL, $bArea, true, DEFSELECT));
			  ?>
    </select></td>
  </tr>
<tr align="left">
	<td class="encabezado-formulario">Usuaria:</td>
	<td><input name="cliente" type="text" class="formulario-textbox" id="cliente" style="width: 180px;" value="<?php print $bCliente; ?>" /></td>
	<td colspan="2" class="encabezado-formulario"><select name="idcliente" class="formulario-textbox" id="idcliente" style="width: 342px;">
	    <?php
					$sSQL = "SELECT CliNro, CliRsocial FROM cliente  ";
					$sSQL .= "ORDER BY CliRsocial ASC ";
					print(GenerarOptions($sSQL, $bIdCliente, true, DEFSELECT));
			  ?>
      </select></td>
  </tr>
<tr align="left">
	<td class="encabezado-formulario">Situaci&oacute;n del pedido: </td>
	<td><select name="estado" class="formulario-textbox" id="estado" style="width: 180px;">
      <?php
					$sSQL = "SELECT nombre, nombre FROM _ofertas_estados  ";
					print(GenerarOptions($sSQL, $bEstado, true, DEFSELECT));
			  ?>
    </select></td>
	<td class="encabezado-formulario">Selector:</td>
	<td><select name="idselector" class="formulario-textbox" id="idselector" style="width: 180px;">
      <?php
					$sSQL = "SELECT MEmpNro, CONCAT(MEmpApellido, \" \", MEmpNombres) FROM miembroempresa  ";
					$sSQL .= "WHERE MEmpAdmin = 0 ";
					$sSQL .= "ORDER BY MEmpApellido ASC, MEmpNombres ASC ";
					print(GenerarOptions($sSQL, $bSelector, true, DEFSELECT));
			  ?>
    </select></td>
  </tr>
<tr align="left">
	<td class="encabezado-formulario">ID:</td>
	<td><input name="referencia" type="text" class="formulario-textbox" id="referencia" style="width: 140px;" value="<?php print $bReferencia; ?>" /></td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
<!--	<td class="encabezado-formulario">Ejecutivo de Cuenta:</td>
	<td><select name="EjCta" class="formulario-textbox" id="EjCta" style="width: 180px;">
              <?php
					$sSQL = "SELECT MEmpNro, CONCAT(MEmpNombres, \" \", MEmpApellido) ";
					$sSQL .= "FROM miembroempresa WHERE MEmpAdmin = 2 ";
					print(GenerarOptions($sSQL, $aRegistro["CliEjCta"], true, "[Indistinto]"));
			  ?>
    </select></td>
--></tr>
<tr align="left">
	<td class="encabezado-formulario">&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
    <td align="right" class="encabezado-formulario" style="padding-right:50px;">
      <input name="btnFiltrar" type="image" id="btnFiltrar" src="images/btn-buscar.jpg" alt="Filtrar" value="1"/>    </td>
</tr>
