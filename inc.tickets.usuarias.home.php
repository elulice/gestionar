<div class="portlet" style="margin-left: 10px;width: 450px;">
    <div class="portlet-header">
        <span> 
            <img src="images/icons/chart_bar.gif" />Reportes
        </span>
        <!--a class="custom_menu" style="background-image:url(images/icons/link.png);">Configurar</a-->
    </div>
    <div class="portlet-content">
        <div id="production-chart" style="height: 170px"></div>
    </div>
    <script type="text/javascript">
<?php
//                    $sSQL = 'SELECT
//                                c.CliRsocial
//                                , COUNT(*) AS nro
//                            FROM
//                                postulacion p
//                                LEFT JOIN oferta o ON p.OfeNro = o.OfeNro
//                                LEFT JOIN ofertascliente oc ON o.OfeNro = oc.OfeNro
//                                LEFT JOIN cliente c ON oc.CliNro = c.CliNro
//                            GROUP BY
//                                c.CliNro
//                            ORDER BY
//                                COUNT(*) DESC
//                            LIMIT 10';

$sSQL = 'SELECT cliente.CliRsocial, COUNT(*) AS nro FROM all_tickets LEFT JOIN cliente ON all_tickets.cliente_id = cliente.CliNro WHERE all_tickets.visible = "S" GROUP BY cliente.CliNro ORDER BY COUNT(*) DESC LIMIT 10';
$db = new BD();
$db->Conectar();
$experiencias = $db->Ejecutar($sSQL);
while ($experiencia = mysql_fetch_assoc($experiencias)) {
    $usuarias[] = $experiencia['CliRsocial'];
    $cantidad[] = (int) $experiencia['nro'];
}
?>
    $(document).ready(function() {
        chart = new Highcharts.Chart({
            chart: {
                renderTo: 'production-chart',
                type: 'column',
                margin: [ 15, 15, 60, 50]
            },
            credits : {
                enabled: false
            },
            title: {
                text: 'Tickets por Usuaria',
                style: {
                    font: 'bold 13px Verdana, sans-serif'
                }
            },
            xAxis: {
                categories: <?php echo json_encode($usuarias) ?>,
                labels: {
                    rotation: -45,
                    align: 'right',
                    style: {
                        font: 'normal 10px Verdana, sans-serif'
                    }
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Tickets'
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                formatter: function() {
                    return '<b>'+ this.x +'</b><br/>'+
                        'Tickets: '+ this.y;
                }
            },
            series: [{
                    name: 'Ticket',
                    data: <?php echo json_encode($cantidad) ?>,
                    dataLabels: {
                        enabled: true,
                        rotation: -90,
                        color: '#FFFFFF',
                        align: 'right',
                        x: -3,
                        y: 10,
                        formatter: function() {
                            return this.y;
                        },
                        style: {
                            font: 'normal 11px Verdana, sans-serif'
                        }
                    }
                }]
        });
    });

    </script>
</div>
