$(document).ready (function() {

	actualizar_noticias_listado();

	$("#dialog-detalle-noticia").dialog({
modal: true,
autoOpen: false,
width: 650,
title: "Detalle de Noticia",
buttons: {
			"Guardar": function() {
				$("#form-detalle-noticia").submit();
				$(this).dialog("close");
			},
			"Cancelar": function() {
				$(this).dialog("close");
			}
		}
	});

	$("#frm_detalle_noticia input[name='fecha']").datepicker(dp_options);

});

function actualizar_noticias_listado() {

	$.ajax({
type: "get",
url:  "feed.noticias.php?objeto=principal",
data: $("#frm_noticias_listado_filtro").serialize(),
		beforeSend : function() {
			$("#div_noticias_listado").html('<img src="images/loading.gif" class="loading" />');
		},
success: function(msg) {
			$("#div_noticias_listado").html(msg);
		}
	});
}

function nueva_noticia() {

	var target = $("#dialog-detalle-noticia");

	$.get("feed.noticias.php?objeto=detalle-noticia", function(res) {
		$(target).html(res);
		$(target).dialog("open");
	});

}

function editar_noticia(id_noticia) {

	var target = $("#dialog-detalle-noticia");

	$.get("feed.noticias.php?objeto=detalle-noticia&id=" + id_noticia, function(res) {
		$(target).html(res);
		$(target).dialog("open");
	});
}



function eliminar_noticia(id_noticia) {

   var res = confirm("�Desea Eliminar esta noticia?");
   if (res == false) return false;

   $.ajax({
      type: "get",
      url:  "abm.php?tabla=noticias&columna=idnoticia&idregistro=" + id_noticia,
      success: function(res) {
      }
   });
  actualizar_noticias_listado();
}
