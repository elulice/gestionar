$(document).ready (function() {

    $("#accordion_opciones").accordion({
        collapsible: true,
        autoHeight: true
    /*
      active: false,
      clearStyle: true
      */
    });

    $("#accordion_opciones button").button();
    $("#form_opciones_nuevo_pais button").button();

    /*
   $("#accordion_opciones button").button();
   $("#accordion_opciones input").keydown (function(ev) {

      if (ev.keyCode == 13) {

	 var id = $(this).attr("id").substring(4, 100);
	 var func = "cmd_opcion_" + id + "_click";
	 window[func]();

      }

   });
   */

    /* bindings de jquery form plugin */
    $("#form_opciones_nuevo_pais").ajaxForm(function(res) {
        cargar_lista_opciones("paises");
        $("#form_opciones_nuevo_pais").clearForm();
    });

    $("#form_opciones_nueva_provincia").ajaxForm(function(res) {
        cargar_lista_opciones("provincias");
        $("#form_opciones_nueva_provincia").clearForm();
    });

    $("#form_opciones_nueva_area").ajaxForm(function(res) {
        cargar_lista_opciones("areas");
        $("#form_opciones_nueva_area").clearForm();
    });

    $("#form_opciones_nuevo_estudio").ajaxForm(function(res) {
        cargar_lista_opciones("estudios");
        $("#form_opciones_nuevo_estudio").clearForm();
    });

    $("#form_opciones_nueva_contratacion").ajaxForm(function(res) {
        cargar_lista_opciones("contrataciones");
        $("#form_opciones_nueva_contratacion").clearForm();
    });

    $("#form_opciones_nueva_sucursal").ajaxForm(function(res) {
        cargar_lista_opciones("sucursales");
        $("#form_opciones_nueva_sucursal").clearForm();
    });
    
    $("#form_opciones_nuevo_responsable").ajaxForm(function(res) {
        cargar_lista_opciones("responsable");
        $("#form_opciones_nuevo_responsable").clearForm();
    });
    
    $("#form_opciones_nuevo_estado").ajaxForm(function(res) {
        cargar_lista_opciones("estado");
        $("#form_opciones_nuevo_estado").clearForm();
    });
    
    $("#form_opciones_nuevo_origen").ajaxForm(function(res) {
        cargar_lista_opciones("origen");
        $("#form_opciones_nuevo_origen").clearForm();
    });
    
    $("#form_opciones_nuevo_motivos").ajaxForm(function(res) {
        cargar_lista_opciones("motivos");
        $("#form_opciones_nuevo_motivos").clearForm();
    });

    $("#form_opciones_nueva_sucursal input[type='submit']").button();
    $("#form_opciones_nueva_provincia input[type='submit']").button();
    $("#form_opciones_nueva_area input[type='submit']").button();
    $("#form_opciones_nuevo_estudio input[type='submit']").button();
    $("#form_opciones_nueva_contratacion input[type='submit']").button();
    $("#form_opciones_nuevo_responsable input[type='submit']").button();
    $("#form_opciones_nuevo_estado input[type='submit']").button();
    $("#form_opciones_nuevo_origen input[type='submit']").button();
    $("#form_opciones_nuevo_motivos input[type='submit']").button();
    
    $("#dialog_opciones_sucursales_edit input[type='button']").button();
   

    var dialog_options = {
        modal: true,
        width: "auto",
        height: "auto",
        autoOpen: false,
        buttons: {
            "Cerrar": function() {
                $(this).dialog("close");
            }
        }
    };

    /* dialog paises */
    $("#dialog_opciones_paises").dialog(dialog_options);
    $("#dialog_opciones_areas").dialog(dialog_options);
    $("#dialog_opciones_estudios").dialog(dialog_options);
    $("#dialog_opciones_provincias").dialog(dialog_options);
    $("#dialog_opciones_contrataciones").dialog(dialog_options);
    $("#dialog_opciones_sucursales").dialog(dialog_options);
    $("#dialog_opciones_responsable").dialog(dialog_options);
    $("#dialog_opciones_estado").dialog(dialog_options);
    $("#dialog_opciones_origen").dialog(dialog_options);
    $("#dialog_opciones_motivos").dialog(dialog_options);
    
    $("#dialog_opciones_sucursales_edit").dialog(dialog_options);

});

function configurar_opciones(dialog) {

    cargar_lista_opciones(dialog);
    $("#dialog_opciones_" + dialog + " form").clearForm();
    $("#dialog_opciones_" + dialog).dialog("option", "title", dialog.toUpperCase());
    $("#dialog_opciones_" + dialog).dialog("open");

}

function cargar_lista_opciones(seccion) {

    $.ajax({
        type: "get",
        url:  "feed.opciones.php?objeto=" + seccion,
        success: function(res) {
            $("#dialog_opciones_" + seccion + "_lista").html(res);
        }
    });

}

function borrar_elemento(seccion, id) {

    var res = confirm("Desea eliminar este elemento?");
    if (res == false) return;

    var url = "abm.php?";

    switch (seccion) {

        case "provincias" :
            url += "tabla=provincia&columna=PrvNro&idregistro=" + id;
            break;

        case "paises" :
            url += "tabla=pais&columna=PaiNro&idregistro=" + id;
            break;

        case "estudios" :
            url += "tabla=area_estudio&columna=id_area_estudio&idregistro=" + id;
            break;

        case "areas" :
            url += "tabla=areasuniversitarias&columna=AreuNro&idregistro=" + id;
            break;

        case "contrataciones" :
            url += "tabla=tipo_contratacion&columna=id_tipo_contratacion&idregistro=" + id;
            break;

        case "sucursales" :
            url += "tabla=unidadorg&columna=UniNro&idregistro=" + id;
            break;
            
        case "responsable" :
            url += "tabla=responsable&columna=id&idregistro=" + id;
            break;
            
        case "estado" :
            url += "tabla=estado_tickets&columna=id&idregistro=" + id;
            break;
            
        case "origen" :
            url += "tabla=origen_tickets&columna=id&idregistro=" + id;
            break;
            
        case "motivos" :
            url += "tabla=motivos&columna=id&idregistro=" + id;
            break;

        default:
            return;
            break;
    }

    $.ajax({
        type: "get",
        url: url,
        success: function(res) {
            cargar_lista_opciones(seccion);
        }
    });
   
}

function editar_elemento(seccion, id, value, value1, value2, value3) {

    var url = "abm.php?accion=actualizar";

    switch (seccion) {

        case "sucursales" :
            configurar_opciones('sucursales_edit')
            document.getElementById('sucHelper').value = "";
            document.getElementById('sucHelper').value = id;
            break;
            
        case "sucursales_save_edit" :
            url += "&uid=" + id + "&UniMail=" + value + "&UniResponsable=" + value1;
            $.ajax({
                type: "get",
                url: url,
                success: function() {
                    cargar_lista_opciones('sucursales');
                    $("#dialog_opciones_sucursales_edit").dialog("close");
                }
            });
            break;

        default:
            return;
            break;
    }
   
}

function cmd_opcion_pais_click() {
    var pais = $("#txt_pais");
    if ($(pais).val().length == 0) {
        $(pais).focus();
        return;
    }
    alert("OK");
    $(pais).val("");
}

function cmd_opcion_estudio_click() {
    var estudio = $("#txt_estudio");
    if ($(estudio).val().length == 0) {
        $(estudio).focus();
        return;
    }
    alert("OK");
    $(estudio).val("");
}

function cmd_opcion_provincia_click() {
    var provincia = $("#txt_provincia");
    if ($(provincia).val().length == 0) {
        $(provincia).focus();
        return;
    }
    $("#dialog_opciones_provincias").dialog("open");
    $(provincia).val("");
}

function cmd_opcion_sucursales_click() {
    var sucursales = $("#txt_sucursales");
    if ($(sucursales).val().length == 0) {
        $(sucursales).focus();
        return;
    }
    $("#dialog_opciones_sucursales").dialog("open");
    $(sucursales).val("");
}

function cmd_opcion_area_click() {
    var area = $("#txt_area");
    if ($(area).val().length == 0) {
        $(area).focus();
        return;
    }
    alert("OK");
    $(area).val("");
}

function cmd_opcion_contratacion_click() {
    var elemento = $("#txt_contratacion");
    if ($(elemento).val().length == 0) {
        $(elemento).focus();
        return;
    }
    alert("OK");
    $(elemento).val("");
}

function cmd_opcion_responsable_click() {
    var responsable = $("#txt_responsable");
    if ($(responsable).val().length == 0) {
        $(responsable).focus();
        return;
    }
    $("#dialog_opciones_responsable").dialog("open");
    $(responsable).val("");
}

function cmd_opcion_estado_click() {
    var estado = $("#txt_estado");
    if ($(estado).val().length == 0) {
        $(estado).focus();
        return;
    }
    $("#dialog_opciones_estado").dialog("open");
    $(estado).val("");
}

function cmd_opcion_origen_click() {
    var origen = $("#txt_origen");
    if ($(origen).val().length == 0) {
        $(origen).focus();
        return;
    }
    $("#dialog_opciones_origen").dialog("open");
    $(origen).val("");
}