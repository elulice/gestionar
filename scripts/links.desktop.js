$(document).ready(function(){
    $( "#dialog-form-links" ).dialog({
        autoOpen: false,
        height: 'auto',
        width: 370,
        modal: true,
        resizable: false,
        title: '<span class="ui-icon ui-icon-link"></span> Agregar Link',
        buttons: {
            "Agregar Link": function() {
                $("#formAgregarLink").submit();
            },
            "Cancelar": function() {
                $( this ).dialog( "close" );
            }
        }
    });
    $("#dialog-listar-links").dialog({
        modal: true,
        autoOpen: false,
        width: 520,
        height: 'auto',
        resizable: false,
        title: '<span class="ui-icon ui-icon-link"></span> Links',
        resizeStop: function(event, ui) {
            $('#dialog-listar-links').dialog('option', 'position', 'center');
        },
        open: function(){
            actualizarListadoLinks();
        },
        close: function(){
            actualizarLinks();
        }
    });
});
function actualizarLinks(){
    
    $.ajax({
        type: "GET",
        url: "feed.links.php?objeto=links",
        beforeSend: function(){
            $("#links_clientes").html('<img src="images/loading.gif" class="loading" />');
        },
        success: function(msg){
            $("#links_clientes").html(msg);
        }
    });
}

function actualizarListadoLinks(){
    if($("#dialog-listar-links").length > 0){
        $.ajax({
            type: "GET",
            url: "feed.links.php?objeto=listar_links",
            beforeSend: function(){
                $("#dialog-listar-links").html('<img src="images/loading.gif" class="loading" />');
            },
            success: function(msg){
                $("#dialog-listar-links").html(msg);
                $('#dialog-listar-links').dialog('option', 'width', 'auto');
                $('#dialog-listar-links').dialog('option', 'position', 'center');
                
            }
        });
    }
}

actualizarLinks();