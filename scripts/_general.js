function cargaDatosEj(pSelect)
{
    var mEmpNro = pSelect.options[pSelect.selectedIndex].value;
    var mNombre = document.getElementById("CliEjCta");
    var mEmail = document.getElementById("CliEMailEjCta");
    x_CargarDatosEj(mEmpNro, function(pCadena)
    {
        var datos = pCadena.split(":|:");
        mNombre.value = datos[0];
        mEmail.value = datos[1];
    });
}

function validarExtension(pForm)
{
    var oArchivo = document.getElementById("FILE_cv;../postulantes/curriculums/");
    if(oArchivo.value != "")
    {
        var aExtension = oArchivo.value.split(".");
        if (aExtension[1] != "pdf" && aExtension[1] != "doc")
        {
            alert("Archivo de formato no valido: " + aExtension[1]);
            oArchivo.focus();
        }
    }
}


function formateaFecha(pFecha) {
    partes = pFecha.split("-");
    if (partes.length == 3) 
    {
        pFecha = partes[2] + "-" +  partes[1] + "-" +  partes[0];
    }
    else
    {
        var d = new Date();
        pFecha = d.getFullYear() + "-" +  (d.getMonth() + 1) + "-" + d.getDate();
    }
    return pFecha;
}

function corrigeFecha(pFecha) {
    var fechaNota = document.getElementById(pFecha);
    partes = fechaNota.value.split("-");
    if (partes.length == 3) 
    {
        fechaNota.value = partes[2] + "-" +  partes[1] + "-" +  partes[0];
    }
    else
    {
        var d = new Date();
        fechaNota.value = d.getFullYear() + "-" +  (d.getMonth() + 1) + "-" + d.getDate();
    }
    return true;
}

function corrigeFechas(pArregloFechas) 
{
    for (var i = 0; i < pArregloFechas.length; i++)
    {
        var fechaNota = document.getElementById(pArregloFechas[i]);
        partes = fechaNota.value.split("-");
        if (partes.length == 3) 
        {
            fechaNota.value = partes[2] + "-" +  partes[1] + "-" +  partes[0];
        }
        else
        {
            var d = new Date();
            fechaNota.value = d.getFullYear() + "-" +  (d.getMonth() + 1) + "-" + d.getDate();
        }
    }
    return true;
}


/*******************************************/
// @pControl		combo principal
// @pRelacionado	combo cuyos valores estan relacionados al seleccionado en pControl
/*******************************************/
function cargarListas(pControl, pRelacionado)
{
    var oLista;
    var pValor = pControl.options[pControl.selectedIndex].value;
    oLista = document.getElementById(pRelacionado);
    oLista.length = 1;
    oLista.options[0].text = "Cargando...";
    oLista.disabled = true;
	
    if(pControl != "")
    {
        oLista = document.getElementById(pRelacionado);
        x_CargarListas(pValor, pRelacionado, function(pCadena)
        {
            var lJ;
            var lK = 0;
            var aDatos = pCadena.split(":|:");

            oLista.disabled = false;
            for(lJ = 1; lJ < aDatos.length; lJ++)
            {
                oLista[lK] = new Option(aDatos[lJ + 1], aDatos[lJ]);
                lJ++;
                lK++;
            }
        //alert(pCadena);
        });
    }
}


/**FUNCIONES DE DESPLIEGUE *************************************************************/
function visualizarBuscador(pIDElemento)
{
    var tContenedor = document.getElementById(pIDElemento);
    var mas = document.getElementById("BTN_mas");
    if(tContenedor.className != "listado-fila-visible") 
    {
        tContenedor.className = "listado-fila-visible";
        mas.innerHTML = "&lt;&lt; Ocultar";
    }
    else
    {
        tContenedor.className = "listado-fila-oculta";
        mas.innerHTML = "Avanzado &gt;&gt;";
    }
}

function visualizarElemento(pIDElemento)
{
    var tContenedor = document.getElementById(pIDElemento);
    if(tContenedor.className != "listado-fila-visible") tContenedor.className = "listado-fila-visible";
    else
        tContenedor.className = "listado-fila-oculta";
}



function verPostulante(pIdPostulante, pPosicion)
{
    visualizarElemento("trPostulante-" + pIdPostulante);
	
    var tCelda = document.getElementById("tdPostulante-" + pIdPostulante);
	
    if (tCelda)
    {
        x_VerPostulante(pIdPostulante, pPosicion, function(pCadena)
        {
            tCelda.innerHTML = pCadena;
        });
    }
}




function verNota(pIdNota)
{
    visualizarElemento("trNota-" + pIdNota);
	
    var tCelda = document.getElementById("tdNota-" + pIdNota);
	
    if (tCelda)
    {
        x_VerNota(pIdNota, function(pCadena)
        {
            tCelda.innerHTML = pCadena;
        });
    }
}

function actualizarNombre()
{
    var oArchivo = document.getElementById("FILE_Archivo;../Documentacion/");
    var oTitulo = document.getElementById("DNombre");
    if(oArchivo.value != "")
    {
        var aNombre = oArchivo.value.split("\\");
        oTitulo.value = aNombre[aNombre.length - 1];
    }
    return true;
}


/*************************************************************************/
function verPostulanteFull(pIdPostulante)
{
    var oVentana = new Window(
    {
        className: "alphacube",
        title: "Detalles del Postulante",
        width: 800,
        height: 600,
        left: 110,
        top: 150,
        draggable: true,
        resizable: true,
        minimizable: false,
        maximizable: false,
        destroyOnClose: true
    }
    );
    oVentana.show();
    var sURL = "ver-postulante.php?idregistro=" + pIdPostulante ;
    oVentana.setAjaxContent(sURL);
}

function verNotas(pIdPostulante)
{
    var oVentana = new Window(
    {
        className: "alphacube",
        title: "Notas sobre el Postulante",
        width: 780,
        height: 300,
        left: 110,
        top: 150,
        draggable: true,
        resizable: true,
        minimizable: false,
        maximizable: false,
        destroyOnClose: true
    }
    );
    oVentana.show();
    var sURL = "ver-postulante-notas.php?idregistro=" + pIdPostulante ;
    oVentana.setAjaxContent(sURL);
}

function verOfertasParaPostular(pIdPostulante)
{
    var oVentana = new Window(
    {
        className: "alphacube",
        title: "Postular a una Oferta",
        width: 780,
        height: 300,
        left: 110,
        top: 150,
        draggable: true,
        resizable: true,
        minimizable: false,
        maximizable: false,
        destroyOnClose: true
    }
    );
    oVentana.show();
    var sURL = "ver-ofertas-para-postular.php?idregistro=" + pIdPostulante ;
    oVentana.setAjaxContent(sURL);
}


/***** cambio de estado *******************/
function cambiarEstado(pIDPropuesta, pIDPerfil, pEstado)
{
    x_CambiarEstado(pIDPropuesta, pIDPerfil, pEstado, function(pCadena)
    {
        //tCelda.innerHTML = pCadena;
        });
}
/***** Asociar Postulante a Oferta *******************/
function asociarPostulante(pIDPropuesta, pIDPerfil)
{
    x_AsociarPostulante(pIDPropuesta, pIDPerfil, function(pCadena)
    {
        //tCelda.innerHTML = pCadena;
        });
}
/***** Postulante Postulante de Oferta *******************/
function eliminarPostulado(pIDPostulado)
{
    x_EliminarPostulado(pIDPostulado, function(pCadena)
    {
        //tCelda.innerHTML = pCadena;
        });
}
/***** Postulante Postulante de Oferta *******************/
function eliminarCalificacionCliente(pIDPersona, pIDCliente)
{
    x_EliminarCalificacionCliente(pIDPersona, pIDCliente, function(pCadena)
    {
        //tCelda.innerHTML = pCadena;
        });
    location.href = "listado-curriculums-clientes.php?idpostulante="+pIDPersona;
}




/***** VISTAS RAPIDAS *****/
function verExperiencia(pIdExperiencia, pPosicion)
{
    visualizarElemento("trExperiencia-" + pIdExperiencia);
	
    var tCelda = document.getElementById("tdExperiencia-" + pIdExperiencia);
	
    if (tCelda)
    {
        x_VerExperiencia(pIdExperiencia, pPosicion, function(pCadena)
        {
            tCelda.innerHTML = pCadena;
        });
    }
}
function verEstudio(pIdEstudio, pNivel)
{
    visualizarElemento("trEstudio-" + pIdEstudio);
	
    var tCelda = document.getElementById("tdEstudio-" + pIdEstudio);
	
    if (tCelda)
    {
        x_VerEstudio(pIdEstudio, pNivel, function(pCadena)
        {
            tCelda.innerHTML = pCadena;
        });
    }
}

function verPostulados(pIdpropuesta, pPosicion)
{
    visualizarElemento("trOferta-" + pIdpropuesta);
	
    var tCelda = document.getElementById("tdOferta-" + pIdpropuesta);
	
    if (tCelda)
    {
        x_VerPostulados(pIdpropuesta, pPosicion, function(pCadena)
        {
            tCelda.innerHTML = pCadena;
        });
    }
}

function verCliente(Id)
{
    visualizarElemento("trCliente-" + Id);
	
    var tCelda = document.getElementById("tdCliente-" + Id);
	
    if (tCelda)
    {
        x_VerCliente(Id, function(pCadena)
        {
            tCelda.innerHTML = pCadena;
        });
    }
}

/***** INGRESO USUARIO *****/
function validarUsuario(pForm)
{
    with(pForm)
        x_ValidarUsuario(313, txtUsuario.value, txtClave.value, txtRedirect.value, function(pCadena)
        {
            if(pCadena == "S"){
                location.href = "escritorio.php";
            }else if(pCadena == "N"){
                alert("Usuario o Clave incorrecto");
                pForm.txtClave = "";
                txtUsuario.focus();
            }else{
                location.href = pCadena;
            }
        });
    return false;
}
//No borrar esta funci�n !
function editar_ticket(a){
    window.location='tickets.edit.php?id='+a;
}
$(document).keyup(function(e) {

    if (e.keyCode == 27) {
        $("#p_fecha_edit").datepicker("disable");
    }
});
function editTicket(a,b){
    $.ajax({
        url: 'scripts/showTextOfTicketToEdit.php?idNum='+a,
        success: function(data) {
            $('#'+b).html(data);
            $('textarea#p_content_edit_a').focus();
            //            $('#p_fecha_edit').datepicker();
            //            $("#p_fecha_edit").datepicker("disable");
            //            $("#p_fecha_edit").datepicker("enable");
            return false;
        //alert(data);
        }
    });
}
function saveEditedTicket(a,b){
    
    emailCliente = $('#p_email_cliente_new').val();
    emailSend = $('#email_new').val();
    emailCc = $('#cc_email_new').val();
    emailSucursal = $('#p_email_sucursal').val();
    emailResponsable = $('#p_email_responsable').val();
    
    fh = $('#p_fecha').val();
    nm = $('#p_nombres').val();
    ec = $('#p_email_cliente').val();
    cl = $('#p_cliente').val();
    or = $('#p_origen').val();
    su = $('#p_sucursal').val();
    re = $('#p_responsable').val();
    es = $('#p_estado').val();
    ems = $('#p_email_send').val();
    ecc = $('#p_email_cc').val();
    
    fh_edit = $('#p_fecha_edit').val();
    nm_edit = $('#p_nombres_edit').val();
    ec_edit = $('#p_email_cliente_edit').val();
    cl_edit = $('#p_cliente_edit').val();
    or_edit = $('#p_origen_edit').val();
    su_edit = $('#p_sucursal_edit').val();
    re_edit = $('#p_responsable_edit').val();
    es_edit = $('#p_estado_edit').val();
    ems_edit = $('#p_email_send_edit').val();
    ecc_edit = $('#p_email_cc_edit').val();
    
    $.ajax({
        url: 'scripts/saveEditedTicket.php?gid='+a+'&gce='+b+'&fh='+fh+'&nm='+nm+'&ec='+ec+'&cl='+cl+"&or="+or+"&su="+su+"&re="+re+"&es="+es+'&ems='+ems+'&ecc='+ecc+'&fh_edit='+fh_edit+'&nm_edit='+nm_edit+'&ec_edit='+ec_edit+'&cl_edit='+cl_edit+"&or_edit="+or_edit+"&su_edit="+su_edit+"&re_edit="+re_edit+"&es_edit="+es_edit+'&ems_edit='+ems_edit+'&ecc_edit='+ecc_edit,
        success: function(data) {
            sendEmailToClient(emailCliente,a,emailSend,emailCc,emailResponsable,emailSucursal,'newTicket',a)
            $('#response').html('Guardado Correctamente');
            setTimeout(window.location="tickets.edit.php?id="+a,2);
        }
    });

}
function makeTicketAnswer(a,b){
    emailCliente = $('#p_email_cliente').val();
    emailSend = $('#email_new').val();
    emailCc = $('#cc_email_new').val();
    emailSucursal = $('#p_email_sucursal').val();
    emailResponsable = $('#p_email_responsable').val();
    
    $.ajax({
        url: 'scripts/makeTicketAnswer.php?gid='+a+'&gce='+b,
        success: function(data) {
            $('#responseMake').html('Guardado Correctamente');
            setTimeout(window.location="tickets.edit.php?id="+a,2);
        }
    });
}
function getSelectsContent(a){
    $.ajax({
        url: 'scripts/getSelectsContent.php?idtk='+a,
        success: function(data) {
            document.getElementById('selectsContent').innerHTML = data;
        //alert(data);
        }
    });
}
function editAnswer(a,b){
    $.ajax({
        url: 'scripts/showTextOfAnswerToEdit.php?idNum='+a,
        success: function(data) {
            $('#'+b).html(data);
        //alert(data);
        }
    });
}
function editTicketAnswer(a,b){
    $.ajax({
        url: 'scripts/editTicketAnswer.php?idAnsw='+a+"&gca="+b,
        success: function(data) {
            setTimeout(window.location="tickets.edit.php?id="+a,2);
        }
    });
}
function deleteTicket(a,b){
    var answer = confirm("Seguro desea Eliminar este Ticket?")
    if (answer){
        $.ajax({
            url: 'scripts/deleteTicket.php?delID='+a,
            success: function(data) {
                if(b == "desktop"){
                    setTimeout(window.location="escritorio.php",2);
                }else{
                    actualizar_tickets_usuarias_listado();
                }
            }
        });
    }
}
function generateNewTicket(){
    $("#p_fecha_new").datepicker("disable");
    $("#generateNewTicket").dialog("open");
    $("#p_fecha_new").datepicker("enable");
    $('textarea#p_content_new_ticket').focus();
    $("input#p_fecha_new").datepicker('setDate', new Date());
    return false;
}
function saveNewTicket(a,b){
    emailCliente = $('#p_email_cliente_new').val();
    emailSend = $('#email_new').val();
    emailCc = $('#cc_email_new').val();
    emailSucursal = $('#p_email_sucursal').val();
    emailResponsable = $('#p_email_responsable').val();
    //    alert(emailSend + " / " + emailCc);
    fecha = $('#p_fecha_new').val();
    nm = $('#p_nombres_new').val();
    cl = $('#p_cliente_new').val();
    or = $('#p_origen_new').val();
    su = $('#p_sucursal_new').val();
    re = $('#p_responsable_new').val();
    es = $('#p_estado_new').val();
    
    $.ajax({
        url: 'scripts/saveNewTicket.php?gid='+a+'&fecha='+fecha+'&nm='+nm+'&cl='+cl+"&or="+or+"&su="+su+"&re="+re+"&es="+es+'&cn='+b+"&emailCliente="+emailCliente+"&emailSend="+emailSend+"&emailCc="+emailCc,
        success: function(data) {
            sendEmailToClient(emailCliente,a,emailSend,emailCc,emailResponsable,emailSucursal,'newTicket',a)
            if(data == "0"){
                alert('Faltan Datos');
            }else{
                setTimeout(window.location="tickets.edit.php?id="+a,2);
            }
            
        }
    });
}
function showTicketForAnswer(a,b){
    $.ajax({
        url: 'scripts/showTextOfTicketToEdit.php?idNum='+a+'&what=resp',
        success: function(data) {
            $('#'+b).html(data);
            $('textarea#p_content_response_a').focus();
            return false;
        //alert(data);
        }
    });
}
function sendEmailToClient(a,b,c,d,e,f,g,extraInfo){
    // id Si es necesario
    
    if(b == 'responseForClient'){
        b = $('#respuesta_a_cliente').html();
    }else{
        b = b;
    }
    $.ajax({
        url: "scripts/send.mails.php?email="+a+"&contenido="+b+"&cc="+c+"&cco="+d+"&eResponsable="+e+"&eSucursal="+f+"&case="+g+"&id="+extraInfo,
        success: function(data) {
            $('#respuestaAlCliente').dialog('close');
            $('#response_for_dialogs').dialog('open');
            $('#content_information').html("Mail enviado con &eacute;xito")

            $('#response_for_dialogs').dialog({
                close: function() {
                    window.location="tickets.edit.php?id="+extraInfo;
                }
            });

        }
    });
}
function loadTicketsDesktop(){
    $.ajax({
        url: 'scripts/send.mails.php?email='+a+"&contenido="+b+"&cc="+c+"&cco="+d+"&eResponsable="+e+"&eSucursal="+f+"&case="+g,
        success: function(data) {
            $('#respuestaAlCliente').dialog('close');
            $('#response_for_dialogs').dialog('open');
            $('#content_information').html("Mail enviado con &eacute;xito")
            if(g == "newTicket"){
                setTimeout(window.location="tickets.edit.php?id="+extraInfo,2);
            }
        }
    });
}
function feedTicketsHome(){
    
    $.ajax({
        type: "get",
        url:  "feed.tickets.home.php",
        beforeSend: function() {
            $("#fill_tickets_home").html('<img src="images/loading.gif" class="loading" />');
        },
        success: function(res) {
            $("#fill_tickets_home").html(res);
        }
    });
}

$(document).ready(function(){
    feedTicketsHome();
});