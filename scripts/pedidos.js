$(document).ready (function() {

    $("input[name='dp_fecha_desde']").datepicker(dp_options);
    $("input[name='dp_fecha_hasta']").datepicker(dp_options);
    $("input[name=OfeFechAlta'']").datepicker(dp_options);
    $("input[name=OfeValidez'']").datepicker(dp_options);

    actualizar_pedidos_listado();

    $("#div_dialog_pedidos").dialog({
        modal: true,
        width: "800",
        height: "auto",
        autoOpen: false,
        buttons: {
            "Imprimir" : function(){
                imprimir_oferta();
            },
            "Guardar" : function() {
                guardar_oferta();
                $("#div_dialog_pedidos").dialog("close");
            },
            "Cancelar" : function() {
                $("#div_dialog_pedidos").dialog("close");
            }
        }
    });

    $("#dialog_pedidos_postulantes").dialog({
        modal: true,
        width: "640",
        height: "auto",
        autoOpen: false,
        buttons: {
            "Cerrar": function() {
                $(this).dialog("close");
            }
        }
    });
   
    $("#dialog_pedidos_postulantes2").dialog({
        modal: true,
        width: "870",
        height: "auto",
        autoOpen: false,
        buttons: {
            "Cerrar": function() {
                $(this).dialog("close");
            }
        }
    });

});

function actualizar_pedidos_listado() {

    $.ajax({
        type: "get",
        url: "feed.pedidos.php?objeto=principal",
        data: $("#frm_pedidos_listado_filtro").serialize(),
        beforeSend: function() {
            $("#div_pedidos_listado").html('<img src="images/loading.gif" class="loading" />');
        },
        success: function(res) {
            $("#div_pedidos_listado").html(res);
        }
    });
}

function nueva_oferta() {

    var target = $("#div_dialog_pedidos");

    $.get("feed.pedidos.php?objeto=detalle", function(res) {
        $(target).html(res);
        $("#div_dialog_pedidos").dialog("open");
    });

/*
   $("#frm_pedidos_detalle").clearForm();
   $("#div_dialog_pedidos").dialog("open");
   $("input[name='id_oferta']").val("0");
   $("input[name='OfeFactura']").focus();
   */
}

function guardar_oferta() {

    $("#form-pedidos-detalle").ajaxSubmit(function(res) {
        if (res.length > 0) {
            alert("Cambios efectuados con exito");
            actualizar_pedidos_listado();
        }
    });

    return false;

}

function mostrar_postulantes(id_oferta) {

    $("#dialog_pedidos_postulantes input#dpp_id_oferta").val(id_oferta);
    cargar_lista_postulantes();
    $("#dialog_pedidos_postulantes").dialog("open");

}

function mostrar_postulantes2(id_oferta) {

    $("#dialog_pedidos_postulantes2 input#dpp_id_oferta").val(id_oferta);
    actualizar_curriculums_listado();
    $("#dialog_pedidos_postulantes2").dialog("open");

}

function cargar_lista_postulantes() {

    var id_oferta = $("#dialog_pedidos_postulantes input#dpp_id_oferta").val();

    $.ajax({
        type: "get",
        url:  "feed.pedidos.php?objeto=postulantes&id_oferta=" + id_oferta,
        success: function(res) {
            $("#dpp_lista_postulantes").html(res);
        }
    });

}

function eliminar_oferta(id_oferta) {

    var res = confirm("¿Desea Eliminar esta Oferta?");
    if (res == false) return false;

    $.ajax({
        type: "get",
        url:  "abm.php?tabla=oferta&columna=OfeNro&idregistro=" + id_oferta,
        success: function(res) {
        }
    });
    actualizar_pedidos_listado();
}

function eliminar_postulante(id_oferta) {

    var res = confirm("¿Desea Eliminar esta Postulacion?");
    if (res == false) return false;

    $.ajax({
        type: "get",
        url:  "abm.php?tabla=postulacion&columna=id_postulacion&idregistro=" + id_oferta,
        success: function(res) {
        }
    });
    cargar_lista_postulantes();
}

function editar_oferta(id_oferta) {

    var target = $("#div_dialog_pedidos");

    $.get("feed.pedidos.php?objeto=detalle&id_oferta=" + id_oferta, function(res) {
        $(target).html(res);
        $("#div_dialog_pedidos").dialog("open");
    });
}

function actualizar_curriculums_listado() {

    var form = $("#frm_curriculums_listado_filtro");
    var div = $("#div_curriculums_listado");
    var id_oferta = $("#dialog_pedidos_postulantes2 input#dpp_id_oferta").val();

    $.ajax({
        type: "get",
        url: "feed.curriculums.php?objeto=principal&idoferta="+id_oferta,
        data: $(form).serialize(),
        beforeSend: function() {
        },
        success: function(msg) {
            $(div).html(msg);
        }
    });
}

function asignar_postulante_oferta2(id_postulante) {

    var res = confirm("Desea asignar este Postulante a esta Oferta?");
    if (res == false) return;
   
    var id_oferta = $("#dialog_pedidos_postulantes2 input#dpp_id_oferta").val();

    $.ajax({
        type: "post",
        url:  "abm.php?tabla=postulacion",
        data: {
            PerNro:  id_postulante,
            OfeNro: id_oferta
        },
        success: function(res) {
            actualizar_curriculums_listado();
        }
    });

}



function dca_buscar_ofertas() {

    var id = $("#dialog_pedidos_postulantes2 input#dpp_id_oferta").val();

    $("#dca_ofertas_filtro").ajaxSubmit({
        data: {
            id_postulante: id
        },
        success: function(res) {
            $("#dca_lista_ofertas").html(res);
        }
    });

}

function imprimir_oferta() {
    var id_oferta = $("#id_oferta").val();
    //alert(id_oferta);
    window.open('./pedidos.detalle.print.php?id_oferta=' + id_oferta);
}