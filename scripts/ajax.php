<?php

session_start();
include("../clases/flxajax-0.2.2/flxajax.class.php4");
include("../clases/flxajax-0.2.2/flxajax_client.class.php4");
include("../clases/flxajax-0.2.2/flxajax_server.class.php4");
include("../clases/framework-1.0/class.bd.php");
include("../includes/funciones.php");

$cAjax = &new flxajax();
$cAjax->add(
        "CargarListas", "ValidarUsuario", "VerCliente", "VerPostulados", "VerPostulante", "VerEstudio", "VerExperiencia", "CargarDatosEj", "AsociarPostulante", "EliminarCalificacionCliente", "GenerarAtributos", "CambiaAtributoValor", "VerNota", "CambiarEstado", "EliminarPostulado"
);


$cServidor = &$cAjax->get_server();
if ($cServidor->check_client_request()) {
    print($cServidor->handle_client_request());
    exit();
}
$cCliente = &$cAjax->get_client();
print($cCliente->get_javascript());

/* * **************************************************************************** */

function CargarDatosEj($pIdEj) {
    $sSQL = "SELECT CONCAT(MEmpNombres, \" \", MEmpApellido), MEmpEmail ";
    $sSQL .= "FROM miembroempresa ";
    $sSQL .= "WHERE MEmpNro = " . $pIdEj . " ";

    $cBD = new BD();
    $aRegistro = $cBD->Seleccionar($sSQL, true);
    if ($aRegistro) {
        $sHTML = $aRegistro[0] . ':|:' . $aRegistro[1];
    }
    return $sHTML;
}

function VerEmpresa($pIdEmpresa) {
    $sSQL = "SELECT rubro, localidad, domicilio, email, ";
    $sSQL .= "ejecutivo_email, fecha_alta, fecha_modificado ";
    $sSQL .= "FROM clientes ";
    $sSQL .= "WHERE idcliente = " . $pIdEmpresa . " ";

    $cBD = new BD();
    $aRegistro = $cBD->Seleccionar($sSQL, true);
    if ($aRegistro) {
        $sHTML = '<table width="600" border="0" align="center" cellpadding="2" cellspacing="0" class="informe-texto" style="margin: 5px auto;">';
        $sHTML .= '<tr><td width="300"><strong>Rubro:</strong> ' . ReemplazarCaracteres($aRegistro["rubro"]) . '</td>';
        $sHTML .= '<td width="300"><strong>Domicilio:</strong> ' . ReemplazarCaracteres($aRegistro["domicilio"]) . '</td></tr>';

        $sHTML .= '<tr><td><strong>Email:</strong> <a href="mailto:' . $aRegistro["email"] . '">' . $aRegistro["email"] . '</a></td>';
        $sHTML .= '<td><strong>Email Ejecutivo de cuenta:</strong>  <a href="mailto:' . $aRegistro["ejecutivo_email"] . '">' . $aRegistro["ejecutivo_email"] . '</a></td></tr>';

        $sHTML .= '<tr><td><strong>Fecha de alta:</strong> ' . date("d-m-Y", strtotime($aRegistro["fecha_alta"])) . '</td>';
        $sHTML .= '<td><strong>&Uacute;ltima Modificaci&oacute;n:</strong> ' . date("d-m-Y", strtotime($aRegistro["fecha_modificado"])) . '</td></tr>';
        $sHTML .= '</table>';
    }
    return $sHTML;
}

function VerPostulados($pIdPropuesta, $pPosicion) {
    $sSQL = "SELECT p.PerNro, PerApellido, PerNombres, PerTelefono, ";
    $sSQL .= "PosFecha, PEsDescrip ";

    $sSQL .= "FROM persona p ";
    $sSQL .= "INNER JOIN postulacion po ON p.PerNro = po.PerNro ";
    $sSQL .= "LEFT JOIN postulacionestado ep ON ep.PEsNro = po.PEsNro ";
    $sSQL .= "WHERE po.OfeNro = " . $pIdPropuesta . " ";


    $sSQL .= "ORDER BY PerApellido, PerNombres, PosFecha DESC ";

    $cBD = new BD();
    $oResultados = $cBD->Seleccionar($sSQL);

    $sHTML = '<table width="750" border="1" align="center" cellpadding="0" cellspacing="0" class="vista">';
    $sHTML .= '<tr><th>Apellido</th><th>Nombre</th><th>Telefono</th><th>CV Actualizado</th>';
    $sHTML .= '<th>Postulado</th><th>Situaci&oacute;n</th><th>Fecha</th><th>&nbsp;</th></tr>';

    while ($aRegistro = $cBD->RetornarFila($oResultados)) {
        $sHTML .= '<tr>';
        $sHTML .= '<td width="150">' . ReemplazarCaracteres($aRegistro["PerApellido"]) . '</td>';
        $sHTML .= '<td width="150">' . ReemplazarCaracteres($aRegistro["PerNombres"]) . '</td>';
        $sHTML .= '<td width="120">' . $aRegistro["PerTelefono"] . '</td>';
        $sHTML .= '<td width="100">' . date("d/m/Y H:i:s", strtotime($aRegistro["fecha_modificado"])) . '</td>';
        $sHTML .= '<td width="80">' . $aRegistro["ingreso"] . '</td>';
        $sHTML .= '<td width="100">' . ReemplazarCaracteres($aRegistro["PEsDescrip"]) . '</td>';
        $sHTML .= '<td>' . date("d/m/Y", strtotime($aRegistro["PosFecha"])) . '</td>';
        $sHTML .= '<td><a href="javascript:verPostulanteFull(' . $aRegistro["PerNro"] . ')"><img src="images/btn-ver-mas.png" alt="Vista Completa" border="0"></a></td></tr>';
    }
    $sHTML .= '</table>';

    return $sHTML;
}

function VerPostulante($pIdPostulante) {
    $sSQL = "SELECT p.PerNombres, p.PerApellido, p.PerTipoDoc, p.PerDocumento, ";
    $sSQL .= "p.PerRemuneracion, p.PerEmail, p.PerFechModific, p.PerTelefono, ";
    $sSQL .= "p.PerCelular, p.PerTelMensaje, ";
    $sSQL .= "p.PerECivil, p.PerPais, p.PerProvincia, p.PerLocalidad,  ";
    $sSQL .= "a1.AreNom AS AreNom1, a2.AreNom AS AreNom2, a3.AreNom  AS AreNom3 ";

    $sSQL .= "FROM persona p ";
    $sSQL .= "LEFT JOIN area a1 ON p.AreNro1 = a1.AreNro ";
    $sSQL .= "LEFT JOIN area a2 ON p.AreNro2 = a2.AreNro ";
    $sSQL .= "LEFT JOIN area a3 ON p.AreNro3 = a3.AreNro ";

    $sSQL .= "WHERE p.PerNro = " . $pIdPostulante . " ";

    $cBD = new BD();
    $aRegistro = $cBD->Seleccionar($sSQL, true);
    if ($aRegistro) {
        $sHTML = '<table width="600" border="0" align="center" cellpadding="2" cellspacing="0" class="informe-texto" style="margin: 5px auto;">';
        $sHTML .= '<tr><td width="300"><strong>Fecha &uacute;ltima Modificaci&oacute;n:</strong> ' . date("d-m-Y", strtotime($aRegistro["fecha_modificado"])) . '</td>';
        $sHTML .= '<td width="300">&nbsp;</td></tr>';

        $sHTML .= '<tr><td width="300"><strong>Apellido y Nombres:</strong> ' . ReemplazarCaracteres($aRegistro["PerApellido"]) . ", " . ReemplazarCaracteres($aRegistro["PerNombres"]) . '</td>';
        $sHTML .= '<td width="300"><strong>Pa&iacute;s:</strong> ' . ReemplazarCaracteres($aRegistro["PerPais"]) . '</td></tr>';

        $sHTML .= '<tr><td width="300"><strong>Email:</strong> <a href="mailto:' . ReemplazarCaracteres($aRegistro["PerEmail"]) . '">' . ReemplazarCaracteres($aRegistro["PerEmail"]) . '</a></td>';
        $sHTML .= '<td width="300"><strong>Provincia:</strong> ' . ReemplazarCaracteres($aRegistro["PerProvincia"]) . '</td></tr>';

        $sHTML .= '<tr valign="top"><td width="300"><strong>Tel&eacute;fono:</strong> ' . ReemplazarCaracteres($aRegistro["PerTelefono"]) . '<br><strong>Celular: </strong>' . $aRegistro["PerCelular"] . '<br><strong>Tel. para mensajes: </strong>' . $aRegistro["PerTelMensaje"];

        $sHTML .= '</td><td width="300"><strong>Localidad:</strong> ' . ReemplazarCaracteres($aRegistro["PerLocalidad"]) . '</td></tr>';

        $sHTML .= '<tr><td width="300"><strong>Remuneraci&oacute;n pretendida:</strong> ' . ReemplazarCaracteres($aRegistro["PerRemuneracion"]) . ' ';

        $sHTML .= '</td><td width="300"><strong>&Aacute;reas de interes:</strong> ' . ReemplazarCaracteres($aRegistro["AreNom1"]) . ',' . ReemplazarCaracteres($aRegistro["AreNom2"]) . ', ' . ReemplazarCaracteres($aRegistro["AreNom3"]) . '</td></tr>';

        $sHTML .= '</table>';
    }
    return $sHTML;
}

function VerExperiencia($pIdExperiencia) {
    $sSQL = "SELECT a.AreNom, pue.PueNom, e.PueCargos, e.PueDescPuesto, e.PueFecha ";
    $sSQL .= "FROM datospuesto e INNER JOIN area a ON a.AreNro = e.AreNro ";
    $sSQL .= "INNER JOIN puesto pue ON pue.PueNro = e.PueNro ";
    $sSQL .= "WHERE e.DPueNro = " . $pIdExperiencia . " ";

    $cBD = new BD();
    $aRegistro = $cBD->Seleccionar($sSQL, true);
    if ($aRegistro) {
        $sHTML = '<table width="600" border="0" align="center" cellpadding="2" cellspacing="0" class="informe-texto" style="margin: 5px auto;">';
        $sHTML .= '<tr valign="top"><td width="300"><strong>Fecha de Ingreso:</strong> ' . date("m/Y", $aRegistro["PueFecha"]) . '</td>';
        $sHTML .= '<td width="300" rowspan="3"><strong>Tareas realizadas: </strong>' . ReemplazarCaracteres($aRegistro["PueDescPuesto"]) . '</td></tr>';
        $sHTML .= '<tr><td><strong>Area: </strong>' . ReemplazarCaracteres($aRegistro["AreNom"]) . '</td></tr>';
        $sHTML .= '<tr><td><strong>Cargo: </strong>' . ReemplazarCaracteres($aRegistro["PueCargos"], false, true) . '</td></tr>';
        $sHTML .= '</table>';
    }
    return $sHTML;
}

function VerEstudio($pIdEstudio, $pNivel) {
    $sSQL = "SELECT e.EtuCursadas, e.EtuAprobadas, e.EtuPromedio, e.EtuObservaciones, e.EtuEgreso ";
    if ($pNivel == 2)
        $sSQL .= ", a.AresNombre AS area ";
    if ($pNivel == 4)
        $sSQL .= ", a.AreuNombre AS area ";


    $sSQL .= "FROM estudio e ";

    if ($pNivel == 2)
        $sSQL .= "INNER JOIN areassecundarias a ON a.AresNro = e.EtuAreaEstudio ";
    if ($pNivel == 4)
        $sSQL .= "INNER JOIN areasdeestudio a ON a.AreuNro = e.EtuAreaEstudio ";

    $sSQL .= "WHERE e.EtuNro = " . $pIdEstudio . " ";

    $cBD = new BD();
    $aRegistro = $cBD->Seleccionar($sSQL, true);
    if ($aRegistro) {
        $sHTML = '<table width="600" border="0" align="center" cellpadding="2" cellspacing="0" class="informe-texto" style="margin: 5px auto;">';
        $sHTML .= '<tr valign="top"><td width="400" colspan="3"><strong>Fecha de Egreso:</strong> ' . ($aRegistro["fecha_egreso"] ? date("d/m/Y", strtotime($aRegistro["EtuEgreso"])) : "-") . '</td>';
        $sHTML .= '<td width="200" rowspan="3"><strong>Observaciones: </strong>' . ReemplazarCaracteres($aRegistro["EtuObservaciones"]) . '</td></tr>';
        $sHTML .= '<tr><td><strong>Materias cursadas: </strong>' . $aRegistro["EtuCursadas"] . '</td>';
        $sHTML .= '<td><strong>Materias aprobadas: </strong>' . $aRegistro["EtuAprobadas"] . '</td>';
        $sHTML .= '<td><strong>Promedio: </strong>' . $aRegistro["EtuPromedio"] . '</td></tr>';
        $sHTML .= '</table>';
    }
    return $sHTML;
}

function VerNota($pIdNota) {

    $sSQL = "SELECT n.idnota, n.nota, c1.abrev AS presencia, c2.abrev AS imagen, ";
    $sSQL .= "c3.abrev AS contextura, c4.abrev AS trato, c5.abrev AS comunicacion, ";
    $sSQL .= "c6.abrev AS nivelsociocultural, c7.abrev AS flexibilidad, c8.abrev AS dinamismo, ";
    $sSQL .= "c9.abrev AS actitud, c10.abrev AS perfilcomercial, c11.abrev AS experiencia, ";
    $sSQL .= "c12.abrev AS conceptogeneral ";

    $sSQL .= "FROM notas_postulantes n ";
    $sSQL .= "LEFT OUTER JOIN calificaciones c1 ON c1.idcalificacion = n.presencia ";
    $sSQL .= "LEFT OUTER JOIN calificaciones c2 ON c2.idcalificacion = n.imagen ";
    $sSQL .= "LEFT OUTER JOIN calificaciones c3 ON c3.idcalificacion = n.contextura ";
    $sSQL .= "LEFT OUTER JOIN calificaciones c4 ON c4.idcalificacion = n.trato ";
    $sSQL .= "LEFT OUTER JOIN calificaciones c5 ON c5.idcalificacion = n.comunicacion ";
    $sSQL .= "LEFT OUTER JOIN calificaciones c6 ON c6.idcalificacion = n.nivelsociocultural ";
    $sSQL .= "LEFT OUTER JOIN calificaciones c7 ON c7.idcalificacion = n.flexibilidad ";
    $sSQL .= "LEFT OUTER JOIN calificaciones c8 ON c8.idcalificacion = n.dinamismo ";
    $sSQL .= "LEFT OUTER JOIN calificaciones c9 ON c9.idcalificacion = n.actitud ";
    $sSQL .= "LEFT OUTER JOIN calificaciones c10 ON c10.idcalificacion = n.perfilcomercial ";
    $sSQL .= "LEFT OUTER JOIN calificaciones c11 ON c11.idcalificacion = n.experiencia ";
    $sSQL .= "LEFT OUTER JOIN calificaciones c12 ON c12.idcalificacion = n.conceptogeneral ";
    $sSQL .= "WHERE n.idnota = " . $pIdNota . " ";

    $cBD = new BD();
    $aRegistro = $cBD->Seleccionar($sSQL, true);
    if ($aRegistro) {
        $sHTML = '<table width="600" border="0" align="center" cellpadding="2" cellspacing="0" class="informe-texto" style="margin: 5px auto;">';
        $sHTML .= '<tr valign="top">';
        $sHTML .= '<td width="110"><strong>Presencia: </strong>' . $aRegistro["presencia"] . '</td>';
        $sHTML .= '<td width="140"><strong>Comunicaci&oacute;n: </strong>' . $aRegistro["comunicacion"] . '</td>';
        $sHTML .= '<td width="140"><strong>Actitud/Disposici&oacute;n: </strong>' . $aRegistro["actitud"] . '</td>';
        $sHTML .= '<td width="210" rowspan="4"><strong>Notas: </strong>' . $aRegistro["nota"] . '</td>';
        $sHTML .= '</tr>';
        $sHTML .= '<tr valign="top">';
        $sHTML .= '<td><strong>Imagen: </strong>' . $aRegistro["presencia"] . '</td>';
        $sHTML .= '<td><strong>Nivel socio-cultural: </strong>' . $aRegistro["nivelsociocultural"] . '</td>';
        $sHTML .= '<td><strong>Perfil comercial: </strong>' . $aRegistro["perfilcomercial"] . '</td>';
        $sHTML .= '</tr>';
        $sHTML .= '<tr valign="top">';
        $sHTML .= '<td><strong>Contextura: </strong>' . $aRegistro["contextura"] . '</td>';
        $sHTML .= '<td><strong>Flexibiildad: </strong>' . $aRegistro["flexibilidad"] . '</td>';
        $sHTML .= '<td><strong>Experiencia: </strong>' . $aRegistro["experiencia"] . '</td>';
        $sHTML .= '</tr>';
        $sHTML .= '<tr valign="top">';
        $sHTML .= '<td><strong>Trato: </strong>' . $aRegistro["trato"] . '</td>';
        $sHTML .= '<td><strong>Dinamismo: </strong>' . $aRegistro["dinamismo"] . '</td>';
        $sHTML .= '<td><strong>Concepto general: </strong>' . $aRegistro["conceptogeneral"] . '</td>';
        $sHTML .= '</tr>';
        $sHTML .= '</table>';
    }
    return $sHTML;
}

////////////////////////////////////////////////////////////////////////////
function CambiarEstado($pIDPropuesta, $pIdPostulante, $pEstado) {
    $sSQL = "UPDATE postulados SET estado = " . $pEstado . " ";

    $sSQL .= "WHERE idpostulante = " . $pIdPostulante . " ";
    $sSQL .= "AND idpropuesta = " . $pIDPropuesta . " ";

    $cBD = new BD();
    $aRegistro = $cBD->Seleccionar($sSQL, true);
    if ($aRegistro) {
        $sHTML = '<table width="600" border="0" align="center" cellpadding="2" cellspacing="0" class="informe-texto" style="margin: 5px auto;">';
        $sHTML .= '<tr><td width="300"><strong>Fecha de Registro:</strong> ' . date("d-m-Y", strtotime($aRegistro["fecha_registro"])) . '</td>';
        $sHTML .= '<td width="300"><strong>Fecha &uacute;ltima Modificaci&oacute;n:</strong> ' . date("d-m-Y", strtotime($aRegistro["fecha_modificado"])) . '</td></tr>';

        $sHTML .= '<tr><td width="300"><strong>Apellido y Nombres:</strong> ' . ReemplazarCaracteres($aRegistro["apellido"]) . ", " . ReemplazarCaracteres($aRegistro["nombres"]) . '</td>';
        $sHTML .= '<td width="300"><strong>Pa&iacute;s:</strong> ' . ReemplazarCaracteres($aRegistro["pais"]) . '</td></tr>';

        $sHTML .= '<tr><td width="300"><strong>Email:</strong> <a href="mailto:' . ReemplazarCaracteres($aRegistro["email"]) . '">' . ReemplazarCaracteres($aRegistro["email"]) . '</a></td>';
        $sHTML .= '<td width="300"><strong>Provincia:</strong> ' . ReemplazarCaracteres($aRegistro["provincia"]) . '</td></tr>';

        $sHTML .= '<tr><td width="300"><strong>Tel&eacute;fono:</strong> ' . ReemplazarCaracteres($aRegistro["telefono"]) . ' ';
        if ($aRegistro["celular"])
            $sHTML .= '/ <strong>Celular:</strong> ' . ReemplazarCaracteres($aRegistro["celular"]) . ' ';
        $sHTML .= '</td><td width="300"><strong>Localidad:</strong> ' . ReemplazarCaracteres($aRegistro["localidad"]) . '</td></tr>';

        $sHTML .= '<tr><td width="300"><strong>Usuario:</strong> ' . ReemplazarCaracteres($aRegistro["usuario"]) . '</td>';
        $sHTML .= '<td width="300"><strong>Clave:</strong> ' . ReemplazarCaracteres($aRegistro["clave"]) . '</td></tr>';
        $sHTML .= '</table>';
    }
    return $sHTML;
}

////////////////////////////////////////////////////////////////////////////
function AsociarPostulante($pIDPropuesta, $pIdPostulante) {
    $sSQL = "INSERT INTO postulacion (PerNro, OfeNro, PosFecha) VALUES (";
    $sSQL .= $pIdPostulante . ", " . $pIDPropuesta . ", '" . date("Y-m-d") . "') ";

    $cBD = new BD();
    $aRegistro = $cBD->Ejecutar($sSQL, true);
}

////////////////////////////////////////////////////////////////////////////
function GenerarAtributos($pIDGrupo) {
    $sSQL = "SELECT a.AtrNro, a.AtrNombre, a.AtrTipoDato FROM atributo a ";
    $sSQL .= "INNER JOIN grupoatributo ga ON a.AtrNro = ga.AtrNro ";
    $sSQL .= "WHERE ga.GruNro =  " . $pIDGrupo;

    $cBD = new BD();
    $oResultado = $cBD->Seleccionar($sSQL);
    if ($cBD->ContarFilas($oResultado) > 0) {
        $sHTML = "<table cellpadding=0 cellspacing=0 border=0>";
        while ($aRegistro = $cBD->RetornarFila($oResultado)) {
            $sHTML .= "<tr>";
            $sHTML .= "<td><input type='checkbox' value='" . $aRegistro["AtrNro"] . "' name='Atributos[]'></td>";
            $sHTML .= "<td>" . $aRegistro["AtrNombre"] . "</td>";
            if ($aRegistro["AtrTipoDato"] == "B") {
                $valores = array(array("-", "-"), array("Si", "Si"), array("No", "No"));

                $sHTML .= "<td><select name='valor-" . $aRegistro["AtrNro"] . "'>" . GenerarOptionsArray($valores, "-") . "</select></td>";
            }
            $sHTML .= "</tr>";
        }
        $sHTML .= "</table>";
    }

    return $sHTML;
}

function CambiaAtributoValor($IDRegistro, $IDPersona, $IDGrupo, $IDAtributo, $sValor) {
    if ($IDRegistro == 0) {
        $sSQL = "INSERT INTO descrippersona (PerNro, EmpNro, GruNro, AtrNro, DPerValor) ";
        $sSQL .= "VALUES (" . $IDPersona . ", " . RetornarIdEmpresa() . ", " . $IDGrupo . ", " . $IDAtributo . ", '" . $sValor . "') ";
    } elseif ($IDRegistro > 0 && strlen($sValor) > 0) {
        $sSQL = "UPDATE descrippersona SET DPerValor = " . $sValor;
        $sSQL .= "WHERE DPerNro = " . $IDRegistro;
    } elseif ($IDRegistro > 0 && strlen($sValor) == 0) {
        $sSQL = "DELETE FROM descrippersona WHERE DPerNro = " . $IDRegistro;
    }

    $cBD = new BD();
    $cBD->Ejecutar($sSQL);
}

////////////////////////////////////////////////////////////////////////////
function EliminarPostulado($pIdPostulado) {
    $sSQL = "DELETE FROM postulados WHERE idpostulado = " . $pIdPostulado;

    $cBD = new BD();
    $aRegistro = $cBD->Ejecutar($sSQL, true);
}

function EliminarCalificacionCliente($pIdPersona, $pIdCliente) {
    $sSQL = "DELETE FROM personacliente WHERE PerNro = " . $pIdPersona;
    $sSQL .= " AND cliNro = " . $pIdCliente;

    $cBD = new BD();
    $aRegistro = $cBD->Ejecutar($sSQL, true);
}

/* * *** VISTAS RAPIDAS **** */

function VerCliente($pId) {
    $sSQL = "SELECT CliRubro, CliDomicilio, CliEMail, ";
    $sSQL .= "CliEMailEjCta, CliFechaInicio, CliFechaFin ";
    $sSQL .= "FROM cliente ";
    $sSQL .= "WHERE CliNro = " . $pId . " ";

    $cBD = new BD();
    $aRegistro = $cBD->Seleccionar($sSQL, true);
    if ($aRegistro) {
        $sHTML = '<table width="600" border="1" align="center" cellpadding="0" cellspacing="0" class="vista">';
        $sHTML .= '<tr><td width="250"><strong>Rubro:</strong> ' . ReemplazarCaracteres($aRegistro["CliRubro"]) . '</td>';
        $sHTML .= '<td width="350"><strong>Domicilio:</strong> ' . ReemplazarCaracteres($aRegistro["CliDomicilio"]) . '</td></tr>';

        $sHTML .= '<tr><td><strong>Email:</strong> <a href="mailto:' . $aRegistro["CliEMail"] . '">' . $aRegistro["CliEMail"] . '</a></td>';
        $sHTML .= '<td><strong>Email Ejecutivo de cuenta:</strong>  <a href="mailto:' . $aRegistro["CliEMailEjCta"] . '">' . $aRegistro["CliEMailEjCta"] . '</a></td></tr>';

        $sHTML .= '<tr><td><strong>Fecha de Inicio:</strong> ' . date("d-m-Y", strtotime($aRegistro["CliFechaInicio"])) . '</td>';
        $sHTML .= '<td><strong>&Uacute;ltima Modificaci&oacute;n:</strong> ' . date("d-m-Y", strtotime($aRegistro["CliFechaFin"])) . '</td></tr>';
        $sHTML .= '</table>';
    }
    return $sHTML;
}

/* * *** CARGA COMBOS **** */

function CargarListas($pPadre, $pHijo) {
    $pHijo = explode("_", $pHijo);
    if (count($pHijo > 1))
        $pHijo = $pHijo[count($pHijo) - 1];
    else
        $pHijo = $pHijo[0];

    switch ($pHijo) {
        case "UniNro":
            $sSQL = "SELECT UniNro, UniNombre FROM unidadorg ";
            break;
        case "PueNro":
        case "PueNro[]":
        case "PueNro1[]":
            $sSQL = "SELECT PueNro, PueNom FROM puesto WHERE AreNro = " . $pPadre;
            break;
        case "EtuAreaEstudio":
            if ($pPadre == 2)
                $sSQL .= "SELECT AresNro, AresNombre FROM areassecundarias ";
            elseif ($pPadre == 4)
                $sSQL .= "SELECT AreuNro, AreuNombre FROM areasuniversitarias ";
            break;
    }

    $cBD = new BD();
    $oResultado = $cBD->Seleccionar($sSQL);
    $sHTML = $pHijo . ":|:";
    while ($aRegistro = $cBD->RetornarFila($oResultado)) {
        $sHTML .= $aRegistro[0] . ":|:" . ReemplazarCaracteres($aRegistro[1]) . ":|:";
    }
    return $sHTML;
}

/* * *** INGRESO USUARIO **** */

function ValidarUsuario($pEmpresa, $pUsuario, $pClave, $pRedirect) {
//   $sSQL = "SELECT MEmpNro, em.MEmpAdmin AS tipo_usuario, MEmpUsuario, MEmpApellido, MEmpNombres, em.EmpNro, EmpRSocial, MEmpEmail, em.PerNro, em.UniNro ";
    $sSQL = "SELECT MEmpNro, user_type.tipo_id AS tipo_usuario, user_reports.reportes AS reportes, MEmpUsuario, MEmpApellido, MEmpNombres, em.EmpNro, EmpRSocial, MEmpEmail, em.PerNro, em.UniNro ";
    $sSQL .= "FROM miembroempresa em INNER JOIN empresa e ON em.EmpNro = e.EmpNro ";
    $sSQL .= "LEFT JOIN user_type ON em.PerNro = user_type.idusuario ";
    $sSQL .= "LEFT JOIN user_reports ON em.PerNro = user_reports.idusuario ";
    $sSQL .= "INNER JOIN passwords p ON p.user_id = em.PerNro ";
    $sSQL .= "WHERE MEmpUsuario = '" . $pUsuario . "' ";
    $sSQL .= "AND p.password = '" . $pClave . "' ";
    $sSQL .= "AND em.EmpNro = " . $pEmpresa . " ";
//    error_log("aca");
//    var_dump($sSQL);
    $cBD = new BD();
    $aRegistro = $cBD->Seleccionar($sSQL, true);
    if (!$aRegistro) {
        session_unset();
        session_destroy();
        $sHTML = "N";
    } else {
        $_SESSION["idempresa"] = $aRegistro["EmpNro"];
        $_SESSION["idmiembro"] = $aRegistro["MEmpNro"];
        $_SESSION["usuario"] = $aRegistro["MEmpUsuario"];
        $_SESSION["tipo_usuario"] = $aRegistro["tipo_usuario"];
        $_SESSION["persona"]["apellido"] = $aRegistro["MEmpApellido"];
        $_SESSION["persona"]["nombres"] = $aRegistro["MEmpNombres"];
        $_SESSION["persona"]["email"] = $aRegistro["MEmpEmail"];
        $_SESSION["empresa"] = $aRegistro["EmpRSocial"];
        $_SESSION["empresa_id"] = $aRegistro["UniNro"];
        $_SESSION["PerNro"] = $aRegistro["PerNro"];
        $_SESSION["reportes"] = $aRegistro["reportes"];

        if ($aRegistro["tipo_usuario"] == 1) {
            $_SESSION["strict_menu"] = true;
        }
        if ($pRedirect != "") {
            $sHTML = $pRedirect;
        } else {
            $sHTML = "S";
        }
    }
    return $sHTML;
}

?>
