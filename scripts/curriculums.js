$(document).ready (function() {

  
   $("div.ddu_campo_comandos button").button();
   $("input[name='PerFechaNac']").datepicker(dp_options);
   $("input[name='EtuIngreso']").datepicker(dp_options);
   $("input[name='EtuEgreso']").datepicker(dp_options);
   $("#dca_ofertas_filtro input[name='dp_fecha_desde']").datepicker(dp_options);
   $("#dca_ofertas_filtro input[name='dp_fecha_hasta']").datepicker(dp_options);
   $("input[name='HIDDEN_fecha_alta']").datepicker(dp_options);
   $("input[name='HIDDEN_fecha_baja']").datepicker(dp_options);
  
   var yearRange = $( "input[name='PerFechaNac']" ).datepicker( "option", "yearRange" );

	$( "input[name='PerFechaNac']" ).datepicker( "option", "yearRange", '1970:2012' );
 	
	$( "#PerFechaNac" ).datepicker({
				changeYear: true,
				dateFormat: 'dd-mm-yy'
			}); 
  
	
	$( "input[name='EtuIngreso']" ).datepicker( "option", "yearRange", '1970:2012' );
 	$( "input[name='EtuEgreso']" ).datepicker( "option", "yearRange", '1970:2012' );
	$( "input[name='EtuIngreso']" ).datepicker({
				changeYear: true,
				dateFormat: 'dd-mm-yy'
			}); 			
   
 actualizar_curriculums_listado();
   /* dialog nuevo documento */
   $("#dlg_nuevo_documento").dialog({
      modal : true,
      autoOpen : false,
      title : "Agregar Documento",
      width : 390,
      height: 200,
      buttons : {
	 "Guardar" : function() {
	    guardar_documento();
	    $(this).dialog("close");
	 },
	 "Cancelar" : function() {
	    $(this).dialog("close");
	 }
      }
   });

   /* dialog nuevo cliente */
   $("#dlg_nuevo_cliente").dialog({
      modal : true,
      autoOpen : false,
      width : 390,
      title : "Agregar Cliente",
      buttons : {
	 "Guardar" : function() {
	    guardar_cliente();
	    $(this).dialog("close");
	 },
	 "Cancelar" : function() {
	    $(this).dialog("close");
	 }
      }
   });
   
   /* dialog nuevo experiencia laboral */
   $("#dlg_nuevo_ex").dialog({
      modal : true,
      autoOpen : false,
      width : 390,
      title : "Experiencia Laboral",
      buttons : {
	 "Cancelar" : function() {
	    $(this).dialog("close");
	 }
      }
   });
   
      /* dialog nuevo estudios */
   $("#dlg_nuevo_estudios").dialog({
      modal : true,
      autoOpen : false,
      width : 390,
      title : "Educacion",
      buttons : {
	 "Cancelar" : function() {
	    $(this).dialog("close");
	 }
      }
   });
   
   /* dialog principal */
   $("#div_dialog_curriculums").dialog({
      modal: true,
      width: 823,
      height: "auto",
      title: "Titulo",
      autoOpen: false,
      buttons: {
         "Exportar PDF" : function(){
             exportar_curriculum();
         },
         "Imprimir" : function(){
             imprimir_curriculum();
         },
	 "Guardar" : function() {
	    guardar_perfil();
	 },
	 "Cerrar" : function() {
	    $("#div_dialog_curriculums").dialog("close");
	 }
      },
      open: function () {
	 $("#curriculum_tabs").tabs("select", 0);
      }
   });

   /* dialog asignaciones */
   $("#dialog_cvs_asignaciones").dialog({
      modal: true,
      autoOpen: false,
      width: 760,
      height: "auto",
      buttons: {
	 "Cerrar": function() {
	    $(this).dialog("close");
	 }
      }
   });

   $("#curriculum_tabs").tabs({
      
      select: function(event, ui) {

	 var tab = $(ui.panel).attr("id");

	 switch (tab) {
	 
	    case "tab_documentos" :
	       var div = $("#div_cvs_documentos");
	       if ($(div).html() == "") cargar_documentos();
	       break;

	    case "tab_clientes" :
	       var div = $("#div_cvs_clientes");
	       if ($(div).html() == "") cargar_clientes();
	       break;
		   
		   case "tab_postulaciones" :
	       var div = $("#div_cvs_postulaciones");
	      cargar_postulaciones();
	       break;
	
		 case "tab_experiencia_laboral" :
	       var div = $("#div_cvs_clientes");
	       if ($(div).html() == "") cargar_explaboral();
	       break;
		   
		   case "tab_estudios" :
	       var div = $("#div_cvs_clientes");
	       if ($(div).html() == "") cargar_estudios();
	       break;
		   
	    default : break;

	 }
      }
   });

   /*
   $("#frm_nuevo_cliente").submit(function() {
      cargar_clientes();
   });
   */

   /*
   $("#frm_nuevo_documento").submit(function() {
	 alert("lala");
   });
   */
});

/*
function guardar_asignacion() {

   var id = $("#dialog_cvs_asignaciones input#dca_id_postulante").val();

   $("#form_cvs_asignacion").ajaxForm({
      data: { PerNro: id },
      success: function(res) {
	 cargar_asignaciones();
      }
   });

}
*/

function asignar_postulante_oferta(id_oferta) {

   var res = confirm("Desea asignar este Postulante a esta Oferta?");
   if (res == false) return;
   
   var id_postulante = $("#dialog_cvs_asignaciones input#dca_id_postulante").val();

   $.ajax({
      type: "post",
      url:  "abm.php?tabla=postulacion",
      data: {
	 PerNro: id_postulante,
	 OfeNro: id_oferta
      },
      success: function(res) {
	 if (res.length > 0) {
	    //alert("Postulación Efectuada con Exito");
	    dca_buscar_ofertas();
	 }
      }
   });

}

function dca_buscar_ofertas() {

   var id = $("#dialog_cvs_asignaciones input#dca_id_postulante").val();

   $("#dca_ofertas_filtro").ajaxSubmit({
      data: {
	 id_postulante: id
      },
      success: function(res) {
	 $("#dca_lista_ofertas").html(res);
      }
   });

}

function mostrar_asignaciones(id_postulante) {
   $("#dialog_cvs_asignaciones input#dca_id_postulante").val(id_postulante);
   /*cargar_asignaciones();*/
   $("#dca_ofertas_filtro").clearForm();
   dca_buscar_ofertas();
   $("#dialog_cvs_asignaciones").dialog("open");
}

/*
function cargar_asignaciones() {

   var id = $("#dialog_cvs_asignaciones input#dca_id_postulante").val();

   $.ajax({
      type: "get",
      url:  "feed.curriculums.php?objeto=asignacion&id_postulante=" + id,
      success: function(res) {
	 $("#dca_listado_asignaciones").html(res);
      }
   });
}
*/

function actualizar_curriculums_listado() {

   var form = $("#frm_curriculums_listado_filtro");
   var div = $("#div_curriculums_listado");

   $.ajax({
      type: "get",
      url: "feed.curriculums.php?objeto=principal",
      data: $(form).serialize(),
      beforeSend: function() {
      },
      success: function(msg) {
	 $(div).html(msg);
      }
   });
}

function nuevo_curriculum() {
   
   $("#id_postulante").val(0);
   $("#div_cvs_clientes").html("");
   $("#div_cvs_documentos").html("");
   $("#div_dialog_curriculums form").clearForm();
   $("#frm_nuevo_documento input[name='PerNro']").val(0);
   $("#div_dialog_curriculums").dialog("option", "title", "Nuevo Postulante");
   $("#div_dialog_curriculums").dialog("open");
}

function editar_curriculum(id_post) {
   $("#id_postulante").val(id_post);
   $("#frm_curriculums_notas").clearForm();
   $("#frm_nuevo_documento input[name='PerNro']").val(id_post);

   $.getJSON("feed.curriculums.php?objeto=detalle&id_postulante=" + id_post,
      function(ret) {
	 llenar_formulario("frm_cv_notas", ret);
	 var title = ret["PerApellido"] + ", " + ret["PerNombres"];
	 $("#div_dialog_curriculums").dialog("option", "title", title);
      }
   );

   /* cambiar titulo de ventana */
   //$("#div_dialog_curriculums").dialog({title: 

   $("#div_cvs_clientes").html("");
   $("#div_cvs_documentos").html("");
   $("#div_dialog_curriculums").dialog("open");

}

function cargar_documentos() {

   var id_postulante = $("input#id_postulante").val();

   $.ajax({
      type  : "get",
      url   : "feed.curriculums.php?objeto=documentos&id_postulante=" + id_postulante,
      beforeSend : function() {
      },
      success : function(ret) {
	 $("#div_cvs_documentos").html(ret);
      }
   });

}

function cargar_clientes() {

   var id_postulante = $("input#id_postulante").val();

   $.ajax({
      type : "get",
      url : "feed.curriculums.php?objeto=clientes&id_postulante=" + id_postulante,
      beforeSend : function() {
      },
      success : function(res) {
	 $("#div_cvs_clientes").html(res);
      }
   });
}

function cargar_postulaciones() {

   var id_postulante = $("input#id_postulante").val();

   $.ajax({
      type : "get",
      url : "feed.curriculums.php?objeto=postulaciones&id_postulante=" + id_postulante,
      beforeSend : function() {
      },
      success : function(res) {
	 $("#div_cvs_postulaciones").html(res);
      }
   });
}

function actualizar_estado(idvalor,idpostulacion) {
	$.ajax({
      type: "get",
      url:  "abm.php?tabla=actualizarestado&columna=id_postulacion&idregistro=" + idpostulacion + "&idvalor=" +idvalor,
      success: function(res) {
	  alert ("Actualizacion Realizada")
      }
   });
	
}

function cargar_explaboral() {

   var id_postulante = $("input#id_postulante").val();

   $.ajax({
      type : "get",
      url : "feed.curriculums.php?objeto=explaboral&id_postulante=" + id_postulante,
      beforeSend : function() {
      },
      success : function(res) {
	 $("#div_cvs_explaboral").html(res);
      }
   });
}

function cargar_estudios() {

   var id_postulante = $("input#id_postulante").val();

   $.ajax({
      type : "get",
      url : "feed.curriculums.php?objeto=estudios&id_postulante=" + id_postulante,
      beforeSend : function() {
      },
      success : function(res) {
	 $("#div_cvs_estudios").html(res);
      }
   });
}

function nuevo_documento() {
   var id_postulante = $("#tab_notas input[name='id_registro']").val();
   $("#frm_nuevo_documento").clearForm();
   $("#frm_nuevo_documento input[name='PerNro']").val(id_postulante);
   $("#dlg_nuevo_documento").dialog("open");
}

function nuevo_cliente() {
   var id_postulante = $("#tab_notas input[name='id_registro']").val();
   $("#frm_nuevo_cliente input[name='PerNro']").val(id_postulante);
   $("#dlg_nuevo_cliente").dialog("open");
}


function cargar_expl(nro) {
   $("#dlg_nuevo_ex").dialog("open").html("<iframe src='popup.php?id="+nro+"' width='365 ' height='350' ></iframe>");
}

function cargar_estudio(nro) {
   $("#dlg_nuevo_estudios").dialog("open").html("<iframe src='popup-estudios.php?id="+nro+"' width='365 ' height='350' ></iframe>");
}

function guardar_perfil() {

   var id_postulante = $("#div_dialog_curriculums input#id_postulante").val() * 1;
   document.nuevo = true;
   document.exito = true;

   /* definicion de urls */
   var url_datos	= "abm.php?tabla=persona";
   var url_xp_laboral	= "abm.php?tabla=datospuesto";
   var url_estudios	= "abm.php?tabla=estudio";
   var url_notas	= "abm.php?tabla=postulantenotas";

   if (id_postulante > 0) {

      document.nuevo = false;

      url_datos	     += "&columna=PerNro&idregistro="	 + id_postulante;
      url_xp_laboral += "&columna=DPueNro&idregistro="	 + $("input[name='DPueNro']").val();
      url_estudios   += "&columna=EtuNro&idregistro="	 + $("input[name='EtuNro']").val();
      url_notas	     += "&columna=NotNro&idregistro="	 + $("input[name='NotNro']").val();

   } 

   $.ajax({	     /* envia los datos del postulante */
      type: "post",
      url: url_datos,
      data: $("#frm_cv_datos_personales").serialize() + "&" + $("#frm_cv_datos_laborales").serialize(),
      success: function(res) {

	 var campo = "";
	 if (document.nuevo == true) {
	    if (res.length == 0) {
	       alert("Error al Agregar Registro");
	       document.exito = false;
	       return;
	    }
	    campo = "&PerNro=" + res;
	 }

	// $.ajax({    /* envia los datos de experiencia laboral */
	//  type: "post",
	// url:  url_xp_laboral,
	//data: $("#frm_cv_xp_laboral").serialize() + campo,
	//success: function(res) {
	//}
	//});

	 $.ajax({    /* envia los datos de estudios */
	    type: "post",
	    url:  url_estudios,
	    data: $("#frm_cv_estudios").serialize() + campo,
	    success: function(res) {
	    }
	 });

	 $.ajax({    /* envia los datos de notas */
	    type: "post",
	    url:  url_notas,
	    data: $("#frm_cv_notas").serialize() + campo,
	    success: function(res) {
	    }
	 });

	 $("#div_dialog_curriculums").dialog("close");
	 actualizar_curriculums_listado();

      }
   });
}

function guardar_documento() {

   var id_postulante = "&PerNro=" + $("#div_dialog_curriculums input#id_postulante").val();
   var data = $("#frm_nuevo_documento").serialize() + id_postulante;

   $("input[name='FILE_documento']").upload("abm.php?tabla=documentacion", data, function(res) {

      if ($(this).val().length > 0 && res != "file_OK") {
	 alert("Error al subir archivo");
	 return;
      }
      cargar_documentos();
   });
}

function guardar_cliente() {

   var id_postulante = "&PerNro=" + $("#div_dialog_curriculums input#id_postulante").val();

   $.ajax({
      type: "post",
      url:  "abm.php?tabla=personacliente",
      data: $("#frm_nuevo_cliente").serialize() + id_postulante,
      success: function(res) {
	 cargar_clientes();
      }
   });

}

function guardar_experiencia() {
	alert($("#frm_cv_xp_laboral").serialize());
}

function guardar_datos_personales() {
   $("#frm_cv_datos_personales").submit();
}

function eliminar_documento(id_postulante, id_documento) {

   var res = confirm("¿Desea Eliminar este Documento?");
   if (res == false) return;

   var url = "abm.php?tabla=documentacion&columna=DNro&idregistro=" + id_documento + "&id_postulante=" + id_postulante;

   $.ajax({
      type : "get",
      url : "abm.php",
      data : {
	 tabla : "documentacion",
	 columna : "DNro",
	 idregistro : id_documento,
	 id_postulante : id_postulante
      },
      success : function(res) {
	 cargar_documentos();
      }
   });

}

function eliminar_cliente(id_postulante) {

   var res = confirm("¿Desea Eliminar el Elemento Seleccionado?");
   if (res == false) return;

   var url_string = "abm.php?tabla=personacliente&columna=PerCli&idregistro=" + id_postulante;

   $.ajax({
      type : "get",
      url : url_string,
      success: function(msg) {
	 cargar_clientes();
      }
   });

}

function eliminar_cv(id_cv) {

   var res = confirm("¿Desea Eliminar esta Persona?");
   if (res == false) return false;

   $.ajax({
      type: "get",
      url:  "abm.php?tabla=persona&columna=PerNro&idregistro=" + id_cv,
      success: function(res) {
      }
   });

  actualizar_curriculums_listado();
}

function eliminar_postulaciones(id_cv) {

   var res = confirm("¿Desea Eliminar esta Postulacion?");
   if (res == false) return false;

   $.ajax({
      type: "get",
      url:  "abm.php?tabla=postulacion&columna=id_postulacion&idregistro=" + id_cv,
      success: function(res) {
	   cargar_postulaciones();
      }
   });
}

function imprimir_curriculum() {
    var id_postulante = $("#div_dialog_curriculums input#id_postulante").val();
    //alert(id_postulante);
    window.open('./curriculum.detalle.print.php?id_postulante=' + id_postulante);
}

function exportar_curriculum() {
    var id_postulante = $("#div_dialog_curriculums input#id_postulante").val();
    //alert(id_postulante);
    window.open('./curriculum.detalle.print.php?id_postulante=' + id_postulante + '&PDF=1');
}