$(document).ready (function() {

   actualizar_trayectoria_listado();
   $("#frm_trayectoria_listado_filtro select").change(actualizar_trayectoria_listado());

});

function actualizar_trayectoria_listado() {

   $.ajax({
      type: "get",
      url:  "feed.trayectoria.php?objeto=principal",
      data: $("#frm_trayectoria_listado_filtro").serialize(),
      beforeSend : function() {
	 $("#div_trayectoria_listado").html('<img src="images/loading.gif" class="loading" />');
      },
      success: function(msg) {
	 $("#div_trayectoria_listado").html(msg);
      }
   });
}
