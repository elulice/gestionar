function actualizarEventos(){
   
    $.ajax({
        type: "GET",
        url: "feed.calendar.list.php",
        beforeSend: function(){
            $("#ultimos_eventos").html('<img src="images/loading.gif" class="loading" />');
        },
        success: function(msg){
            $("#ultimos_eventos").html(msg);
        }
    });
}


$(document).ready(function(){
    $("#calendario").fullCalendar('destroy');
    $("#calendario").fullCalendar({
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
        dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
        monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        buttonText: {
            today:  'Hoy',
            week:   'Semana',
            month:  'Mes',
            day:    'Día'
        },
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        theme: true,
        eventSources: [
        'feed-calendar.php'
        ],
        eventClick: function(calEvent, jsEvent, view) {
            $.ajax({
                type: "GET",
                url: "ver.php?elemento=evento&id="+calEvent.id,
                success: function(msg){
                    $("#dialog-response").html(msg);
                    $("#dialog-response").dialog('open');
                    $("#dialog-response").dialog("option", "title", '<span class="ui-icon ui-icon-calendar"></span> Eventos');
                }
            });
        },
        eventAfterRender: function( event, element, view ) {
            $('#dialogo-calendario-eventos').dialog('option', 'position', 'center');
        }
    });
    
    $('.datetime').datetimepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "yy-mm-dd",
        regional: 'es',
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
        dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
        dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
        monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        currentText: 'Ahora',
        closeText: 'Listo',
        ampm: false,
        timeFormat: 'hh:mm:ss',
        timeOnlyTitle: 'Fecha y hora',
        timeText: 'Hora',
        hourText: 'Hora',
        minuteText: 'Minutos',
        secondText: 'Segundos'
    });
    
    
    $( "#dialog:ui-dialog" ).dialog( "destroy" );
    $("#dialogo-calendario-eventos").dialog({
        modal: true,
        autoOpen: false,
        width: 800,
        height: 'auto',
        title: '<span class="ui-icon ui-icon-calculator"></span> Calendario',
        resizable: false
    });
    
    $( "#dialog-form-eventos" ).dialog({
        autoOpen: false,
        height: 'auto',
        width: 'auto',
        maxWidth: 450,
        modal: true,
        title: '<span class="ui-icon ui-icon-calendar"></span> Agregar Evento',
        resizable: false,
        buttons: {
            "Agregar Evento": function() {
                $.ajax({
                    type: "POST",
                    url: $("#formNuevoEvento").attr('action'),
                    data: $("#formNuevoEvento").serialize(),
                    success: function(msg){
                        $("#dialog-form-eventos").dialog("close");
                        actualizarEventos();
                    }
                });
            },
            "Cancelar": function() {
                $( this ).dialog( "close" );
                actualizarEventos();
            }
        }
    });
});
actualizarEventos();