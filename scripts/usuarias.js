$(document).ready (function() {
   
   actualizar_usuarias_listado();

   $("input[name='CliFechaInicio']").datepicker(dp_options);

   /* configuracion de dialog usuarias */
   $("#div_dialog_usuarias").dialog({
      modal: true,
      resizable: false,
      width: "400",
      autoOpen: false,
      title: "Datos de Cliente",
      buttons: {
	 "Guardar" : function() {
	    guardar_cliente();
	    $(this).dialog("close");
	    actualizar_usuarias_listado();
	 },
	 "Cancelar" : function() {
	    $(this).dialog("close");
	 }
      }
   });

});

function actualizar_usuarias_listado() {

   var div = $("#div_usuarias_listado");
   var form = $("#frm_usuarias_listado_filtro");

   $.ajax({
      type: "get",
      url: "feed.usuarias.php?objeto=principal",
      data: $(form).serialize(),
      beforeSend: function() {
	 $(div).html('<img src="images/loading.gif" class="loading" />');
      },
      success: function(res) {
	 $(div).html(res);
      }
   });
   
   return false;

}

function guardar_cliente() {

   var id_cliente = $("#div_dialog_usuarias input[name='hdn_id_cliente']").val();
   var url = "abm.php?tabla=cliente";

   if (id_cliente > 0) url += "&columna=CliNro&idregistro=" + id_cliente;

   var form = $("#frm_clientes_detalle");
   $(form).attr("action", url);
   $(form).submit();

   /*
   $.ajax({
      url: url_string,
      type: "post",
      data: $("#frm_clientes_detalle").serialize(),
      success: function() {
	 actualizar_usuarias_listado();
      }
   });
   */

}

function editar_cliente(id_cliente) {

   $("#frm_clientes_detalle").clearForm();
   $("#div_dialog_usuarias input[name='hdn_id_cliente']").val(id_cliente);

   $.getJSON("feed.usuarias.php?objeto=detalle&id_cliente=" + id_cliente, function(ret) {

      $.each(ret, function(key, value) {
	 $("input[name='" + key + "']").val(value);
      });
   });

   $("#div_dialog_usuarias").dialog("open");
}

function nuevo_cliente() {

   $("#div_dialog_usuarias input[name='hdn_id_cliente']").val("0");
   $("#frm_clientes_detalle").clearForm();
   $("#div_dialog_usuarias").dialog("open");
}

function eliminar_cliente(id_cliente) {

   var res = confirm("Desea eliminar este cliente?");

   if (res == true) {
      $.ajax({url: "abm.php?tabla=cliente&columna=CliNro&idregistro=" + id_cliente});
      actualizar_usuarias_listado();
   }
}