$(document).ready(function() {

    $("#dlg_detalle_usuario").dialog({
        modal: true,
        title: "Detalle de Usuario",
        width: 390,
        autoOpen: false,
        buttons: {
            "Guardar": function() {
                if (guardar_usuario()) return;
                cargar_lista_usuarios();
                $(this).dialog("close");
            },
            "Cancelar": function() {
                $(this).dialog("close");
            }
        }
    });

    cargar_lista_usuarios();
});

function cargar_lista_usuarios() {

    var form = $("#frm_usuarios_listado_filtro");

    $.ajax({
        type: "get",
        url: "feed.usuarios.php?objeto=principal",
        data: $(form).serialize(),
        beforeSend: function() {
            $("#div_usuarios_lista").html('<div src="images/loading.gif" class="loading" />');
        },
        success: function(res) {
            $("#div_usuarios_lista").html(res);
        }
    });
}

function nuevo_usuario() {
    $("#frm_detalle_usuario").clearForm();
    $("#dlg_detalle_usuario").dialog("open");
}


function guardar_usuario() {

    var pw1 = $("input[name='MEmpClave']").val();
    var id = $("input[name='MEmpNro']").val();
    var username = $("input[name='MEmpUsuario']").val();
    var pw2 = $("input#MEmpClave").val();

    if (pw1 != pw2) {
        alert("Las contraseñas no coinciden.");
        return true;
    }
    //if( validar_usuario(username)){
    //return true;
    //}
   
    //    $("#frm_detalle_usuario").ajaxSubmit({
    //        success: function(res) {
    //            alert("Usuario creado con éxito")
    //	  
    //        }
    //    });

    $.ajax({
        type: "post",
        url: $('#frm_detalle_usuario').attr('action') + id,
        data: $('#frm_detalle_usuario').serialize(),
        success: function(res) {
            alert("Guardado con éxito");
        }
    });
  
}

function validar_usuario(username) {
	
    $.ajax({
        type: "get",
        url: "feed.usuarios.php?objeto=usuario&username="+username,
        success: joel =function(res) {
            m=res;
            return "mam"+m
        }
    });
    alert(joel())
    return true;
}

function editar_usuario(id_usuario) {

    $.getJSON("feed.usuarios.php?objeto=detalle&id_usuario=" + id_usuario, function(ret) {
        llenar_formulario("frm_detalle_usuario", ret);
    });

    $("#dlg_detalle_usuario").dialog("open");

}

function eliminar_usuario(id_usuario) {

    var res = confirm("Desea eliminar este usuario?");

    if (res == true) {
        $.ajax({
            url: "abm.php?tabla=miembroempresa&columna=MEmpNro&idregistro=" + id_usuario
        });
        cargar_lista_usuarios();
    }
}

