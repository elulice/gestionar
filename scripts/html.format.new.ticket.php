<?php

switch ($status):
    case 1: $status = "Pendiente";
        break;
    case 2: $status = "En Proceso";
        break;
    case 3: $status = "Cerrado";
        break;
    default : $status = "Indefinido";
endswitch;

if (isset($dni) && $dni != "") {
    $dniNew = "[DNI: $dni]";
}

if ($type == "respuesta_cliente") {
    $body = '
<center>
<table style="font-family: Arial"> 
   <tr>
        <td width="650" colspan="2">
            <div style="width: 650px;">
                <img src="http://www.grupo-gestion.com.ar/gestion.ar/images/encabezado-mail-soporte.jpg"/>


                <p><font size="2"></font><font size="2">Su Reclamo ha sido resuelto</font></p>

                <p><font size="2"><strong>Reclamo: </strong>' . $content . '</font></p>
                
                <br/>
                <br/>
           <tr>
           <td>
                <p><strong><font size="2">Servicios al Cliente</strong></font></p>
                <font size="2"><a href="mailito:gestionresponde@grupo-gestion.com.ar">gestionresponde@grupo-gestion.com.ar</a></font><br/>
                
</td>
<td align="right">
                <p><strong><font size="2">Grupo Gesti&oacute;n</font></strong></p>
                <font size="2">Tel: (54 11)</font> <font size="2">4384-0660 / 0810 666 GESTION (4378466)</font><br/>
                <font size="2"><a href="mailito:gestionresponde@grupo-gestion.com.ar">gestionresponde@grupo-gestion.com.ar</a></font><br/>
                <font size="2"><a href="http://www.grupo-gestion.com.ar">www.grupo-gestion.com.ar</a></font><br/>
           </td>
           </tr>
        </td>
            </div>
        
    </tr>
    </table>
    </center>';
}
if ($type == "new_ticket") {
    $body = '
<center>
<table>
    <tr>
        <td width="650" colspan="2">
            <div style="width: 650px;">
                <img src="http://www.grupo-gestion.com.ar/gestion.ar/images/encabezado-mail-soporte.jpg"/>
                <p><font size="2"><strong>Estimado/a </strong>' . utf8_decode($nombreSol) . ',</font></p>

                <p><font size="2"><strong>Asunto: </strong></font><font size="2">TICKET NRO ' . $nroTicket . '</font></p>
                <p><font size="2"><strong>Status: </strong></font><font size="2">' . $status . '</font></p>

                <p><font size="2">' . $content . '</font></p>
                
                <br/>
                <br/>
           <tr>
           <td>
                <p><strong><font size="2">Servicios al Cliente</strong></font></p>
                <font size="2"><a href="mailito:gestionresponde@grupo-gestion.com.ar?subject=Respuesta de ' . $email . '">gestionresponde@grupo-gestion.com.ar</a></font><br/>
                <font size="2">Recibir&aacute; la respuesta con un nuevo e-mail.</font>
</td>
<td align="right">
                <p><strong><font size="2">Grupo Gesti&oacute;n</font></strong></p>
                <font size="2">Tel: (54 11)</font> <font size="2">4384-0660 / 0810 666 GESTION (4378466)</font><br/>
                <font size="2"><a href="mailito:gestionresponde@grupo-gestion.com.ar?subject=Respuesta de ' . $email . '">gestionresponde@grupo-gestion.com.ar</a></font><br/>
                <font size="2"><a href="http://www.grupo-gestion.com.ar">www.grupo-gestion.com.ar</a></font><br/>
           </td>
           </tr>
        </td>
            </div>
        
    </tr>
    </table>
    </center>';
}

if ($type == "sucresp") {
    $body = '
<center>
<table>
    <tr>
        <td width="650" colspan="2">
            <div style="width: 650px;">
                <img src="http://www.grupo-gestion.com.ar/gestion.ar/images/encabezado-mail-soporte.jpg"/>


                <p><font size="2">
                El usuario ' . $created_by . ' ha cargado un nuevo Ticket para ' . $nombreSol . ' ([' . strtoupper($tipo_usuario) . ']' . $dniNew . ')  con el Nro ' . $nroTicket . ' <br/><br/>
                Contenido del Ticket: ' . $contenido . '<br/><br/>
                Enlace al Ticket: http://www.grupo-gestion.com.ar/gestion.ar/?redirect=tickets.edit.php?id=' . $nroTicket . '<br/><br/>
                </font></p>
                
                <br/>
                <br/>
           <tr>
           <td>
                <p><strong><font size="2">Servicios al Cliente</strong></font></p>
                <font size="2"><a href="mailito:gestionresponde@grupo-gestion.com.ar">gestionresponde@grupo-gestion.com.ar</a></font><br/>
                
</td>
<td align="right">
                <p><strong><font size="2">Grupo Gesti&oacute;n</font></strong></p>
                <font size="2">Tel: (54 11)</font> <font size="2">4384-0660 / 0810 666 GESTION (4378466)</font><br/>
                <font size="2"><a href="mailito:gestionresponde@grupo-gestion.com.ar">gestionresponde@grupo-gestion.com.ar</a></font><br/>
                <font size="2"><a href="http://www.grupo-gestion.com.ar">www.grupo-gestion.com.ar</a></font><br/>
           </td>
           </tr>
        </td>
            </div>
        
    </tr>
    </table>
    </center>';
}
if ($type == "supervisor") {
    $body = '
<center>
<table>
    <tr>
        <td width="650" colspan="2">
            <div style="width: 650px;">
                <img src="http://www.grupo-gestion.com.ar/gestion.ar/images/encabezado-mail-soporte.jpg"/>


                <p><font size="2">
                El usuario ' . $nombreSol . ' ha respondido un Ticket <br/><br/>
                Si desea autorizar esta respuesta, ingrese al siguiente enlace: <br/><br/>
                Enlace al Ticket: http://www.grupo-gestion.com.ar/gestion.ar/?redirect=tickets.edit.php?id=' . $nroTicket . '<br/><br/>
                </font></p>
                
                <br/>
                <br/>
           <tr>
           <td>
                <p><strong><font size="2">Servicios al Cliente</strong></font></p>
                <font size="2"><a href="mailito:gestionresponde@grupo-gestion.com.ar">gestionresponde@grupo-gestion.com.ar</a></font><br/>
                
</td>
<td align="right">
                <p><strong><font size="2">Grupo Gesti&oacute;n</font></strong></p>
                <font size="2">Tel: (54 11)</font> <font size="2">4384-0660 / 0810 666 GESTION (4378466)</font><br/>
                <font size="2"><a href="mailito:gestionresponde@grupo-gestion.com.ar">gestionresponde@grupo-gestion.com.ar</a></font><br/>
                <font size="2"><a href="http://www.grupo-gestion.com.ar">www.grupo-gestion.com.ar</a></font><br/>
           </td>
           </tr>
        </td>
            </div>
        
    </tr>
    </table>
    </center>';
}
if ($type == "supervisoradmin") {
    $body = '
<center>
<table>
    <tr>
        <td width="650" colspan="2">
            <div style="width: 650px;">
                <img src="http://www.grupo-gestion.com.ar/gestion.ar/images/encabezado-mail-soporte.jpg"/>


                <p><font size="2">
                El usuario ' . $nombreSol . ' ha respondido un Ticket <br/><br/>
                    Respuesta: ' . $respuesta . '<br/><br/>
                Si desea visualizar el Ticket, ingrese al siguiente enlace: <br/><br/>
                Enlace al Ticket: http://www.grupo-gestion.com.ar/gestion.ar/?redirect=tickets.edit.php?id=' . $nroTicket . '<br/><br/>
                </font></p>
                
                <br/>
                <br/>
           <tr>
           <td>
                <p><strong><font size="2">Servicios al Cliente</strong></font></p>
                <font size="2"><a href="mailito:gestionresponde@grupo-gestion.com.ar">gestionresponde@grupo-gestion.com.ar</a></font><br/>
                
</td>
<td align="right">
                <p><strong><font size="2">Grupo Gesti&oacute;n</font></strong></p>
                <font size="2">Tel: (54 11)</font> <font size="2">4384-0660 / 0810 666 GESTION (4378466)</font><br/>
                <font size="2"><a href="mailito:gestionresponde@grupo-gestion.com.ar">gestionresponde@grupo-gestion.com.ar</a></font><br/>
                <font size="2"><a href="http://www.grupo-gestion.com.ar">www.grupo-gestion.com.ar</a></font><br/>
           </td>
           </tr>
        </td>
            </div>
        
    </tr>
    </table>
    </center>';
}
?>

<?php

echo $body;
?>