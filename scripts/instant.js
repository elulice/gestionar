function iniciar() {
    // Inicializa variables y eventos
    currentSearch = '';
    var searchBox = $('#searchBox');
    var searchBoxCVA = $('#inputApellido');
    var searchBoxCVN = $('#inputNombre');
    var searchBoxCVD = $('#inputDocumento');
    searchBox.keyup(doInstantSearch);
    searchBox.submit(doInstantSearch);
    searchBoxCVA.keyup(doInstantSearchCV);
    searchBoxCVN.keyup(doInstantSearchCV);
    searchBoxCVD.keyup(doInstantSearchCV);
    // Focus search box
    searchBox.focus();
}

function doInstantSearch() {
	var searchBox = $('#searchBox');
	var inputEjecutivo = $('#inputEjecutivo').val();
	if (currentSearch != searchBox.val()) {
			currentSearch = searchBox.val();
			 $.ajax ({
					type: "GET",
					url: "feed.usuarias.php",
					data: "objeto=principal&cliente=" + currentSearch + "&ejecutivo=" + inputEjecutivo,
					beforeSend: function() {
					   $("#div_usuarias_listado").html('<img src="images/loading.gif" class="loading" />');
  				        },
					success: function(res) {
					   $("#div_usuarias_listado").html(res);
	  				}
				});
			

		}
	
	/*
	else {
		// Podria hacer algo como esto.
		// $('#resultadoDiv').hide(1000);
	}
	*/
}

function doInstantSearchCV() {
	var apellido = $('#inputApellido').val();
	var nombre = $('#inputNombre').val();
	var documento = $('#inputDocumento').val();
	var dialog = $('#dialog').val();
	 var id_oferta = $("#dialog_pedidos_postulantes2 input#dpp_id_oferta").val();
	
	if(dialog){
	dialog= "&dialog=1";
	} else {
	dialog= "";
	}
	
	$.ajax ({
		type: "GET",
		url: "feed.curriculums.php",
		data: "objeto=principal" + dialog + "&txt_apellido=" + apellido + "&txt_nombre=" + nombre + "&txt_dni=" + documento + "&idoferta=" + id_oferta,
		beforeSend: function() {
		   $("#div_curriculums_listado").html('<img src="images/loading.gif" class="loading" />');
	        },
		success: function(res) {
		   $("#div_curriculums_listado").html(res);
		}
	 });
	if (apellido != '') {
		$.ajax ({
			type: "GET",
			dataType: 'json',
			url: 'feed.curriculums.nombres.autocomplete.php',
			data: 'apellido=' + apellido  + dialog +'&idoferta=' + id_oferta,
			success: function (res) { $(" .labelsNombres ").autocomplete("option", "source", res); }
		 });
		$.ajax ({
			type: "GET",
			dataType: 'json',
			url: 'feed.curriculums.documentos.autocomplete.php',
			data: 'apellido=' + apellido + dialog +'&nombre=' + nombre +'&idoferta=' + id_oferta,
			success: function (res) { $(" .labelsDocumentos ").autocomplete("option", "source", res); }
		 });
	}
}




