$(function() {
    $("#form-ticket-adjuntos").dialog({
        autoOpen: false,
        height: 'auto',
        width: 370,
        modal: true,
        resizable: false,
        title: '<span class="ui-icon ui-icon-link"></span> Adjuntar Archivo',
        buttons: {
            "Adjuntar": function() {
                $("#inc-files-polizas").html('<img src="images/loading.gif" class="loading" />');
                $("#formNuevoArchivo").submit();
            },
            "Cancelar": function() {
                $(this).dialog("close");
            }
        }
    });

    $("#form-ticket-adjuntos-list").dialog({
        autoOpen: false,
        height: 'auto',
        width: '365',
        //maxWidth: 580,
        modal: true,
        resizable: false,
        title: '<span class="ui-icon ui-icon-link"></span> Archivos adjuntos',
        resizeStop: function(event, ui) {
            $('#form-tickets-adjuntos-list').dialog('option', 'position', 'center');
        },
        buttons: {
            "Cerrar": function() {
                $(this).dialog("close");
            }
        },
        open: function() {
        },
        close: function() {
        }
    });
});

function cargaDatosEj(pSelect)
{
    var mEmpNro = pSelect.options[pSelect.selectedIndex].value;
    var mNombre = document.getElementById("CliEjCta");
    var mEmail = document.getElementById("CliEMailEjCta");
    x_CargarDatosEj(mEmpNro, function(pCadena)
    {
        var datos = pCadena.split(":|:");
        mNombre.value = datos[0];
        mEmail.value = datos[1];
    });
}

function validarExtension(pForm)
{
    var oArchivo = document.getElementById("FILE_cv;../postulantes/curriculums/");
    if (oArchivo.value != "")
    {
        var aExtension = oArchivo.value.split(".");
        if (aExtension[1] != "pdf" && aExtension[1] != "doc")
        {
            alert("Archivo de formato no valido: " + aExtension[1]);
            oArchivo.focus();
        }
    }
}


function formateaFecha(pFecha) {
    partes = pFecha.split("-");
    if (partes.length == 3)
    {
        pFecha = partes[2] + "-" + partes[1] + "-" + partes[0];
    }
    else
    {
        var d = new Date();
        pFecha = d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate();
    }
    return pFecha;
}

function corrigeFecha(pFecha) {
    var fechaNota = document.getElementById(pFecha);
    partes = fechaNota.value.split("-");
    if (partes.length == 3)
    {
        fechaNota.value = partes[2] + "-" + partes[1] + "-" + partes[0];
    }
    else
    {
        var d = new Date();
        fechaNota.value = d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate();
    }
    return true;
}

function corrigeFechas(pArregloFechas)
{
    for (var i = 0; i < pArregloFechas.length; i++)
    {
        var fechaNota = document.getElementById(pArregloFechas[i]);
        partes = fechaNota.value.split("-");
        if (partes.length == 3)
        {
            fechaNota.value = partes[2] + "-" + partes[1] + "-" + partes[0];
        }
        else
        {
            var d = new Date();
            fechaNota.value = d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate();
        }
    }
    return true;
}


/*******************************************/
// @pControl		combo principal
// @pRelacionado	combo cuyos valores estan relacionados al seleccionado en pControl
/*******************************************/
function cargarListas(pControl, pRelacionado)
{
    var oLista;
    var pValor = pControl.options[pControl.selectedIndex].value;
    oLista = document.getElementById(pRelacionado);
    oLista.length = 1;
    oLista.options[0].text = "Cargando...";
    oLista.disabled = true;

    if (pControl != "")
    {
        oLista = document.getElementById(pRelacionado);
        x_CargarListas(pValor, pRelacionado, function(pCadena)
        {
            var lJ;
            var lK = 0;
            var aDatos = pCadena.split(":|:");

            oLista.disabled = false;
            for (lJ = 1; lJ < aDatos.length; lJ++)
            {
                oLista[lK] = new Option(aDatos[lJ + 1], aDatos[lJ]);
                lJ++;
                lK++;
            }
        //alert(pCadena);
        });
    }
}


/**FUNCIONES DE DESPLIEGUE *************************************************************/
function visualizarBuscador(pIDElemento)
{
    var tContenedor = document.getElementById(pIDElemento);
    var mas = document.getElementById("BTN_mas");
    if (tContenedor.className != "listado-fila-visible")
    {
        tContenedor.className = "listado-fila-visible";
        mas.innerHTML = "&lt;&lt; Ocultar";
    }
    else
    {
        tContenedor.className = "listado-fila-oculta";
        mas.innerHTML = "Avanzado &gt;&gt;";
    }
}

function visualizarElemento(pIDElemento)
{
    var tContenedor = document.getElementById(pIDElemento);
    if (tContenedor.className != "listado-fila-visible")
        tContenedor.className = "listado-fila-visible";
    else
        tContenedor.className = "listado-fila-oculta";
}



function verPostulante(pIdPostulante, pPosicion)
{
    visualizarElemento("trPostulante-" + pIdPostulante);

    var tCelda = document.getElementById("tdPostulante-" + pIdPostulante);

    if (tCelda)
    {
        x_VerPostulante(pIdPostulante, pPosicion, function(pCadena)
        {
            tCelda.innerHTML = pCadena;
        });
    }
}




function verNota(pIdNota)
{
    visualizarElemento("trNota-" + pIdNota);

    var tCelda = document.getElementById("tdNota-" + pIdNota);

    if (tCelda)
    {
        x_VerNota(pIdNota, function(pCadena)
        {
            tCelda.innerHTML = pCadena;
        });
    }
}

function actualizarNombre()
{
    var oArchivo = document.getElementById("FILE_Archivo;../Documentacion/");
    var oTitulo = document.getElementById("DNombre");
    if (oArchivo.value != "")
    {
        var aNombre = oArchivo.value.split("\\");
        oTitulo.value = aNombre[aNombre.length - 1];
    }
    return true;
}


/*************************************************************************/
function verPostulanteFull(pIdPostulante)
{
    var oVentana = new Window(
    {
        className: "alphacube",
        title: "Detalles del Postulante",
        width: 800,
        height: 600,
        left: 110,
        top: 150,
        draggable: true,
        resizable: true,
        minimizable: false,
        maximizable: false,
        destroyOnClose: true
    }
    );
    oVentana.show();
    var sURL = "ver-postulante.php?idregistro=" + pIdPostulante;
    oVentana.setAjaxContent(sURL);
}

function verNotas(pIdPostulante)
{
    var oVentana = new Window(
    {
        className: "alphacube",
        title: "Notas sobre el Postulante",
        width: 780,
        height: 300,
        left: 110,
        top: 150,
        draggable: true,
        resizable: true,
        minimizable: false,
        maximizable: false,
        destroyOnClose: true
    }
    );
    oVentana.show();
    var sURL = "ver-postulante-notas.php?idregistro=" + pIdPostulante;
    oVentana.setAjaxContent(sURL);
}

function verOfertasParaPostular(pIdPostulante)
{
    var oVentana = new Window(
    {
        className: "alphacube",
        title: "Postular a una Oferta",
        width: 780,
        height: 300,
        left: 110,
        top: 150,
        draggable: true,
        resizable: true,
        minimizable: false,
        maximizable: false,
        destroyOnClose: true
    }
    );
    oVentana.show();
    var sURL = "ver-ofertas-para-postular.php?idregistro=" + pIdPostulante;
    oVentana.setAjaxContent(sURL);
}


/***** cambio de estado *******************/
function cambiarEstado(pIDPropuesta, pIDPerfil, pEstado)
{
    x_CambiarEstado(pIDPropuesta, pIDPerfil, pEstado, function(pCadena)
    {
        //tCelda.innerHTML = pCadena;
        });
}
/***** Asociar Postulante a Oferta *******************/
function asociarPostulante(pIDPropuesta, pIDPerfil)
{
    x_AsociarPostulante(pIDPropuesta, pIDPerfil, function(pCadena)
    {
        //tCelda.innerHTML = pCadena;
        });
}
/***** Postulante Postulante de Oferta *******************/
function eliminarPostulado(pIDPostulado)
{
    x_EliminarPostulado(pIDPostulado, function(pCadena)
    {
        //tCelda.innerHTML = pCadena;
        });
}
/***** Postulante Postulante de Oferta *******************/
function eliminarCalificacionCliente(pIDPersona, pIDCliente)
{
    x_EliminarCalificacionCliente(pIDPersona, pIDCliente, function(pCadena)
    {
        //tCelda.innerHTML = pCadena;
        });
    location.href = "listado-curriculums-clientes.php?idpostulante=" + pIDPersona;
}




/***** VISTAS RAPIDAS *****/
function verExperiencia(pIdExperiencia, pPosicion)
{
    visualizarElemento("trExperiencia-" + pIdExperiencia);

    var tCelda = document.getElementById("tdExperiencia-" + pIdExperiencia);

    if (tCelda)
    {
        x_VerExperiencia(pIdExperiencia, pPosicion, function(pCadena)
        {
            tCelda.innerHTML = pCadena;
        });
    }
}
function verEstudio(pIdEstudio, pNivel)
{
    visualizarElemento("trEstudio-" + pIdEstudio);

    var tCelda = document.getElementById("tdEstudio-" + pIdEstudio);

    if (tCelda)
    {
        x_VerEstudio(pIdEstudio, pNivel, function(pCadena)
        {
            tCelda.innerHTML = pCadena;
        });
    }
}

function verPostulados(pIdpropuesta, pPosicion)
{
    visualizarElemento("trOferta-" + pIdpropuesta);

    var tCelda = document.getElementById("tdOferta-" + pIdpropuesta);

    if (tCelda)
    {
        x_VerPostulados(pIdpropuesta, pPosicion, function(pCadena)
        {
            tCelda.innerHTML = pCadena;
        });
    }
}

function verCliente(Id)
{
    visualizarElemento("trCliente-" + Id);

    var tCelda = document.getElementById("tdCliente-" + Id);

    if (tCelda)
    {
        x_VerCliente(Id, function(pCadena)
        {
            tCelda.innerHTML = pCadena;
        });
    }
}

/***** INGRESO USUARIO *****/
function validarUsuario(pForm)
{
    with (pForm)
        x_ValidarUsuario(313, txtUsuario.value, txtClave.value, txtRedirect.value, function(pCadena)
        {
            if (pCadena == "S") {
                location.href = "escritorio.php";
            } else if (pCadena == "N") {
                alert("Usuario o Clave incorrecto");
                pForm.txtClave = "";
                txtUsuario.focus();
            } else {
                location.href = pCadena;
            }
        });
    return false;
}
//No borrar esta funci�n !
function editar_ticket(a) {
    window.location = 'tickets.edit.php?id=' + a;
}
$(document).keyup(function(e) {

    if (e.keyCode == 27) {
        $("#p_fecha_edit").datepicker("disable");
    }
});
function editTicket(a, b) {
    $.ajax({
        url: 'scripts/showTextOfTicketToEdit.php?idNum=' + a,
        success: function(data) {
            $('#' + b).html(data);
            $('textarea#p_content_edit_a').focus();
            $('#p_responsable_edit').change();
            return false;
        }
    });
}
function saveEditedTicket(a, b) {

    emailCliente = $('#p_email_cliente_new').val();
    emailSend = $('#email_new').val();
    emailCc = $('#cc_email_new').val();
    emailSucursal = $('#p_email_sucursal').val();
    emailResponsable = $('#p_email_responsable').val();

    fh = $('#p_fecha').val();
    nm = $('#p_nombres').val();
    ec = $('#p_email_cliente').val();
    cl = $('#p_cliente').val();
    or = $('#p_origen').val();
    su = $('#p_sucursal').val();
    re = $('#p_responsable').val();
    es = $('#p_estado').val();
    ems = $('#p_email_send').val();
    ecc = $('#p_email_cc').val();
    
    se = $("#sector_select_2").val();
    mt = $("#p_motivo").val();
    tpe = $("#tipo_error").val();
        
    

    fh_edit = $('#p_fecha_edit').val();
    nm_edit = $('#p_nombres_edit').val();
    ec_edit = $('#p_email_cliente_edit').val();
    cl_edit = $('#p_cliente_edit').val();
    or_edit = $('#p_origen_edit').val();
    su_edit = $('#p_sucursal_edit').val();
    re_edit = $('#p_responsable_edit').val();  
    es_edit = $('#p_estado_edit').val();
    
    se_edit = $('#sector_select_3').val();
    mt_edit = $("#p_motivo_edit").val();
    tpe_edit = $("#p_tipo_error_edit").val();
    
    ems_edit = $('#p_email_send_edit').val();
    ecc_edit = $('#p_email_cc_edit').val();
    var content = $("#p_content_new_ticket").val();

    $.ajax({
        url: 'scripts/saveEditedTicket.php?gid=' + a + '&gce=' + b + '&fh=' + fh + '&nm=' + nm + '&ec=' + ec + '&cl=' + cl + "&or=" + or + "&su=" + su + "&re=" + re + "&es=" + es + '&ems=' + ems + '&ecc=' + ecc + '&se=' + se + '&mt=' + mt + '&tpe=' + tpe +'&fh_edit=' + fh_edit + '&nm_edit=' + nm_edit + '&ec_edit=' + ec_edit + '&cl_edit=' + cl_edit + "&or_edit=" + or_edit + "&su_edit=" + su_edit + "&re_edit=" + re_edit + "&es_edit=" + es_edit + '&ems_edit=' + ems_edit + '&ecc_edit=' + ecc_edit + '&se_edit=' + se_edit + '&mt_edit=' + mt_edit + '&tpe_edit=' + tpe_edit,
        success: function(data) {
            sendEmailToClient(emailCliente, content, emailSend, emailCc, emailResponsable, emailSucursal, 'newTicket', a, 'noClient')
            $('#content_information').html('El ticket se ha editado correctamente.');
            $('#response_for_dialogs').dialog('open');
            $('#response_for_dialogs').dialog({
                close: function() {
                    window.location = "tickets.edit.php?id=" + a;
                }
            });
        //
        }
    });

}
function makeTicketAnswer(a, b, sendmail, usuario) {
    emailCliente = $('#p_email_cliente').val();
    emailSend = $('#email_new').val();
    emailCc = $('#cc_email_new').val();
    emailSucursal = $('#p_email_sucursal').val();
    emailResponsable = $('#p_email_responsable').val();

    $.ajax({
        url: 'scripts/makeTicketAnswer.php?gid=' + a + '&gce=' + b,
        success: function(data) {
            $('#responseMake').html('Guardado Correctamente');
            if(sendmail == "responsable"){
                sendEmailToClient("","","","","","","sendsupervisor", a, "", "", usuario);
            }
            if(sendmail == "administracion"){
                sendEmailToClient("", b, "", "", "", "", "sendsupervisor", a, "", "", usuario, "admin");
            }
        //            setTimeout(window.location = "tickets.edit.php?id=" + a, 2);
        }
    });
}
function getSelectsContent(a) {
    $.ajax({
        url: 'scripts/getSelectsContent.php?idtk=' + a,
        success: function(data) {
            $('#selectsContent').html(data);
        //alert(data);
        }
    });
}
function editAnswer(a, b) {
    $.ajax({
        url: 'scripts/showTextOfAnswerToEdit.php?idNum=' + a,
        success: function(data) {
            $('#' + b).html(data);
        //alert(data);
        }
    });
}
function editTicketAnswer(a, b) {
    $.ajax({
        url: 'scripts/editTicketAnswer.php?idAnsw=' + a + "&gca=" + b,
        success: function(data) {
            setTimeout(window.location = "tickets.edit.php?id=" + a, 2);
        }
    });
}
function deleteTicket(a, b) {
    var answer = confirm("Seguro desea Eliminar este Ticket?")
    if (answer) {
        $.ajax({
            url: 'scripts/deleteTicket.php?delID=' + a,
            success: function(data) {
                if (b == "desktop") {
                    setTimeout(window.location = "escritorio.php", 2);
                } else {
                    actualizar_tickets_usuarias_listado();
                }
            }
        });
    }
}
function generateNewTicket() {
    //    $("#p_fecha_new").datepicker("disable");
    $("#generateNewTicket").dialog("open");
    $("#generateNewTicket #email_new").val("adp-supervisores@grupo-gestion.com.ar");
    $("#generateNewTicket #cc_email_new").val("administracioncomercial@grupo-gestion.com.ar");
    //    $("#p_fecha_new").datepicker("enable");
    $('textarea#p_content_new_ticket').focus();
    //    $("input#p_fecha_new").datepicker('setDate', new Date());
    return false;
}


function validarEmail(valor) {
    var exr = /^[0-9a-z_\-\.]+@[0-9a-z\-\.]+\.[a-z]{2,4}$/i;
    return exr.test(valor);
}

function saveNewTicket(a, b, close) {

    var helper = '0';

    var emailCliente = $('#p_email_cliente_new').val();
    var emailSend = $('#email_new').val();
    var emailCc = $('#cc_email_new').val();
    var emailSucursal = $('#p_email_sucursal').val();
    var emailResponsable = $('#p_email_responsable').val();

    var solicitante = $('#p_solicitante').val();
    var fecha = $('#p_fecha_new').val();
    var nm = $('#p_nombres_new').val();
    var cl = $('#p_cliente_new').val();
    var or = $('#p_origen_new').val();
    var su = $('#p_sucursal_new').val();
    var re = $('#p_responsable_new').val();
    var es = $('#p_estado_new').val();
    var dni = $('#p_dni').val();
    var cid = $('#p_dni_id').val();
    var sector = $('#sector_select').val();
    var motivo = $('#p_motivo').val();
    var terror = $('#p_tipo_error_org').val();
    var solic = "Usuario";
    var content = $("#p_content_new_ticket").val();
    
    if(nm != ""){
        solic = nm;
    }else if(cl != ""){
        solic = cl;
    }else{
        solic = "Usuario";
    }
    
    if($("#p_origen_new").val() == 1){
        if($("#p_nombres_new").val() == ""){
            
            $('#response_for_dialogs').dialog('open');
            $('#content_information').html('Por favor complete el Nombre del Solicitante');
        }else if($("#p_email_cliente_new").val() == ""){
            
            $('#response_for_dialogs').dialog('open');
            $('#content_information').html('Por favor ingrese el email del Solicitante');
            
        }else if($("#p_motivo").val() == 0){
            
            $('#response_for_dialogs').dialog('open');
            $('#content_information').html('Por favor seleccione el motivo');
            
                    
        }else if($("#p_sucursal_new").val() == "" || $("#p_sucursal_new").val() == 0 || $("#sector_select").val() == "" || $("#sector_select").val() == 0 || $("#p_responsable_new").val() == "" || $("#p_responsable_new").val() == 0){
            
            alert = "Debe asignar";
            ant = "";
            if($("#p_sucursal_new").val() == "" || $("#p_sucursal_new").val() == 0){
                alert += " una Sucursal "
                ant = "1";
            }
            if($("#sector_select").val() == "" || $("#sector_select").val() == 0){
                if(ant == "1"){
                    alert += "=>";    
                }
                alert += " un Sector "
                ant = "2";
            }
            if($("#p_responsable_new").val() == "" || $("#p_responsable_new").val() == 0){
                if(ant == "2"){
                    alert += "=>";    
                }
                alert += " un Responsable "
            }
            
            if(ant != ""){
                $('#response_for_dialogs').dialog('open');
                $('#content_information').html(alert);
            }
            
        }else if(b == "" || b == "Escribir la consulta o reclamo..."){
            
            $('#response_for_dialogs').dialog('open');
            $('#content_information').html('Ingrese una consulta o Reclamo');
        }else{
            $.ajax({
                url: 'scripts/saveNewTicket.php?gid=' + a + '&solicitante=' + solicitante + '&dni=' + dni + '&cid=' + cid + '&fecha=' + fecha + '&nm=' + nm + '&cl=' + cl + "&or=" + or + "&su=" + su + "&re=" + re + "&es=" + es + '&cn=' + b + "&emailCliente=" + emailCliente + "&emailSend=" + emailSend + "&emailCc=" + emailCc + "&sector_id=" + sector + "&motivo_id=" + motivo + "&terror_id=" + terror,
                beforeSend: function() {
                    $('#response_for_dialogs').dialog('open');
                    $("#content_information").html('<img src="images/loading.gif" class="loading" />');
                },
                success: function(data) {
                    sendEmailToClient(emailCliente, content, emailSend, emailCc, emailResponsable, emailSucursal, 'newTicket', a, 'noClient', or, solic, es, dni)
                    if (data == "0") {
                        alert('Faltan Datos');
                    } else {

                        if (close) {
                            $('#content_information').html('El ticket se ha cargado correctamente.');
                            $('#response_for_dialogs').dialog({
                                close: function() {
                                    window.location = "tickets.php";
                                }
                            });
                        } else {
                            $("#edit_ticket_dialog").dialog({
                                width: 850,
                                height: 510,
                                closeOnEscape: true,
                                autoOpen: false,
                                modal: true,
                                resizable: false,
                                title: 'Editar Ticket - ' + data
                            });
                            $('#edit_ticket_dialog').dialog('open');

                            $("#response_for_dialogs:ui-dialog").dialog("destroy");
                            $("#generateNewTicket:ui-dialog").dialog("destroy");

                            editTicket(data, 'textToEdit');
                        }

                    /*
                         $("#edit_ticket_dialog" ).dialog({
                         width: 830,
                         height: 510,
                         closeOnEscape: true,
                         autoOpen: false,
                         modal: true,
                         resizable: false,
                         title: 'Editar Ticket - ' + data
                         });
                         $('#edit_ticket_dialog').dialog('open');
                         
                         $( "#response_for_dialogs:ui-dialog" ).dialog( "destroy" );
                         $( "#generateNewTicket:ui-dialog" ).dialog( "destroy" );
                         
                         editTicket(data,'textToEdit');
                     */

                    //$('#response_for_dialogs').dialog('destroy');
                    //$('#response_for_dialogs').dialog('open');

                    /*
                         $('#content_information').html('El ticket se ha cargado correctamente.');
                         $('#response_for_dialogs').dialog({
                         close: function() {
                         window.location="tickets.php";
                         }
                         });
                     */

                    }

                }
            });
        }
    }else{
        
        if (cl != "0" && or != "0" && su != "0" && re != "0" && es != "0" && b != "" && b != "Escribir la consulta o reclamo...") {

            //        if(!validarEmail(emailCliente)){
            //            alert('El Email del Cliente parece incorrecto');
            //            helper = '1';
            //
            //        }
            if (emailSend != "") {
                if (!validarEmail(emailSend)) {
                    alert('El Email parece incorrecto');
                    helper = '1';
                }
            }
            if (emailCc != "") {
                if (!validarEmail(emailCc)) {
                    alert('El Email parece incorrecto');
                    helper = '1';
                }
            }
            if (helper != '1') {
                $.ajax({
                    url: 'scripts/saveNewTicket.php?gid=' + a + '&solicitante=' + solicitante + '&dni=' + dni + '&cid=' + cid + '&fecha=' + fecha + '&nm=' + nm + '&cl=' + cl + "&or=" + or + "&su=" + su + "&re=" + re + "&es=" + es + '&cn=' + b + "&emailCliente=" + emailCliente + "&emailSend=" + emailSend + "&emailCc=" + emailCc + "&sector_id=" + sector + "&motivo_id=" + motivo + "&terror_id=" + terror,
                    beforeSend: function() {
                        $('#response_for_dialogs').dialog('open');
                        $("#content_information").html('<img src="images/loading.gif" class="loading" />');
                    },
                    success: function(data) {
                        sendEmailToClient(emailCliente, content, emailSend, emailCc, emailResponsable, emailSucursal, 'newTicket', a, 'noClient', or, solic, es, dni)
                        if (data == "0") {
                            alert('Faltan Datos');
                        } else {

                            if (close) {
                                $('#content_information').html('El ticket se ha cargado correctamente.');
                                $('#response_for_dialogs').dialog({
                                    close: function() {
                                        window.location = "tickets.php";
                                    }
                                });
                            } else {
                                $("#edit_ticket_dialog").dialog({
                                    width: 850,
                                    height: 510,
                                    closeOnEscape: true,
                                    autoOpen: false,
                                    modal: true,
                                    resizable: false,
                                    title: 'Editar Ticket - ' + data
                                });
                                $('#edit_ticket_dialog').dialog('open');

                                $("#response_for_dialogs:ui-dialog").dialog("destroy");
                                $("#generateNewTicket:ui-dialog").dialog("destroy");

                                editTicket(data, 'textToEdit');
                            }

                        /*
                         $("#edit_ticket_dialog" ).dialog({
                         width: 830,
                         height: 510,
                         closeOnEscape: true,
                         autoOpen: false,
                         modal: true,
                         resizable: false,
                         title: 'Editar Ticket - ' + data
                         });
                         $('#edit_ticket_dialog').dialog('open');
                         
                         $( "#response_for_dialogs:ui-dialog" ).dialog( "destroy" );
                         $( "#generateNewTicket:ui-dialog" ).dialog( "destroy" );
                         
                         editTicket(data,'textToEdit');
                         */

                        //$('#response_for_dialogs').dialog('destroy');
                        //$('#response_for_dialogs').dialog('open');

                        /*
                         $('#content_information').html('El ticket se ha cargado correctamente.');
                         $('#response_for_dialogs').dialog({
                         close: function() {
                         window.location="tickets.php";
                         }
                         });
                         */

                        }

                    }
                });
            }

        } else {


            $('#response_for_dialogs').dialog('open');
            $('#content_information').html('Debe completar todos los campos');

        }
    }

}

function showTicketForAnswer(a, b,sendmail, usuario) {
    $.ajax({
        url: 'scripts/showTextOfTicketToEdit.php?idNum=' + a + '&what=resp',
        success: function(data) {
            $('#' + b).html(data);
            $('textarea#p_content_response_a').focus();
            return false;
        //alert(data);
        }
    });
}
function sendEmailToClient(a, b, c, d, e, f, g, extraInfo, h, i, nombresolicitante, estadoDelTicket, dniColab) {
    // id Si es necesario

    if (b == 'responseForClient') {
        b = $('#respuesta_a_cliente').html();
        checkIfAnswerWasSentToClient(extraInfo,'new')
    } else {
        b = b;
    }
    $.ajax({
        url: "scripts/send.mails.php?email=" + a + "&contenido=" + b + "&cc=" + c + "&cco=" + d + "&eResponsable=" + e + "&eSucursal=" + f + "&case=" + g + "&id=" + extraInfo + "&tipo_usuario=" + i + "&nomsolic=" + nombresolicitante + "&status=" + estadoDelTicket + "&dniColab=" + dniColab,
        
        success: function(data) {
            if (h != "noClient") {
                $('#respuestaAlCliente').dialog('close');
                $('#response_for_dialogs').dialog('open');
                $('#content_information').html("Respuesta enviada con &eacute;xito")
                $('#response_for_dialogs').dialog({
                    close: function() {
                        window.location = "tickets.php";
                    }
                });
            }

        }
    });
}
function loadTicketsDesktop() {
    $.ajax({
        url: 'scripts/send.mails.php?email=' + a + "&contenido=" + b + "&cc=" + c + "&cco=" + d + "&eResponsable=" + e + "&eSucursal=" + f + "&case=" + g,
        success: function(data) {
            $('#respuestaAlCliente').dialog('close');
            $('#response_for_dialogs').dialog('open');
            $('#content_information').html("Mail enviado con &eacute;xito")
            if (g == "newTicket") {
                setTimeout(window.location = "tickets.edit.php?id=" + extraInfo, 2);
            }
        }
    });
}
function feedTicketsHome() {

    $.ajax({
        type: "get",
        url: "feed.tickets.home.php",
        beforeSend: function() {
            $("#fill_tickets_home").html('<img src="images/loading.gif" class="loading" />');
        },
        success: function(res) {
            $("#fill_tickets_home").html(res);
        }
    });
}

function sendEmailRecoverPass(a) {
    var helper = '0';

    if (a != "") {
        if (!validarEmail(a)) {
            helper = '1';
            $('#response_for_dialogs').dialog('open');
            $('#content_information').html('"El correo parece no tener un formato correcto"')
        }
        if (helper != '1') {
            $.ajax({
                url: "scripts/send.mails.php?email=" + a + '&case=recoverpass',
                success: function(data) {
                    $('#recoverPass').dialog('close');
                    $('#response_for_dialogs').dialog('open');
                    $('#content_information').html(data)
                }
            });
        }
    }
}

function checkIfAnswerWasSentToClient(a,b,c){
    $.ajax({
        type: "get",
        dataType: "html",
        url: "scripts/check.if.ticket.is.responded.to.client.php?id="+a+'&do='+b,
        beforeSend: function() {
            $('#'+c).html('<img src="images/loading.gif" class="loading" />');
        },
        error:function(xhr, status, errorThrown) {
            alert(errorThrown+'\n'+status+'\n'+xhr.statusText);
        },
        success: function(data) {
            $('#'+c).html(data);
        }
    });
}

$(document).ready(function() {
    feedTicketsHome();


    $('[placeholder]').focus(function() {
        var input = $(this);
        if (input.val() == input.attr('placeholder')) {
            input.val('');
            input.removeClass('placeholderClass');
        }
    }).blur(function() {
        var input = $(this);
        if (input.val() == '' || input.val() == input.attr('placeholder')) {
            input.addClass('placeholderClass');
            input.val(input.attr('placeholder'));
        }
    }).blur();

    $('[placeholder]').parents('form').submit(function() {
        $(this).find('[placeholder]').each(function() {
            var input = $(this);
            if (input.val() == input.attr('placeholder')) {
                input.val('');
            }
        })
    });
});

function getFormAdjunto(idTicket, contador) {
    if ($("#form-ticket-adjuntos").length > 0) {
        $.ajax({
            type: "GET",
            url: "inc.form.adjunto.php?idregistro=" + idTicket,
            beforeSend: function() {
                $("#form-ticket-adjuntos").html('<img src="images/loading.gif" class="loading" />');
            },
            success: function(msg) {
                $("#form-ticket-adjuntos").html(msg);
            }
        });
    }
}

function closeTicket(a){
    var answer = confirm("Seguro desea Cerrar este Ticket?")
    if (answer){
        $.ajax({
            url: 'scripts/deleteTicket.php?delID='+a+'&do=close',
            success: function(data) {
                setTimeout(window.location="tickets.php",2);
            }
        });
    }
}

function checkIfTicketIsClosed(a){
    $.ajax({
        url: 'scripts/deleteTicket.php?delID='+a+'&do=checkclose',
        success: function(data) {
            if(data == "S"){
                $("#btn-agregar-editar").remove();
                $("#close_ticket").remove();
                $("#cerrar_ticket").remove();
                $("#agregar_adjunto").remove();
            }
        }
    });
}

function obtenerSolicitante(id){
    $.ajax({
        type: "get",
        url: "feed.tickets.solicitante.php?id=" + id,
        success: function(res) {
            $("#solicitante_header").html(res);
        }
    });
}