$(document).ready(function() {

    $(".column").sortable({
        connectWith: ".column"
    });

    $(function() {
        $( ".column" ).sortable({
            connectWith: ".column"
        });

        $( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" )
        .find( ".portlet-header" )
        .addClass( "ui-widget-header ui-corner-top" )
        .prepend( "<span class='ui-icon ui-icon-minusthick'></span>")
        .end()
        .find( ".portlet-content" );

        $( ".portlet-header .ui-icon" ).click(function() {
            $( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
            $( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
        });

        $( ".column" ).disableSelection();
    });

    $("#dialog_widget_eventos_detalle").dialog({
        modal: true,
        autoOpen: false,
        width: 390,
        title: "Agregar Evento",
        buttons: {
            "Cerrar": function() {
                $(this).dialog("close");
            }
        }
    });
    
    if($("#dialog-response").length > 0){
        $("#dialog-response").dialog({
            modal: true,
            autoOpen: false,
            width: 'auto',
            maxWidth: 580,
            height: 'auto',
            resizable: false,
            title: "Administrador de Seguros",
            //hide: 'explode',
            resizeStop: function(event, ui) {
                $('#dialog-response').dialog('option', 'position', 'center');
            },
            open: function(){
                $('textarea').blur();
            },
            buttons: {
                "Cerrar": function() {
                    $( this ).dialog( "close" );
                }
            }
        });
    }

    cargar_widget("pedidos");
    cargar_widget("postulantes");
    cargar_widget("eventos");
});

function cargar_widget(nombre)
{
    $.ajax({
        type: "get",
        url:  "feed.escritorio.php?objeto=" + nombre,
        beforeSend: function() {
            $("#widget_pedidos").html('<img src="images/loading.gif" class="loading" />');
        },
        success: function(res) {
            $("#widget_" + nombre).html(res);
        }
    });
}

function nuevo_evento() {
    $("#dialog_widget_eventos_detalle").dialog("open");
}

$(".verEvento").live('click', function(){
    var enlace = $(this).attr("href");
    $.ajax({
        type: "GET",
        url: enlace,
        success: function(msg){
            $("#dialog-response").html(msg);
            $("#dialog-response").dialog('open');
            $("#dialog-response").dialog("option", "title", 'Eventos');
        }
    });
    return false;
});