var funciones_actualizar;  /* funciones a ejecutar luego de un delete */
var funcion_actualizar;  /* funciones a ejecutar luego de un delete */

function actualizar_lista(id) {

    switch (id) {

        /* funciones de actualizacion de lista seccion CONTACTOS */
        case "a_contactos_agente" :
            actualizarContactosAgentes();
            break;

        case "a_contactos_listado" :
            actualizarContactosListado();
            break;

        case "a_contactos_contrato" :
            actualizarContactosContratos();
            break;

        case "a_contactos_principal" :
            actualizarContactosHome();
            break;

        /* funciones de actualizacion de lista seccion COMPANIAS */
        case "a_companias_principal" :
            actualizarCompaniasHome();
            break;

        case "a_companias_productos" :
            actualizarCompaniasProductos();
            break;

        case "a_companias_planes" :
            actualizarCompaniasPlanes();
            break;

        case "a_companias_coberturas" :
            actualizarCoberturas();
            break;

        case "a_companias_listado" :
            actualizarPlanesListado();
            break;

        /* funciones de actualizacion de lista seccion POLIZAS */
        case "a_polizas_principal" :
            actualizarPolizasHome();
            break;

        case "a_polizas_buscar" :
            break;

        case "a_polizas_archivos_list" :
            actualizarArchivosPoliza();
            break;

        case "a_polizas_delete" :
            getFormPolizas(0);
            break;

        /* funciones de actualizacion de lista seccion PRODUCCION */
        case "a_produccion_reportes" :
            actualizarListadoReportes();
            break;

        case "a_produccion_historiales" :
            actualizarListadoHistoriales();
            break;

        /* funciones de actualizacion de lista seccion CONCURSOS */
        case "a_concursos_delete" :
            break;

        /* funciones de actualizacion de lista seccion CONCURSOS */
        case "a_opciones_principal" :
            actualizarOpcionesHome();
            break;

        case "a_opciones_abm_monedas" :
            actualizarOpcionesABM("monedas-abm");
            break;

        case "a_home_polizas_widget" :
            actualizarHomePolizasWidget();
            break;

    }
}

/*
 * Date Format 1.2.3
 * (c) 2007-2009 Steven Levithan <stevenlevithan.com>
 * MIT license
 *
 * Includes enhancements by Scott Trenda <scott.trenda.net>
 * and Kris Kowal <cixar.com/~kris.kowal/>
 *
 * Accepts a date, a mask, or a date and a mask.
 * Returns a formatted version of the given date.
 * The date defaults to the current date/time.
 * The mask defaults to dateFormat.masks.default.
 */

var dateFormat = function () {
    var token = /d{1,4}|m{1,4}|yy(?:yy)?|([HhMsTt])\1?|[LloSZ]|"[^"]*"|'[^']*'/g,
    timezone = /\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g,
    timezoneClip = /[^-+\dA-Z]/g,
    pad = function (val, len) {
        val = String(val);
        len = len || 2;
        while (val.length < len) val = "0" + val;
        return val;
    };

    // Regexes and supporting functions are cached through closure
    return function (date, mask, utc) {
        var dF = dateFormat;

        // You can't provide utc if you skip other args (use the "UTC:" mask prefix)
        if (arguments.length == 1 && Object.prototype.toString.call(date) == "[object String]" && !/\d/.test(date)) {
            mask = date;
            date = undefined;
        }

        // Passing date through Date applies Date.parse, if necessary
        date = date ? new Date(date) : new Date;
        if (isNaN(date)) throw SyntaxError("invalid date");

        mask = String(dF.masks[mask] || mask || dF.masks["default"]);

        // Allow setting the utc argument via the mask
        if (mask.slice(0, 4) == "UTC:") {
            mask = mask.slice(4);
            utc = true;
        }

        var _ = utc ? "getUTC" : "get",
        d = date[_ + "Date"](),
        D = date[_ + "Day"](),
        m = date[_ + "Month"](),
        y = date[_ + "FullYear"](),
        H = date[_ + "Hours"](),
        M = date[_ + "Minutes"](),
        s = date[_ + "Seconds"](),
        L = date[_ + "Milliseconds"](),
        o = utc ? 0 : date.getTimezoneOffset(),
        flags = {
            d:    d,
            dd:   pad(d),
            ddd:  dF.i18n.dayNames[D],
            dddd: dF.i18n.dayNames[D + 7],
            m:    m + 1,
            mm:   pad(m + 1),
            mmm:  dF.i18n.monthNames[m],
            mmmm: dF.i18n.monthNames[m + 12],
            yy:   String(y).slice(2),
            yyyy: y,
            h:    H % 12 || 12,
            hh:   pad(H % 12 || 12),
            H:    H,
            HH:   pad(H),
            M:    M,
            MM:   pad(M),
            s:    s,
            ss:   pad(s),
            l:    pad(L, 3),
            L:    pad(L > 99 ? Math.round(L / 10) : L),
            t:    H < 12 ? "a"  : "p",
            tt:   H < 12 ? "am" : "pm",
            T:    H < 12 ? "A"  : "P",
            TT:   H < 12 ? "AM" : "PM",
            Z:    utc ? "UTC" : (String(date).match(timezone) || [""]).pop().replace(timezoneClip, ""),
            o:    (o > 0 ? "-" : "+") + pad(Math.floor(Math.abs(o) / 60) * 100 + Math.abs(o) % 60, 4),
            S:    ["th", "st", "nd", "rd"][d % 10 > 3 ? 0 : (d % 100 - d % 10 != 10) * d % 10]
        };

        return mask.replace(token, function ($0) {
            return $0 in flags ? flags[$0] : $0.slice(1, $0.length - 1);
        });
    };
}();

// Some common format strings
dateFormat.masks = {
    "default":      "ddd mmm dd yyyy HH:MM:ss",
    shortDate:      "m/d/yy",
    mediumDate:     "mmm d, yyyy",
    longDate:       "mmmm d, yyyy",
    fullDate:       "dddd, mmmm d, yyyy",
    shortTime:      "h:MM TT",
    mediumTime:     "h:MM:ss TT",
    longTime:       "h:MM:ss TT Z",
    isoDate:        "yyyy-mm-dd",
    isoTime:        "HH:MM:ss",
    isoDateTime:    "yyyy-mm-dd'T'HH:MM:ss",
    isoUtcDateTime: "UTC:yyyy-mm-dd'T'HH:MM:ss'Z'"
};

// Internationalization strings
dateFormat.i18n = {
    dayNames: [
    "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat",
    "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"
    ],
    monthNames: [
    "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec",
    "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
    ]
};

// For convenience...
Date.prototype.format = function (mask, utc) {
    return dateFormat(this, mask, utc);
};


function exportarListado(seccion, formato) {

    var url = null;
    var pformato = null;

    if (formato == null) pformato = "xls";
    else pformato = formato;

    var id_form = null;

    switch (seccion) {

        case "contactos_principal" :
            url = "feed.contactos.php?objeto=principal";
            id_form = "#formContactoBuscarHome";
            break;

        case "contactos_listar" :
            url = "feed.contactos.php?objeto=listado"
            id_form = "#formContactosListado";
            break;

        case "contactos_cumples" :
            url = "feed.contactos.php?objeto=cumpleanios"
            id_form = "#formContactosCumpleanios";
            break;

        case "contactos_contratos" :
            url = "feed.contactos.php?objeto=contratos"
            id_form = "#formContactosContratos";
            break;

        case "polizas_principal" :
            url = "feed.polizas.php?objeto=principal"
            id_form = "#formPolizasBuscarHome";
            break;

        case "polizas_renovaciones" :
            url = "feed.polizas.php?objeto=renovaciones"
            id_form = "#formPolizasRenovaciones";
            break;

        case "polizas_pendientes" :
            url = "feed.polizas.php?objeto=pendientes"
            id_form = "#formPolizasPendientes";
            break;

        case "concursos_ver" :
            url = "feed.concursos.php?objeto=ver"
            id_form = "#formConcursosVer";
            break;
            
        case "reclamos_principal" :
            url = "feed.reclamos.php?objeto=principal"
            id_form = "#formReclamosBuscarHome";
            break;
    }

    url = url + "&formato=" + pformato + "&" + $(id_form).serialize();
    window.open(url);
}

function clear_form_elements(ele) {
    $(ele).find(':input').each(function() {
        switch(this.type) {
            case 'password':
            case 'select-multiple':
            case 'text':
            case 'textarea':
            case 'hidden':
                $(this).val('');
                break;
            case 'select-one':
                $(this).val(0);
                break;
            case 'checkbox':
            case 'radio':
                this.checked = false;
        }
    });
}

$(document).ready(function(){
    $(".buttons").button();

    $(".delete").live("click", function(ev){

        ev.preventDefault();

        var elem_id = $(this).attr("id");

        /*
       alert(elem_id);
       return false;
       */

        /* xstat */
        var txt = "¿Desea eliminar este elemento?";
        var response = confirm(txt);
        if (response == false) return false;

        var enlace = $(this).attr("href");
        if (enlace == "") {
            alert ("VACIO");
            return false;
        }

        $.ajax({
            type: "GET",
            url: enlace,
            success: function(msg){
                dialogoRespuesta('Contenido eliminado correctamente!'+msg);
                actualizar_lista(elem_id);
            }
        });

        return false;
    });



    $(".verEvento").live('click', function(){
        var enlace = $(this).attr("href");
        $.ajax({
            type: "GET",
            url: enlace,
            success: function(msg){
                $("#dialog-response").html(msg);
                $("#dialog-response").dialog('open');
                $("#dialog-response").dialog("option", "title", '<span class="ui-icon ui-icon-calendar"></span> Eventos');
            }
        });
        return false;
    });
    $(".verNotificacion").live('click', function(){
        var enlace = $(this).attr("href");
        $.ajax({
            type: "GET",
            url: enlace,
            success: function(msg){
                $("#dialog-response").html(msg);
                $("#dialog-response").dialog('open');
                $("#dialog-response").dialog("option", "title", '<span class="ui-icon ui-icon-info"></span> Notificaciones');
            }
        });
        return false;
    });
    $(".navPage").live('click', function(){
        var enlace = $(this).attr("href");
        var parentId = $(this).parent().parent().attr('id');
        $.ajax({
            type: "GET",
            url: enlace,
            beforeSend: function(){
                $("#"+parentId).html('<img src="images/loading.gif" class="loading" />');
            },
            success: function(msg){
                $("#"+parentId).html(msg);
            }
        });
        return false;
    });
    $(".navRepo").live('click', function(){
        var enlace = $(this).attr("href");
        var parentId = $(this).parent().parent().attr('id');
        formatearFechas();
        $.ajax({
            type: "POST",
            url: enlace,
            data: $('#formProduccion').serialize(),
            beforeSend: function(){
                $("#"+parentId).html('<img src="images/loading.gif" class="loading" />');
            },
            success: function(msg){
                $("#"+parentId).html(msg);
                formatearFechas();
            }
        });
        return false;
    });

    /**
     * DATE AND TIME PICKERS INITIALIZATION
     *
     */
    Date.prototype.toString = function () {
        return isNaN (this) ? 'NaN' : [this.getFullYear(), this.getMonth() > 8 ? this.getMonth() + 1 : '0' + (this.getMonth() + 1), this.getDate() > 9 ? this.getDate() : '0' + this.getDate()].join('-')
    }
    var d = new Date().toString();
    $(".datepicker").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "yy-mm-dd",
        defaultDate: d,
        regional: 'es',
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
        dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
        dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
        monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
    });
    if($("#fecha_nac").length > 0){
        $("#fecha_nac").datepicker( "option", "minDate", new Date(1920, 0, 1) );
        $( "#fecha_nac" ).datepicker( "option", "yearRange", '1920:c' );
    }
    $('#cumpleanios_fecha_hidden').datepicker({
        onSelect: function(dateText, inst) {
            $('#cumpleanios_fecha').html(dateText);
            actualizarCumpleanios(dateText);
        //alert(dateText);
        },
        dateFormat: 'dd-mm-yy',
        defaultDate: d,
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
        dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
        dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
        monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
    });
    $('.datetime').datetimepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "yy-mm-dd",
        regional: 'es',
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
        dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
        dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
        monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        currentText: 'Ahora',
        closeText: 'Listo',
        ampm: false,
        timeFormat: 'hh:mm:ss',
        timeOnlyTitle: 'Fecha y hora',
        timeText: 'Hora',
        hourText: 'Hora',
        minuteText: 'Minutos',
        secondText: 'Segundos'
    });
    $("#calendario").fullCalendar('destroy');
    $("#calendario").fullCalendar({
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
        dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
        monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        buttonText: {
            today:  'Hoy',
            week:   'Semana',
            month:  'Mes',
            day:    'Día'
        },
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        theme: true,
        eventSources: [
        'feed-calendar.php'
        ],
        eventClick: function(calEvent, jsEvent, view) {
            $.ajax({
                type: "GET",
                url: "ver.php?elemento=evento&id="+calEvent.id,
                success: function(msg){
                    $("#dialog-response").html(msg);
                    $("#dialog-response").dialog('open');
                    $("#dialog-response").dialog("option", "title", '<span class="ui-icon ui-icon-calendar"></span> Eventos');
                }
            });
        },
        eventAfterRender: function( event, element, view ) {
            $('#dialogo-calendario-eventos').dialog('option', 'position', 'center');
        }
    });
    $( "#dialog:ui-dialog" ).dialog( "destroy" );
    if($("#dialogo-calendario-eventos").length > 0){
        $("#dialogo-calendario-eventos").dialog({
            modal: true,
            autoOpen: false,
            width: 800,
            height: 'auto',
            title: '<span class="ui-icon ui-icon-calculator"></span> Calendario',
            resizable: false
        });
    }
    if($("#dialog-form-eventos").length > 0){
        $( "#dialog-form-eventos" ).dialog({
            autoOpen: false,
            height: 'auto',
            width: 'auto',
            maxWidth: 450,
            modal: true,
            title: '<span class="ui-icon ui-icon-calendar"></span> Agregar Evento',
            resizable: false,
            buttons: {
                "Agregar Evento": function() {
                    $.ajax({
                        type: "POST",
                        url: $("#formNuevoEvento").attr('action'),
                        data: $("#formNuevoEvento").serialize(),
                        success: function(msg){
                            $("#dialog-form-eventos").dialog("close");
                            dialogoRespuesta(msg);
                            actualizarEventos();
                        }
                    });
                },
                "Cancelar": function() {
                    $( this ).dialog( "close" );
                }
            }
        });
    }
    if($("#notify").length > 0){
        $( "#notify" ).dialog({
            autoOpen: false,
            height: 'auto',
            width: 'auto',
            maxWidth: 370,
            modal: true,
            resizable: false,
            title: '<span class="ui-icon ui-icon-info"></span> Enviar Notificaci&oacute;n',
            buttons: {
                "Enviar": function() {
                    $( this ).dialog( "close" );
                    $.ajax({
                        type: "POST",
                        url: $("#formNotificar").attr('action'),
                        data: $("#formNotificar").serialize(),
                        success: function(msg){
                            dialogoRespuesta(msg)
                            $("#notify").dialog("close");
                        }
                    });
                },
                "Cancelar": function() {
                    $( this ).dialog( "close" );
                }
            },
            close: function(event, ui) {
                $("#invites_spans").empty();
                $("#formNotificar > [id*='formNotificar_']").each(function(i){
                    $(this).remove();
                });
                clear_form_elements("#formNotificar");
            }
        });
    }
    if($("#enviarPorMail").length > 0){
        $( "#enviarPorMail" ).dialog({
            autoOpen: false,
            height: 'auto',
            width: 'auto',
            maxWidth: 370,
            modal: true,
            resizable: false,
            title: '<span class="ui-icon ui-icon-mail-closed"></span> Enviar por Email',
            buttons: {
                "Enviar": function() {
                    $.ajax({
                        type: "POST",
                        url: $("#formEnviarMail").attr('action'),
                        data: $("#formEnviarMail").serialize(),
                        success: function(msg){
                            $("#enviarPorMail").dialog("close");
                            dialogoRespuesta(msg)
                        }
                    });
                },
                "Cancelar": function() {
                    $( this ).dialog( "close" );
                }
            },
            close: function(event, ui) {
                $("#usuarios_mails").empty();
                $("#formEnviarMail > [id*='formEnviarMail_']").each(function(i){
                    $(this).remove();
                });
                clear_form_elements("#formEnviarMail");
            }
        });
    }
    $("#ui-datepicker-div").css("z-index", "9999");
    $("#ui-datepicker-div").css("display", "none");
    $( "#dialog:ui-dialog" ).dialog( "destroy" );
    
    /**
     * CARGAR COMPONENTES AJAX
     * 
     */
    actualizarCumpleanios()
    actualizarLinks();
    actualizarNovedades();
    actualizarHomePolizasWidget();
    actualizarEventos();
    actualizarHorarios();
    actualizarProduccion();
    actualizarNotificaciones();
    actualizarTabNovedades();

    if($("#dialog-response").length > 0){
        $("#dialog-response").dialog({
            modal: true,
            autoOpen: false,
            width: 'auto',
            maxWidth: 580,
            height: 'auto',
            resizable: false,
            title: "Administrador de Seguros",
            //hide: 'explode',
            resizeStop: function(event, ui) {
                $('#dialog-response').dialog('option', 'position', 'center');
            },
            open: function(){
                $('textarea').blur();
            },
            buttons: {
                "Cerrar": function() {
                    $( this ).dialog( "close" );
                }
            }
        });
    }
    if($("#dialog-listar-links").length > 0){
        $("#dialog-listar-links").dialog({
            modal: true,
            autoOpen: false,
            width: 520,
            height: 'auto',
            resizable: false,
            title: '<span class="ui-icon ui-icon-link"></span> Links',
            resizeStop: function(event, ui) {
                $('#dialog-listar-links').dialog('option', 'position', 'center');
            },
            open: function(){
                actualizarListadoLinks();
            },
            close: function(){
                actualizarLinks();
            }
        });
    }
    if($("#dialog-listar-horarios").length > 0){
        $("#dialog-listar-horarios").dialog({
            modal: true,
            autoOpen: false,
            width: 320,
            height: 'auto',
            resizable: false,
            title: '<span class="ui-icon ui-icon-clock"></span> Horarios',
            resizeStop: function(event, ui) {
                $('#dialog-listar-horarios').dialog('option', 'position', 'center');
            },
            open: function(){
                actualizarListadoHorarios();
            },
            close: function(){
                actualizarHorarios();
            }
        });
    }
    $(".verNovedad").live('click', function(){
        var enlace = $(this).attr("href");
        $.ajax({
            type: "GET",
            url: enlace,
            success: function(msg){
                $("#dialog-response").dialog( "option", "title", '<span class="ui-icon ui-icon-lightbulb"></span> Novedades' );
                $("#dialog-response").html(msg);
                $("#dialog-response").dialog('open');
            }
        });
        return false;
    });
    $(".form-contacto-text").live('change', function(){
        $("#formChanged").val("true");
    });
    if($("#dialog-form-novedades").length > 0){

        $( "#dialog-form-novedades" ).dialog({
            autoOpen: false,
            height: 'auto',
            width: 370,
            modal: true,
            resizable: false,
            title: '<span class="ui-icon ui-icon-lightbulb"></span> Agregar Novedad',
            buttons: {
                "Agregar Novedad": function() {
                    $.ajax({
                        type: "POST",
                        url: $("#formNuevaNovedad").attr('action'),
                        data: $("#formNuevaNovedad").serialize(),
                        success: function(msg){
                            $("#dialog-form-novedades").dialog("close");
                            dialogoRespuesta(msg);
                            actualizarNovedades();
                        }
                    });
                },
                "Cancelar": function() {
                    $( this ).dialog( "close" );
                }
            }
        });
    }
    if($("#dialog-form-links").length > 0){
        $( "#dialog-form-links" ).dialog({
            autoOpen: false,
            height: 'auto',
            width: 370,
            modal: true,
            resizable: false,
            title: '<span class="ui-icon ui-icon-link"></span> Agregar Link',
            buttons: {
                "Agregar Link": function() {
                    $("#formAgregarLink").submit();
                },
                "Cancelar": function() {
                    $( this ).dialog( "close" );
                }
            }
        });
    }
    if($("#dialog-form-horarios").length > 0){
        $( "#dialog-form-horarios" ).dialog({
            autoOpen: false,
            height: 'auto',
            width: 370,
            modal: true,
            resizable: false,
            title: '<span class="ui-icon ui-icon-clock"></span> Agregar Link',
            buttons: {
                "Agregar Horario": function() {
                    $.ajax({
                        type: "POST",
                        url: $("#formNuevoHorario").attr('action'),
                        data: $("#formNuevoHorario").serialize(),
                        success: function(msg){
                            $("#dialog-form-horarios").dialog("close");
                            dialogoRespuesta(msg);
                            actualizarHorarios();
                        }
                    });
                },
                "Cancelar": function() {
                    $( this ).dialog( "close" );
                }
            }
        });
    }
});


function dialogoRespuesta(mensaje, titulo){
    if(titulo == null){
        titulo = '<span class="ui-icon ui-icon-alert"></span> Atenci&oacute;n';
    }
    $("#dialog-response").html(mensaje);
    $("#dialog-response").dialog("option", "title", titulo);
    $("#dialog-response").dialog('open');
}
function actualizarEventos(){
    if($("#ultimos_eventos").length > 0){
        $.ajax({
            type: "GET",
            url: "feed.home.php?objeto=evento&cantidad=3",
            beforeSend: function(){
                $("#ultimos_eventos").html('<img src="images/loading.gif" class="loading" />');
            },
            success: function(msg){
                $("#ultimos_eventos").html(msg);
            }
        });
    }
}
function actualizarNovedades(){
    if($("#ultimas_novedades").length > 0){
        $.ajax({
            type: "GET",
            url: "feed.home.php?objeto=novedad&cantidad=2",
            beforeSend: function(){
                $("#ultimas_novedades").html('<img src="images/loading.gif" class="loading" />');
            },
            success: function(msg){
                $("#ultimas_novedades").html(msg);
            }
        });
    }
}
function actualizarLinks(){
    if($("#links_clientes").length > 0){
        $.ajax({
            type: "GET",
            url: "feed.home.php?objeto=links&cantidad=2",
            beforeSend: function(){
                $("#links_clientes").html('<img src="images/loading.gif" class="loading" />');
            },
            success: function(msg){
                $("#links_clientes").html(msg);
            }
        });
    }
}

function actualizarProduccion() {

    if ($("#home-produccion-grafico").length > 0) {

        var cbo_rango = $("#cbo_home_prod_rango").val();
        var cbo_tipo = $("#cbo_home_prod_tipo").val();

        $.ajax ({
            type: "POST",
            url:  "feed.home.php?objeto=produccion",
            data: {
                rango: cbo_rango,
                tipo: cbo_tipo
            },
            beforeSend: function() {
                $("#home-produccion-grafico").html('<img style="margin:80px 0 0 80px;" src="images/loading.gif" class="loading" />');
            },
            success: function(res) {
                $("#home-produccion-grafico").html(res);
            }
        });

    }
}

function actualizarHomePolizasWidget() {

    if ($("#home_portlet_polizas").length > 0) {

        var cod_compania = $("select[name=HOME_POLIZAS_COMPANIA]").val();
        var cod_producto = $("select[name=HOME_POLIZAS_PRODUCTO]").val();

        $.ajax({
            type: "POST",
            url:  "feed.home.php?objeto=polizas",
            data: {
                id_compania: cod_compania,
                id_producto: cod_producto
            },
            beforeSend: function() {
            },
            success: function(msg) {
                $("#home_portlet_polizas").html(msg);
            }
        });

    }

}
function actualizarHorarios(){
    if($("#horarios_ciudades").length > 0){
        $.ajax({
            type: "GET",
            url: "feed.home.php?objeto=horarios&cantidad=2",
            beforeSend: function(){
                $("#horarios_ciudades").html('<img src="images/loading.gif" class="loading" />');
            },
            success: function(msg){
                $("#horarios_ciudades").html(msg);
            }
        });
    }
}
function actualizarListadoLinks(){
    if($("#dialog-listar-links").length > 0){
        $.ajax({
            type: "GET",
            url: "feed.home.php?objeto=links_list&cantidad=2",
            beforeSend: function(){
                $("#dialog-listar-links").html('<img src="images/loading.gif" class="loading" />');
            },
            success: function(msg){
                $("#dialog-listar-links").html(msg);
                $('#dialog-listar-links').dialog('option', 'width', 'auto');
                $('#dialog-listar-links').dialog('option', 'position', 'center');
                
            }
        });
    }
}
function actualizarListadoHorarios(){
    if($("#dialog-listar-horarios").length > 0){
        $.ajax({
            type: "GET",
            url: "feed.home.php?objeto=horarios_list&cantidad=2",
            beforeSend: function(){
                $("#dialog-listar-horarios").html('<img src="images/loading.gif" class="loading" />');
            },
            success: function(msg){
                $("#dialog-listar-horarios").html(msg);
                $('#dialog-listar-horarios').dialog('option', 'width', 'auto');
                $('#dialog-listar-horarios').dialog('option', 'position', 'center');
                
            }
        });
    }
}
function actualizarCumpleanios(fecha){
    if($("#cumpleanios_del_dia").length > 0){
        if(fecha != null){
            fecha = "&fecha="+fecha;
        }
        else{
            fecha = "";
        }
        $.ajax({
            type: "GET",
            url: "feed.home.php?objeto=cumpleanios&cantidad=30"+fecha,
            beforeSend: function(){
                $("#cumpleanios_del_dia").html('<img src="images/loading.gif" class="loading" />');
            },
            success: function(msg){
                $("#cumpleanios_del_dia").html(msg);
            }
        });
    }
}
function cambiarTextoBoton(idForm, newText, indexButton){
    if(indexButton == null){
        indexButton = 0;
    }
    var buttons = $(idForm).dialog("option", "buttons");
    buttons[indexButton].text = newText;
    $(idForm).dialog("option", "buttons", buttons);
}
function actualizarNotificacion(elem){
    var tr = $(elem).parent().parent();
    var id = $(elem).attr('name');
    var value = elem.checked;
    var leida = 0;
    if(value){
        leida = 1;
    }
    $.ajax({
        type: "POST",
        url: "abm.php?tabla=notificaciones&columna=id&idregistro="+id+"&archivo=0&contador=1&ajax=true",
        data: "leida="+leida,
        success: function(msg){
            if(value){
                tr.css('font-weight', 'normal');
            }
            else{
                tr.css('font-weight', 'bold');
            }
        }
    });
}
function actualizarNotificaciones(){
    if($("#notificaciones_listado").length > 0){
        $.ajax({
            type: "GET",
            url: "feed.home.php?objeto=notificaciones&cantidad=6",
            beforeSend: function(){
                $("#ultimos_eventos").html('<img src="images/loading.gif" class="loading" />');
            },
            success: function(msg){
                $("#notificaciones_listado").html(msg);
            }
        });
    }
}

function actualizarTabNovedades() {
    if ($("#novedades_listado").length > 0) {
        $.ajax({
            type: "GET",
            url:  "feed.home.php?objeto=novedad",
            beforeSend: function() {
                $("#novedades_listado").html('<img src="images/loading.gif" class="loading" />');
            },
            success: function(res) {
                $("#novedades_listado").html(res);
            }
        });
    }
}


function formatearFechas(){
    $(".datepicker").each(function(index, elem){
        var fecha = $(elem).val();
        if(strpos(fecha, '/') == false && strpos(fecha, '-') == false)
            return;
        if(strpos(fecha, '-') != false){
            fecha = fecha.split("-");
            $(elem).val(fecha[2] + "/" + fecha[1] + "/" + fecha[0]);
            return;
        }
        else{
            fecha = fecha.split("/");
            $(elem).val(fecha[2] + "-" + fecha[1] + "-" + fecha[0]);
            return;
        }
    });
}
function actualizarAgente(id){
    $.ajax({
        type: "GET",
        url: "feed.polizas.php?objeto=agente&idagente="+id,
        success: function(msg){
            $("#agente-datos").html(msg);
        }
    });
}
function desformatearFechas(){
    $(".datepicker").each(function(index, elem){
        var fecha = $(elem).val();
        if(strpos(fecha, '/') == false && strpos(fecha, '-') == false)
            return;
        if(strpos(fecha, '/') != false){
            fecha = fecha.split("/");
            $(elem).val(fecha[2] + "-" + fecha[1] + "-" + fecha[0]);
        }else
            formatearFechas();
    });
}
function strpos (haystack, needle, offset) {
    // Finds position of first occurrence of a string within another  
    // 
    // version: 1103.1210
    // discuss at: http://phpjs.org/functions/strpos    // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +   improved by: Onno Marsman    
    // +   bugfixed by: Daniel Esteban
    // +   improved by: Brett Zamir (http://brett-zamir.me)
    // *     example 1: strpos('Kevin van Zonneveld', 'e', 5);    // *     returns 1: 14
    var i = (haystack + '').indexOf(needle, (offset || 0));
    return i === -1 ? false : i;
}

function getDialogHomePolizasWidget() {

    $("#dialog-home-polizas-widget").dialog({
        modal: true,
        resizable: false,
        buttons: {
            "Aceptar": function() {
                $("#form_home_polizas_widget").submit();
                actualizarHomePolizasWidget();
                $("#dialog-home-polizas-widget").dialog("close");
            },
            "Cerrar": function() {
                $("#dialog-home-polizas-widget").dialog("close");
            }
        }
    });
}



function parseDate(text, format){
    if(format == null){
        format = 'dd/mm/yyyy';
    }
    if(format == 'dd/mm/yyyy'){
        text = text.split("/");
        return new Date(text[2], parseInt(text[1]) - 1, text[0]);
    }else if(format == 'yyyy-mm-dd'){
        text = text.split("-");
        return new Date(text[0], parseInt(text[1]) - 1, text[2]);
    }
    return null;
}

function sprintf() {
    if (!arguments || arguments.length < 1 || !RegExp) {
        return;
    }
    var str = arguments[0];
    var re = /([^%]*)%('.|0|\x20)?(-)?(\d+)?(\.\d+)?(%|b|c|d|u|f|o|s|x|X)(.*)/;
    var a = b = [], numSubstitutions = 0, numMatches = 0;
    while (a = re.exec(str)) {
        var leftpart = a[1], pPad = a[2], pJustify = a[3], pMinLength = a[4];
        var pPrecision = a[5], pType = a[6], rightPart = a[7];
        numMatches++;
        if (pType == '%') {
            subst = '%';
        } else {
            numSubstitutions++;
            if (numSubstitutions >= arguments.length) {
                alert('Error! Not enough function arguments (' + (arguments.length - 1) + ', excluding the string)\nfor the number of substitution parameters in string (' + numSubstitutions + ' so far).');
            }
            var param = arguments[numSubstitutions];
            var pad = '';
            if (pPad && pPad.substr(0,1) == "'") pad = leftpart.substr(1,1);
            else if (pPad) pad = pPad;
            var justifyRight = true;
            if (pJustify && pJustify === "-") justifyRight = false;
            var minLength = -1;
            if (pMinLength) minLength = parseInt(pMinLength);
            var precision = -1;
            if (pPrecision && pType == 'f') precision = parseInt(pPrecision.substring(1));
            var subst = param;
            if (pType == 'b') subst = parseInt(param).toString(2);
            else if (pType == 'c') subst = String.fromCharCode(parseInt(param));
            else if (pType == 'd') subst = parseInt(param) ? parseInt(param) : 0;
            else if (pType == 'u') subst = Math.abs(param);
            else if (pType == 'f') subst = (precision > -1) ? Math.round(parseFloat(param) * Math.pow(10, precision)) / Math.pow(10, precision): parseFloat(param);
            else if (pType == 'o') subst = parseInt(param).toString(8);
            else if (pType == 's') subst = param;
            else if (pType == 'x') subst = ('' + parseInt(param).toString(16)).toLowerCase();
            else if (pType == 'X') subst = ('' + parseInt(param).toString(16)).toUpperCase();
        }
        str = leftpart + subst + rightPart;
    }
    return str;
}