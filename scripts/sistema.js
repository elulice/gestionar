/*
$.fn.clearForm = function() {

   return this.each(function() {

      var type = this.type, tag = this.tagName.toLowerCase();
      if (tag == "form") return $(":input", this).clearForm();
      if (tag == "select") $(this).val(0);

      switch (type) {

	 case "text" : case "password" : case "textarea" : case "file" :
	    this.value = "";
	    break;

	 case "checkbox" :
	    this.selectedIndex = 0;
	    break;

	 case "radio" :
	    $(this).attr("checked", false);
	    break;

	 default : break;
      }
   });

}
*/

function llenar_formulario(form_id, data) {

   var form = $("#" + form_id);

   $.each(data, function(key, value) {

      var elem = $("[name='" + key + "']");
      if (elem != null) var type = $(elem).attr("type");
      var tag = $(elem).attr("nodeName");

      /*console.log(tag + " " + type + " " + key + " => " + value + "\n");*/

      switch (tag) {

	 case "INPUT" :
	    
	    if (type == "text" || type == "password" || type == "hidden") {
	       $(elem).val(value);
	    }
	    else if (type == "radio") {
	       if (value == null) {
		  /* Seleccionar el primer radio */
		  $("[name='" + key + "'] :checked").attr("checked", false);
	       }
	       else {
		  $("[name='" + key + "'][value='" + value + "']").attr("checked", true);
	       }
	    }
	    else if (type == "checkbox")
	       $(elem).attr("checked", true);

	    break;

	 case "TEXTAREA" :
	    $(elem).html(value);
	    //console.log(key + " => " + value + "\n");
	    break;

	 case "SELECT" :
	    $(elem).val(value);
	    break;

	 default : break;
      }

      /*
	 case "textarea" :
	    $(elem).html(value);
	    break;

	 case "checkbox" :
	    $(elem).attr("checked", true);
	    break;

	 default :
	    break;
      }
      */
   });
}

var dp_options = { dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true }

$(document).ready (function() {

   $("div.fila_comandos span").button();

   $("a.navPage").live("click", function() {
      var cont = $(this).parent().parent();

      $.ajax({
	 url: $(this).attr("href"),
	 beforeSend: function() {
	    $(cont).html("");
	 },
	 success: function(res) {
	    $(cont).html(res);
	 }
      });

      return false;
   });

});
