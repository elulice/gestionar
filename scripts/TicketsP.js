$(document).ready (function() {
    actualizar_tickets_usuarias_listado();
    var onlyDni = "";
    $("#p_dni").autocomplete({
        source: "feed.tickets.colaboradores.php",
        minLength: 3,
        select: function( event, ui ) {
            if(ui.item.id != 0){
                $('#p_nombres_new').val(ui.item.nombre);
                //                $('#email_new').val(ui.item.email);
                $('#p_email_cliente_new').val(ui.item.email);
                $('#p_dni_id').val(ui.item.id);
                onlyDni = ui.item.label;
                onlyDni = onlyDni.split(' - ');
                //                alert(onlyDni[0]);

                
                //                if (ui.item.salida == null) {
                //                    $('#p_cliente_new').val(ui.item.cliente);
                //                    updateSucursales(ui.item.clienteid);
                //                } else {
                //                    $('#p_cliente_new').val('');
                //                    updateSucursales(0);
                //                }
                
                $('#p_cliente_new').val(ui.item.cliente);
                updateSucursales(ui.item.clienteid);
                $('#p_dni').val('');
            }
        },
        close: function( event, ui ) {
            //            alert(onlyDni[0]);
            $('#p_dni').val('');
            $('#p_dni').val(onlyDni[0]);
        }
    });
    
    if(typeof $('#p_dni').val() != "undefined") {
        $("#p_dni").data("autocomplete")._renderItem = function(ul, item) {
            
            var bold = '';
            if (item.ultima_entrada) {
                bold = 'font-weight: bold;';
            }
            
            if (item.salida) {
                return $("<li></li>" ).data("item.autocomplete", item).append('<a class="ui-corner-all" tabindex="-1"><span style="color: red;' + bold + '">'+ item.label + '</span></a>').appendTo(ul);
            } else {
                return $("<li></li>" ).data("item.autocomplete", item).append('<a class="ui-corner-all" tabindex="-1"><span style="' + bold + '">'+ item.label + '<span></a>').appendTo(ul);
            }
        };
    }
    
/*$('#p_responsable_new, #p_responsable, #p_responsable_edit').live('change', function(){
        var id = $(this).val();
        var next = $(this).parents('.form-administrar').next();
        $.ajax({
            type: "get",
            url: "feed.usuarias.php?objeto=sector&res_id=" + id,
            beforeSend: function() {
                next.find('#select_sector img').show();
            },
            success: function(res) {
                next.find('#select_sector img').hide();
                next.find('#sector_select').val(res);
            }
        });
    });*/
    
//    $("#p_origen_new").live('change', function(){
//        if($("#p_origen_new").val() == '3'){
//            $('#rem_col').remove();
//            $("#insert_mail_col").html('<div class = "form-administrar" style = "margin-top:9px;" id="rem_col_or"><span class = "form-label"><img src="images/icons/pixel.png" class="icons_new_tickets int_mail"/>Email Colab:</span><input type = "text" name = "email" id = "email_new" class = "form-contacto-text smallInput " style = "width:150px; float:right;"></div>');
//        }else{
//            $('#rem_col_or').remove();
//            $("#insert_email").html('<div class = "form-administrar" style = "margin-top:9px;" id="rem_col"><span class = "form-label"><img src="images/icons/pixel.png" class="icons_new_tickets int_mail"/>Email:</span><input type = "text" name = "email" id = "email_new" class = "form-contacto-text smallInput " style = "width:150px; float:right;"></div>');            
//        }
//    })
    
});

function updateTicketsForm(option){
    var element = $('#cliente_new').detach();
    
    if(option == 0 || option == 1){
        $('#dni').fadeOut();
        $("#original_place_usuaria").append(element);

        clearMainData();
        
        $('#p_nombres_new').val('');
        $('#p_email_cliente_new').val('');
        
        $('#name').fadeIn();
        $('#email').fadeIn();
        return;
    }
    if(option == 2){
        $('#dni').fadeOut();
        $('#name').fadeOut();
        //        $('#email').fadeOut();
        $("#p_cliente_new").val('');
        $("#cliente_new").fadeIn();
        $("#move_usuaria_here").append(element);
        clearMainData()
    }
    if(option == 3){
        $("#cliente_new").fadeOut();
        $('#p_nombres_new').val('');
        $('#p_dni').val('');
        $('#p_email_cliente_new').val('');
        $("#original_place_usuaria").append(element);
        
        $('#name').fadeIn();
        $('#dni').fadeIn();
        $('#email').fadeIn();
    }
}
function showColaboradorInformation(){
    if($('#p_dni').val() == 0 || $('#p_dni').val() == ''){
        alert('Debe seleccionar un colaborador primero');
        return false;
    }else{
        $.ajax({
            type: 'get',
            url: 'get_colaborador_data.php?id='+$('#p_dni_id').val(),
            success: function(data){
                $('<div></div>').html(data).dialog({
                    width: 380,
                    title: 'Datos del colaborador',
                    modal: true,
                    resizable: false,
                    dragable: false
                });
            }
        });

    }
    return false;
}

function clearMainData(){
    $('#p_dni_id').val('');
    $('#p_email_cliente_new').val('');
    $('#p_nombres_new').val('');
}

function actualizar_tickets_usuarias_listado() {

    var div = $("#div_tickets_usuarias_listado");
    var form = $("#frm_tickets_usuarias_listado_filtro");

    $.ajax({
        type: "get",
        url: "feed.tickets.usuarias.php",
        data: $(form).serialize(),
        beforeSend: function() {
            $(div).html('<img src="images/loading.gif" class="loading" />');
        },
        success: function(res) {
            $(div).html(res);
        }
    });

    return false;

}


function fill_responsable_select(id_of_sucursal, select_to_reload){
    $.ajax({
        type: "get",
        url: "scripts/check.if.sucursal.have.responsable.php?id=" + id_of_sucursal,
        success: function(res) {
            $('#'+select_to_reload).html(res);
        }
    });
}
function fill_mail_sucursal(id_of_sucursal, select_to_reload){
    $.ajax({
        type: "get",
        url: "scripts/check.sucursal.responsable.mail.php?id=" + id_of_sucursal+"&ob=sucursal",
        success: function(res) {
            document.getElementById(select_to_reload).value = res;
        //            fill_mail_responsable($('#p_responsable_new').val(), 'p_email_responsable')
        }
    });
    
/*$.ajax({
        type: "get",
        url: "scripts/check.sucursal.responsable.mail.php?id=" + id_of_sucursal+"&ob=sucursal",
        success: function(res) {
            document.getElementById(select_to_reload).value = res;
            fill_mail_responsable($('#p_responsable_new').val(), 'p_email_responsable')
        }
    });*/
}
function fill_mail_responsable(id_of_responsable){
    $.ajax({
        type: "get",
        url: "scripts/check.sucursal.responsable.mail.php?id=" + id_of_responsable,
        success: function(res) {
            $("#p_email_responsable").val(res);
        }
    });
}

$("#p_motivo").live('change',function(){
    id = $("#p_motivo").val();
    $.ajax({
        type: "get",
        url: "scripts/load.error.type.php?id=" + id,
        success: function(res) {
            $('#p_tipo_error_org').html(res);
        }
    });
});

$("#p_motivo_edit").live('change',function(){
    id = $("#p_motivo").val();
    $.ajax({
        type: "get",
        url: "scripts/load.error.type.php?id=" + id,
        success: function(res) {
            $('#p_tipo_error_edit').html(res);
        }
    });
});

function autorizarRespuesta(id){
    var answer = confirm("Seguro desea Autorizar esta respuesta?")
    if (answer) {
        $.ajax({
            type: "get",
            url: "scripts/autorizar.respuesta.php?id=" + id,
            success: function(res) {
                if(res == 'S'){
                    location.reload();
                }else{
                    alert("Ha ocurrido un error al intentar autorizar la respuesta");
                }
                
            }
        });
    }
}

$("#p_responsable_new").live('change', function(){
    fill_mail_responsable($('#p_responsable_new').val())
});

$("#sector_select").live('change', function(){
    fill_mail_responsable($('#p_responsable_new').val())
});
