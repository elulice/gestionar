$(document).ready (function() {

   $("input[name='fecha_desde']").datepicker(dp_options);
   $("input[name='fecha_hasta']").datepicker(dp_options);

   actualizar_agenda_listado();

   $("#div_dialog_agenda").dialog({
      modal: true,
      width: 780,
      autoOpen: false,
      buttons: {
	 "Guardar" : function() {
	    $("#div_dialog_agenda").dialog("close");
	    guardar_agenda();
	 },
	 "Cancelar" : function() {
	    $("#div_dialog_agenda").dialog("close");
	 }
      }
   });

});

function actualizar_agenda_listado() {

   var form = $("#frm_agenda_listado_filtro");

   $.ajax({
      type: "get",
      url:  "feed.agenda.php?objeto=principal",
      data: $(form).serialize(),
      beforeSend: function() {
	 $("#div_agenda_listado").html('<img src="images/loading.gif" class="loading" />');
      },
      success: function(res) {
	 $("#div_agenda_listado").html(res);
      }
   });
}

function editar_agenda(id_registro) {

   $("input[name='id_registro']").val(id_registro);

   $.getJSON("feed.agenda.php?objeto=detalle&id_postulante=" + id_registro, function(ret) {
      llenar_formulario("frm_postulante_notas", ret);
   });

   $("#div_dialog_agenda").dialog("open");

}

function guardar_agenda() {

   var id_registro = $("input[name='id_registro']").val();
   var form = $("#frm_postulante_notas");
   var url = "abm.php?tabla=postulantenotas&columna=NotNro&idregistro=" + id_registro;

   $(form).attr("action", url);
   $(form).submit();
}