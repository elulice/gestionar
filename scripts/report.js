$(document).ready(function(){

    $("#report-dialog").dialog({
        modal: true,
        autoOpen: false,
        width: 980,
        height: 600,
        resizable: false,
        title: '<span class="ui-icon ui-icon-folder-open"></span> Reporte',
        resizeStop: function(event, ui) {
            $('#report-dialog').dialog('option', 'position', 'center');
        }
    });
    
    $('#container-fields input[type="checkbox"]').click(function(){
        var options = '<option value="">- Campo -</option>';
        $('#container-fields input[type="checkbox"]:checked').each(function(){
            options += '<option value="' + $(this).attr('id') + '">' + $('label[for="' + $(this).attr('id') + '"]').html() + '</option>';
        });
        $('.sel-total-campos').html(options);
    });
    
    $('#groupby').change(function(){
        if ($(this).val()) {
            $('#count_tickets').attr('checked', true);
        } else {
            $('#count_tickets').attr('checked', false);
        }
    });
    
    $('#filters_estado').change(function(){
        var name = $("#filters_estado option:selected").html();
        $('#filters_estado_name').val(name);
    });
    
    $('#filters_responsed_by').change(function(){
        var name = $("#filters_responsed_by option:selected").html();
        $('#filters_responsed_by_name').val(name);
    });
    
    $('#filters_edited_by').change(function(){
        var name = $("#filters_edited_by option:selected").html();
        $('#filters_edited_by_name').val(name);
    });
    
    $('#filters_nom_solic').change(function(){
        var name = $("#filters_nom_solic option:selected").html();
        $('#filters_nom_solic_name').val(name);
    });
    
    $('#filters_usuaria').change(function(){
        var name = $("#filters_usuaria option:selected").html();
        $('#filters_usuaria_name').val(name);
    });
    
});

function generateReport(){
    if($("#report-dialog").length > 0){
        $.ajax({
            type: "POST",
            url: "inc.ticket.reporte.php",
            beforeSend: function(){
                $("#report-dialog").html('<img src="images/loading.gif" class="loading" />');
            },
            success: function(msg){
                $("#report-dialog").html(msg);
                $("#report-dialog").dialog('open');
                $('#report-dialog').dialog('option', 'position', 'center');
                loadListReport();
            }
        });
    }
}

function loadListReport(){
    var data = $('#form_report').serialize();
    $.ajax({
        type: "GET",
        url: "feed.report.tickets.php",
        data: data,
        beforeSend: function(){
            $("#repo-div").html('<img src="images/loading.gif" class="loading" />');
        },
        success: function(msg){
            $("#repo-div").html(msg);
        }
    });
}

function loadChart(){
    var data = $('#form_report').serialize();
    var data_chart = $('#form_chart').serialize();
    $.ajax({
        type: "GET",
        url: "feed.report.tickets.php?chart=1",
        data: data + '&' + data_chart,
        beforeSend: function(){
            $("#repo-div").hide();
            $("#menu-repo-div").hide();
            $("#graficos-div").show();
            $("#menu-chart-div").show();
            $("#graficos-div").html('<img src="images/loading.gif" class="loading" />');
        },
        success: function(msg){
            $("#graficos-div").html(msg);
        }
    });
}

function loadList(){
    var data = $('#form_report').serialize();
    $.ajax({
        type: "GET",
        url: "feed.report.tickets.php",
        data: data,
        beforeSend: function(){
            $("#graficos-div").hide();
            $("#menu-chart-div").hide();
            $("#repo-div").show();
            $("#menu-repo-div").show();
            $("#repo-div").html('<img src="images/loading.gif" class="loading" />');
        },
        success: function(msg){
            $("#repo-div").html(msg);
        }
    });
}

$(document).ready(function(){
    
    
    });
function requestData() {
    $.getJSON("feed.produccion.graficos.php", null, function(point){
        var series = grafico.series[0],
        shift = series.data.length > 20; // shift if the series is longer than 20

        // add the point
        grafico.series[0].addPoint(point, true, shift);
            
        // call it again after one second
        if(!shift)
            setTimeout(requestData, 1000);    
    });
    
}
function cambiarComparacion(elemName, link){
    var htmls = ['ui-icon ui-icon-plusthick', 'ui-icon ui-icon-minusthick', '<strong>=</strong>'];
    var titles = ['Mayor que', 'Menor que', 'Igual a'];
    var comp = parseInt($('input[name="'+elemName +'"]').val()) + 1;
    if(comp == 3)
        comp = 0;
    $('input[name="'+elemName +'"]').val(comp);
    var span = $(link).children('span').children('span');
    if(comp != 2){
        $(span).removeClass(htmls[comp-1]);
        $(span).addClass(htmls[comp]);
    }
    else{
        $(span).removeClass();
        $(span).html(htmls[comp]);
    }
    //    $(link).html(htmls[comp]);
    $(link).attr('title', titles[comp]);
}
function cambiarComparacion(elemName, link){
    var htmls = ['ui-icon ui-icon-plusthick', 'ui-icon ui-icon-minusthick', '<strong>=</strong>'];
    var titles = ['Mayor que', 'Menor que', 'Igual a'];
    var comp = parseInt($('input[name="'+elemName +'"]').val()) + 1;
    if(comp == 3)
        comp = 0;
    $('input[name="'+elemName +'"]').val(comp);
    var span = $(link).children('span').children('span');
    if(comp != 2){
        $(span).removeClass(htmls[comp-1]);
        $(span).addClass(htmls[comp]);
    }
    else{
        $(span).removeClass();
        $(span).html(htmls[comp]);
    }
    //    $(link).html(htmls[comp]);
    $(link).attr('title', titles[comp]);
}
function guardarReporte(como){
    var q = 'template'
    if(como != null){
        q = 'reporte';
    }
    formatearFechas();
    $.ajax({
        type: "POST",
        data: $('#form_report').serialize(),
        url: "guardar_reporte.php?q="+q,
        success: function(msg){
            dialogoRespuesta(msg);
            formatearFechas();
        }
    });
}
function cargarReporte(id){
    if(id > 0){
        $.getJSON("feed.produccion.php",{
            id: id,
            objeto: 'reporte'
        }, function(j){
            clear_form_elements('#form_report');
            $.each(j, function(index, value) {
                var elem = $('[name="'+value.name+'"]')
                if($(elem).attr('type') == 'checkbox'){
                    if(value.value == 1){
                        $(elem).attr('checked', 'checked');
                    }
                    else{
                        $(elem).removeAttr('checked');
                    }
                    actualizarCheckeds();
                }
                else{
                    $('[name="'+value.name+'"]').val(value.value);
                }
            });
            actualizarComparacion();
            formatearFechas();
            
            $("#reportes-dialog").dialog('close');
        });
    }
}
function actualizarComparacion(){
    var htmls = ['ui-icon ui-icon-plusthick', 'ui-icon ui-icon-minusthick', '<strong>=</strong>'];
    var titles = ['Mayor que', 'Menor que', 'Igual a'];
    $('.comparador').each(function(index, element){
        var comp = $(element).val();
        if(comp == ''){
            $(element).val(0);
            comp = 0;
        }
        //alert(comp);
        var link = $(element).parent().find('a.buttons');
        var span = $(link).children('span').children('span');
        if(comp != 2){
            $(span).removeClass();
            $(span).addClass(htmls[comp]);
        }
        else{
            $(span).removeClass();
            $(span).html(htmls[comp]);
        }
        $(link).attr('title', titles[comp]);
    });
}
function actualizarListadoReportes(){
    if($("#reportes-dialog").length > 0){
        $.ajax({
            type: "GET",
            url: "feed.produccion.php?objeto=reportes",
            beforeSend: function(){
                $("#reportes-dialog").html('<img src="images/loading.gif" class="loading" />');
            },
            success: function(msg){
                $("#reportes-dialog").html(msg);
                $('#reportes-dialog').dialog('option', 'position', 'center');
            }
        });
    }
}
function actualizarListadoHistoriales(){
    if($("#historiales-dialog").length > 0){
        $.ajax({
            type: "GET",
            url: "feed.produccion.php?objeto=historiales",
            beforeSend: function(){
                $("#historiales-dialog").html('<img src="images/loading.gif" class="loading" />');
            },
            success: function(msg){
                $("#historiales-dialog").html(msg);
                $('#historiales-dialog').dialog('option', 'position', 'center');
            }
        });
    }
}

function generarReporteProduccion(){
    if($("#produccion-dialog").length > 0){
        $.ajax({
            type: "GET",
            url: "inc.produccion.reporte.php",
            beforeSend: function(){
                $("#produccion-dialog").html('<img src="images/loading.gif" class="loading" />');
            },
            success: function(msg){
                $("#produccion-dialog").html(msg);
                $("#produccion-dialog").dialog('open');
                $('#produccion-dialog').dialog('option', 'position', 'center');
            }
        });
    }
}

function generarReporteHistorial(nombre){
    $("#historiales-dialog").dialog('close');
    if($("#produccion-dialog").length > 0){
        $.ajax({
            type: "GET",
            url: "inc.produccion.reporte.php?tipo=histo&nombre="+nombre,
            beforeSend: function(){
                $("#produccion-dialog").html('<img src="images/loading.gif" class="loading" />');
            },
            success: function(msg){
                $("#produccion-dialog").html(msg);
                $("#produccion-dialog").dialog('open');
                $('#produccion-dialog').dialog('option', 'position', 'center');
            }
        });
    }
}
function generarListadoReporteProduccion(){
    if($("#repo-div").length > 0){
        formatearFechas();
        $.ajax({
            type: "POST",
            url: "feed.produccion.php?objeto=repo",
            data: $('#form_report').serialize(),
            beforeSend: function(){
                $("#repo-div").html('<img src="images/loading.gif" class="loading" />');
            },
            success: function(msg){
                $("#repo-div").html(msg);
                formatearFechas();
            }
        });
    }
}
function generarListadoHistorialProduccion(nombre){
    if($("#repo-div").length > 0){
        $.ajax({
            type: "GET",
            url: "feed.produccion.php?objeto=histo&nombre="+nombre,
            //data: $('#formProduccion').serialize(),
            beforeSend: function(){
                $("#repo-div").html('<img src="images/loading.gif" class="loading" />');
            },
            success: function(msg){
                $("#repo-div").html(msg);
            }
        });
    }
}
function getTotalesReporte(){
    if($("#resultados-totales").length > 0){
        formatearFechas();
        $.ajax({
            type: "POST",
            url: "feed.produccion.php?objeto=repo-tot",
            data: $('#form_report').serialize(),
            success: function(msg){
                $("#resultados-totales").html(msg);
                formatearFechas();
            }
        });
    }
}
function getTotalesReporteHisto(nombre){
    if($("#resultados-totales").length > 0){
        $.ajax({
            type: "GET",
            url: "feed.produccion.php?objeto=histo-tot&nombre="+nombre,
            //data: $('#formProduccion').serialize(),
            beforeSend: function(){
                $("#resultados-totales").html('<img src="images/loading.gif" class="loading" />');
            },
            success: function(msg){
                $("#resultados-totales").html(msg);
            }
        });
    }
}

function imprimirReporte() {

    var win = window.open();
    win.document.write($("#repo-div").html() + $("#tot-funciones").html() + "<br />" + $("#tot-filtros").html());
    win.print();
    win.close();

}

function exportarReporteExcel(){ 
    $('#excel').submit();
}
function exportarReportePdf(){ 
    $('#pdf').submit();
}