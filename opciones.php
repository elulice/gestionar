<?php

session_start();

if (!$_SESSION["strict_menu"]) {
    header("Location: escritorio.php");
}
$seccion = "opciones";
$titulo = "Opciones";

include("clases/framework-1.0/class.bd.php");
include("includes/funciones.php");
require_once ('clases/phppaging/PHPPaging.lib.php');

$seccion_principal = "paises";

include("inc.encabezado.php");

/*
  if (!isset($_GET["sub"])) $sub = $seccion_principal;
  else $sub = $_GET["sub"];
  $filename = "inc.generales.$sub.php";

  if (file_exists($filename)) include_once($filename);
  else echo "ERR";
 */

include("inc.opciones.php");
include("inc.pie.php");
?>
