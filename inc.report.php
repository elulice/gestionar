<link rel="stylesheet" type="text/css" href="css/960.css" />
<link rel="stylesheet" type="text/css" href="css/reset.css" />
<link rel="stylesheet" type="text/css" href="css/text.css" />
<link rel="stylesheet" type="text/css" href="css/blue.css" />
<link rel="stylesheet" type="text/css" href="css/shadowbox.css" />
<link type="text/css" href="css/redmond/jquery-ui-1.8.10.custom.css" rel="stylesheet" />
<link rel='stylesheet' type='text/css' href='css/fullcalendar.css' />

<script type="text/javascript" src="js/jquery-1.4.4.min.js"></script>
<script type="text/javascript" src="js/blend/jquery.blend.js"></script>

<script type="text/javascript" src="js/jquery-ui-1.8.10.custom.js"></script>
<script type="text/javascript" src="scripts/adminseg.js"></script>
<script type="text/javascript" src="scripts/report.js"></script>

<script type="text/javascript" src="scripts/shadowbox.js"></script>
<script type='text/javascript' src='scripts/fullcalendar.js'></script>
<script src="js/jquery-ui-timepicker-addon.js" type="text/javascript"></script>
<script src="scripts/highchart/highcharts.js" type="text/javascript"></script>
<script src="scripts/highchart/modules/exporting.js" type="text/javascript"></script>

<script type="text/javascript" src="js/effects.js"></script>
<script language="JavaScript" type="text/javascript" src="scripts/general.js"></script>
<script language="JavaScript" type="text/javascript" src="scripts/swfobject.js"></script>
<script language="JavaScript" type="text/javascript" src="scripts/correctPNG.js"></script>
<script language="JavaScript" type="text/javascript" src="scripts/ajax.php"></script>
<script type="text/javascript" src="scripts/jScrollPane.js"></script>
<link rel="stylesheet" type="text/css" media="all" href="css/jScrollPane.css" />

<style type="text/css">
    /* css for timepicker */
    .ui-timepicker-div .ui-widget-header{ margin-bottom: 8px; }
    .ui-timepicker-div dl{ text-align: left; }
    .ui-timepicker-div dl dt{ height: 25px; }
    .ui-timepicker-div dl dd{ margin: -25px 0 10px 65px; }
    .ui-timepicker-div td { font-size: 90%; }
    .ui-dialog .ui-dialog-title .ui-icon {
        float: left;
        margin-right: 4px;
        margin-top:1px;
    }
    .ui-button-text-only .ui-button-text { padding: .2em 1em; font-size:11px; font-weight:normal; }

    #container-fields { width:880px; border-top: 1px solid #CCCCCC; display:inline; float:left; height:145px; overflow:hidden; padding: 10px 0px 0px 25px; }

    .rep-form-footer { width:900px; display:block; float:left; clear: both; padding-left: 5px; border-bottom: 1px solid #DDDDDD;border-top: 1px solid #DDDDDD;margin-bottom: 5px; }
    .rep-form-footer div { float: left; padding: 0pt 15px 0 0; margin: 5px 0pt; height: 24px; }
    .rep-form-footer div select { width:80px; float:left; margin: 0 2px; }
    .rep-form-footer div label { font-size: 13px; font-weight: bold; }
</style>

<div id="portlets">
    <div class="grid_8" style="width: 165px;">
        <h1 class="prod_edit">Reportes</h1></div>
    <div style=" float:right;display:block; width:455px; margin-top:15px;">
        <!--<a href="javascript: void(0);" onClick="$('#historiales-dialog').dialog('open')" class="buttons buttons-history" style="width: 100px; font-size:11px; font-weight: normal; float: left; margin-left: 5px;background: #EAEAEA; color: #666;">Historial</a>-->
        <a href="javascript: void(0);" onClick="$('#reportes-dialog').dialog('open')" class="buttons buttons-cargar" style="width: 100px; font-size:11px; font-weight: normal; float: left; margin-left: 5px; background: #EAEAEA; color: #666;">Cargar</a>
        <a href="javascript: void(0);" onClick="if($('[name=id_reporte]').val() == 0){$('#nombre-dialog').dialog('open');} else{guardarReporte();}" class="buttons buttons-guardar" style="width: 100px; font-size:11px; font-weight: normal; float: left; margin-left: 5px; background: #EAEAEA; color: #666;">Guardar</a>
        <a href="javascript: void(0);" onClick="$('#nombre-dialog').dialog('open');" class="buttons buttons-guardar-como" style="width: 120px; font-size:11px; font-weight: normal; float: left; margin-left: 5px; background: #EAEAEA; color: #666;">Guardar como</a>
        <a href="javascript: void(0);" onClick="generateReport();" class="buttons buttons-consultar" style="width: 100px; font-size:11px; font-weight: bold; float: left; margin-left: 5px;">Consultar</a>
    </div>
    <div class="clear"></div>
    <form action="#" method="get" name="form_report" id="form_report">

        <div class="portlet">
            <span class="portlet-header" style="color: #333333 !important;">Datos a Consultar</span>
            <div id="container-fields">

                <div style="width:125px; margin-right:15px; display:block; float:left;">
                    <div class="form-administrar2"><input name="fields[all_tickets][id]" id="all_tickets-id" type="checkbox" class="form-contacto-text selectCheckbox"/> <label for="all_tickets-id">Nro. Ticket</label></div>
                    <div class="form-administrar2"><input name="fields[all_tickets][email_cliente]" id="all_tickets-email_cliente" type="checkbox" class="form-contacto-text selectCheckbox"/> <label for="all_tickets-email_cliente">Email</label></div>
                    <div class="form-administrar2"><input name="fields[estado_tickets][nombre]" id="estado_tickets-nombre" type="checkbox" class="form-contacto-text selectCheckbox"/> <label for="estado_tickets-nombre">Estado</label></div>
                    <div class="form-administrar2"><input name="fields[respuesta_tickets][responsed_by]" id="respuesta_tickets-responsed_by" type="checkbox" class="form-contacto-text selectCheckbox"/> <label for="respuesta_tickets-responsed_by">Respondido por</label></div>
                    <div class="form-administrar2"><input name="fields[all_tickets][creacion_respuesta]" id="all_tickets-creacion_respuesta" type="checkbox" class="form-contacto-text selectCheckbox"/> <label for="all_tickets-creacion_respuesta">Creación/Respuesta</label></div>
                </div>
                <div style="width:100px; margin-right:15px; display:block; float:left;">
                    <div class="form-administrar2"><input name="fields[all_tickets][fecha]" id="all_tickets-fecha" type="checkbox" class="form-contacto-text selectCheckbox"/> <label for="all_tickets-fecha">Fecha Alta</label></div>
                    <div class="form-administrar2"><input name="fields[cliente][CliRsocial]" id="cliente-CliRsocial" type="checkbox" class="form-contacto-text selectCheckbox"/> <label for="cliente-CliRsocial">Usuaria</label></div>
                    <div class="form-administrar2"><input name="fields[all_tickets][email_send]" id="all_tickets-email_send" type="checkbox" class="form-contacto-text selectCheckbox"/> <label for="all_tickets-email_send">Email Copia</label></div>
                    <div class="form-administrar2"><input disabled="disabled" name="id_producto" id="id_producto" type="checkbox" class="form-contacto-text selectCheckbox"/> <label for="id_producto">Mes</label></div>
                    <div class="form-administrar2"><input name="fields[all_tickets][creacion_cierre]" id="all_tickets-creacion_cierre" type="checkbox" class="form-contacto-text selectCheckbox"/> <label for="all_tickets-creacion_cierre">Creación/Cierre</label></div>
                </div>
                <div style="width:125px; margin-right:15px; display:block; float:left;">
                    <div class="form-administrar2"><input name="fields[respuesta_tickets][respuesta_fecha]" id="respuesta_tickets-respuesta_fecha" type="checkbox" class="form-contacto-text selectCheckbox"/> <label for="respuesta_tickets-respuesta_fecha">Fecha Respuesta</label></div>
                    <div class="form-administrar2"><input name="fields[origen_tickets][id]" id="origen_tickets-id" type="checkbox" class="form-contacto-text selectCheckbox"/> <label for="origen_tickets-id">Origen</label></div>
                    <div class="form-administrar2"><input name="fields[all_tickets][email_cc]" id="all_tickets-email_cc" type="checkbox" class="form-contacto-text selectCheckbox"/> <label for="all_tickets-email_cc">Email Copia 2</label></div>
                    <div class="form-administrar2"><input disabled="disabled" name="id_producto" id="id_producto" type="checkbox" class="form-contacto-text selectCheckbox"/> <label for="id_producto">Año</label></div>
                    <div class="form-administrar2"><input name="fields[all_tickets][respuesta_cierre]" id="all_tickets-respuesta_cierre" type="checkbox" class="form-contacto-text selectCheckbox"/> <label for="all_tickets-respuesta_cierre">Respuesta/Cierre</label></div>
                </div>
                <div style="width:125px; margin-right:15px; display:block; float:left;">
                    <div class="form-administrar2"><input name="fields[responded_to_client][rtc_fecha]" id="responded_to_client-rtc_fecha" type="checkbox" class="form-contacto-text selectCheckbox"/> <label for="responded_to_client-rtc_fecha">Fecha Envio/Cierre</label></div>
                    <div class="form-administrar2"><input name="fields[unidadorg][UniNro]" id="unidadorg-UniNombre" type="checkbox" class="form-contacto-text selectCheckbox"/> <label for="unidadorg-UniNombre">Sucursal</label></div>
<!--                    <div class="form-administrar2"><input disabled="disabled" name="demora_interna" id="demora_interna" type="checkbox" class="form-contacto-text selectCheckbox"/> <label for="demora_interna">Demora interna</label></div>-->
                    <div class="form-administrar2"><input name="fields[respuesta_tickets][content_response]" id="respuesta_enviada" type="checkbox" class="form-contacto-text selectCheckbox"/> <label for="respuesta_enviada">Respuesta</label></div>
                    <div class="form-administrar2"><input name="count_tickets" id="count_tickets" type="checkbox" class="form-contacto-text selectCheckbox"/> <label for="count_tickets">Cantidad de tickets</label></div>
                </div>
                <div style="width:125px; margin-right:15px; display:block; float:left;">
                    <div class="form-administrar2"><input name="fields[tipo_solicitante][nombre]" id="tipo_solicitante-nombre" type="checkbox" class="form-contacto-text selectCheckbox"/> <label for="tipo_solicitante-nombre">Tipo Solicitante</label></div>
                    <div class="form-administrar2"><input name="fields[miembroempresa][MempNombres]" id="miembroempresa-MEmpNombres" type="checkbox" class="form-contacto-text selectCheckbox"/> <label for="miembroempresa-MEmpNombres">Responsable</label></div>
                    <!--<div class="form-administrar2"><input disabled="disabled" name="demora_cliente" id="demora_cliente" type="checkbox" class="form-contacto-text selectCheckbox"/> <label for="demora_cliente">Demora cliente</label></div>-->
                    <div class="form-administrar2"><input name="fields[all_tickets][ticket_contenido]" id="reclamo" type="checkbox" class="form-contacto-text selectCheckbox"/> <label for="reclamo">Reclamo</label></div>
                    <div class="form-administrar2"><input name="fields[motivos][nombre_motivo]" id="motivos-nombre_motivo" type="checkbox" class="form-contacto-text selectCheckbox"/> <label for="motivos-nombre_motivo">Motivo</label></div>
                </div>
                <div style="width:125px; margin-right:15px; display:block; float:left;">
                    <div class="form-administrar2"><input name="fields[all_tickets][tickets_nombre]" id="all_tickets-tickets_nombre" type="checkbox" class="form-contacto-text selectCheckbox"/> <label for="all_tickets-tickets_nombre">Nombre Solicitante</label></div>
                    <div class="form-administrar2"><input name="fields[area][AreNom]" id="area-AreNom" type="checkbox" class="form-contacto-text selectCheckbox"/> <label for="area-AreNom">Sector</label></div>
                    <div class="form-administrar2"><input name="fields[tickets_edit][edited_by]" id="tickets_edit-edited_by" type="checkbox" class="form-contacto-text selectCheckbox"/> <label for="tickets_edit-edited_by">Editado por</label></div>
                    <div class="form-administrar2"><input name="fields[tipo_error][nombre_error]" id="tipo_error-nombre_error" type="checkbox" class="form-contacto-text selectCheckbox"/> <label for="tipo_error-nombre_error">Tipo Error</label></div>
                </div>

            </div>
            <div class="portlet">
                <span class="portlet-header" style="color: #333333 !important;">Filtros</span>
                <div style="width:880px; display:inline; border-top: 1px solid #CCCCCC; float:left; height:180px; overflow:hidden; padding: 10px 0px 0px 25px; font-weight: normal;" >
                    <div style="width:190px; margin-right:15px; display:block; float:left;">
                        <div class="form-administrar2" style="width:190px;"><label for="filters_fecha_desde" style="width:120px;">Fecha de Alta desde:</label>
                            <input type="text" name="filters[fecha][desde]" id="filters_fecha_desde" style="width:60px; float:right;" value="" class="datepicker form-contacto-text smallInput"/>
                        </div>
                        <div class="form-administrar2" style="width:190px;"><label for="filters_fecha_hasta" style="width:120px;">Fecha de Alta hasta:</label>
                            <input type="text" name="filters[fecha][hasta]" id="filters_fecha_hasta"  style="width:60px; float:right;" value="" class="datepicker form-contacto-text smallInput"/>
                        </div>
                        <div class="form-administrar2" style="width:190px;"><label for="fecha_respuesta-desde" style="width:120px;">Fecha de Resp. desde:</label>
                            <input type="text" name="filters[fecha_respuesta][desde]" id="filters_fecha_respuesta_desde" style="width:60px; float:right;" value="" class="datepicker form-contacto-text smallInput"/>
                        </div>
                        <div class="form-administrar2" style="width:190px;"><label for="fecha_respuesta-hasta" style="width:120px;">Fecha de Resp. hasta:</label>
                            <input type="text" name="filters[fecha_respuesta][hasta]" id="filters_fecha_respuesta_hasta"  style="width:60px; float:right;" value="" class="datepicker form-contacto-text smallInput"/>
                        </div>
                        <div class="form-administrar2" style="width:190px;"><label for="fecha_envio-desde" style="width:120px;">Fecha Envio/Cierre d:</label>
                            <input type="text" name="filters[fecha_envio][desde]" id="fecha_envio-desde" style="width:60px; float:right;" value="" class="datepicker form-contacto-text smallInput"/>
                        </div>
                        <div class="form-administrar2" style="width:190px;"><label for="fecha_envio-hasta" style="width:120px;">Fecha Envio/Cierre h:</label>
                            <input type="text" name="filters[fecha_envio][hasta]" id="fecha_envio-hasta"  style="width:60px; float:right;" value="" class="datepicker form-contacto-text smallInput"/>
                        </div>
                    </div>
                    <div style="width:190px; margin-right:15px; display:block; float:left;">
                        <div class="form-administrar2" style="width:190px;"><label for="filters_solicitante" style="width:100px;">Solicitante:</label>
                            <div style="width:90px; display:block; float:right;">
                                <select name="filters[solicitante][id]" id="filters_solicitante" class="smallInput" title="Solicitante" style="width:90px; float:right; margin-bottom: 5px ;" >
                                    <?php
                                    $query = "SELECT idsolicitante, nombre FROM tipo_solicitante ORDER BY nombre";
                                    echo GenerarOptions($query, NULL, TRUE, "- Campo -");
                                    ?>
                                </select>
                                <input type="hidden" name="filters[solicitante][name]" id="filters_solicitante_name" value="" />
                            </div>
                        </div>
                        <div class="form-administrar2" style="width:190px;"><label for="filters_usuaria" style="width:100px;">Usuaria:</label>
                            <div style="width:90px; display:block; float:right;">
                                <select name="filters[usuaria][id]" id="filters_usuaria" class="smallInput" title="Usuaria" style="width:90px; float:right; margin-bottom: 5px ;" >
                                    <?php
                                    $query = "SELECT CliNro, cliente.CliRsocial FROM all_tickets 
                                        LEFT JOIN cliente ON all_tickets.cliente_id = cliente.CliNro 
                                        WHERE CliRsocial != ''
                                        GROUP BY CliRsocial ORDER BY CliRsocial";
                                    echo GenerarOptions($query, NULL, TRUE, "- Campo -");
                                    ?>
                                </select>
                                <input type="hidden" name="filters[usuaria][name]" id="filters_usuaria_name" value="" />
                            </div>
                        </div>
                        <div class="form-administrar2" style="width:190px;"><label for="filters_sucursal" style="width:100px;">Sucursal:</label>
                            <div style="width:90px; display:block; float:right;">
                                <select name="filters[sucursal][id]" id="filters_sucursal" class="smallInput" title="Sucursal" style="width:90px; float:right; margin-bottom: 5px ;" >
                                    <?php
                                    $query = "SELECT UniNro, UniNombre FROM unidadorg ORDER BY UniNombre";
                                    echo GenerarOptions($query, NULL, TRUE, "- Campo -");
                                    ?>
                                </select>
                                <input type="hidden" name="filters[sucursal][name]" id="filters_sucursal_name" value="" />
                            </div>
                        </div>
                        <div class="form-administrar2" style="width:190px;"><label for="filters_estado" style="width:100px;">Estado:</label>
                            <div style="width:90px; display:block; float:right;">
                                <select name="filters[estado][id]" id="filters_estado" class="smallInput" title="Estado" style="width:90px; float:right; margin-bottom: 5px ;" >
                                    <?php
                                    $query = "SELECT id, nombre FROM estado_tickets ORDER BY id";
                                    echo GenerarOptions($query, NULL, TRUE, "- Campo -");
                                    ?>
                                </select>
                                <input type="hidden" name="filters[estado][name]" id="filters_estado_name" value="" />
                            </div>
                        </div>
                        <div class="form-administrar2" style="width:190px;"><label for="f_fecha_recibido_h" style="width:100px;">Tiempo Resp:</label>
                            <input disabled="disabled" type="text" name="" id=""  style="width:82px; float:left;" value="" class="form-contacto-text smallInput"/>
                        </div>
                    </div>
                    <div style="width:190px; margin-right:15px; display:block; float:left;">
                        <div class="form-administrar2" style="width:190px;"><label for="filters_responsed" style="width:100px;">Respondido:</label>
                            <div style="width:90px; display:block; float:right;">
                                <select name="filters[responsed][id]" id="filters_responsed" class="smallInput" title="Respondido" style="width:90px; float:right; margin-bottom: 5px ;" >
                                    <option value="">- Campo -</option>
                                    <option value="S">Si</option>
                                    <option value="N">No</option>
                                </select>
                                <input type="hidden" name="filters[responsed][name]" id="filters_responsed_name" value="" />
                            </div>
                        </div>
                        <div class="form-administrar2" style="width:190px;"><label for="filters_enviado" style="width:100px;">Enviado:</label>
                            <div style="width:90px; display:block; float:right;">
                                <select name="filters[enviado][id]" id="filters_enviado" class="smallInput" title="Enviado" style="width:90px; float:right; margin-bottom: 5px ;" >
                                    <option value="">- Campo -</option>
                                    <option value="S">Si</option>
                                    <option value="N">No</option>
                                </select>
                                <input type="hidden" name="filters[enviado][name]" id="filters_enviado_name" value="" />
                            </div>
                        </div>
                        <div class="form-administrar2" style="width:190px;"><label for="filters_responsed_by" style="width:100px;">Respondido Por:</label>
                            <div style="width:90px; display:block; float:right;">
                                <select name="filters[responsed_by][id]" id="filters_responsed_by" class="smallInput" title="Respondido Por" style="width:90px; float:right; margin-bottom: 5px ;" >
                                    <?php
                                    $query = "SELECT ticket_id, responsed_by FROM respuesta_tickets GROUP BY responsed_by ORDER BY responsed_by";
                                    echo GenerarOptions($query, NULL, TRUE, "- Campo -");
                                    ?>
                                </select>
                                <input type="hidden" name="filters[responsed_by][name]" id="filters_responsed_by_name" value="" />
                            </div>
                        </div>
                        <div class="form-administrar2" style="width:190px;"><label for="filters_edited_by" style="width:100px;">Editado Por:</label>
                            <div style="width:90px; display:block; float:right;">
                                <select name="filters[edited_by][id]" id="filters_edited_by" class="smallInput" title="Editado Por" style="width:90px; float:right; margin-bottom: 5px ;" >
                                    <?php
                                    $query = "SELECT ticket_id, edited_by FROM tickets_edit GROUP BY edited_by ORDER BY edited_by";
                                    echo GenerarOptions($query, NULL, TRUE, "- Campo -");
                                    ?>
                                </select>
                                <input type="hidden" name="filters[edited_by][name]" id="filters_edited_by_name" value="" />
                            </div>
                        </div>
                        <div class="form-administrar2" style="width:190px;"><label for="filters_nom_solic" style="width:100px;">Nombre Solicitante:</label>
                            <div style="width:90px; display:block; float:right;">
                                <select name="filters[nom_solic][id]" id="filters_nom_solic" class="smallInput" title="Nombre Solicitante" style="width:90px; float:right; margin-bottom: 5px ;" >
                                    <?php
                                    $query = "SELECT id, tickets_nombre FROM all_tickets WHERE tickets_nombre != '' GROUP BY tickets_nombre ORDER BY tickets_nombre";
                                    echo GenerarOptions($query, NULL, TRUE, "- Campo -");
                                    ?>
                                </select>
                                <input type="hidden" name="filters[nom_solic][name]" id="filters_nom_solic_name" value="" />
                            </div>
                        </div>
                    </div>
                    <div style="width:90px; margin-right:15px; display:block; float:left;">
                        <div class="form-administrar2" style="width:190px;"><label for="orderdir" style="width:100px;">Ordenar:</label>
                            <select name="orderdir" id="orderdir"  class="smallInput" title="Ordenar por" style="width:90px; float:left; margin-bottom: 5px ;" >
                                <option value="ASC">Ascendente</option>
                                <option value="DESC">Descendente</option>
                            </select>
                        </div>
                        <div class="form-administrar2" style="width:190px;"><label for="orderby" style="width:100px;">Ordenar por:</label>
                            <select name="orderby" id="orderby"  class="smallInput sel-total-campos" title="Ordenar por" style="width:90px; float:left; margin-bottom: 5px ;" >
                                <option value="">- Campo -</option>
                            </select>
                        </div>
                        <div class="form-administrar2" style="width:190px;"><label for="o_order" style="width:100px;">Agrupar por:</label>
                            <select name="groupby" id="groupby"  class="smallInput sel-total-campos" title="Ordenar por" style="width:90px; float:left; margin-bottom: 5px ;" >
                                <option value="">- Campo -</option>
                            </select>
                        </div>
                        <div class="form-administrar2" style="width:190px;"><label for="filters_motivo" style="width:70px;">Motivo:</label>
                            <div style="width:120px; display:block; float:left;">
                                <select name="filters[motivo][id]" id="filters_motivo" class="smallInput" title="Motivo" style="width:90px; float:right; margin-bottom: 5px ;" >
                                    <?php
                                    $query = "SELECT id, nombre_motivo FROM motivos ORDER BY nombre_motivo";
                                    echo GenerarOptions($query, NULL, TRUE, "- Campo -");
                                    ?>
                                </select>
                                <input type="hidden" name="filters[motivo][name]" id="filters_motivo_name" value="" />
                            </div>
                        </div>
                        <div class="form-administrar2" style="width:190px;"><label for="filters_tipo_error" style="width:70px;">Tipo de Error:</label>
                            <div style="width:120px; display:block; float:left;">
                                <select name="filters[tipo_error][id]" id="filters_tipo_error" class="smallInput" title="Tipo de Error" style="width:90px; float:right; margin-bottom: 5px ;" >
                                    <?php
                                    $query = "SELECT iderror, nombre_error FROM tipo_error ORDER BY nombre_error";
                                    echo GenerarOptions($query, NULL, TRUE, "- Campo -");
                                    ?>
                                </select>
                                <input type="hidden" name="filters[tipo_error][name]" id="filters_tipo_error_name" value="" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="rep-form-footer">
                <div>
                    <span class="label">Totales:</span>
                </div>
                <?php for ($i = 1; $i <= 4; $i++): ?>
                    <div>
                        <select disabled="disabled" name="totales_<?php echo $i ?>_func"  class="smallInput">
                            <option value="" selected="selected">- Función -</option>
                            <option value="cuenta" >Cuenta</option>
                            <option value="suma" >Suma</option>
                            <option value="promedio" >Promedio</option>
                            <option value="mayor" >Mayor</option>
                            <option value="menor" >Menor</option>
                        </select>
                        <select disabled="disabled" name="total-campo-<?php echo $i ?>"  class="smallInput sel-total-campos">
                            <option>- Campo -</option>
                        </select>
                    </div>
                <?php endfor; ?>
            </div>
            <input type="hidden" name="nombre_reporte" value="" />
            <input type="hidden" name="id_reporte" value="0" />
            <input type="hidden" name="cot_cotizar" value="0" />
            <div id="serialized-report" style="display: none;"></div>
            <div id="nombre-dialog" style="display: none;">
                <div id="dialog-response" title="Alerta" style="display:none;"></div>
                <label for="tmp_reporte" style="width:70px;">Nombre:</label>
                <input type="text" name="tmp_reporte" id="tmp_reporte" style="width:200px;" value="" class="form-contacto-text smallInput"/>
            </div>
            <div id="nombrehist-dialog" style="display: none;">
                <label for="tmp_hist" style="width:70px;">Nombre:</label>
                <input type="text" name="tmp_hist" id="tmp_hist" style="width:200px;" value="" class="form-contacto-text smallInput"/>
            </div>
            <div id="reportes-dialog" style="display: none;"></div>
            <div id="historiales-dialog" style="display: none;"></div>
            <div id="report-dialog" style="display: none;"></div>
        </div>
    </form>
</div>

<script type="text/javascript">
    if ($.browser.webkit) {
        $('select.smallInput').css('padding', '1px');
    }

    Date.prototype.toString = function () {return isNaN (this) ? 'NaN' : [this.getDate() > 9 ? this.getDate() : '0' + this.getDate(),  this.getMonth() > 8 ? this.getMonth() + 1 : '0' + (this.getMonth() + 1), this.getFullYear()].join('/')}
    var d = new Date().toString();
    $(".datepicker").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd/mm/yy",
        defaultDate: d,
        regional: 'es',
        minDate: new Date(1920, 0, 1),
        yearRange:'1920:c',
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
        dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
        dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
        monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
    });
    $(document).ready(function(){
        $( ".buttons-cargar" ).button( "option", "icons", {primary:'ui-icon-folder-open'} );
        $( ".buttons-guardar" ).button( "option", "icons", {primary:'ui-icon-disk'} );
        $( ".buttons-guardar-como" ).button( "option", "icons", {primary:'ui-icon-disk'} );
        $( ".buttons-consultar" ).button( "option", "icons", {primary:'ui-icon-gear'} );
        $( ".buttons-history" ).button( "option", "icons", {primary:'ui-icon-clock'} );
        clear_form_elements('#formProduccion');
        formatearFechas();
        actualizarCheckeds();
        $('.selectCheckbox').change(function(){
            if($(this).attr('checked')){
                $('#o_order').append('<option value="'+$(this).attr('name')+'">'+$('label[for="'+$(this).attr('id')+'"]').html()+'</option>');
                if($('#o_order').find('option[value="0"]').length > 0)
                    $('#o_order').find('option[value="0"]').remove();
                $('#o_group').append('<option value="'+$(this).attr('name')+'">'+$('label[for="'+$(this).attr('id')+'"]').html()+'</option>');
            }
            else{
                $('#o_order').find('option[value="'+$(this).attr('name')+'"]').remove();
                if($('#o_order option').size() == 0)
                    $('#o_order').append('<option value="0">Campo</option>');
                $('#o_group').find('option[value="'+$(this).attr('name')+'"]').remove();
            }
            ordenarSelect('#o_order');
            ordenarSelect('#o_group');
        });
    });
    function actualizarCheckeds(){
        $('input.selectCheckbox:checked').each(function(){
            if(!($('#o_order').find('option[value="'+$(this).attr('name')+'"]').length > 0))
                $('#o_order').append('<option value="'+$(this).attr('name')+'">'+$('label[for="'+$(this).attr('id')+'"]').html()+'</option>');
            if($('#o_order').find('option[value="0"]').length > 0)
                $('#o_order').find('option[value="0"]').remove();
            if(!($('#o_group').find('option[value="'+$(this).attr('name')+'"]').length > 0))
                $('#o_group').append('<option value="'+$(this).attr('name')+'">'+$('label[for="'+$(this).attr('id')+'"]').html()+'</option>');
        });
        ordenarSelect('#o_order');
        ordenarSelect('#o_group');
    }



    function exportarReporte(src){
        if(src == null){
            src = 'produccion';
        }
        else{
            src = 'history&src='+src;
        }
        $('#form_report').attr('action', 'exportar_reporte.php?objeto='+src+'&tipo=xls');
        $('#form_report').attr('target', '_blank');
        $('#form_report').submit();
        $('#form_report').attr('action', '#');
        $('#form_report').removeAttr('target');
    }

    function exportarReportePDF(src) {

        if (src == null) src = "produccion";
        else src = "history&src=" + src;

        $("#form_report").attr("action", "exportar_reporte.php?objeto=" + src + "&tipo=pdf");
        $("#form_report").attr("target", "_blank");
        $("#form_report").submit();
        $("#form_report").attr("action", "#");
        $("#form_report").removeAttr("target");
    }



    function ordenarSelect(id){
        var ops = $(id +" option");
        var slected = $(id+' option:selected').val();
        ops.sort(function (a,b) {
            return ( $(a).text().toUpperCase() > $(b).text().toUpperCase())
        });
        var html="";
        for(i=0;i<ops.length;i++)
        {
            html += "<option value='" + $(ops[i]).val() + "'>" + $(ops[i]).text() + "</option>";
        }
        $(id).html(html);
        if($(id).find('option[value="'+slected+'"]').length > 0)
            $(id).find('option[value="'+slected+'"]').attr('selected', 'selected');
        else
            $(id).val(0);
    }
    $("#nombre-dialog").dialog({
        modal: true,
        autoOpen: false,
        width: 370,
        height: 'auto',
        title: '<span class="ui-icon ui-icon-disk"></span> Guardar',
        resizable: false,
        resizeStop: function(event, ui) {
            $('#nombre-dialog').dialog('option', 'position', 'center');
        },
        buttons: {
            "Guardar": function() {
                $('[name="nombre_reporte"]').val($('#tmp_reporte').val());
                $('[name="id_reporte"]').val(0);
                guardarReporte();
                $( this ).dialog( "close" );
            },
            "Cancelar": function() {
                $( this ).dialog( "close" );
            }
        }
    });
    $("#nombrehist-dialog").dialog({
        modal: true,
        autoOpen: false,
        width: 370,
        height: 'auto',
        title: '<span class="ui-icon ui-icon-clock"></span> Guardar',
        resizable: false,
        resizeStop: function(event, ui) {
            $('#nombre-dialog').dialog('option', 'position', 'center');
        },
        buttons: {
            "Guardar": function() {
                var nom = $('[name="nombre_reporte"]').val();
                $('[name="nombre_reporte"]').val($('#tmp_hist').val());
                guardarReporte(1111111);
                $('[name="nombre_reporte"]').val(nom);
                $( this ).dialog( "close" );
            },
            "Cancelar": function() {
                $( this ).dialog( "close" );
            }
        }
    });
    $("#reportes-dialog").dialog({
        modal: true,
        autoOpen: false,
        width: 'auto',
        maxWidth: 320,
        height: 'auto',
        resizable: false,
        title: '<span class="ui-icon ui-icon-folder-open"></span> Cargar',
        resizeStop: function(event, ui) {
            $('#reportes-dialog').dialog('option', 'position', 'center');
        },
        open: function(){
            actualizarListadoReportes();
        }
    });
    $("#historiales-dialog").dialog({
        modal: true,
        autoOpen: false,
        width: 'auto',
        maxWidth: 320,
        height: 'auto',
        resizable: false,
        title: '<span class="ui-icon ui-icon-clock"></span> Cargar Historial',
        resizeStop: function(event, ui) {
            $('#historiales-dialog').dialog('option', 'position', 'center');
        },
        open: function(){
            actualizarListadoHistoriales();
        }
    });
    $("#produccion-dialog").dialog({
        modal: true,
        autoOpen: false,
        width: 980,
        //maxWidth: 800,
        height: 600,
        resizable: false,
        title: '<span class="ui-icon ui-icon-folder-open"></span> Reporte',
        resizeStop: function(event, ui) {
            $('#produccion-dialog').dialog('option', 'position', 'center');
        }
        //                            open: function(){
        //                                generarReporteProduccion();
        //                            }
    });
</script>

<style>
    input[disabled="disabled"], select[disabled="disabled"]{
        background: #CCC;
    }
</style>