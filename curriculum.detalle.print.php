<?php
if(isset($_GET['PDF'])){
    ob_start();
    require_once("pdf/dompdf_config.inc.php");
}
require_once("clases/framework-1.0/class.bd.php");
require_once("clases/phppaging/PHPPaging.lib.php");
require_once("includes/funciones.php");

if ($_GET["id_postulante"] != null && $_GET['id_postulante'] > 0) {

    $sSQL = "SELECT
                pe.PerApellido
                , pe.PerNombres
                , td.TipoDocNombre
                , pe.PerDocumento
                , pe.PerCuitCuil
                , s.nombre AS PerSexoF
                , DATE_FORMAT(pe.PerFechaNac, '%d-%m-%Y') AS PerFechaNacF
                , pe.PerNacionalidad
                , pe.PerECivil
                , pe.PerHijos
                , IF(pe.PerAcargo,'Si','No') as PerAcargoF
                , pe.PerPais
                , pe.PerProvincia
                , pe.PerLocalidad
                , z.ZonDescrip
                , pe.PerBarrio
                , pe.PerCodPos
                , pe.PerDomicilio
                , pe.PerTelefono
                , pe.PerTelMensaje
                , pe.PerCelular
                , pe.PerEmail
                , tdi.TDiDescrip
                , IF(pe.PerAceptaEve,'Si','No') as PerAceptaEveF
                , IF(pe.PerDispViaje,'Si','No') as PerDispViajeF
                , IF(pe.PerDispCiudad,'Si','No') as PerDispCiudadF
                , pe.PerPrefLaboral
                , pe.PerAspPuesto
                , re.descripcion AS Remuneracion
                , a1.AreNom AS AreNom1
                , a2.AreNom AS AreNom2
                , a3.AreNom AS AreNom3
                , IF(pe.PerIngles,'Si','No') AS PerInglesF
                , n1.nombre AS PerInglesHablaF
                , n2.nombre AS PerInglesEscribeF
                , pe.PerIdiomas
                , pe.PerOtrosConoc
            FROM
                persona pe
                LEFT JOIN tipodocumento td ON pe.PerTipoDoc = td.TipoDocNro
                LEFT JOIN _sexos s ON pe.PerSexo = s.idsexo
                LEFT JOIN zona z ON pe.ZonNro = z.ZonNro
                LEFT JOIN tipodisponibilidad tdi ON pe.PerDisponible = tdi.TDiNro
                LEFT JOIN remuneracion re ON pe.PerRemuneracion = re.idremuneracion
                LEFT JOIN area a1 ON pe.AreNro1 = a1.AreNro
                LEFT JOIN area a2 ON pe.AreNro2 = a2.AreNro
                LEFT JOIN area a3 ON pe.AreNro3 = a3.AreNro
                LEFT JOIN _nivel_idioma n1 ON pe.PerInglesHabla = n1.idnivel
                LEFT JOIN _nivel_idioma n2 ON pe.PerInglesEscribe = n2.idnivel
            WHERE
                pe.PerNro = " . $_GET["id_postulante"];

    //echo $sSQL;

    $db = new BD();
    $db->Conectar();
    $row = $db->Seleccionar($sSQL, TRUE);
} else
    $row = FALSE;

//print_r($row);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Gesti&oacute;n Consultores</title>
        <?php if(!isset($_GET['PDF'])): ?>
        <link rel="stylesheet" type="text/css" href="css/print.css" />
        <?php else: ?>
        <style>
            body {
                font-family: Arial, Helvetica, sans serif;
                margin: 0px;
                font-size: 14px;
            }
            h1 {
                font-size: 18px;
                margin: 0px;
            }
            h2 {
                font-size: 16px;
                margin: 0px;
            }
            h3 {
                font-size: 14px;
                margin: 0px;
            }
            p {
                margin: 0px;
            }
            hr {
                border: 0px none;
                page-break-after: always;
            }
        </style>
        <!--link rel="stylesheet" type="text/css" href="css/export.css" media="all" /-->
        <?php endif; ?>
    </head>
    <body <?php if(!isset($_GET['PDF'])): ?>onload="window.print(); window.close();"<?php endif; ?>>
        <div id="main-container">
            <h1><?php echo $row['PerApellido']; ?>, <?php echo $row['PerNombres']; ?>.</h1>
            <h2>Datos personales.</h2>
            <dl>
                <dt><h3>Apellido:</h3></dt>
                <dd><p><?php echo $row['PerApellido']; ?></p></dd>
                <dt><h3>Nombres:</h3></dt>
                <dd><p><?php echo $row['PerNombres']; ?></p></dd>
                <dt><h3>Documento:</h3></dt>
                <dd><p><?php echo $row['TipoDocNombre']; ?> <?php echo $row['PerDocumento']; ?></p></dd>
                <dt><h3>CUIT / CUIL:</h3></dt>
                <dd><p><?php echo $row['PerCuitCuil']; ?></p></dd>
                <dt><h3>Sexo:</h3></dt>
                <dd><p><?php echo $row['PerSexoF']; ?></p></dd>
                <dt><h3>Fecha de nacimiento:</h3></dt>
                <dd><p><?php echo $row['PerFechaNacF']; ?></p></dd>
                <dt><h3>Nacionalidad:</h3></dt>
                <dd><p><?php echo $row['PerNacionalidad']; ?></p></dd>
                <dt><h3>Estado Civil:</h3></dt>
                <dd><p><?php echo $row['PerECivil']; ?></p></dd>
                <dt><h3>Cantidad de hijos:</h3></dt>
                <dd><p><?php echo $row['PerHijos']; ?></p></dd>
                <dt><h3>Personas a Cargo:</h3></dt>
                <dd><p><?php echo $row['PerAcargoF']; ?></p></dd>
                <dt><h3>Pa&iacute;s:</h3></dt>
                <dd><p><?php echo $row['PerPais']; ?></p></dd>
                <dt><h3>Provincia:</h3></dt>
                <dd><p><?php echo $row['PerProvincia']; ?></p></dd>
                <dt><h3>Localidad:</h3></dt>
                <dd><p><?php echo $row['PerLocalidad']; ?></p></dd>
                <dt><h3>Zona:</h3></dt>
                <dd><p><?php echo $row['ZonDescrip']; ?></p></dd>
                <dt><h3>Barrio:</h3></dt>
                <dd><p><?php echo $row['PerBarrio']; ?></p></dd>
                <dt><h3>C&oacute;digo Postal:</h3></dt>
                <dd><p><?php echo $row['PerCodPos']; ?></p></dd>
                <dt><h3>Domicilio:</h3></dt>
                <dd><p><?php echo $row['PerDomicilio']; ?></p></dd>
                <dt><h3>Tel&eacute;fono:</h3></dt>
                <dd><p><?php echo $row['PerTelefono']; ?></p></dd>
                <dt><h3>Tel&eacute;fono para mensajes:</h3></dt>
                <dd><p><?php echo $row['PerTelMensaje']; ?></p></dd>
                <dt><h3>Tel&eacute;fono celular:</h3></dt>
                <dd><p><?php echo $row['PerCelular']; ?></p></dd>
                <dt><h3>E-mail:</h3></dt>
                <dd><p><?php echo $row['PerEmail']; ?></p></dd>
            </dl>
            <hr />
            <h1>Datos Laborales.</h1>
            <h2>Objetivos laborales.</h2>
            <dl>
                <dt><h3>Disponibilidad:</h3></dt>
                <dd><p><?php echo $row['TDiDescrip']; ?></p></dd>
                <dt><h3>Disponibilidad para viajes:</h3></dt>
                <dd><p><?php echo $row['PerDispViajeF']; ?></p></dd>
                <dt><h3>Puesto Eventuales:</h3></dt>
                <dd><p><?php echo $row['PerAceptaEveF']; ?></p></dd>
                <dt><h3>Relocaci&oacute;n:</h3></dt>
                <dd><p><?php echo $row['PerDispCiudadF']; ?></p></dd>
                <dt><h3>Preferencias Laborales:</h3></dt>
                <dd><p><?php echo $row['PerPrefLaboral']; ?></p></dd>
                <dt><h3>Aspiraciones:</h3></dt>
                <dd><p><?php echo $row['PerAspPuesto']; ?></p></dd>
                <dt><h3>Remuneraci&oacute;n:</h3></dt>
                <dd><p><?php echo $row['Remuneracion']; ?></p></dd>
            </dl>
            <h2>&Aacute;reas de inter&eacute;s.</h2>
            <dl>
                <dt><h3>&Aacute;rea 1:</h3></dt>
                <dd><p><?php echo $row['AreNom1']; ?></p></dd>
                <dt><h3>&Aacute;rea 2:</h3></dt>
                <dd><p><?php echo $row['AreNom2']; ?></p></dd>
                <dt><h3>&Aacute;rea 3:</h3></dt>
                <dd><p><?php echo $row['AreNom3']; ?></p></dd>
            </dl>
            <h2>Conocimientos.</h2>
            <dl>
                <dt><h3>Ingl&eacute;s:</h3></dt>
                <dd><p><?php echo $row['PerInglesF']; ?></p></dd>
                <dt><h3>Nivel de habla Ingl&eacute;s</h3></dt>
                <dd><p><?php echo $row['PerInglesHablaF']; ?></p></dd>
                <dt><h3>Nivel de escritura Ingl&eacute;s</h3></dt>
                <dd><p><?php echo $row['PerInglesEscribeF']; ?></p></dd>
                <dt><h3>Otros idiomas:</h3></dt>
                <dd><p><?php echo $row['PerIdiomas']; ?></p></dd>
                <dt><h3>Otros conocimientos:</h3></dt>
                <dd><p><?php echo $row['PerOtrosConoc']; ?></p></dd>
            </dl>
            <hr />
            <h1>Experiencias laborales.</h1>
            <?php
                $eSQL = "SELECT
                            dp.PueUltimaEmpresa
                            , a.AreNom
                            , p.PueNom
                            , dp.PueCargos
                            , dp.PueEmpRubro
                            , re.descripcion AS Remuneracion
                            , dp.PueDescPuesto
                            , DATE_FORMAT(dp.PueFecha, '%d-%m-%Y') AS PueFechaF
                            , DATE_FORMAT(dp.PueFechaBaja, '%d-%m-%Y') AS PueFechaBajaF
                            , dp.PueExpAnos
                            , dp.PueExpMeses
                            , dp.PueCausaBaja
                        FROM
                            datospuesto dp
                            LEFT JOIN area a ON dp.AreNro = a.AreNro
                            LEFT JOIN puesto p ON dp.PueNro = p.PueNro
                            LEFT JOIN remuneracion re ON dp.PueRemuneracion = re.idremuneracion
                        WHERE
                            dp.PerNro = " . $_GET["id_postulante"];
                $experiencias = $db->Ejecutar($eSQL);
                while($experiencia = mysql_fetch_assoc($experiencias)):
            ?>
            <h2><?php echo $experiencia['PueUltimaEmpresa']; ?></h2>
            <dl>
                <dt><h3>Area:</h3></dt>
                <dd><p><?php echo $experiencia['AreNom']; ?></p></dd>
                <dt><h3>Puesto:</h3></dt>
                <dd><p><?php echo $experiencia['PueNom']; ?></p></dd>
                <dt><h3>Cargo:</h3></dt>
                <dd><p><?php echo $experiencia['PueCargos']; ?></p></dd>
                <dt><h3>Rubro:</h3></dt>
                <dd><p><?php echo $experiencia['PueEmpRubro']; ?></p></dd>
                <dt><h3>Remuneraci&oacute;n:</h3></dt>
                <dd><p><?php echo $experiencia['Remuneracion']; ?></p></dd>
                <dt><h3>Tareas realizadas:</h3></dt>
                <dd><p><?php echo $experiencia['PueDescPuesto']; ?></p></dd>
                <dt><h3>Fecha alta:</h3></dt>
                <dd><p><?php echo $experiencia['PueFechaF']; ?></p></dd>
                <dt><h3>Fecha baja:</h3></dt>
                <dd><p><?php echo $experiencia['PueFechaBajaF']; ?></p></dd>
                <dt><h3>Antig&uuml;edad:</h3></dt>
                <dd><p><?php echo $experiencia['PueExpAnos']; ?> años, <?php echo $experiencia['PueExpMeses']; ?> meses</p></dd>
                <dt><h3>Causa baja:</h3></dt>
                <dd><p><?php echo $experiencia['PueCausaBaja']; ?></p></dd>
            </dl>
            <?php endwhile; ?>
            <hr />
            <h1>Educaci&oacute;n.</h1>
            <?php
                $edSQL = "SELECT
                                e.EtuInstitucion
                                , DATE_FORMAT(e.EtuIngreso, '%d-%m-%Y') AS EtuIngresoF
                                , DATE_FORMAT(e.EtuEgreso, '%d-%m-%Y') AS EtuEgresoF
                                , NEsDescrip
                                , eo.nombre AS Estado
                                , ae.nombre AS AreaEstudio
                                , e.EtuTitulo
                                , e.EtuCursadas
                                , e.EtuAprobadas
                                , e.EtuPromedio
                                , e.EtuAnoCursado
                                , e.EtuObservaciones
                            FROM
                                estudio e
                                LEFT JOIN nivelestudio ne ON e.NEsNro = ne.NEsNro
                                LEFT JOIN _estado_estudio eo ON e.EtuFinalizado = eo.idestado
                                LEFT JOIN area_estudio ae ON e.EtuAreaEstudio = ae.id_area_estudio
                            WHERE
                                e.PerNro = " . $_GET["id_postulante"];
                $educaciones = $db->Ejecutar($edSQL);
                while($educacion = mysql_fetch_assoc($educaciones)):
            ?>
            <h2><?php echo $educacion['EtuInstitucion'] ?></h2>
            <dl>
                <dt><h3>Ingreso:</h3></dt>
                <dd><p><?php echo $educacion['EtuIngresoF'] ?></p></dd>
                <dt><h3>Egreso:</h3></dt>
                <dd><p><?php echo $educacion['EtuEgresoF'] ?></p></dd>
                <dt><h3>Nivel estudios:</h3></dt>
                <dd><p><?php echo $educacion['NEsDescrip'] ?></p></dd>
                <dt><h3>Finalizado:</h3></dt>
                <dd><p><?php echo $educacion['Estado'] ?></p></dd>
                <dt><h3>&Aacute;rea estudio:</h3></dt>
                <dd><p><?php echo $educacion['AreaEstudio'] ?></p></dd>
                <dt><h3>T&iacute;tulo:</h3></dt>
                <dd><p><?php echo $educacion['EtuTitulo'] ?></p></dd>
                <dt><h3>Materias Cursadas:</h3></dt>
                <dd><p><?php echo $educacion['EtuCursadas'] ?></p></dd>
                <dt><h3>Aprobadas:</h3></dt>
                <dd><p><?php echo $educacion['EtuAprobadas'] ?></p></dd>
                <dt><h3>Promedio:</h3></dt>
                <dd><p><?php echo $educacion['EtuPromedio'] ?></p></dd>
                <dt><h3>Cursando:</h3></dt>
                <dd><p><?php echo $educacion['EtuAnoCursado'] ?></p></dd>
                <dt><h3>Notas:</h3></dt>
                <dd><p><?php echo $educacion['EtuObservaciones'] ?></p></dd>
            </dl>
            <?php endwhile; ?>
        </div>
    </body>
</html>
<?php $db->Desconectar(); ?>
<?php
if(isset($_GET['PDF'])){
    $html = ob_get_clean();
    ob_end_clean();
    
    $dompdf = new DOMPDF();
    $dompdf->load_html($html);
    //$dompdf->set_paper('a4', 'landscape');
    $dompdf->render();
    $dompdf->stream("curriculum.pdf");
}
?>
