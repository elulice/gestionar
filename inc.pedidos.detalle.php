<?php
$form_action = "abm.php?tabla=oferta";
if (is_array($row)) $form_action .= "&columna=OfeNro&idregistro=$id_oferta";
?>
<script type="text/javascript">
    $(document).ready(function() {

        $("#form-pedidos-detalle input[name='OfeFechAlta']").datepicker(dp_options);
        $("#form-pedidos-detalle input[name='OfeValidez']").datepicker(dp_options);

        var availableTags = <?php include("feed.pedidos.detalle.autocomplete.php");  ?>; 
        $(".labelsUsuaria").autocomplete({
                source: availableTags,
                change: function(event, ui){
                    $('#CliNro').val(ui.item.id);
                }
        });
        
        $('#AreNro').change(function(s){
            $.ajax({
                type: "POST",
                url: "feed.pedidos.detalle.puestos.php",
                data: "id_area=" + s.target.value,
                success: function(msg){
                    $('#PueNro').html(msg);
                }
            });
        });
    });
</script>
			<a href="" tabindex="1" style="position:fixed;">&nbsp;</a>
			<input type="hidden" id="id_oferta" name="id_oferta" value="<?php echo $id_oferta; ?>" />
			<form id="form-pedidos-detalle" action="<?php echo $form_action; ?>" method="post">
			   <div class="dlg_column_left" style="float:left;height:510px;">
			      <div style="width:360px;margin-top:4px;">
				 <div class="ddu_title"><span>Generales</span></div>
				 <div style="width:360px;height:28px;">
				    <div style="float:left;width:190px;">
				       <div class="ddu_campo">
					  <span>Fecha Alta:</span>
					  <input type="text" class="smallInput" name="OfeFechAlta" style="width:75px;text-align:center;" value="<?php echo $row["OfeFechAlta"]; ?>" />
				       </div>
				    </div>
				    <div style="float:left;width:170px;">
				       <div class="ddu_campo">
					  <span style="width:70px;margin-left:10px;">Vigencia:</span>
					  <input type="text" class="smallInput" name="OfeValidez" style="width:75px;text-align:center;" value="<?php echo $row["OfeValidez"]; ?>" />
				       </div>
				    </div>
				 </div>
				 <div style="width:360px;">
				    <div style="float:left;width:190px;">
				       <div class="ddu_campo">
					  <span style="margin-right:3px;">Visible:</span>
					  <input type="radio" name="OfeVisible" value="1" <?php echo $row["OfeVisible"] == 1 ? "checked=\"checked\"" : ""; ?> style="width:10px;float:left;" /><span style="float:left;width:10px;">Si</span>
					  <input type="radio" name="OfeVisible" value="0" <?php echo (!is_array($row) OR $row["OfeVisible"] == 0) ? "checked=\"checked\"" : ""; ?> style="float:left;width:13px;margin-left:10px;" /><span style="width:15px;margin-left:5px;">No</span>
				       </div>
				    </div>
				    <div style="float:left;width:170px;">
				       <div class="ddu_campo">
					  <span style="width:72px;margin-left:10px;">Destacada:</span>
					  <input type="radio" name="OfeDestacada" value="1" <?php echo $row["OfeDestacada"] == 1 ? "checked=\"checked\"" : ""; ?> style="width:10px;float:left;margin-left:4px;" /><span style="float:left;width:10px;">Si</span>
					  <input type="radio" name="OfeDestacada" value="0" <?php echo (!is_array($row) OR $row["OfeDestacada"] == 0) ? "checked=\"checked\"" : ""; ?> style="float:left;width:10px;margin-left:10px;" /><span style="width:15px;margin-left:5px;">No</span>
				       </div>
				    </div>
				 </div>
				 <div style="width:340px;">
				    <div style="float:left;width:170px;">
				       <div class="ddu_campo_select">
					  <span>Situación:</span>
					  <select class="smallInput" style="width:88px;" name="HIDDEN_ofeEstado">
					  <?php
   $query = "SELECT nombre, nombre FROM _ofertas_estados";
   echo GenerarOptions($query, $row["EOfeValor"]);
					  ?>
					  </select>
				       </div>
				    </div>
				    <div style="float:left;width:170px;">
				       <div class="ddu_campo" style="width:190px;">
					  <span style="width:70px;margin-left:31px;">Factura:</span>
					  <input type="text" class="smallInput" name="OfeFactura" value="<?php echo $row["OfeFactura"]; ?>" style="width:75px;text-align:center;" />
				       </div>
				    </div>
				 </div>
				 <div class="ddu_title" style="margin-top:60px;"><span>Datos de la Usuaria</span></div>
				 <div class="ddu_campo" style="margin-top:15px;">
				    <span>Empresa:</span>
                                    <input type="text" class="smallInput labelsUsuaria" id="searchBox" name="HIDDEN_cliente" value="<?php echo $row['nombrecliente'] ?>" />
                                    <input type="hidden" value="<?php echo $row["CliNro"]; ?>" name="HIDDEN_CliNro" id="CliNro" />
				 </div>
				 <div class="ddu_campo">
				    <span>Solicitante:</span>
				    <input type="text" name="OFePedidoPor" value="<?php echo $row["OFePedidoPor"]; ?>" class="smallInput" />
				 </div>
				 <div class="ddu_campo">
				    <span>Dirección:</span>
				    <input type="text" name="OfeDireccion" class="smallInput" value="<?php echo $row["OfeDireccion"]; ?>" />
				 </div>
				 <div class="ddu_campo">
				    <span>Email:</span>
				    <input type="text" name="OfeEMail" class="smallInput" value="<?php echo $row["OfeEMail"]; ?>" />
				 </div>
				 <div class="ddu_campo">
				    <span>URL:</span>
				    <input type="text" name="OfeURL" class="smallInput" value="<?php echo $row["OfeURL"]; ?>" />
				 </div>
				 <div class="ddu_title" style="margin-top:6px;"><span>Datos de Publicación</span></div>
				 <div style="width:360px;height:28px;">
				    <div style="float:left;width:190px;">
				       <div class="ddu_campo">
					  <span>Nº Referencia:</span>
					  <input type="text" class="smallInput" name="OfeReferencia" value="<?php echo $row["OfeReferencia"]; ?>" style="width:75px;text-align:center;" />
				       </div>
				    </div>
				    <div style="float:left;width:170px;">
				       <div class="ddu_campo">
					  <span style="width:70px;margin-left:10px;">Vacantes:</span>
					  <input type="text" class="smallInput" name="OfeCantPost" value="<?php echo $row["OfeCantPost"]; ?>" style="width:75px;text-align:center;" />
				       </div>
				    </div>
				 </div>
				 <div class="ddu_campo_select">
				    <span>Área Interés:</span>
				    <select name="AreNro" class="smallInput" id="AreNro">
				    <?php
   $query = "SELECT AreNro, AreNom FROM area ORDER BY AreNom ASC";
   echo GenerarOptions($query, $row["AreNro"]);
				    ?>
				    </select>
				 </div>
				 <div class="ddu_campo_select">
				    <span>Puesto:</span>
				    <select name="PueNro" class="smallInput" id="PueNro">
				    <?php
   $query = "SELECT PueNro, PueNom FROM puesto WHERE AreNro = ".$row["AreNro"]." ORDER BY PueNom ASC";
   echo GenerarOptions($query, $row["PueNro"]);
				    ?>
				    </select>
				 </div>
				 <div class="ddu_campo">
				    <span>Título:</span>
				    <input type="text" name="OfeTitulo" class="smallInput" value="<?php echo $row["OfeTitulo"]; ?>" />
				 </div>
				 <div class="ddu_campo_textarea">
				    <span>Descripción:</span>
				    <textarea class="smallInput" name="OfeOferta" style="height:62px;"><?php echo $row["OfeOferta"]; ?></textarea>
				 </div>
				 <div class="ddu_campo_select">
				    <span>Zona:</span>
				    <select class="smallInput" name="ZonNro">
				    <?php
   $query = "SELECT ZonNro, ZonDescrip FROM zona ORDER BY ZonDescrip ASC";
   echo GenerarOptions($query, $row["ZonNro"]);
				    ?>
				    </select>
				 </div>
			      </div>
			   </div>
			   <div class="dlg_column_right" style="float:right;margin-left:20px;">
			      <div style="width:360px;margin-top:4px;">
				 <div class="ddu_title"><span>Generales</span></div>
				 <div class="ddu_campo_select">
				    <span>Tipo Búsqueda:</span>
				    <select class="smallInput" name="TPeNro">
				    <?php
   $query = "SELECT idtipo, nombre FROM _tipo_pedido";
   echo GenerarOptions($query, $row["TPeNro"]);
				    ?>
				    </select>
				 </div>
				 <div style="width:360px;height:28px;margin-top:3px;">
				    <div style="float:left;width:190px;">
				       <div class="ddu_campo">
					  <span>Edad Desde:</span>
					  <input type="text" class="smallInput" name="OfeDeEdad" value="<?php echo $row["OfeDeEdad"]; ?>" style="width:75px;text-align:center;" />
				       </div>
				    </div>
				    <div style="float:left;width:170px;">
				       <div class="ddu_campo">
					  <span style="width:70px;margin-left:10px;">Hasta:</span>
					  <input type="text" class="smallInput" name="OfeHtaEdad" value="<?php echo $row["OfeHtaEdad"]; ?>" style="width:75px;text-align:center;" />
				       </div>
				    </div>
				 </div>
				 <div class="ddu_campo_select">
				    <span>Sexo:</span>
				    <select class="smallInput" name="OfeSexo">
				    <?php
   $query = "SELECT idsexo, nombre FROM _sexos";
   echo GenerarOptions($query, $row["OfeSexo"]);
				    ?>
				    </select>
				 </div>
				 <div class="ddu_campo_select">
				    <span>Nivel Educación:</span>
				    <select class="smallInput" name="OfeEducacion">
				    <?php
   $query = "SELECT idnivel, nombre FROM _nivel_educacion";
   echo GenerarOptions($query, $row["OfeEducacion"]);
				    ?>
				    </select>
				 </div>
				 <div class="ddu_campo_select">
				    <span>Provincia:</span>
				    <select class="smallInput" name="OfeProvincia">
				    <?php
   $query = "SELECT PrvNom, PrvNom FROM provincia ORDER BY PrvNom ASC";
   echo GenerarOptions($query, $row["OfeProvincia"]);
				    ?>
				    </select>
				 </div>
				 <div class="ddu_campo" style="margin-top:6px;">
				    <span>Localidad:</span>
				    <input type="text" class="smallInput" name="OfeLocalidad" value="<?php echo $row["OfeLocalidad"]; ?>" />
				 </div>
				 <div class="ddu_campo_select">
				    <span>Habla Inglés:</span>
				    <select class="smallInput" name="OfeIngles">
				    <?php
   $query = "SELECT idnivel, nombre FROM _nivel_idioma";
   echo GenerarOptions($query, $row["OfeIngles"]);
				    ?>
				    </select>
				 </div>
				 <div class="ddu_campo" style="margin-top:6px;">
				    <span>Exp. Previa:</span>
				    <input type="text" name="OfeExpAnios" class="smallInput" value="<?php echo $row["OfeExpAnios"]; ?>" style="width:170px;float:left;"/>
				    <span style="float:right;width:60px;">A&ntilde;os</span>
				 </div>
				 <div class="ddu_campo" style="margin-top:6px;">
				    <span>Remuneración:</span>
				    <input type="text" name="OfeRemunerac" class="smallInput" value="<?php echo $row["OfeRemunerac"]; ?>" style="width:170px;float:left;" />
				    <span style="float:right;width:60px;">USD</span>
				 </div>
				 <div class="ddu_campo_textarea">
				    <span>Tareas:</span>
				    <textarea class="smallInput" name="OfeDescTar" style="height:64px;"><?php echo $row["OfeDescTar"]; ?></textarea>
				 </div>
				 <div class="ddu_campo">
				    <span>Clasificación:</span>
				    <input type="text" name="OfeClasificacion" class="smallInput" value="<?php echo $row["OfeClasificacion"]; ?>" />
				 </div>
				 <div class="ddu_campo_textarea">
				    <span>Observaciones:</span>
				    <textarea class="smallInput" name="OfeDescSel" style="height:105px;"><?php echo $row["OfeDescSel"]; ?></textarea>
				 </div>
				 <div class="ddu_campo_select">
				    <span>Sucursal:</span>
				    <select class="smallInput" name="NodNro">
				    <?php
   $query = "SELECT UniNro, UniNombre FROM unidadorg";
   echo GenerarOptions($query, $row["NodNro"]);
				    ?>
				    </select>
				 </div>
			      </div>
			   </div>
			</form>
