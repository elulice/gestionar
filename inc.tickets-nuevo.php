<?php
if ($min_id == '0') {
    header("Location: index.php");
}
?>

<style>
    ul.files { padding-top: 0px; padding-left: 3px; margin-bottom: 0; }
    ul.files li { display: inline-block; padding-right: 10px; width: 250px; }
    ul.files li span.icon { display: inline-block; width: 16px; height: 16px; background: url("images/icons/link.png") 0 3px; }
</style>

<script type="text/javascript" >
    $(document).ready(function(){
        //        $('#p_fecha').datepicker();
        $.datepicker.setDefaults({ dateFormat: 'dd/mm/yy' });
        
        //        checkIfAnswerWasSentToClient('<?php // echo $getID;                                                                 ?>');
        getFormAdjunto(<?php echo ($getID); ?>);
        
        var availableTags = <?php include("feed.tickets.usuarias.autocomplete.php"); ?>;
        $(".labelsUsuaria").autocomplete({
            source: availableTags
        });

        var availableTagsNew = <?php include("feed.new.ticket.usuarias.php"); ?>;
        $("#p_cliente_new").autocomplete({
            source: availableTagsNew,
            select: function(event, ui) {
                updateSucursales(ui.item.id);
                updateEmail(ui.item.id);
            }
        });
        $('#p_sucursal_new').change(function(){
            var select = $(this);
            
            $.ajax({
                type: 'get',
                url: 'feed.usuarias.php?objeto=sector_por_sucursal&id_sucursal='+select.val(),
                beforeSend: function(){
                    $('#load-sector').css('display', 'block');
                },
                success: function(data){
                    $('#load-sector').css('display', 'none');
                    $('#sector_select').html(data);
                    //                    alert(data);
                    $('#p_responsable_new').html('');
                }
            });
        });
        
        $('#sector_select').change(function(){
            var select = $(this);
            
            $.ajax({
                type: 'get',
                url: 'feed.usuarias.php?objeto=responsables_por_sector&id_sector=' + select.val() + '&id_sucursal=' + $('#p_sucursal_new').val(),
                beforeSend: function(){
                    $('#load-responsable').css('display', 'block');
                },
                success: function(data){
                    $('#load-responsable').css('display', 'none');
                    $('#p_responsable_new').html(data);
                    
                    var responsable = '';
                    $('#p_responsable_new option').each(function(){
                        $(this).removeAttr("selected");
                        if ($(this).html().indexOf('Responsable') != -1) {
                            $(this).css('font-weight', 'bold');
                            $(this).attr('selected', 'selected');
                            responsable = $(this);
                            $(this).remove();
                        }
                    });
                    
                    if (responsable) {
                        $('#p_responsable_new').prepend(responsable);
                    }
                    fill_mail_responsable($('#p_responsable_new').val())
                }
            });
        });
        
    });
    function updateSucursales(empresaId){
        if(empresaId != 0){
            $.ajax({
                type: 'get',
                url: 'feed.usuarias.php?objeto=sucursales&id_cliente='+empresaId,
                beforeSend: function(){
                    $('#select_planes_polizas, #select_planes_polizas_new').find('img').css('display', 'block');
                    $('#select_planes_polizas select, #select_planes_polizas_new select').attr('disabled', 'disabled');
                    $('#select_planes_polizas select, #select_planes_polizas_new select').attr('readonly', 'readonly');
                    $('#select_planes_polizas select, #select_planes_polizas_new select').css('background-color', '#DDD');
                },
                success: function(data){
                    $('#select_planes_polizas, #select_planes_polizas_new').find('img').css('display', 'none');
                    $('#select_planes_polizas select, #select_planes_polizas_new select').removeAttr('disabled');
                    $('#select_planes_polizas select, #select_planes_polizas_new select').removeAttr('readonly');
                    $('#select_planes_polizas select, #select_planes_polizas_new select').css('background-color', '#FFF');
                    $('#p_sucursal_new').html(data);
                    $('#p_sucursal_new').change();
                }
            });
        }
    }
    
    function updateEmail(empresaId){
        solicitante = $("#p_origen_new").val();
        if(empresaId != 0 && solicitante == 2){
            $.ajax({
                type: 'get',
                url: 'feed.usuarias.php?objeto=email&id_cliente='+empresaId,
                success: function(data){
                    $('#p_email_cliente_new').val(data);
                }
            });
        }
    }

    function makeSuggest(){
        var availableTagsEdit = <?php include("feed.new.ticket.usuarias.php"); ?>;
        $("#p_cliente_edit").autocomplete({
            source: availableTagsEdit
        });

    }

    var doIt = '1';
    $('#p_cliente_edit').live('focus',function(){
        if(doIt == '1'){
            makeSuggest();
            doIt = '0';
        }
    });
    
    

</script>
<?
if (!empty($getID)) {
    $queryB = "SELECT all_tickets.*, cliente.CliRsocial as razon_social, origen_tickets.nombre_origen as donde_origen, unidadorg.UniNombre as donde_sucursal, unidadorg.UniMail as mail_sucursal, miembroempresa.* , estado_tickets.nombre as state_tickets, area.AreNom as sector, motivos.nombre_motivo, tipo_error.nombre_error FROM all_tickets
                                LEFT JOIN cliente ON all_tickets.cliente_id = cliente.CliNro
                                LEFT JOIN origen_tickets ON all_tickets.origen_id = origen_tickets.id
                                LEFT JOIN unidadorg ON all_tickets.sucursal_id = unidadorg.UniNro
                                LEFT JOIN miembroempresa ON all_tickets.creado_por_id = miembroempresa.MEmpNro
                                LEFT JOIN area ON all_tickets.sector_id = area.AreNro
                                LEFT JOIN motivos ON all_tickets.motivo_id = motivos.id 
                                LEFT JOIN tipo_error ON all_tickets.tipo_error_id = tipo_error.iderror  
                                LEFT JOIN estado_tickets ON all_tickets.estado_id = estado_tickets.id WHERE all_tickets.id = '$getID' AND all_tickets.visible = 'S'";

    $cBD = new BD();
    $oResultado = $cBD->Seleccionar($queryB);
    $aRegistro = $cBD->RetornarFila($oResultado);
    $tkID = $aRegistro['id'];
    $oID = $aRegistro['origen_id'];
    $sID = $aRegistro['sucursal_id'];
//    echo $sID . " " . $_SESSION["empresa_id"];
    $rId = $aRegistro['responsable_id'];
    $eID = $aRegistro['estado_id'];
    $cID = $aRegistro['cliente_id'];
    $hora = $aRegistro['hora'];
    $email_cliente = $aRegistro['email_cliente'];
    $email_send = $aRegistro['email_send'];
    $email_Cc = $aRegistro['email_cc'];
//        $tipoError = $aRegistro['tipo_error'];
    $sMail = $aRegistro['mail_sucursal'];
    $allTkNombre = html_entity_decode($aRegistro['tickets_nombre']);
    $allTkContent = html_entity_decode($aRegistro['ticket_contenido']);
    $fechaAllTk = $aRegistro['fecha'];
    $fechaAllTk = str_replace('-', '/', $fechaAllTk);
    $fechaAllTk = explode('/', $fechaAllTk);
    $fechaAllTk = $fechaAllTk[2] . '/' . $fechaAllTk[1] . '/' . $fechaAllTk[0];
    $edited = $aRegistro['editado'];
    $responsed = $aRegistro['responsed'];
    $response_edited = $aRegistro['response_edited'];
    $response_stc = $aRegistro['response_sended_to_client'];
    $sector = $aRegistro["sector"];
    $sector_id = $aRegistro["sector_id"];
    $motivo = $aRegistro["nombre_motivo"];
    $motivo_id = $aRegistro["motivo_id"];
    $tipoError = $aRegistro["nombre_error"];
    $tipo_error_id = $aRegistro["tipo_error_id"];
    $can_response = $aRegistro["can_response"];
    ?>
    <div style = "width:830px; display:inline; float:left; height:430px; margin-left:20px; overflow:hidden;margin-top: -25px;">
        <div style = "width:240px; margin-right:30px; display:block; float:left; margin-top:38px;">
            <div class = "form-administrar" style = "margin-top:0px;"><span class = "form-label"><img src="images/icons/pixel.png" class="icons_new_tickets int_origen"/>Nro Ticket:</span>
                <input type = "text" name = "nombres" id = "nro_ticket" class = "form-contacto-text smallInput disabledNew" disabled="disabled" value="<?php echo zerofill($getID, 5) ?>" style = "width:150px; float:right;"/>
            </div>
            <!--fecha-->
            <div class = "form-administrar" style = "margin-top:0px;"><span class = "form-label"><img src="images/icons/pixel.png" class="icons_new_tickets int_fecha"/>Fecha:</span>
                <input type = "text" name = "apellido" disabled = "disabled" id = "p_fecha" class = "form-contacto-text smallInput disabledNew" style = "width:150px; float:right;" value="<?php echo $fechaAllTk ?>"/>
            </div>
            <!--NOMBRE-->
            <div class = "form-administrar" style = "margin-top:0px;"><span class = "form-label"><img src="images/icons/pixel.png" class="icons_new_tickets int_nombres"/>Nombres:</span>
                <input type = "text" name = "nombres" disabled = "disabled" id = "p_nombres" class = "form-contacto-text smallInput disabledNew" style = "width:150px; float:right;" value="<?php echo html_entity_decode($allTkNombre) ?>">
            </div>
            <div class = "form-administrar" style = "margin-top:0px;"><span class = "form-label"><img src="images/icons/pixel.png" class="icons_new_tickets int_mail_cliente"/>Email Solicit:</span>
                <input type = "text" name = "nombres" disabled = "disabled" id = "p_email_cliente" class = "form-contacto-text smallInput disabledNew" style = "width:150px; float:right;" value="<?php echo $email_cliente ?>">
            </div>
            <!--cliente-->
            <div class = "form-administrar" style = "margin-top:0px;"><span class = "form-label"><img src="images/icons/pixel.png" class="icons_new_tickets int_clientes"/>Usuaria:</span>
                <select name = "id_compania" disabled = "disabled" id = "p_cliente" size = "1" class = "form-contacto-text smallInput disabledNew" style = "width:157px; float:right;" >
                    <?php
                    $query = "SELECT CliNro, CliRsocial FROM cliente ORDER BY CliRsocial";
                    echo GenerarOptions($query, $cID, TRUE, DEFSELECT);
                    ?>
                </select>
            </div>
            <!-- origen -->
            <div class="form-administrar" style="margin-top:0px;"><span class="form-label"><img src="images/icons/pixel.png" class="icons_new_tickets int_origen"/>Origen:</span>
                <select name="id_compania" disabled = "disabled" id="p_origen" size="1" class="form-contacto-text smallInput disabledNew" style="width:157px; float:right;">
                    <?php
                    $query = "SELECT id, nombre_origen FROM origen_tickets
                            ORDER BY id ASC";
                    echo html_entity_decode(GenerarOptions($query, $oID, TRUE, DEFSELECT));
                    ?>
                </select>
            </div>
            <!-- sucursal -->
            <div class="form-administrar" style="margin-top:0px;"><span class="form-label"><img src="images/icons/pixel.png" class="icons_new_tickets int_sucursal"/>Sucursal:</span>
                <div id="select_planes_polizas">
                    <select name="id_plan" disabled = "disabled" id="p_sucursal" size="1" class="form-contacto-text smallInput disabledNew" style="width:157px; float:right;">
                        <?php
                        $query = "SELECT UniNro, UniNombre FROM unidadorg";
                        echo GenerarOptions($query, $sID, TRUE, DEFSELECT);
                        ?>
                    </select>
                </div>
            </div>
            <!-- RESPONSABLE -->
            <div class="form-administrar" style="margin-top:0px;">
                <span class="form-label"><img src="images/icons/pixel.png" class="icons_new_tickets int_responsable"/>Responsable:</span>
                <div id="select_planes_polizas">
                    <select name="id_plan" disabled="disabled" id="p_responsable" size="1" class="form-contacto-text smallInput disabledNew" style="width:157px; float:right;">
                        <?php
                        $query = "SELECT PerNro, CONCAT(MEmpApellido, ' ', MEmpNombres) FROM miembroempresa ORDER BY MEmpApellido";
                        echo GenerarOptions($query, $rId, TRUE, DEFSELECT);
                        ?>
                    </select>
                </div>
            </div>
            <!-- SECTOR -->
            <div class="form-administrar" style="margin-top:0px;  width: 264px;">
                <span class="form-label">
                    <img src="images/icons/branch.png" class="icons_new_tickets int_responsable"/>Sector:
                </span>
                <div id="select_sector">
                    <div style="float: right; width: 24px; height: 24px;">
                        <img src="images/ajax-loader.gif" style="float: right; margin: 4px;display: none;" />
                    </div>
                    <select name="id_plan" id="sector_select_2" size="1" class="form-contacto-text smallInput disabledNew" readonly="readonly" style="width:157px; float:right;">
                        <?php
                        $query = "SELECT AreNro, AreNom FROM area";
                        echo html_entity_decode(GenerarOptions($query, $sector_id, TRUE, DEFSELECT));
                        ?>
                    </select>
                </div>
            </div>
            <!-- ESTADO  -->
            <div class="form-administrar" style="margin-top:0px;">
                <span class="form-label"><img src="images/icons/pixel.png" class="icons_new_tickets int_estado"/>Estado:</span>
                <select name="id_plan" disabled = "disabled" id="p_estado" size="1" class="form-contacto-text smallInput disabledNew" style="width:157px; float:right;">
                    <?php
                    $query = "SELECT id, nombre FROM estado_tickets";
                    echo GenerarOptions(html_entity_decode($query), $eID, TRUE, DEFSELECT);
                    ?>
                </select>
            </div>
            <!-- MOTIVO -->
            <div id="agente-datos">
                <div class="form-administrar" style="margin-top:0px;">
                    <span class="form-label"><img src="images/icons/pixel.png" class="icons_new_tickets int_reason" />Motivo:</span>
                    <select id="p_motivo" size="1" class="form-contacto-text smallInput disabledNew" readonly="readonly" style="width:157px; float:right;">
                        <?php
                        $query = "SELECT id, nombre_motivo FROM motivos";
                        echo GenerarOptions($query, $motivo_id, TRUE, DEFSELECT);
                        ?>
                    </select>
                </div>
            </div>
            <!-- TIPO ERROR -->
            <div class="form-administrar" style="margin-top:0px;">
                <span class="form-label">
                    <img src="images/icons/pixel.png" class="icons_new_tickets int_error"/>Tipo Error:
                </span>
                <select id="p_tipo_error" size="1" class="form-contacto-text smallInput disabledNew" readonly="readonly" style="width:157px; float:right;">
                    <?php
                    $query = "SELECT iderror, nombre_error FROM tipo_error";
                    echo GenerarOptions($query, $tipo_error_id, TRUE, DEFSELECT);
                    ?>
                </select>   
            </div>
            <div class = "form-administrar" style = "margin-top:0px;"><span class = "form-label"><img src="images/icons/pixel.png" class="icons_new_tickets int_mail"/>CC:</span>
                <input type = "text" name = "nombres" disabled = "disabled" id = "p_email_send" class = "form-contacto-text smallInput disabledNew" style = "width:150px; float:right;" value="<?php echo $email_send ?>">
            </div>
            <div class = "form-administrar" style = "margin-top:0px;"><span class = "form-label"><img src="images/icons/pixel.png" class="icons_new_tickets int_mail"/>CC2:</span>
                <input type = "text" name = "nombres" disabled = "disabled" id = "p_email_cc" class = "form-contacto-text smallInput disabledNew" style = "width:150px; float:right;" value="<?php echo $email_Cc ?>">
            </div>
        </div>
        <!-- DETALLES Y ADJUNTOS -->
        <div id="campo-reclamo-total">
            <!-- DETALLES  -->

            <div id = "campo-reclamo" style = "overflow-y: scroll;overflow-x: hidden;width:540px;max-width: 540px;margin-top: -5px; ">
                <?
                if ($response_edited == "S") {
                    $queryR = "SELECT * FROM respuesta_tickets_edit WHERE ticket_id = '$tkID' ORDER BY id DESC";
                    $cBD = new BD();
                    $eResultado = $cBD->Seleccionar($queryR);
                    while ($rRegistro = $cBD->RetornarFila($eResultado)) {
                        $response_content = $rRegistro['answer_edited'];
                        $response_content_edited = $response_content;

                        $responsed_by = $rRegistro['answer_edited_by'];
                        $resp_edit_date = $rRegistro['respuesta_edit_fecha'];
                        $resp_edit_hour = $rRegistro['respuesta_edit_hora'];
                        $resp_edit_date = str_replace('-', '/', $resp_edit_date);
                        $resp_edit_date = explode('/', $resp_edit_date);
                        $resp_edit_date = $resp_edit_date[2] . '/' . $resp_edit_date[1] . '/' . $resp_edit_date[0];
                        ?>
                        <div id="campo-dialogo" style="cursor: pointer;margin-bottom: 5px;background: #79a2ff !important;height: 110px;">
                            <div id="fecha-hora">
                                <div id="fecha"><img src="images/Usermini.png"/><p style="margin-bottom:3px;">Editado Por: <?php echo $responsed_by ?><p style="margin-bottom:3px;">Fecha: <?php echo $resp_edit_date ?></p><p style="margin-bottom:3px;">Hora: <?php echo $resp_edit_hour ?></p><p></p></div>
                            </div>
                            <div class="texto-cuadro-reclamo">
                                <textarea name="detalles" class="form-contacto-text smallInput textarea_no_resize" disabled="disabled" style="cursor: pointer;width:385px; height: 82px; margin-top: 6px; float:right; margin-right:6px;"><?php echo $response_content ?></textarea>
                            </div>
                        </div>
                        <?
                    }
                }
                if ($responsed == "S") {
                    $queryR = "SELECT * FROM respuesta_tickets WHERE ticket_id = '$tkID'";
                    $cBD = new BD();
                    $eResultado = $cBD->Seleccionar($queryR);
                    while ($rRegistro = $cBD->RetornarFila($eResultado)) {
                        $response_content = $rRegistro['content_response'];
                        $responsed_by = $rRegistro['responsed_by'];
                        $resp_date = $rRegistro['respuesta_fecha'];
                        $resp_hour = $rRegistro['respuesta_hora'];
                        $resp_date = str_replace('-', '/', $resp_date);
                        $resp_date = explode('/', $resp_date);
                        $resp_date = $resp_date[2] . '/' . $resp_date[1] . '/' . $resp_date[0];
                        ?>
                        <div id="campo-dialogo" style="cursor: pointer;margin-bottom: 5px;background: #79a2ff !important;height: 110px;">
                            <div id="fecha-hora">
                                <div id="fecha"><img src="images/Usermini.png"/><p style="margin-bottom:3px;">Respondido Por: <?php echo $responsed_by ?></p><p style="margin-bottom:3px;">Fecha: <?php echo $resp_date ?></p><p style="margin-bottom:3px;">Hora: <?php echo $resp_hour ?></p><p></p></div>
                            </div>
                            <div class="texto-cuadro-reclamo">
                                <textarea name="detalles" class="form-contacto-text smallInput textarea_no_resize" disabled="disabled" style="cursor: pointer;width:385px; height: 82px; margin-top: 6px; float:right; margin-right:6px;"><?php echo $response_content ?></textarea>
                            </div>
                        </div>
                        <?
                    }
                }
                if ($edited == "S") {
                    $queryE = "SELECT * FROM tickets_edit WHERE ticket_id = '$tkID' AND id = (SELECT MAX(id) FROM tickets_edit) ORDER BY id DESC LIMIT 1";
                    $cBD = new BD();
                    $eResultado = $cBD->Seleccionar($queryE);
                    while ($zRegistro = $cBD->RetornarFila($eResultado)) {
                        $edited_content = $zRegistro['edited_content'];
                        $edited_by = $zRegistro['edited_by'];
                        $edited_date = $zRegistro['ticket_edit_fecha'];
                        $edited_date = str_replace('-', '/', $edited_date);
                        $edited_date = explode('/', $edited_date);
                        $edited_date = $edited_date[2] . '/' . $edited_date[1] . '/' . $edited_date[0];
                        $edited_hour = $zRegistro['ticket_edit_hora'];
                        $tk_id = $zRegistro['ticket_id'];
                        $id = $zRegistro['id'];
                        ?>
                        <div id="<?php echo $tk_id ?>">
                            <div id="campo-dialogo" style="margin-bottom: 5px;cursor: pointer;height: 110px;" >
                                <div id="fecha-hora">
                                    <div id="fecha"><img src="images/Usermini.png"/><p style="margin-bottom:3px;">Editado Por: <?php echo $edited_by ?></p><p style="margin-bottom:3px;">Fecha: <?php echo $edited_date ?></p><p style="margin-bottom:3px;">Hora: <?php echo $edited_hour ?></p><p></p></div>
                                </div>
                                <div class="texto-cuadro-reclamo">
                                    <textarea name="detalles" class="form-contacto-text smallInput textarea_no_resize" disabled="disabled" style="cursor: pointer;width:385px; height: 82px; margin-top: 6px; float:right; margin-right:6px;"><?php echo $edited_content ?></textarea>
                                </div>
                            </div>
                        </div>
                        <?
                    }
                } else {
                    ?>
                    <div id="campo-dialogo" style="cursor: pointer;margin-bottom: 5px;height: 110px;">
                        <div id="fecha-hora">
                            <div id="fecha"><img src="images/Usermini.png"/><p style="margin-bottom:3px;"><?php echo $allTkNombre ?></p><p style="margin-bottom:3px;">Fecha: <?php echo $fechaAllTk ?></p><p style="margin-bottom:3px;">Hora: <?php echo $hora ?></p><p></p></div>
                        </div>
                        <div class="texto-cuadro-reclamo">
                            <textarea name="detalles" class="form-contacto-text smallInput textarea_no_resize" disabled="disabled" style="cursor: pointer; width:385px; height: 82px; margin-top: 6px; float:right; margin-right:6px;"><?php echo $allTkContent ?></textarea>
                        </div>
                    </div>
                <? } ?>
            </div>
        </div>

        <div class="form-administrar" style="margin-top:10px;">
            <div style="background: #EEEEE0 !important;border: 1px solid #999 !important; min-height: 50px;padding: 5px;width: 528px;margin-left: 20px;margin-top: -12px;">
                <span>
                    <a href="javascript: void(0);" onclick="$('#form-ticket-adjuntos-list' ).dialog('open');" style="margin-left: 3px;">Archivos adjuntos:</a>
                </span>
                <span style="float:right;">
                    <a href="javascript:void(0);" onclick="$('#form-ticket-adjuntos').dialog('open');" class="edit_inline" style="margin-left:10px;">Agregar </a>
                </span>

                <ul class="files">
                    <?php $sql = "SELECT id, nombre, archivo, extension FROM archivos_ticket WHERE ticket_id = " . $_GET['id'] ?>
                    <?php $db = new BD() ?>
                    <?php $result = $db->Seleccionar($sql) ?>
                    <?php while ($row = $db->RetornarFila($result)): ?>
                        <li><span class="icon icon-<?php echo $row['extension'] ?>"></span> <a href="download.php?f=upload%2ftickets%2f<?php echo $row['archivo'] ?>"><?php echo $row['nombre'] ?></a></li>
                    <?php endwhile; ?>
                </ul>
            </div>

            <div id="form-ticket-adjuntos-list" style="display:none;">
                <table width="340" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet" style="font-size:9px;">
                    <thead>
                        <tr class="portlet-header">
                            <th width="70" scope="col">Tipo</th>
                            <th width="200" scope="col">Nombre</th>
                            <th width="70" scope="col">Opciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $sql = "SELECT id, nombre, archivo, extension FROM archivos_ticket WHERE ticket_id = " . $_GET['id'] ?>
                        <?php $db = new BD() ?>
                        <?php $result = $db->Seleccionar($sql) ?>
                        <?php while ($row = $db->RetornarFila($result)): ?>
                            <tr>
                                <td style="padding:8px;"><h4><img src="images/icons/link.png" style="float:left;"></h4></td>
                                <td style="padding:8px;"><h4><?php echo $row['nombre'] ?></h4></td>
                                <td style="padding:8px;">
                                    <a id="a_polizas_archivos_list" class="delete" href="javascript:void(0);" onclick="deleteAttach('<?php echo $row['id'] ?>', '<?php echo $row['archivo'] ?>')"><img src="images/icons/page_delete.png"></a>
                                </td>
                            </tr>
                        <?php endwhile; ?>
                    </tbody>
                </table>
            </div>

            <script type="text/javascript">
                                                                                                                                                                                                                                                                                                            
                function deleteAttach(id, archivo) {
                    if (confirm('¿Realmente deseas eliminar este archivo adjunto?')) {
                        var url = 'abm.php?tabla=archivos_ticket&columna=id&idregistro=' + id + '&ajax=true&archivo=upload/tickets/' + archivo + '&redirect=tickets.edit.php?id=' + <?php echo $_GET['id'] ?>;
                        window.location = url;
                    }
                }
            </script>

            <?php if ($responsed != "S"): ?>
                <div id="btn-agregar-editar" style="padding-left: 20px; padding-top: 5px;">
                    <!--- Tipo Usuario 9 = Responsable - Origen 2 = Usuaria --->
                    <a href="javascript:void(0);" onclick="$('#response_ticket_dialog').dialog('open');showTicketForAnswer('<?php echo $tkID; ?>','insert_txtarea_ticket')" class="buttons ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only">
                        <span class="ui-button-text" style="font-size: 11px;"> Responder </span>
                    </a>
                    <a href="javascript:void(0);" onclick="$('#edit_ticket_dialog').dialog('open');editTicket(<?php echo $getID ?>,'textToEdit');"class="buttons ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only">
                        <span class="ui-button-text" style="font-size: 11px;margin-left: 0px;">Editar</span>
                    </a>
                <?php else: ?>
                    <div id="btn-agregar-editar" style="margin-left: 21px;margin-top: 5px">
                        <a href="javascript:void(0);"  <?php if ($response_stc != "S"): ?> onclick="$('#response_edited_ticket_dialog').dialog('open');editAnswer(<?php echo $getID ?>,'answerToEdit')"<?php endif; ?> class="buttons ui-button ui-widget ui-state-<?php echo ($response_stc != "S") ? "default" : "disabled"; ?> ui-corner-all ui-button-text-only"><span class="ui-button-text" style="font-size: 11px;">Editar Respuesta</span></a><?php endif; ?>
                    <!--- CUANDO NO ES RESPONSABLE NI ADMINISTRADOR ---->
                    <?php if ($_SESSION["tipo_usuario"] != 0 && $_SESSION["tipo_usuario"] != 1 && $_SESSION["tipo_usuario"] != 6): ?>
                        <?php if ($can_response != "S"): ?>
                            <?php if ($_SESSION["autorizar_respuesta"] == true): ?>
                                <a href = "javascript:void(0);" onclick = "autorizarRespuesta(<?php echo $getID; ?>)" class = "buttons ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style = "margin-top: -21px;margin-left: 390px;top: -11px\9"><span class = "ui-button-text" style = "font-size: 11px;width: 125px;color: #fd0643">Autorizar Respuesta</span></a>
                            <?php endif; ?>
                        <?php else: ?>
                            <?php if ($responsed == "S"): ?>
                                <?php if ($response_stc != "S"):
                                    ?><a href = "javascript:void(0);" onclick = "$('#respuestaAlCliente').dialog('open');" class = "buttons ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style = "margin-top: -21px;margin-left: 415px;top: -11px\9"><span class = "ui-button-text" style = "font-size: 11px;width: 100px">Enviar Respuesta</span></a>
                                <?php else: ?>
                                    <a href = "javascript:void(0);" onclick = "$('#verRespuestaAlCliente').dialog('open');" class = "buttons ui-button ui-widget ui-state-disabled ui-corner-all ui-button-text-only for_ie_boton" style = "margin-top: -21px;margin-left: 415px; top: -11px\9"><span class = "ui-button-text" style = "font-size: 11px;width: 100px">Enviar Respuesta</span></a>
                                <?
                                endif;
                            endif;
                        endif;
                    else:
                        if ($_SESSION["tipo_usuario"] == 0 && ($oID == 1 || $oID == 3)):
                            if ($responsed == "S"):
                                if ($response_stc != "S"):
                                    ?><a href = "javascript:void(0);" onclick = "$('#respuestaAlCliente').dialog('open');" class = "buttons ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style = "margin-top: -21px;margin-left: 415px;top: -11px\9"><span class = "ui-button-text" style = "font-size: 11px;width: 100px">Enviar Respuesta</span></a>
                                    <?php if ($_SESSION["autorizar_respuesta"] == true): ?>
                                        <a href = "javascript:void(0);" onclick = "autorizarRespuesta(<?php echo $getID; ?>)" class = "buttons ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style = "margin-top: -21px;margin-left: 260px;top: -11px\9"><span class = "ui-button-text" style = "font-size: 11px;width: 125px;color: #fd0643">Autorizar Respuesta</span></a>
                                    <?php endif; ?>
                                <?php else: ?>
                                    <a href = "javascript:void(0);" onclick = "$('#verRespuestaAlCliente').dialog('open');" class = "buttons ui-button ui-widget ui-state-disabled ui-corner-all ui-button-text-only for_ie_boton" style = "margin-top: -21px;margin-left: 415px; top: -11px\9"><span class = "ui-button-text" style = "font-size: 11px;width: 100px">Enviar Respuesta</span></a>
                                <?
                                endif;
                            endif;
                        else:
                            if ($_SESSION["tipo_usuario"] == 1 || $_SESSION["tipo_usuario"] == 6):
                                if ($responsed == "S"):
                                    if ($response_stc != "S"):
                                        ?><a href = "javascript:void(0);" onclick = "$('#respuestaAlCliente').dialog('open');" class = "buttons ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style = "margin-top: -21px;margin-left: 415px;top: -11px\9"><span class = "ui-button-text" style = "font-size: 11px;width: 100px">Enviar Respuesta</span></a>
                                        <?php if ($_SESSION["autorizar_respuesta"] == true): ?>
                                            <a href = "javascript:void(0);" onclick = "autorizarRespuesta(<?php echo $getID; ?>)" class = "buttons ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style = "margin-top: -21px;margin-left: 260px;top: -11px\9"><span class = "ui-button-text" style = "font-size: 11px;width: 125px;color: #fd0643">Autorizar Respuesta</span></a>
                                        <?php endif; ?>
                                    <?php else: ?>
                                        <a href = "javascript:void(0);" onclick = "$('#verRespuestaAlCliente').dialog('open');" class = "buttons ui-button ui-widget ui-state-disabled ui-corner-all ui-button-text-only for_ie_boton" style = "margin-top: -21px;margin-left: 415px; top: -11px\9"><span class = "ui-button-text" style = "font-size: 11px;width: 100px">Enviar Respuesta</span></a>
                                    <?
                                    endif;
                                endif;
                            endif;
                        endif;
                    endif;
                    ?>
                    <?php if ($edited == "S"): ?>
                        <?php if ($_SESSION["autorizar_respuesta"] == true): ?>
                            <?php $style = "float: right;margin: -21px 155px 0 0;"; ?>
                        <?php else: ?>
                            <?php $style = "float: right;margin: -21px 0 0 444px;"; ?>
                        <?php endif; ?>
                        <a href = "javascript:void(0);" onclick = "$('#modificacionesTicket').dialog('open');getSelectsContent('<?php echo $tkID; ?>');" class = "buttons ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only for_ie_boton" style = "<?php echo $style; ?>"><span class = "ui-button-text" style = "font-size: 11px;">Modificaciones</span></a>
                    <?php endif; ?>
                </div>
            </div>
        </div>

        <div class="form-administrar">
            <div style="width:548px;margin-left: -220px;margin-top: 68px; ">
            </div>
        </div>

        <?
    } else {
        ?>
        <script type="text/javascript">
            window.location='tickets.php';
        </script>
        <?
    }
    ?>
    <div id="reclamos-facturas" style="display:none;">
        <input type="hidden" class="tmp_factura" id="factura_actual" name="Factura[1]" value="">
    </div>
    <div style="float:right;z-index: 9999999;margin-right: 10px;margin-top: -10px;">
        <a style="float:left;z-index: 9999999;" href="javascript: void(0);" onclick="window.location='tickets.php'">
            <span class="ui-icon ui-icon-arrowreturnthick-1-w" style="float:left;z-index: 999999;"></span>
            Volver a Reclamos Principal
        </a>
    </div>
    <div style="display: none; z-index: 1000; outline: 0px; " class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-form-reclamos-adjuntos"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"><span class="ui-dialog-title" id="ui-dialog-title-form-reclamos-adjuntos"><span class="ui-icon ui-icon-link"></span> Adjuntar Archivo</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button"><span class="ui-icon ui-icon-closethick">close</span></a></div><div id="form-reclamos-adjuntos" style="" class="ui-dialog-content ui-widget-content">
            <div class="form-administrar">
                <span class="form-label">Archivo:</span>
                <input type="file" id="file_actual" name="1" width="250">
            </div>
        </div><div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><div class="ui-dialog-buttonset"><button type="button" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false"><span class="ui-button-text">Adjuntar</span></button><button type="button" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false"><span class="ui-button-text">Cancelar</span></button></div></div></div></form>
<div id="edit_ticket_dialog">
    <div id="textToEdit"></div>
</div>
<div id="response_ticket_dialog" style="top: 0;left: 0;margin: 0 0 0 0;width: 100%;height: 100%;">
    <div style="width: 100%;">
        <span style="margin-left: 10px;font-weight: bold;">Ticket</span>
        <span style="margin-left: 345px;font-weight: bold;">Respuesta</span>
    </div>
    <div id="insert_txtarea_ticket"></div>
    <div class="texto-cuadro-reclamo">
        <textarea name="detalles" class="form-contacto-text smallInput textarea_no_resize" id="p_content_response_a" style="width:350px; height: 210px;float: right;margin-right: 5px;margin-top: 5px;"></textarea>
    </div>
    <?php if ($_SESSION["tipo_usuario"] == 0 && $aRegistro["origen_id"] == 2): ?>
        <div style="right: 15px;position: absolute;bottom: 10px;"><a href="javascript:void(0)" onclick="makeTicketAnswer('<?php echo $getID; ?>',$('textarea#p_content_response_a').val(), 'responsable', '<?php echo $_SESSION["persona"]["apellido"] . ", " . $_SESSION["persona"]["nombres"]; ?>')"class="buttons ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"><span class="ui-button-text" style="font-size: 11px;">Guardar</span></a> <a href="javascript:void(0)" onclick="$( '#response_ticket_dialog:ui-dialog' ).dialog( 'close' );"class="buttons ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"><span class="ui-button-text" style="font-size: 11px;">Cancelar</span></a></div>
    <?php else: ?>
        <div style="right: 15px;position: absolute;bottom: 10px;"><a href="javascript:void(0)" onclick="makeTicketAnswer('<?php echo $getID; ?>',$('textarea#p_content_response_a').val(), 'administracion', '<?php echo $_SESSION["persona"]["apellido"] . ", " . $_SESSION["persona"]["nombres"]; ?>')"class="buttons ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"><span class="ui-button-text" style="font-size: 11px;">Guardar</span></a> <a href="javascript:void(0)" onclick="$( '#response_ticket_dialog:ui-dialog' ).dialog( 'close' );"class="buttons ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"><span class="ui-button-text" style="font-size: 11px;">Cancelar</span></a></div>
    <?php endif; ?>
    <div id="responseMake"></div>
</div>
<div id="response_edited_ticket_dialog" style="top: 0;left: 0;margin: 0 0 0 0;width: 100%;height: 100%;">
    <div id="answerToEdit"></div>
</div>

<?php include 'form_new_ticket.php' ?>

<div id="respuestaAlCliente" style="top: 0;left: 0;margin: 0 0 0 0;background:#fefefe;width: 100%;height: 100%;">
    <div class="texto-cuadro-reclamo">
        <a id="modif_resp_email" href="javascript:void(0)" onclick="$('#email_resp_cliente').removeAttr('disabled');$('#email_resp_cliente').removeClass('disabledNew');$('#email_resp_cliente').focus();$('#confirm_resp_email').show();$('#modif_resp_email').hide();" class="buttons ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style="float: right;margin-right: 5px;"><span class="ui-button-text" style="font-size: 11px;">Modificar</span>
        </a>
        <a id="confirm_resp_email" href="javascript:void(0)" onclick="$('#email_resp_cliente').attr('disabled=disabled');$('#email_resp_cliente').addClass('disabledNew');$('#confirm_resp_email').hide();$('#modif_resp_email').show();" class="buttons ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style="float: right;margin-right: 5px;display:none"><span class="ui-button-text" style="font-size: 11px;">Finalizar</span>
        </a>
        <span style="margin-left: 5px;width: 400px;margin-top: 5px;margin-bottom: 20px;">La respuesta se enviar&aacute; a:<input type="text" name="email_to_cliente" id="email_resp_cliente" class = "form-contacto-text smallInput disabledNew" disabled="disabled" style="float: right;margin-right: 5px;width: 175px;margin-top: -1px;" value="<?php echo $email_cliente; ?>"/></span>

        <div style="margin-top: 15px;margin-left: 5px;">Respuesta:</div>
        <div id="respuesta_a_cliente" style="width:410px; height: 80px;float: right;margin-right: 5px;max-height: 80px;overflow-y: scroll;" class="form-contacto-text smallInput">
            <?php
            if ($response_edited == "S") {
                echo $response_content_edited;
            } else {
                echo $response_content;
            }
            ?>
        </div>
        <span style="margin-left: 22px;width: 400px;margin-top: 10px;float: left"><img src="images/icons/pixel.png" class="icons_new_tickets int_mail"/>CC:<input type="text" name="email_to_cliente_cc" id="email_resp_cliente_cc" class="form-contacto-text smallInput " style="margin-left: 25px;width: 175px;margin-top: -20px;" value=""/></span>
    </div>
    <div style="right: 15px;position: absolute;bottom: 10px;">
        <a href="javascript:void(0)" onclick="sendEmailToClient($('#email_resp_cliente').val(),'responseForClient',$('#email_resp_cliente_cc').val(),'','','','sendClientResp','<?php echo $tkID; ?>')" class="buttons ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only">
            <span class="ui-button-text" style="font-size: 11px;">Enviar</span>
        </a>
        <a href="javascript:void(0)" onclick="$( '#respuestaAlCliente:ui-dialog' ).dialog( 'close' );" class="buttons ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only">
            <span class="ui-button-text" style="font-size: 11px;">Cancelar</span>
        </a>
    </div>
</div>
<div id="response_for_dialogs" style="top: 0;left: 0;margin: 0 0 0 0;background:#fefefe;width: 100%;height: 100%;">
    <div id="content_information"></div>
    <div style="right: 15px;position: absolute;bottom: 10px;"><a href="javascript:void(0)" onclick="$( '#response_for_dialogs:ui-dialog' ).dialog( 'close' ); "class="buttons ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" ><span class="ui-button-text" style="font-size: 11px;">Cerrar</span></a></div>
</div>
<div id="modificacionesTicket" style="top: 0;left: 0;margin: 0 0 0 0;background:#fefefe;width: 100%;height: 100%;">
    <!--    <div class="texto-cuadro-reclamo">
            <table cellspacing="0" cellpadding="0" width="540" style="font-size: 10px !important;text-align: center;">
                <thead>
                    <tr>
                        <th width="100" scope="col"><span style="color:#c60;">Referencia</span></th>
                        <th width="100" scope="col"><span style="color:#c60;">Fecha</span></th>
                        <th width="100" scope="col"><span style="color:#c60;">Nombre</span></th>
                        <th width="100" scope="col"><span style="color:#c60;">Usuaria</span></th>
                        <th width="100" scope="col"><span style="color:#c60;">Origen</span></th>
                        <th width="100" scope="col"><span style="color:#c60;">Sucursal</span></th>
                        <th width="100" scope="col"><span style="color:#c60;">Responsable</span></th>
                        <th width="100" scope="col"><span style="color:#c60;">Estado</span></th>
                    </tr>
                </thead>-->
    <!--            <tbody id="selectsContent">
                </tbody>-->
    <!--        </table>
        </div>-->
    <?php
    if ($edited == "S") {
        $queryE = "SELECT * FROM tickets_edit WHERE ticket_id = '$tkID' ORDER BY id DESC ";
        $cBD = new BD();
        $eResultado = $cBD->Seleccionar($queryE);
        while ($zRegistro = $cBD->RetornarFila($eResultado)) {
            $edited_content = $zRegistro['edited_content'];
            $edited_by = $zRegistro['edited_by'];
            $edited_date = $zRegistro['ticket_edit_fecha'];
            $edited_date = str_replace('-', '/', $edited_date);
            $edited_date = explode('/', $edited_date);
            $edited_date = $edited_date[2] . '/' . $edited_date[1] . '/' . $edited_date[0];
            $edited_hour = $zRegistro['ticket_edit_hora'];
            $tk_id = $zRegistro['ticket_id'];
            $id = $zRegistro['id'];
            ?>
            <div id="<?php echo $tk_id ?>">
                <div id="campo-dialogo" style="margin-bottom: 5px;cursor: pointer;height: 110px;" >
                    <div id="fecha-hora">
                        <div id="fecha"><img src="images/Usermini.png"/><p style="margin-bottom:3px;">Editado Por: <?php echo $edited_by ?></p><p style="margin-bottom:3px;">Fecha: <?php echo $edited_date ?></p><p style="margin-bottom:3px;">Hora: <?php echo $edited_hour ?></p><p></p></div>
                    </div>
                    <div class="texto-cuadro-reclamo">
                        <textarea name="detalles" class="form-contacto-text smallInput textarea_no_resize" disabled="disabled" style="cursor: pointer;width:385px; height: 82px; margin-top: 6px; float:right; margin-right:6px;"><?php echo $edited_content ?></textarea>
                    </div>
                </div>
            </div>
            <?
        }
    }
    ?>
    <div id="campo-dialogo" style="cursor: pointer;margin-bottom: 5px;height: 110px;">
        <div id="fecha-hora">
            <div id="fecha"><img src="images/Usermini.png"/><p style="margin-bottom:3px;"><?php echo $allTkNombre ?></p><p style="margin-bottom:3px;">Fecha: <?php echo $fechaAllTk ?></p><p style="margin-bottom:3px;">Hora: <?php echo $hora ?></p><p></p></div>
        </div>
        <div class="texto-cuadro-reclamo">
            <textarea name="detalles" class="form-contacto-text smallInput textarea_no_resize" disabled="disabled" style="cursor: pointer; width:385px; height: 82px; margin-top: 6px; float:right; margin-right:6px;"><?php echo $allTkContent ?></textarea>
        </div>
    </div>
    <div style="float: right;margin: 5px -2px 0px 0px;">
        <a href="javascript:void(0)" onclick="$( '#modificacionesTicket:ui-dialog' ).dialog( 'close' ); "class="buttons ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only">
            <span class="ui-button-text" style="font-size: 11px;">Cerrar</span>
        </a>
    </div>

</div>
<div id="form-ticket-adjuntos" style="display:none;"></div>
<script type="text/javascript" >
    $(function() {
        // a workaround for a flaw in the demo system (http://dev.jqueryui.com/ticket/4375), ignore!
        $( "#edit_ticket_dialog:ui-dialog" ).dialog( "destroy" );
        $( "#response_ticket_dialog:ui-dialog" ).dialog( "destroy" );
        $( "#response_edited_ticket_dialog:ui-dialog" ).dialog( "destroy" );
        $( "#respuestaAlCliente:ui-dialog" ).dialog( "destroy" );
        //        $('#p_fecha_new').datepicker();

        $( "#generateNewTicket:ui-dialog" ).dialog( "destroy" );
        $( "#generateNewTicket" ).dialog({
            width: 855,
            height: 531,
            closeOnEscape: true,
            autoOpen: false,
            modal: true,
            resizable: false,
            title: 'Nuevo Ticket'
        });

        $( "#edit_ticket_dialog" ).dialog({
            width: 855,
            height: 510,
            closeOnEscape: true,
            autoOpen: false,
            modal: true,
            resizable: false,
            title: 'Editar Ticket - <?php echo htmlentities("Nro. ") . zerofill($getID, 5); ?>'
        });
        $( "#response_ticket_dialog" ).dialog({
            width: 790,
            height: 320,
            closeOnEscape: true,
            autoOpen: false,
            modal: true,
            resizable: false,
            title: 'Responder Ticket'
        });
        $( "#response_edited_ticket_dialog" ).dialog({
            width: 600,
            height: 320,
            closeOnEscape: true,
            autoOpen: false,
            modal: true,
            resizable: false,
            title: 'Editar Respuesta'
        });
        $( "#respuestaAlCliente" ).dialog({
            width: 450,
            height: 230,
            closeOnEscape: true,
            autoOpen: false,
            modal: true,
            resizable: false,
            title: 'Enviar Respuesta'
        });
        $( "#response_for_dialogs" ).dialog({
            width: 250,
            height: 115,
            closeOnEscape: true,
            autoOpen: false,
            modal: true,
            resizable: false,
            title: 'Informaci&oacute;n'
        });
        $( "#modificacionesTicket" ).dialog({
            width: 542,
            height: 420,
            closeOnEscape: true,
            autoOpen: false,
            modal: true,
            resizable: false,
            title: 'Ticket / Modificaciones '
        });
        
        $( "#dialog-alert" ).dialog({
            width: 250,
            height: 115,
            closeOnEscape: true,
            autoOpen: false,
            modal: true,
            resizable: false,
            title: 'Advertencia'
        });

    });

    jQuery(function($){
        $.datepicker.regional['es'] = {
            clearText: 'Borra',
            clearStatus: 'Borra fecha actual',
            closeText: 'Cerrar',
            closeStatus: 'Cerrar sin guardar',
            prevText: '<Ant',
            prevBigText: '<<',
            prevStatus: 'Mostrar mes anterior',
            prevBigStatus: 'Mostrar año anterior',
            nextText: 'Sig>',
            nextBigText: '>>',
            nextStatus: 'Mostrar mes siguiente',
            nextBigStatus: 'Mostrar año siguiente',
            currentText: 'Hoy',
            currentStatus: 'Mostrar mes actual',
            monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio', 'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
            monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
            monthStatus: 'Seleccionar otro mes',
            yearStatus: 'Seleccionar otro año',
            weekHeader: 'Sm',
            weekStatus: 'Semana del año',
            dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
            dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Jue', 'Vie', 'Sáb'],
            dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
            dayStatus: 'Set DD as first week day',
            dateStatus: 'Select D, M d',
            dateFormat: 'dd/mm/yy',
            firstDay: 1,
            initStatus: 'Seleccionar fecha',
            isRTL: false
        };
        $.datepicker.setDefaults($.datepicker.regional['es']);
        $('#p_responsable').change();
    });
</script>
