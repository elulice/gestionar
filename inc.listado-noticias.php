<table width="780" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="30" class="encabezado-titulo-bg"><table width="100%" border="0" cellspacing="0" cellpadding="0">
     
        <td width="20"><img src="images/espacio.gif" width="1" height="1" /></td>
        <td class="encabezado-titulo-texto" align="left">Listado de Noticias</td>
        <td><form action="listado-noticias.php" method="get" name="frmFiltro" id="frmFiltro">
		</form>
                
        </td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><img src="images/espacio.gif" width="1" height="20"></td>
  </tr>
</table>
<table width="750" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="20"><img src="images/listado-encabezado-inicio.jpg" width="20" height="37"></td>
        <td class="listado-encabezado-bg"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="70"><img src="images/espacio.gif" width="1" height="1"></td>
            <td width="75" class="listado-encabezado-texto">Fecha </td>
            <td width="450" class="listado-encabezado-texto">T&iacute;tulo </td>
            <td width="55" class="listado-encabezado-texto">Portada </td>
            <td class="listado-encabezado-texto">Visible </td>
          </tr>
        </table></td>
        <td width="20"><img src="images/listado-encabezado-final.jpg" width="20" height="37"></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="5" class="listado-contenido-inicio"><img src="images/espacio.gif" width="1" height="1"></td>
        <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
		  <?php
		  		$lRegistros = 0;
			 	$sSQL = "SELECT idnoticia, fecha, titulo, portada, destacada ";
			 	$sSQL .= "FROM noticias ORDER BY fecha DESC, idnoticia DESC ";
				$cBD = new BD();
				$oResultado = $cBD->Seleccionar($sSQL);
				while($aRegistro = $cBD->RetornarFila($oResultado))
				{
					$sPosicion = (($sPosicion == "1") ? "2" : "1");
		  ?>
          <tr>
            <td class="listado-fila-bg-<?php print($sPosicion); ?>"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="15"><img src="images/espacio.gif" width="1" height="1"></td>
                <td width="70"><table width="70" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="30"><a href="am-noticias.php?idregistro=<?php print($aRegistro["idnoticia"]); ?>&url=<?php print($m_sURL); ?>"><img src="images/btn-modificar-<?php print($sPosicion); ?>.jpg" alt="Modificar" width="24" height="23" border="0"></a></td>
                    <td><a href="abm.php?tabla=noticias&columna=idnoticia&idregistro=<?php print($aRegistro["idnoticia"]); ?>&url=<?php print($m_sURL); ?>" onclick="return confirm('&iquest;Desea eliminar esta Noticia?')"><img src="images/btn-eliminar-<?php print($sPosicion); ?>.jpg" alt="Eliminar" width="24" height="23" border="0"></a></td>
                  </tr>
                </table></td>
                <td width="75" class="listado-texto"><?php print(date("d/m/Y", strtotime($aRegistro["fecha"]))); ?></td>
                <td width="450" class="listado-texto"><?php print($aRegistro["titulo"]); ?></td>
                <td width="55" class="listado-texto"><img src="images/chk-<?php print($aRegistro["portada"]); ?>-<?php print($sPosicion); ?>.jpg" width="23" height="23" /></td>
                <td class="listado-texto"><img src="images/chk-<?php print($aRegistro["destacada"]); ?>-<?php print($sPosicion); ?>.jpg" width="23" height="23" /></td>
              </tr>
            </table></td>
          </tr>
          <?php
			 		$lRegistros++;
				}
				$cBD->Desconectar();
				if($lRegistros == 0)
				{
			 ?>
          <tr>
            <td><img src="images/espacio.gif" width="1" height="20"></td>
          </tr>
			 <?php } ?>
        </table></td>
        <td width="6" class="listado-contenido-final"><img src="images/espacio.gif" width="1" height="1"></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="100"><a href="am-noticias.php?idregistro=0&idseccion=<?php print($lIDSeccion); ?>&url=<?php print($m_sURL); ?>"><img src="images/listado-pie-inicio.jpg" alt="Agregar" width="100" height="40" border="0"></a></td>
        <td class="listado-pie-bg">&nbsp;</td>
        <td width="20"><img src="images/listado-pie-final.jpg" width="20" height="40"></td>
      </tr>
    </table></td>
  </tr>
</table>
