<?php
session_start();
if ($_SESSION["usuario"]) {
    $redirect = $_GET['redirect'];
    if (!isset($redirect)) {
        header("Location: escritorio.php");
    } else {
        header("Location: $redirect");
    }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Login | Gestion.Ar v1.0</title>
        <link href="css/960.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/reset.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/text.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/login.css" rel="stylesheet" type="text/css" media="all" />

        <script type="text/javascript" src="js/jquery-1.4.4.min.js"></script>
        <script type="text/javascript" src="scripts/jquery-ui-1.8.10.custom.js"></script>
        <script language="JavaScript" type="text/javascript" src="scripts/general.js"></script>
        <script language="JavaScript" type="text/javascript" src="scripts/swfobject.js"></script>
        <script language="JavaScript" type="text/javascript" src="scripts/correctPNG.js"></script>
        <script language="JavaScript" type="text/javascript" src="scripts/ajax.php"></script>
<!--        <script type="text/javascript" src="js/jquery-1.4.4.min.js"></script>-->
        <link type="text/css" href="css/redmond/jquery-ui-1.8.10.custom.css" rel="stylesheet" />
        <script type="text/javascript">
            $(document).ready(function(){
                $( "#recoverPass" ).dialog({
                    width: 410,
                    height: 200,
                    closeOnEscape: true,
                    autoOpen: false,
                    modal: true,
                    resizable: false,
                    title: 'Recuperaci&oacute;n de Contrase&ntilde;a'
                });
                $( "#response_for_dialogs" ).dialog({
                    width: 250,
                    height: 115,
                    closeOnEscape: true,
                    autoOpen: false,
                    modal: true,
                    resizable: false,
                    title: 'Informaci&oacute;n'
                });
            })
        </script>
    </head>
    <body>
        <div id="recoverPass" class="form-contacto-text" style="color: #222;font-size: 11px;">
            Ingrese su direcci&oacute;n de correo electr&oacute;nico.<br/>
            A continuaci&oacute;n se le enviar&aacute; un correo electr&oacute;nico con las instrucciones para recuperar su clave.<br/><br/>
            <p style="margin-left: 35px;margin-bottom: 5px;margin-top: 5px;">
                <span style="font-size: 13px;">Email: <input type="text" style="height: 20px;padding: 2px;font-size: 12px;width: 250px;" id="email_recover_pass"/></span>
            </p>
            <div style="right: 15px;position: absolute;bottom: 10px;">
                <a href="javascript:void(0)" onclick="sendEmailRecoverPass($('#email_recover_pass').val())" class="buttons ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only">
                    <span class="ui-button-text" style="font-size: 11px;">Recuperar</span>
                </a> 
                <a href="javascript:void(0)" onclick="$( '#recoverPass:ui-dialog' ).dialog( 'close' );" class="buttons ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only">
                    <span class="ui-button-text" style="font-size: 11px;">Cancelar</span>
                </a>
            </div>
        </div>
        <div id="response_for_dialogs" style="top: 0;left: 0;margin: 0 0 0 0;background:#fefefe;width: 100%;height: 100%;font-size: 12px;">
            <div id="content_information"></div>
            <div style="right: 15px;position: absolute;bottom: 10px;"><a href="javascript:void(0)" onclick="$( '#response_for_dialogs:ui-dialog' ).dialog( 'close' ); "class="buttons ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" ><span class="ui-button-text" style="font-size: 11px;">Cerrar</span></a></div>
        </div>
        <div class="container_16">
            <div class="grid_6 prefix_5 suffix_5">
                <h1>Gestion.Ar v1.0 - Login </h1>
                <div id="login">
                            <!--  <p class="tip">You just need to hit the button and you're in!</p>
                              <p class="error">This is when something is wrong!</p> -->
                    <form action="" method="post" name="frmLogin" id="frmLogin" onSubmit="return validarUsuario(this);" >
                        <p>
                            <label><strong>Usuario</strong>
                                <input type="text" name="usuario" class="inputText" id="txtUsuario" />
                            </label>
                        </p>
                        <p>
                            <label><strong>Contraseña</strong>
                                <input type="password" name="clave" class="inputText" id="txtClave" />
                            </label>
                            <label >
                                <p class="fogot_pass" onclick="$('#recoverPass').dialog('open')">&iquest; Olvid&oacute; su contrase&ntilde;a ?</p>
                            </label>
                            <input type="hidden" name="redirect" id="txtRedirect" value="<?php echo $_GET['redirect']; ?>"/>
                        </p>
                        <input type="image" src="images/btn-ing.gif" value="Ingresar" id="BTN_Ingresar" style="float:right;  *margin-bottom: 20px;margin-right: 8px;" />
                        <input type="image" src="images/btn-can.gif" id="BTN_Cancelar" value="Cancelar" style="float:right; margin-right:5px;" />


                        <!--             <label>
                                     <input style="float:left; margin-left:-10px; margin-top:20px;" type="checkbox" name="checkbox" id="checkbox" />
                                    <p style="margin-top:18px; float:left; font-size:11px; width:110px;">Recordar Contraseña</p></label>-->
                    </form>
                    <br clear="all" />
                    <div style="clear:both"></div>
                    <?php echo $error; ?>
                </div>
                <!--     <div id="forgot">
                     <a href="#" class="forgotlink"><span>Forgot your username or password?</span></a></div>-->
                <img style="float:right; margin-top:15px;" src="images/arwebs-100.png" width="100" height="16" />

            </div>
        </div>
        <br clear="all" />
    </body>
</html>
