

<div style="width:380px;margin:auto;">
   <input type="hidden" name="DPueNro" />
   <form id="frm_cv_xp_laboral" method="post" target="hidden_iframe">
   <div class="ddu_title">Experiencia Laboral</div>

   <div class="ddu_campo_select">
      <span>Puesto:</span>
      <select class="smallInput" name="PueNro">
      <?php
   $query = "SELECT PueNro, PueNom FROM puesto";
   echo GenerarOptions($query, NULL);
      ?>
      </select>
   </div>
   <div class="ddu_campo">
      <span>Cargo:</span>
      <input type="text" class="smallInput" name="PueCargos" />
   </div>
   <div class="ddu_campo">
      <span>Empresa:</span>
      <input type="text" class="smallInput" name="PueUltimaEmpresa" />
   </div>
   <div class="ddu_campo">
      <span>Rubro:</span>
      <input type="text" class="smallInput" name="PueEmpRubro" />
   </div>
   <div class="ddu_campo">
      <span>Remuneración:</span>
      <input type="text" class="smallInput" name="PueRemuneracion" />
   </div>
   <div class="ddu_campo_textarea">
      <span>Tareas Realizadas:</span>
      <textarea type="text" class="smallInput" name="PueDescPuesto"></textarea>
   </div>
   <div class="ddu_campo_doble">
      <div class="ddu_campo_col_izq">
	 <div class="ddu_campo">
	    <span>Fecha Alta:</span>
	    <input type="text" name="PueFecha"  class="smallInput" style="width:75px;" />
	 </div>
      </div>
      <div class="ddu_campo_col_der">
	 <div class="ddu_campo">
	    <span>Fecha Baja:</span>
	    <input type="text" name="PueFechaBaja" class="smallInput" style="width:75px;" />
	 </div>
      </div>
   </div>
   <div class="ddu_campo_doble">
      <div class="ddu_campo_col_izq">
	 <div class="ddu_campo">
	    <span>Antigüedad Años:</span>
	    <input type="text" name="PueExpAnos" class="smallInput" />
	 </div>
      </div>
      <div class="ddu_campo_col_der">
	 <div class="ddu_campo">
	    <span>Meses:</span>
	    <input type="text" name="PueExpMeses" class="smallInput" />
	 </div>
      </div>
   </div>
   <div class="ddu_campo">
      <span>Causa Baja:</span>
      <input type="text" name="PueCausaBaja" class="smallInput" />
   </div>
   </form>
</div>
