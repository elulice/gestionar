<input type="hidden" name="NotNro" />
<form id="frm_cv_notas" action="abm.php" method="post" target="hidden_iframe">
<div class="ddu_title"><span>Notas sobre el Postulante</span></div>

<div class="ddu_campo">
<span>Notas</span>
</div>
<div class="ddu_campo_g" style="margin:auto;margin-top:-15px;">
  <textarea name="NotaDescrip" class="ddu_campo_g1"></textarea>
</div>


<div class="ddu_title" style="margin-top:10px;">Grilla</div>
<div class="ddu_campo-grilla">
<div class="fila">
<div class="texto-grillas">Presencia</div>
<input name="presencia" type="radio" value="Excelente" style="float:left; margin-top:5px; margin-left:3px;" />
<div class="texto-grilla-gris">Excelente</div>
<input name="presencia" type="radio" value="Muy Buena" style="float:left; margin-top:5px; margin-left:3px;" />
<div class="texto-grilla-gris">Muy Buena</div>
<input name="presencia" type="radio" value="Buena" style="float:left; margin-top:5px; margin-left:3px;" />
<div class="texto-grilla-gris">Buena</div>
<input name="presencia" type="radio" value="Regular" style="float:left; margin-top:5px; margin-left:3px;" />
<div class="texto-grilla-gris">Regular</div>
<input name="presencia" type="radio" value="Mala" style="float:left; margin-top:5px; margin-left:3px;" />
<div class="texto-grilla-gris" style="border-right:none;">Mala</div>

</div>

<div class="fila" style="border-top:none;">
<div class="texto-grillas">Imagen</div>
<input name="imagen" type="radio" value="Excelente" style="float:left; margin-top:5px; margin-left:3px;" />
<div class="texto-grilla-gris">Excelente</div>
<input name="imagen" type="radio" value="Muy Buena" style="float:left; margin-top:5px; margin-left:3px;" />
<div class="texto-grilla-gris">Muy Buena</div>
<input name="imagen" type="radio" value="Buena" style="float:left; margin-top:5px; margin-left:3px;" />
<div class="texto-grilla-gris">Buena</div>
<input name="imagen" type="radio" value="Regular" style="float:left; margin-top:5px; margin-left:3px;" />
<div class="texto-grilla-gris">Regular</div>
<input name="imagen" type="radio" value="Mala" style="float:left; margin-top:5px; margin-left:3px;" />
<div class="texto-grilla-gris" style=" border-right:none;">Mala</div>

</div>

<div class="fila" style="border-top:none;">
<div class="texto-grillas">Contextura</div>
<input name="contextura" type="radio" value="Apto tareas pesadas" style="float:left; margin-top:5px; margin-left:3px;" />
<div class="texto-grilla-gris">Tareas Pesadas</div>
<input name="contextura" type="radio" value="Grande" style="float:left; margin-top:5px; margin-left:3px;"/>
<div class="texto-grilla-gris">Grande </div>
<input name="contextura" type="radio" value="Media" style="float:left; margin-top:5px; margin-left:3px;" />
<div class="texto-grilla-gris">Mediana</div>
<input name="contextura" type="radio" value="Pequeña" style="float:left; margin-top:5px; margin-left:3px;" />
<div class="texto-grilla-gris">Pequeño</div>


</div>

<div class="fila" style="border-top:none;">
<div class="texto-grillas">Trato</div>
<input name="trato" type="radio" value="Bueno" style="float:left; margin-top:5px; margin-left:3px;" />
<div class="texto-grilla-gris">Bueno</div>
<input name="trato" type="radio" value="Regular" style="float:left; margin-top:5px; margin-left:3px;" />
<div class="texto-grilla-gris">Regular</div>
<input name="trato" type="radio" value="Malo" style="float:left; margin-top:5px; margin-left:3px;" />
<div class="texto-grilla-gris">Malo</div>



</div>

<div class="fila" style="border-top:none;">
<div class="texto-grillas">Comunicación</div>
<input name="comunicacion" type="radio" value="Fluida" style="float:left; margin-top:5px; margin-left:3px;" />
<div class="texto-grilla-gris">Fluida</div>
<input name="comunicacion" type="radio" value="Normal" style="float:left; margin-top:5px; margin-left:3px;" />
<div class="texto-grilla-gris">Normal </div>
<input name="comunicacion" type="radio" value="Escasa" style="float:left; margin-top:5px; margin-left:3px;" />
<div class="texto-grilla-gris">Escasa</div>
<input name="comunicacion" type="radio" value="Dificultada" style="float:left; margin-top:5px; margin-left:3px;" />
<div class="texto-grilla-gris">Dificultada</div>


</div>


<div class="fila" style="border-top:none;">
<div class="texto-grillas">Nivel Socio Cultural</div>
<input name="nivel_sociocultural" type="radio" value="Alto" style="float:left; margin-top:5px; margin-left:3px;" />
<div class="texto-grilla-gris">Alto</div>
<input name="nivel_sociocultural" type="radio" value="Medio" style="float:left; margin-top:5px; margin-left:3px;" />
<div class="texto-grilla-gris">Medio</div>
<input name="nivel_sociocultural" type="radio" value="Medio-bajo" style="float:left; margin-top:5px; margin-left:3px;" />
<div class="texto-grilla-gris">Medio Bajo</div>
<input name="nivel_sociocultural" type="radio" value="Bajo" style="float:left; margin-top:5px; margin-left:3px;" />
<div class="texto-grilla-gris">Bajo</div>


</div>


<div class="fila"  style="border-top:none;">
<div class="texto-grillas">Flexibilidad / Adaptacion</div>
<input name="flexibilidad" type="radio" value="Excelente" style="float:left; margin-top:5px; margin-left:3px;" />
<div class="texto-grilla-gris">Excelente</div>
<input name="flexibilidad" type="radio" value="Muy Buena" style="float:left; margin-top:5px; margin-left:3px;" />
<div class="texto-grilla-gris">Muy Buena</div>
<input name="flexibilidad" type="radio" value="Buena" style="float:left; margin-top:5px; margin-left:3px;" />
<div class="texto-grilla-gris">Buena</div>
<input name="flexibilidad" type="radio" value="Regular" style="float:left; margin-top:5px; margin-left:3px;" />
<div class="texto-grilla-gris">Regular</div>
<input name="flexibilidad" type="radio" value="Mala" style="float:left; margin-top:5px; margin-left:3px;" />
<div class="texto-grilla-gris" style=" border-right:none;">Mala</div>

</div>



<div class="fila"  style="border-top:none;">
<div class="texto-grillas">Dinamismo / Vitalidad</div>
<input name="dinamismo" type="radio" value="Excelente" style="float:left; margin-top:5px; margin-left:3px;" />
<div class="texto-grilla-gris">Excelente</div>
<input name="dinamismo" type="radio" value="Muy Buena" style="float:left; margin-top:5px; margin-left:3px;" />
<div class="texto-grilla-gris">Muy Buena</div>
<input name="dinamismo" type="radio" value="Buena" style="float:left; margin-top:5px; margin-left:3px;" />
<div class="texto-grilla-gris">Buena</div>
<input name="dinamismo" type="radio" value="Regular" style="float:left; margin-top:5px; margin-left:3px;" />
<div class="texto-grilla-gris">Regular</div>
<input name="dinamismo" type="radio" value="Mala" style="float:left; margin-top:5px; margin-left:3px;" />
<div class="texto-grilla-gris" style=" border-right:none;">Mala</div>

</div>

<div class="fila"  style="border-top:none;">
<div class="texto-grillas">Actitud Hacia el Trabajo</div>
<input name="actitud" type="radio" value="Excelente" style="float:left; margin-top:5px; margin-left:3px;" />
<div class="texto-grilla-gris">Excelente</div>
<input name="actitud" type="radio" value="Muy Buena" style="float:left; margin-top:5px; margin-left:3px;" />
<div class="texto-grilla-gris">Muy Buena</div>
<input name="actitud" type="radio" value="Buena" style="float:left; margin-top:5px; margin-left:3px;" />
<div class="texto-grilla-gris">Buena</div>
<input name="actitud" type="radio" value="Regular" style="float:left; margin-top:5px; margin-left:3px;" />
<div class="texto-grilla-gris">Regular</div>
<input name="actitud" type="radio" value="Mala" style="float:left; margin-top:5px; margin-left:3px;" />
<div class="texto-grilla-gris" style=" border-right:none;">Mala</div>

</div>


<div class="fila"  style="border-top:none;">
<div class="texto-grillas">Perfil Comercial</div>
<input name="perfil_comercial" type="radio" value="Excelente" style="float:left; margin-top:5px; margin-left:3px;" />
<div class="texto-grilla-gris">Excelente</div>
<input name="perfil_comercial" type="radio" value="Muy Buena" style="float:left; margin-top:5px; margin-left:3px;" />
<div class="texto-grilla-gris">Muy Buena</div>
<input name="perfil_comercial" type="radio" value="Buena" style="float:left; margin-top:5px; margin-left:3px;" />
<div class="texto-grilla-gris">Buena</div>
<input name="perfil_comercial" type="radio" value="Regular" style="float:left; margin-top:5px; margin-left:3px;" />
<div class="texto-grilla-gris">Regular</div>
<input name="perfil_comercial" type="radio" value="Mala" style="float:left; margin-top:5px; margin-left:3px;" />
<div class="texto-grilla-gris" style=" border-right:none;">Mala</div>

</div>

<div class="fila"  style="border-top:none;">
<div class="texto-grillas">Experiencia</div>
<input name="experiencia" type="radio" value="+ 5" style="float:left; margin-top:5px; margin-left:3px;" />
<div class="texto-grilla-gris">+5</div>
<input name="experiencia" type="radio" value="de 2 a 5" style="float:left; margin-top:5px; margin-left:3px;" />
<div class="texto-grilla-gris">2 a 5</div>
<input name="experiencia" type="radio" value="1 a 2" style="float:left; margin-top:5px; margin-left:3px;" />
<div class="texto-grilla-gris">1 a 2</div>
<input name="experiencia" type="radio" value="discontinua" style="float:left; margin-top:5px; margin-left:3px;" />
<div class="texto-grilla-gris">Discontinua</div>
<input name="experiencia" type="radio" value="No posee" style="float:left; margin-top:5px; margin-left:3px;" />
<div class="texto-grilla-gris" style=" border-right:none;">No Posee</div>

</div>

<div class="fila"  style="border-top:none;">
<div class="texto-grillas">Concepto General</div>
<input name="concepto" type="radio" value="Excelente" style="float:left; margin-top:5px; margin-left:3px;" />
<div class="texto-grilla-gris">Excelente</div>
<input name="concepto" type="radio" value="Muy Buena" style="float:left; margin-top:5px; margin-left:3px;" />
<div class="texto-grilla-gris">Muy Buena</div>
<input name="concepto" type="radio" value="Buena" style="float:left; margin-top:5px; margin-left:3px;" />
<div class="texto-grilla-gris">Buena</div>
<input name="concepto" type="radio" value="Regular" style="float:left; margin-top:5px; margin-left:3px;" />
<div class="texto-grilla-gris">Regular</div>
<input name="concepto" type="radio" value="Mala" style="float:left; margin-top:5px; margin-left:3px;" />
<div class="texto-grilla-gris" style=" border-right:none;">Mala</div>

</div>

<div class="fila"  style="border-top:none;">
<div class="texto-grillas">Dis. Trabajos Temporales</div>
<input name="disp_temporal" type="radio" value="1 dia" style="float:left; margin-top:5px; margin-left:3px;" />
<div class="texto-grilla-gris">1 Día</div>
<input name="disp_temporal" type="radio" value="1 semana" style="float:left; margin-top:5px; margin-left:3px;" />
<div class="texto-grilla-gris">1 Semana</div>
<input name="disp_temporal" type="radio" value="prolongados" style="float:left; margin-top:5px; margin-left:3px;" />
<div class="texto-grilla-gris">Prolongados</div>
<input name="disp_temporal" type="radio" value="a efectivizar" style="float:left; margin-top:5px; margin-left:3px;" />
<div class="texto-grilla-gris">a Efectivizar</div>
<input name="disp_temporal" type="radio" value="no" style="float:left; margin-top:5px; margin-left:3px;" />
<div class="texto-grilla-gris" style=" border-right:none;">No</div>

</div>

<div class="fila"  style="border-top:none;">
<div class="texto-grillas">Disposicion Turnos</div>
<input name="disp_turnos" type="radio" value="Americanos" style="float:left; margin-top:5px; margin-left:3px;" />
<div class="texto-grilla-gris">Americanos</div>
<input name="disp_turnos" type="radio" value="MTN" style="float:left; margin-top:5px; margin-left:3px;" />
<div class="texto-grilla-gris">MTN</div>
<input name="disp_turnos" type="radio" value="Noche" style="float:left; margin-top:5px; margin-left:3px;" />
<div class="texto-grilla-gris">Noche</div>
<input name="disp_turnos" type="radio" value="Solo fijo" style="float:left; margin-top:5px; margin-left:3px;" />
<div class="texto-grilla-gris">Solo Fijo</div>

</div>

<div class="fila"  style="border-top:none;">
<div class="texto-grillas">Dis. Fin de Semana</div>
<input name="disp_finde" type="radio" value="Si" style="float:left; margin-top:5px; margin-left:3px;" />
<div class="texto-grilla-gris">Si</div>
<input name="disp_finde" type="radio" value="Ocasional" style="float:left; margin-top:5px; margin-left:3px;" />
<div class="texto-grilla-gris">Ocasional</div>
<input name="disp_finde" type="radio" value="No" style="float:left; margin-top:5px; margin-left:3px;" />
<div class="texto-grilla-gris">No</div>


</div>

<div class="fila"  style="border-top:none;">
<div class="texto-grillas">Libreta Sanitaria</div>
<input name="libreta_sanitaria" type="radio" value="Si" style="float:left; margin-top:5px; margin-left:3px;" />
<div class="texto-grilla-gris">Si</div>
<input name="libreta_sanitaria" type="radio" value="No" style="float:left; margin-top:5px; margin-left:3px;" />
<div class="texto-grilla-gris">No</div>
<input name="libreta_sanitaria" type="radio" value="Tramita" style="float:left; margin-top:5px; margin-left:3px;" />
<div class="texto-grilla-gris">En Tramite</div>


</div>
<div class="fila"  style="border-top:none;">
<div class="texto-grillas">Registro</div>
<input name="registro" type="radio" value="CNRT" style="float:left; margin-top:5px; margin-left:3px;" />
<div class="texto-grilla-gris">CNRT</div>
<input name="registro" type="radio" value="Particular" style="float:left; margin-top:5px; margin-left:3px;" />
<div class="texto-grilla-gris">Particular</div>
<input name="registro" type="radio" value="Moto" style="float:left; margin-top:5px; margin-left:3px;" />
<div class="texto-grilla-gris">Moto</div>
<input name="registro" type="radio" value="No" style="float:left; margin-top:5px; margin-left:3px;" />
<div class="texto-grilla-gris">No</div>



</div>
</div>
</form>
