<div class="portlet">
    <div class="portlet-header"> <span style="display:block; width:90px; float:left "><img src="images/icons/services.png" width="16" height="16" alt="Comments" />Eventos</span>
        <span style=" float:right;display:block; width:170px; "><a class="edit_cal"  href="javascript:void(0);" onclick="$('#dialogo-calendario-eventos').dialog( 'open' ); $('#calendario').fullCalendar('today'); $('#dialogo-calendario-eventos').dialog('option', 'position', 'center')" >Ver calendario</a>
            <?php if ($_SESSION["tipo_usuario"] == 6 || $_SESSION["tipo_usuario"] == 1): ?>
                <a class="edit_inline" href="javascript:void(0);" onclick="$('#dialog-form-eventos').dialog( 'open' );" style="margin-left:10px;">    Evento </a>
            <?php endif; ?>
        </span>
        <div style="clear:both"></div>
    </div>
    <div class="portlet-content">
        <div id="ultimos_eventos" style="height: inherit;">
            <img src="images/loading.gif" class="loading" />
        </div>
    </div>
</div>
<div id="dialogo-calendario-eventos" style="display:none;">
    <div id="calendario"></div>
</div>
<div id="dialog-response" style="display:none;"></div>
<div id="dialog-form-eventos" title="Nuevo Evento" style="display:none;">
    <div style="width:340px">
        <form onsubmit="return false;" action="abm.php?tabla=events&amp;columna=idevento&amp;idregistro=0&amp;archivo=0&amp;url=home.php" method="post" enctype="multipart/form-data" name="formNuevoEvento" id="formNuevoEvento">

            <div class="form-cuadros">
                <label for="titulo" class="form-label new_class_add_links" style="height: auto;margin-bottom: 0;margin-left: -30px;">T&iacute;tulo:</label>

                <input type="text" name="heading" id="heading" class="form-contacto-text smallInput" style="width: 330px; float: left;" />
            </div>
            <div class="form-cuadros">
                <div style="float:left; width:150px;"><label for="desde" class="form-label new_class_add_links" style="height: auto;margin-bottom: 0;">Desde:</label>
                    <input type="text" name="date" id="date" value="" class="form-contacto-text smallInput datetime" style="width: 100px;z-index: 9999" />
                </div>
                <div style="float:left; width:150px; margin-left:40px">
                    <label for="hasta" class="form-label new_class_add_links" style="height: auto;margin-bottom: 0;">Hasta:</label>
                    <input type="text" name="dateout" id="dateout" value="" class="form-contacto-text smallInput datetime" style="width: 100px;" />
                </div>
            </div>
            <div class="form-cuadros">
                <label for="fullbody" class="form-label new_class_add_links" style="height: auto;margin-bottom: 0;">Descripci&oacute;n:</label>
                <textarea name="fullbody" id="fullbody" class="form-contacto-text smallInput" style="width: 330px; height: 110px;" ></textarea>
            </div>
        </form>
        <div class="form-cuadros" style="clear:both; width:340px;">
            <label for="invite" class="form-label new_class_add_links" style="height: auto;margin-bottom: 0;width: auto;">Invitar Usuarios:</label>
            <input type="text" name="invite" id="invite" class="form-contacto-text smallInput" style="width: 237px; margin-bottom:10px;" />
            <div id="invites_parent"></div>
        </div>
    </div>

</div>
<!--<label for="titulo">T&iacute;tulo</label>
        <input type="text" name="heading" id="heading" class="ui-corner-all" style="width: 80%;" />
        <label for="desde">Desde</label>
        <input type="text" name="date" id="date" value="" class="ui-corner-all datetime" />
        <label for="hasta">Hasta</label>
        <input type="text" name="dateout" id="dateout" value="" class="ui-corner-all datetime" />
        <label for="fullbody">Descripci&oacute;n</label>
        <textarea name="fullbody" id="fullbody" class="ui-corner-all" style="width: 90%; height: 100px;" ></textarea>
        <label for="invite">Invitar Usuarios</label>
        <input type="text" name="invite" id="invite" style="margin-bottom: 10px;"/>-->
<script type="text/javascript">
    $(document).ready(function(){
        $(function(){
            var availableTags =
<?php
$bd = new BD();
$result = $bd->Seleccionar("SELECT PerNro, CONCAT(MEmpApellido,' ',MEmpNombres) AS usuario FROM miembroempresa");
$json = array();
while ($usuario = $bd->RetornarFila($result)) {
    $json[] = array("value" => $usuario['usuario'], "id" => $usuario['PerNro']);
}
echo json_encode($json);
?>
            ;
            function split( val ) {
                return val.split( /,\s*/ );
            }
            function extractLast( term ) {
                return split( term ).pop();
            }
            //attach autocomplete
            $("#invite").autocomplete({
                minLength: 0,
                //define callback to format results
                source: function( request, response ) {
                    //alert(request);
                    // delegate back to autocomplete, but extract the last term
                    response( $.ui.autocomplete.filter(
                    availableTags, extractLast( request.term ) ) );
                },
                //define select handler
                select: function(e, ui) {
                    if (!($('#invitadoFormEvento_'+ui.item.id).length > 0)) {
                        //create formatted friend
                        var friend = ui.item.value,
                        span = $("<span>").text(friend),
                        a = $("<a>").addClass("remove").attr({
                            href: "javascript:",
                            title: "Remove " + friend,
                            rel:    ui.item.id
                        }).text("x").appendTo(span);
                        span.addClass("ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only");

                        //add friend to friend div
                        //span.insertBefore("#invite");
                        span.appendTo("#invites_parent");
                        $("#formNuevoEvento").append('<input type="hidden" name="invitados[]" id="invitadoFormEvento_'+ui.item.id+'" value="'+ui.item.id+'" />');
                        this.value = '';
                        return false;
                    }
                },
                //define select handler
                change: function(event, ui) {
                    //prevent 'to' field being updated and correct position
                    $("#invite").val("").css("top", 2);
                }
            });
        });
        $("#invites_parent").click(function(){

            //focus 'to' field
            $("#invite").focus();
        });

        //add live handler for clicks on remove links
        $(".remove", document.getElementById("invites_parent")).live("click", function(){
            $('#invitadoFormEvento_'+$(this).attr('rel')).remove();
            //remove current friend
            $(this).parent().remove();

            //correct 'to' field position
            if($("#invites_parent span").length === 0) {
                $("#invite").css("top", 0);
            }
        });
    });
</script>
