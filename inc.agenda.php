<?php require_once("inc.postulante.detalle.php"); ?>
<div id="div_dialog_agenda" style="display:none;"><?php require_once("inc.curriculums.notas.php"); ?></div>
<div style="margin:8px 0 0 18px;">
    <form id="frm_agenda_listado_filtro" target="hidden_iframe" onsubmit="actualizar_agenda_listado();">
        <input type="submit" style="display:none;" />
        <table width="796" cellspacing="3" style="margin-left:40px;">
            <tbody>
                <tr height="30">
                    <td width="230">
                        <div style="float:left;width:220px;">
                            <div class="form-label" style="width:80px;text-align:left;">Estado:</div>
                            <select class="smallInput" onchange="actualizar_agenda_listado();" name="estadopost" style="width:130px;">
                                <?php
                                $query = "SELECT PEsNro, PEsDescrip FROM postulacionestado";
                                echo GenerarOptions($query, NULL, TRUE, DEFSELECT);
                                ?>
                            </select>
                        </div>
                    </td>
                    <td width="230">
                        <div style="float:left;width:220px;">
                            <div class="form-label" style="width:80px;text-align:left;">Situación:</div>
                            <select class="smallInput" onchange="actualizar_agenda_listado();" name="estado" style="width:130px;">
<?php
$query = "SELECT nombre, nombre FROM _ofertas_estados";
echo GenerarOptions($query, NULL, TRUE, DEFSELECT);
?>
                            </select>
                        </div>
                    </td>
                    <td width="230">
                        <div style="float:left;width:220px;">
                            <div class="form-label" style="width:80px;text-align:left;">Cliente:</div>
                            <select class="smallInput" onchange="actualizar_agenda_listado();" name="CliNro" style="width:130px;">
<?php
$query = "SELECT MEmpNro, CONCAT(MEmpApellido, ' ', MEmpNombres) FROM miembroempresa WHERE MEmpAdmin = 0
ORDER BY MEmpApellido ASC, MEmpNombres ASC";
echo GenerarOptions($query, NULL, TRUE, DEFSELECT);
?>
                            </select>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td width="230">
                        <div style="float:left;width:220px;">
                            <div class="form-label" style="width:80px;text-align:left;">Fecha Desde:</div>
                            <input class="smallInput" name="fecha_desde" style="width:120px;text-align:center;" />
                        </div>
                    </td>
                    <td width="230">
                        <div style="float:left;width:220px;">
                            <div class="form-label" style="width:80px;text-align:left;">Fecha Hasta:</div>
                            <input class="smallInput" name="fecha_hasta" style="width:120px;text-align:center;" />
                        </div>
                    </td>
                    <td width="230">
                        <div style="float:left;width:220px;">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td width="230">
                        <div style="float:left;width:220px;">
                            <div class="form-label" style="width:80px;text-align:left;">Selector:</div>
                            <select class="smallInput" onchange="actualizar_agenda_listado();" name="MEmpNro" style="width:130px;">
<?php
$query = "SELECT MEmpNro, CONCAT(MEmpApellido, ' ', MEmpNombres) FROM miembroempresa ORDER BY MEmpApellido ASC, MEmpNombres ASC";
echo GenerarOptions($query, NULL, TRUE, DEFSELECT);
?>
                            </select>
                        </div>
                    </td>
                    <td width="230">
                        <!--div style="float:left;width:220px;">
                           <div class="form-label" style="width:80px;text-align:left;">Sucursal:</div>
                           <select class="smallInput" onchange="actualizar_agenda_listado();" name="UniNro" style="width:130px;">
                                <?php
                                $query = "SELECT UniNro, UniNombre FROM unidadorg";
                                echo GenerarOptions($query, NULL, TRUE, DEFSELECT);
                                ?>
                           </select>
                        </div-->
                    </td>
                    <td width="290">
                        <a class="button_notok" onclick="$('#frm_agenda_listado_filtro').clearForm(); actualizar_agenda_listado();" style="margin-top:3px;"><span>Limpiar Búsqueda</span></a>
                        <a class="button_ok" onclick="actualizar_agenda_listado();" style="margin-top:3px;"><span>Buscar</span></a>
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
</div>
<div id="div_agenda_listado" class="navPage" style="height:220px;">
    <img src="images/loading.gif" class="loading" />
</div>

