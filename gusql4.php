<?php

ini_set('max_execution_time', 0);
# te paso los pasos para conectarte por PHP via ODBC a nuestro SQL.
# 1 - conecto con el DSN "SQLGestion" con nuevo usuario y password.
$connect = odbc_connect("SQLGestion", "consulta", "56Axton2015");

# 2 - armo la tabla empresa para los campos numero y razonsocial
$query = "select e2.EmpRSocial as 'Emp.Administrada', uo2.UniNombre as 'UOP',legajo.legnumero as 'Legajo', persona.PerDocumento as 'Documento',persona.PerApellido as 'Apellido', persona.PerNombres as 'Nombres',
  e1.EmpRSocial as 'Usuaria',uo1.UniNombre as 'UOC',
  Legajonodo.LNodFecIng as 'F.Ingreso Asig.', legajonodo.LNodFecEgr as 'F.Egreso Asig.',domicilio.DomCalleNombre as 'Calle',
  domicilio.DomCalleNro as 'Numero', domicilio.DomPiso as 'Piso', domicilio.DomDpto as 'Departamento',
  localidad.LocNombre as 'Localidad', departamento.DepNombre as 'Partido', provincia.PrvNombre as 'Provincia',
  com1.ComValor as 'Email', com3.ComValor as 'Telefono', com2.ComValor as 'Telefono 1', com4.ComValor as 'Telefono 2',
  legajo.LegCelular as 'Celular', categoria.CatNombre as 'Categoria', e1.EmpNro as 'Cod.Cliente', persona.PerNro as 'Cod.persona',
  e2.EmpNro as 'Cod.EmpAdministrada'
from LegajoNodo
  join legajo on legajonodo.LegNro=legajo.LegNro
  join Persona on legajo.PerNro=persona.PerNro
  join Domicilio on persona.PerNro=domicilio.PerNro
  join Localidad on domicilio.LocNro=localidad.LocNro
  join Departamento on domicilio.DepNro=departamento.DepNro
  join Provincia on Domicilio.PrvNro=Provincia.PrvNro
  join Nodo as n1 on legajonodo.NodNro=n1.NodNro
  join Nodo as n2 on legajonodo.MiNodNro=n2.NodNro
  join Empresa as e1 on n1.EmpNro=e1.EmpNro
  join Empresa as e2 on e2.EmpNro=legajo.EmpNro
  join Nodo2 on nodo2.NodNro=legajonodo.NodNro
  join categoria on categoria.CatNro=legajonodo.CatNro
  left join Comunicacion as com1 on com1.PerNro=persona.PerNro and com1.MComNro=2  -- email
  left join comunicacion as com2 on com2.pernro=persona.pernro and com2.MComNro=1  -- Telefono 1
  left join comunicacion as com3 on com3.pernro=persona.pernro and com3.MComNro=7  -- Telefono
  left join comunicacion as com4 on com4.pernro=persona.pernro and com4.MComNro=8  -- Telefono 2
  join UnidadOrg as uo1 on uo1.UniNro=n1.UniNro --UOC
  join UnidadOrg as uo2 on uo2.UniNro=n2.UniNro --UOP
";

// Nodo.NodNro IN (7562, 11501, 11500, 11870, 11499, 12162) AND  MiNodo.NodNro IN (8125, 2735)
# 3 - ejecuto la consulta
$result = odbc_exec($connect, $query);

function formatDate($date) {
    $date = trim($date);
    if (!empty($date)) {
        $y = substr($date, 0, 4);
        $m = substr($date, 4, 2);
        $d = substr($date, 6, 2);
        return $y . '-' . $m . '-' . $d;
    } else {
        return NULL;
    }
}

if ($result !== FALSE) {
    #4 - Limpio la tabla de Clientes
    $conectID2 = mysql_connect('localhost', 'root', '');
    mysql_select_db('gestion_ar');
    $res = mysql_query('TRUNCATE TABLE colaboradores', $conectID2);
    # 5 - guardo la data en una estructura repetitiva y actualiza la base de datos.
    if ($res !== FALSE) {
        $count = 0;
        $begin = 'INSERT INTO
                        colaboradores
                            (id,
                             dni,
                             nombre,
                             apellido,
                             calle,
                             numero,
                             piso,
                             departamento,
                             localidad,
                             provincia,
                             email,
                             telefono,
                             usuaria_id,
                             fecha_desde,
                             fecha_hasta
                             )
                    VALUES ';
        $consulta = '';
        while (odbc_fetch_row($result)) {
            $data['dni'] = trim(odbc_result($result, 4));
            $data['nombre'] = trim(odbc_result($result, 6));
            $data['apellido'] = trim(odbc_result($result, 5));
            $data['fIngreso'] = formatDate(odbc_result($result, 9));
            $data['fEgreso'] = formatDate(odbc_result($result, 10));
            $data['calle'] = trim(odbc_result($result, 11));
            $data['numero'] = trim(odbc_result($result, 12));
            $data['piso'] = trim(odbc_result($result, 13));
            $data['departamento'] = trim(odbc_result($result, 14));
            $data['localidad'] = trim(odbc_result($result, 16));
            $data['provincia'] = trim(odbc_result($result, 17));
            $data['email'] = trim(odbc_result($result, 18));
            $data['telefono'] = trim(odbc_result($result, 19));
            $data['usuaria_id'] = trim(odbc_result($result, 24));
            $data['colaborador_id'] = trim(odbc_result($result, 25));

            $consulta .= "('" . $data['colaborador_id'] . "',
                        '" . $data['dni'] . "',
                        '" . $data['nombre'] . "',
                        '" . $data['apellido'] . "',
                        '" . $data['calle'] . "',
                        '" . $data['numero'] . "',
                        '" . $data['piso'] . "',
                        '" . $data['departamento'] . "',
                        '" . $data['localidad'] . "',
                        '" . $data['provincia'] . "',
                        '" . $data['email'] . "',
                        '" . $data['telefono'] . "',
                        '" . $data['usuaria_id'] . "',
                        '" . $data['fIngreso'] . "',
                        " . (($data['fEgreso'] == NULL) ? 'NULL' : "'" . $data['fIngreso'] . "'") . "
                        )";
            $count++;
            if ($count >= 700) {
                $count = 0;
                mysql_query($begin . $consulta . ';', $conectID2);
                $consulta = '';
            } else {
                $consulta .= ', ';
            }
        }
    }
}
# 6 - destruyo la conexion
odbc_close($connect);
?>
