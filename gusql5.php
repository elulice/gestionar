<?php

ini_set('max_execution_time', 0);
error_reporting(E_ALL);
ini_set('display_errors', '1');
# te paso los pasos para conectarte por PHP via ODBC a nuestro SQL.
# 1 - conecto con el DSN "SQLGestion" con nuevo usuario y password.
$connect = odbc_connect("SQLGestion", "gestiondbconsultas", "gconsultas");

# 2 - armo la tabla empresa para los campos numero y razonsocial
$query = "SELECT
       /*MiembroEmpresa.MEmpNro AS MIEMBROEMPRESA, */
       Persona.PerNro AS IDUSUARIO,
       lower(left(Persona.PerNombres,1)+(Persona.PerApellido)) AS USUARIO,
       Persona.PerNombres,
       Persona.PerApellido,
       Puesto.PueNro as IDPUESTO,
       Puesto.PueDesc AS PUESTO,
       Area.AreNro AS IDAREA,
       Area.AreNombre AS AREA,
       Comunicacion.ComValor AS [E-Mail Principal],
       UnidadOrg.UniNro AS IDSUCURSAL,
       UnidadOrg.UniNombre AS SUCURSAL
FROM Persona
       INNER JOIN MiembroEmpresa ON Persona.PerNro = MiembroEmpresa.PerNro
       INNER JOIN Empresa ON MiembroEmpresa.EmpNro = Empresa.EmpNro
       LEFT JOIN Puesto ON MiembroEmpresa.PueNro = Puesto.PueNro
       LEFT JOIN Area ON MiembroEmpresa.AreNro = Area.AreNro
       LEFT JOIN Comunicacion ON Comunicacion.PerNro = Persona.PerNro
       JOIN Nodo ON Nodo.NodNro = MiembroEmpresa.NodNro
       JOIN UnidadOrg ON UnidadOrg.UniNro = Nodo.UniNro
WHERE
       IsNull(MiembroEmpresa.MEmpFEgr, '') = '' AND
       Comunicacion.TUsoNro = 4 AND
       Empresa.EmpNro = 398
ORDER BY
       SUCURSAL, AREA, USUARIO ASC
";

// Nodo.NodNro IN (7562, 11501, 11500, 11870, 11499, 12162) AND  MiNodo.NodNro IN (8125, 2735)
# 3 - ejecuto la consulta
$result = odbc_exec($connect, $query);

//odbc_result_all($result);
//die();

function formatDate($date) {
    $date = trim($date);
    if (!empty($date)) {
        $y = substr($date, 0, 4);
        $m = substr($date, 4, 2);
        $d = substr($date, 6, 2);
        return $y . '-' . $m . '-' . $d;
    } else {
        return NULL;
    }
}

function rand_string($length) {
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    $size = strlen($chars);
    $str = '';
    for ($i = 0; $i < $length; $i++) {
        $str .= $chars[rand(0, $size - 1)];
    }

    return $str;
}

echo '<pre>';
if ($result !== FALSE) {

    $areas = array();
    $puestos = array();

    #4 - Limpio la tabla de Clientes
    $conectID2 = mysql_connect('localhost', 'root', 'arwebs2013');
    mysql_select_db('gestion_ar');
    $res = mysql_query('TRUNCATE TABLE miembroempresa', $conectID2);
    # 5 - guardo la data en una estructura repetitiva y actualiza la base de datos.
    if ($res !== FALSE) {
        $begin = 'INSERT INTO
                        miembroempresa
                            (PerNro,
                             MEmpUsuario,
                             MEmpNombres,
                             MEmpApellido,
                             PueNro,
                             AreNro,
                             MEmpEmail,
                             UniNro,
                             EmpNro
                             )
                    VALUES ';

        while ($data = odbc_fetch_array($result)) {
            //for ($i = 1; $i <= odbc_num_rows($result); $i++) {
            //$data = odbc_fetch_array($result, $i);
            var_dump($data);

            $consulta = "('" . $data['IDUSUARIO'] . "',
                        '" . $data['USUARIO'] . "',
                        '" . ucfirst(strtolower($data['PerNombres'])) . "',
                        '" . ucfirst(strtolower($data['PerApellido'])) . "',
                        '" . $data['IDPUESTO'] . "',
                        '" . $data['IDAREA'] . "',
                        '" . $data['E-Mail Principal'] . "',
                        '" . $data['IDSUCURSAL'] . "',
                            313);";
            if (!in_array($data['AREA'], $areas)) {
                $areas[$data['IDAREA']] = $data['AREA'];
            }
            if (!in_array($data['PUESTO'], $puestos)) {
                $puestos[$data['IDPUESTO']] = $data['PUESTO'];
            }

            $res = mysql_query($begin . $consulta, $conectID2);
            var_dump($res);

            $sql = 'SELECT COUNT(*) FROM passwords WHERE user_id=' . $data['IDUSUARIO'];
            $res = mysql_query($sql, $conectID2);
            if ($res !== FALSE && mysql_num_rows($res) > 0) {
                $count = mysql_fetch_array($res);
                if ($count[0] == 0) {
                    $sql = 'INSERT INTO passwords (user_id, password) 
                        VALUES("' . $data['IDUSUARIO'] . '", "' . rand_string(10) . '")';
                    $res = mysql_query($sql, $conectID2);
                }
            }
            echo $begin . $consulta . "\n";
        }

        $sql = "INSERT INTO  `miembroempresa` (
                `EmpNro` ,
                `MEmpNro` ,
                `PueNro` ,
                `PerNro` ,
                `AreNro` ,
                `UniNro` ,
                `OrgNro` ,
                `NodNro` ,
                `MEmpObservac` ,
                `MEmpDetalle` ,
                `MEmpLegajo` ,
                `MEmpTipo` ,
                `MEmpFIngr` ,
                `MEmpUsuario` ,
                `MEmpAdmin` ,
                `MEmpEmail` ,
                `MEmpNombres` ,
                `MEmpApellido` ,
                `MEmpPostEmail` ,
                `MEmpCantMail` ,
                `MEmpForward` ,
                `MEmpTipoDoc` ,
                `MEmpDocumento` ,
                `MEmpKUO` ,
                `active`
                )
                VALUES (
                '313', NULL , NULL ,  '0', NULL , NULL ,
                NULL ,  '1', NULL , NULL , NULL , NULL , 
                NULL ,  'administrador',  '1',  'emoreno@arwebs.net',
                'Eduardo',  'Moreno', NULL , NULL , NULL , NULL , 
                NULL , NULL ,  '1'
                )";
        $res = mysql_query($sql, $conectID2);

        var_dump($res);

        mysql_query('TRUNCATE TABLE area', $conectID2);
        foreach ($areas as $id => $area) {
            $sql = '
                INSERT INTO area (AreNro, AreNom) VALUES (
                    "' . $id . '",
                    "' . ucfirst(strtolower($area)) . '"
                );
            ';
            $res = mysql_query($sql, $conectID2);
            echo $sql;
        }

        mysql_query('TRUNCATE TABLE puesto', $conectID2);
        foreach ($puestos as $id => $puesto) {
            $sql = '
                INSERT INTO puesto (PueNro, PueNom) VALUES (
                    "' . $id . '",
                    "' . ucfirst(strtolower($puesto)) . '"
                );
            ';
            $res = mysql_query($sql, $conectID2);
            var_dump($res);
            echo $sql;
        }

//        var_dump($areas);
//        var_dump($puestos);
    }
}
echo '</pre>';
# 6 - destruyo la conexion
odbc_close($connect);
?>
