<?php
	session_start();
	include("clases/framework-1.0/class.bd.php");
	include("includes/funciones.php");
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?php print SITIO; ?></title>
<script language="JavaScript" type="text/javascript" src="scripts/general.js"></script>
<script language="JavaScript" type="text/javascript" src="scripts/ajax.php"></script>
<link href="estilos/general.css" rel="stylesheet" type="text/css">

<script src="scripts/jquery.js"></script>
<script src="scripts/SearchHighlight.js"></script>
<script type='text/javascript'>
  var opzioni = {exact:"exact",style_name_suffix:false,keys: "<?php print $_SESSION["Palabras"]; ?>", style_name: "resaltado"};
  jQuery(function(){
	jQuery(document).SearchHighlight(opzioni);
  });
</script>
</head>

<body>
<?php
	include("inc.encabezado.php");
	if ($m_lIDPostulante > 0) include("inc.submenu-curriculums.php");
	include("inc.am-curriculums-notas.php");
	include("inc.pie.php");
?>
</body>
</html>
