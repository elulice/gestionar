<link rel="stylesheet" type="text/css" href="css/960.css" />
<link rel="stylesheet" type="text/css" href="css/reset.css" />
<link rel="stylesheet" type="text/css" href="css/text.css" />
<link rel="stylesheet" type="text/css" href="css/blue.css" />
<link rel="stylesheet" type="text/css" href="css/shadowbox.css" />
<link type="text/css" href="css/redmond/jquery-ui-1.8.10.custom.css" rel="stylesheet" />
<link rel='stylesheet' type='text/css' href='css/fullcalendar.css' />

<script type="text/javascript" src="js/jquery-1.4.4.min.js"></script>
<script type="text/javascript" src="js/blend/jquery.blend.js"></script>

<script type="text/javascript" src="js/jquery-ui-1.8.10.custom.js"></script>
<script type="text/javascript" src="scripts/adminseg.js"></script>
<script type="text/javascript" src="scripts/produccion.js"></script>        <script type="text/javascript" src="scripts/concursos.js"></script>

<script type="text/javascript" src="scripts/shadowbox.js"></script>
<script type='text/javascript' src='scripts/fullcalendar.js'></script>
<script src="js/jquery-ui-timepicker-addon.js" type="text/javascript"></script>
<script src="scripts/highchart/highcharts.js" type="text/javascript"></script>
<script src="scripts/highchart/modules/exporting.js" type="text/javascript"></script>

<script type="text/javascript" src="js/effects.js"></script>
<script language="JavaScript" type="text/javascript" src="scripts/general.js"></script>
<script language="JavaScript" type="text/javascript" src="scripts/swfobject.js"></script>
<script language="JavaScript" type="text/javascript" src="scripts/correctPNG.js"></script>
<script language="JavaScript" type="text/javascript" src="scripts/ajax.php"></script>
<script type="text/javascript" src="scripts/jScrollPane.js"></script>
<link rel="stylesheet" type="text/css" media="all" href="css/jScrollPane.css" />

<style type="text/css">
    /* css for timepicker */
    .ui-timepicker-div .ui-widget-header{ margin-bottom: 8px; }
    .ui-timepicker-div dl{ text-align: left; }
    .ui-timepicker-div dl dt{ height: 25px; }
    .ui-timepicker-div dl dd{ margin: -25px 0 10px 65px; }
    .ui-timepicker-div td { font-size: 90%; }
    .ui-dialog .ui-dialog-title .ui-icon {
        float: left;
        margin-right: 4px;
        margin-top:1px;}
    .ui-button-text-only .ui-button-text { padding: .2em 1em; font-size:11px; font-weight:normal;}


</style>
<div id="portlets">
    <div class="grid_8" style="width: 165px;">
        <h1 class="prod_edit">Reportes</h1></div>
    <div style=" float:right;display:block; width:580px; margin-top:18px;">
        <a href="javascript: void(0);" onClick="$('#historiales-dialog').dialog('open')" class="buttons buttons-history" style="width: 100px; font-size:11px; font-weight: normal; float: left; margin-left: 5px;">Historial</a>
        <a href="javascript: void(0);" onClick="$('#reportes-dialog').dialog('open')" class="buttons buttons-cargar" style="width: 100px; font-size:11px; font-weight: normal; float: left; margin-left: 5px;">Cargar</a>
        <a href="javascript: void(0);" onClick="if($('[name=id_reporte]').val() == 0){$('#nombre-dialog').dialog('open');} else{guardarReporte();}" class="buttons buttons-guardar" style="width: 100px; font-size:11px; font-weight: normal; float: left; margin-left: 5px;">Guardar</a>
        <a href="javascript: void(0);" onClick="$('#nombre-dialog').dialog('open');" class="buttons buttons-guardar-como" style="width: 120px; font-size:11px; font-weight: normal; float: left; margin-left: 5px;">Guardar como</a>
        <a href="javascript: void(0);" onClick="generarReporteProduccion();" class="buttons buttons-consultar" style="width: 100px; font-size:11px; font-weight: bold; float: left; margin-left: 5px;">Consultar</a>
    </div>
    <div class="clear"></div>
    <form action="#" method="post" enctype="multipart/form-data" name="formProduccion" id="formProduccion">
        <div class="portlet">
            <span class="portlet-header" style="color: #333333 !important;">Datos a Consultar</span>
            <div style="width:880px; border-top: 1px solid #CCCCCC; display:inline; float:left; height:80px; overflow:hidden; padding: 10px 0px 0px 25px;">

                <input type="submit" style="display:none" value="Submit" />

                <div style="width:125px; margin-right:15px; display:block; float:left;">
                    <div class="form-administrar2"><input name="nombres" id="nombres" type="checkbox" class="form-contacto-text selectCheckbox"/> <label for="nombres">Nro. Reporte</label></div>
                    <div class="form-administrar2"><input name="fecha_nacimiento" id="fecha_nacimiento" type="checkbox" class="form-contacto-text selectCheckbox"/> <label for="fecha_nacimiento">Estado</label></div>

                </div>

                <div style="width:100px; margin-right:15px; display:block; float:left;">
                    <div class="form-administrar2"><input name="id_compania" id="id_compania" type="checkbox" class="form-contacto-text selectCheckbox"/> <label for="id_compania">Sucursal</label></div>
                    <div class="form-administrar2"><input name="id_producto" id="id_producto" type="checkbox" class="form-contacto-text selectCheckbox"/> <label for="id_producto">Tiempo de Resp</label></div>

                </div>
                <div style="width:125px; margin-right:15px; display:block; float:left;">
                    <div class="form-administrar2"><input name="forma_pago" id="forma_pago" type="checkbox" class="form-contacto-text selectCheckbox"/> <label for="forma_pago">Nombres</label></div>
                    <div class="form-administrar2"><input name="id_producto" id="id_producto" type="checkbox" class="form-contacto-text selectCheckbox"/> <label for="id_producto">Fecha</label></div>
                </div>
                <div style="width:100px; margin-right:15px; display:block; float:left;">
                    <div class="form-administrar2"><input name="prima_planeada" id="prima_planeada" type="checkbox" class="form-contacto-text selectCheckbox" /> <label for="prima_planeada">Usuarias</label></div>

                </div>
                <div style="width:125px; margin-right:15px; display:block; float:left;">
                    <div class="form-administrar2"><input name="estado" id="estado" type="checkbox" class="form-contacto-text selectCheckbox" /> <label for="estado">Origen</label></div>
                </div>

                <div style="width:100px; margin-right:15px; display:block; float:left;">
                    <div class="form-administrar2"><input name="fecha_vigencia" id="fecha_vigencia" type="checkbox" class="form-contacto-text selectCheckbox"> <label for="fecha_vigencia">Sucursal</label></div>
                </div>
                <div style="width:100px; margin-right:15px; display:block; float:left;">
                    <div class="form-administrar2"><input name="frecuencia_pago" id="frecuencia_pago" type="checkbox" class="form-contacto-text selectCheckbox"/> <label for="frecuencia_pago">Responsable</label></div>
                </div>
            </div>
            <div class="portlet">
                <span class="portlet-header" style="color: #333333 !important;">Filtros</span>
                <div style="width:880px; display:inline; border-top: 1px solid #CCCCCC; float:left; height:180px; overflow:hidden; padding: 10px 0px 0px 25px; font-weight: normal;" >
                    <div style="width:190px; margin-right:15px; display:block; float:left;">
                        <div class="form-administrar2" style="width:190px;"><label for="f_fecha_envio_d" style="width:120px;">Fecha de Envio desde:</label>
                            <input type="text" name="f_fecha_envio_d" id="f_fecha_envio_d" style="width:60px; float:right;" value="" class="datepicker form-contacto-text smallInput"/>
                        </div>
                        <div class="form-administrar2" style="width:190px;"><label for="f_fecha_envio_h" style="width:120px;">Fecha de Envio hasta:</label>
                            <input type="text" name="f_fecha_envio_h" id="f_fecha_envio_h"  style="width:60px; float:right;" value="2012-08-23" class="datepicker form-contacto-text smallInput"/>
                        </div>
                        <div class="form-administrar2" style="width:190px;"><label for="f_fecha_nacimiento_d" style="width:120px;">Fecha de Resp. desde:</label>
                            <input type="text" name="f_fecha_nacimiento_d" id="f_fecha_nacimiento_d" style="width:60px; float:right;" value="" class="datepicker form-contacto-text smallInput"/>
                        </div>
                        <div class="form-administrar2" style="width:190px;"><label for="f_fecha_nacimiento_h" style="width:120px;">Fecha de Resp. hasta:</label>
                            <input type="text" name="f_fecha_nacimiento_h" id="f_fecha_nacimiento_h"  style="width:60px; float:right;" value="" class="datepicker form-contacto-text smallInput"/>
                        </div>
                    </div>

                    <div style="width:190px; margin-right:15px; display:block; float:left;">
                        <div class="form-administrar2" style="width:190px;"><label for="f_suma_asegurada_v" style="width:70px;">Usuaria:</label>
                            <div style="width:120px; display:block; float:left;">
                                <input type="text" name="f_suma_asegurada_v" id="f_suma_asegurada_v" style="width:60px; float:right;" value="" class="form-contacto-text smallInput"/>
                                <input type="hidden" class="comparador" name="f_suma_asegurada_c" value="0" />
                                <a href="javascript: void(0);" onClick="cambiarComparacion('f_suma_asegurada_c', this);" class="buttons" style="margin-right: 10px; font-size: 16px; float:right; text-decoration: none; width: 38px; height: 20px;" title="Mayor que"><span class="ui-icon ui-icon-plusthick"></span></a>
                            </div>
                        </div>
                        <div class="form-administrar2" style="width:190px;"><label for="f_suma_asegurada_v" style="width:70px;">Sucursal:</label>
                            <div style="width:120px; display:block; float:left;">
                                <input type="text" name="f_suma_asegurada_v" id="f_suma_asegurada_v" style="width:60px; float:right;" value="" class="form-contacto-text smallInput"/>
                                <input type="hidden" class="comparador" name="f_suma_asegurada_c" value="0" />
                                <a href="javascript: void(0);" onClick="cambiarComparacion('f_suma_asegurada_c', this);" class="buttons" style="margin-right: 10px; font-size: 16px; float:right; text-decoration: none; width: 38px; height: 20px;" title="Mayor que"><span class="ui-icon ui-icon-plusthick"></span></a>
                            </div>
                        </div>
                        <div class="form-administrar2" style="width:190px;"><label for="f_suma_asegurada_v" style="width:70px;">Estado:</label>
                            <div style="width:120px; display:block; float:left;">
                                <input type="text" name="f_suma_asegurada_v" id="f_suma_asegurada_v" style="width:60px; float:right;" value="" class="form-contacto-text smallInput"/>
                                <input type="hidden" class="comparador" name="f_suma_asegurada_c" value="0" />
                                <a href="javascript: void(0);" onClick="cambiarComparacion('f_suma_asegurada_c', this);" class="buttons" style="margin-right: 10px; font-size: 16px; float:right; text-decoration: none; width: 38px; height: 20px;" title="Mayor que"><span class="ui-icon ui-icon-plusthick"></span></a>
                            </div>
                        </div>
                        <div class="form-administrar2" style="width:190px;"><label for="f_fecha_recibido_h" style="width:120px;">Tiempo Resp:</label>
                            <input type="text" name="" id=""  style="width:60px; float:right;" value="" class="form-contacto-text smallInput"/>
                        </div>

                    </div>
                    <div style="width:190px; margin-right:15px; display:block; float:left;">
                        <div class="form-administrar2" style="width:190px;"><label for="o_order" style="width:100px;">Respondido por:</label>
                            <select name="o_order" id="o_order"  class="smallInput" title="Ordenar por" style="width:90px; float:left; margin-bottom: 5px ;" >
                                <option value="0">Campo</option>
                            </select>
                        </div>
                        <div class="form-administrar2" style="width:190px;"><label for="o_progresion" style="width:100px;">Editado por:</label>
                            <select name="o_progresion" id="o_progresion"  class="smallInput" title="Progresion" style="width:90px; float:left; margin-bottom: 5px ;" >
                                <option value="0">Campo</option>
                            </select>
                        </div>
                        <div class="form-administrar2" style="width:190px;"><label for="o_order" style="width:100px;">Agrupar por:</label>
                            <select name="o_order" id="o_order"  class="smallInput" title="Ordenar por" style="width:90px; float:left; margin-bottom: 5px ;" >
                                <option value="0">Campo</option>
                            </select>
                        </div>
                        <div class="form-administrar2" style="width:190px;"><label for="f_fecha_recibido_h" style="width:120px;">Nombre:</label>
                            <input type="text" name="" id=""  style="width:60px; float:right;" value="" class="form-contacto-text smallInput"/>
                        </div>
                    </div>
                    <div style="width:90px; margin-right:15px; display:block; float:left;">
                        <div class="form-administrar2" style="width:190px;"><label for="o_order" style="width:100px;">Ordenar:</label>
                            <select name="o_order" id="o_order"  class="smallInput" title="Ordenar por" style="width:90px; float:left; margin-bottom: 5px ;" >
                                <option value="0">Ascendente</option>
                                <option value="0">Descendente</option>
                            </select>
                        </div>
                        <div class="form-administrar2" style="width:190px;"><label for="o_order" style="width:100px;">Ordenar por:</label>
                            <select name="o_order" id="o_order"  class="smallInput" title="Ordenar por" style="width:90px; float:left; margin-bottom: 5px ;" >
                                <option value="0">Campo</option>
                            </select>
                        </div>
                        <div class="form-administrar2" style="width:190px;"><label for="f_fecha_recibido_h" style="width:120px;">Nro. Ticket:</label>
                            <input type="text" name="" id=""  style="width:60px; float:right;" value="" class="form-contacto-text smallInput"/>
                        </div>
                    </div>
                </div>
            </div>
            <div style="width:900px; display:block; float:left; clear: both; padding-left: 15px; border-bottom: 1px solid #DDDDDD;border-top: 1px solid #DDDDDD;margin-bottom: 5px;" >
                <div style="float: left; padding: 0pt 15px 0 0; margin: 5px 0pt; height: 24px;">
                    <span style="font-size: 13px; font-weight: bold;">Totales:</span>
                </div>
                <div style="border-right: 2px solid #CCCCCC; float: left; padding: 0pt 15px; margin: 5px 0pt;">
                    <select name="totales_1_func"  class="smallInput" style="width:80px; float:left; margin-right: 5px;" >
                        <option value="0" selected="selected">Funci&oacute;n</option><option value="1" >Cuenta</option><option value="2" >Suma</option><option value="3" >Promedio</option><option value="4" >Mayor</option><option value="5" >Menor</option>                                </select>
                    <select name="totales_1_cam"  class="smallInput" style="width:80px; float:left;" >
                        <option value="0" selected="selected">Campo</option><option value="1" >Suma Asegurada</option><option value="2" >Prima Inicial</option><option value="3" >Prima Anual/option><option value="4" >Prima Target</option><option value="5" >Prima Adic.</option><option value="6" >Prima &Uacute;nica</option>                                </select>
                </div>
                <div style="border-right: 2px solid #CCCCCC; float: left; padding: 0pt 15px; margin: 5px 0pt;">
                    <select name="totales_2_func"  class="smallInput" style="width:80px; float:left; margin-right: 5px;" >
                        <option value="0" selected="selected">Funci&oacute;n</option><option value="1" >Cuenta</option><option value="2" >Suma</option><option value="3" >Promedio</option><option value="4" >Mayor</option><option value="5" >Menor</option>                                </select>
                    <select name="totales_2_cam"  class="smallInput" style="width:80px; float:left;" >
                        <option value="0" selected="selected">Campo</option><option value="1" >Suma Asegurada</option><option value="2" >Prima Inicial</option><option value="3" >Prima Anual</option><option value="4" >Prima Target</option><option value="5" >Prima Adic.</option><option value="6" >Prima &Uacute;nica</option>                                </select>
                </div>
                <div style="border-right: 2px solid #CCCCCC; float: left; padding: 0pt 15px; margin: 5px 0pt;">
                    <select name="totales_3_func"  class="smallInput" style="width:80px; float:left; margin-right: 5px;" >
                        <option value="0" selected="selected">Funci&oacute;n</option><option value="1" >Cuenta</option><option value="2" >Suma</option><option value="3" >Promedio</option><option value="4" >Mayor</option><option value="5" >Menor</option>                                </select>
                    <select name="totales_3_cam"  class="smallInput" style="width:80px; float:left;" >
                        <option value="0" selected="selected">Campo</option><option value="1" >Suma Asegurada</option><option value="2" >Prima Inicial</option><option value="3" >Prima Anual</option><option value="4" >Prima Target</option><option value="5" >Prima Adic.</option><option value="6" >Prima &Uacute;nica</option>                                </select>
                </div>
                <div style="float: left; padding: 0pt 15px; margin: 5px 0pt;">
                    <select name="totales_4_func"  class="smallInput" style="width:80px; float:left; margin-right: 5px;" >
                        <option value="0" selected="selected">Funci&oacute;n</option><option value="1" >Cuenta</option><option value="2" >Suma</option><option value="3" >Promedio</option><option value="4" >Mayor</option><option value="5" >Menor</option>                                </select>
                    <select name="totales_4_cam"  class="smallInput" style="width:80px; float:left;" >
                        <option value="0" selected="selected">Campo</option><option value="1" >Suma Asegurada</option><option value="2" >Prima Inicial</option><option value="3" >Prima Anual</option><option value="4" >Prima Target</option><option value="5" >Prima Adic.</option><option value="6" >Prima &Uacute;nica</option>                                </select>
                </div>
            </div>
            <input type="hidden" name="nombre_reporte" value="" />
            <input type="hidden" name="id_reporte" value="0" />
            <input type="hidden" name="cot_cotizar" value="0" />
            <div id="serialized-report" style="display: none;"></div>
            <div id="nombre-dialog" style="display: none;">
                <label for="tmp_reporte" style="width:70px;">Nombre:</label>
                <input type="text" name="tmp_reporte" id="tmp_reporte" style="width:200px;" value="" class="form-contacto-text smallInput"/>
            </div>
            <div id="nombrehist-dialog" style="display: none;">
                <label for="tmp_hist" style="width:70px;">Nombre:</label>
                <input type="text" name="tmp_hist" id="tmp_hist" style="width:200px;" value="" class="form-contacto-text smallInput"/>
            </div>
            <div id="reportes-dialog" style="display: none;"></div>
            <div id="historiales-dialog" style="display: none;"></div>
            <div id="produccion-dialog" style="display: none;"></div>

        </div>
    </form>
</div>
