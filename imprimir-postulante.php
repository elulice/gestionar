<?php
	session_start();
	include("clases/framework-1.0/class.bd.php");
	include("includes/funciones.php");
?>

<?php

	$m_lIDRegistro = is_numeric($_REQUEST["idpostulante"]) ? $_REQUEST["idpostulante"] : 0;

	if($m_lIDRegistro > 0)
	{
		$sSQL = "SELECT p.PerApellido, p.PerNombres, p.PerTipoDoc, p.PerDocumento, p.PerFechaNac, ";
		$sSQL .= "p.PerCuitCuil, p.PerNumeroCC, p.PerNacionalidad, ec.nombre AS PerECivil, ";
		$sSQL .= "p.PerTelefono, p.PerCelular, p.PerTelMensaje, p.PerEmail, s.nombre AS PerSexo, ";
		$sSQL .= "p.PerHijos, p.PerAcargo, p.PerCodPos, p.PerDomicilio, ";
		$sSQL .= "p.PerBarrio, ";
		$sSQL .= "p.PerPais, PerProvincia, PerLocalidad, ";
		$sSQL .= "PerAceptaEve, PerDispViaje, PerDispCiudad, ";
		$sSQL .= "PerPrefLaboral,PerAspPuesto, ";
		$sSQL .= "p.PerIngles, nih.nombre AS InglesHabla, nie.nombre AS InglesEscribe  ";

		$sSQL .= "FROM persona p ";
		$sSQL .= "INNER JOIN _sexos s ON p.PerSexo = s.idsexo ";
		$sSQL .= "LEFT JOIN _estadocivil ec ON p.PerECivil = ec.idestadocivil ";
		$sSQL .= "LEFT JOIN _nivel_idioma nih ON nih.idnivel = PerInglesHabla ";
		$sSQL .= "LEFT JOIN _nivel_idioma nie ON nie.idnivel = PerInglesEscribe ";
		
		$sSQL .= "WHERE PerNro = " . $m_lIDRegistro;
		
		$cBD = new BD();
		$aRegistro = $cBD->Seleccionar($sSQL, true);
	}
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?php print SITIO; ?></title>
<link href="estilos/imprimir.css" rel="stylesheet" type="text/css">
</head>

<body onLoad="javascript:window.print();">

<table width="800" border="0" cellspacing="0" cellpadding="5" align="center">
            <tr>
              <td height="60" colspan="2"><h3 style="text-transform:capitalize;"><?php print($aRegistro["PerApellido"]); ?>, <?php print($aRegistro["PerNombres"]); ?></h3></td>
  </tr>
            <tr>
              <td colspan="2"><h5 class="borde">DATOS PERSONALES </h5></td>
  </tr>
          <tr>
            <td width="50%"><strong>Documento:</strong> <?php print($aRegistro["PerTipoDoc"]); ?> <?php print($aRegistro["PerDocumento"]); ?></td>
            <td width="340"><strong>Pa&iacute;s:</strong> <?php print $aRegistro["PerPais"]; ?></td>
          </tr>
          <tr>
            <td><strong><?php print($aRegistro["PerCuitCuil"]); ?>:</strong> <?php print($aRegistro["PerNumeroCC"]); ?></td>
            <td><strong>Provincia:</strong> <?php print $aRegistro["PerProvincia"]; ?></td>
          </tr>
          <tr>
            <td><strong>Fecha de Nacimiento: </strong><?php print(date("d-m-Y", strtotime($aRegistro["PerFechaNac"]))); ?></td>
            <td><strong>Localidad:</strong> <?php print $aRegistro["PerLocalidad"]; ?> (<?php print $aRegistro["PerCodPos"]; ?>)</td>
          </tr>
          <tr>
            <td><strong>Sexo:</strong> <?php print $aRegistro["PerSexo"]; ?></td>
            <td valign="top"><strong>Domicilio:</strong> <?php print $aRegistro["PerDomicilio"]; ?></td>
          </tr>
          <tr>
            <td><strong>Nacionalidad:</strong> <?php  print $aRegistro["PerNacionalidad"]; ?></td>
            <td><strong>Barrio:</strong> <?php ($aRegistro["PerBarrio"]) ? print $aRegistro["PerBarrio"] : print NODATA; ?></td>
          </tr>
          <tr>
            <td><strong>Estado Civil:</strong>  <?php print $aRegistro["PerECivil"]; ?></td>
            <td><strong>Telefono:</strong> <?php ($aRegistro["PerTelefono"]) ? print $aRegistro["PerTelefono"] : print NODATA; ?></td>
          </tr>
          <tr>
            <td><strong>Cantidad de hijos:</strong> <?php print $aRegistro["PerHijos"]; ?></td>
            <td><strong>Celular:</strong>
              <?php ($aRegistro["PerCelular"]) ? print $aRegistro["PerCelular"] : print NODATA; ?> / 
			  <strong>Mensajes:</strong>
              <?php ($aRegistro["PerTelMensaje"]) ? print $aRegistro["PerTelMensaje"] : print NODATA; ?></td>
          </tr>
          <tr>
            <td><strong>Personas a cargo: </strong> <?php ($aRegistro["PerAcargo"] == "1") ? print "S&iacute;" : print "No"; ?></td>
            <td valign="top"><strong>Email:</strong> <a href="mailto:<?php print $aRegistro["PerEmail"]; ?>"><?php print $aRegistro["PerEmail"]; ?></a></td>
          </tr>
          <tr>
            <td colspan="2">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="2"><h5 class="borde">ESTUDIOS </h5></td>
          </tr>
<?php 
		  		$lRegistros = 0;
				
			 	$sSQL = "SELECT e.EtuNro, ee.nombre AS EtuEstado, e.EtuTitulo, e.EtuIngreso, e.EtuEgreso, ";
			 	$sSQL .= "e.EtuInstitucion, ne.NEsDescrip, ne.NEsNro ";
				
				$sSQL .= "FROM estudio e ";
				$sSQL .= "LEFT JOIN nivelestudio ne ON e.NEsNro = ne.NEsNro ";
				$sSQL .= "LEFT JOIN _estado_estudio ee ON ee.idestado = e.EtuFinalizado ";
				$sSQL .= "WHERE PerNro = ".$m_lIDRegistro." ";
				$sSQL .= "ORDER BY EtuIngreso DESC ";
									
				$cBD = new BD();
				$oResultadoEstudio = $cBD->Seleccionar($sSQL);
				while($aEstudio = $cBD->RetornarFila($oResultadoEstudio))
				{
		  
?>          <tr>
            <td colspan="2">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr valign="top">
                <td><strong><?php print($aEstudio["EtuTitulo"]); ?></strong> [<?php print($aEstudio["NEsDescrip"]); ?> - <?php print $aEstudio["EtuEstado"]; ?>] [ <?php print(date("d/m/y", strtotime($aEstudio["EtuIngreso"]))); ?> - <?php print(date("d/m/y", strtotime($aEstudio["EtuEgreso"]))); ?>] . <?php print($aEstudio["EtuInstitucion"]); ?></td>
			  </tr>
			</table>			</td>
          </tr>
		  
          <?php
			 		$lRegistros++;
				}
				if($lRegistros == 0)
				{
			 ?>
          <tr>
            <td colspan="2">No hay estudios asociadas a este postulante.</td>
          </tr>
			 <?php } ?>
		  
		  



          <tr>
            <td colspan="2">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="2"><h5 class="borde">IDIOMAS / CONOCIMIENTOS</h5></td>
          </tr>
          <tr valign="top">
            <td><strong>Ingl&eacute;s:</strong> 
			<?php 	if (!$aRegistro["PerIngles"])
					{
						print "No";
					} else {
						print "Si &nbsp;&nbsp; (Habla: ".$aRegistro["InglesHabla"];
						print " - Escribe: ". $aRegistro["InglesEscribe"].")";	
					}		
			?>

			<?php 	if(strlen($aRegistro["PerIdiomas"]) > 0)
					{
			?>
					<br><strong>Otros Idiomas: </strong>
					<?php ($aRegistro["PerIdiomas"]); 
					} else
						print "&nbsp;"
					?>
			</td>
            <td>
			<?php 	if(strlen($aRegistro["PerOtrosConoc"]) > 0)
					{
			?>
					<strong>Otros Conocimientos: </strong>
					<?php ($aRegistro["PerOtrosConoc"]); 
					} else
						print "&nbsp;"
					?>
			</td>
          </tr>



          <tr>
            <td colspan="2">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="2"><h5 class="borde">EXPERIENCIA LABORAL</h5></td>
          </tr>
<?php 
		  		$lRegistros = 0;
			 	$sSQL = "SELECT exp.PueUltimaEmpresa, pue.PueNom, ";
			 	$sSQL .= "a.AreNom, exp.PueFechaBaja, exp.PueCausaBaja, exp.PueDescPuesto ";
				$sSQL .= "FROM datospuesto exp ";
				$sSQL .= "INNER JOIN puesto pue ON pue.PueNro = exp.PueNro ";
				$sSQL .= "INNER JOIN area a ON a.AreNro = exp.AreNro ";
				$sSQL .= "WHERE exp.PerNro = ".$m_lIDRegistro." ";
				$sSQL .= "ORDER BY PueFechaBaja DESC ";
									
									
									
				$cBD = new BD();
				$oResultadoEstudio = $cBD->Seleccionar($sSQL);
				while($aEstudio = $cBD->RetornarFila($oResultadoEstudio))
				{
		  
?>          <tr>
            <td colspan="2">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr valign="top">
                <td width="50%"><strong><?php print($aEstudio["PueNom"]); ?>.</strong> <?php print($aEstudio["PueUltimaEmpresa"]); ?>, <?php print($aEstudio["AreNom"]); ?>.<br>
				Egreso: </strong><?php print(date("m/Y", strtotime($aEstudio["PueFechaBaja"]))); ?>. Motivo: <?php print($aEstudio["PueCausaBaja"]); ?></td>
                <td>Tareas realizadas:<br>
				<?php print($aEstudio["PueDescPuesto"]); ?></td>
			  </tr>
			</table>			</td>
          </tr>
		  
          <?php
			 		$lRegistros++;
				}
				if($lRegistros == 0)
				{ 
			 ?>
          <tr>
            <td colspan="2">No hay experiencias asociadas a este postulante.</td>
          </tr>
			 <?php } ?>
		  
          <tr>
            <td colspan="2">&nbsp;</td>
          </tr>		  
        </table>

</body>
</html>
