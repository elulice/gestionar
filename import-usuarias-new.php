<?php

ini_set('max_execution_time', 0);
error_reporting(E_ALL);
ini_set('display_errors', '1');
# te paso los pasos para conectarte por PHP via ODBC a nuestro SQL.
# 1 - conecto con el DSN "SQLGestion" con nuevo usuario y password.
$connect = odbc_connect("SQLGestion", "gestiondbconsultas", "gconsultas");

# 2 - armo la tabla empresa para los campos numero y razonsocial
$query = "select
                            EA.EmpRSocial
                            , UOP.UniNombre AS UOP
                            , Empresa.EmpNro
                            , Empresa.EmpRSocial
                            , Empresa.EmpCUIT
                            , max(IsNull(Comunicacion.ComValor, '')) as  email
                        from
                            Empresa
                            inner join Organigrama
                            inner join Nodo
                            inner join UnidadOrg
                                on UnidadOrg.UniNro = Nodo.UniNro
                                on Nodo.OrgNro = Organigrama.OrgNro
                                on Organigrama.EmpNro = Empresa.EmpNro
                                    and Organigrama.OrgPrincipal = 1
                            inner join RelEntreEmpr
                            inner join Empresa EA
                                on EA.EmpNro = RelEntreEmpr.EmpNro
                                on RelEntreEmpr.TieneEmprNro = Empresa.EmpNro
                                    and RelEntreEmpr.RelTipo = 2
                            inner join Organigrama OP
                            inner join Nodo NP
                            inner join UnidadOrg UOP
                                on UOP.UniNro=NP.UniNro
                                on NP.OrgNro = OP.OrgNro
                                on OP.EmpNro = EA.EmpNro
                                    and OP.OrgPrincipal = 1
                            inner join RelEntreNodos
                                on RelEntreNodos.NodNro = Nodo.NodNro
                                    and RelEntreNodos.MiNodNro = NP.NodNro
                            left join Comunicacion
                            inner join ParametrosGenerales
                                on ParametrosGenerales.IdEmail = Comunicacion.MComNro
                                on comunicacion.EmpNro = Empresa.EmpNro
                        where
                            EA.EmpNro in (17, 271,398,4311,6247,6906,8148)  --IDs de Empresas Administradas
                            and REEFechFin = ''
                        group by
                            EA.EmpRSocial
                            , UOP.UniNombre
                            , Empresa.EmpNro
                            , Empresa.EmpRSocial
                            , Empresa.EmpCUIT
                            , Comunicacion.ComValor
                        order by
                            EA.EmpRSocial
                            , UOP.UniNombre
                            , Empresa.EmpRSocial";

// Nodo.NodNro IN (7562, 11501, 11500, 11870, 11499, 12162) AND  MiNodo.NodNro IN (8125, 2735)
# 3 - ejecuto la consulta
$result = odbc_exec($connect, $query);

odbc_result_all($result);
die();

function formatDate($date) {
    $date = trim($date);
    if (!empty($date)) {
        $y = substr($date, 0, 4);
        $m = substr($date, 4, 2);
        $d = substr($date, 6, 2);
        return $y . '-' . $m . '-' . $d;
    } else {
        return NULL;
    }
}

function rand_string($length) {
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    $size = strlen($chars);
    $str = '';
    for ($i = 0; $i < $length; $i++) {
        $str .= $chars[rand(0, $size - 1)];
    }

    return $str;
}

echo '<pre>';
if ($result !== FALSE) {
    $areas = array();
    $puestos = array();

    #4 - Limpio la tabla de Clientes
    $conectID2 = mysql_connect('localhost', 'root', '');
    mysql_select_db('consultores');
    $res = mysql_query('TRUNCATE TABLE cliente', $conectID2);

    # 5 - guardo la data en una estructura repetitiva y actualiza la base de datos.
    while ($row = mssql_fetch_assoc($result)) {
        //print_r($row);
        $aux = mysql_query("SELECT COUNT(*) AS nro FROM cliente WHERE EmpNro = " . $row['EmpNro'], $conectID2);
        $existe = mysql_fetch_assoc($aux);
        if ($existe['nro'] == 0) {
            $consulta = "INSERT INTO
                        cliente
                            (CliNro
                            , CliRsocial
                            , CliEMailEjCta
                            , EmpNro
                            , CliCUIT
                            , CliUop
                            , CliClave)
                    VALUES
                        ('" . $row['EmpNro'] . "'
                        , '" . $row['EmpRSocial'] . "'
                        , '" . $row['email'] . "'
                        , '" . $row['EmpNro'] . "'
                        , '" . $row['EmpCUIT'] . "'
                        , '" . $row['UOP'] . "'
                        , '')";
            mysql_query($consulta, $conectID2);
        }
    }
}
# 6 - destruyo la conexion
odbc_close($connect);
?>
