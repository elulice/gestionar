<link href="estilos/general.css" rel="stylesheet" type="text/css" />
<table width="780" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="30" class="encabezado-titulo-bg"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="20"><img src="images/espacio.gif" width="1" height="1" /></td>
        <td valign="top" class="encabezado-titulo-texto" style="padding-top:5px;">Agenda de Postulantes </td>
        </tr>
    </table></td>
  </tr>
  <tr>
    <td><img src="images/espacio.gif" width="1" height="20"></td>
  </tr>
</table>


<?php include("inc.buscador-agenda.php"); ?>

<table width="870" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="20"><img src="images/listado-encabezado-inicio.jpg" width="20" height="37"></td>
        <td class="listado-encabezado-bg"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="180" class="listado-encabezado-texto">Apellido y Nombre</td>
            <td width="100" class="listado-encabezado-texto">Cuil</td>
            <td width="120" class="listado-encabezado-texto">Pedido</td>
            <td width="180" class="listado-encabezado-texto">Usuaria</td>
            <td width="100" class="listado-encabezado-texto">Situaci&oacute;n</td>
            <td width="70" class="listado-encabezado-texto">Fecha</td>
            <td width="50" class="listado-encabezado-texto">Hora</td>
            <td class="listado-encabezado-texto">Notas</td>
          </tr>
        </table></td>
        <td width="20"><img src="images/listado-encabezado-final.jpg" width="20" height="37"></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="5" class="listado-contenido-inicio"><img src="images/espacio.gif" width="1" height="1"></td>
        <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
		  <?php
		  		$lRegistros = 0;
			 	$sSQL = "SELECT p.PerNro, p.PerApellido, p.PerNombres, p.PerNumeroCC, ";
			 	$sSQL .= "pe.PEsDescrip, ph.PosEstFecha, c.CliRSocial, pu.PueNom ";

				$sSQL .= "FROM postulantenotas np ";
				$sSQL .= "INNER JOIN persona p ON p.PerNro = np.PerNro ";
				$sSQL .= "LEFT JOIN postulacionhistorico ph ON ph.PerNro = p.PerNro ";
				$sSQL .= "LEFT JOIN postulacionestado pe ON pe.PEsNro = ph.PEsNro ";
				$sSQL .= "INNER JOIN oferta o ON o.OfeNro = ph.OfeNro ";
				$sSQL .= "INNER JOIN ofertascliente oc ON oc.OfeNro = o.OfeNro ";
				$sSQL .= "INNER JOIN cliente c ON c.CliNro = oc.CliNro ";
				$sSQL .= "INNER JOIN puesto pu ON pu.PueNro = o.PueNro ";
				$sSQL .= "INNER JOIN estadooferta oe ON oe.OfeNro = o.OfeNro ";
				$sSQL .= "LEFT JOIN miembroempresa me ON me.MEmpNro = o.MEmpNro ";
				
				$sSQL .= "WHERE 1 ";

				if ($bCliente <> "")
					$sSQL .= "AND c.CliRSocial LIKE '%". $bClientePart ."%' ";
				if ($bIdCliente > 0)
					$sSQL .= "AND c.CliNro = ". $bIdCliente ." ";
				
				if ($bFechaDesde <> "")
				{				
					$bFechaDesde = explode("-", $bFechaDesde);
					$sFecha = $bFechaDesde[2]."-".$bFechaDesde[1]."-".$bFechaDesde[0];
					$sSQL .= "AND ph.PosEstFecha >= '". date("Y-m-d", strtotime($sFecha)) ."' ";
				}
				if ($bFechaHasta <> "")
				{
					$bFechaHasta = explode("-", $bFechaHasta);
					$sFecha = $bFechaHasta[2]."-".$bFechaHasta[1]."-".$bFechaHasta[0];
					$sSQL .= "AND ph.PosEstFecha <= '". date("Y-m-d", strtotime($sFecha)) ."' ";
				}
				if ($bEstadoPost > 0)
					$sSQL .= "AND pe.PEsNro = ". $bEstadoPost ." ";
				if ($bEstadoOf > 0)
					$sSQL .= "AND oe.EOfeValor = '". $bEstadoOf ."' ";
				if ($bSelector > 0)
					$sSQL .= "AND oe.MEmpNro = '". $bSelector ."' ";

//echo $sSQL;

				$cBD = new BD();
				$oResultado = $cBD->Seleccionar($sSQL);
				while($aRegistro = $cBD->RetornarFila($oResultado))
				{
					$sPosicion = (($sPosicion == "1") ? "2" : "1");
					
		  ?>
          <tr>
            <td class="listado-fila-bg-<?php print($sPosicion); ?>">
			
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr class="listado-texto-chico">
                <td width="15"><img src="images/espacio.gif" width="1" height="1"></td>
                <td width="180"><?php print($aRegistro["PerApellido"]); ?>, <?php print($aRegistro["PerNombres"]); ?></td>
                <td width="100"><?php print($aRegistro["PerNumeroCC"]); ?></td>
                <td width="120"><?php print($aRegistro["PueNom"]); ?></td>
                <td width="180"><?php print($aRegistro["CliRSocial"]); ?></td>
                <td width="100"><?php print($aRegistro["PEsDescrip"]); ?></td>
                <td width="70"><?php print(date("d/m/y", strtotime($aRegistro["PosEstFecha"]))); ?></td>
                <td width="50"><?php print(date("H:i", strtotime($aRegistro["PosEstFecha"]))); ?></td>
                <td><a href="am-agenda.php?idregistro=<?php print($aRegistro["PerNro"]); ?>&url=<?php print($m_sURL); ?>"><img src="images/btn-ver-mas.png" alt="Ver Notas sobre el Postulante" width="23" height="23" border="0"></a></td>
              </tr>
			  </table></td>
          </tr>
          <?php
			 		$lRegistros++;
				}
				if($lRegistros == 0)
				{
			 ?>
          <tr>
            <td><img src="images/espacio.gif" width="1" height="20"></td>
          </tr>
			 <?php } ?>
        </table></td>
        <td width="6" class="listado-contenido-final"><img src="images/espacio.gif" width="1" height="1"></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="100"><img src="images/listado-pie-inicio-deshabilitado.jpg" alt="Agregar" width="100" height="40" border="0"></td>
        <td class="listado-pie-bg">&nbsp;</td>
        <td width="20"><img src="images/listado-pie-final.jpg" width="20" height="40"></td>
      </tr>
    </table></td>
  </tr>
</table>
