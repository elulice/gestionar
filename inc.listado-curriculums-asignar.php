<link href="estilos/general.css" rel="stylesheet" type="text/css" />
<table width="780" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="30" class="encabezado-titulo-bg"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="20" valign="top"><img src="images/espacio.gif" width="1" height="1" /></td>
        <td width="130" valign="top" class="encabezado-titulo-texto">Pedidos </td>
        <td width="610" valign="top" class="encabezado-titulo-texto">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><img src="images/espacio.gif" width="1" height="10"></td>
  </tr>
  <tr>
    <td class="formulario-textbox">Seleccione mediante los filtros la oferta laboral de su interes</td>
  </tr>
</table>



<form action="listado-curriculums-asignar.php" method="get" name="frmFiltro" id="frmFiltro">
<input name="idpostulante" id="idpostulante" type="hidden" value="<?php print $m_lIDPostulante; ?>" />
<input name="filtrar" id="filtrar" type="hidden" value="1" />
<table border="0" align="center" cellpadding="0" cellspacing="0" style="margin-bottom:10px;" class="buscar">

<?php include("inc.buscador-ofertas.php"); ?>
</table>
</form>

<?php 
  if ($_GET["filtrar"])
  {
?>
<table width="870" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="20"><img src="images/listado-encabezado-inicio.jpg" width="20" height="37"></td>
        <td class="listado-encabezado-bg"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="70"><img src="images/espacio.gif" width="1" height="1"></td>
            <td width="75" class="listado-encabezado-texto">Fecha</td>
            <td width="200" class="listado-encabezado-texto">Usuaria</td>
            <td width="220" class="listado-encabezado-texto">Puesto</td>
            <td width="200" class="listado-encabezado-texto">&Aacute;rea</td>
            <td class="listado-encabezado-texto">Situaci&oacute;n</td>
          </tr>
        </table></td>
        <td width="20"><img src="images/listado-encabezado-final.jpg" width="20" height="37"></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="5" class="listado-contenido-inicio"><img src="images/espacio.gif" width="1" height="1"></td>
        <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
		  <?php
		  		$lRegistros = 0;
				
			 	$sSQL = "SELECT oferta.OfeNro, PueNom, OfeFechAlta, EOfeValor, ";
			 	$sSQL .= "OfeReferencia, OfeCantPost, OfeLocalidad, ";
				$sSQL .= "AreNom, CliRSocial  ";
				
				$sSQL .= "FROM oferta ";
				$sSQL .= "INNER JOIN puesto ON oferta.PueNro = puesto.PueNro ";
				$sSQL .= "INNER JOIN estadooferta ON estadooferta.OfeNro = oferta.OfeNro ";
				$sSQL .= "INNER JOIN area ON area.AreNro = oferta.AreNro ";
				$sSQL .= "INNER JOIN ofertascliente oc ON oc.OfeNro = oferta.OfeNro  ";
				$sSQL .= "INNER JOIN cliente ON oc.CliNro = cliente.CliNro ";

				$sFiltro .= "WHERE 1 ";
				//$sFiltro .= "AND p.idpropuesta = po.idpropuesta ";
			
				if ($bCliente <> "")
					$sFiltro .= "AND CliRsocial LIKE '%". $bCliente ."%' ";
				if ($bIdCliente <> "")
					$sFiltro .= "AND cliente.CliNro =". $bIdCliente ." ";
					
				if ($bReferencia <> "")
					$sFiltro .= "AND OfeReferencia LIKE '%". $bReferencia ."%' ";
				if ($bFechaDesde <> "")
				{				
					$bFechaDesde = explode("-", $bFechaDesde);
					$sFecha = $bFechaDesde[2]."-".$bFechaDesde[1]."-".$bFechaDesde[0];
					$sFiltro .= "AND OfeFechAlta >= '". date("Y-m-d", strtotime($sFecha)) ."' ";
				}
				if ($bFechaHasta <> "")
				{
					$bFechaHasta = explode("-", $bFechaHasta);
					$sFecha = $bFechaHasta[2]."-".$bFechaHasta[1]."-".$bFechaHasta[0];
					$sFiltro .= "AND OfeFechAlta <= '". date("Y-m-d", strtotime($sFecha)) ."' ";
				}

				
				$sSQL .= $sFiltro . "ORDER BY OfeFechAlta DESC, OfeNro DESC ";
							
				//print $sSQL;
					
				$cBD = new BD();
				$oResultado = $cBD->Seleccionar($sSQL);
				while($aRegistro = $cBD->RetornarFila($oResultado))
				{
					$sPosicion = (($sPosicion == "1") ? "2" : "1");
		  ?>


          <tr>
            <td class="listado-fila-bg-<?php print($sPosicion); ?>"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr class="listado-texto-chico">
                <td width="15"><img src="images/espacio.gif" width="1" height="1"></td>
                <td width="70"><table width="70" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="30"><a href="javascript:verPostulados(<?php print($aRegistro["OfeNro"]); ?>, <?php print($sPosicion); ?>)"><img src="images/btn-ver-mas.png" alt="Ver Candidatos" width="23" height="23" border="0"></a></td>

                    <td width="30"><a href="javascript:asociarPostulante(<?php print($aRegistro["OfeNro"]); ?>,<?php print $m_lIDPostulante; ?>);verPostulados(<?php print($aRegistro["OfeNro"]); ?>, <?php print($sPosicion); ?>);verPostulados(<?php print($aRegistro["OfeNro"]); ?>, <?php print($sPosicion); ?>)"  onclick="return confirm('&iquest;Desea asignar este Postulante a esta oferta?')"><img src="images/btn-adjuntar.png" alt="Postular" width="23" height="23" border="0"></a></td>
                  </tr>
                </table></td>
                <td width="75"><?php print(date("d/m/Y", strtotime($aRegistro["OfeFechAlta"]))); ?></td>
                <td width="200"><?php print($aRegistro["CliRSocial"]); ?></td>
                <td width="220"><?php print($aRegistro["PueNom"]); ?></td>
                <td width="200"><?php print($aRegistro["AreNom"]); ?></td>
                <td><?php print($aRegistro["EOfeValor"]); ?></td>
              </tr>

              <tr id="trOferta-<?php print($aRegistro["OfeNro"]); ?>" class="listado-fila-oculta">
                <td colspan="7" id="tdOferta-<?php print($aRegistro["OfeNro"]); ?>" class="informe-separador" style="padding: 5px 0;">&nbsp;</td>
              </tr>
		  
            </table></td>
          </tr>


          <?php
			 		$lRegistros++;
				}
				if($lRegistros == 0)
				{
			 ?>
          <tr>
            <td><img src="images/espacio.gif" width="1" height="20"></td>
          </tr>
			 <?php } ?>
        </table></td>
        <td width="6" class="listado-contenido-final"><img src="images/espacio.gif" width="1" height="1"></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="100"><img src="images/listado-pie-inicio-deshabilitado.jpg" width="100" height="40" border="0"></td>
        <td class="listado-pie-bg">&nbsp;</td>
        <td width="20"><img src="images/listado-pie-final.jpg" width="20" height="40"></td>
      </tr>
    </table></td>
  </tr>
</table>
<?php 			}
?>
