<link href="estilos/general.css" rel="stylesheet" type="text/css" />
<table width="780" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="30" class="encabezado-titulo-bg"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="20"><img src="images/espacio.gif" width="1" height="1" /></td>
        <td width="130" class="encabezado-titulo-texto">Listado de Usuarias </td>
        <td width="610" align="right" class="encabezado-titulo-texto"><a href="am-clientes.php?idregistro=0&amp;url=<?php print($m_sURL); ?>"><img src="images/btn-agregar.jpg" width="80" height="20" border="0" /></a> </td>
        <td width="10" class="encabezado-titulo-texto">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><img src="images/espacio.gif" width="1" height="20"></td>
  </tr>
</table>



<?php
	$bCliente = ($_GET["cliente"] ? $_GET["cliente"] : "");
	$bEjecutivo = ($_GET["ejecutivo"] ? $_GET["ejecutivo"] : "");
?>

<form action="" method="get" name="frmFiltro" id="frmFiltro">

<table border="0" align="center" cellpadding="0" cellspacing="0" style="margin-bottom:10px;" class="buscar">
<tr align="left">
	<td class="encabezado-formulario">Usuaria:</td>
	<td><input name="cliente" type="text" class="formulario-textbox" id="cliente" style="width: 260px;" value="<?php print $bCliente; ?>" /></td>
	<td class="encabezado-formulario">Ejecutivo de cuenta: </td>
    <td class="encabezado-formulario"><select name="ejecutivo" class="formulario-textbox" id="ejecutivo" style="width: 180px;">
              <?php
					$sSQL = "SELECT MEmpNro, CONCAT(MEmpNombres, \" \", MEmpApellido) ";
					$sSQL .= "FROM miembroempresa WHERE MEmpAdmin = 2 ";
					print(GenerarOptions($sSQL, $bEjecutivo, true, DEFSELECT));
			  ?>
    </select></td>
</tr>
<tr align="left">
	<td colspan="4" align="right" class="encabezado-formulario">
      <input name="btnFiltrar" type="image" id="btnFiltrar" src="images/btn-buscar.jpg" alt="Filtrar" />    </td>
	</tr>
  </table>
</form>


<table width="880" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="20"><img src="images/listado-encabezado-inicio.jpg" width="20" height="37"></td>
        <td class="listado-encabezado-bg"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="130"><img src="images/espacio.gif" width="1" height="1"></td>
            <td width="220" class="listado-encabezado-texto">Raz&oacute;n social </td>
            <td width="150" class="listado-encabezado-texto">Contacto</td>
            <td width="100" class="listado-encabezado-texto">Tel&eacute;fono</td>
            <td width="150" class="listado-encabezado-texto">Ejecutivo de Cuenta</td>
            <td class="listado-encabezado-texto">ID</td>
            </tr>
        </table></td>
        <td width="20"><img src="images/listado-encabezado-final.jpg" width="20" height="37"></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="5" class="listado-contenido-inicio"><img src="images/espacio.gif" width="1" height="1"></td>
        <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
		  <?php
		  		$lRegistros = 0;
			 	$sSQL = "SELECT cliente.CliNro AS idcliente, CliRsocial, CliContacto, CliEjCta, CliTelefono ";
				$sSQL .= "FROM cliente LEFT JOIN clienteejcta cej ON cliente.CliNro = cej.CliNro ";
				$sSQL .= "WHERE EmpNro = ".RetornarIdEmpresa()." ";


				if (strlen($bCliente) > 0)
					$sSQL .= "AND CliRsocial LIKE '%". $bCliente ."%' ";
				if ($bEjecutivo)
					$sSQL .= "AND cej.MEmpNro = ". $bEjecutivo ." ";
				
				$sSQL .= "ORDER BY CliRsocial ";

				$cBD = new BD();
				$cBD->Conectar();
				$paging = new PHPPaging($cBD->RetornarConexion());
				$paging->agregarConsulta($sSQL);
				$paging->ejecutar();
				
				while($aRegistro = $paging->fetchResultado())
				{
					$sPosicion = (($sPosicion == "1") ? "2" : "1");
		  ?>
          <tr>
            <td class="listado-fila-bg-<?php print($sPosicion); ?>">
			
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr class="listado-texto-chico">
                <td width="15"><img src="images/espacio.gif" width="1" height="1"></td>
                <td width="130">
				
				<table width="130" border="0" cellspacing="0" cellpadding="0">
                  <tr>
				  
                    <td width="30"><a href="javascript:verCliente(<?php print($aRegistro["idcliente"]); ?>)"><img src="images/btn-ver-mas.png" alt="Vista R&aacute;pida" width="23" height="23" border="0"></a></td>
                    <td width="30"><a href="am-clientes.php?idregistro=<?php print($aRegistro["idcliente"]); ?>&url=<?php print($m_sURL); ?>"><img src="images/btn-modificar-<?php print($sPosicion); ?>.jpg" alt="Modificar" width="24" height="23" border="0"></a></td>

                    <td><a href="abm.php?tabla=cliente&columna=CliNro&idregistro=<?php print($aRegistro["idcliente"]); ?>&url=<?php print($m_sURL); ?>" onclick="return confirm('&iquest;Desea eliminar este Cliente?')"><img src="images/btn-eliminar-<?php print($sPosicion); ?>.jpg" alt="Eliminar" width="24" height="23" border="0"></a></td>
                    <td>
					<?php
						$sArchivo = "../clientes/".$aRegistro["idcliente"].".xls";
						if (file_exists($sArchivo)) {
					?>
					<a href="<?php print($sArchivo); ?>" target="_blank"><img src="images/btn-adjuntar.png" alt="Ver archivo del cliente" width="23" height="23" border="0"></a>
					<?php } else print "&nbsp;"; ?>					</td>
                  </tr>
                </table>				</td>
                <td width="220"><?php print($aRegistro["CliRsocial"]); ?></td>
                <td width="150"><?php print($aRegistro["CliContacto"]); ?></td>
                <td width="100"><?php print($aRegistro["CliTelefono"]); ?></td>
                <td width="150"><?php print($aRegistro["CliEjCta"]); ?></td>
                <td width="50"><?php print($aRegistro["idcliente"]); ?></td>
                <td align="center">&nbsp;</td>
              </tr>
			  
              <tr id="trCliente-<?php print($aRegistro["idcliente"]); ?>" class="listado-fila-oculta">
                <td colspan="8" id="tdCliente-<?php print($aRegistro["idcliente"]); ?>" class="informe-separador">&nbsp;</td>
              </tr>
            </table></td>
          </tr>
          <?php
			 		$lRegistros++;
				}
				if($lRegistros == 0)
				{
			 ?>
          <tr>
            <td><img src="images/espacio.gif" width="1" height="20"></td>
          </tr>
			 <?php } ?>
        </table></td>
        <td width="6" class="listado-contenido-final"><img src="images/espacio.gif" width="1" height="1"></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="100"><img src="images/listado-pie-inicio-deshabilitado.jpg" alt="Agregar" width="100" height="40" border="0"></td>
        <td class="listado-pie-bg link" align="center"><?php echo $paging->fetchNavegacion();?></td>
        <td width="20"><img src="images/listado-pie-final.jpg" width="20" height="40"></td>
      </tr>
    </table></td>
  </tr>
</table>
