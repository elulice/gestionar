<?php
	//$m_lIDEmpresa = (is_numeric($_GET["idempresa"]) ? $_GET["idempresa"]: 0); 
	
	if($m_lIDRegistro > 0)
	{
		$sSQL = "SELECT o.OfeTitulo, o.OfeFechAlta, o.OfeValidez, O.OfeVisible, ";
		$sSQL .= "oc.CliNro, o.OFePedidoPor, o.OfeDireccion, o.OfeEMail, o.OfeURL, ";
		$sSQL .= "o.OfeReferencia, o.AreNro, o.PueNro, o.ZonNro, ";
		$sSQL .= "o.OfeOferta, o.OfeCantPost, o.TPeNro, ";
		$sSQL .= "o.OfeDeEdad, o.OfeHtaEdad, o.OfeSexo, ";
		$sSQL .= "o.OfeEducacion, o.OfeProvincia, o.OfeLocalidad, ";
		$sSQL .= "o.OfeIngles, o.OfeExpAnios, o.OfeRemunerac, ";
		$sSQL .= "o.OfeDescTar, o.OfeClasificacion, ";
		$sSQL .= "o.OfeVisible, o.OfeDestacada, o.OfeEnviaMail, ";
		$sSQL .= "eo.EOfeValor, o.OfeDescSel, o.NodNro, ";
		$sSQL .= "me.MEmpNro, me.MEmpApellido, me.MEmpNombres, me.MEmpEmail ";
		
		$sSQL .= "FROM oferta o ";
		$sSQL .= "INNER JOIN ofertascliente oc ON o.OfeNro = oc.OfeNro ";
		$sSQL .= "INNER JOIN cliente c ON oc.CliNro = c.CliNro ";
		$sSQL .= "INNER JOIN area a ON o.AreNro = a.AreNro ";
		$sSQL .= "INNER JOIN puesto p ON p.PueNro = o.PueNro ";
		$sSQL .= "LEFT JOIN estadooferta eo ON eo.OfeNro = o.OfeNro ";
		$sSQL .= "INNER JOIN miembroempresa me ON me.MEmpNro = o.MEmpNro ";
		$sSQL .= "WHERE o.OfeNro = " . $m_lIDRegistro;

		$cBD = new BD();
		$aRegistro = $cBD->Seleccionar($sSQL, true);
	} else {
		$aRegistro["OfeFechAlta"] = date("Y-m-d");
		$aRegistro["OfeValidez"] = date("Y-m-d", strtotime("+1 week"));
		$aRegistro["MEmpNro"] = $_SESSION["idmiembro"];
		$aRegistro["MEmpApellido"] = $_SESSION["persona"]["apellido"];
		$aRegistro["MEmpNombres"] = $_SESSION["persona"]["nombres"];
		$aRegistro["MEmpEmail"] = $_SESSION["persona"]["email"];
		$aRegistro["OfeVisible"] = "1";
		$aRegistro["OfeDestacada"] = "0";
		$aRegistro["OfeCantPost"] = "1";
	}
?>
<link href="estilos/general.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
.style1 {font-size: 10px}
-->
</style>


<table width="780" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="30" class="encabezado-titulo-bg"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="20"><img src="images/espacio.gif" width="1" height="1"></td>
        <td class="encabezado-titulo-texto">Alta y modificaci&oacute;n de Pedidos por Clientes</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><img src="images/espacio.gif" width="1" height="20"></td>
  </tr>
</table>
<table width="570" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="20"><img src="images/formulario-encabezado-inicio.jpg" width="20" height="37"></td>
        <td class="formulario-encabezado-bg">Pedido</td>
        <td width="20"><img src="images/formulario-encabezado-final.jpg" width="20" height="37"></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="4" class="formulario-contenido-inicio"><img src="images/espacio.gif" width="1" height="1"></td>
        <td><form action="abm.php?tabla=oferta&columna=OfeNro&idregistro=<?php print($m_lIDRegistro); ?>&archivo=0&url=<?php print($m_sURL); ?>" method="post" name="frmRegistro" id="frmRegistro" onsubmit="corrigeFechas(['OfeFechAlta', 'OfeValidez']);">
 
          <table width="100%" border="0" cellspacing="0" cellpadding="4">
            <tr>
              <td height="30" colspan="4" valign="bottom" class="detalle-seccion">generales</td>
              </tr>
            <tr>
              <td width="170" class="formulario-etiquetas">Fecha de alta:</td>
              <td width="150" class="formulario-textbox"><input name="OfeFechAlta" type="text" class="formulario-textbox" id="OfeFechAlta" style="width: 70px;" maxlenght="10" value="<?php print(date("d-m-Y", strtotime($aRegistro["OfeFechAlta"]))); ?>" />
                <span class="style1">                dd-mm-aaaa </span></td>
              <td width="100" class="formulario-etiquetas">V&aacute;lida hasta:</td>
              <td width="150" class="formulario-textbox"><input name="OfeValidez" type="text" class="formulario-textbox" id="OfeValidez" style="width: 70px;" maxlenght="10" value="<?php print(date("d-m-Y", strtotime($aRegistro["OfeValidez"]))); ?>" />
                <span class="style1">dd-mm-aaaa </span></td>
            </tr>
            <tr>
              <td width="170" class="formulario-etiquetas">Visible:</td>
              <td width="150" class="formulario-textbox"><table width="80" border="0" cellspacing="0" cellpadding="0" class="formulario-textbox">
                <tr>
                  <td width="20"><input name="OfeVisible" type="radio" value="1" <?php if($aRegistro["OfeVisible"] == "1") { ?>checked="checked" <?php } ?>/></td>
                  <td width="20" class="formulario-textbox">Si</td>
                  <td width="20"><input name="OfeVisible" type="radio" value="0" <?php if($aRegistro["OfeVisible"] == "0") { ?>checked="checked" <?php } ?>/></td>
                  <td class="formulario-textbox">No</td>
                </tr>
              </table></td>
              <td width="100" class="formulario-etiquetas">Destacada:</td>
              <td width="150" class="formulario-textbox"><table width="80" border="0" cellspacing="0" cellpadding="0" class="formulario-textbox">
                <tr>
                  <td width="20"><input name="OfeDestacada" type="radio" value="1" <?php if($aRegistro["OfeDestacada"] == "1") { ?>checked="checked" <?php } ?>/></td>
                  <td width="20" class="formulario-textbox">Si</td>
                  <td width="20"><input name="OfeDestacada" type="radio" value="0" <?php if($aRegistro["OfeDestacada"] == "0") { ?>checked="checked" <?php } ?>/></td>
                  <td class="formulario-textbox">No</td>
                </tr>
              </table></td>
            </tr>
            <tr>
              <td class="formulario-etiquetas">Situaci&oacute;n del pedido: </td>
              <td colspan="3"><select name="HIDDEN_ofeEstado" id="HIDDEN_ofeEstado" style="width: 300px;" onchange="habilitarFactura(this);">
                  <?php
					$sSQL = "SELECT nombre, nombre FROM _ofertas_estados  ";
					print(GenerarOptions($sSQL, $aRegistro["EOfeValor"]));
			  ?>
              </select></td>
            </tr>
            <tr>
              <td class="formulario-etiquetas">Factura: </td>
              <td colspan="3"><input name="OfeFactura" type="text" class="formulario-textbox" id="OfeFactura" style="width: 100px;" value="<?php print($aRegistro["OfeFactura"]); ?>" <?php if(($aRegistro["EOfeValor"]) <> "Cumplida") print "disabled"; ?> /></td>
            </tr>
            <tr>
              <td height="30" colspan="4" valign="bottom" class="detalle-seccion">Datos de la usuaria </td>
            </tr>



          <tr>
            <td class="formulario-etiquetas">Empresa:</td>
            <td colspan="3"><select name="HIDDEN_CliNro" id="HIDDEN_CliNro" style="width: 300px;" <?php if ($m_lIDRegistro > 0 ) print "disabled"; ?>>
              <?php
					$sSQL = "SELECT CliNro, CliRSocial FROM cliente  ";
					$sSQL .= "ORDER BY CliRSocial ASC ";
					print(GenerarOptions($sSQL, $aRegistro["CliNro"]));
			  ?>
            </select></td>
          </tr>
          <tr>
            <td class="formulario-etiquetas">Pedido realizado por :</td>
            <td colspan="3"><input name="OFePedidoPor" type="text" class="formulario-textbox" id="OFePedidoPor" style="width: 300px;" maxlenght="64" value="<?php print($aRegistro["OFePedidoPor"]); ?>" /></td>
          </tr>
          <tr>
            <td class="formulario-etiquetas">Direcci&oacute;n:</td>
            <td colspan="3"><input name="OfeDireccion" type="text" class="formulario-textbox" id="OfeDireccion" style="width: 300px;" maxlenght="64" value="<?php print($aRegistro["OfeDireccion"]); ?>" /></td>
          </tr>
          <tr>
            <td class="formulario-etiquetas">Email:</td>
            <td colspan="3"><input name="OfeEMail" type="text" class="formulario-textbox" id="OfeEMail" style="width: 300px;" maxlenght="64" value="<?php print($aRegistro["OfeEMail"]); ?>" /></td>
          </tr>
          <tr>
            <td class="formulario-etiquetas">URL:</td>
            <td colspan="3"><input name="OfeURL" type="text" class="formulario-textbox" id="OfeURL" style="width: 300px;" maxlenght="64" value="<?php print($aRegistro["OfeURL"]); ?>" /></td>
          </tr>



          <tr>
            <td height="30" colspan="4" valign="bottom" class="detalle-seccion">Datos del selector </td>
			<input name="MEmpNro" type="hidden" value="<?php print($aRegistro["MEmpNro"]); ?>" />
          </tr>
          <tr>
            <td class="formulario-etiquetas">Nombre:</td>
            <td class="formulario-textbox"><?php print($aRegistro["MEmpApellido"] ." ".$aRegistro["MEmpNombres"]); ?></td>
            <td class="formulario-etiquetas">Email:</td>
            <td class="formulario-textbox"><?php print($aRegistro["MEmpEmail"]); ?></td>
          </tr>
		  
		  
		  
		  
          <tr>
            <td height="30" colspan="4" valign="bottom" class="detalle-seccion">Datos de publicaci&oacute;n </td>
          </tr>
          <tr>
            <td class="formulario-etiquetas">N&ordm; referencia/pedido:</td>
            <td class="formulario-textbox"><input name="OfeReferencia" type="text" class="formulario-textbox" id="OfeReferencia" style="width: 70px;" maxlenght="10" value="<?php print($aRegistro["OfeReferencia"]); ?>" <?php if($m_lIDRegistro) print "readonly='readonly'";?> /></td>
            <td class="formulario-etiquetas">Vacantes:</td>
            <td class="formulario-textbox"><input name="OfeCantPost" type="text" class="formulario-textbox " id="OfeCantPost" style="width: 70px;" value="<?php print($aRegistro["OfeCantPost"]); ?>" maxlenght="10" /></td>
          </tr>
          <tr>
            <td class="formulario-etiquetas">&Aacute;rea de Inter&eacute;s :</td>
            <td colspan="3"><select name="AreNro" id="AreNro" style="width: 300px;" onchange="cargarListas(this, 'PueNro')">
                <?php
					$sSQL = "SELECT AreNro, AreNom FROM area  ";
					$sSQL .= "ORDER BY AreNom ASC ";
					print(GenerarOptions($sSQL, $aRegistro["AreNro"]));
			  ?>
            </select></td>
          </tr>
          <tr>
            <td class="formulario-etiquetas">Puesto:</td>
            <td colspan="3"><select name="PueNro" id="PueNro" style="width: 300px;" >
              <?php
					$sSQL = "SELECT PueNro, PueNom FROM puesto  ";
					$sSQL .= "ORDER BY PueNom ASC ";
					print(GenerarOptions($sSQL, $aRegistro["PueNro"]));
			  ?>
            </select></td>
          </tr>
          <tr>
            <td class="formulario-etiquetas">T&iacute;tulo:</td>
            <td colspan="3"><input name="OfeTitulo" type="text" class="formulario-textbox" id="OfeTitulo" style="width: 300px;" maxlenght="64" value="<?php print($aRegistro["OfeTitulo"]); ?>" /></td>
          </tr>
          <tr valign="top">
            <td class="formulario-etiquetas">Descripci&oacute;n:</td>
            <td colspan="3"><textarea name="OfeOferta" class="formulario-textbox" id="OfeOferta" style="width: 300px;" maxlenght="512"><?php print($aRegistro["OfeOferta"]); ?></textarea></td>
          </tr>
<tr>
            <td class="formulario-etiquetas">Zona:</td>
            <td colspan="3"><select name="ZonNro" id="ZonNro" style="width: 300px;">
                <?php
					$sSQL = "SELECT ZonNro, ZonDescrip FROM zona  ";
					$sSQL .= "ORDER BY ZonDescrip ASC ";
					print(GenerarOptions($sSQL, $aRegistro["ZonNro"]));
			  ?>
            </select></td>
          </tr>
<tr>
  <td height="30" colspan="4" valign="bottom" class="detalle-seccion">Datos de la oferta </td>
  <input name="MEmpNro" type="hidden" value="<?php print($aRegistro["MEmpNro"]); ?>" />
</tr>
<tr>
  <td class="formulario-etiquetas">Tipo de B&uacute;squeda:</td>
  <td colspan="3"><select name="TPeNro" id="TPeNro" style="width: 300px;">
    <?php
					$sSQL = "SELECT idtipo, nombre FROM _tipo_pedido  ";
					print(GenerarOptions($sSQL, $aRegistro["idtipo"]));
			  ?>
  </select></td>
</tr>
<tr>
  <td class="formulario-etiquetas">Edad:</td>
  <td colspan="3" class="formulario-textbox">Desde:
    <input name="OfeDeEdad" type="text" class="formulario-textbox" id="OfeDeEdad" style="width: 110px;" maxlenght="2" value="<?php print($aRegistro["OfeDeEdad"]); ?>" />
    / Hasta:
    <input name="OfeHtaEdad" type="text" class="formulario-textbox" id="OfeHtaEdad" style="width: 110px;" maxlenght="64" value="<?php print($aRegistro["OfeHtaEdad"]); ?>" maxlength="2"/></td>
</tr>
<tr>
  <td class="formulario-etiquetas">Sexo:</td>
  <td colspan="3"><select name="OfeSexo" id="OfeSexo" style="width: 300px;">
    <?php
					$sSQL = "SELECT idsexo, nombre FROM _sexos  ";
					print(GenerarOptions($sSQL, $aRegistro["OfeSexo"]));
			  ?>
  </select></td>
</tr>
<tr>
  <td class="formulario-etiquetas">Nivel de Educaci&oacute;n: </td>
  <td colspan="3"><table border="0" cellspacing="0" cellpadding="0" class="formulario-textbox">
<!--    <tr>
      <td>Nivel</td>
      <td>Estado</td>
      <td>Excluyente</td>
    </tr>
-->    <tr>
      <td><select name="OfeEducacion" id="OfeEducacion" style="width: 120px;">
        <?php
					$sSQL = "SELECT idnivel, nombre FROM _nivel_educacion  ";
					print(GenerarOptions($sSQL, $aRegistro["OfeEducacion"]));
			  ?>
      </select></td>
<!--      <td><select name="idestadodeestudio" id="idestadodeestudio" style="width: 120px;">
          <?php
					$sSQL = "SELECT idestadodeestudio, nombre FROM estadosdeestudio  ";
					$sSQL .= "ORDER BY idestadodeestudio ASC ";
					print(GenerarOptions($sSQL, $aRegistro["idestadodeestudio"]));
			  ?>
      </select></td>
      <td align="right"><table width="80" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="20"><input name="excluyenteestudio" type="radio" value="S" <?php if($aRegistro["excluyenteestudio"] == "S") { ?>checked="checked" <?php } ?>/></td>
            <td width="20" class="formulario-textbox">Si</td>
            <td width="20"><input name="excluyenteestudio" type="radio" value="N" <?php if($aRegistro["excluyenteestudio"] == "N") { ?>checked="checked" <?php } ?>/></td>
            <td class="formulario-textbox">No</td>
          </tr>
      </table></td>
-->    </tr>
  </table></td>
</tr>
<tr>
  <td class="formulario-etiquetas">Provincia:</td>
  <td colspan="3"><select name="OfeProvincia" id="OfeProvincia" style="width: 300px;" >
    <?php
					$sSQL = "SELECT PrvNom, PrvNom FROM provincia  ";
					$sSQL .= "ORDER BY PrvNom ASC ";
					print(GenerarOptions($sSQL, $aRegistro["OfeProvincia"]));
			  ?>
  </select></td>
</tr>
<tr>
  <td class="formulario-etiquetas">Localidad:</td>
  <td colspan="3"><input name="OfeLocalidad" type="text" class="formulario-textbox" id="OfeLocalidad" style="width: 300px;" maxlenght="64" value="<?php print($aRegistro["OfeLocalidad"]); ?>" /></td>
</tr>
<tr>
  <td class="formulario-etiquetas">Ingl&eacute;s (habla y escribe):</td>
  <td colspan="3"><span class="formulario-textbox">
    <select name="OfeIngles" id="OfeIngles" style="width: 120px;">
      <?php
					$sSQL = "SELECT idnivel, nombre FROM _nivel_idioma  ";
					print(GenerarOptions($sSQL, $aRegistro["OfeIngles"]));
			  ?>
    </select>
  </span></td>
</tr>
<tr>
  <td class="formulario-etiquetas">Experiencia Previa:</td>
  <td><span class="formulario-textbox">
    <input name="OfeExpAnios" type="text" class="formulario-textbox" id="OfeExpAnios" style="width: 70px;" maxlenght="5" value="<?php print ($aRegistro["OfeExpAnios"]); ?>" />
a&ntilde;os</span></td>
<!--  <td class="formulario-etiquetas">Excluyente:</td>
  <td><table width="80" border="0" cellspacing="0" cellpadding="0" class="formulario-textbox">
    <tr>
      <td width="20"><input name="experienciaexcluyente" type="radio" value="S" <?php if($aRegistro["experienciaexcluyente"] == "S") { ?>checked="checked" <?php } ?>/></td>
      <td width="20" class="formulario-textbox">Si</td>
      <td width="20"><input name="experienciaexcluyente" type="radio" value="N" <?php if($aRegistro["experienciaexcluyente"] == "N") { ?>checked="checked" <?php } ?>/></td>
      <td class="formulario-textbox">No</td>
    </tr>
  </table></td>
--></tr>
<tr>
  <td class="formulario-etiquetas">Remuneraci&oacute;n:</td>
  <td colspan="3"><input name="OfeRemunerac" type="text" class="formulario-textbox" id="OfeRemunerac" style="width: 300px;" maxlenght="64" value="<?php print($aRegistro["OfeRemunerac"]); ?>" /></td>
</tr>
<tr valign="top">
  <td class="formulario-etiquetas">Tareas a realizar:</td>
  <td colspan="3"><textarea name="OfeDescTar" class="formulario-textbox" id="OfeDescTar" style="width: 300px;" maxlenght="512"><?php print($aRegistro["OfeDescTar"]); ?></textarea></td>
</tr>
<tr valign="top">
  <td class="formulario-etiquetas">Clasificaci&oacute;n:</td>
  <td colspan="3"><input name="OfeClasificacion" type="text" class="formulario-textbox" id="OfeClasificacion" style="width: 300px;" value="<?php print($aRegistro["OfeClasificacion"]); ?>" maxlenght="512" /></td>
</tr>
          <tr>
            <td valign="top" class="formulario-etiquetas">Observaciones del selector:</td>
            <td colspan="3"><textarea name="OfeDescSel" class="formulario-textbox" id="OfeDescSel" style="width: 300px; height:100px;"><?php print($aRegistro["OfeDescSel"]); ?></textarea></td>
          </tr>
          <tr>
            <td class="formulario-etiquetas">Sucursal: </td>
            <td colspan="3"><select name="NodNro" id="NodNro" style="width: 300px;">
                <?php
					$sSQL = "SELECT UniNro, UniNombre FROM unidadorg  ";
					print(GenerarOptions($sSQL, $aRegistro["NodNro"]));
			  ?>
                        </select></td>
          </tr>
          <tr>
            <td height="30" class="formulario-etiquetas"><img src="images/espacio.gif" width="1" height="1"></td>
            <td colspan="3" align="center"><input name="BTN_Guardar" type="submit" id="BTN_Guardar" value="Guardar">
              <input name="BTN_Cancelar" type="reset" id="BTN_Cancelar" value="Cancelar" onclick="history.back();"></td>
          </tr>
        </table>
        </form></td>
        <td width="6" class="formulario-contenido-final"><img src="images/espacio.gif" width="1" height="1"></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="20"><img src="images/formulario-pie-inicio.jpg" width="20" height="40"></td>
        <td class="formulario-pie-bg"><img src="images/espacio.gif" width="1" height="1"></td>
        <td width="20"><img src="images/formulario-pie-final.jpg" width="20" height="40"></td>
      </tr>
    </table></td>
  </tr>
</table>
<script>
function habilitarFactura(pSelect)
{
	var pValor = pSelect.options[pSelect.selectedIndex].value;
	var factura = document.getElementById("OfeFactura");
	if (pValor == "Cumplida")
		factura.disabled = false;
	else
		factura.disabled = true;
}
</script>
