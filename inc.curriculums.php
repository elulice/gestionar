<?php require_once("inc.curriculums.asignaciones.php"); ?>
<?php require_once("inc.postulante.detalle.php"); ?>


		       <script>
			$(document).ready(function () {
				var availableTags = <?php include("feed.curriculums.apellidos.autocomplete.php");  ?>; 
				$(".labelsApellidos").autocomplete({
					minLength: 3,
					source: availableTags
				});
				var availableTags = <?php include("feed.curriculums.nombres.autocomplete.php");  ?>; 
				$(".labelsNombres").autocomplete({
					minLength: 3,
					source: availableTags
				});
				var availableTags = <?php include("feed.curriculums.documentos.autocomplete.php");  ?>; 
				$(".labelsDocumentos").autocomplete({
					minLength: 0,
					source: availableTags
				});

				
			});
			
	
			
		   </script>
		     <div style="margin:8px 0 0 18px;">
			<form id="frm_curriculums_listado_filtro" target="hidden_iframe" onsubmit="actualizar_agenda_listado();">
			   <input type="submit" style="display:none;" />
			   <table width="796" cellspacing="3" style="margin-left:40px;margin-bottom:0px;">
			      <tbody>
				 <tr>
				    <td width="230">
				       <div style="float:left;width:260px;">
					  <div class="form-label" style="width:80px;text-align:left;">Apellido:</div>
					  <input type="text" id="inputApellido" class="smallInput labelsApellidos" name="txt_apellido" style="width:130px;" />
				       </div>
				    </td>
				    <td width="230">
				       <div style="float:left;width:260px;">
					  <div class="form-label" style="width:80px;text-align:left;">Nombres:</div>
					  <input type="text" id="inputNombre" class="smallInput labelsNombres" name="txt_nombre" style="width:130px;" />
				       </div>
				    </td>
				    <td width="230">
				       <div style="float:left;width:260px;">
					  <div class="form-label" style="width:80px;text-align:left;">Documento:</div>
					  <input type="text" id="inputDocumento" class="smallInput labelsDocumentos" name="txt_dni" style="width:130px;" />
				       </div>
				    </td>
				 </tr>
				 <tr>
				    <td colspan="3" style="padding-top:10px;">
				       <div style="float:right;margin-right:32px;">
				       <a class="button_notok" onclick="$('#frm_curriculums_listado_filtro').clearForm(); actualizar_curriculums_listado();" style="margin-top:3px;"><span>Limpiar Búsqueda</span></a>
				       <a class="button_ok" onclick="actualizar_curriculums_listado();" style="margin-top:3px;"><span>Buscar</span></a>
				       </div>
				    </td>
				 </tr>
			      </tbody>
			   </table>
			</form>
		     </div>
		     <div id="div_curriculums_listado" class="navPage" style="height:220px;">
			<img src="images/loading.gif" class="loading" />
		     </div>
